/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <execinfo.h>

#include <glib/gi18n.h>
#include <gio/gio.h>

#include <gconf/gconf-client.h>

#include <gst/gst.h>

#ifdef HAVE_MEDIA_PROFILES
#include <profiles/gnome-media-profiles.h>
#endif

#include <unique/unique.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-plugin.h>
#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-mt.h>
#include <marlin/marlin-gst-extras.h>

#include <ephy/ephy-toolbars-model.h>

#include "main.h"
#include "marlin-window.h"
#include "marlin-session.h"
#include "marlin-stock.h"

static EphyToolbarsModel *tb_model = NULL;
static GList *marlin_windows = NULL; /* List of MarlinWindow */
static UniqueApp *unique_app;

char **files = NULL;
static const GOptionEntry marlin_options[] = {
	{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &files,
	  NULL, N_("[FILE...]") },
	{ NULL }

};

static void
print_backtrace (void)
{
#if defined (HAVE_BACKTRACE) && defined (HAVE_BACKTRACE_SYMBOLS_FD)
	void *array[20];
	size_t size;

#define MSG "\n######### Backtrace (Marlin version " VERSION ") ##########\n"
	write (STDERR_FILENO, MSG, strlen (MSG));
#undef MSG

	size = backtrace (array, 20);
	backtrace_symbols_fd (array, size, STDERR_FILENO);
#endif /* HAVE_BACKTRACE && HAVE_BACKTRACE_SYMBOLS_FD */
}

static void
critical_handler (const char    *log_domain,
		  GLogLevelFlags log_level,
		  const char    *message,
		  gpointer       user_data)
{
	g_log_default_handler (log_domain, log_level, message, user_data);
	print_backtrace ();
}

#ifdef HAVE_SIGNAL
static void
segv_handler (int sig)
{
#define MSG "caught SIGSEGV\n"
	write (STDERR_FILENO, MSG, strlen (MSG));
#undef MSG

	print_backtrace ();
	abort ();
}
#endif /* HAVE_SIGNAL */

static void
add_signal_handlers (void)
{
#ifdef HAVE_SIGNAL
	signal (SIGSEGV, segv_handler);
#endif /* HAVE_SIGNAL */
}

static gboolean
marlin_shutdown (gpointer data)
{
	marlin_program_destroy_default ();
	gtk_main_quit ();

	return FALSE;
}

static void
window_destroyed (GtkWidget *widget,
		  gpointer data)
{
	marlin_windows = g_list_remove (marlin_windows, widget);

	/* No windows left */
	if (marlin_windows == NULL) {
		g_idle_add ((GSourceFunc) marlin_shutdown, NULL);
	}
}

static gboolean
window_deleted (GtkWidget *window,
		GdkEventAny *event,
		gpointer data)
{
	return marlin_window_can_close (MARLIN_WINDOW (window));
}

GList *
marlin_get_window_list (void)
{
	return marlin_windows;
}

void
marlin_foreach_window (MarlinForeachFunction func,
		       gpointer closure)
{
	GList *p;

	for (p = marlin_windows; p; p = p->next) {
		func (p->data, closure);
	}
}

void
marlin_quit (void)
{
	GList *p;

	for (p = marlin_windows; p;) {
		MarlinWindow *window = p->data;

		/* p is set here instead of in the loop
		   because by the time we get back to the loop
		   p will be invalid. */
		p = p->next;
		marlin_window_close (window);
	}
}

static char *
make_window_name (void)
{
	static int sample_count = 1;
	char *name;

	if (sample_count != 1) {
		name = g_strdup_printf ("Untitled-%d", sample_count);
	} else {
		name = g_strdup ("Untitled");
	}

	sample_count++;
	return name;
}

GtkWidget *
marlin_open_window_with_sample (MarlinSample *sample)
{
	MarlinWindow *window;
	char *name;

	name = make_window_name ();

	g_object_set (G_OBJECT (sample),
		      "filename", name,
		      NULL);
	g_free (name);

	window = marlin_window_new ();
	g_object_set (G_OBJECT (window),
		      "sample", sample,
		      NULL);

	g_object_unref (sample);

	g_signal_connect (G_OBJECT (window), "delete-event",
			  G_CALLBACK (window_deleted), NULL);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (window_destroyed), NULL);

	marlin_windows = g_list_append (marlin_windows, window);

	/* Not sure how this gets replaced in unique */
/* 	gtk_unique_app_add_window (unique_app, GTK_WINDOW (window)); */

	gtk_widget_show (GTK_WIDGET (window));

	return GTK_WIDGET (window);
}

GtkWidget *
marlin_open_window (const char *uri,
		    const char *uuid,
		    GdkScreen  *screen,
		    const char *startup_id)
{
	GConfClient *client = gconf_client_get_default ();
	MarlinWindow *window;
	MarlinSample *sample;
	int rate, channels;

	/* Create a sample to be used in the window */
	sample = marlin_sample_new ();

	/* Set the defaults */
	rate = gconf_client_get_int (client, "/apps/marlin/system-state/new-sample-rate", NULL);
	channels = gconf_client_get_int (client, "/apps/marlin/system-state/new-sample-channels", NULL);
	g_object_unref (G_OBJECT (client));

	/* If there's something wrong (like the user hasn't installed
	   marlin correctly, then these can be dumb values */
	if (channels == 0) {
		channels = 2;
	}

	if (rate == 0) {
		rate = 48000;
	}

	g_object_set (G_OBJECT (sample),
		      "sample-rate", rate,
		      "channels", channels,
		      NULL);

	if (uri == NULL) {
		char *name = make_window_name ();

		g_object_set (G_OBJECT (sample),
			      "filename", name,
			      NULL);

		g_free (name);
	}

	if (uuid != NULL) {
/* 		window = marlin_window_new_from_state (sample, uuid); */
		window = NULL;
	} else {
		window = marlin_window_new ();
		g_object_set (G_OBJECT (window),
			      "sample", sample,
			      NULL);
	}

	g_object_unref (sample);

	g_signal_connect (G_OBJECT (window), "delete-event",
			  G_CALLBACK (window_deleted), NULL);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (window_destroyed), NULL);

	if (uri != NULL) {
		GtkRecentManager *manager;
		GtkRecentData data;
		gboolean uncertain;

		marlin_window_load_file (window, uri);

		manager = gtk_recent_manager_get_default ();

		data.display_name = NULL;
		data.description = NULL;
		data.mime_type = (char *) g_content_type_guess (uri + 7,
								NULL, 0,
								&uncertain);
		data.app_name = "Marlin";
		/* This should be filled with argv[0]? */
		data.app_exec = "marlin %u";
		data.groups = NULL;
		data.is_private = FALSE;

		gtk_recent_manager_add_full (manager, uri, &data);
		g_free (data.mime_type);
	}

	marlin_windows = g_list_prepend (marlin_windows, window);

	/* FIXME: Not sure what this should be */
/* 	gtk_unique_app_add_window (unique_app, GTK_WINDOW (window)); */

	gtk_widget_show (GTK_WIDGET (window));
	if (screen) {
		gtk_window_set_screen (GTK_WINDOW (window), screen);
	}

	if (startup_id) {
		gtk_window_set_startup_id (GTK_WINDOW (window), startup_id);
	}

	return (GtkWidget *) window;
}

static void
new_view_request_cb (MarlinProgram *program,
		     MarlinSample *sample,
		     gpointer userdata)
{
	marlin_open_window_with_sample (sample);
}

static void
update_toolbar_remove_flag (EphyToolbarsModel *model,
			    gpointer data)
{
	int i, n_toolbars;
	int not_removable = EGG_TB_MODEL_ICONS_ONLY;

	n_toolbars = egg_toolbars_model_n_toolbars (EGG_TOOLBARS_MODEL (model));
	if (n_toolbars <= 1) {
		not_removable &= EGG_TB_MODEL_NOT_REMOVABLE;
	}

	for (i = 0; i < n_toolbars - 1; i++) {
		egg_toolbars_model_set_flags (EGG_TOOLBARS_MODEL (model), not_removable, i);
	}
}

EphyToolbarsModel *
marlin_get_toolbars_model (void)
{
	if (tb_model == NULL) {
		tb_model = EPHY_TOOLBARS_MODEL (ephy_toolbars_model_new ());

		ephy_toolbars_model_load (tb_model);

		g_signal_connect (G_OBJECT (tb_model), "toolbar_added",
				  G_CALLBACK (update_toolbar_remove_flag),
				  NULL);
		g_signal_connect (G_OBJECT (tb_model), "toolbar_removed",
				  G_CALLBACK (update_toolbar_remove_flag),
				  NULL);
	}

	return tb_model;
}

static char *
make_full_uri (const char *path)
{
	char *retval, *curdir, *escaped;

	g_return_val_if_fail (path != NULL, NULL);

	if (strstr (path, "://") != NULL) {
		return g_strdup (path);
	}

	if (path[0] == '/') {
		escaped = g_uri_escape_string (path, NULL, TRUE);

		retval = g_strdup_printf ("file://%s", path);
		g_free (escaped);
		return retval;
	}

	curdir = g_get_current_dir ();
	retval = g_strdup_printf ("file://%s%s%s", curdir,
				  G_DIR_SEPARATOR_S, path);
	g_free (curdir);

	return retval;
}

static void
process_command_line (void)
{
	if (files == NULL) {
		marlin_open_window (NULL, NULL, NULL, NULL);
	} else {
		int i;
		/* Open a new window for each file */
		for (i = 0; files[i]; i++) {
			char *uri;

			uri = make_full_uri (files[i]);
			marlin_open_window (uri, NULL, NULL, NULL);
			g_free (uri);
		}
	}
}

static UniqueResponse
app_message_cb (UniqueApp         *app,
		int                command,
		UniqueMessageData *message,
		guint              timestamp,
		gpointer           userdata)
{
	GdkScreen *screen;
	const gchar *startup_id;
	char *filename;

	screen = unique_message_data_get_screen (message);
	startup_id = unique_message_data_get_startup_id (message);

	switch (command) {
	case UNIQUE_NEW:
		marlin_open_window (NULL, NULL, NULL, NULL);
		break;

	case UNIQUE_OPEN:
		filename = unique_message_data_get_text (message);
		marlin_open_window (filename, NULL, screen, startup_id);
		g_free (filename);
		break;

	case UNIQUE_ACTIVATE:
		if (marlin_windows) {
			gtk_window_present (GTK_WINDOW (marlin_windows->data));
		}
		break;

	default:
		break;
	}

	return UNIQUE_RESPONSE_OK;
}

static void
show_tmp_error (void)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK,
					 _("Marlin is not able to write to %s.\n"
					   "Please check the permissions and restart Marlin"),
					 marlin_get_tmp_dir ());
	gtk_widget_show (dialog);
	gtk_dialog_run (GTK_DIALOG (dialog));
}

static void
ensure_dirs_exist (void)
{
	char *path, *versioned;

	marlin_ensure_dir_exists (marlin_dot_dir ());

	/* Plugins, versioned of course */
	path = g_build_filename (marlin_dot_dir (), "plugins", NULL);
	marlin_ensure_dir_exists (path);

	versioned = g_build_filename (path, VERSION, NULL);
	marlin_ensure_dir_exists (versioned);
	g_free (versioned);
	g_free (path);
}

int
main (int argc,
      char **argv)
{
	GOptionContext *option_ctxt;
	MarlinProgram *marlin_program;
	GError *error = NULL;
#ifdef HAVE_MEDIA_PROFILES
	GConfClient *client;
#endif

	/* Init gettext */
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	/* Initialise the SIGSEGV handler */
	add_signal_handlers ();

	ensure_dirs_exist ();

	/* Initialise GThread at the very start */
	g_thread_init (NULL);

	option_ctxt = g_option_context_new (_("- Marlin"));
	g_option_context_add_main_entries (option_ctxt, marlin_options,
					   GETTEXT_PACKAGE);
	g_option_context_add_group (option_ctxt, gst_init_get_option_group ());
	g_option_context_add_group (option_ctxt, gtk_get_option_group (FALSE));
	g_option_context_set_ignore_unknown_options (option_ctxt, TRUE);

	g_option_context_parse (option_ctxt, &argc, &argv, &error);
	if (error != NULL) {
		g_error ("Error parsing options: %s", error->message);
	}

	gtk_init (&argc, &argv);

	g_set_application_name (_("Marlin Sample Editor"));

	g_log_set_handler ("Glib-GObject",
			   G_LOG_LEVEL_CRITICAL |
			   G_LOG_LEVEL_ERROR |
			   G_LOG_FLAG_FATAL |
			   G_LOG_FLAG_RECURSION,
			   critical_handler, NULL);
	g_log_set_handler ("Glib",
			   G_LOG_LEVEL_CRITICAL |
			   G_LOG_LEVEL_ERROR |
			   G_LOG_FLAG_FATAL |
			   G_LOG_FLAG_RECURSION,
			   critical_handler, NULL);
	g_log_set_handler (NULL,
			   G_LOG_LEVEL_CRITICAL |
			   G_LOG_LEVEL_ERROR |
			   G_LOG_FLAG_FATAL |
			   G_LOG_FLAG_RECURSION,
			   critical_handler, NULL);

	/* Do this here so we can throw up a dialog */
	if (marlin_check_tmp_dir () == FALSE) {
		show_tmp_error ();
		gdk_notify_startup_complete ();
		exit (1);
	}

#ifdef HAVE_MEDIA_PROFILES
	/* Init media profiles */
	client = gconf_client_get_default ();
	gnome_media_profiles_init (client);
	g_object_unref (G_OBJECT (client));
#endif

	/* IPC stuff */
	unique_app = unique_app_new ("org.gnome.Marlin", "marlin");
	if (unique_app_is_running (unique_app)) {
		UniqueMessageData *message;

		if (files == NULL) {
			unique_app_send_message (unique_app, UNIQUE_NEW, NULL);
		} else {
			int i;

			for (i = 0; files[i]; i++) {
				char *path;

				message = unique_message_data_new ();

				path = make_full_uri (files[i]);
				unique_message_data_set_text (message, path, strlen (path));

				unique_app_send_message (unique_app,
							 UNIQUE_OPEN, message);
				g_free (path);

				unique_message_data_free (message);
			}
		}

		gdk_notify_startup_complete ();
		g_object_unref (unique_app);

		exit (0);
	}

	/* Init Marlin */
/* 	marlin_mt_initialise (); */
	marlin_gst_register ();
	marlin_stock_icons_register ();

	/* FIXME: This might fail */
	marlin_plugin_initialise (NULL);

	/* Marlin is ready... */
	marlin_program = marlin_program_get_default ();
	g_signal_connect (G_OBJECT (marlin_program), "new-view",
			  G_CALLBACK (new_view_request_cb), NULL);

#if 0
	marlin_session_initialise (argv[0]);

	if (marlin_session_is_restored ()) {
		marlin_session_load ();
	} else {
		process_command_line ();
	}
#else
	process_command_line ();
#endif

	/* After the command line has been processed,
	   we can free the option context */
	g_option_context_free (option_ctxt);

	g_signal_connect (unique_app, "message-received",
			  G_CALLBACK (app_message_cb), NULL);

	gtk_main ();

	return 0;
}
