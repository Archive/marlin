/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Emmanuele Bassi
 *
 *  Copyright 2006 Emmanuele Bassi
 *
 *  This file is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation, or (at your option)
 *  any later version.
 *
 *  This file is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include "marlin-recent-action.h"

enum {
	ITEM_ACTIVATED,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0};

G_DEFINE_TYPE (MarlinRecentAction, marlin_recent_action, GTK_TYPE_ACTION);

static void
more_activated_cb (GtkMenuItem        *menu_item,
		   MarlinRecentAction *mra)
{
	GtkWidget *dialog;
	GtkRecentChooser *chooser;
	GtkRecentFilter *filter;

	dialog = gtk_recent_chooser_dialog_new ("Recent Documents",
						NULL,
						GTK_STOCK_CANCEL,
						GTK_RESPONSE_CANCEL,
						GTK_STOCK_OPEN,
						GTK_RESPONSE_ACCEPT,
						NULL);

	chooser = GTK_RECENT_CHOOSER (dialog);

	filter = gtk_recent_filter_new ();
	gtk_recent_filter_set_name (filter, _("All files"));
	gtk_recent_filter_add_pattern (filter, "*");
	gtk_recent_chooser_add_filter (chooser, filter);
	gtk_recent_chooser_set_filter (chooser, filter);

	filter = gtk_recent_filter_new ();
	gtk_recent_filter_set_name (filter, _("All audio files"));
	gtk_recent_filter_add_mime_type (filter, "audio/*");
	gtk_recent_chooser_add_filter (chooser, filter);

	filter = gtk_recent_filter_new ();
	gtk_recent_filter_set_name (filter, _("All files used by Marlin"));
	gtk_recent_filter_add_application (filter, "Marlin");
	gtk_recent_chooser_add_filter (chooser, filter);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		GtkRecentInfo *info;

		info = gtk_recent_chooser_get_current_item (chooser);
		if (info == NULL) {
			return;
		}

		g_signal_emit (mra, signals[ITEM_ACTIVATED], 0, info);

		gtk_recent_info_unref (info);
	}

	gtk_widget_destroy (dialog);
}

static void
recent_chooser_item_activated_cb (GtkRecentChooser   *chooser,
				  MarlinRecentAction *mra)
{
	GtkRecentInfo *current;

	current = gtk_recent_chooser_get_current_item (chooser);
	if (current == NULL) {
		return;
	}

	g_signal_emit (mra, signals[ITEM_ACTIVATED], 0, current);

	gtk_recent_info_unref (current);
}

static GtkWidget *
create_recent_menu (GtkAction *action)
{
	GtkWidget *chooser_menu;
	GtkWidget *item;
	GtkRecentFilter *filter;

	chooser_menu = gtk_recent_chooser_menu_new ();

	filter = gtk_recent_filter_new ();
	gtk_recent_filter_set_name (filter, _("All files used by Marlin"));
	gtk_recent_filter_add_application (filter, "Marlin");
	gtk_recent_chooser_set_filter (GTK_RECENT_CHOOSER (chooser_menu),
				       filter);

	g_signal_connect (chooser_menu, "item-activated",
			  G_CALLBACK (recent_chooser_item_activated_cb),
			  action);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (chooser_menu), item);
	gtk_widget_show (item);

	item = gtk_menu_item_new_with_mnemonic (_("_More..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (chooser_menu), item);
	g_signal_connect (item, "activate",
			  G_CALLBACK (more_activated_cb), action);
	gtk_widget_show (item);

	return chooser_menu;
}

static GtkWidget *
recent_action_create_menu_item (GtkAction *action)
{
	GtkWidget *retval;
	GtkWidget *chooser_menu;

	chooser_menu = create_recent_menu (action);
	retval = g_object_new (GTK_TYPE_IMAGE_MENU_ITEM, NULL);
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (retval), chooser_menu);

	return retval;
}

static GtkWidget *
recent_action_create_tool_item (GtkAction *action)
{
	GtkWidget *retval;
	GtkWidget *chooser_menu;

	chooser_menu = create_recent_menu (action);
	retval = g_object_new (GTK_TYPE_MENU_TOOL_BUTTON, NULL);
	gtk_menu_tool_button_set_menu (GTK_MENU_TOOL_BUTTON (retval),
				       chooser_menu);

	return retval;
}

static void
marlin_recent_action_class_init (MarlinRecentActionClass *klass)
{
	GtkActionClass *a_class;

	a_class = (GtkActionClass *) klass;

	a_class->menu_item_type = GTK_TYPE_IMAGE_MENU_ITEM;
	a_class->create_menu_item = recent_action_create_menu_item;

	a_class->toolbar_item_type = GTK_TYPE_IMAGE_MENU_ITEM;
	a_class->create_tool_item = recent_action_create_tool_item;

	signals[ITEM_ACTIVATED] = g_signal_new ("item-activated",
						G_OBJECT_CLASS_TYPE (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinRecentActionClass, item_activated),
						NULL, NULL,
						g_cclosure_marshal_VOID__BOXED,
						G_TYPE_NONE, 1,
						GTK_TYPE_RECENT_INFO);
}

static void
marlin_recent_action_init (MarlinRecentAction *mra)
{
}

MarlinRecentAction *
marlin_recent_action_new (const char *name)
{
	return g_object_new (MARLIN_TYPE_RECENT_ACTION,
			     "name", name,
			     "label", "Open _Recent",
			     "tooltip", NULL,
			     "stock-id", NULL,
			     NULL);
}

MarlinRecentAction *
marlin_recent_action_toolbar_new (const char *name,
				  const char *label,
				  const char *stock_id)
{
	return g_object_new (MARLIN_TYPE_RECENT_ACTION,
			     "name", name,
			     "label", label,
			     "tooltip", NULL,
			     "stock-id", stock_id,
			     NULL);
}
