/*
 * Copyright (C) 2007 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <config.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "koto-undo-action.h"

G_DEFINE_TYPE (KotoUndoAction, koto_undo_action, GTK_TYPE_ACTION);

#define KOTO_UNDO_ACTION_GET_PRIVATE(obj)      \
        (G_TYPE_INSTANCE_GET_PRIVATE ((obj),    \
         KOTO_TYPE_UNDO_ACTION,                \
         KotoUndoActionPrivate))

typedef struct {
  MarlinUndoManager *manager;
  guint changed_id;
  gboolean undo_mode;
} KotoUndoActionPrivate;

enum {
  PROP_0,
  PROP_MANAGER,
  PROP_MODE,
};

static void
on_manager_changed (MarlinUndoManager *manager, gpointer user_data)
{
  KotoUndoActionPrivate *priv = KOTO_UNDO_ACTION_GET_PRIVATE (user_data);
  GtkAction *action = GTK_ACTION (user_data);
  char *label;
  gboolean sensitive;

  sensitive = priv->undo_mode ? marlin_undo_manager_can_undo (manager) :
    marlin_undo_manager_can_redo (manager);

  if (sensitive) {
    if (priv->undo_mode) {
      label = g_strdup_printf (_("Undo '%s'"),
			       marlin_undo_manager_get_undo_name (manager));
    } else {
      label = g_strdup_printf (_("Redo '%s'"),
			       marlin_undo_manager_get_redo_name (manager));
    }

    g_object_set (action,
                  "sensitive", TRUE,
                  "label", label,
                  NULL);
    g_free (label);
  } else {
    g_object_set (action,
                  "sensitive", FALSE,
                  "label", priv->undo_mode ? _("Undo") : _("Redo"),
                  NULL);
  }
}

static void
koto_undo_action_activate (GtkAction *action)
{
  KotoUndoActionPrivate *priv = KOTO_UNDO_ACTION_GET_PRIVATE (action);
  MarlinUndoManager *manager;

  manager = KOTO_UNDO_ACTION_GET_PRIVATE (action)->manager;
  
  if (priv->undo_mode) {
    if (!marlin_undo_manager_can_undo (manager)) {
      g_warning ("Undo action should be disabled");
      return;
    }
  
    marlin_undo_manager_undo (manager);
  } else {
    if (!marlin_undo_manager_can_redo (manager)) {
      g_warning ("Redo action should be disabled");
      return;
    }

    marlin_undo_manager_redo (manager);
  }
}

static void
koto_undo_action_set_property (GObject *gobject, guint prop_id,
                               const GValue *value, GParamSpec *pspec)
{
  KotoUndoActionPrivate *priv = KOTO_UNDO_ACTION_GET_PRIVATE (gobject);

  switch (prop_id) {
  case PROP_MANAGER:
    if (priv->manager) {
      g_signal_handler_disconnect (priv->manager, priv->changed_id);
      g_object_unref (priv->manager);
    }
    priv->manager = (MarlinUndoManager *) g_value_dup_object (value);
    priv->changed_id = g_signal_connect (priv->manager, "changed",
                                         G_CALLBACK (on_manager_changed), gobject);
    /* Update the state */
    on_manager_changed (priv->manager, gobject);
    break;

  case PROP_MODE:
    priv->undo_mode = g_value_get_boolean (value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
    return;
  }
  /* TODO: propagate to proxies */
}

static void
koto_undo_action_get_property (GObject *gobject, guint prop_id,
                               GValue *value, GParamSpec *pspec)
{
  KotoUndoActionPrivate *priv = KOTO_UNDO_ACTION_GET_PRIVATE (gobject);

  switch (prop_id) {
  case PROP_MANAGER:
    g_value_set_object (value, priv->manager);
    break;

  case PROP_MODE:
    g_value_set_boolean (value, priv->undo_mode);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
    return;
  }
}

static void
koto_undo_action_dispose (GObject *gobject)
{
  KotoUndoActionPrivate *priv = KOTO_UNDO_ACTION_GET_PRIVATE (gobject);

  if (priv->manager) {
    g_object_unref (priv->manager);
    priv->manager = NULL;
  }

  G_OBJECT_CLASS (koto_undo_action_parent_class)->dispose (gobject);
}

static void
koto_undo_action_class_init (KotoUndoActionClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkActionClass *action_class = GTK_ACTION_CLASS (klass);

  g_type_class_add_private (klass, sizeof (KotoUndoActionPrivate));

  gobject_class->dispose = koto_undo_action_dispose;
  gobject_class->set_property = koto_undo_action_set_property;
  gobject_class->get_property = koto_undo_action_get_property;

  action_class->activate = koto_undo_action_activate;
  //action_class->connect_proxy = gtk_recent_action_connect_proxy;
  //action_class->disconnect_proxy = gtk_recent_action_disconnect_proxy;
  action_class->menu_item_type = GTK_TYPE_IMAGE_MENU_ITEM;
  
  g_object_class_install_property (gobject_class, PROP_MANAGER,
                                   g_param_spec_object ("manager", "manager", "Undo manager",
                                                        MARLIN_UNDO_MANAGER_TYPE,
                                                        G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, PROP_MODE,
				   g_param_spec_boolean ("mode", "", "",
							 TRUE,
							 G_PARAM_READWRITE));
}

static void
koto_undo_action_init (KotoUndoAction *action)
{
}

GtkAction *
koto_undo_action_new (const char        *name, 
		      MarlinUndoManager *undo_manager,
		      gboolean           undo_mode)
{
  g_return_val_if_fail (IS_MARLIN_UNDO_MANAGER (undo_manager), NULL);

  return g_object_new (KOTO_TYPE_UNDO_ACTION,
                       "name", name,
                       "label", "Undo something",
                       "stock-id", undo_mode ? GTK_STOCK_UNDO : GTK_STOCK_REDO,
                       "manager", undo_manager,
		       "mode", undo_mode,
                       NULL);
}

