/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/***************************************************************************
 *            gtkvumeter.c
 *
 *  Fri Jan 10 20:06:23 2003
 *  Copyright  2003  Todd Goyen
 *  wettoad@knighthoodofbuh.org
 *  Modified by Iain Holmes - Copyright (C) 2004-2008
 ****************************************************************************/

#include <math.h>
#include <gtk/gtk.h>

#include <marlin/marlin-types.h>
#include <marlin/marlin-utils.h>

#include "gtkvumeter.h"

#define MIN_HORIZONTAL_VUMETER_WIDTH   400
#define HORIZONTAL_VUMETER_HEIGHT  20
#define VERTICAL_VUMETER_WIDTH     20
#define MIN_VERTICAL_VUMETER_HEIGHT    200

G_DEFINE_TYPE (GtkVUMeter, gtk_vumeter, GTK_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_SCALE
};

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (gtk_vumeter_parent_class)->finalize (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	GtkVUMeter *vu = GTK_VUMETER (object);
	GtkWidget *widget = GTK_WIDGET (object);

	switch (prop_id) {
	case PROP_SCALE:
		vu->scale = g_value_get_enum (value);
		if (vu->scale == MARLIN_SCALE_LINEAR) {
			g_warning ("MARLIN_SCALE_LINEAR does not work");
			g_warning ("Forcing MARLIN_SCALE_LOG");
			vu->scale = MARLIN_SCALE_LOG;
		}

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle rect;

			rect.x = 0;
			rect.y = 0;
			rect.width = widget->allocation.width;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &rect, FALSE);
		}
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
}

static void
free_colors (GtkVUMeter *vumeter)
{
	if (vumeter->f_colors) {
		gdk_colormap_free_colors (vumeter->colormap, vumeter->f_colors,
					  vumeter->colors);
		g_free (vumeter->f_colors);
		vumeter->f_colors = NULL;
	}

	if (vumeter->b_colors) {
		gdk_colormap_free_colors (vumeter->colormap, vumeter->b_colors,
					  vumeter->colors);
		g_free (vumeter->b_colors);
		vumeter->b_colors = NULL;
	}
}

static void
setup_colors (GtkVUMeter *vumeter)
{
	int index, colours;
	int f_step, b_step;
	int first, second;
	int log_max = 0;

	g_return_if_fail (vumeter->colormap != NULL);

	if (vumeter->orientation == GTK_VUMETER_VERTICAL) {
		colours = MAX (GTK_WIDGET (vumeter)->allocation.height - 2, 0);
	} else {
		colours = MAX (GTK_WIDGET (vumeter)->allocation.width - 2, 0);
	}

	if (colours == vumeter->colors) {
		return;
	}

	free_colors (vumeter);

	vumeter->colors = colours;
	if (vumeter->colors == 0)
		return;

	vumeter->f_colors = g_malloc (vumeter->colors * sizeof (GdkColor));
	vumeter->b_colors = g_malloc (vumeter->colors * sizeof (GdkColor));

	if (vumeter->scale == MARLIN_SCALE_LINEAR) {
		first = vumeter->colors / 2;
		second = vumeter->colors;
	} else {
		log_max = -20 * log10 (1.0 / 20.0);

		first = (int) ((double) vumeter->colors * (6.0 / log_max));
		second = (int) ((double) vumeter->colors * (18.0 / log_max));
	}

	vumeter->f_colors[0].red = 65535;
	vumeter->f_colors[0].green = 0;
	vumeter->f_colors[0].blue = 0;

	vumeter->b_colors[0].red = 49151;
	vumeter->b_colors[0].green = 0;
	vumeter->b_colors[0].blue = 0;

	/* Allocate from Red to Yellow */
	f_step = 65535 / (first - 1);
	b_step = 49151 / (first - 1);

	for (index = 1; index < first; index++) {
		vumeter->f_colors[index].red = 65535;
		vumeter->f_colors[index].green = vumeter->f_colors[index - 1].green + f_step;
		vumeter->f_colors[index].blue = 0;

		vumeter->b_colors[index].red = 49151;
		vumeter->b_colors[index].green = vumeter->b_colors[index - 1].green + b_step;
		vumeter->b_colors[index].blue = 0;
	}

	/* Allocate from Yellow to Green */
	f_step = 65535 / (second - first);
	b_step = 49151 / (second - first);

	for (index = first; index < second; index++) {
		vumeter->f_colors[index].red = vumeter->f_colors[index - 1].red - f_step;
		vumeter->f_colors[index].green = vumeter->f_colors[index - 1].green;
		vumeter->f_colors[index].blue = 0;

		vumeter->b_colors[index].red = vumeter->b_colors[index - 1].red - b_step;
		vumeter->b_colors[index].green = vumeter->b_colors[index - 1].green;
		vumeter->b_colors[index].blue = 0;
	}

	if (vumeter->scale == MARLIN_SCALE_LOG) {
		/* Allocate from Green to Dark Green */
		f_step = 32767 / (vumeter->colors - second);
		b_step = 32767 / (vumeter->colors - second);

		for (index = second; index < vumeter->colors; index++) {
			vumeter->f_colors[index].red = 0;
			vumeter->f_colors[index].green = vumeter->f_colors[index - 1].green - f_step;
			vumeter->f_colors[index].blue = 0;

			vumeter->b_colors[index].red = 0;
			vumeter->b_colors[index].green = vumeter->b_colors[index - 1].green - b_step;
			vumeter->b_colors[index].blue = 0;
		}
	}

	for (index = 0; index < vumeter->colors; index++) {
		gdk_colormap_alloc_color (vumeter->colormap,
					  &vumeter->f_colors[index],
					  FALSE, TRUE);

		gdk_colormap_alloc_color (vumeter->colormap,
					  &vumeter->b_colors[index],
					  FALSE, TRUE);
	}
}

static int
sound_level_to_draw_level (GtkVUMeter *vumeter,
			   double      level)
{
	double draw_level;
	double height;

	height = GTK_WIDGET (vumeter)->allocation.height - 2;

	if (vumeter->scale == MARLIN_SCALE_LINEAR) {
		draw_level = 0;
	} else {
		draw_level = level * height;
	}

	return (int) draw_level;
}

static void
realize (GtkWidget *widget)
{
	GtkVUMeter *vumeter = (GtkVUMeter *) widget;

	GTK_WIDGET_CLASS (gtk_vumeter_parent_class)->realize (widget);

	/* colors */
	vumeter->colormap = gdk_colormap_get_system ();
	setup_colors (vumeter);

	/* Set up GC */
	vumeter->gc = gdk_gc_new (widget->window);
	gdk_gc_copy (vumeter->gc, widget->style->fg_gc[GTK_STATE_NORMAL]);
}

static void
unrealize (GtkWidget *widget)
{
	GtkVUMeter *vumeter = (GtkVUMeter *) widget;

	free_colors (vumeter);
	g_object_unref (G_OBJECT (vumeter->gc));

	GTK_WIDGET_CLASS (gtk_vumeter_parent_class)->unrealize (widget);
}

static void
size_request (GtkWidget      *widget,
	      GtkRequisition *requisition)
{
	GtkVUMeter *vumeter;

	vumeter = GTK_VUMETER (widget);

	switch (vumeter->orientation) {
	case GTK_VUMETER_VERTICAL:
		requisition->width = VERTICAL_VUMETER_WIDTH;
		requisition->height = MIN_VERTICAL_VUMETER_HEIGHT;
		break;

	case GTK_VUMETER_HORIZONTAL:
		requisition->width = MIN_HORIZONTAL_VUMETER_WIDTH;
 		requisition->height = HORIZONTAL_VUMETER_HEIGHT;
		break;
	}
}

static void
size_allocate (GtkWidget     *widget,
	       GtkAllocation *allocation)
{
	GtkVUMeter *vumeter;

	widget->allocation = *allocation;
	vumeter = GTK_VUMETER (widget);

	if (GTK_WIDGET_REALIZED (widget) == FALSE) {
		return;
	}

	setup_colors (vumeter);
}

static gint
expose (GtkWidget      *widget,
	GdkEventExpose *event)
{
	GtkVUMeter *vumeter;
	int index, rms_level, peak_level;
	int x, y;
	int width, height;

	if (event->count > 0) {
		return FALSE;
	}

	vumeter = GTK_VUMETER (widget);
	rms_level = sound_level_to_draw_level (vumeter, vumeter->rms_level);
	peak_level = sound_level_to_draw_level (vumeter, vumeter->peak_level);

	x = widget->allocation.x;
	y = widget->allocation.y;
	width = widget->allocation.width;
	height = widget->allocation.height;

	switch (vumeter->orientation) {
	case GTK_VUMETER_VERTICAL:
		height -= 3;

		for (index = rms_level; index < peak_level; index++) {
			gdk_gc_set_foreground (vumeter->gc,
					       &vumeter->b_colors[index]);
			gdk_draw_line (widget->window, vumeter->gc,
				       x + 1, (y + height) - index,
				       x + (width - 3), (y + height) - index);
		}

		for (index = 0; index < rms_level; index++) {
			gdk_gc_set_foreground (vumeter->gc,
					       &vumeter->f_colors[index]);
			gdk_draw_line (widget->window, vumeter->gc,
				       x + 1, (y + height) - index,
				       x + (width - 3), (y + height) - index);
		}

		height += 3;
		gtk_paint_shadow (widget->style, widget->window,
				  GTK_STATE_NORMAL, GTK_SHADOW_IN,
				  NULL, widget, "", x, y, width, height);
		break;

	case GTK_VUMETER_HORIZONTAL:
		width = widget->allocation.width;
		height = widget->allocation.height - 2;

		/* draw border */
		/* FIXME: Tell Thomas there's a bug in Clearlooks
		   that means I can't use trough as the detail here */
		gtk_paint_box (widget->style, widget->window,
			       GTK_STATE_NORMAL, GTK_SHADOW_IN,
			       NULL, widget, "", 0, 0,
			       width, widget->allocation.height);

		for (index = rms_level; index < peak_level; index++) {
			gdk_gc_set_foreground (vumeter->gc,
					       &vumeter->b_colors[index]);
			gdk_draw_line (widget->window, vumeter->gc,
				       width - index - 1, 1,
				       width - index - 1, height);
		}

		for (index = peak_level; index < width - 2; index++) {
			gdk_gc_set_foreground (vumeter->gc,
					       &vumeter->f_colors[index]);
			gdk_draw_line (widget->window, vumeter->gc,
				       width - index - 1, 1,
				       width - index - 1, height);
		}

		break;

	default:
		break;
	}

	return FALSE;
}

static void
gtk_vumeter_class_init (GtkVUMeterClass *class)
{
	GObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GObjectClass*) class;
	widget_class = (GtkWidgetClass*) class;

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->realize = realize;
	widget_class->unrealize = unrealize;
	widget_class->expose_event = expose;
	widget_class->size_request = size_request;
	widget_class->size_allocate = size_allocate;

	g_object_class_install_property (object_class,
					 PROP_SCALE,
					 g_param_spec_enum ("scale", "", "",
							    MARLIN_TYPE_SCALE,
							    MARLIN_SCALE_LOG,
							    G_PARAM_READWRITE));
}

static void
gtk_vumeter_init (GtkVUMeter *vumeter)
{
	GTK_WIDGET_SET_FLAGS (vumeter, GTK_NO_WINDOW);

	vumeter->peaks_falloff = GTK_VUMETER_PEAKS_FALLOFF_MEDIUM;

	vumeter->gc = NULL;

	vumeter->rms_level = 0.0;
	vumeter->peak_level = 0.0;
	vumeter->scale = MARLIN_SCALE_LOG;
}


/* Orientation should be a property */
GtkWidget*
gtk_vumeter_new (GtkVUMeterOrientation orientation)
{
	GtkVUMeter *vumeter;

	vumeter = gtk_type_new (GTK_TYPE_VUMETER);
	vumeter->orientation = orientation;
	return GTK_WIDGET (vumeter);
}

void
gtk_vumeter_set_levels (GtkVUMeter *vumeter,
			double      rms,
			double      peak)
{
	g_return_if_fail (vumeter != NULL);
	g_return_if_fail (GTK_IS_VUMETER (vumeter));

	vumeter->rms_level = rms;
	vumeter->peak_level = peak;

	gtk_widget_queue_draw (GTK_WIDGET (vumeter));
}

/* Should be set as a property */
void
gtk_vumeter_set_peaks_falloff (GtkVUMeter       *vumeter,
			       GtkVUMeterFallOff falloff)
{
	g_return_if_fail (GTK_IS_VUMETER (vumeter));

	vumeter->peaks_falloff = falloff;
}
