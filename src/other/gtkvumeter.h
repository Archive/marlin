/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/***************************************************************************
 *            gtkvumeter.h
 *
 *  Fri Jan 10 20:06:41 2003
 *  Copyright  2003  Todd Goyen
 *  wettoad@knighthoodofbuh.org
 *  Modified Iain Holmes <iaingnome@gmail.com>
 *  Copyright  2004 - 2008 Iain Holmes
 ****************************************************************************/

#ifndef __GTKVUMETER_H__
#define __GTKVUMETER_H__

#include <gtk/gtk.h>

#include <marlin/marlin-types.h>

G_BEGIN_DECLS

#define GTK_TYPE_VUMETER                (gtk_vumeter_get_type ())
#define GTK_VUMETER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_VUMETER, GtkVUMeter))
#define GTK_VUMETER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_VUMETER GtkVUMeterClass))
#define GTK_IS_VUMETER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_VUMETER))

typedef struct _GtkVUMeter      GtkVUMeter;
typedef struct _GtkVUMeterClass GtkVUMeterClass;

typedef enum {
	GTK_VUMETER_PEAKS_FALLOFF_SLOW,
	GTK_VUMETER_PEAKS_FALLOFF_MEDIUM,
	GTK_VUMETER_PEAKS_FALLOFF_FAST
} GtkVUMeterFallOff;

typedef enum {
	GTK_VUMETER_VERTICAL,
	GTK_VUMETER_HORIZONTAL
} GtkVUMeterOrientation;

struct _GtkVUMeter {
	GtkWidget widget;

	GdkColormap *colormap;
	int colors;

	GdkColor *f_colors;
	GdkColor *b_colors;

	GdkGC *gc;

	GtkVUMeterOrientation orientation;
	double rms_level;

	GtkVUMeterFallOff peaks_falloff;
	int delay_peak_level;
	double peak_level;

	MarlinScale scale;
};

struct _GtkVUMeterClass {
	GtkWidgetClass  parent_class;
};

GType    gtk_vumeter_get_type (void) G_GNUC_CONST;
GtkWidget *gtk_vumeter_new (GtkVUMeterOrientation orientation);
void gtk_vumeter_set_levels (GtkVUMeter *vumeter, double rms, double peak);
void gtk_vumeter_set_peaks_falloff (GtkVUMeter *vumeter,
				    GtkVUMeterFallOff peaks_falloff);

G_END_DECLS

#endif /* __GTKVUMETER_H__ */
