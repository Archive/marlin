/*
 * Copyright (C) 2007 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KOTO_UNDO_ACTION_H
#define _KOTO_UNDO_ACTION_H

#include <gtk/gtk.h>
#include <marlin/marlin-undo-manager.h>

G_BEGIN_DECLS

#define KOTO_TYPE_UNDO_ACTION                  (koto_undo_action_get_type ())
#define KOTO_UNDO_ACTION(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOTO_TYPE_UNDO_ACTION, KotoUndoAction))
#define KOTO_IS_UNDO_ACTION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOTO_TYPE_UNDO_ACTION))
#define KOTO_UNDO_ACTION_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), KOTO_TYPE_UNDO_ACTION, KotoUndoActionClass))
#define KOTO_IS_UNDO_ACTION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), KOTO_TYPE_UNDO_ACTION))
#define KOTO_UNDO_ACTION_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), KOTO_TYPE_UNDO_ACTION, KotoUndoActionClass))

typedef struct _KotoUndoAction KotoUndoAction;
typedef struct _KotoUndoActionClass KotoUndoActionClass;

struct _KotoUndoAction {
  GtkAction parent;
};

struct _KotoUndoActionClass {
  GtkActionClass parent;
};

GType koto_undo_action_get_type (void) G_GNUC_CONST;

GtkAction *koto_undo_action_new (const char        *name,
				 MarlinUndoManager *undo_manager,
				 gboolean           undo_mode);

G_END_DECLS

#endif /* _KOTO_UNDO_ACTION_H */
