/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2003 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-program.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>

#include "marlin-clipboard-info.h"

static void
fill_in_clipboard_details (MarlinClipboardInfo *info,
			   MarlinSample *clipboard)
{
	guint64 total, ms, size;
	guint rate, channels;
	char *text;

	g_object_get (G_OBJECT (clipboard),
		      "total_frames", &total,
		      "sample_rate", &rate,
		      "channels", &channels,
		      NULL);

	text = g_strdup_printf ("%d hz (%s)", rate, channels == 2 ? _("Stereo") : _("Mono"));
	gtk_label_set_text (GTK_LABEL (info->attributes), text);
	g_free (text);

	text = g_strdup_printf ("%llu", total);
	gtk_label_set_text (GTK_LABEL (info->frames), text);
	g_free (text);

	ms = marlin_frames_to_ms (total, rate);
	text = marlin_ms_to_time_string (ms);
	gtk_label_set_text (GTK_LABEL (info->length), text);
	g_free (text);

	gtk_label_set_text (GTK_LABEL (info->src),
			    marlin_program_get_clip_source (marlin_program_get_default ()));

	size = channels * total * sizeof (float);
	text = g_strdup_printf ("%llu bytes", size);
	gtk_label_set_text (GTK_LABEL (info->size), text);
	g_free (text);
}

static void
clip_changed (MarlinProgram *program,
	      MarlinClipboardInfo *info)
{
	MarlinSample *clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));

	fill_in_clipboard_details (info, clipboard);
}

static void
play_clipboard (GtkButton *button,
		MarlinClipboardInfo *info)
{
	MarlinProgram *program = marlin_program_get_default ();

	marlin_program_play_clipboard (program);
}

static void
dialog_response_cb (GtkDialog *dialog,
		    guint response_id,
		    MarlinClipboardInfo *info)
{
	MarlinProgram *program = marlin_program_get_default ();

	switch (response_id) {
	default:
		break;
	}

	g_signal_handler_disconnect (G_OBJECT (program), info->clip_id);

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

#define PACK_TABLE(t, w, x, y) gtk_table_attach (GTK_TABLE (t), (w),\
						 x, x + 1, y, y + 1,\
						 GTK_FILL, GTK_FILL, 0, 0)
MarlinClipboardInfo *
marlin_clipboard_info_new (GtkWindow *parent)
{
	MarlinClipboardInfo *info;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));
	GtkWidget *vbox, *table, *label, *button;

	info = g_new (MarlinClipboardInfo, 1);

	info->dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (info->dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (info->dialog), FALSE);
	gtk_window_set_default_size (GTK_WINDOW (info->dialog), 300, 130);
	gtk_window_set_transient_for (GTK_WINDOW (info->dialog), parent);
	gtk_window_set_title (GTK_WINDOW (info->dialog), _("Clipboard Info"));

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->dialog)->vbox), vbox);
	table = gtk_table_new (5, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
	gtk_widget_show (table);

	label = marlin_make_title_label (_("Source:"));
	gtk_widget_show (label);
	PACK_TABLE (table, label, 0, 0);

	info->src = marlin_make_info_label ("");
	gtk_widget_show (info->src);
	PACK_TABLE (table, info->src, 1, 0);

	label = marlin_make_title_label (_("Attributes:"));
	gtk_widget_show (label);
	PACK_TABLE (table, label, 0, 1);

	info->attributes = marlin_make_info_label ("");
	gtk_widget_show (info->attributes);
	PACK_TABLE (table, info->attributes, 1, 1);

	label = marlin_make_title_label (_("Length:"));
	gtk_widget_show (label);
	PACK_TABLE (table, label, 0, 2);

	info->length = marlin_make_info_label ("");
	gtk_widget_show (info->length);
	PACK_TABLE (table, info->length, 1, 2);

	label = marlin_make_title_label (_("Frames:"));
	gtk_widget_show (label);
	PACK_TABLE (table, label, 0, 3);

	info->frames = marlin_make_info_label ("");
	gtk_widget_show (info->frames);
	PACK_TABLE (table, info->frames, 1, 3);

	label = marlin_make_title_label (_("Size:"));
	gtk_widget_show (label);
	PACK_TABLE (table, label, 0, 4);

	info->size = marlin_make_info_label ("");
	gtk_widget_show (info->size);
	PACK_TABLE (table, info->size, 1, 4);

	button = gtk_button_new_with_label (_("Play Clipboard"));
	g_signal_connect (button, "clicked",
			  G_CALLBACK (play_clipboard), info);

	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

	gtk_dialog_add_button (GTK_DIALOG (info->dialog),
			       GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);

	g_signal_connect (G_OBJECT (info->dialog), "response",
			  G_CALLBACK (dialog_response_cb), info);

	info->clip_id = g_signal_connect (G_OBJECT (program), "clipboard-changed",
					  G_CALLBACK (clip_changed), info);
	fill_in_clipboard_details (info, clipboard);
	return info;
}
