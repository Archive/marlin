/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#if 0
#include <libgnome/gnome-config.h>
#include <libgnomeui/gnome-client.h>

#include <gconf/gconf-client.h>

#include "main.h"
#include "marlin-session.h"
#include "marlin-window.h"

static GnomeClient *master_client = NULL;
static const char *program_argv0 = NULL;

static void
interaction_function (GnomeClient *client,
		      int key,
		      GnomeDialogType dialog_type,
		      gpointer shutdown)
{
	gnome_interaction_key_return (key, FALSE);
}

static gboolean
client_save_yourself_cb (GnomeClient *client,
			 int phase,
			 GnomeSaveStyle save_style,
			 gboolean shutdown,
			 GnomeInteractStyle interact_style,
			 gboolean fast,
			 gpointer data)
{
	char *argv[] = { NULL, NULL };

	gnome_client_request_interaction (client,
					  GNOME_DIALOG_NORMAL,
					  interaction_function,
					  GINT_TO_POINTER (shutdown));

		/* Save all unsaved files */
#if 0
	if (GPOINTER_TO_INT (shutdown)) {
		marlin_foreach_window (save_function);
	}
#endif

	marlin_foreach_window (marlin_window_save_state, NULL);
	argv[0] = (char *) program_argv0;
	argv[1] = NULL;

	g_print ("Saving\n");
	gnome_client_set_clone_command (client, 1, argv);
	gnome_client_set_restart_command (client, 1, argv);

	return TRUE;
}
		
static void
client_die_cb (GnomeClient *client,
	       gpointer data)
{
#if 0
	marlin_quit ();
#endif
}

void
marlin_session_initialise (const char *argv0)
{
	if (master_client != NULL) {
		return;
	}

	program_argv0 = argv0;

	master_client = gnome_master_client ();

	g_signal_connect (master_client, "save-yourself",
			  G_CALLBACK (client_save_yourself_cb), NULL);
	g_signal_connect (master_client, "die",
			  G_CALLBACK (client_die_cb), NULL);
}

gboolean
marlin_session_is_restored (void)
{
	gboolean restored;

	if (master_client == NULL) {
		return FALSE;
	}

	restored = (gnome_client_get_flags (master_client) & GNOME_CLIENT_RESTORED) != 0;
	
	return restored;
}

void
marlin_session_load (void)
{
	GConfClient *client;
	char *gconf_root, *key;
	GConfValue *window_value;
	GSList *windows;

	client = gconf_client_get_default ();
	if (client == NULL) {
		/* FIXME: Graphical error? */
		g_warning ("Could not restart session");
		return;
	}
	
	gconf_root = "/apps/marlin/system-state/";

	key = g_strdup_printf ("%swindow-list", gconf_root);
	window_value = gconf_client_get (client, key, NULL);
	g_free (key);

	windows = gconf_value_get_list (window_value);
	for (; windows; windows = windows->next) {
		GConfValue *str = windows->data;
		const char *uid, *filename;
		gboolean result;
		GError *tmp_error;
		
		uid = gconf_value_get_string (str);
		key = g_strdup_printf ("%s%s/filename", gconf_root, uid);
		filename = gconf_client_get_string (client, key, NULL);
		g_free (key);

		marlin_open_window (filename, uid, NULL, NULL);

		marlin_window_remove_state (uid);
		/* Now we delete the directory */
		key = g_strdup_printf ("%s%s", gconf_root, uid);
		result = gconf_client_unset (client, key, &tmp_error);
		if (result == FALSE) {
			g_print ("Cannot remove %s: %s\n", key, tmp_error->message);
		}
		g_free (key);
	}

	key = g_strdup_printf ("%swindow-list", gconf_root);
	gconf_client_unset (client, key, NULL);
	
	return;
}
#endif
