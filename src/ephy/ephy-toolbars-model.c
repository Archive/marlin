/*
 *  Copyright (C) 2002-2004 Marco Pesenti Gritti
 *  Copyright (C) 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ephy-toolbars-model.h"

#include <string.h>

#include <marlin/marlin-file-utils.h>

#define EPHY_TOOLBARS_XML_FILE		"marlin-toolbars-2.xml"
#define EPHY_TOOLBARS_XML_VERSION	"1.0"

#define GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_TOOLBARS_MODEL, EphyToolbarsModelPrivate))

struct EphyToolbarsModelPrivate
{
	char *xml_file;
	guint timeout;
};

G_DEFINE_TYPE (EphyToolbarsModel, ephy_toolbars_model, EGG_TYPE_TOOLBARS_MODEL);

static void
finalize (GObject *object)
{
	EphyToolbarsModel *model = EPHY_TOOLBARS_MODEL (object);

	if (model->priv->timeout != 0) {
                g_source_remove (model->priv->timeout);
                model->priv->timeout = 0;
	}

	g_free (model->priv->xml_file);

	G_OBJECT_CLASS (ephy_toolbars_model_parent_class)->finalize (object);
}

static gboolean
save_changes_idle (EphyToolbarsModel *model)
{
	egg_toolbars_model_save
		(EGG_TOOLBARS_MODEL (model),
		 model->priv->xml_file,
		 EPHY_TOOLBARS_XML_VERSION);

	model->priv->timeout = 0;

	/* don't run again */
	return FALSE;
}

static void
dispose (GObject *object)
{
	EphyToolbarsModel *model = EPHY_TOOLBARS_MODEL (object);

	save_changes_idle (model);

	G_OBJECT_CLASS (ephy_toolbars_model_parent_class)->dispose (object);
}

static void
ephy_toolbars_model_class_init (EphyToolbarsModelClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	object_class->dispose = dispose;
	object_class->finalize = finalize;

	g_type_class_add_private (object_class, sizeof (EphyToolbarsModelPrivate));
}

static void
save_changes (EphyToolbarsModel *model)
{
	if (model->priv->timeout == 0) {
		model->priv->timeout =
			g_idle_add ((GSourceFunc) save_changes_idle, model);
	}
}

static void
update_flags_and_save_changes (EphyToolbarsModel *model)
{
	EggToolbarsModel *eggmodel = EGG_TOOLBARS_MODEL (model);
	int i, n_toolbars;
	int flag = 0;

	n_toolbars = egg_toolbars_model_n_toolbars (eggmodel);

	if (n_toolbars <= 1) {
		flag |= EGG_TB_MODEL_NOT_REMOVABLE;
	}

	for (i = 0; i < n_toolbars; i++) {
		const char *t_name;
		EggTbModelFlags flags;

		t_name = egg_toolbars_model_toolbar_nth (eggmodel, i);
		g_return_if_fail (t_name != NULL);

		flags = egg_toolbars_model_get_flags (eggmodel, i);
		flags &= ~EGG_TB_MODEL_NOT_REMOVABLE;
		flags |= flag;
		egg_toolbars_model_set_flags (eggmodel, flags, i);
	}

	save_changes (model);
}

static int G_GNUC_UNUSED
get_toolbar_pos (EggToolbarsModel *model,
		 const char *name)
{
	int i, n_toolbars;

	n_toolbars = egg_toolbars_model_n_toolbars (model);

	for (i = 0; i < n_toolbars; i++) {
		const char *t_name;

		t_name = egg_toolbars_model_toolbar_nth (model, i);
		g_return_val_if_fail (t_name != NULL, -1);
		if (strcmp (name, t_name) == 0) {
			return i;
		}
	}

	return -1;
}

void
ephy_toolbars_model_load (EphyToolbarsModel *model)
{
	EggToolbarsModel *eggmodel = EGG_TOOLBARS_MODEL (model);
	gboolean success;

	success = egg_toolbars_model_load (eggmodel, model->priv->xml_file);
#if 0
	if (success == FALSE) {
		char *old_xml;
		old_xml = g_build_filename (marlin_dot_dir (),
					    "marlin-toolbars.xml",
					    NULL);
		success = egg_toolbars_model_load (eggmodel, old_xml);
		g_free (old_xml);
	}
#endif

	/* Still no success, load the default toolbars */
	if (success == FALSE) {
		success = egg_toolbars_model_load
				(eggmodel, marlin_file ("toolbars.xml"));
	}

	/* Ensure we have at least 1 toolbar */
	if (egg_toolbars_model_n_toolbars (eggmodel) < 1) {
		egg_toolbars_model_add_toolbar (eggmodel, 0, "DefaultToolbar");
	}
}

static void
ephy_toolbars_model_init (EphyToolbarsModel *model)
{
	model->priv = GET_PRIVATE (model);

	model->priv->xml_file = g_build_filename (marlin_dot_dir (),
						  EPHY_TOOLBARS_XML_FILE,
						  NULL);

	g_signal_connect_after (model, "item_added",
				G_CALLBACK (save_changes), NULL);
	g_signal_connect_after (model, "item_removed",
				G_CALLBACK (save_changes), NULL);
	g_signal_connect_after (model, "toolbar_added",
				G_CALLBACK (update_flags_and_save_changes), NULL);
	g_signal_connect_after (model, "toolbar_removed",
				G_CALLBACK (update_flags_and_save_changes), NULL);
}


EggToolbarsModel *
ephy_toolbars_model_new (void)
{
	return EGG_TOOLBARS_MODEL (g_object_new (EPHY_TYPE_TOOLBARS_MODEL, NULL));
}
