/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <time.h>

#include <sys/stat.h>

#include <glib/gi18n.h>

#include <gio/gio.h>

#include <gtk/gtk.h>

#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-sample.h>

#include "marlin-information-dialog.h"

struct _InformationDialog {
	GtkWidget *dialog;
	MarlinSample *sample;
	MarlinUndoManager *undo;

	GtkAdjustment *track_no, *bpm;
	GtkWidget *name, *artist, *album;
	GtkWidget *comments;
	GtkTextBuffer *buffer;

	char *cname, *cartist, *calbum, *ccomments;

	GstStructure *taglist;
};

static char *
get_string_from_taglist (GstTagList *taglist,
			 const char *tag)
{
	char *str;

	if (gst_tag_list_get_string (taglist, tag, &str)) {
		return str;
	} else {
		return g_strdup ("");
	}
}

static void
set_meta_data (struct _InformationDialog *idd)
{
	char *name;
	guint track_no, bpm;

	name = get_string_from_taglist (idd->taglist, GST_TAG_TITLE);
	gtk_entry_set_text (GTK_ENTRY (idd->name), name);
	g_free (name);

	name = get_string_from_taglist (idd->taglist, GST_TAG_ARTIST);
	gtk_entry_set_text (GTK_ENTRY (idd->artist), name);
	g_free (name);

	name = get_string_from_taglist (idd->taglist, GST_TAG_ALBUM);
	gtk_entry_set_text (GTK_ENTRY (idd->album), name);
	g_free (name);

	name = get_string_from_taglist (idd->taglist, GST_TAG_COMMENT);
	gtk_text_buffer_set_text (idd->buffer, name, -1);
	g_free (name);

	track_no = 0;
	gst_tag_list_get_uint (idd->taglist, GST_TAG_TRACK_NUMBER, &track_no);
	gtk_adjustment_set_value (idd->track_no, track_no);

	bpm = 120;
	gst_tag_list_get_uint (idd->taglist, MARLIN_TAG_BPM, &bpm);
	gtk_adjustment_set_value (idd->bpm, bpm);
}

struct _set_info_closure {
	MarlinSample *sample;
	GstStructure *taglist;

	char *name, *artist, *album, *comments;
	char *nname, *nartist, *nalbum, *ncomments;
	guint track_no, bpm;
	guint ntrack_no, nbpm;
};

static void
set_info_undo (gpointer data)
{
	struct _set_info_closure *c = data;

	gst_tag_list_add (GST_TAG_LIST (c->taglist),
			  GST_TAG_MERGE_REPLACE,
			  GST_TAG_ARTIST, c->artist ? c->artist : "",
			  GST_TAG_TITLE, c->name ? c->name : "",
			  GST_TAG_ALBUM, c->album ? c->album : "",
			  GST_TAG_COMMENT, c->comments ? c->comments : "",
			  GST_TAG_TRACK_NUMBER, c->track_no,
			  MARLIN_TAG_BPM, c->bpm,
			  NULL);
}

static void
set_info_redo (gpointer data)
{
	struct _set_info_closure *c = data;
	gst_tag_list_add (GST_TAG_LIST (c->taglist),
			  GST_TAG_MERGE_REPLACE,
			  GST_TAG_ARTIST, c->nartist ? c->nartist : "",
			  GST_TAG_TITLE, c->nname ? c->nname : "",
			  GST_TAG_ALBUM, c->nalbum ? c->nalbum : "",
			  GST_TAG_COMMENT, c->ncomments ? c->ncomments : "",
			  GST_TAG_TRACK_NUMBER, c->ntrack_no,
			  MARLIN_TAG_BPM, c->nbpm,
			  NULL);
}

static void
set_info_destroy (gpointer data)
{
	struct _set_info_closure *c = data;

	g_free (c->name);
	g_free (c->artist);
	g_free (c->album);
	g_free (c->comments);
	g_free (c->nname);
	g_free (c->nartist);
	g_free (c->nalbum);
	g_free (c->ncomments);

	g_free (c);
}

static void
properties_response (GtkWidget *dialog,
		     guint32 response_id,
		     struct _InformationDialog *idd)
{
	const char *name, *artist, *album;
	char *comments;
	guint track_no, bpm;
	MarlinUndoContext *ctxt;
	MarlinUndoable *u;
	struct _set_info_closure *c;
	GtkTextIter start, end;

	switch (response_id) {
	case GTK_RESPONSE_REJECT:
		set_meta_data (idd);
		return;

	case GTK_RESPONSE_CLOSE:
		break;

	case GTK_RESPONSE_OK:
		name = gtk_entry_get_text (GTK_ENTRY (idd->name));
		artist = gtk_entry_get_text (GTK_ENTRY (idd->artist));
		album = gtk_entry_get_text (GTK_ENTRY (idd->album));

		gtk_text_buffer_get_bounds (idd->buffer, &start, &end);
		comments = gtk_text_buffer_get_text (idd->buffer, &start, &end, FALSE);

		track_no = (guint) idd->track_no->value;
		bpm = (guint) idd->bpm->value;

		ctxt = marlin_undo_manager_context_begin (idd->undo,
							  _("Change Information"));
		c = g_new (struct _set_info_closure, 1);
		c->nname = g_strdup (name);
		c->nartist = g_strdup (artist);
		c->nalbum = g_strdup (album);
		c->ncomments = g_strdup (comments);
		c->name = g_strdup (idd->cname);
		c->artist = g_strdup (idd->cartist);
		c->album = g_strdup (idd->calbum);
		c->comments = g_strdup (idd->ccomments);
		c->taglist = idd->taglist;
		c->ntrack_no = track_no;
		c->track_no = (int) idd->track_no->value;
		c->nbpm = bpm;
		c->bpm = (int) idd->bpm->value;

		gst_tag_list_add (GST_TAG_LIST (idd->taglist),
				  GST_TAG_MERGE_REPLACE,
				  GST_TAG_ARTIST, artist ? artist : "",
				  GST_TAG_TITLE, name ? name : "",
				  GST_TAG_ALBUM, album ? album : "",
				  GST_TAG_COMMENT, comments ? comments : "",
				  GST_TAG_TRACK_NUMBER, track_no,
				  MARLIN_TAG_BPM, bpm,
				  NULL);

		g_free (comments);

		u = marlin_undoable_new (set_info_undo,
					 set_info_redo,
					 set_info_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
		marlin_undo_manager_context_end (idd->undo, ctxt);

		g_object_set (G_OBJECT (idd->sample),
			      "dirty", TRUE,
			      NULL);
		break;
	}

	g_object_unref (G_OBJECT (idd->undo));

	gtk_widget_destroy (dialog);
	g_free (idd->cname);
	g_free (idd->cartist);
	g_free (idd->calbum);
	g_free (idd->ccomments);
	g_free (idd);
}

GdkPixbuf *
get_pixbuf_from_icon (GIcon *icon)
{
	GtkIconTheme *theme;
	GtkIconInfo *icon_info;
	GdkPixbuf *pixbuf = NULL;
	GError *error = NULL;

	theme = gtk_icon_theme_get_default ();
	icon_info = gtk_icon_theme_lookup_by_gicon
		(theme, icon, 48, GTK_ICON_LOOKUP_USE_BUILTIN);
	if (icon_info == NULL) {
		return NULL;
	}

	pixbuf = gtk_icon_info_load_icon (icon_info, &error);
	if (error != NULL) {
		g_warning ("Error loading icon: %s", error->message);
		g_error_free (error);
	}

	gtk_icon_info_free (icon_info);
	return pixbuf;
}

static GtkWidget *
make_general_page (struct _InformationDialog *idd)
{
	GtkWidget *table, *label, *sep;
	char *filename, *name, *str;
	const char *mimetype;
	GFile *file;
	GFileInfo *info;
	int channels;
	guint64 frames, size;
	GError *error = NULL;

	g_object_get (G_OBJECT (idd->sample),
		      "total_frames", &frames,
		      "filename", &filename,
		      "channels", &channels,
		      NULL);

	file = g_file_new_for_uri (filename);
	info = g_file_query_info (file, "*", G_FILE_QUERY_INFO_NONE,
				  NULL, &error);
	if (error != NULL) {
		g_warning ("Error getting %s details: %s", filename,
			   error->message);
		g_error_free (error);
	}

	table = marlin_make_table (8, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 6);
	gtk_widget_show (table);

	if (info) {
		GtkWidget *image;
		GdkPixbuf *icon_pb;
		GIcon *icon;

		icon = g_file_info_get_icon (info);
		icon_pb = get_pixbuf_from_icon (icon);
		if (icon_pb) {
			image = gtk_image_new_from_pixbuf (icon_pb);
			gtk_misc_set_alignment (GTK_MISC (image), 1.0, 0.5);
			g_object_unref (G_OBJECT (icon_pb));

			PACK (table, image, 0, 0, GTK_FILL);
			gtk_widget_show (image);
		}
	}

	if (info) {
		name = g_path_get_basename (g_file_info_get_display_name (info));
	} else {
		name = g_path_get_basename (filename);
	}
	label = marlin_make_info_label (name);
	g_free (name);

	PACK (table, label, 1, 0, GTK_FILL);
	gtk_widget_show (label);

	sep = gtk_label_new ("");
	gtk_table_attach (GTK_TABLE (table), sep,
			  0, 2, 1, 2,
			  GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (sep);

	label = marlin_make_title_label (_("Type:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 2, GTK_FILL);
	gtk_widget_show (label);

	if (info) {
		mimetype = g_file_info_get_content_type (info);
	} else {
		mimetype = NULL;
	}

	if (mimetype) {
		char *type;

		type = g_content_type_get_description (mimetype);
		label = marlin_make_info_label (type);
		g_free (type);
	} else {
		label = marlin_make_info_label (_("Unknown"));
	}

	PACK (table, label, 1, 2, GTK_FILL);
	gtk_widget_show (label);

	label = marlin_make_title_label (_("Location:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 3, GTK_FILL);
	gtk_widget_show (label);

	name = g_path_get_dirname (filename);
	label = marlin_make_info_label (name);
	PACK (table, label, 1, 3, GTK_FILL);
	gtk_widget_show (label);
	g_free (name);

	label = marlin_make_title_label (_("Size on Disk:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 4, GTK_FILL);
	gtk_widget_show (label);

	if (info) {
		str = g_format_size_for_display (g_file_info_get_size (info));
		label = marlin_make_info_label (str);
		g_free (str);
	} else {
		label = marlin_make_info_label (_("Unknown"));
	}
	PACK (table, label, 1, 4, GTK_FILL);
	gtk_widget_show (label);

	label = marlin_make_title_label (_("Size in Memory:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 5, GTK_FILL);
	gtk_widget_show (label);

	/* FIXME: This will be a bad estimate when we are editing */
	size = frames * channels * sizeof (float);
	str = g_format_size_for_display (size);
	label = marlin_make_info_label (str);
	PACK (table, label, 1, 5, GTK_FILL);
	gtk_widget_show (label);
	g_free (str);

	sep = gtk_label_new ("");
	gtk_table_attach (GTK_TABLE (table), sep,
			  0, 2, 6, 7,
			  GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (sep);

	label = marlin_make_title_label (_("Last Modification:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 7, GTK_FILL);
	gtk_widget_show (label);

	if (info) {
		GTimeVal modified_time;
		GDate *date;
		char date_str[256];

		g_file_info_get_modification_time (info, &modified_time);

		date = g_date_new ();
		g_date_set_time_val (date, &modified_time);
		g_date_strftime (date_str, 256, "%c", date);
		g_date_free (date);

		label = marlin_make_info_label (date_str);
	} else {
		label = marlin_make_info_label (_("Unknown"));
	}
	PACK (table, label, 1, 7, GTK_FILL);
	gtk_widget_show (label);

	g_free (filename);
	g_object_unref (file);
	g_object_unref (info);

	return table;
}

static GtkWidget *
make_audio_page (struct _InformationDialog *idd)
{
	GtkWidget *table, *sw, *sep, *label, *track;
	guint64 frames;
	guint sample_rate, num_channels;
	guint trackno, bpm;
	char *name, *str;

	/* Get the details */
	g_object_get (G_OBJECT (idd->sample),
		      "name", &name,
		      "sample_rate", &sample_rate,
		      "total_frames", &frames,
		      "channels", &num_channels,
		      NULL);

	table = marlin_make_table (10, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 6);
	gtk_widget_show (table);
	
	label = marlin_make_title_label (_("Name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 0, GTK_FILL);
	gtk_widget_show (label);

	name = get_string_from_taglist (idd->taglist, GST_TAG_TITLE);
	idd->name = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (idd->name), name);

	PACK (table, idd->name, 1, 0, GTK_FILL);
	gtk_widget_show (idd->name);

	idd->cname = name;
	
	label = marlin_make_title_label (_("Artist:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 1, GTK_FILL);
	gtk_widget_show (label);

	name = get_string_from_taglist (idd->taglist, GST_TAG_ARTIST);
	idd->artist = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (idd->artist), name);
	PACK (table, idd->artist, 1, 1, GTK_FILL);
	gtk_widget_show (idd->artist);

	idd->cartist = name;

	label = marlin_make_title_label (_("Album:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 2, GTK_FILL);
	gtk_widget_show (label);

	name = get_string_from_taglist (idd->taglist, GST_TAG_ALBUM);
	idd->album = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (idd->album), name);
	PACK (table, idd->album, 1, 2, GTK_FILL);
	gtk_widget_show (idd->album);

	idd->calbum = name;

	label = marlin_make_title_label (_("Track Number:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 3, GTK_FILL);
	gtk_widget_show (label);

	trackno = 0;
	gst_tag_list_get_uint (idd->taglist, GST_TAG_TRACK_NUMBER, &trackno);
	/* FIXME: Check 99 is the highest track a CD can have */
	idd->track_no = GTK_ADJUSTMENT (gtk_adjustment_new ((double) trackno,
							    0.0, 99.0,
							    1.0, 1.0, 10.0));
	track = gtk_spin_button_new (idd->track_no, 1.0, 0);

	PACK (table, track, 1, 3, GTK_FILL);
	gtk_widget_show (track);

	label = marlin_make_title_label (_("Comments:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	PACK (table, label, 0, 4, GTK_FILL);
	gtk_widget_show (label);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC);
	PACK (table, sw, 1, 4, GTK_FILL);
	gtk_widget_show (sw);

	name = get_string_from_taglist (idd->taglist, GST_TAG_COMMENT);
	idd->comments = gtk_text_view_new ();
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (idd->comments), GTK_WRAP_WORD);
	idd->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (idd->comments));
	gtk_container_add (GTK_CONTAINER (sw), idd->comments);
	gtk_widget_show (idd->comments);

	gtk_text_buffer_set_text (idd->buffer, name, -1);
	idd->ccomments = name;

	label = marlin_make_title_label (_("Beats Per Minute:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 5, GTK_FILL);
	gtk_widget_show (label);

	bpm = 120;
	gst_tag_list_get_uint (idd->taglist, MARLIN_TAG_BPM, &bpm);
	/* Four beats a second should be enough for 
	   all the happy hardcore fans? */
	idd->bpm = GTK_ADJUSTMENT (gtk_adjustment_new ((double) bpm, 0.0, 
						       240.0, 1.0, 1.0, 10.0));
	track = gtk_spin_button_new (idd->bpm, 1.0, 0);
	
	PACK (table, track, 1, 5, GTK_FILL);
	gtk_widget_show (track);

	sep = gtk_label_new ("");
	gtk_table_attach (GTK_TABLE (table), sep,
			  0, 2, 6, 7,
			  GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (sep);

	label = marlin_make_title_label (_("Length:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 7, GTK_FILL);
	gtk_widget_show (label);

	str = marlin_ms_to_pretty_time (marlin_frames_to_ms (frames, sample_rate), TRUE);
	label = marlin_make_info_label (str);
	PACK (table, label, 1, 7, GTK_FILL);
	gtk_widget_show (label);

	g_free (str);

	label = marlin_make_title_label (_("Number of Frames:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 8, GTK_FILL);
	gtk_widget_show (label);

	str = g_strdup_printf ("%llu", frames);
	label = marlin_make_info_label (str);
	PACK (table, label, 1, 8, GTK_FILL);
	gtk_widget_show (label);

	g_free (str);

	label = marlin_make_title_label (_("Sample Rate:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 9, GTK_FILL);
	gtk_widget_show (label);

	str = g_strdup_printf ("%u Hz", sample_rate);
	label = marlin_make_info_label (str);
	PACK (table, label, 1, 9, GTK_FILL);
	gtk_widget_show (label);

	g_free (str);
	
	label = marlin_make_title_label (_("Number of Channels:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 10, GTK_FILL);
	gtk_widget_show (label);

	str = g_strdup_printf ("%d (%s)", num_channels,
			       num_channels == 1 ? _("mono") : _("stereo"));
	label = marlin_make_info_label (str);
	PACK (table, label, 1, 10, GTK_FILL);
	gtk_widget_show (label);

	g_free (str);

/* 	gtk_widget_show (page); */
	return table;
}

GtkWidget *
marlin_information_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *parent;
	struct _InformationDialog *idd;
	GtkWidget *pd, *vbox;
	GtkWidget *notebook, *page;
	char *name, *filename, *title;
	
	sample = marlin_base_window_get_sample (base);
	parent = GTK_WINDOW (base);

	idd = g_new (struct _InformationDialog, 1);
	idd->sample = sample;

	/* Get the details */
	g_object_get (G_OBJECT (sample),
		      "filename", &filename,
		      "name", &name,
		      "tags", &idd->taglist,
		      NULL);

	idd->undo = marlin_base_window_get_undo_manager (base);

	title = g_strdup_printf (_("Sample Information: %s"), name);
	g_free (name);
	
	idd->dialog = pd = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (pd), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (pd), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (pd), parent);
	gtk_window_set_resizable (GTK_WINDOW (pd), FALSE);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (pd)->vbox), vbox);
	gtk_widget_show (vbox);
	
	notebook = gtk_notebook_new ();
	gtk_widget_show (notebook);
	gtk_container_add (GTK_CONTAINER (vbox), notebook);

	page = make_general_page (idd);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
				  page,
				  gtk_label_new (_("General")));
	page = make_audio_page (idd);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
				  page,
				  gtk_label_new (_("Audio")));

	gtk_dialog_add_button (GTK_DIALOG (pd),
			       GTK_STOCK_REVERT_TO_SAVED,
			       GTK_RESPONSE_REJECT);
	gtk_dialog_add_button (GTK_DIALOG (pd),
			       GTK_STOCK_CLOSE,
			       GTK_RESPONSE_CLOSE);
	gtk_dialog_add_button (GTK_DIALOG (pd),
			       _("Set Information"),
			       GTK_RESPONSE_OK);

	g_signal_connect (pd, "response",
			  G_CALLBACK (properties_response), idd);

	return pd;
}
