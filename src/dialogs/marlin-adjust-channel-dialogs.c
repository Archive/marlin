/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2004 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-utils.h>

#include "marlin-adjust-channel-dialogs.h"

/* Currently the two interfaces for remove and add are identical
   but in the future they could be very different, which is
   why they're seperate now */

struct _remove_channel_data {
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;

	GtkWidget *dialog;
	GtkWidget *left, *right;
	GtkWidget *mix;
};

static void
remove_channels_response (GtkDialog *dialog,
			  guint response_id,
			  struct _remove_channel_data *rcd)
{
	MarlinChannelPosition channel;
	MarlinUndoContext *ctxt;

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-remove-channels-dialog");
		return;

	case GTK_RESPONSE_OK:
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (rcd->left))) {
			channel = MARLIN_CHANNEL_LEFT;
		} else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (rcd->right))) {
			channel = MARLIN_CHANNEL_RIGHT;
		} else {
			g_assert_not_reached ();
		}

		ctxt = marlin_undo_manager_context_begin (rcd->undo_manager,
							  _("Remove Channel"));
		marlin_sample_remove_channel (rcd->sample,
					      channel, 
					      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (rcd->mix)),
					      NULL, ctxt, NULL);
		marlin_undo_manager_context_end (rcd->undo_manager, ctxt);

		break;

	case GTK_RESPONSE_CANCEL:
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (rcd);
}

GtkWidget *
marlin_remove_channel_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	GtkWidget *vbox, *label, *hbox, *inner_vbox, *inner_vbox2, *spacer;
	struct _remove_channel_data *rcd;
	char *name, *title;
	guint64 num_frames;

	sample = marlin_base_window_get_sample (base);
	window = GTK_WINDOW (base);

	rcd = g_new (struct _remove_channel_data, 1);
	rcd->sample = sample;
	rcd->undo_manager = marlin_base_window_get_undo_manager (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "total_frames", &num_frames,
		      NULL);
	title = g_strdup_printf (_("Remove Channel: %s"), name);
	g_free (name);

	rcd->dialog = gtk_dialog_new ();
	gtk_window_set_transient_for (GTK_WINDOW (rcd->dialog), window);
	gtk_window_set_title (GTK_WINDOW (rcd->dialog), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (rcd->dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (rcd->dialog), FALSE);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (rcd->dialog)->vbox), vbox);
	
	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox,
			    FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Remove Channel:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (inner_vbox), hbox, FALSE, FALSE, 0);

	spacer = marlin_make_spacer ();
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (hbox), spacer, FALSE, FALSE, 0);

	inner_vbox2 = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox2);
	gtk_box_pack_start (GTK_BOX (hbox), inner_vbox2, TRUE, TRUE, 0);

	rcd->left = gtk_radio_button_new_with_mnemonic (NULL, _("_Left Channel"));
	gtk_widget_show (rcd->left);
	gtk_box_pack_start (GTK_BOX (inner_vbox2), rcd->left, FALSE, FALSE, 0);

	rcd->right = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (rcd->left),
								     _("_Right Channel"));
	gtk_widget_show (rcd->right);
	gtk_box_pack_start (GTK_BOX (inner_vbox2), rcd->right, FALSE, FALSE, 0);

	rcd->mix = gtk_check_button_new_with_mnemonic (_("_Mix channels together"));
	gtk_widget_show (rcd->mix);
	gtk_box_pack_start (GTK_BOX (vbox), rcd->mix, FALSE, FALSE, 0);

	if (num_frames == 0) {
		gtk_widget_set_sensitive (rcd->mix, FALSE);
	}

	gtk_dialog_add_button (GTK_DIALOG (rcd->dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (rcd->dialog),
			       _("_Remove Channel"), GTK_RESPONSE_OK);
#if 0
	gtk_dialog_add_button (GTK_DIALOG (rcd->dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
#endif
	gtk_dialog_set_default_response (GTK_DIALOG (rcd->dialog), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (rcd->dialog), "response",
			  G_CALLBACK (remove_channels_response), rcd);

	return rcd->dialog;
}

struct _add_channel_data {
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;

	GtkWidget *dialog;
	GtkWidget *left, *right;
	GtkWidget *clone;
};

static void
add_channels_response (GtkDialog *dialog,
		       guint response_id,
		       struct _add_channel_data *acd)
{
	MarlinChannelPosition channel;
	MarlinUndoContext *ctxt;

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-add-channel-dialog");
		return;

	case GTK_RESPONSE_OK:
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (acd->left))) {
			channel = MARLIN_CHANNEL_LEFT;
		} else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (acd->right))) {
			channel = MARLIN_CHANNEL_RIGHT;
		} else {
			g_assert_not_reached ();
		}

		ctxt = marlin_undo_manager_context_begin (acd->undo_manager, 
							  _("Add Channel"));
		marlin_sample_add_channel (acd->sample, channel,
					   gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (acd->clone)),
					   NULL, ctxt, NULL);
		marlin_undo_manager_context_end (acd->undo_manager, ctxt);
		break;

	case GTK_RESPONSE_CANCEL:
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (acd);
}

GtkWidget *
marlin_add_channel_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	GtkWidget *vbox, *label, *hbox, *inner_vbox, *inner_vbox2, *spacer;
	struct _add_channel_data *acd;
	char *name, *title;
	guint64 num_frames;

	sample = marlin_base_window_get_sample (base);
	window = GTK_WINDOW (base);

	acd = g_new (struct _add_channel_data, 1);
	acd->sample = sample;
	acd->undo_manager = marlin_base_window_get_undo_manager (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "total-frames", &num_frames,
		      NULL);
	title = g_strdup_printf (_("Add New Channel: %s"), name);
	g_free (name);

	acd->dialog = gtk_dialog_new ();
	gtk_window_set_transient_for (GTK_WINDOW (acd->dialog), window);
	gtk_window_set_title (GTK_WINDOW (acd->dialog), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (acd->dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (acd->dialog), FALSE);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (acd->dialog)->vbox), vbox);
	
	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox,
			    FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Add New Channel As:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (inner_vbox), hbox, FALSE, FALSE, 0);

	spacer = marlin_make_spacer ();
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (hbox), spacer, FALSE, FALSE, 0);

	inner_vbox2 = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox2);
	gtk_box_pack_start (GTK_BOX (hbox), inner_vbox2, TRUE, TRUE, 0);

	acd->left = gtk_radio_button_new_with_mnemonic (NULL, _("_Left Channel"));
	gtk_widget_show (acd->left);
	gtk_box_pack_start (GTK_BOX (inner_vbox2), acd->left, FALSE, FALSE, 0);

	acd->right = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (acd->left),
								     _("_Right Channel"));
	gtk_widget_show (acd->right);
	gtk_box_pack_start (GTK_BOX (inner_vbox2), acd->right, FALSE, FALSE, 0);

	acd->clone = gtk_check_button_new_with_mnemonic (_("_Clone data from original channel"));
	gtk_widget_show (acd->clone);
	gtk_box_pack_start (GTK_BOX (vbox), acd->clone, FALSE, FALSE, 0);

	/* Can't clone channels if there's no data */
	if (num_frames == 0) {
		gtk_widget_set_sensitive (acd->clone, FALSE);
	}

	gtk_dialog_add_button (GTK_DIALOG (acd->dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (acd->dialog),
			       _("_Add Channel"), GTK_RESPONSE_OK);
#if 0
	gtk_dialog_add_button (GTK_DIALOG (acd->dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
#endif
	gtk_dialog_set_default_response (GTK_DIALOG (acd->dialog), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (acd->dialog), "response",
			  G_CALLBACK (add_channels_response), acd);

	return acd->dialog;
}
