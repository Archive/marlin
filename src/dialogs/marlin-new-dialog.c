/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-utils.h>

#include "marlin-new-dialog.h"

struct _NewDialog {
	GtkWidget *dialog;
	GtkWidget *mono;
	GtkWidget *stereo;
	GtkWidget *rate;
	GList *list;
};

static const char *default_rates[] = {
	"2000",
	"4000",
	"8000",
	"11025",
	"16000",
	"22050",
	"32000",
	"44100",
	"48000",
	NULL
};

static void
fill_combo_box_with_defaults (struct _NewDialog *nd)
{
	GList *rates = NULL;
	char *def_str;
	int i, def_rate;

	for (i = 0; default_rates[i]; i++) {
		rates = g_list_append (rates, g_strdup (default_rates[i]));
	}

	nd->list = rates;
	gtk_combo_set_popdown_strings (GTK_COMBO (nd->rate), rates);

	def_rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
	def_str = g_strdup_printf ("%d", def_rate);
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (nd->rate)->entry), def_str);
	g_free (def_str);
}

gboolean
marlin_new_sample_dialog_run (MarlinBaseWindow *base,
			      const char       *title,
			      const char       *affirmative_text,
			      int              *rate,
			      int              *channels)
{
	struct _NewDialog *nd;
	GtkWidget *hbox, *image, *vbox, *inner_hb, *inner_vb, *label;
	int chans;
	GList *p;
	const char *rate_str;
	gboolean mono, retval;

	nd = g_new (struct _NewDialog, 1);
	nd->dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (nd->dialog), title);
	gtk_window_set_transient_for (GTK_WINDOW (nd->dialog),
				      GTK_WINDOW (base));
	gtk_dialog_set_has_separator (GTK_DIALOG (nd->dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (nd->dialog), FALSE);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), MARLIN_DIALOG_BORDER_WIDTH);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (nd->dialog)->vbox), hbox, TRUE, TRUE, 0);

#if 0
	icon_filename = gnome_vfs_mime_get_icon ("application/x-ogg");

	g_print ("Using icon: %s\n", icon_filename);
	image = gtk_image_new_from_file (icon_filename);
#endif
	image = gtk_image_new_from_stock (MARLIN_STOCK_NEW_SAMPLE, GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);

	/* Sample rate */
	inner_hb = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_hb, TRUE, TRUE, 0);

	label = marlin_make_title_label (_("Sample rate:"));
	gtk_box_pack_start (GTK_BOX (inner_hb), label, FALSE, FALSE, 0);

	nd->rate = gtk_combo_new ();
	fill_combo_box_with_defaults (nd);
	gtk_box_pack_start (GTK_BOX (inner_hb), nd->rate, TRUE, TRUE, 0);

	label = gtk_label_new (_("hz"));
	gtk_box_pack_start (GTK_BOX (inner_hb), label, FALSE, FALSE, 0);

	marlin_add_paired_relations (nd->rate, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	/* Channels */
	inner_vb = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vb, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Channels:"));
	gtk_box_pack_start (GTK_BOX (inner_vb), label, FALSE, FALSE, 0);

	inner_hb = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, TRUE, TRUE, 0);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), label, FALSE, FALSE, 0);

	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), inner_vb, TRUE, TRUE, 0);

	nd->mono = gtk_radio_button_new_with_mnemonic (NULL, _("_Mono"));
	gtk_box_pack_start (GTK_BOX (inner_vb), nd->mono, FALSE, FALSE, 0);

	nd->stereo = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (nd->mono),
								     _("_Stereo"));
	gtk_box_pack_start (GTK_BOX (inner_vb), nd->stereo, FALSE, FALSE, 0);

	marlin_add_paired_relations (nd->mono, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);
	marlin_add_paired_relations (nd->stereo, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	chans = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");
	if (chans == 1) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (nd->mono), TRUE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (nd->stereo), TRUE);
	}

	gtk_dialog_add_button (GTK_DIALOG (nd->dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (nd->dialog),
			       affirmative_text, GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (nd->dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (nd->dialog),
					 GTK_RESPONSE_OK);

	gtk_widget_show_all (nd->dialog);

 run:
	switch (gtk_dialog_run (GTK_DIALOG (nd->dialog))) {
	case GTK_RESPONSE_OK:
		rate_str = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (nd->rate)->entry));
		if (rate_str == NULL) {
			*rate = MARLIN_DEFAULT_RATE;
		} else {
			*rate = atoi (rate_str);
		}

		mono = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (nd->mono));
		*channels = mono ? 1 : 2;

		marlin_gconf_set_int ("/apps/marlin/system-state/new-sample-rate", *rate);
		marlin_gconf_set_int ("/apps/marlin/system-state/new-sample-channels", *channels);

		retval = TRUE;
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-new-sample-dialog");
		goto run;
		break;

	default:
		retval = FALSE;
	}

	for (p = nd->list; p; p = p->next) {
		g_free (p->data);
	}
	g_list_free (nd->list);

	gtk_widget_destroy (nd->dialog);
	g_free (nd);

	return retval;
}
