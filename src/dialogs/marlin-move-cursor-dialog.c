/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-types.h>

#include <dialogs/marlin-move-cursor-dialog.h>

struct _move_dialog_data {
	MarlinSampleView *view;
	MarlinSample *sample;
	MarlinSampleSelection *selection;
	MarlinPositionSpinner *position;
	GtkWidget *combo;
	GtkWidget *marker_combo;
	GtkWidget *marker_hbox, *position_hbox;

	MarlinMarkerModel *marker_model;
	GList *markers;
	guint64 cursor_position;
	guint fpp;
};

enum {
	MOVE_START,
	MOVE_END,
	MOVE_SELECT_START,
	MOVE_SELECT_END,
	MOVE_PREV_ZERO,
	MOVE_NEXT_ZERO,
	MOVE_CUSTOM,
	MOVE_MARKER,
	MOVE_0
};

static void
make_move_combo_menu (GtkComboBox *combo,
		      struct _move_dialog_data *mdd)
{
	int i, history;
	static char *positions[] = {
		N_("Start of Sample"),
		N_("End of Sample"),
		N_("Start of Selection"),
		N_("End of Selection"),
		N_("Previous Zero Crossing"),
		N_("Next Zero Crossing"),
		N_("Custom Position"),
		N_("Marker"),
		NULL
	};

	for (i = 0; positions[i] != NULL; i++) {
		if (i != MOVE_MARKER || mdd->markers != NULL) {
			gtk_combo_box_append_text (combo, _(positions[i]));
		}
	}

	history = marlin_gconf_get_int ("/apps/marlin/system-state/move-cursor-history");
	gtk_combo_box_set_active (combo, history);
	gtk_widget_set_sensitive (mdd->marker_hbox, (history == MOVE_MARKER));
	if (history == MOVE_MARKER) {
		gtk_widget_hide (mdd->position_hbox);
		gtk_widget_show (mdd->marker_hbox);
	} else {
		gtk_widget_hide (mdd->marker_hbox);
		gtk_widget_show (mdd->position_hbox);
	}
}

static void
move_cursor_response_cb (GtkDialog *dialog,
			 guint response_id,
			 struct _move_dialog_data *mdd)
{
	GtkAdjustment *adj;
	MarlinDisplay display;
	guint64 position;
	int marker_no;
	MarlinMarker *marker;

	switch (response_id) {
	case GTK_RESPONSE_OK:

		switch (gtk_combo_box_get_active (GTK_COMBO_BOX (mdd->combo))) {
		case MOVE_START:
			position = (guint64) 0;
			break;

		case MOVE_END:
			position = mdd->cursor_position - mdd->fpp;
			break;

		case MOVE_SELECT_START:
			marlin_sample_selection_get (mdd->selection,
						     NULL, &position,
						     NULL);
			break;

		case MOVE_SELECT_END:
			marlin_sample_selection_get (mdd->selection,
						     NULL, NULL,
						     &position);

			break;

		case MOVE_PREV_ZERO:
			position = marlin_sample_previous_zero (mdd->sample,
								mdd->cursor_position,
								MARLIN_COVERAGE_BOTH);
			break;

		case MOVE_NEXT_ZERO:
			position = marlin_sample_next_zero (mdd->sample,
							    mdd->cursor_position,
							    MARLIN_COVERAGE_BOTH);
			break;

		case MOVE_CUSTOM:
			g_object_get (G_OBJECT (mdd->position),
				      "display_as", &display,
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/move-cursor-dialog-display", display);
			adj = GTK_SPIN_BUTTON (mdd->position)->adjustment;
			position = (guint64) adj->value;
			break;

		case MOVE_MARKER:
			marker_no = gtk_combo_box_get_active (GTK_COMBO_BOX (mdd->marker_combo));
			marker = g_list_nth_data (mdd->markers, marker_no);
			g_assert (marker != NULL);
			position = marker->position;
			break;

		default:
			g_assert_not_reached ();
		}

		g_object_set (G_OBJECT (mdd->view),
			      "cursor_position", position,
			      NULL);
		break;

	case GTK_RESPONSE_CANCEL:
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-move-cursor-dialog");
		return;

	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_object_unref (G_OBJECT (mdd->marker_model));
	g_object_unref (G_OBJECT (mdd->selection));
	g_free (mdd);
}

static void
move_combo_changed (GtkComboBox *combo,
		    struct _move_dialog_data *mdd)
{
	int active = gtk_combo_box_get_active (combo);

	gtk_widget_set_sensitive (mdd->marker_hbox, (active == MOVE_MARKER));
	gtk_widget_set_sensitive (mdd->position_hbox, (active == MOVE_CUSTOM));
	if (active == MOVE_CUSTOM) {
		gtk_widget_show (mdd->position_hbox);
		gtk_widget_hide (mdd->marker_hbox);
	} else if (active == MOVE_MARKER) {
		gtk_widget_show (mdd->marker_hbox);
		gtk_widget_hide (mdd->position_hbox);
	}
}

GtkWidget *
marlin_move_cursor_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	MarlinSampleView *view;
	guint64 position;
	struct _move_dialog_data *mdd;
	GtkWidget *editor;
	GtkWidget *vbox, *table, *label;
	guint64 num_frames;
	MarlinDisplay display;
	char *name, *title;
	guint rate;

	sample = marlin_base_window_get_sample (base);
	view = marlin_base_window_get_sample_view (base);
	position = marlin_base_window_get_position (base);
	window = GTK_WINDOW (base);

	mdd = g_new (struct _move_dialog_data, 1);
	mdd->view = view;
	mdd->sample = sample;
	mdd->cursor_position = position;
	g_object_get (G_OBJECT (view),
		      "frames_per_pixel", &mdd->fpp,
		      NULL);

	g_object_get (G_OBJECT (sample),
		      "total_frames", &num_frames,
		      "sample_rate", &rate,
		      "name", &name,
		      "markers", &mdd->marker_model,
		      "selection", &mdd->selection,
		      NULL);

	g_object_get (G_OBJECT (mdd->marker_model),
		      "markers", &mdd->markers,
		      NULL);

	editor = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (editor), FALSE);
/* 	gtk_window_set_default_size (GTK_WINDOW (editor), 325, 95); */
	gtk_window_set_resizable (GTK_WINDOW (editor), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (editor), window);
	title = g_strdup_printf (_("Move Cursor: %s"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (editor), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (editor)->vbox), vbox);

	table = gtk_table_new (2, 3, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic ("Move cursor _to:");
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	mdd->combo = gtk_combo_box_new_text ();
	/* FIXME: the combo is controller for other things too */
	marlin_add_paired_relations (GTK_WIDGET (mdd->combo),
				     ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	g_signal_connect (G_OBJECT (mdd->combo), "changed",
			  G_CALLBACK (move_combo_changed), mdd);

	gtk_widget_show (mdd->combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), mdd->combo);

	gtk_table_attach (GTK_TABLE (table), mdd->combo,
			  1, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	mdd->position_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (mdd->position_hbox);
	gtk_table_attach (GTK_TABLE (table), mdd->position_hbox,
			  1, 3, 1, 2,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	label = gtk_label_new_with_mnemonic (_("_Position:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (mdd->position_hbox), label, FALSE, FALSE, 0);

	mdd->position = (MarlinPositionSpinner *) marlin_position_spinner_new ();
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (mdd->position), (double) position);

	marlin_add_paired_relations (GTK_WIDGET (mdd->position), ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	display = marlin_gconf_get_int ("/apps/marlin/system-state/move-cursor-dialog-display");
	g_object_set (G_OBJECT (mdd->position),
		      "max_frames", num_frames,
		      "display_as", (MarlinDisplay) display,
		      "rate", rate,
		      NULL);

	gtk_entry_set_activates_default (GTK_ENTRY (mdd->position), TRUE);
	gtk_widget_show (GTK_WIDGET (mdd->position));
	gtk_box_pack_start (GTK_BOX (mdd->position_hbox), GTK_WIDGET (mdd->position), TRUE, TRUE, 0);

	label = marlin_position_spinner_label (mdd->position);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (mdd->position_hbox), label, FALSE, FALSE, 0);

	mdd->marker_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (mdd->marker_hbox);
	gtk_table_attach (GTK_TABLE (table), mdd->marker_hbox,
			  1, 3, 2, 3,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	label = gtk_label_new_with_mnemonic (_("Mar_ker:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (mdd->marker_hbox), label, FALSE, FALSE, 0);

	mdd->marker_combo = gtk_combo_box_new_text ();
	make_marker_combo_menu (GTK_COMBO_BOX (mdd->marker_combo), mdd->markers);

	gtk_widget_show (mdd->marker_combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), mdd->marker_combo);
	gtk_box_pack_start (GTK_BOX (mdd->marker_hbox), mdd->marker_combo,
			    TRUE, TRUE, 0);

	if (mdd->markers == NULL) {
		gtk_widget_hide (mdd->marker_hbox);
	}

	/* Do this here so that the marker_hbox will exist */
	make_move_combo_menu (GTK_COMBO_BOX (mdd->combo), mdd);

	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       _("_Move cursor"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (editor), GTK_RESPONSE_OK);

	g_signal_connect (editor, "response",
			  G_CALLBACK (move_cursor_response_cb), mdd);

	return editor;
}
