/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-x-utils.h>

#include <dialogs/marlin-select-region-dialog.h>

struct _select_region_data {
	GtkWidget *window;
	MarlinSample *sample;
	MarlinSampleSelection *selection;
	MarlinUndoManager *undo_manager;
	GtkWidget *start, *finish;
	GtkWidget *start_combo, *finish_combo;
	GtkWidget *sp_hbox, *sm_hbox, *fp_hbox, *fm_hbox;
	GtkWidget *sm_combo, *fm_combo;
	GtkWidget *snap, *coverage;

	GList *markers;

	guint fpp;
	guint64 position;
};

enum {
	SELECT_START_START,
	SELECT_START_CURSOR,
	SELECT_START_CUSTOM,
	SELECT_START_MARKER,
	SELECT_START_0
};

enum {
	SELECT_END_END,
	SELECT_END_CURSOR,
	SELECT_END_CUSTOM,
	SELECT_END_MARKER,
	SELECT_END_0
};

static void
dialog_response_cb (GtkDialog *dialog,
		    guint response_id,
		    struct _select_region_data *srd)
{
	int start_active, end_active;
	guint64 start, finish;
	MarlinDisplay display;
	MarlinUndoContext *ctxt;
	MarlinMarker *marker;
	MarlinCoverage coverage;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		start_active = gtk_combo_box_get_active (GTK_COMBO_BOX (srd->start_combo));
		end_active = gtk_combo_box_get_active (GTK_COMBO_BOX (srd->finish_combo));

		switch (start_active) {
		case SELECT_START_START:
			start = (guint64) 0;
			break;

		case SELECT_START_CURSOR:
			start = srd->position;
			break;

		case SELECT_START_CUSTOM:
			start = (guint64) GTK_SPIN_BUTTON (srd->start)->adjustment->value;
			g_object_get (G_OBJECT (srd->start),
				      "display_as", &display,
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/select-range-start-display", display);

			break;

		case SELECT_START_MARKER:
			marker = g_list_nth_data (srd->markers, gtk_combo_box_get_active (GTK_COMBO_BOX (srd->sm_combo)));
			g_assert (marker != NULL);

			start = marker->position;
			break;

		default:
			g_assert_not_reached ();
		}

		switch (end_active) {
		case SELECT_END_END:
			g_object_get (G_OBJECT (srd->sample),
				      "total_frames", &finish,
				      NULL);

			finish -= srd->fpp;
			break;

		case SELECT_END_CURSOR:
			finish = srd->position;
			break;

		case SELECT_END_CUSTOM:
			finish = GTK_SPIN_BUTTON (srd->finish)->adjustment->value;

			g_object_get (G_OBJECT (srd->finish),
				      "display_as", &display,
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/select-range-finish-display", display);
			break;

		case SELECT_END_MARKER:
			marker = g_list_nth_data (srd->markers, gtk_combo_box_get_active (GTK_COMBO_BOX (srd->fm_combo)));
			g_assert (marker != NULL);

			finish = marker->position;
			break;

		default:
			g_assert_not_reached ();
		}

		marlin_sample_selection_clear (srd->selection, NULL);

		ctxt = marlin_undo_manager_context_begin (srd->undo_manager,
							  _("Select Region"));

		coverage = gtk_combo_box_get_active (GTK_COMBO_BOX (srd->coverage));
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (srd->snap))) {
			start = marlin_sample_previous_zero (srd->sample, start,
							     coverage);
			finish = marlin_sample_next_zero (srd->sample, finish,
							  coverage);
		}

		marlin_sample_selection_set (srd->selection,
					     coverage,
					     start, finish, ctxt);
		marlin_undo_manager_context_end (srd->undo_manager, ctxt);

		break;

	case GTK_RESPONSE_CANCEL:
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-window-select-region-dialog");
		return;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_object_unref (G_OBJECT (srd->selection));

	g_free (srd);
}

#define PACK(t, w, l, top, x) gtk_table_attach (GTK_TABLE ((t)), (w), \
					     (l), (l) + 1, \
					     (top), top + 1, \
					     (x), GTK_FILL, 0, 0)

static void
select_start_changed (GtkComboBox *combo,
		      struct _select_region_data *srd)
{
	int active = gtk_combo_box_get_active (combo);

	gtk_widget_set_sensitive (srd->sm_hbox, (active == SELECT_START_MARKER));
	gtk_widget_set_sensitive (srd->sp_hbox, (active == SELECT_START_CUSTOM));
	if (active == SELECT_START_CUSTOM) {
		gtk_widget_show (srd->sp_hbox);
		gtk_widget_hide (srd->sm_hbox);
	} else if (active == SELECT_START_MARKER) {
		gtk_widget_show (srd->sm_hbox);
		gtk_widget_hide (srd->sp_hbox);
	}
}

static void
select_finish_changed (GtkComboBox *combo,
		       struct _select_region_data *srd)
{
	int active = gtk_combo_box_get_active (combo);

	gtk_widget_set_sensitive (srd->fm_hbox, (active == SELECT_END_MARKER));
	gtk_widget_set_sensitive (srd->fp_hbox, (active == SELECT_END_CUSTOM));
	if (active == SELECT_END_CUSTOM) {
		gtk_widget_show (srd->fp_hbox);
		gtk_widget_hide (srd->fm_hbox);
	} else if (active == SELECT_END_MARKER) {
		gtk_widget_show (srd->fm_hbox);
		gtk_widget_hide (srd->fp_hbox);
	}
}

static void
make_coverage_combo_menu (GtkComboBox *combo)
{
	int i;
	static char *coverage[] = {
		N_("Both Channels"),
		N_("Left Channel Only"),
		N_("Right Channel Only"),
		NULL
	};

	for (i = 0; coverage[i] != NULL; i++) {
		gtk_combo_box_append_text (combo, _(coverage[i]));
	}
}

static void
make_start_combo_menu (GtkComboBox *combo,
		       struct _select_region_data *srd)
{
	int i;
	static char *positions[] = {
		N_("Start of Sample"),
		N_("Cursor Position"),
		N_("Custom Position"),
		N_("Marker"),
		NULL
	};

	for (i = 0; positions[i] != NULL; i++) {
		if (i != SELECT_START_MARKER || srd->markers != NULL) {
			gtk_combo_box_append_text (combo, _(positions[i]));
		}
	}

	gtk_combo_box_set_active (combo, 0);
	gtk_widget_set_sensitive (srd->sm_hbox, FALSE);
	gtk_widget_set_sensitive (srd->sp_hbox, FALSE);
	gtk_widget_show (srd->sp_hbox);
	gtk_widget_hide (srd->sm_hbox);
}

static void
make_finish_combo_menu (GtkComboBox *combo,
			struct _select_region_data *srd)
{
	int i;
	static char *positions[] = {
		N_("End of Sample"),
		N_("Cursor Position"),
		N_("Custom Position"),
		N_("Marker"),
		NULL
	};

	for (i = 0; positions[i] != NULL; i++) {
	  if (i != SELECT_END_MARKER || srd->markers != NULL) {
			gtk_combo_box_append_text (combo, _(positions[i]));
		}
	}

	gtk_combo_box_set_active (combo, 0);
	gtk_widget_set_sensitive (srd->fm_hbox, FALSE);
	gtk_widget_set_sensitive (srd->fp_hbox, FALSE);
	gtk_widget_show (srd->fp_hbox);
	gtk_widget_hide (srd->fm_hbox);
}

GtkWidget *
marlin_select_region_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	MarlinSampleView *view;
	guint64 position;
	struct _select_region_data *srd;
	GtkWidget *editor;
	GtkWidget *vbox, *table, *hbox;
	GtkWidget *label;
	MarlinDisplay display;
	MarlinCoverage coverage;
	guint64 start, finish;
	MarlinMarkerModel *mm;
	guint64 total;
	guint rate;
	char *title, *name;

	sample = marlin_base_window_get_sample (base);
	position = marlin_base_window_get_position (base);
	view = marlin_base_window_get_sample_view (base);
	window = GTK_WINDOW (base);

	srd = g_new (struct _select_region_data, 1);
	srd->window = GTK_WIDGET (window);
	srd->sample = sample;
	srd->position = position;
	srd->undo_manager = marlin_base_window_get_undo_manager (base);
	g_object_get (G_OBJECT (view),
		      "frames_per_pixel", &srd->fpp,
		      NULL);

	g_object_get (G_OBJECT (sample),
		      "total_frames", &total,
		      "sample_rate", &rate,
		      "selection", &srd->selection,
		      "name", &name,
		      "markers", &mm,
		      NULL);

	marlin_sample_selection_get (srd->selection, &coverage,
				     &start, &finish);

	g_object_get (G_OBJECT (mm),
		      "markers", &srd->markers,
		      NULL);

	g_object_unref (G_OBJECT (mm));

	editor = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (editor), FALSE);
	/* 	gtk_window_set_default_size (GTK_WINDOW (editor), 325, 128); */
	gtk_window_set_resizable (GTK_WINDOW (editor), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (editor), GTK_WINDOW (window));

	title = g_strdup_printf (_("Select a Region: %s"), name);
	g_free (name);
	gtk_window_set_title (GTK_WINDOW (editor), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
 	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (editor)->vbox), vbox);

	table = gtk_table_new (2, 5, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic (_("Selection _Start:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	srd->start_combo = gtk_combo_box_new_text ();
	marlin_add_paired_relations (srd->start_combo, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);
	g_signal_connect (G_OBJECT (srd->start_combo), "changed",
			  G_CALLBACK (select_start_changed), srd);

	gtk_widget_show (srd->start_combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->start_combo);

	gtk_table_attach (GTK_TABLE (table), srd->start_combo,
			  1, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_table_attach (GTK_TABLE (table), hbox,
			  1, 3, 1, 2,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0 ,0);

	srd->sp_hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), srd->sp_hbox, TRUE, TRUE, 0);
	gtk_widget_show (srd->sp_hbox);

	label = gtk_label_new_with_mnemonic (_("_Position:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->sp_hbox), label, FALSE, FALSE, 0);

	display = marlin_gconf_get_int ("/apps/marlin/system-state/select-range-start-display");
	srd->start = marlin_position_spinner_new ();
	g_object_set (G_OBJECT (srd->start),
		      "max_frames", total,
		      "rate", rate,
		      "display_as", display,
		      NULL);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (srd->start), (double) start);

	gtk_entry_set_activates_default (GTK_ENTRY (srd->start), TRUE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->start);
	gtk_widget_show (srd->start);
	gtk_box_pack_start (GTK_BOX (srd->sp_hbox), srd->start, TRUE, TRUE, 0);

	marlin_add_paired_relations (srd->start, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (srd->start));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->sp_hbox), label, FALSE, FALSE, 0);

	srd->sm_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (srd->sm_hbox);
	gtk_box_pack_start (GTK_BOX (hbox), srd->sm_hbox, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic (_("S_tart Marker:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->sm_hbox), label, FALSE, FALSE, 0);

	srd->sm_combo = gtk_combo_box_new_text ();
	make_marker_combo_menu (GTK_COMBO_BOX (srd->sm_combo), srd->markers);
	gtk_widget_show (srd->sm_combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->sm_combo);
	gtk_box_pack_start (GTK_BOX (srd->sm_hbox), srd->sm_combo, TRUE, TRUE, 0);

	if (srd->markers == NULL) {
		gtk_widget_hide (srd->sm_hbox);
	}

	make_start_combo_menu (GTK_COMBO_BOX (srd->start_combo), srd);

	label = gtk_label_new_with_mnemonic (_("Selection _End:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 2, GTK_FILL);

	srd->finish_combo = gtk_combo_box_new_text ();
	marlin_add_paired_relations (srd->finish_combo, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);
	g_signal_connect (G_OBJECT (srd->finish_combo), "changed",
			  G_CALLBACK (select_finish_changed), srd);

	gtk_widget_show (srd->finish_combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->finish_combo);

	gtk_table_attach (GTK_TABLE (table), srd->finish_combo,
			  1, 3, 2, 3,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_table_attach (GTK_TABLE (table), hbox,
			  1, 3, 3, 4,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	srd->fp_hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), srd->fp_hbox, TRUE, TRUE, 0);
	gtk_widget_show (srd->fp_hbox);

	label = gtk_label_new_with_mnemonic (_("P_osition:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->fp_hbox), label, FALSE, FALSE, 0);

	display = marlin_gconf_get_int ("/apps/marlin/system-state/select-range-finish-display");
	srd->finish = marlin_position_spinner_new ();
	g_object_set (G_OBJECT (srd->finish),
		      "max_frames", total,
		      "rate", rate,
		      "display_as", display,
		      NULL);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (srd->finish), (double) finish);

	gtk_entry_set_activates_default (GTK_ENTRY (srd->finish), TRUE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->finish);
	gtk_widget_show (srd->finish);
	gtk_box_pack_start (GTK_BOX (srd->fp_hbox), srd->finish, TRUE, TRUE, 0);

	marlin_add_paired_relations (srd->finish, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (srd->finish));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->fp_hbox), label, FALSE, FALSE, 0);

	srd->fm_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (srd->fm_hbox);
	gtk_box_pack_start (GTK_BOX (hbox), srd->fm_hbox, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic (_("Start Mar_ker:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (srd->fm_hbox), label, FALSE, FALSE, 0);

	srd->fm_combo = gtk_combo_box_new_text ();
	make_marker_combo_menu (GTK_COMBO_BOX (srd->fm_combo), srd->markers);
	gtk_widget_show (srd->fm_combo);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), srd->fm_combo);
	gtk_box_pack_start (GTK_BOX (srd->fm_hbox), srd->fm_combo, TRUE, TRUE, 0);

	if (srd->markers == NULL) {
		gtk_widget_hide (srd->fm_hbox);
	}

	make_finish_combo_menu (GTK_COMBO_BOX (srd->finish_combo), srd);

	label = gtk_label_new_with_mnemonic (_("Co_verage:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 4, GTK_FILL);

	srd->coverage = gtk_combo_box_new_text ();
	make_coverage_combo_menu (GTK_COMBO_BOX (srd->coverage));
	gtk_combo_box_set_active (GTK_COMBO_BOX (srd->coverage), MARLIN_COVERAGE_BOTH);
	gtk_widget_show (srd->coverage);
	gtk_table_attach (GTK_TABLE (table), srd->coverage,
			  1, 3, 4, 5,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	srd->snap = gtk_check_button_new_with_mnemonic (_("Snap Selection To _Zero Crossing"));
	gtk_box_pack_start (GTK_BOX (vbox), srd->snap, FALSE, FALSE, 0);
	gtk_widget_show (srd->snap);

	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       _("Select _Region"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (editor), GTK_RESPONSE_OK);

	g_signal_connect (editor, "response",
			  G_CALLBACK (dialog_response_cb), srd);

	return editor;
}
