/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-x-utils.h>

#include "marlin-edit-marker-dialog.h"

struct _EditDialog {
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;

	GtkWidget *dialog;
	GtkWidget *entry;
	GtkWidget *position;

	MarlinMarkerModel *model;
	MarlinMarker *marker;
};

static void
edit_marker_response (GtkDialog *dialog,
		      guint response_id,
		      struct _EditDialog *ed)
{
	MarlinUndoContext *ctxt;
	const char *name;
	guint64 pos;
	GtkAdjustment *adj;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		ctxt = marlin_undo_manager_context_begin (ed->undo_manager,
							  _("Edit Marker"));

		name = gtk_entry_get_text (GTK_ENTRY (ed->entry));
		g_object_get (G_OBJECT (ed->position),
			      "adjustment", &adj,
			      NULL);

		pos = (guint64) adj->value;

		if (name != NULL) {
			marlin_marker_model_rename_marker (ed->model,
							   ed->marker,
							   name, ctxt);
		}

		marlin_marker_model_move_marker (ed->model,
						 ed->marker,
						 pos, ctxt);
		marlin_undo_manager_context_end (ed->undo_manager, ctxt);
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-edit-marker-dialog");
		return;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	/* FIXME: We need to unref model here */
	g_free (ed);
}

GtkWidget *
marlin_edit_marker_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	MarlinMarkerView *view;
	GtkWindow *parent;
	char *name, *title;
	GtkWidget *vbox, *table, *label;
	struct _EditDialog *ed;
	guint64 max_frames;
	guint rate;
	GtkAdjustment *adj;

	sample = marlin_base_window_get_sample (base);
	view = marlin_base_window_get_marker_view (base);
	parent = GTK_WINDOW (base);

	ed = g_new (struct _EditDialog, 1);
	ed->sample = sample;
	ed->marker = marlin_marker_view_get_selected_marker (view);
	ed->undo_manager = marlin_base_window_get_undo_manager (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "markers", &ed->model,
		      NULL);

	title = g_strdup_printf ("Edit Marker: %s", name);
	g_free (name);

	ed->dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (ed->dialog), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (ed->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (ed->dialog), parent);
	gtk_window_set_resizable (GTK_WINDOW (ed->dialog), FALSE);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (ed->dialog)->vbox), vbox);

	table = marlin_make_table (2, 3, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = marlin_make_title_label (_("_Name:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	ed->entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (ed->entry), ed->marker->name);
	gtk_entry_set_activates_default (GTK_ENTRY (ed->entry), TRUE);

	gtk_widget_show (ed->entry);
	gtk_table_attach (GTK_TABLE (table), ed->entry,
			  1, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), ed->entry);
	marlin_add_paired_relations (ed->entry, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_make_title_label (_("_Position:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 1, GTK_FILL);

	g_object_get (G_OBJECT (sample),
		      "total_frames", &max_frames,
		      "sample_rate", &rate,
		      NULL);

	ed->position = marlin_position_spinner_new ();
	g_object_set (G_OBJECT (ed->position),
		      "rate", rate,
		      "max_frames", max_frames,
		      NULL);
	g_object_get (G_OBJECT (ed->position),
		      "adjustment", &adj,
		      NULL);

	adj->value = (double) ed->marker->position;

	gtk_widget_show (ed->position);
	PACK (table, ed->position, 1, 1, GTK_FILL | GTK_EXPAND);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), ed->position);
	marlin_add_paired_relations (ed->position, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (ed->position));
	gtk_widget_show (label);
	PACK (table, label, 2, 1, GTK_FILL);

	gtk_dialog_add_button (GTK_DIALOG (ed->dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (ed->dialog),
			       _("Change Details"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (ed->dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (ed->dialog),
					 GTK_RESPONSE_OK);

	g_signal_connect (ed->dialog, "response",
			  G_CALLBACK (edit_marker_response), ed);

	return ed->dialog;
}
