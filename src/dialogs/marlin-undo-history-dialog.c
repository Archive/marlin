/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-x-utils.h>

#include <dialogs/marlin-undo-history-dialog.h>

struct _undo_history_data {
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;

	GtkWidget *dialog, *treeview;
	GtkWidget *undo_button, *redo_button;
	GtkListStore *history;
	GtkTreeSelection *selection;

	guint32 changed_id;
};

static void
undo_history_response (GtkDialog                 *dialog,
		       struct _undo_history_data *uhd)
{
	if (uhd->changed_id > 0) {
		g_signal_handler_disconnect (uhd->undo_manager,
					     uhd->changed_id);
	}
	g_free (uhd);
}

enum {
	COL_PIXBUF,
	COL_DETAILS,
	COL_ID,
	COL_LAST
};

static void
selection_changed (GtkTreeSelection *selection,
		   struct _undo_history_data *uhd)
{
}

static void
make_history_model (struct _undo_history_data *uhd)
{
	GList *history, *h;
	GdkPixbuf *pb = NULL;
	int i;
	GtkTreePath *current = NULL;

	history = marlin_undo_manager_get_history (uhd->undo_manager);

	for (h = history, i = 0; h; h = h->next, i++) {
		MarlinUndoHistory *hist = h->data;
		GtkTreeIter iter;
		char *txt;

		if (i == 0) {
			pb = gtk_widget_render_icon (uhd->dialog,
						     GTK_STOCK_SAVE,
						     GTK_ICON_SIZE_MENU,
						     "undo-history");
		} else {
			pb = NULL;
		}

		if (hist->info != NULL) {
			txt = g_strdup_printf ("<span weight=\"bold\">%s</span>\n<span size=\"smaller\">   %s</span>",
					       hist->name, hist->info);
		} else {
			txt = g_strdup_printf ("<span weight=\"bold\">%s</span>",
					       hist->name);
		}

		gtk_list_store_append (uhd->history, &iter);
		gtk_list_store_set (uhd->history, &iter,
				    COL_PIXBUF, pb,
				    COL_DETAILS, txt,
				    COL_ID, i,
				    -1);

		if (hist->current) {
			current = gtk_tree_model_get_path (GTK_TREE_MODEL (uhd->history), &iter);
		}

		g_free (txt);
		if (pb != NULL) {
			g_object_unref (G_OBJECT (pb));
		}
	}

	marlin_undo_manager_free_history_list (history);

	if (current) {
		/* Block the signals so that selecting the path does not
		   trigger selection_changed */
		g_signal_handlers_block_matched (uhd->selection,
						 G_SIGNAL_MATCH_FUNC,
						 0, 0, NULL,
						 G_CALLBACK (selection_changed), uhd);
		gtk_tree_selection_select_path (uhd->selection, current);
		g_signal_handlers_unblock_matched (uhd->selection,
						   G_SIGNAL_MATCH_FUNC,
						   0, 0, NULL,
						   G_CALLBACK (selection_changed), uhd);
	}
}

/* When the Undo manager's undo list changes, update the list */
static void
undo_changed (MarlinUndoManager *undo,
	      struct _undo_history_data *uhd)
{
	gtk_widget_set_sensitive (uhd->undo_button,
				  marlin_undo_manager_can_undo (uhd->undo_manager));
	gtk_widget_set_sensitive (uhd->redo_button,
				  marlin_undo_manager_can_redo (uhd->undo_manager));

	/* Block signals so clearing the list does not trigger
	   a selection change */
	g_signal_handlers_block_matched (uhd->selection,
					 G_SIGNAL_MATCH_FUNC,
					 0, 0, NULL,
					 G_CALLBACK (selection_changed), uhd);
	gtk_list_store_clear (GTK_LIST_STORE (uhd->history));
	g_signal_handlers_unblock_matched (uhd->selection,
					   G_SIGNAL_MATCH_FUNC,
					   0, 0, NULL,
					   G_CALLBACK (selection_changed), uhd);
	make_history_model (uhd);
}

static void
undo_clicked (GtkButton *undo,
	      struct _undo_history_data *uhd)
{
	marlin_undo_manager_undo (uhd->undo_manager);
}

static void
redo_clicked (GtkButton *undo,
	      struct _undo_history_data *uhd)
{
	marlin_undo_manager_redo (uhd->undo_manager);
}

GtkWidget *
marlin_undo_history_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	GtkWidget *vbox, *sw, *hbox;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	struct _undo_history_data *uhd;
	char *name, *title;

	sample = marlin_base_window_get_sample (base);
	window = GTK_WINDOW (base);

	uhd = g_new (struct _undo_history_data, 1);
	uhd->sample = sample;
	uhd->undo_manager = marlin_base_window_get_undo_manager (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      NULL);

	/* Listen for the undo list changing */
	uhd->changed_id = g_signal_connect (uhd->undo_manager, "changed",
					    G_CALLBACK (undo_changed), uhd);

	title = g_strdup_printf (_("Undo History: %s"), name);
	g_free (name);

	uhd->dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for (GTK_WINDOW (uhd->dialog), window);
	gtk_window_set_title (GTK_WINDOW (uhd->dialog), title);
	gtk_window_set_default_size (GTK_WINDOW (uhd->dialog), 245, 245);
	gtk_window_set_type_hint (GTK_WINDOW (uhd->dialog),
				  GDK_WINDOW_TYPE_HINT_UTILITY);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
 	gtk_container_set_border_width (GTK_CONTAINER (vbox), 2);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (uhd->dialog), vbox);

	/* Make tree */
	uhd->history = gtk_list_store_new (COL_LAST,
					   GDK_TYPE_PIXBUF,
					   G_TYPE_STRING,
					   G_TYPE_INT);

	uhd->treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL (uhd->history));
	uhd->selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (uhd->treeview));
  	make_history_model (uhd);
	g_signal_connect (uhd->selection, "changed",
			  G_CALLBACK (selection_changed), uhd);

	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (uhd->treeview), FALSE);
 	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (uhd->treeview), FALSE);

	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer,
							   "pixbuf", COL_PIXBUF,
							   NULL);
	gtk_tree_view_insert_column (GTK_TREE_VIEW (uhd->treeview), column, -1);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer, 
							   "markup", COL_DETAILS,
							   NULL);
	gtk_tree_view_insert_column (GTK_TREE_VIEW (uhd->treeview), column, -1);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), uhd->treeview);
	gtk_widget_show (uhd->treeview);
	gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	hbox = gtk_hbox_new (TRUE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	uhd->undo_button = gtk_button_new_from_stock (GTK_STOCK_UNDO);
	gtk_box_pack_start (GTK_BOX (hbox), uhd->undo_button, TRUE, TRUE, 0);
	gtk_widget_show (uhd->undo_button);
	g_signal_connect (uhd->undo_button, "clicked",
			  G_CALLBACK (undo_clicked), uhd);

	uhd->redo_button = gtk_button_new_from_stock (GTK_STOCK_REDO);
	gtk_box_pack_start (GTK_BOX (hbox), uhd->redo_button, TRUE, TRUE, 0);
	gtk_widget_show (uhd->redo_button);
	g_signal_connect (uhd->redo_button, "clicked",
			  G_CALLBACK (redo_clicked), uhd);

	gtk_widget_set_sensitive (uhd->undo_button,
				  marlin_undo_manager_can_undo (uhd->undo_manager));
	gtk_widget_set_sensitive (uhd->redo_button,
				  marlin_undo_manager_can_redo (uhd->undo_manager));

	g_signal_connect (uhd->dialog, "destroy",
			  G_CALLBACK (undo_history_response), uhd);

	return uhd->dialog;
}
