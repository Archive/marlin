/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2003 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <gst/gst.h>

#include <marlin/marlin-save-pipeline.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>

#include "marlin-mp3-dialog.h"

typedef void (*MarlinCallback) (gpointer data);
struct _mp3_data {
	MarlinPipeline *pipeline;
	GstElement *encoder;
	GtkWidget *qual_hbox, *cbr_hbox, *vbr_hbox;
	GtkAdjustment *quality;
	GtkWidget *mode, *cbr;
	GtkWidget *min_vbr, *avg_vbr, *max_vbr;
	GtkToggleButton *tb_qual, *tb_cbr, *tb_vbr;
};

static void
make_cbr_menu (GtkComboBox *combo,
	       struct _mp3_data *md)
{
	/* FIXME: Need better names */
	char *rates_str[] = {
		N_("64kHz (Low Quality)"),
		N_("128kHz (Near CD Quality)"),
		N_("196kHz (CD Quality)"),
		N_("256kHz (High Quality)"),
		N_("320kHz (Super High Quality)"),
		NULL
	};
	int i;

	for (i = 0; rates_str[i]; i++) {
		gtk_combo_box_append_text (combo, _(rates_str[i]));
	}
}

static void
make_mode_menu (GtkComboBox *combo,
		struct _mp3_data *md)
{
	char *modes[] = {
		N_("Stereo"),
		N_("Joint Stereo"),
		N_("Dual Channel"),
		N_("Mono"),
		N_("Automatic"),
		NULL
	};
	int i;

	for (i = 0; modes[i]; i++) {
		gtk_combo_box_append_text (combo, _(modes[i]));
	}
}

static void
dialog_response (GtkDialog *dialog,
		 guint response_id,
		 struct _mp3_data *md)
{
	int rates[] = { 64, 128, 196, 256, 320, -1 };
	int hist;
	
	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-mp3-dialog");
		return;

	case GTK_RESPONSE_OK:
		hist = gtk_combo_box_get_active (GTK_COMBO_BOX (md->mode));
		g_object_set (G_OBJECT (md->encoder),
			      "mode", hist,
			      NULL);
		marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-mode", hist);
		if (gtk_toggle_button_get_active (md->tb_qual)) {
			g_object_set (G_OBJECT (md->encoder),
				      "quality", (int) md->quality->value,
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-type", 0);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-quality", (int) md->quality->value);
		} else if (gtk_toggle_button_get_active (md->tb_cbr)) {
			hist = gtk_combo_box_get_active (GTK_COMBO_BOX (md->cbr));
			g_object_set (G_OBJECT (md->encoder),
				      "bitrate", rates[hist],
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-type", 1);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-cbr", hist);
		} else {
			g_object_set (G_OBJECT (md->encoder),
				      "vbr", TRUE,
				      "vbr-min-bitrate", (int) GTK_SPIN_BUTTON (md->min_vbr)->adjustment->value,
				      "vbr-mean-bitrate", (int) GTK_SPIN_BUTTON (md->avg_vbr)->adjustment->value,
				      "vbr-max-bitrate", (int) GTK_SPIN_BUTTON (md->max_vbr)->adjustment->value,
				      NULL);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-type", 2);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-vbr-min", (int) GTK_SPIN_BUTTON (md->min_vbr)->adjustment->value);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-vbr-avg", (int) GTK_SPIN_BUTTON (md->avg_vbr)->adjustment->value);
			marlin_gconf_set_int ("/apps/marlin/system-state/mp3-dialog-vbr-max", (int) GTK_SPIN_BUTTON (md->max_vbr)->adjustment->value);
		}
		
		g_print ("Pipeline starting...\n");
 		gst_element_set_state (GST_ELEMENT (md->pipeline), 
				       GST_STATE_PLAYING);
		break;

	default:
		gst_element_set_state (GST_ELEMENT (md->pipeline), 
				       GST_STATE_NULL);
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (md);
}

static void
qual_toggled (GtkToggleButton *tb,
	      struct _mp3_data *md)
{
	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}
	
	gtk_widget_set_sensitive (md->qual_hbox, TRUE);
	gtk_widget_set_sensitive (md->cbr_hbox, FALSE);
	gtk_widget_set_sensitive (md->vbr_hbox, FALSE);
}

static void
cbr_toggled (GtkToggleButton *tb,
	     struct _mp3_data *md)
{
	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}
	
	gtk_widget_set_sensitive (md->qual_hbox, FALSE);
	gtk_widget_set_sensitive (md->cbr_hbox, TRUE);
	gtk_widget_set_sensitive (md->vbr_hbox, FALSE);
}

static void
vbr_toggled (GtkToggleButton *tb,
	     struct _mp3_data *md)
{
	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}
	
	gtk_widget_set_sensitive (md->qual_hbox, FALSE);
	gtk_widget_set_sensitive (md->cbr_hbox, FALSE);
	gtk_widget_set_sensitive (md->vbr_hbox, TRUE);
}

void
marlin_mp3_dialog_new (GtkWindow *parent,
		       MarlinPipeline *save_pipeline)
{
	MarlinSample *sample;
	GtkWidget *vbox, *table, *inner_vbox, *inner_hbox;
	struct _mp3_data *md;
	GtkWidget *dialog, *qual, *cbr, *vbr, *label;
	GtkWidget *spacer, *q_slider;
	char *title, *name;
	int def_qual, vbr_def;
	
	md = g_new (struct _mp3_data, 1);
	md->encoder = gst_bin_get_by_name (GST_BIN (save_pipeline), 
					   "encoder-element");
	g_assert (md->encoder != NULL);

	md->pipeline = save_pipeline;
	
	dialog = gtk_dialog_new ();
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	g_object_get (G_OBJECT (parent),
		      "sample", &sample,
		      NULL);
	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      NULL);
	g_object_unref (G_OBJECT (sample));

	title = g_strdup_printf (_("MP3 Encoding Parameters: %s"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (dialog), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, FALSE, FALSE, 0);

	qual = gtk_radio_button_new_with_mnemonic (NULL, _("_Quality"));
	md->tb_qual = GTK_TOGGLE_BUTTON (qual);
	g_signal_connect (G_OBJECT (qual), "toggled",
			  G_CALLBACK (qual_toggled), md);
	gtk_box_pack_start (GTK_BOX (inner_vbox), qual, FALSE, FALSE, 0);
	gtk_widget_show (qual);

	md->qual_hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (md->qual_hbox);
	gtk_box_pack_start (GTK_BOX (inner_vbox), md->qual_hbox, FALSE, FALSE, 0);

	marlin_add_paired_relations (GTK_WIDGET (md->tb_qual), ATK_RELATION_CONTROLLER_FOR,
				     md->qual_hbox, ATK_RELATION_CONTROLLED_BY);
	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (md->qual_hbox), spacer, FALSE, FALSE, 0);
	gtk_widget_show (spacer);

	inner_hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (md->qual_hbox), inner_hbox, TRUE, TRUE, 0);
	gtk_widget_show (inner_hbox);

	label = gtk_label_new (_("Min"));
	gtk_box_pack_start (GTK_BOX (inner_hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	def_qual = marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-quality");
	md->quality = GTK_ADJUSTMENT (gtk_adjustment_new ((double) def_qual, 0.0, 9.0, 1.0, 3.0, 1.0));
	q_slider = gtk_hscale_new (md->quality);
	gtk_scale_set_value_pos (GTK_SCALE (q_slider), GTK_POS_TOP);
	gtk_range_set_inverted (GTK_RANGE (q_slider), TRUE);
	
	gtk_widget_show (q_slider);
	gtk_box_pack_start (GTK_BOX (inner_hbox), q_slider, TRUE, TRUE, 0);

	label = gtk_label_new (_("Max"));
	gtk_box_pack_start (GTK_BOX (inner_hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, FALSE, FALSE, 0);

	cbr = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (qual), _("_Constant Bitrate"));
	md->tb_cbr = GTK_TOGGLE_BUTTON (cbr);
	g_signal_connect (G_OBJECT (cbr), "toggled",
			  G_CALLBACK (cbr_toggled), md);
	gtk_widget_show (cbr);
	gtk_box_pack_start (GTK_BOX (inner_vbox), cbr, FALSE, FALSE, 0);

	md->cbr_hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vbox), md->cbr_hbox, FALSE, FALSE, 0);
	gtk_widget_show (md->cbr_hbox);

	marlin_add_paired_relations (cbr, ATK_RELATION_CONTROLLER_FOR,
				     md->cbr_hbox, ATK_RELATION_CONTROLLED_BY);
	
	spacer = gtk_label_new ("    ");
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (md->cbr_hbox), spacer, FALSE, FALSE, 0);

	md->cbr = gtk_combo_box_new_text ();
	make_cbr_menu (GTK_COMBO_BOX (md->cbr), md);
	gtk_combo_box_set_active (GTK_COMBO_BOX (md->cbr),
				  marlin_gconf_get_int ("/apps/marlin/system-state/cbr"));

	gtk_widget_show (md->cbr);
	gtk_box_pack_start (GTK_BOX (md->cbr_hbox), md->cbr, TRUE, TRUE, 0);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, FALSE, FALSE, 0);
	gtk_widget_show (inner_vbox);

	vbr = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (cbr), _("_Variable Bitrate"));
	md->tb_vbr = GTK_TOGGLE_BUTTON (vbr);
	g_signal_connect (G_OBJECT (vbr), "toggled",
			  G_CALLBACK (vbr_toggled), md);
	gtk_widget_show (vbr);
	gtk_box_pack_start (GTK_BOX (inner_vbox), vbr, FALSE, FALSE, 0);

	md->vbr_hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vbox), md->vbr_hbox, FALSE, FALSE, 0);
	gtk_widget_show (md->vbr_hbox);

	marlin_add_paired_relations (vbr, ATK_RELATION_CONTROLLER_FOR,
				     md->vbr_hbox, ATK_RELATION_CONTROLLED_BY);
	
	spacer = gtk_label_new ("    ");
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (md->vbr_hbox), spacer, FALSE, FALSE, 0);

	table = gtk_table_new (3, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (md->vbr_hbox), table, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic (_("M_inimum Bitrate:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	vbr_def = marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-vbr-min");
	md->min_vbr = gtk_spin_button_new_with_range (1.0, 320.0, 1.0);
	gtk_adjustment_set_value (GTK_SPIN_BUTTON (md->min_vbr)->adjustment, vbr_def);
	gtk_widget_show (md->min_vbr);
	PACK_FULL (table, md->min_vbr, 1, 0, GTK_FILL | GTK_EXPAND, GTK_FILL);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), md->min_vbr);
	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     md->min_vbr, ATK_RELATION_LABELLED_BY);
	
	label = gtk_label_new_with_mnemonic (_("_Average Bitrate:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 1, GTK_FILL);

	vbr_def = marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-vbr-avg");
	md->avg_vbr = gtk_spin_button_new_with_range (1.0, 320.0, 1.0);
	gtk_adjustment_set_value (GTK_SPIN_BUTTON (md->avg_vbr)->adjustment, vbr_def);
	gtk_widget_show (md->avg_vbr);
	PACK_FULL (table, md->avg_vbr, 1, 1, GTK_FILL | GTK_EXPAND, GTK_FILL);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), md->avg_vbr);
	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     md->avg_vbr, ATK_RELATION_LABELLED_BY);
	
	label = gtk_label_new_with_mnemonic (_("Ma_ximum Bitrate:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 2, GTK_FILL);

	vbr_def = marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-vbr-max");
	md->max_vbr = gtk_spin_button_new_with_range (1.0, 320.0, 1.0);
	gtk_adjustment_set_value (GTK_SPIN_BUTTON (md->max_vbr)->adjustment, vbr_def);
	gtk_widget_show (md->max_vbr);
	PACK_FULL (table, md->max_vbr, 1, 2, GTK_FILL | GTK_EXPAND, GTK_FILL);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), md->max_vbr);
	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     md->max_vbr, ATK_RELATION_LABELLED_BY);
	
	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("_Encoding Mode:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);

	inner_hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (inner_hbox);
	gtk_box_pack_start (GTK_BOX (inner_vbox), inner_hbox, FALSE, FALSE, 0);

	spacer = gtk_label_new ("    ");
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (inner_hbox), spacer, FALSE, FALSE, 0);

	md->mode = gtk_combo_box_new_text ();
	make_mode_menu (GTK_COMBO_BOX (md->mode), md);
	gtk_combo_box_set_active (GTK_COMBO_BOX (md->mode),
				  marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-mode"));
	
	gtk_widget_show (md->mode);
	gtk_box_pack_start (GTK_BOX (inner_hbox), md->mode, TRUE, TRUE, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), md->mode);
	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     md->mode, ATK_RELATION_LABELLED_BY);
	
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_SAVE, GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (dialog_response), md);

	switch (marlin_gconf_get_int ("/apps/marlin/system-state/mp3-dialog-type")) {
	case 0:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (qual), TRUE);
		gtk_widget_set_sensitive (md->cbr_hbox, FALSE);
		gtk_widget_set_sensitive (md->vbr_hbox, FALSE);
		break;

	case 1:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbr), TRUE);
		gtk_widget_set_sensitive (md->qual_hbox, FALSE);
		gtk_widget_set_sensitive (md->vbr_hbox, FALSE);
		break;

	case 2:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (vbr), TRUE);
		gtk_widget_set_sensitive (md->cbr_hbox, FALSE);
		gtk_widget_set_sensitive (md->qual_hbox, FALSE);
		break;

	default:
		g_warning ("Bad setting for mp3-dialog-type");
		break;
	}
	
	gtk_widget_show (dialog);
}
