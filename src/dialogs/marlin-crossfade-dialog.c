/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gconf/gconf-client.h>

#include <gtk/gtk.h>

#include <marlin/marlin-cross-fader.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-utils.h>

#include "marlin-crossfade-dialog.h"

struct _xfade_dialog_data {
	GtkWindow *window;
	GtkWidget *xfade;
#if 0
	GtkWidget *length;
	GtkWidget *start, *end, *both;
#endif
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;
};

static void
load_settings (struct _xfade_dialog_data *xfd)
{
/* 	int ms, radio; */
	float src_in, src_out, dest_in, dest_out;

#if 0
	ms = gconf_client_get_int (client,
				   "/apps/marlin/system-state/xfade-length",
				   NULL);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (xfd->length), (double) ms);

	radio = gconf_client_get_int (client,
				      "/apps/marlin/system-state/xfade-location",
				      NULL);
	switch (radio) {
	case 0:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xfd->start),
					      TRUE);
		break;

	case 1:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xfd->end),
					      TRUE);
		break;

	case 2:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xfd->both),
					      TRUE);
		break;

	default:
		break;
	}

#endif
	src_in = marlin_gconf_get_float_with_default
		("/apps/marlin/system-state/xfade-src-in-level", 1.0);
	src_out = marlin_gconf_get_float_with_default
		("/apps/marlin/system-state/xfade-src-out-level", 0.0);
	dest_in = marlin_gconf_get_float_with_default
		("/apps/marlin/system-state/xfade-dest-in-level", 0.0);
	dest_out = marlin_gconf_get_float_with_default
		("/apps/marlin/system-state/xfade-dest-out-level", 1.0);

	marlin_cross_fader_set_levels (MARLIN_CROSS_FADER (xfd->xfade),
				       src_in, src_out,
				       dest_in, dest_out);
}

static void
save_settings (struct _xfade_dialog_data *xfd)
{
	GConfClient *client;
/* 	int ms, radio; */
	float src_in, src_out, dest_in, dest_out;

	client = gconf_client_get_default ();

#if 0
	ms = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (xfd->length));
	gconf_client_set_int (client,
			      "/apps/marlin/system-state/xfade-length", ms,
			      NULL);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (xfd->start))) {
		radio = 0;
	} else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (xfd->end))) {
		radio = 1;
	} else {
		radio = 2;
	}
	gconf_client_set_int (client,
			      "/apps/marlin/system-state/xfade-location", radio,
			      NULL);
#endif

	marlin_cross_fader_get_levels (MARLIN_CROSS_FADER (xfd->xfade),
				       &src_in, &src_out, &dest_in, &dest_out);
	gconf_client_set_float (client,
				"/apps/marlin/system-state/xfade-src-in-level",
				src_in, NULL);
	gconf_client_set_float (client,
				"/apps/marlin/system-state/xfade-src-out-level",
				src_out, NULL);
	gconf_client_set_float (client,
				"/apps/marlin/system-state/xfade-dest-in-level",
				dest_in, NULL);
	gconf_client_set_float (client,
				"/apps/marlin/system-state/xfade-dest-out-level",
				dest_out, NULL);
	g_object_unref (client);
}

static void
xfade_response_cb (GtkDialog *dialog,
		   guint response_id,
		   struct _xfade_dialog_data *xfd)
{
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSampleFade sf, df;
	MarlinSample *src, *dest;
	guint64 length, position;
	MarlinUndoContext *ctxt;
	MarlinRange range;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		src = MARLIN_SAMPLE (marlin_program_get_clipboard (program));
		dest = xfd->sample;

		g_object_get (G_OBJECT (src),
			      "total_frames", &length,
			      NULL);

		position = marlin_base_window_get_position (MARLIN_BASE_WINDOW (xfd->window));

		range.start = 0;
		range.finish = length - 1;

		ctxt = marlin_undo_manager_context_begin (xfd->undo_manager,
							  _("Crossfade"));

		marlin_cross_fader_get_levels (MARLIN_CROSS_FADER (xfd->xfade),
					       &sf.in_level, &sf.out_level,
					       &df.in_level, &df.out_level);

		sf.fade_start = 0;
		sf.fade_end = length - 1;

		df.fade_start = position;
		df.fade_end = position + (length - 1);

		marlin_sample_crossfade (src, dest, &sf, &df, NULL, ctxt, NULL);
		marlin_undo_manager_context_end (xfd->undo_manager, ctxt);

		save_settings (xfd);
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-xfade-dialog");
		return;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (xfd);
}

#if 0
static void
length_changed (GtkSpinButton             *length,
		struct _xfade_dialog_data *xfd)
{
	marlin_cross_fader_set_length (MARLIN_CROSS_FADER (xfd->xfade),
				       gtk_spin_button_get_value_as_int (length));
}
#endif

GtkWidget *
marlin_crossfade_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *window;
	struct _xfade_dialog_data *xfd;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *clipboard;
	GtkWidget *dialog, *vbox; /*, *hbox, *inner_vbox */
/* 	GtkWidget *spacer, *r_vbox, *label; */
	char *name, *title;
	guint64 total_frames;
	guint rate;

	sample = marlin_base_window_get_sample (base);
	window = GTK_WINDOW (base);

	clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));

	xfd = g_new (struct _xfade_dialog_data, 1);
	xfd->window = window;
	xfd->sample = sample;
	xfd->undo_manager = marlin_base_window_get_undo_manager (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "sample_rate", &rate,
		      NULL);
	g_object_get (G_OBJECT (clipboard),
		      "total_frames", &total_frames,
		      NULL);

	dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_default_size (GTK_WINDOW (dialog), 450, 440);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
	title = g_strdup_printf (_("Crossfade Sample: %s"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (dialog), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), vbox);

	xfd->xfade = g_object_new (MARLIN_CROSS_FADER_TYPE,
				   "dest", sample,
				   "src", clipboard,
				   NULL);

	marlin_cross_fader_set_length (MARLIN_CROSS_FADER (xfd->xfade),
				       ((double) total_frames / (double) rate) * 1000);
	gtk_widget_show (xfd->xfade);
	gtk_box_pack_start (GTK_BOX (vbox), xfd->xfade, TRUE, TRUE, 0);

#if 0
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new (_("Crossfade length:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	xfd->length = gtk_spin_button_new_with_range (100, 10000, 100);
	g_signal_connect (G_OBJECT (xfd->length), "value-changed",
			  G_CALLBACK (length_changed), xfd);
	gtk_widget_show (xfd->length);
	gtk_box_pack_start (GTK_BOX (hbox), xfd->length, FALSE, FALSE, 0);

	label = gtk_label_new (_("ms"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Apply Crossfade at:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (inner_vbox), hbox, TRUE, TRUE, 0);

	spacer = marlin_make_spacer ();
	gtk_widget_show (spacer);
	gtk_box_pack_start (GTK_BOX (hbox), spacer, FALSE, FALSE, 0);

	r_vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (r_vbox);
	gtk_box_pack_start (GTK_BOX (hbox), r_vbox, TRUE, TRUE, 0);

	xfd->start = gtk_radio_button_new_with_mnemonic (NULL, _("_Start"));
	gtk_widget_show (xfd->start);
	gtk_box_pack_start (GTK_BOX (r_vbox), xfd->start, FALSE, FALSE, 0);

	xfd->end = gtk_radio_button_new_with_mnemonic_from_widget 
		(GTK_RADIO_BUTTON (xfd->start), _("_Finish"));
	gtk_widget_show (xfd->end);
	gtk_box_pack_start (GTK_BOX (r_vbox), xfd->end, FALSE, FALSE, 0);

	xfd->both = gtk_radio_button_new_with_mnemonic_from_widget
		(GTK_RADIO_BUTTON (xfd->start), _("_Both Start and Finish"));
	gtk_widget_show (xfd->both);
	gtk_box_pack_start (GTK_BOX (r_vbox), xfd->both, FALSE, FALSE, 0);
#endif

	load_settings (xfd);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       _("Crossfade Samples"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	g_signal_connect (dialog, "response",
			  G_CALLBACK (xfade_response_cb), xfd);

	gtk_widget_show (dialog);

	return dialog;
}
