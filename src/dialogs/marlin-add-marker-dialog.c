/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-position-spinner.h>

#include "marlin-add-marker-dialog.h"

struct _AddDialog {
	MarlinSample *sample;
	MarlinUndoManager *undo_manager;

	GtkWidget *dialog;
	GtkWidget *entry;
	GtkWidget *position;

	MarlinMarkerModel *model;
};

static void
add_marker_response (GtkDialog         *dialog,
		     guint              response_id,
		     struct _AddDialog *ad)
{
	MarlinUndoContext *ctxt;
	const char *name;
	guint64 pos;
	GtkAdjustment *adj;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		ctxt = marlin_undo_manager_context_begin (ad->undo_manager,
							  _("Add Marker"));

		name = gtk_entry_get_text (GTK_ENTRY (ad->entry));
		g_object_get (G_OBJECT (ad->position),
			      "adjustment", &adj,
			      NULL);

		pos = (guint64) adj->value;

		marlin_marker_model_add_marker (ad->model, pos, name, ctxt);
		marlin_undo_manager_context_end (ad->undo_manager, ctxt);
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-edit-marker-dialog");
		return;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));

	/* FIXME: Does model need to be unreffed now? */
	g_free (ad);
}

GtkWidget *
marlin_add_marker_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *parent;
	MarlinMarkerModel *model;
	char *name, *title;
	GtkWidget *vbox, *table, *label;
	struct _AddDialog *ad;
	guint64 max_frames;
	guint rate;
	GtkAdjustment *adj;

	sample = marlin_base_window_get_sample (base);
	parent = GTK_WINDOW (base);

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "marker-model", &model,
		      NULL);

	ad = g_new (struct _AddDialog, 1);
	ad->sample = sample;
	ad->undo_manager = marlin_base_window_get_undo_manager (base);
	ad->model = model;

	title = g_strdup_printf ("Add Marker: %s", name);
	g_free (name);

	ad->dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (ad->dialog), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (ad->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (ad->dialog), parent);
	gtk_window_set_resizable (GTK_WINDOW (ad->dialog), FALSE);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (ad->dialog)->vbox), vbox);

	table = marlin_make_table (2, 3, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = marlin_make_title_label (_("_Name:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	ad->entry = gtk_entry_new ();
	gtk_entry_set_activates_default (GTK_ENTRY (ad->entry), TRUE);

	gtk_widget_show (ad->entry);
	gtk_table_attach (GTK_TABLE (table), ad->entry,
			  1, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), ad->entry);
	marlin_add_paired_relations (ad->entry, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_make_title_label (_("_Position:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 1, GTK_FILL);

	g_object_get (G_OBJECT (sample),
		      "total_frames", &max_frames,
		      "sample_rate", &rate,
		      NULL);

	ad->position = marlin_position_spinner_new ();
	g_object_set (G_OBJECT (ad->position),
		      "rate", rate,
		      "max_frames", max_frames,
		      NULL);
	g_object_get (G_OBJECT (ad->position),
		      "adjustment", &adj,
		      NULL);

	adj->value = (double) 0.0;

	gtk_widget_show (ad->position);
	PACK (table, ad->position, 1, 1, GTK_FILL | GTK_EXPAND);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), ad->position);
	marlin_add_paired_relations (ad->position, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	label = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (ad->position));
	gtk_widget_show (label);
	PACK (table, label, 2, 1, GTK_FILL);

	gtk_dialog_add_button (GTK_DIALOG (ad->dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (ad->dialog),
			       _("Add Marker"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (ad->dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (ad->dialog),
					 GTK_RESPONSE_OK);

	g_signal_connect (ad->dialog, "response",
			  G_CALLBACK (add_marker_response), ad);

	return ad->dialog;
}
