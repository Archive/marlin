/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <gst/gst.h>

#include <gconf/gconf-client.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-jack-record.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-x-utils.h>

#include <other/gtkvumeter.h>

#include "main.h"

enum _GRState {
	GR_NOTHING,
	GR_PLAYING,
	GR_RECORDING
};

typedef struct _RecordData {
	GtkWidget *dialog;

	GtkWindow *parent_window;
	MarlinSample *sample;

	MarlinJack *recorder;
	guint jack_owner_id;

	GtkWidget *left_vu, *right_vu;

	GtkWidget *timer, *settings;
	GtkWidget *duration, *status;
	GtkWidget *record, *play, *stop;
	GtkWidget *start, *rewind, *ffwd, *end;
	GtkWidget *profile_menu, *new_profile;
	GtkWidget *new_radio, *exist_radio;
	GtkWidget *exist_box;
	GtkWidget *exist_combo, *position;
	GtkWidget *ins_combo;
	GtkWidget *rate, *channels;

	GList *destroy_ids;

	enum _GRState state;
} RecordData;

struct _destroy_id {
	MarlinWindow *window;
	guint32 id;
};

static void
dialog_response (GtkDialog *dialog,
		 guint response_id,
		 RecordData *rd)
{
	int display;

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-record-dialog");
		return;

	default:
		break;
	}

	/* Store options */
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (rd->new_radio))) {
		marlin_gconf_set_int ("/apps/marlin/system-state/record-destination", 0);
	} else {
		marlin_gconf_set_int ("/apps/marlin/system-state/record-destination", 1);
	}

	marlin_gconf_set_int ("/apps/marlin/system-state/record-position",
			      gtk_combo_box_get_active (GTK_COMBO_BOX (rd->ins_combo)));

	g_object_get (G_OBJECT (rd->position),
		      "display_as", &display,
		      NULL);
	marlin_gconf_set_int ("/apps/marlin/system-state/record-position-display", display);

	if (rd->sample != NULL) {
		g_object_unref (G_OBJECT (rd->sample));
	}

	/* Release jack */
	marlin_program_release_jack (marlin_program_get_default (),
				     rd->jack_owner_id);

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (rd);
}

static void
set_media_controls (RecordData *rd,
		    guint64 frames)
{
	gboolean on;

	/* Set the controls on of off depending on how
	   many frames there are. If there's none, then
	   we can't play the sample. */

	if (frames == 0) {
		on = FALSE;
	} else {
		on = TRUE;
	}

	gtk_widget_set_sensitive (rd->play, on);
	gtk_widget_set_sensitive (rd->start, on);
	gtk_widget_set_sensitive (rd->rewind, on);
	gtk_widget_set_sensitive (rd->ffwd, on);
	gtk_widget_set_sensitive (rd->end, on);
}

static GtkWidget *
make_button (const char *stock_id,
	     GCallback callback,
	     gpointer closure)
{
	GtkWidget *image, *button;

	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), image);

	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (callback), closure);
	return button;
}

#if 0
struct _record_src {
	char *element;
	char *name;
	char *stock_id;
};

/* FIXME: Replace these with nice icons */
static struct _record_src extra_src [] = {
	{ "artsdsrc", N_("aRtsd"), GNOME_STOCK_MIC },
	{ "esdsrc", N_("ESD"), GNOME_STOCK_MIX },
	{ "gnomevfssrc", N_("Remote stream"), GNOME_STOCK_MIC },
	{ "jacksrc", N_("JACK"), GNOME_STOCK_MIC },
	{ NULL, NULL, NULL }
};

static GtkTreeModel *
make_profile_menu (void)
{
	GdkPixbuf *pixbuf;
	GtkWidget *cellview;
	GtkTreeIter iter;
	GtkListStore *store;
	int i;

	cellview = gtk_cell_view_new ();

	store = gtk_list_store_new (2, GDK_TYPE_PIXBUF, G_TYPE_STRING);

	pixbuf = gtk_widget_render_icon (cellview, GNOME_STOCK_MIC,
					 GTK_ICON_SIZE_MENU, NULL);
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
			    0, pixbuf,
			    1, "Mic",
			    -1);

	pixbuf = gtk_widget_render_icon (cellview, GNOME_STOCK_LINE_IN,
					 GTK_ICON_SIZE_MENU, NULL);
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
			    0, pixbuf,
			    1, "Line In",
			    -1);

	for (i = 0; extra_src[i].element; i++) {
		GstElementFactory *factory;

		factory = gst_element_factory_find (extra_src[i].element);
		if (factory == NULL) {
			continue;
		}

		pixbuf = gtk_widget_render_icon (cellview, extra_src[i].stock_id,
						 GTK_ICON_SIZE_MENU, NULL);
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				    0, pixbuf,
				    1, _(extra_src[i].name),
				    -1);
	}

	return GTK_TREE_MODEL (store);
}
#endif

static void
change_rate_and_channels (RecordData *rd,
			  int channels,
			  int rate)
{
	char *str;

	str = g_strdup_printf (_("%d hz"), rate);
	gtk_label_set_text (GTK_LABEL (rd->rate), str);
	g_free (str);

	str = g_strdup_printf ("%d (%s)", channels, channels == 1 ? _("mono") : _("stereo"));
	gtk_label_set_text (GTK_LABEL (rd->channels), str);
	g_free (str);
}

#if 0
static void
item_activated (GtkMenuItem *item,
		RecordData *rd)
{
	MarlinSample *sample;
	int rate, channels;
	guint64 frames;

	sample = g_object_get_data (G_OBJECT (item), "marlin-sample");
	g_object_get (G_OBJECT (sample),
		      "sample-rate", &rate,
		      "channels", &channels,
		      "total-frames", &frames,
		      NULL);

	change_rate_and_channels (rd, channels, rate);

	/* Only set the play if there is a sample */
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (rd->exist_radio))) {
		g_object_set (G_OBJECT (rd->position),
			      "max-frames", frames,
			      "rate", rate,
			      NULL);

		set_media_controls (rd, frames);
		g_object_set (G_OBJECT (rd->play_pipeline),
			      "sample", sample,
			      NULL);
		if (frames > 0) {
			marlin_play_pipeline_seek_range (rd->play_pipeline, (guint64)0, frames);
		}
	}
}

static void
remove_item (MarlinWindow *window,
	     GtkWidget *item)
{
	RecordData *rd = g_object_get_data (G_OBJECT (item), "rd");
	GtkWidget *parent;
	GList *p;

	parent = gtk_widget_get_parent (item);
	gtk_container_remove (GTK_CONTAINER (parent), item);

	/* Find the destroy id */
	for (p = rd->destroy_ids; p; p = p->next) {
		struct _destroy_id *did = p->data;

		if (did->window == window) {
			rd->destroy_ids = g_list_remove_link (rd->destroy_ids, p);
			g_free (did);
		}
	}
}
#endif

static void
make_existing_menu (RecordData *rd)
{
#if 0
	GList *window_list;

	window_list = marlin_get_window_list ();
	for (; window_list; window_list = window_list->next) {
		MarlinWindow *window = window_list->data;
		MarlinSample *sample;
		struct _destroy_id *did;
		char *filename;

#warning FIXME: Make this work.

#if 0
		did = g_new (struct _destroy_id, 1);
		did->window = window;
#endif
		g_object_get (G_OBJECT (window),
			      "sample", &sample,
			      NULL);
		g_object_get (G_OBJECT (sample),
			      "filename", &filename,
			      NULL);

		gtk_combo_box_append_text (GTK_COMBO_BOX (rd->exist_combo),
					   filename);
#if 0
		item = gtk_menu_item_new_with_label (filename);
		g_free (filename);

		g_object_set_data (G_OBJECT (item), "marlin-sample", sample);
		g_object_set_data (G_OBJECT (item), "rd", rd);
		g_signal_connect (G_OBJECT (item), "activate",
				  G_CALLBACK (item_activated), rd);

		/* Connect to the destroyed signal of the window
		   so that when the window is destroyed and the
		   record window is still open, the menuitem 
		   will be removd */
		did->id = g_signal_connect (G_OBJECT (window), "destroy",
					    G_CALLBACK (remove_item), item);

		rd->destroy_ids = g_list_append (rd->destroy_ids, did);

		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU (menu), item);
#endif
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (rd->exist_combo), 0);
#endif
}

static GtkWidget *
make_info_label (const char *text)
{
	GtkWidget *label;

	label = gtk_label_new (text);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (label), GTK_WRAP_WORD);

	return label;
}

static void
pack_table_widget (GtkWidget *table,
		   GtkWidget *widget,
		   int left,
		   int top)
{
	gtk_table_attach (GTK_TABLE (table), widget,
			  left, left + 1, top, top + 1,
			  GTK_FILL, GTK_FILL, 0, 0);
}

/* Callbacks */
static void
new_radio_toggled (GtkToggleButton *tb,
		   RecordData *rd)
{
	int rate, channels;

	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}

	gtk_widget_set_sensitive (rd->exist_box, FALSE);

	rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
	channels = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");

	change_rate_and_channels (rd, channels, rate);
#if 0
	g_object_set (G_OBJECT (rd->play_pipeline),
		      "sample", NULL,
		      NULL);
#endif
	set_media_controls (rd, 0);

}

static void
exist_radio_toggled (GtkToggleButton *tb,
		     RecordData *rd)
{
	MarlinWindow *window;
	MarlinSample *sample;
	int index, channels, rate;
	GList *l;
	guint64 frames;

	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}

	gtk_widget_set_sensitive (rd->exist_box, TRUE);

	index = gtk_combo_box_get_active (GTK_COMBO_BOX (rd->exist_combo));
	g_print ("Using window %d\n", index);

	l = marlin_get_window_list ();
	window = g_list_nth_data (l, index);

	g_object_get (G_OBJECT (window),
		      "sample", &sample,
		      NULL);
	g_object_get (G_OBJECT (sample),
		      "total-frames", &frames,
		      "sample-rate", &rate,
		      "channels", &channels,
		      NULL);

	change_rate_and_channels (rd, channels, rate);
#if 0
	g_object_set (G_OBJECT (rd->play_pipeline),
		      "sample", sample,
		      NULL);
#endif
	g_print ("max_frames: %llu\n", frames);
	g_object_set (G_OBJECT (rd->position),
		      "max_frames", frames,
		      "rate", rate,
		      NULL);
	set_media_controls (rd, frames);

#if 0
	if (frames > 0) {
		marlin_play_pipeline_seek_range (rd->play_pipeline, (guint64)0, frames);
	}
#endif
}

static void
record_clicked (GtkButton *button,
		RecordData *rd)
{
	if (rd->state != GR_NOTHING) {
		return;
	}

	rd->state = GR_RECORDING;
	marlin_jack_start (MARLIN_JACK (rd->recorder), NULL);

	gtk_label_set_text (GTK_LABEL (rd->status), _("Recording"));
	gtk_widget_set_sensitive (rd->record, FALSE);
	gtk_widget_set_sensitive (rd->play, FALSE);
	gtk_widget_set_sensitive (rd->start, FALSE);
	gtk_widget_set_sensitive (rd->rewind, FALSE);
	gtk_widget_set_sensitive (rd->ffwd, FALSE);
	gtk_widget_set_sensitive (rd->end, FALSE);

	gtk_widget_set_sensitive (rd->stop, TRUE);
}

static void
play_clicked (GtkButton *button,
	      RecordData *rd)
{
	if (rd->state != GR_NOTHING) {
		return;
	}
	rd->state = GR_PLAYING;
#if 0
	marlin_pipeline_set_state (MARLIN_PIPELINE (rd->play_pipeline),
				   GST_STATE_PLAYING);
#endif

	gtk_label_set_text (GTK_LABEL (rd->status), _("Playing"));
	gtk_widget_set_sensitive (rd->record, FALSE);
	gtk_widget_set_sensitive (rd->play, FALSE);
	gtk_widget_set_sensitive (rd->start, FALSE);
	gtk_widget_set_sensitive (rd->rewind, FALSE);
	gtk_widget_set_sensitive (rd->ffwd, FALSE);
	gtk_widget_set_sensitive (rd->end, FALSE);

	gtk_widget_set_sensitive (rd->stop, TRUE);
}

static void
recorder_finished_cb (MarlinJackRecord *jack,
		      RecordData       *rd)
{
	int rate, channels;

	/* Make a new window from the sample */
	marlin_open_window_with_sample (rd->sample);

	rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
	channels = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");

	rd->sample = marlin_sample_new ();
	g_object_set (G_OBJECT (rd->sample),
		      "filename", "recorded-sample",
		      "sample-rate", rate,
		      "channels", channels,
		      NULL);

	g_object_set (G_OBJECT (rd->recorder),
		      "sample", rd->sample,
		      NULL);
}

static void
stop_clicked (GtkButton *button,
	      RecordData *rd)
{
	switch (rd->state) {
	case GR_NOTHING:
		break;

	case GR_PLAYING:
		break;

	case GR_RECORDING:
		marlin_jack_stop (MARLIN_JACK (rd->recorder));
		break;
	}
	rd->state = GR_NOTHING;
	gtk_label_set_text (GTK_LABEL (rd->status), _("Ready"));

	gtk_widget_set_sensitive (rd->stop, FALSE);

	gtk_widget_set_sensitive (rd->record, TRUE);
	gtk_widget_set_sensitive (rd->play, TRUE);
	gtk_widget_set_sensitive (rd->start, TRUE);
	gtk_widget_set_sensitive (rd->rewind, TRUE);
	gtk_widget_set_sensitive (rd->ffwd, TRUE);
	gtk_widget_set_sensitive (rd->end, TRUE);
}

#if 0
}

static void
play_eos_reached (MarlinPipeline *pipeline,
		  RecordData *rd)
{
	gtk_label_set_text (GTK_LABEL (rd->status), _("Ready"));

	rd->state = GR_NOTHING;

	gtk_widget_set_sensitive (rd->stop, FALSE);

	gtk_widget_set_sensitive (rd->record, TRUE);
	gtk_widget_set_sensitive (rd->play, TRUE);
	gtk_widget_set_sensitive (rd->start, TRUE);
	gtk_widget_set_sensitive (rd->rewind, TRUE);
	gtk_widget_set_sensitive (rd->ffwd, TRUE);
	gtk_widget_set_sensitive (rd->end, TRUE);

}

static void
play_position_changed (MarlinPlayPipeline *pipeline,
		       guint64 position,
		       RecordData *rd)
{
	gtk_label_set_text (GTK_LABEL (rd->status), _("Ready"));
}
#endif
static void
previous_clicked (GtkButton *button,
		  RecordData *rd)
{
}

static void
rewind_clicked (GtkButton *button,
		RecordData *rd)
{
}

static void
ffwd_clicked (GtkButton *button,
	      RecordData *rd)
{
}

static void
next_clicked (GtkButton *button,
	      RecordData *rd)
{
}

#if 0
static void
record_level (GstElement *level,
	      double time,
	      int channel,
	      double rms,
	      double peak,
	      double decay,
	      RecordData *rd)
{
	GtkWidget *vumeter;

	vumeter = (channel == 0) ? rd->left_vu : rd->right_vu;

	gtk_vumeter_set_levels (GTK_VUMETER (vumeter), rms, peak);
}
#endif

GtkWidget *
marlin_record_new (GtkWindow *parent_window)
{
	RecordData *rd = NULL;
	MarlinProgram *program;
	MarlinSample *window_sample;
	GtkWidget *vbox, *hbox, *main_vb, *inner_vb, *inner_hb, *spacer, *label;
	GtkWidget *iivb, *iihb, *position_hb;
	GtkWidget *info_table;
	GtkWidget *extra_hb;
	int rate, channels, state;
	int display;
	guint64 frames;
	char *str;

	rd = g_new0 (RecordData, 1);

	rd->state = GR_NOTHING;

	rd->sample = marlin_sample_new ();
	rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
	channels = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");

	g_object_set (G_OBJECT (rd->sample),
		      "filename", "recorded-sample",
		      "sample-rate", rate,
		      "channels", channels,
		      NULL);

	program = marlin_program_get_default ();
	rd->recorder = (MarlinJack *) marlin_program_get_recorder (program);
	rd->jack_owner_id = marlin_program_request_jack_owner_id (program);

	/* Acquire jack, and lock it */
	marlin_program_acquire_jack (program, rd->jack_owner_id, TRUE);

	if (rd->recorder) {
		g_signal_connect (rd->recorder, "record-finished",
				  G_CALLBACK (recorder_finished_cb), rd);
		g_object_set (G_OBJECT (rd->recorder),
			      "sample", rd->sample,
			      NULL);
	}

#if 0
	rd->play_pipeline = marlin_play_pipeline_new ();
	g_signal_connect (G_OBJECT (rd->play_pipeline), "eos",
			  G_CALLBACK (play_eos_reached), rd);
	g_signal_connect (G_OBJECT (rd->play_pipeline), "position-changed",
			  G_CALLBACK (play_position_changed), rd);
#endif
	g_object_get (G_OBJECT (parent_window),
		      "sample", &window_sample,
		      NULL);
	g_object_get (G_OBJECT (window_sample),
		      "total-frames", &frames,
		      NULL);
	g_object_unref (G_OBJECT (window_sample));

	rd->parent_window = parent_window;
	rd->dialog = gtk_dialog_new_with_buttons (_("Record audio"),
						  parent_window,
						  GTK_DIALOG_NO_SEPARATOR,
						  GTK_STOCK_HELP,
						  GTK_RESPONSE_HELP,
						  GTK_STOCK_CLOSE,
						  GTK_RESPONSE_CLOSE,
						  NULL);
	gtk_window_set_default_size (GTK_WINDOW (rd->dialog), 450, 390);
	gtk_window_set_resizable (GTK_WINDOW (rd->dialog), FALSE);
	g_signal_connect (G_OBJECT (rd->dialog), "response",
			  G_CALLBACK (dialog_response), rd);

	/* VBox inside */
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), MARLIN_DIALOG_BORDER_WIDTH);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (rd->dialog)->vbox), vbox);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	inner_vb = gtk_vbox_new (FALSE, 6);
	main_vb = inner_vb;
	gtk_box_pack_start (GTK_BOX (hbox), inner_vb, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Recording Controls"));
	gtk_box_pack_start (GTK_BOX (inner_vb), label, FALSE, FALSE, 0);

	/* Media bar */
	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, FALSE, FALSE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	/* To get the layout right we need this extra hbox... Grrr */
	extra_hb = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), extra_hb, TRUE, TRUE, 0);

	iihb = gtk_hbox_new (TRUE, 0);
	gtk_box_pack_start (GTK_BOX (extra_hb), iihb, FALSE, FALSE, 0);

	rd->record = make_button (MARLIN_STOCK_RECORD, G_CALLBACK (record_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->record, FALSE, FALSE, 0);

	rd->play = make_button (MARLIN_STOCK_PLAY, G_CALLBACK (play_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->play, FALSE, FALSE, 0);

	rd->stop = make_button (MARLIN_STOCK_STOP, G_CALLBACK (stop_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->stop, FALSE, FALSE, 0);
	/* Stop is grey by default...*/
	gtk_widget_set_sensitive (rd->stop, FALSE);

	rd->start = make_button (MARLIN_STOCK_PREVIOUS, G_CALLBACK (previous_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->start, FALSE, FALSE, 0);

	rd->rewind = make_button (MARLIN_STOCK_REWIND, G_CALLBACK (rewind_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->rewind, FALSE, FALSE, 0);

	rd->ffwd = make_button (MARLIN_STOCK_FFWD, G_CALLBACK (ffwd_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->ffwd, FALSE, FALSE, 0);

	rd->end = make_button (MARLIN_STOCK_NEXT, G_CALLBACK (next_clicked), rd);
	gtk_box_pack_start (GTK_BOX (iihb), rd->end, FALSE, FALSE, 0);

 	set_media_controls (rd, frames);
#if 0
	rd->profile_menu = gtk_combo_box_new_with_model (make_profile_menu ());
	gtk_box_pack_start (GTK_BOX (extra_hb), rd->profile_menu, TRUE, TRUE, 0);

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (rd->profile_menu),
				    renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (rd->profile_menu),
					renderer,
					"pixbuf", 0,
					NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (rd->profile_menu),
				    renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (rd->profile_menu),
					renderer,
					"text", 1,
					NULL);
	gtk_combo_box_set_active (GTK_COMBO_BOX (rd->profile_menu), 0);

	rd->settings = gtk_button_new_with_mnemonic (_("_Settings"));
	gtk_widget_set_sensitive (rd->settings, FALSE);
	gtk_box_pack_start (GTK_BOX (extra_hb), rd->settings, FALSE, FALSE, 0);
#endif
	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, FALSE, FALSE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	/* Again for layout purposes */
	extra_hb = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), extra_hb, TRUE, TRUE, 0);

	/* Status */
	rd->status = gtk_label_new (_("Ready"));
	gtk_misc_set_alignment (GTK_MISC (rd->status), 0, 0.5);
	gtk_box_pack_start (GTK_BOX (extra_hb), rd->status, FALSE, FALSE, 0);

	/* Duration */
	rd->duration = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (rd->duration), 0, 0.5);
	gtk_box_pack_start (GTK_BOX (extra_hb), rd->duration, FALSE, FALSE, 0);

	rd->timer = gtk_label_new ("0:00:00.000");
	gtk_box_pack_start (GTK_BOX (extra_hb), rd->timer, TRUE, TRUE, 0);

	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (main_vb), inner_vb, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Sample Information"));
	gtk_box_pack_start (GTK_BOX (inner_vb), label, FALSE, FALSE, 0);

	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, FALSE, FALSE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	info_table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (info_table), 12);
	gtk_box_pack_start (GTK_BOX (inner_hb), info_table, TRUE, TRUE, 0);

	label = make_info_label (_("Number of Channels:"));
	pack_table_widget (info_table, label, 0, 0);

	channels = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");
	rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");

	str = g_strdup_printf ("%d (%s)", channels, channels == 1 ? _("mono") : _("stereo"));
	rd->channels = make_info_label (str);
	g_free (str);
	pack_table_widget (info_table, rd->channels, 1, 0);

	label = make_info_label (_("Sample Rate:"));
	pack_table_widget (info_table, label, 0, 1);

	str = g_strdup_printf (_("%d hz"), rate);
	rd->rate = make_info_label (str);
	g_free (str);
	pack_table_widget (info_table, rd->rate, 1, 1);

	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (main_vb), inner_vb, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("Destination"));
	gtk_box_pack_start (GTK_BOX (inner_vb), label, FALSE, FALSE, 0);

	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, TRUE, TRUE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	iivb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), iivb, TRUE, TRUE, 0);

	rd->new_radio = gtk_radio_button_new_with_mnemonic (NULL, _("_Create a new window"));
	g_signal_connect (G_OBJECT (rd->new_radio), "toggled",
			  G_CALLBACK (new_radio_toggled), rd);
	gtk_box_pack_start (GTK_BOX (iivb), rd->new_radio, FALSE, FALSE, 0);

	rd->exist_radio = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (rd->new_radio),
									  _("Use an _existing window"));
	g_signal_connect (G_OBJECT (rd->exist_radio), "toggled",
			  G_CALLBACK (exist_radio_toggled), rd);

	/* Only cos it'll crash if you select this... */
	gtk_widget_set_sensitive (rd->exist_radio, FALSE);

	gtk_box_pack_start (GTK_BOX (iivb), rd->exist_radio, FALSE, FALSE, 0);

	iihb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (iivb), iihb, FALSE, FALSE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (iihb), spacer, FALSE, FALSE, 0);

	rd->exist_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (iihb), rd->exist_box, TRUE, TRUE, 0);

	rd->exist_combo = gtk_combo_box_new_text ();
	gtk_box_pack_start (GTK_BOX (rd->exist_box), rd->exist_combo, FALSE, FALSE, 0);
	make_existing_menu (rd);

	position_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (rd->exist_box), position_hb, TRUE, TRUE, 0);

	rd->ins_combo = gtk_combo_box_new_text ();
 	gtk_box_pack_start (GTK_BOX (position_hb), rd->ins_combo, FALSE, FALSE, 0);
	gtk_widget_show (rd->ins_combo);

	gtk_combo_box_append_text (GTK_COMBO_BOX (rd->ins_combo), _("Insert"));
	gtk_combo_box_append_text (GTK_COMBO_BOX (rd->ins_combo), _("Overwrite"));

	label = gtk_label_new (_("at"));
	gtk_box_pack_start (GTK_BOX (position_hb), label, FALSE, FALSE, 0);

	display = marlin_gconf_get_int ("/apps/marlin/system-state/record-position-display");
	rd->position = marlin_position_spinner_new ();
	g_object_set (G_OBJECT (rd->position),
		      "display_as", display,
		      NULL);

	gtk_box_pack_start (GTK_BOX (position_hb), rd->position, TRUE, TRUE, 0);

	state = marlin_gconf_get_int ("/apps/marlin/system-state/record-destination");
	if (state == 0) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rd->new_radio), TRUE);
		gtk_widget_set_sensitive (rd->exist_box, FALSE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rd->exist_radio), TRUE);
	}

	state = marlin_gconf_get_int ("/apps/marlin/system-state/record-position");
	gtk_combo_box_set_active (GTK_COMBO_BOX (rd->ins_combo), state);

	rd->left_vu = gtk_vumeter_new (GTK_VUMETER_VERTICAL);
	g_object_set (G_OBJECT (rd->left_vu),
		      "scale", MARLIN_SCALE_LOG,
		      NULL);
	rd->right_vu = gtk_vumeter_new (GTK_VUMETER_VERTICAL);
	g_object_set (G_OBJECT (rd->right_vu),
		      "scale", MARLIN_SCALE_LOG,
		      NULL);

	gtk_box_pack_start (GTK_BOX (hbox), rd->left_vu, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), rd->right_vu, FALSE, FALSE, 0);

	gtk_widget_show_all (rd->dialog);
	return rd->dialog;
}
