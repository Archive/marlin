/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MAIN_H__
#define __MARL_H__

#include <ephy/ephy-toolbars-model.h>
#include "marlin-window.h"

typedef void (*MarlinForeachFunction) (MarlinWindow *window,
				       gpointer closure);

void marlin_quit (void);
GtkWidget *marlin_open_window_with_sample (MarlinSample *sample);
GtkWidget *marlin_open_window (const char *filename, 
			       const char *uuid,
			       GdkScreen  *screen,
			       const char *startup_id);
void marlin_foreach_window (MarlinForeachFunction func,
			    gpointer closure);
GList *marlin_get_window_list (void);
EphyToolbarsModel *marlin_get_toolbars_model (void);

#endif


