/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2009 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-cross-fader.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-base-window.h>
#include <marlin/marlin-undo-manager.h>

#include <egg-editable-toolbar.h>
#include <egg-toolbar-editor.h>

#include <ephy/ephy-toolbars-model.h>

#include <other/koto-undo-action.h>

#include "marlin-toolbar.h"
#include "marlin-window.h"
#include "marlin-record.h"
#include "marlin-clipboard-info.h"
#include "marlin-recent-action.h"
#include "dialogs/marlin-new-dialog.h"
#include "main.h"

enum {
	RESPONSE_ADD_TOOLBAR
};

/* Command callback functions */
static void
file_new (GtkAction *action,
	  gpointer data)
{
	marlin_open_window (NULL, NULL, NULL, NULL);
}

static void
file_new_dialog (GtkAction *action,
		 gpointer data)
{
	int rate, channels;

	if (marlin_new_sample_dialog_run (MARLIN_BASE_WINDOW (data),
					  _("Create a new sample"),
					  _("Create Sample"),
					  &rate, &channels)) {
		marlin_open_window (NULL, NULL, NULL, NULL);
	}
}

static void
file_open (GtkAction *action,
	   gpointer data)
{
	marlin_window_open (MARLIN_WINDOW (data));
}

static void
file_open_recent (GtkAction     *action,
		  GtkRecentInfo *info,
		  MarlinWindow  *window)
{
	const char *uri;

	uri = gtk_recent_info_get_uri (info);
	if (marlin_window_is_empty (window)) {
		marlin_window_load_file (window, uri);
	} else {
		marlin_open_window (uri, NULL, NULL, NULL);
	}
}

static void
file_save (GtkAction *action,
	   gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);

	marlin_window_save_sample (window);
}

static void
file_save_as (GtkAction *action,
	      gpointer data)
{
	marlin_window_save_sample_as (MARLIN_WINDOW (data));
}

static void
file_properties (GtkAction *action,
		 gpointer data)
{
	marlin_window_show_properties (MARLIN_WINDOW (data));
}

static void
file_close (GtkAction *action,
	    gpointer data)
{
	marlin_window_close (MARLIN_WINDOW (data));
}

static void G_GNUC_UNUSED
file_quit (GtkAction *action,
	   gpointer data)
{
	marlin_quit ();
}

static void
help_contents (GtkAction *action,
	       gpointer data)
{
	marlin_display_help (NULL);
}

static void
help_about (GtkAction *action,
	    gpointer data)
{
	MarlinWindow *window;
	static GtkWidget *about = NULL;
	const char *authors[2] = {"iain <iain@gnome.org>", NULL};
	const char *documentors[] = {"iain <iain@gnome.org>", NULL};

	if (about != NULL) {
		gtk_window_present (GTK_WINDOW (about));
	} else {
		GdkPixbuf *logo = NULL;
		char *logo_file;

		window = MARLIN_WINDOW (data);

		logo_file = marlin_file ("marlin/marlin-logo.png");
		logo = gdk_pixbuf_new_from_file (logo_file, NULL);
		g_free (logo_file);

		about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
				      "name", "Marlin",
				      "comments", _("A sample editor for Gnome."),
				      "version", VERSION,
				      "copyright", "Copyright \xc2\xa9 2002-2008 Iain Holmes",
				      "authors", authors,
				      "documenters", documentors,
				      /*
				       * Note to translators: put your name
				       * and email here so it will show up
				       * in the about box.
				       */
				      "translator-credits", _("translator-credits"),
				      "logo", logo,
				      "website", "http://marlin.sourceforge.net",
				      NULL);

		gtk_window_set_icon (GTK_WINDOW (about), logo);
		gtk_window_set_transient_for (GTK_WINDOW (about),
					      GTK_WINDOW (window));
		gtk_window_set_destroy_with_parent (GTK_WINDOW (about), TRUE);

		g_object_unref (G_OBJECT (logo));

		g_signal_connect (G_OBJECT (about), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);

		g_object_add_weak_pointer (G_OBJECT (about), (gpointer) &about);

		gtk_window_present (GTK_WINDOW (about));
	}
}

static void
media_previous (GtkAction *action,
		gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_move_cursor_to_start (window);
}

static void
media_next (GtkAction *action,
	    gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_move_cursor_to_end (window);
}

static void
media_forward (GtkAction *action,
	       gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_move_cursor (window, 1);
}

static void
media_rewind (GtkAction *action,
	      gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_move_cursor (window, -1);
}

static void
media_play (GtkAction *action,
	    gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_play_sample (window, FALSE);
}

static void
media_stop (GtkAction *action,
	    gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_stop_sample (window);
}

static void
media_pause (GtkAction *action,
	     gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_pause_sample (window);
}

static void
media_repeat (GtkAction *action,
	      gpointer data)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (data);
	marlin_window_play_sample (window, TRUE);
}

static void
media_record (GtkAction *action,
	      gpointer data)
{
	static GtkWidget *dialog = NULL;

	if (dialog == NULL) {
		dialog = marlin_record_new (GTK_WINDOW (data));
		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);
	} else {
		gdk_window_raise (dialog->window);
		gdk_window_show (dialog->window);
	}
}

static void
edit_history (GtkAction *action,
	      gpointer data)
{
	marlin_window_undo_history (MARLIN_WINDOW (data));
}

static void
edit_copy (GtkAction *action,
	   gpointer data)
{
	marlin_window_selection_to_clipboard (MARLIN_WINDOW (data));
}

static void
edit_cut (GtkAction *action,
	  gpointer data)
{
	MarlinWindow *window = MARLIN_WINDOW (data);
	marlin_window_selection_to_clipboard (window);
 	marlin_window_delete (window);
}

static void
edit_paste (GtkAction *action,
	    gpointer data)
{
	marlin_window_paste (MARLIN_WINDOW (data));
}

static void
edit_replace (GtkAction *action,
	      gpointer data)
{
	marlin_window_replace (MARLIN_WINDOW (data));
}

static void
edit_paste_mix (GtkAction *action,
		gpointer data)
{
	marlin_window_mix (MARLIN_WINDOW (data));
}

static void
edit_xfade (GtkAction *action,
	    gpointer data)
{
	MarlinWindow *window = (MarlinWindow *) data;
	MarlinSample *sample;
	MarlinSampleSelection *selection;
	MarlinCoverage coverage;

	sample = marlin_base_window_get_sample (MARLIN_BASE_WINDOW (data));
	g_object_get (G_OBJECT (sample),
		      "selection", &selection,
		      NULL);

	marlin_sample_selection_get (selection, &coverage, NULL, NULL);
	g_object_unref (selection);

	if (coverage == MARLIN_COVERAGE_NONE) {
		marlin_window_xfade (window);
	} else {
		marlin_window_xfade_selection (window);
	}
}

static void
edit_paste_new (GtkAction *action,
		gpointer data)
{
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *sample, *copy;
	GError *error = NULL;

	sample = MARLIN_SAMPLE (marlin_program_get_clipboard (program));

	/* Copy sample */
	copy = marlin_sample_new_from_sample (sample, &error);
	if (copy == NULL) {
		g_warning ("marlin_sample_new_from_sample failed");
		return;
	}

	/* Dirty the copy */
	g_object_set (G_OBJECT (copy),
		      "dirty", TRUE,
		      NULL);

	marlin_open_window_with_sample (copy);
}

static void
edit_select_all (GtkAction *action,
		 gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Select All"));

	marlin_window_select_all (MARLIN_WINDOW (data), ctxt);

	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
edit_select_none (GtkAction *action,
		  gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Select Nothing"));

	marlin_window_select_none (MARLIN_WINDOW (data), ctxt);

	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
zero_previous (GtkAction *action,
	       gpointer data)
{
	marlin_window_move_previous_zero (MARLIN_WINDOW (data));
}

static void
zero_next (GtkAction *action,
	   gpointer data)
{
	marlin_window_move_next_zero (MARLIN_WINDOW (data));
}

static void
selection_halve (GtkAction *action,
		 gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Halve Selection"));

	marlin_window_selection_scale (MARLIN_WINDOW (data), 0.5, ctxt);
	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
selection_double (GtkAction *action,
		  gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Double Selection"));

	marlin_window_selection_scale (MARLIN_WINDOW (data), 2.0, ctxt);
	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
selection_left (GtkAction *action,
		gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Shift Selection Left"));

	marlin_window_selection_shift_left (MARLIN_WINDOW (data), ctxt);

	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
selection_right (GtkAction *action,
		 gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Shift Selection Right"));

	marlin_window_selection_shift_right (MARLIN_WINDOW (data), ctxt);

	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
selection_snap_to_zero (GtkAction *action,
			gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Snap To Zero Crossing"));
	marlin_window_selection_snap_to_zero (MARLIN_WINDOW (data), ctxt);

	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
edit_delete (GtkAction *action,
	     gpointer data)
{
	marlin_window_delete (MARLIN_WINDOW (data));
}

static void
edit_crop (GtkAction *action,
	   gpointer data)
{
	marlin_window_crop (MARLIN_WINDOW (data));
}

static void
edit_move_cursor (GtkAction *action,
		  gpointer data)
{
	marlin_window_move_cursor_dialog (MARLIN_WINDOW (data));
}

static void
edit_select_region (GtkAction *action,
		    gpointer data)
{
	marlin_window_select_region (MARLIN_WINDOW (data));
}

static void
toolbar_editor_destroy_cb (GtkWidget     *tbe,
			   MarlinToolbar *t)
{
	egg_editable_toolbar_set_edit_mode (EGG_EDITABLE_TOOLBAR (t), FALSE);
}

static void
toolbar_editor_response_cb (GtkDialog *dialog,
			    gint response_id,
			    gpointer data)
{
	EphyToolbarsModel *model;
	int position;

	switch (response_id) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;

	case RESPONSE_ADD_TOOLBAR:
		model = marlin_get_toolbars_model ();
		position = egg_toolbars_model_add_toolbar (EGG_TOOLBARS_MODEL (model),
							   -1, "UserCreated");
		egg_toolbars_model_set_flags (EGG_TOOLBARS_MODEL (model),
					      EGG_TB_MODEL_ICONS_ONLY,
					      position);
		break;

	case GTK_RESPONSE_HELP:
		break;
	}
}

static void
edit_toolbars (GtkAction *action,
	       gpointer data)
{
	GtkWidget *editor, *dialog;
	EphyToolbarsModel *model;
	MarlinToolbar *toolbar;
	GtkUIManager *ui_manager = marlin_window_get_menu_merge (MARLIN_WINDOW (data));;
	char *xml;

	toolbar = marlin_window_get_toolbar (MARLIN_WINDOW (data));
	model = marlin_get_toolbars_model ();

	dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_title (GTK_WINDOW (dialog), _("Toolbar Editor"));
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (data));

	editor = egg_toolbar_editor_new (ui_manager, EGG_TOOLBARS_MODEL (model));

	xml = marlin_file ("toolbars.xml");
	egg_toolbar_editor_load_actions (EGG_TOOLBAR_EDITOR (editor), xml);
	g_free (xml);
	gtk_container_set_border_width (GTK_CONTAINER (editor), 6);
	gtk_box_set_spacing (GTK_BOX (editor), 6);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), editor);
	g_signal_connect (editor, "destroy",
			  G_CALLBACK (toolbar_editor_destroy_cb), toolbar);
	gtk_widget_show (editor);

	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 2);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       _("Add a New Toolbar"),
			       RESPONSE_ADD_TOOLBAR);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (toolbar_editor_response_cb), NULL);
	gtk_widget_show (dialog);

	egg_editable_toolbar_set_edit_mode (EGG_EDITABLE_TOOLBAR (toolbar), TRUE);
}

static void
process_mute (GtkAction *action,
	      gpointer data)
{
	marlin_window_clear (MARLIN_WINDOW (data));
}

static void
process_invert (GtkAction *action,
		gpointer data)
{
	marlin_window_invert (MARLIN_WINDOW (data));
}

static void
process_swap_channels (GtkAction *action,
		       gpointer data)
{
	MarlinBaseWindow *base = MARLIN_BASE_WINDOW (data);
	MarlinSample *sample;
	MarlinUndoManager *undo;
	MarlinUndoContext *ctxt;

	g_object_get (G_OBJECT (data),
		      "sample", &sample,
		      NULL);

	undo = marlin_base_window_get_undo_manager (base);
	ctxt = marlin_undo_manager_context_begin (undo, _("Swap Channels"));

	marlin_sample_swap_channels (sample, NULL, ctxt, NULL);
	marlin_undo_manager_context_end (undo, ctxt);
	g_object_unref (G_OBJECT (sample));
}

static void
process_adjust_volume (GtkAction *action,
		       gpointer   data)
{
	marlin_window_adjust_volume (MARLIN_WINDOW (data));
}

static void
process_normalize (GtkAction    *action,
		   MarlinWindow *window)
{
	marlin_window_normalize (window);
}

static void
process_insert_silence (GtkAction *action,
			gpointer   data)
{
	marlin_window_insert_silence (MARLIN_WINDOW (data));
}

static void
process_change_channels (GtkAction *action,
			 gpointer   data)
{
	marlin_window_change_channels (MARLIN_WINDOW (data));
}

static void
process_split_sample (GtkAction *action,
		      gpointer   data)
{
	marlin_window_split_sample (MARLIN_WINDOW (data));
}

static void
process_reverse_sample (GtkAction    *action,
			MarlinWindow *window)
{
	marlin_window_reverse_sample (window);
}

static void
zoom_in (GtkAction *action,
	 gpointer data)
{
	marlin_window_zoom_in (MARLIN_WINDOW (data));
}

static void
zoom_out (GtkAction *action,
	  gpointer data)
{
	marlin_window_zoom_out (MARLIN_WINDOW (data));
}

static void
zoom_all (GtkAction *action,
	  gpointer data)
{
	marlin_window_show_all (MARLIN_WINDOW (data));
}

static void
zoom_selection (GtkAction *action,
		gpointer data)
{
	marlin_window_zoom_selection (MARLIN_WINDOW (data));
}

static void
destroy_info_cb (GtkWidget *widget,
		 MarlinClipboardInfo **info)
{
	g_free (*info);
	*info = NULL;
}

static void
view_clipboard_info (GtkAction *action,
		     gpointer data)
{
	static MarlinClipboardInfo *info = NULL;

	if (info == NULL) {
		info = marlin_clipboard_info_new (GTK_WINDOW (data));

		g_signal_connect (G_OBJECT (info->dialog), "destroy",
				  G_CALLBACK (destroy_info_cb), &info);
		gtk_widget_show (info->dialog);
	} else {
		gtk_window_present (GTK_WINDOW (info->dialog));
	}
}

static void
view_centre (GtkAction *action,
	     gpointer data)
{
	marlin_window_scroll_to_cursor (MARLIN_WINDOW (data));
}

static void
vzoom_in (GtkAction *action,
	  gpointer data)
{
	marlin_window_vzoom_in (MARLIN_WINDOW (data));
}

static void
vzoom_out (GtkAction *action,
	   gpointer data)
{
	marlin_window_vzoom_out (MARLIN_WINDOW (data));
}

static void
view_equalizers (GtkAction *action,
		 gpointer data)
{
	marlin_window_show_equalizers (MARLIN_WINDOW (data),
				       gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)));
}

static void
view_new (GtkAction *action,
	  MarlinWindow *window)
{
	MarlinSample *sample;

	g_object_get (G_OBJECT (window),
		      "sample", &sample,
		      NULL);

	marlin_open_window_with_sample (sample);
/* 	g_object_unref (G_OBJECT (sample)); */
}

/* FIXME: All these displays should use the same gconf key,
   and one place sets it and they all pay attention to it. */
static void
scale_action_changed (GtkAction *action,
		      GtkRadioAction *current,
		      MarlinWindow *window)
{
	MarlinScale scale;

	scale = gtk_radio_action_get_current_value (current);
	marlin_window_set_level_display (window, scale);
	marlin_window_set_meters_display (window, scale);
}

static void
marker_view_add_marker (GtkAction *action,
			MarlinWindow *window)
{
	marlin_window_marker_view_add_marker (window);
}

static void
marker_view_remove_marker (GtkAction *action,
			   MarlinWindow *window)
{
	marlin_window_marker_view_remove_marker (window);
}

static void
marker_view_goto_marker (GtkAction *action,
			 MarlinWindow *window)
{
	marlin_window_marker_view_goto_marker (window);
}

static void
marker_view_edit_marker (GtkAction *action,
			 MarlinWindow *window)
{
	marlin_window_marker_view_edit_marker (window);
}

static void
marker_view_snap_to_ticks (GtkAction *action,
			   MarlinWindow *window)
{
	marlin_window_marker_view_snap_to_ticks (window,
						 gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)));
}

static GtkActionEntry toplevel_entries[] = {
	{ "File", NULL, N_("_File") },
 	{ "Edit", NULL, N_("_Edit") },
	{ "Selection", NULL, N_("_Selection") },
	{ "View", NULL, N_("_View") },
	{ "Transport", NULL, N_("_Transport") },
	{ "Process", NULL, N_("_Process") },
	{ "Tools", NULL, N_("_Tools") },
	{ "Help", NULL, N_("_Help") },
};

static GtkActionEntry file_entries[] = {
	{ "FileNew", GTK_STOCK_NEW, N_("New"), NULL,
	  N_("Create a new sample"),
	  G_CALLBACK (file_new) },
	{ "FileNewDialog", GTK_STOCK_NEW, N_("New..."), "<control>N",
	  N_("Create a new sample"),
	  G_CALLBACK (file_new_dialog) },
	{ "FileOpen", GTK_STOCK_OPEN, N_("Open..."), "<control>O",
	  N_("Open a file"),
	  G_CALLBACK (file_open) },
	{ "FileRecent", NULL, N_("Recent Files"), NULL,
	  N_("Open a recently used file"),
	  NULL },
	{ "FileSave", GTK_STOCK_SAVE, N_("Save"), "<control>S",
	  N_("Save the current file"),
	  G_CALLBACK (file_save) },
	{ "FileSaveAs", GTK_STOCK_SAVE_AS, N_("Save As..."), "<control><shift>S",
	  N_("Save the current file with a different name"),
	  G_CALLBACK (file_save_as) },
	{ "FileProperties", MARLIN_STOCK_SOUND_PROPERTIES, N_("Information"), "<control>P",
	  N_("Information about the sample"),
	  G_CALLBACK (file_properties) },
	{ "FileClose", GTK_STOCK_CLOSE, N_("Close"), "<control>W",
	  N_("Close this window"),
	  G_CALLBACK (file_close) },
#if 0
	{ "FileExit", GTK_STOCK_QUIT, N_("Quit"), "<control>q",
	  N_("Quit Marlin"),
	  G_CALLBACK (file_quit) },
#endif
};

static GtkActionEntry help_entries[] = {
	{ "HelpContents", GTK_STOCK_HELP, N_("Contents"), NULL,
	  N_("Help Contents"),
	  G_CALLBACK (help_contents) },
	{ "HelpAbout", GTK_STOCK_ABOUT, N_("About Marlin"), NULL,
	  N_("Show information about Marlin"),
	  G_CALLBACK (help_about) },
};

static GtkActionEntry media_entries[] = {
	{ "MediaPlay", MARLIN_STOCK_PLAY, N_("_Play"), NULL,
	  N_("Play current sample or selection"),
	  G_CALLBACK (media_play) },
	{ "MediaPause", MARLIN_STOCK_PAUSE, N_("P_ause"), NULL,
	  N_("Pause playback"),
	  G_CALLBACK (media_pause) },
	{ "MediaStop", MARLIN_STOCK_STOP, N_("_Stop"), NULL,
	  N_("Stop playback"),
	  G_CALLBACK (media_stop) },
	{ "MediaPrevious", MARLIN_STOCK_PREVIOUS, N_("Move to S_tart"), NULL,
	  N_("Go to start of sample"),
	  G_CALLBACK (media_previous) },
	{ "MediaNext", MARLIN_STOCK_NEXT, N_("Move to _End"), NULL,
	  N_("Go to end of sample"),
	  G_CALLBACK (media_next) },
	{ "MediaRecord", MARLIN_STOCK_RECORD, N_("_Record..."), NULL,
	  N_("Record a new sample"),
	  G_CALLBACK (media_record) },
	{ "MediaForward", MARLIN_STOCK_FFWD, N_("Step _Forwards"), NULL,
	  N_("Step forward through the sample"),
	  G_CALLBACK (media_forward) },
	{ "MediaRewind", MARLIN_STOCK_REWIND, N_("Step _Backwards"), NULL,
	  N_("Step backwards through the sample"),
	  G_CALLBACK (media_rewind) },
	{ "MediaRepeat", MARLIN_STOCK_REPEAT, N_("Play _Loop"), NULL,
	  N_("Loop sample or selection"),
	  G_CALLBACK (media_repeat) },
};

static GtkActionEntry tools_entries[] = {
	{ "ToolsGenerate", NULL, N_("Generate"), NULL,
	  N_("Generate sounds"),
	  NULL },
};

static GtkActionEntry edit_entries[] = {
#if 0
	{ "EditUndo", GTK_STOCK_UNDO, N_("Undo"), "<control>z",
	  N_("Undo the previous action"),
	  G_CALLBACK (edit_undo) },
	{ "EditRedo", GTK_STOCK_REDO, N_("Redo"), "<control><shift>Z",
	  N_("Redo the next action"),
	  G_CALLBACK (edit_redo) },
#endif
	{ "EditHistory", MARLIN_STOCK_UNDO_HISTORY, N_("Undo History"), NULL,
	  N_("Show the undo history window"),
	  G_CALLBACK (edit_history) },
	{ "EditCopy", GTK_STOCK_COPY, N_("Copy"), "<control>C",
	  N_("Copy the selection to the clipboard"),
	  G_CALLBACK (edit_copy) },
	{ "EditCut", GTK_STOCK_CUT, N_("Cut"), "<control>X",
	  N_("Copy the selection to the clipboard and delete the selection"),
	  G_CALLBACK (edit_cut) },
	{ "EditPaste", GTK_STOCK_PASTE, N_("Paste"), "<control>V",
	  N_("Paste the clipboard contents into the sample"),
	  G_CALLBACK (edit_paste) },
	{ "EditPasteNew", MARLIN_STOCK_PASTE_AS_NEW, N_("Paste as New"), "<control><shift>V",
	  N_("Create a new sample from the contents of the clipboard"),
	  G_CALLBACK (edit_paste_new) },
	{ "EditPasteMix", NULL, N_("Mix..."), NULL,
	  N_("Mix the contents of the clipboard with the current sample"),
	  G_CALLBACK (edit_paste_mix) },
	{ "EditPasteXFade", NULL, N_("Crossfade..."), NULL,
	  N_("Crossfade the contents of the clipboard and the current sample"),
	  G_CALLBACK (edit_xfade) },
	{ "EditReplace", MARLIN_STOCK_PASTE_INTO, N_("Replace"), NULL,
	  N_("Replace the selection with the contents of the clipboard"),
	  G_CALLBACK (edit_replace) },
	{ "EditDelete", GTK_STOCK_DELETE, N_("Delete Selection"), "Delete",
	  N_("Delete the selection"),
	  G_CALLBACK (edit_delete) },
	{ "EditCrop", NULL, N_("Crop Selection"), "<control>Delete",
	  N_("Crop the area around the selection"),
	  G_CALLBACK (edit_crop) },
	{ "EditZero", NULL, N_("Zero Crossing"), NULL,
	  "", NULL },
	{ "EditMoveCursor", NULL, N_("Move Cursor..."), "<control>G",
	  N_("Move the cursor to a specific point"),
	  G_CALLBACK (edit_move_cursor) },
	{ "EditToolbars", NULL, N_("Customize Toolbars..."), NULL,
	  N_("Customize the toolbars"),
	  G_CALLBACK (edit_toolbars) },
};

static GtkActionEntry zero_entries[] = {
	{ "ZeroMovePrev", NULL, N_("Find Previous"), NULL,
	  N_("Move the cursor to the previous zero crossing"),
	  G_CALLBACK (zero_previous) },
	{ "ZeroMoveNext", NULL, N_("Find Next"), NULL,
	  N_("Move the cursor to the next zero crossing"),
	  G_CALLBACK (zero_next) },
};

static GtkActionEntry selection_entries[] = {
	{ "SelectAll", MARLIN_STOCK_SELECT_ALL, N_("Select All"), "<control>A",
	  N_("Select the whole sample"),
	  G_CALLBACK (edit_select_all) },
	{ "SelectNone", MARLIN_STOCK_SELECT_NONE, N_("Select Nothing"), "<shift><control>A",
	  N_("Deselect the whole selection"),
	  G_CALLBACK (edit_select_none) },
	{ "SelectRegion", NULL, N_("Select Region..."), NULL,
	  N_("Select a region"),
	  G_CALLBACK (edit_select_region) },
	{ "SelectHalve", MARLIN_STOCK_SELECTION_SHRINK, N_("Halve Selection Size"), NULL,
	  N_("Halve the selection size"),
	  G_CALLBACK (selection_halve) },
	{ "SelectDouble", MARLIN_STOCK_SELECTION_GROW, N_("Double Selection Size"), NULL,
	  N_("Double the selection size"),
	  G_CALLBACK (selection_double) },
	{ "SelectLeft", NULL, N_("Shift Selection Left"), NULL,
	  N_("Shift the selection to the left"),
	  G_CALLBACK (selection_left) },
	{ "SelectRight", NULL, N_("Shift Selection Right"), NULL,
	  N_("Shift the selection to the right"),
	  G_CALLBACK (selection_right) },
	{ "SelectSnapToZero", NULL, N_("Snap To Zero Crossing"), "Z",
	  N_("Snap the selection to the nearest zero crossings"),
	  G_CALLBACK (selection_snap_to_zero) },
};

static GtkActionEntry process_entries[] = {
	{ "ProcessMute", NULL, N_("Mute Sample"), NULL,
	  N_("Mute the selection"),
	  G_CALLBACK (process_mute) },
	{ "ProcessInvert", NULL, N_("Invert Sample"), NULL,
	  N_("Invert the selection"),
	  G_CALLBACK (process_invert) },
	{ "ProcessSwapChannels", NULL, N_("Swap Sample Channels"), NULL,
	  N_("Swap the samples channels"),
	  G_CALLBACK (process_swap_channels) },
	{ "ProcessVolume", NULL, N_("Adjust Sample Volume..."), NULL,
	  N_("Adjust the volume of the sample"),
	  G_CALLBACK (process_adjust_volume) },
	{ "ProcessNormalize", NULL, N_("Normalize Sample Volume..."), NULL,
	  N_("Normalize the volume of the sample"),
	  G_CALLBACK (process_normalize) },
	{ "ProcessInsertSilence", NULL, N_("Insert Silence..."), NULL,
	  N_("Insert silence into the sample"),
	  G_CALLBACK (process_insert_silence) },
	{ "ProcessAdjustChannels", NULL, N_("Adjust Number of Channels..."), NULL,
	  N_("Change the number of channels in the sample"),
	  G_CALLBACK (process_change_channels) },
	{ "ProcessSplitSample", NULL, N_("Split Sample on Markers"), NULL,
	  N_("Split the sample into multiple new ones"),
	  G_CALLBACK (process_split_sample) },
	{ "ProcessReverse", NULL, N_("Reverse Sample"), NULL,
	  N_("Reverse the sample"),
	  G_CALLBACK (process_reverse_sample) },
};

static GtkActionEntry view_entries[] = {
	{ "ViewNew", MARLIN_STOCK_NEW_VIEW, N_("New View"), NULL,
	  N_("Open a new view of this sample"),
	  G_CALLBACK (view_new) },
	{ "ZoomIn", MARLIN_STOCK_HZOOM_IN, N_("Zoom In"), "<control>plus",
	  N_("Zoom in on the sample"),
	  G_CALLBACK (zoom_in) },
	{ "ZoomOut", MARLIN_STOCK_HZOOM_OUT, N_("Zoom Out"), "<control>minus",
	  N_("Zoom out of the sample"),
	  G_CALLBACK (zoom_out) },
	{ "ShowAll", NULL, N_("Show All"), NULL,
	  N_("Show the whole sample"),
	  G_CALLBACK (zoom_all) },
	{ "ZoomSelection", NULL, N_("Zoom Selection"), NULL,
	  N_("Zoom in on the selection"),
	  G_CALLBACK (zoom_selection) },
	{ "ViewClipboard", NULL, N_("Clipboard Info"), NULL,
	  N_("Show information about the clipboard"),
	  G_CALLBACK (view_clipboard_info) },
	{ "ViewCentre", NULL, N_("Centre on Cursor"), NULL,
	  N_("Centre the sample view on the cursor"),
	  G_CALLBACK (view_centre) },
	{ "ViewVZoomIn", MARLIN_STOCK_VZOOM_IN, N_("Vertical Zoom In"), "<control><shift>plus",
	  N_("Zoom in vertically on the sample"),
	  G_CALLBACK (vzoom_in) },
	{ "ViewVZoomOut", MARLIN_STOCK_VZOOM_OUT, N_("Vertical Zoom Out"), "<control><shift>minus",
	  N_("Zoom out vertically on the sample"),
	  G_CALLBACK (vzoom_out) }
};

static GtkToggleActionEntry toggle_entries[] = {
	{ "ViewEqualizers", NULL, N_("Equalizers"), NULL,
	  N_("Change the visibility of the playback equalizers"),
	  G_CALLBACK (view_equalizers), TRUE },
};

void
marlin_window_menu_setup (MarlinWindow *window)
{
	GtkActionGroup *ag;
	MarlinRecentAction *mra;
	GtkAction *undo_action, *redo_action;
	MarlinUndoManager *undo_manager;

	ag = marlin_window_get_action_group (window);

	gtk_action_group_add_actions (ag, toplevel_entries, G_N_ELEMENTS (toplevel_entries), window);
	gtk_action_group_add_actions (ag, file_entries, G_N_ELEMENTS (file_entries), window);
	gtk_action_group_add_actions (ag, edit_entries, G_N_ELEMENTS (edit_entries), window);
	gtk_action_group_add_actions (ag, zero_entries, G_N_ELEMENTS (zero_entries), window);
	gtk_action_group_add_actions (ag, selection_entries, G_N_ELEMENTS (selection_entries), window);
	gtk_action_group_add_actions (ag, view_entries, G_N_ELEMENTS (view_entries), window);
	gtk_action_group_add_actions (ag, process_entries, G_N_ELEMENTS (process_entries), window);
	gtk_action_group_add_actions (ag, tools_entries, G_N_ELEMENTS (tools_entries), window);
	gtk_action_group_add_actions (ag, help_entries, G_N_ELEMENTS (help_entries), window);
	gtk_action_group_add_actions (ag, media_entries, G_N_ELEMENTS (media_entries), window);
	gtk_action_group_add_toggle_actions (ag, toggle_entries, G_N_ELEMENTS (toggle_entries), window);

	/* Add recent files */
	mra = marlin_recent_action_new ("FileOpenRecent");
	g_signal_connect (mra, "item-activated",
			  G_CALLBACK (file_open_recent), window);

	gtk_action_group_add_action (ag, GTK_ACTION (mra));
	g_object_unref (mra);

	/* Add recent files toolbar action */
	mra = marlin_recent_action_toolbar_new ("FileToolbarOpenRecent",
						NULL, GTK_STOCK_OPEN);
	g_signal_connect (mra, "activate",
			 G_CALLBACK (file_open), window);
	g_signal_connect (mra, "item-activated",
			  G_CALLBACK (file_open_recent), window);

	gtk_action_group_add_action (ag, GTK_ACTION (mra));
	g_object_unref (mra);

	/* Add undo action */
	undo_manager = marlin_base_window_get_undo_manager (MARLIN_BASE_WINDOW (window));

	undo_action = koto_undo_action_new ("EditUndo", undo_manager, TRUE);
	gtk_action_group_add_action_with_accel (ag, GTK_ACTION (undo_action),
						"<control>z");
	g_object_unref (undo_action);

	redo_action = koto_undo_action_new ("EditRedo", undo_manager, FALSE);
	gtk_action_group_add_action_with_accel (ag, GTK_ACTION (redo_action),
						"<shift><control>z");
	g_object_unref (redo_action);

}

static GtkActionEntry popup_toplevel_entries[] = {
	{ "FakeToplevel", NULL, "" },
	{ "FakeLToplevel", NULL, "" },
	{ "FakeMPTopLevel", NULL, "" },
	{ "FakeMetersToplevel", NULL, "" },
};

static GtkToggleActionEntry popup_toggle_entries[] = {
	{ "MarkerSnap", NULL, N_("Snap To Ticks"), NULL,
	  N_("Snap the markers to ticks"),
	  G_CALLBACK (marker_view_snap_to_ticks), FALSE }
};

static GtkRadioActionEntry scale_popup_radio_entries[] = {
	{ "ScalePercent", NULL, N_("_Linear"), NULL,
	  N_("Use a linear scale"),
	  MARLIN_SCALE_LINEAR },
	{ "ScaleDB", NULL, N_("_dB"), NULL,
	  N_("Use a logarithmic scale"),
	  MARLIN_SCALE_LOG },
};

static GtkActionEntry marker_entries[] = {
	{ "MarkerAdd", GTK_STOCK_ADD, N_("Add Marker"), NULL,
	  N_("Add a marker to the sample"),
	  G_CALLBACK (marker_view_add_marker) },
	{ "MarkerDelete", GTK_STOCK_REMOVE, N_("Remove Marker"), NULL,
	  N_("Remove this marker from the sample"),
	  G_CALLBACK (marker_view_remove_marker) },
	{ "MarkerGoto", GTK_STOCK_JUMP_TO, N_("Goto Marker"), NULL,
	  N_("Move the cursor to this marker's location"),
	  G_CALLBACK (marker_view_goto_marker) },
	{ "MarkerEdit", NULL, N_("Edit Marker"), NULL,
	  N_("Edit this marker"),
	  G_CALLBACK (marker_view_edit_marker) },
};

void
marlin_window_menu_popup_setup (MarlinWindow *window)
{
	GtkActionGroup *ag;
	MarlinScale scale;

	scale = marlin_gconf_get_int ("/apps/marlin/scale-display");
	ag = marlin_window_get_popup_action_group (window);

	gtk_action_group_add_actions (ag, popup_toplevel_entries, G_N_ELEMENTS (popup_toplevel_entries), window);

	gtk_action_group_add_radio_actions (ag, scale_popup_radio_entries,
					    G_N_ELEMENTS (scale_popup_radio_entries),
					    scale,
					    G_CALLBACK (scale_action_changed),
					    window);
	gtk_action_group_add_actions (ag, marker_entries, G_N_ELEMENTS (marker_entries), window);
	gtk_action_group_add_toggle_actions (ag, popup_toggle_entries, G_N_ELEMENTS (popup_toggle_entries), window);
}
