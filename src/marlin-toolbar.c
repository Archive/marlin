/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *            (C) 2001, 2002 Jorn Baayen
 *            (C) 2006 - 2008 Iain Holmes  <iain@gnome.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-file-utils.h>

#include <ephy-toolbars-model.h>

#include "marlin-toolbar.h"
#include "marlin-window.h"
#include "main.h"

enum
{
	PROP_0,
	PROP_MARLIN_WINDOW
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_TOOLBAR_TYPE, MarlinToolbarPrivate))
G_DEFINE_TYPE (MarlinToolbar, marlin_toolbar, EGG_TYPE_EDITABLE_TOOLBAR);

struct MarlinToolbarPrivate
{
	MarlinWindow *window;
 	GtkUIManager *ui_merge;
	GtkActionGroup *action_group;
	gboolean visibility;
};

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (marlin_toolbar_parent_class)->finalize (object);
}

static void
action_request_cb (EggEditableToolbar *etoolbar,
		   char               *action_name,
		   gpointer            data)
{
}

static void
marlin_toolbar_set_window (MarlinToolbar *t,
                           MarlinWindow  *window)
{
	EphyToolbarsModel *tb_model = marlin_get_toolbars_model ();

	g_return_if_fail (t->priv->window == NULL);

	t->priv->window = window;
 	t->priv->ui_merge = marlin_window_get_menu_merge (window);

	g_signal_connect (t, "action_request",
			  G_CALLBACK (action_request_cb), NULL);

	g_object_set (G_OBJECT (t),
                      "MenuMerge", t->priv->ui_merge,
		      "ToolbarsModel", tb_model,
		      NULL);
}

static void
set_property (GObject      *object,
              guint         prop_id,
              const GValue *value,
              GParamSpec   *pspec)
{
        MarlinToolbar *t = MARLIN_TOOLBAR (object);

        switch (prop_id) {
	case PROP_MARLIN_WINDOW:
		marlin_toolbar_set_window (t, g_value_get_object (value));
		break;
        }
}

static void
get_property (GObject    *object,
              guint       prop_id,
              GValue     *value,
              GParamSpec *pspec)
{
        MarlinToolbar *t = MARLIN_TOOLBAR (object);

        switch (prop_id) {
	case PROP_MARLIN_WINDOW:
		g_value_set_object (value, t->priv->window);
		break;
        }
}

static void
marlin_toolbar_class_init (MarlinToolbarClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

        g_type_class_add_private (object_class, sizeof (MarlinToolbarPrivate));
	g_object_class_install_property (object_class,
                                         PROP_MARLIN_WINDOW,
                                         g_param_spec_object ("MarlinWindow",
							      "", "",
                                                              MARLIN_WINDOW_TYPE,
                                                              G_PARAM_READWRITE));
}

static void
marlin_toolbar_init (MarlinToolbar *t)
{
        t->priv = GET_PRIVATE (t);

	t->priv->window = NULL;
	t->priv->visibility = TRUE;
}

MarlinToolbar *
marlin_toolbar_new (MarlinWindow *window)
{
	MarlinToolbar *t;

	t = MARLIN_TOOLBAR (g_object_new (MARLIN_TOOLBAR_TYPE,
                                          "MarlinWindow", window,
                                          NULL));

	return t;
}
