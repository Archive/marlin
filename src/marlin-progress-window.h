/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PROGRESS_WINDOW_H__
#define __MARLIN_PROGRESS_WINDOW_H__

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-operation.h>

G_BEGIN_DECLS

typedef struct _MarlinProgressWindow {
	MarlinSample *sample;
	MarlinOperation *operation;
	gulong signal_id;
	gulong op_id;

	GtkWidget *dialog;
	GtkWidget *label;
	GtkWidget *progress_bar;
	GtkWidget *parent_window;
	GtkWidget *sub_process;
	GtkWidget *pause;

	GTimer *timer;

	guint32 visible_id;

	int progress_jar_position;
} MarlinProgressWindow;

MarlinProgressWindow *marlin_progress_window_new (MarlinSample *sample,
						  MarlinOperation *operation,
						  GtkWindow *window,
						  const char *primary,
						  const char *secondary);
void marlin_progress_window_destroy (MarlinProgressWindow *window);

G_END_DECLS

#endif
