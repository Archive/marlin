/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <time.h>
#include <unistd.h>

#if 0
#include <uuid/uuid.h>
#endif

#include <glib.h>

#include <glib/gi18n.h>

#include <gio/gio.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>

#include <gconf/gconf-client.h>

#ifdef HAVE_MEDIA_PROFILES
#include <profiles/gnome-media-profiles.h>
#endif

#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-sample-view.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-overview-bar.h>

#include <marlin/marlin-load-pipeline.h>
#include <marlin/marlin-save-pipeline.h>
#include <marlin/marlin-jack-play.h>

#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-marker-view.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-a11y-helper.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-cross-fader.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-level-ruler.h>
#include <marlin/marlin-plugin.h>
#include <marlin/marlin-base-window.h>
#include <marlin/marlin-progress-dialog.h>
#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-file-chooser.h>

#include <dialogs/marlin-information-dialog.h>
#include <dialogs/marlin-adjust-channel-dialogs.h>
#include <dialogs/marlin-mp3-dialog.h>
#include <dialogs/marlin-vorbis-dialog.h>

#include <dialogs/marlin-crossfade-dialog.h>
#include <dialogs/marlin-select-region-dialog.h>
#include <dialogs/marlin-undo-history-dialog.h>
#include <dialogs/marlin-edit-marker-dialog.h>
#include <dialogs/marlin-add-marker-dialog.h>
#include <dialogs/marlin-move-cursor-dialog.h>

#include <other/gtkvumeter.h>

#include <marlin-toolbar.h>

#include "main.h"
#include "marlin-window.h"
#include "marlin-window-menu.h"

enum {
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SAMPLE,
};

enum {
	DND_TYPE_TEXT_URI_LIST
};

static GtkTargetEntry drop_types[] = {
	{ "text/uri-list", 0, DND_TYPE_TEXT_URI_LIST }
};

static int num_drop_types = sizeof (drop_types) / sizeof (drop_types[0]);

typedef enum _MarlinState {
	MARLIN_STATE_STOPPED,
	MARLIN_STATE_PLAYING,
	MARLIN_STATE_PAUSED,
	MARLIN_STATE_RECORDING
} MarlinState;

/* FIXME: Check how we can make this smaller */
struct _MarlinWindowPrivate {
	GtkWidget *main_vbox;
	GtkWidget *menu_dock;
	GtkWidget *menubar;

	MarlinToolbar *toolbar;
	GList *toolbars;

	MarlinSample *sample;
	guint notify_id;

	MarlinSampleSelection *selection;
	guint selection_id;

	MarlinSampleView *view;
	MarlinProgressDialog *progress_window;
	MarlinOverviewBar *overview_bar;

	MarlinUndoManager *undo;
	guint undo_changed_id;

	GtkWidget *marker_view, *vruler, *hscrollbar;
	GtkWidget *message_status;
	guint help_message_cid;

	MarlinMarkerModel *marker_model;
	guint marker_added_id, marker_removed_id;

	MarlinDisplay position_display;

	GtkWidget *position_status;
	GtkWidget *sel_start_status;
	GtkWidget *sel_end_status;

	/* UI stuff */
	GtkAccelGroup *accel_group;
	GtkActionGroup *ui_action, *popup_action;
	GObject *ui_merge;

	MarlinJackPlay *jack;
	guint jack_owner_id;
	guint32 owner_changed_id;
	guint32 eos_id;
	guint32 level_id;
	guint32 position_id;

	MarlinState state;
	guint64 initial; /* The initial position of the cursor when play
			    starts */

	/* Start finish for looping and selections */
	guint64 start, finish;
	gboolean repeating;

	/* Tell the op_handler what to do */
	gboolean close_on_op_complete;

	guint clip_id; /* Signal handler for listening to the clipboard */

	/* Dialogs */
	GtkWidget *select_region_dialog;
	GtkWidget *move_cursor_dialog;
	GtkWidget *properties_dialog;
	GtkWidget *xfade_dialog;
	GtkWidget *adjust_channels_dialog;
	GtkWidget *undo_history_dialog;
	GtkWidget *edit_marker_dialog;
	GtkWidget *add_marker_dialog;

	time_t last_save; /* Time of the last save, in seconds */

	GtkWidget *meters_event; /* Event box that holds the meters */
	GtkWidget *left_vu, *right_vu;
	gboolean show_vu;

	gboolean mv_in_marker;
	MarlinMarker *current_marker;
};

static void set_path_sensitive (GtkActionGroup *ag,
				const char     *name,
				gboolean        sensitive);
static void set_marker_popup_sensitive (MarlinWindow *window,
					gboolean      sensitive);

void marlin_window_set_file_sensitive (MarlinWindow *window,
				       gboolean      sensitive);
void marlin_window_set_jack_sensitive (MarlinWindow *window,
				       gboolean      sensitive);
void marlin_window_set_media_bar_sensitive (MarlinWindow *window,
					    gboolean      sensitive);
void marlin_window_set_tools_sensitive (MarlinWindow *window,
					gboolean      sensitive);
void marlin_window_set_view_sensitive (MarlinWindow *window,
				       gboolean      sensitive);
void marlin_window_set_edit_sensitive (MarlinWindow *window,
				       gboolean      sensitive);
void marlin_window_set_selection_sensitive (MarlinWindow *window,
					    gboolean      sensitive);
void marlin_window_set_process_sensitive (MarlinWindow *window,
					  gboolean      sensitive);
void marlin_window_set_plugins_sensitive (MarlinWindow *window,
					  gboolean      sensitive);
void marlin_window_set_process_name (MarlinWindow *window);
void marlin_window_set_edit_name (MarlinWindow *window);
void marlin_window_pause_sample (MarlinWindow *window);

static void base_window_init (MarlinBaseWindowClass *iface);

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_WINDOW_TYPE, MarlinWindowPrivate))
G_DEFINE_TYPE_EXTENDED (MarlinWindow, marlin_window, GTK_TYPE_WINDOW, 0,
			G_IMPLEMENT_INTERFACE (MARLIN_BASE_WINDOW_TYPE,
					       base_window_init));
static GdkPixbuf *window_icon = NULL;

#define DEFAULT_WIDTH 770
#define DEFAULT_HEIGHT 400

static void
finalize (GObject *object)
{
	MarlinWindow *window;
	MarlinWindowPrivate *priv;

	window = MARLIN_WINDOW (object);
	priv = window->priv;

	if (priv->select_region_dialog != NULL) {
		gtk_widget_destroy (priv->select_region_dialog);
	}

	if (priv->move_cursor_dialog != NULL) {
		gtk_widget_destroy (priv->move_cursor_dialog);
	}

	if (priv->properties_dialog != NULL) {
		gtk_widget_destroy (priv->properties_dialog);
	}

	if (priv->undo_history_dialog != NULL) {
		gtk_widget_destroy (priv->undo_history_dialog);
	}

	if (priv->xfade_dialog != NULL) {
		gtk_widget_destroy (priv->xfade_dialog);
	}

	if (priv->adjust_channels_dialog != NULL) {
		gtk_widget_destroy (priv->adjust_channels_dialog);
	}

	G_OBJECT_CLASS (marlin_window_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinWindow *window = MARLIN_WINDOW (object);
	MarlinWindowPrivate *priv;

	priv = window->priv;

	if (priv->sample != NULL) {
		marlin_window_stop_sample (window);
		g_signal_handler_disconnect (G_OBJECT (priv->sample),
					     priv->notify_id);
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}

	if (priv->selection != NULL) {
		g_signal_handler_disconnect (G_OBJECT (priv->selection),
					     priv->selection_id);
		g_object_unref (G_OBJECT (priv->selection));
		priv->selection = NULL;
	}

	if (priv->marker_model != NULL) {
		g_signal_handler_disconnect (G_OBJECT (priv->marker_model),
					     priv->marker_added_id);
		g_signal_handler_disconnect (G_OBJECT (priv->marker_model),
					     priv->marker_removed_id);
		g_object_unref (G_OBJECT (priv->marker_model));
		priv->marker_model = NULL;
	}

	if (priv->jack != NULL) {
		MarlinProgram *program;

		program = marlin_program_get_default ();
		g_signal_handler_disconnect (program, priv->owner_changed_id);

		/* Turn off the jack signals */
		g_signal_handler_disconnect (priv->jack, priv->eos_id);
		g_signal_handler_disconnect (priv->jack, priv->level_id);
		g_signal_handler_disconnect (priv->jack, priv->position_id);

		/* Give up our jack ownership */
		marlin_program_release_jack (program, priv->jack_owner_id);

		g_object_unref (priv->jack);
		priv->jack = NULL;
	}

	if (priv->undo != NULL) {
		g_signal_handler_disconnect (G_OBJECT (priv->undo),
					     priv->undo_changed_id);
		g_object_unref (G_OBJECT (priv->undo));
		priv->undo = NULL;
	}

	if (window->priv->clip_id > 0) {
		MarlinProgram *prog = marlin_program_get_default ();
		g_signal_handler_disconnect (G_OBJECT (prog),
					     window->priv->clip_id);
		window->priv->clip_id = 0;
	}

	G_OBJECT_CLASS (marlin_window_parent_class)->dispose (object);
}

static void
marlin_window_sample_notify (MarlinSample *sample,
			     GParamSpec   *pspec,
			     MarlinWindow *window)
{
	guint fpp;
	guint channels;

	if (strcmp (pspec->name, "total-frames") == 0 ||
	    strcmp (pspec->name, "dirty") == 0) {
		g_object_get (G_OBJECT (window->priv->view),
			      "frames_per_pixel", &fpp,
			      NULL);

		g_object_set (G_OBJECT (window->priv->overview_bar),
			      "frames_per_page", (guint64) fpp * GTK_WIDGET (window->priv->view)->allocation.width,
			      NULL);

		marlin_window_set_media_bar_sensitive (window, TRUE);
		marlin_window_set_tools_sensitive (window, TRUE);
		marlin_window_set_file_sensitive (window, TRUE);
		marlin_window_set_title (window, sample);
		marlin_window_set_process_name (window);
		marlin_window_set_process_sensitive (window, TRUE);
		marlin_window_set_edit_sensitive (window, TRUE);
		marlin_window_set_selection_sensitive (window, TRUE);
		marlin_window_set_plugins_sensitive (window, TRUE);
	}

	if (strcmp (pspec->name, "channels") == 0) {
		g_object_get (G_OBJECT (window->priv->sample),
			      "channels", &channels,
			      NULL);
		g_object_set (G_OBJECT (window->priv->vruler),
			      "channels", channels,
			      NULL);

		if (channels == 1) {
			gtk_widget_hide (window->priv->right_vu);
		} else {
			gtk_widget_show (window->priv->right_vu);
		}

		marlin_window_set_process_name (window);
	}
}

static void
selection_changed (MarlinSampleSelection *selection,
		   MarlinWindow *window)
{
	MarlinCoverage coverage;
	guint64 sel_start, sel_finish;

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     &sel_start, &sel_finish);
	if (coverage == MARLIN_COVERAGE_NONE) {
		gtk_statusbar_pop (GTK_STATUSBAR (window->priv->sel_start_status), 0);
		gtk_statusbar_pop (GTK_STATUSBAR (window->priv->sel_end_status), 0);
	} else {
		char *s_display, *e_display;
		guint64 fpm, fpb;
		guint rate, bpm;
		guint64 ms;

		switch (window->priv->position_display) {
		case MARLIN_DISPLAY_FRAMES:
			s_display = g_strdup_printf ("%llu", sel_start);
			e_display = g_strdup_printf ("%llu", sel_finish);
			break;

		case MARLIN_DISPLAY_TIME_LONG:
			g_object_get (G_OBJECT (window->priv->sample),
				      "sample_rate", &rate,
				      NULL);

			ms = marlin_frames_to_ms (sel_start, rate);
			s_display = marlin_ms_to_time_string (ms);

			ms = marlin_frames_to_ms (sel_finish, rate);
			e_display = marlin_ms_to_time_string (ms);
			break;

		case MARLIN_DISPLAY_SECONDS:
			g_object_get (G_OBJECT (window->priv->sample),
				      "sample_rate", &rate,
				      NULL);

			ms = marlin_frames_to_ms (sel_start, rate);
			s_display = g_strdup_printf ("%.3f", (double)ms / 1000.0);
			ms = marlin_frames_to_ms (sel_finish, rate);
			e_display = g_strdup_printf ("%.3f", (double)ms / 1000.0);
			break;

		case MARLIN_DISPLAY_TIME_FRAMES:
			g_object_get (G_OBJECT (window->priv->sample),
				      "sample_rate", &rate,
				      NULL);

			ms = marlin_frames_to_ms (sel_start, rate);
			s_display = marlin_ms_to_time_frame_string (ms, rate);

			ms = marlin_frames_to_ms (sel_finish, rate);
			e_display = marlin_ms_to_time_frame_string (ms, rate);
			break;

		case MARLIN_DISPLAY_BEATS:
			g_object_get (G_OBJECT (window->priv->sample),
				      "sample_rate", &rate,
				      "bpm", &bpm,
				      NULL);

			fpm = rate * 60;
			fpb = fpm / bpm;

			s_display = g_strdup_printf ("%llu", sel_start / fpb);
			e_display = g_strdup_printf ("%llu", sel_finish / fpb);

		default:
			s_display = NULL;
			e_display = NULL;
			break;
		}

		gtk_statusbar_pop (GTK_STATUSBAR (window->priv->sel_start_status), 0);
		gtk_statusbar_pop (GTK_STATUSBAR (window->priv->sel_end_status), 0);
		gtk_statusbar_push (GTK_STATUSBAR (window->priv->sel_start_status), 0, s_display);
		gtk_statusbar_push (GTK_STATUSBAR (window->priv->sel_end_status), 0, e_display);

		g_free (s_display);
		g_free (e_display);
	}

	/* Edit works out what needs to be sensitive... */
	marlin_window_set_edit_sensitive (window, TRUE);
	marlin_window_set_selection_sensitive (window, TRUE);
	marlin_window_set_process_sensitive (window, TRUE);
	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
	marlin_window_set_process_name (window);
	marlin_window_set_edit_name (window);
}

static void
marker_added (MarlinMarkerModel *model,
	      MarlinMarker      *marker,
	      MarlinWindow      *window)
{
	marlin_window_set_file_sensitive (window, TRUE);
	marlin_window_set_process_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

static void
marker_removed (MarlinMarkerModel *model,
		MarlinMarker      *marker,
		MarlinWindow      *window)
{
	marlin_window_set_file_sensitive (window, TRUE);
	marlin_window_set_process_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

static void
undo_changed (MarlinUndoManager *manager,
	      MarlinWindow      *window)
{
	marlin_window_set_edit_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
	marlin_window_set_edit_name (window);
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinWindow *window;
	MarlinWindowPrivate *priv;
	guint64 total_frames;
	gboolean have_sample = FALSE;

	window = MARLIN_WINDOW (object);
	priv = window->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		if (priv->sample != NULL) {
			g_signal_handler_disconnect (G_OBJECT (priv->sample),
						     priv->notify_id);
			g_object_unref (G_OBJECT (priv->sample));
		}

		if (priv->selection != NULL) {
			g_signal_handler_disconnect (G_OBJECT (priv->selection),
						     priv->selection_id);
			g_object_unref (G_OBJECT (priv->selection));
		}

		if (priv->marker_model != NULL) {
			g_signal_handler_disconnect (G_OBJECT (priv->marker_model),
						     priv->marker_added_id);
			g_signal_handler_disconnect (G_OBJECT (priv->marker_model),
						     priv->marker_removed_id);
			g_object_unref (G_OBJECT (priv->marker_model));
		}

		priv->sample = g_value_dup_object (value);

		priv->notify_id = g_signal_connect (G_OBJECT (priv->sample), "notify",
						    G_CALLBACK (marlin_window_sample_notify),
						    window);

		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &total_frames,
			      "selection", &priv->selection,
			      "markers", &priv->marker_model,
			      NULL);

		priv->marker_added_id = g_signal_connect (G_OBJECT (priv->marker_model),
							  "marker-added",
							  G_CALLBACK (marker_added), window);
		priv->marker_removed_id = g_signal_connect (G_OBJECT (priv->marker_model),
							    "marker-removed",
							    G_CALLBACK (marker_removed), window);

		g_object_set (G_OBJECT (priv->view),
			      "sample", priv->sample,
			      NULL);
		g_object_set (G_OBJECT (priv->overview_bar),
			      "sample", priv->sample,
			      NULL);

		g_object_set (G_OBJECT (priv->marker_view),
			      "model", priv->marker_model,
			      "sample", priv->sample,
			      NULL);
		g_object_set (G_OBJECT (priv->undo),
			      "sample", priv->sample,
			      NULL);

		if (priv->jack) {
			g_object_set (G_OBJECT (priv->jack),
				      "sample", priv->sample,
				      NULL);
		}

		priv->selection_id = g_signal_connect (G_OBJECT (priv->selection),
						       "changed",
						       G_CALLBACK (selection_changed),
						       window);

		marlin_window_set_title (window, priv->sample);

		/* If we have frames, then we have a sample that contains
		   some sound */
		have_sample = (total_frames > 0);
		marlin_window_set_file_sensitive (window, have_sample);
		marlin_window_set_media_bar_sensitive (window, have_sample);
		marlin_window_set_tools_sensitive (window, have_sample);
		marlin_window_set_view_sensitive (window, have_sample);
		marlin_window_set_edit_sensitive (window, have_sample);
		marlin_window_set_selection_sensitive (window, have_sample);
		marlin_window_set_process_sensitive (window, have_sample);
		marlin_window_set_plugins_sensitive (window, have_sample);

		marlin_window_set_process_name (window);
		marlin_window_set_edit_name (window);

		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	MarlinWindow *window;

	window = MARLIN_WINDOW (object);

	switch (prop_id) {
	case PROP_SAMPLE:
		g_value_set_object (value, window->priv->sample);
		break;

	default:
		break;
	}
}

static gboolean
focus_in_event (GtkWidget     *widget,
		GdkEventFocus *event)
{
	MarlinWindow *window = (MarlinWindow *) widget;
	MarlinWindowPrivate *priv = window->priv;
	MarlinProgram *program = marlin_program_get_default ();

	/* Will emit jack-owner-changed signal if window is allowed to
	   own jack */
	marlin_program_acquire_jack (program, priv->jack_owner_id, FALSE);

	return FALSE;
}

static gboolean
focus_out_event (GtkWidget     *widget,
		 GdkEventFocus *event)
{
	return FALSE;
}

static void
marlin_window_class_init (MarlinWindowClass *klass)
{
	GObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;

	object_class->finalize = finalize;
	object_class->dispose = dispose;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->focus_in_event = focus_in_event;
	widget_class->focus_out_event = focus_out_event;

	g_type_class_add_private (object_class, sizeof (MarlinWindowPrivate));
	g_object_class_install_property (object_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample", "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
}

static void
add_widget (GtkUIManager *merge,
	    GtkWidget    *widget,
	    MarlinWindow *window)
{
	if (GTK_IS_MENU_SHELL (widget)) {
		window->priv->menubar = widget;
	} else {
		window->priv->toolbars = g_list_append (window->priv->toolbars, widget);
		/* FIXME: Somehow */
		/* Hack to make the toolbars not have text */
/* 		gtk_toolbar_set_style (GTK_TOOLBAR (widget), GTK_TOOLBAR_ICONS); */
	}

	gtk_box_pack_start (GTK_BOX (window->priv->menu_dock), widget,
			    FALSE, FALSE, 0);
	gtk_widget_show (widget);
}

static void
menu_item_select_cb (GtkMenuItem  *proxy,
		     MarlinWindow *window)
{
	GtkAction *action;
	char *message;

	action = g_object_get_data (G_OBJECT (proxy), "gtk-action");
	g_return_if_fail (action != NULL);

	g_object_get (G_OBJECT (action),
		      "tooltip", &message,
		      NULL);
	if (message && window->priv->message_status) {
		gtk_statusbar_push (GTK_STATUSBAR (window->priv->message_status),
				    window->priv->help_message_cid, message);
		g_free (message);
	}
}

static void
menu_item_deselect_cb (GtkMenuItem  *proxy,
		       MarlinWindow *window)
{
	if (window->priv->message_status) {
		gtk_statusbar_pop (GTK_STATUSBAR (window->priv->message_status),
				   window->priv->help_message_cid);
	}
}

static void
connect_proxy_cb (GtkUIManager *manager,
		  GtkAction    *action,
		  GtkWidget    *proxy,
		  MarlinWindow *window)
{
	if (GTK_IS_MENU_ITEM (proxy)) {
		g_signal_connect (proxy, "select",
				  G_CALLBACK (menu_item_select_cb), window);
		g_signal_connect (proxy, "deselect",
				  G_CALLBACK (menu_item_deselect_cb), window);
	}
}

static void
disconnect_proxy_cb (GtkUIManager *manager,
		     GtkAction    *action,
		     GtkWidget    *proxy,
		     MarlinWindow *window)
{
	if (GTK_IS_MENU_ITEM (proxy)) {
		g_signal_handlers_disconnect_by_func (proxy, G_CALLBACK (menu_item_select_cb), window);
		g_signal_handlers_disconnect_by_func (proxy, G_CALLBACK (menu_item_deselect_cb), window);
	}
}

static void
marlin_window_init (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	GtkUIManager *merge;
	GConfClient *client;
	GList *plugins;
	char *xml;

	window->priv = GET_PRIVATE (window);
	priv = window->priv;

	priv->toolbars = NULL;

	priv->undo = marlin_undo_manager_new ();
	priv->undo_changed_id = g_signal_connect (G_OBJECT (priv->undo),
						  "changed",
						  G_CALLBACK (undo_changed),
						  window);

	priv->main_vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (priv->main_vbox);
	gtk_container_add (GTK_CONTAINER (window), priv->main_vbox);

	priv->menu_dock = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (priv->menu_dock);
	gtk_box_pack_start (GTK_BOX (priv->main_vbox), priv->menu_dock,
			    FALSE, TRUE, 0);

	client = gconf_client_get_default ();
	priv->position_display = gconf_client_get_int (client,
						       "/apps/marlin/system-state/position-display",
						       NULL);
	priv->show_vu = gconf_client_get_bool (client,
					       "/apps/marlin/show-equalizers",
					       NULL);
	g_object_unref (G_OBJECT (client));

	priv->close_on_op_complete = FALSE;
	priv->state = MARLIN_STATE_STOPPED;

	merge = gtk_ui_manager_new ();

	/* Connect to the merge for UI updates */
	g_signal_connect (merge, "connect_proxy",
			  G_CALLBACK (connect_proxy_cb), window);
	g_signal_connect (merge, "disconnect_proxy",
			  G_CALLBACK (disconnect_proxy_cb), window);

	/* Action stuff */
	priv->ui_action = gtk_action_group_new ("WindowActions");
	marlin_window_menu_setup (window);
	gtk_ui_manager_insert_action_group (merge, priv->ui_action, 0);

	priv->popup_action = gtk_action_group_new ("PopupsActions");
	marlin_window_menu_popup_setup (window);
	gtk_ui_manager_insert_action_group (merge, priv->popup_action, 0);

	priv->ui_merge = G_OBJECT (merge);
	g_signal_connect (G_OBJECT (merge), "add_widget",
			  G_CALLBACK (add_widget), window);
	xml = marlin_file ("marlin.xml");
	if (xml == NULL) {
		g_error ("Marlin not installed correctly");
	}

	gtk_ui_manager_add_ui_from_file (merge, xml, NULL);
	g_free (xml);

	/* Merge all the plugins */
	for (plugins = marlin_plugin_get_list (); plugins; plugins = plugins->next) {
		MarlinPluginInfo *info = plugins->data;
		MarlinPluginFuncs *funcs = info->funcs;

		if (funcs->merge_ui) {
			funcs->merge_ui (merge, MARLIN_BASE_WINDOW (window));
		}
	}

	priv->accel_group = gtk_ui_manager_get_accel_group (merge);
	gtk_window_add_accel_group (GTK_WINDOW (window), priv->accel_group);

	/* Window holds ref */
	g_object_unref (priv->accel_group);

 	priv->toolbar = marlin_toolbar_new (window);
	gtk_widget_show (GTK_WIDGET (priv->toolbar));
	gtk_box_pack_end (GTK_BOX (priv->menu_dock), GTK_WIDGET (priv->toolbar),
			  FALSE, FALSE, 0);

	priv->last_save = time (NULL);
}

static MarlinSample *
get_sample (MarlinBaseWindow *window)
{
	return MARLIN_WINDOW (window)->priv->sample;
}

static guint64
get_position (MarlinBaseWindow *window)
{
	guint64 position;

	g_object_get (G_OBJECT (MARLIN_WINDOW (window)->priv->view),
		      "cursor_position", &position,
		      NULL);

	return position;
}

static void
set_position (MarlinBaseWindow *window,
	      guint64           position)
{
	g_object_set (G_OBJECT (MARLIN_WINDOW (window)->priv->view),
		      "cursor_position", position,
		      NULL);
}

static MarlinMarkerView *
get_marker_view (MarlinBaseWindow *window)
{
	return (MarlinMarkerView *) MARLIN_WINDOW (window)->priv->marker_view;
}

static MarlinSampleView *
get_sample_view (MarlinBaseWindow *window)
{
	return (MarlinSampleView *) MARLIN_WINDOW (window)->priv->view;
}

static MarlinUndoManager *
get_undo_manager (MarlinBaseWindow *window)
{
	return MARLIN_WINDOW (window)->priv->undo;
}

static void
base_window_init (MarlinBaseWindowClass *iface)
{
	iface->get_sample = get_sample;
	iface->get_position = get_position;
	iface->set_position = set_position;
	iface->get_marker_view = get_marker_view;
	iface->get_sample_view = get_sample_view;
	iface->get_undo_manager = get_undo_manager;
}

static void
statusbar_set_frames (GtkWidget    *statusbar,
		      MarlinWindow *window,
		      guint64       frames)
{
	MarlinWindowPrivate *priv = window->priv;
	guint rate, bpm;
	guint64 ms, fpb, fpm;
	char *display = NULL;

	if (priv->sample) {
		g_object_get (G_OBJECT (priv->sample),
			      "sample_rate", &rate,
			      "bpm", &bpm,
			      NULL);
	} else {
		rate = 48000;
		bpm = 120;
	}

	switch (priv->position_display) {
	case MARLIN_DISPLAY_FRAMES:
		display = g_strdup_printf ("%llu", frames);
		break;

	case MARLIN_DISPLAY_TIME_LONG:
		ms = marlin_frames_to_ms (frames, rate);
		display = marlin_ms_to_time_string (ms);
		break;

	case MARLIN_DISPLAY_SECONDS:
		ms = marlin_frames_to_ms (frames, rate);
		display = g_strdup_printf ("%.3f", (double)ms / 1000.0);
		break;

	case MARLIN_DISPLAY_TIME_FRAMES:
		ms = marlin_frames_to_ms (frames, rate);
		display = marlin_ms_to_time_frame_string (ms, rate);
		break;

	case MARLIN_DISPLAY_BEATS:
		fpm = rate * 60;
		fpb = fpm / bpm;

		display = g_strdup_printf ("%llu", fpb > 0 ? frames / fpb : 0);
		break;

	default:
		return;
	}

	gtk_statusbar_pop (GTK_STATUSBAR (statusbar), 0);
	gtk_statusbar_push (GTK_STATUSBAR (statusbar), 0, display);

	g_free (display);
}

static void
marlin_window_change_overview_bar_fpp (MarlinSampleView *view,
				       guint             frames_per_pixel,
				       MarlinWindow     *window)
{
	g_object_set (G_OBJECT (window->priv->overview_bar),
		      "frames_per_page", (guint64) frames_per_pixel * GTK_WIDGET (view)->allocation.width,
		      NULL);

	g_object_set (G_OBJECT (window->priv->marker_view),
		      "frames_per_pixel", frames_per_pixel,
		      NULL);

	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

static void
view_vzoom_changed (MarlinSampleView *view,
		    float             vmax,
		    float             vmin,
		    MarlinWindow     *window)
{
	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
	marlin_level_ruler_set_levels (MARLIN_LEVEL_RULER (window->priv->vruler),
				       vmin, vmax);
}

static void
marlin_window_page_start_changed (MarlinSampleView *view,
				  guint64           start_frame,
				  MarlinWindow     *window)
{
	guint frames_per_pixel;

	g_object_get (G_OBJECT (view),
		      "frames_per_pixel", &frames_per_pixel,
		      NULL);

	g_object_set (G_OBJECT (window->priv->overview_bar),
		      "page-start", start_frame,
		      NULL);
}

static void
marlin_window_view_size_allocate (MarlinSampleView *view,
				  GtkAllocation    *allocation,
				  MarlinWindow     *window)
{
	guint frames_per_pixel;

	g_object_get (G_OBJECT (view),
		      "frames_per_pixel", &frames_per_pixel,
		      NULL);

	g_object_set (G_OBJECT (window->priv->overview_bar),
		      "frames_per_page", (guint64) frames_per_pixel * allocation->width,
		      NULL);
}

static void
marlin_window_overview_bar_start_changed (MarlinOverviewBar *overview_bar,
					  guint64            start,
					  MarlinWindow      *window)
{
	marlin_sample_view_scroll_to (window->priv->view, start);
}

/* Grr, annoyingly ugly prototype */
static void
marlin_window_view_move_cursor (MarlinSampleView *view,
				GtkMovementStep   step,
				int               count,
				gboolean          extend_selection,
				MarlinWindow     *window);
static void
marlin_window_overview_bar_cursor_changed (MarlinOverviewBar *overview_bar,
					   guint64            position,
					   MarlinWindow      *window)
{
	/* We need to turn off this signal, otherwise we get into
	   a loop of the overview_bar emitting the signal and setting the
	   view's cursor, which then gets back in here, and goes on */
	g_signal_handlers_block_matched (G_OBJECT (window->priv->view),
					 G_SIGNAL_MATCH_FUNC,
					 0, 0, NULL,
					 G_CALLBACK (marlin_window_view_move_cursor), NULL);

	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", position,
		      NULL);

	g_signal_handlers_unblock_matched (G_OBJECT (window->priv->view),
					   G_SIGNAL_MATCH_FUNC,
					   0, 0, NULL,
					   G_CALLBACK (marlin_window_view_move_cursor), NULL);
}

static void
marlin_window_view_move_cursor (MarlinSampleView *view,
				GtkMovementStep   step,
				int               count,
				gboolean          extend_selection,
				MarlinWindow     *window)
{
	guint64 view_pos;

	g_object_get (G_OBJECT (view),
		      "cursor_position", &view_pos,
		      NULL);

	/* Change the position in the status bar */
	statusbar_set_frames (window->priv->position_status,
			      window, (guint64) view_pos);

	/* We need to turn off this signal, otherwise we get into
	   a loop of the overview_bar emitting the signal and setting the
	   view's cursor, which then gets back in here, and goes on */
	g_signal_handlers_block_matched (G_OBJECT (window->priv->overview_bar),
					 G_SIGNAL_MATCH_FUNC,
					 0, 0, NULL,
					 G_CALLBACK (marlin_window_overview_bar_cursor_changed), NULL);

	g_object_set (G_OBJECT (window->priv->overview_bar),
		      "cursor_position", (guint64) view_pos,
		      NULL);
	g_signal_handlers_unblock_matched (G_OBJECT (window->priv->overview_bar),
					   G_SIGNAL_MATCH_FUNC,
					   0, 0, NULL,
					   G_CALLBACK (marlin_window_overview_bar_cursor_changed), NULL);
}

static void
play_eos (MarlinJack   *jack,
	  MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	priv->state = MARLIN_STATE_STOPPED;

	g_object_set (G_OBJECT (priv->view),
		      "show_play_cursor", FALSE,
		      NULL);

	marlin_window_move_cursor_to (window, priv->initial);

	gtk_vumeter_set_levels (GTK_VUMETER (priv->left_vu), 0.0, 0.0);
	gtk_vumeter_set_levels (GTK_VUMETER (priv->right_vu), 0.0, 0.0);
	set_path_sensitive (priv->ui_action, "Process", TRUE);
}

static void
position_changed (MarlinJack   *jack,
		  guint64       position,
		  MarlinWindow *window)
{
/* 	marlin_window_move_cursor_to (window, position); */
	g_object_set (G_OBJECT (window->priv->view),
		      "play_position", position,
		      NULL);
}

static void
change_position_display (GtkCheckMenuItem *item,
			 MarlinWindow     *window)
{
	MarlinWindowPrivate *priv = window->priv;
	GConfClient *client;

	if (gtk_check_menu_item_get_active (item)) {
		guint64 position, sel_start, sel_finish;

		priv->position_display = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "position-display"));

		client = gconf_client_get_default ();
		gconf_client_set_int (client,
				      "/apps/marlin/system-state/position-display",
				      priv->position_display, NULL);
		g_object_unref (G_OBJECT (client));

		/* Update the display */
		g_object_get (G_OBJECT (priv->view),
			      "cursor_position", &position,
			      NULL);

		statusbar_set_frames (priv->position_status, window, position);

		marlin_sample_selection_get (priv->selection, NULL,
					     &sel_start, &sel_finish);
		statusbar_set_frames (priv->sel_start_status, window, sel_start);
		statusbar_set_frames (priv->sel_end_status, window, sel_finish);
		g_object_set (G_OBJECT (window->priv->marker_view),
			      "display_type", window->priv->position_display,
			      NULL);
	}
}

static char *names[MARLIN_DISPLAY_NULL] = {
	N_("_Frames"),
	N_("_Time"),
	N_("_Seconds"),
	N_("T_ime & Frames"),
	N_("_Beats")
};

static void
attach_popup_menu (MarlinWindow *window,
		   GtkWidget    *widget)
{
	static GtkWidget *menu = NULL;
	MarlinDisplay pd;
	GSList *group = NULL;

	/* Share the one menu for all the status bars */
	if (menu == NULL) {
		menu = gtk_menu_new ();

		for (pd = MARLIN_DISPLAY_FRAMES; pd < MARLIN_DISPLAY_NULL; pd++) {
			GtkWidget *item;

			item = gtk_radio_menu_item_new_with_mnemonic (group, _(names[pd]));
			if (pd == window->priv->position_display) {
				gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), TRUE);
			}
			g_object_set_data (G_OBJECT (item), "position-display",
					   GINT_TO_POINTER (pd));

			group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (item));

			gtk_widget_show (item);
			gtk_menu_shell_append ((GtkMenuShell *) menu, item);
			g_signal_connect (G_OBJECT (item), "toggled",
					  G_CALLBACK (change_position_display), window);
		}
	}

	marlin_popup_menu_attach (menu, widget);
}

static gboolean
view_key_press (MarlinSampleView *view,
		GdkEventKey      *event,
		MarlinWindow     *window)
{
	MarlinWindowPrivate *priv = window->priv;
	MarlinUndoContext *ctxt;
	guint64 position;

	switch (event->keyval) {
	case GDK_space:
		switch (priv->state) {
		case MARLIN_STATE_PAUSED:
		case MARLIN_STATE_STOPPED:
			/* Repeat if Shift was held down as well */
			marlin_window_play_sample (window,
						   (event->state & GDK_SHIFT_MASK));
			break;

		case MARLIN_STATE_PLAYING:
			marlin_window_pause_sample (window);
			break;

		case MARLIN_STATE_RECORDING:
			/* FIXME: Is it ever possible for the state of a window
			   to be MARLIN_STATE_RECORDING? */
			break;

		default:
			break;
		}

		break;

	case GDK_m:
		/* Add a marker at the cursor position */
		/* FIXME: Check this is a valid operation */
		g_object_get (G_OBJECT (view),
			      "cursor_position", &position,
			      NULL);

		ctxt = marlin_undo_manager_context_begin (priv->undo,
							  _("Add Marker"));
		marlin_marker_model_add_marker (priv->marker_model,
						position, NULL, ctxt);
		marlin_undo_manager_context_end (priv->undo, ctxt);

		break;

	default:
/* 		g_print ("Other key pressed\n"); */
		return FALSE;
	}

	return TRUE;
}

static void
drag_data_received (GObject          *object,
		    GdkDragContext   *context,
		    int               x,
		    int               y,
		    GtkSelectionData *selection,
		    guint             info,
		    guint             time,
		    gpointer          data)
{
	MarlinWindow *window;
	char *tmp, *filename, **filenames;
	int i;

	window = MARLIN_WINDOW (object);

	switch (info) {
	case DND_TYPE_TEXT_URI_LIST:
		tmp = g_strndup ((char *) selection->data, selection->length);
		filenames = g_strsplit (tmp, "\n", 0);
		g_free (tmp);

		for (i = 0; filenames[i] != NULL; i++) {
			filename = g_strstrip (filenames[i]);

			if (filename[0] == 0) {
				continue;
			}

			marlin_open_window (filename + 7, NULL, NULL, NULL);
			g_free (filename);
		}

		g_free (filenames);
		break;

	default:
		break;
	}
}

static void
clip_changed (MarlinProgram *program,
	      MarlinWindow  *window)
{
	if (marlin_program_get_clipboard (program) == NULL) {
		marlin_window_set_edit_sensitive (window, FALSE);
		marlin_window_set_view_sensitive (window, FALSE);
		marlin_window_set_plugins_sensitive (window, FALSE);
	} else {
		marlin_window_set_edit_sensitive (window, TRUE);
		marlin_window_set_view_sensitive (window, TRUE);
		marlin_window_set_plugins_sensitive (window, TRUE);
	}
}

/* Attach a popup menu from the UI xml to a widget */
static void
attach_menu (MarlinWindow *window,
	     GtkWidget    *widget,
	     const char   *menu_name)
{
	GtkWidget *menu;

	menu = gtk_ui_manager_get_widget (GTK_UI_MANAGER (window->priv->ui_merge),
							  menu_name);

	if (menu == NULL) {
		g_warning ("%s is not valid. Have you installed properly?",
			   menu_name);
		return;
	}

	marlin_popup_menu_attach (menu, widget);
}

static void
attach_popups (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	attach_menu (window, GTK_WIDGET (priv->view), "/ViewPopup");
	attach_menu (window, GTK_WIDGET (priv->vruler), "/LevelPopup");
	attach_menu (window, GTK_WIDGET (priv->marker_view), "/MarkerPopup");
	attach_menu (window, GTK_WIDGET (priv->meters_event), "/MetersPopup");
}

static void
marker_view_move_cursor (MarlinMarkerView *view,
			 guint64           position,
			 MarlinWindow     *window)
{
	marlin_window_move_cursor_to (window, position);
}

static void
overview_bar_play_request (MarlinOverviewBar *bar,
			   guint64            start,
			   MarlinWindow      *window)
{
	guint64 end;

	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &end,
		      NULL);

	marlin_window_play_range (window, start, end, FALSE);
}

static void
level_cb (MarlinJack   *jack,
	  int           channels,
	  double       *rms,
	  double       *peak,
	  MarlinWindow *window)
{
	GtkWidget *vumeter;
	MarlinWindowPrivate *priv;
	int i;

	priv = window->priv;

	for (i = 0; i < channels; i++) {
		vumeter = (i == 0) ? priv->left_vu : priv->right_vu;
		gtk_vumeter_set_levels (GTK_VUMETER (vumeter), rms[i], peak[i]);
	}
}

void
marlin_window_set_icon (MarlinWindow *window,
			GdkPixbuf    *icon)
{
	if (window_icon == NULL) {
		char *filename;
		filename = marlin_file ("marlin/marlin-icon.png");

		window_icon = gdk_pixbuf_new_from_file (filename, NULL);
		g_free (filename);
	}

	if (icon == NULL) {
		icon = window_icon;
	}

	gtk_window_set_icon (GTK_WINDOW (window), icon);
}

static void
baseline_changed (MarlinLevelRuler *ruler,
		  int               base_offset,
		  MarlinWindow     *window)
{
	g_object_set (G_OBJECT (window->priv->view),
		      "base_offset", base_offset,
		      NULL);
}

static void
marker_view_enter_marker (MarlinMarkerView *marker_view,
			  MarlinMarker     *marker,
			  MarlinWindow     *window)
{
	window->priv->mv_in_marker = TRUE;
	window->priv->current_marker = marker;

	set_marker_popup_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

static void
marker_view_leave_marker (MarlinMarkerView *marker_view,
			  MarlinWindow *window)
{
	window->priv->mv_in_marker = FALSE;
	window->priv->current_marker = NULL;

	set_marker_popup_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

static void
display_no_jack_window (GtkWidget *window)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new_with_markup (GTK_WINDOW (window),
						     GTK_DIALOG_MODAL |
						     GTK_DIALOG_DESTROY_WITH_PARENT,
						     GTK_MESSAGE_WARNING,
						     GTK_BUTTONS_OK,
						     _("<b>Unable to start JACK</b>"));
	gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
						  _("Marlin requires JACK for its audio output, but JACK failed to start.\n\n"
						    "Marlin will continue to run, but playback and recording features will be disabled"));
	gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);
}

static void
jack_owner_changed_cb (MarlinProgram *program,
		       guint          jack_owner,
		       MarlinWindow  *window)
{
	MarlinWindowPrivate *priv = window->priv;
	gboolean own_jack = (jack_owner == priv->jack_owner_id);

	/* If we are the jack owner then turn on the
	   play/pause/record actions */
	marlin_window_set_jack_sensitive (window, own_jack);

#if 0
	/* If we don't own jack, we don't want to know about any of
	   its events. We'll just get confused */
	if (own_jack) {
		g_signal_handler_unblock (priv->jack, priv->eos_id);
		g_signal_handler_unblock (priv->jack, priv->level_id);
		g_signal_handler_unblock (priv->jack, priv->position_id);
	} else {
		g_signal_handler_block (priv->jack, priv->eos_id);
		g_signal_handler_block (priv->jack, priv->level_id);
		g_signal_handler_block (priv->jack, priv->position_id);
	}
#endif
}

MarlinWindow *
marlin_window_new (void)
{
	MarlinProgram *program = marlin_program_get_default ();
	MarlinWindow *window;
	MarlinWindowPrivate *priv;
	GtkWidget *sw, *table, *ev, *statusbar, *vu;
	GtkAction *action;
	guint64 frames;
	GConfClient *client = marlin_gconf_get_default ();
	gboolean snap;
	MarlinScale scale;

	window = g_object_new (MARLIN_WINDOW_TYPE, NULL);
	priv = window->priv;

	gtk_window_set_default_size (GTK_WINDOW (window),
				     DEFAULT_WIDTH, DEFAULT_HEIGHT);

	/* DND support */
	gtk_drag_dest_set (GTK_WIDGET (window), GTK_DEST_DEFAULT_ALL,
			   drop_types, num_drop_types, GDK_ACTION_COPY);
	g_signal_connect (G_OBJECT (window), "drag_data_received",
			  G_CALLBACK (drag_data_received), NULL);

	frames = (guint64) 0;
	priv->sample = NULL;
	priv->selection = NULL;
	priv->marker_model = NULL;

	priv->overview_bar = g_object_new (MARLIN_OVERVIEW_BAR_TYPE,
					   "frames_per_page", (guint64) (4096 * DEFAULT_WIDTH),
					   NULL);
	gtk_widget_show (GTK_WIDGET (priv->overview_bar));

	g_signal_connect (G_OBJECT (priv->overview_bar), "page-start-changed",
			  G_CALLBACK (marlin_window_overview_bar_start_changed),
			  window);
	g_signal_connect (G_OBJECT (priv->overview_bar), "cursor-changed",
			  G_CALLBACK (marlin_window_overview_bar_cursor_changed),
			  window);
	g_signal_connect (G_OBJECT (priv->overview_bar), "play-request",
			  G_CALLBACK (overview_bar_play_request), window);

	gtk_box_pack_start (GTK_BOX (priv->main_vbox),
			    GTK_WIDGET (priv->overview_bar),
			    FALSE, FALSE, 0);

	/* 3 rows, 3 columns:
	                  | Horizontal ruler |
	   vertical ruler | sample view      | VU
	                  | Scrollbar.       | ^^
	*/

	table = gtk_table_new (3, 3, FALSE);
	gtk_box_pack_start (GTK_BOX (priv->main_vbox), table, TRUE, TRUE, 0);

	snap = gconf_client_get_bool (client, "/apps/marlin/marker-view-snap-to-ticks", NULL);
	priv->marker_view = g_object_new (MARLIN_MARKER_VIEW_TYPE,
					  "frames_per_pixel", 4096,
					  "snap_to_ticks", snap,
					  "display_type", priv->position_display,
					  "undo_manager", priv->undo,
					  NULL);
	g_signal_connect (G_OBJECT (priv->marker_view), "move-cursor",
			  G_CALLBACK (marker_view_move_cursor), window);
	g_signal_connect (G_OBJECT (priv->marker_view), "enter-marker",
			  G_CALLBACK (marker_view_enter_marker), window);
	g_signal_connect (G_OBJECT (priv->marker_view), "leave-marker",
			  G_CALLBACK (marker_view_leave_marker), window);

	gtk_table_attach (GTK_TABLE (table), priv->marker_view,
			  1, 2, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL,
			  0, 0);

	scale = marlin_gconf_get_int ("/apps/marlin/scale-display");
	priv->vruler = g_object_new (MARLIN_LEVEL_RULER_TYPE,
				     "scale", scale,
				     NULL);
	g_signal_connect (priv->vruler, "baseline-changed",
			  G_CALLBACK (baseline_changed), window);
	gtk_table_attach (GTK_TABLE (table), priv->vruler,
			  0, 1, 1, 2,
			  GTK_FILL,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	/* Create the scrollbar here so we can use it's adjustment for
	   the scrolled window */
	priv->hscrollbar = gtk_hscrollbar_new (NULL);

	sw = gtk_scrolled_window_new (GTK_RANGE (priv->hscrollbar)->adjustment, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_NEVER,
					GTK_POLICY_NEVER);

	/* Set the adjustments on the marker view */
	gtk_widget_set_scroll_adjustments (GTK_WIDGET (priv->marker_view),
					   GTK_RANGE (priv->hscrollbar)->adjustment,
					   GTK_RANGE (GTK_SCROLLED_WINDOW (sw)->vscrollbar)->adjustment);

	priv->view = g_object_new (MARLIN_SAMPLE_VIEW_TYPE,
				   "frames_per_pixel", 4096,
				   "undo_manager", priv->undo,
				   NULL);

	g_signal_connect (G_OBJECT (priv->view), "key-press-event",
			  G_CALLBACK (view_key_press), window);
	gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (priv->view));

	gtk_table_attach (GTK_TABLE (table), sw,
			  1, 2, 1, 2,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	g_signal_connect (G_OBJECT (priv->view), "frames-per-pixel-changed",
			  G_CALLBACK (marlin_window_change_overview_bar_fpp),
			  window);
	g_signal_connect (G_OBJECT (priv->view), "page-start-changed",
			  G_CALLBACK (marlin_window_page_start_changed),
			  window);
	g_signal_connect (G_OBJECT (priv->view), "size-allocate",
			  G_CALLBACK (marlin_window_view_size_allocate),
			  window);
	g_signal_connect (G_OBJECT (priv->view), "move-cursor",
			  G_CALLBACK (marlin_window_view_move_cursor),
			  window);
	g_signal_connect (G_OBJECT (priv->view), "vzoom-changed",
			  G_CALLBACK (view_vzoom_changed), window);

	gtk_table_attach (GTK_TABLE (table), priv->hscrollbar,
			  1, 2, 2, 3,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL,
			  0, 0);

	priv->meters_event = gtk_event_box_new ();
	gtk_table_attach (GTK_TABLE (table), priv->meters_event,
			  2, 3, 0, 3,
			  GTK_FILL,
			  GTK_FILL,
			  0, 0);

	vu = gtk_hbox_new (TRUE, 6);
	gtk_container_add (GTK_CONTAINER (priv->meters_event), vu);

	priv->left_vu = gtk_vumeter_new (GTK_VUMETER_VERTICAL);
	g_object_set (G_OBJECT (priv->left_vu),
		      "scale", scale,
		      NULL);
	gtk_box_pack_start (GTK_BOX (vu), priv->left_vu, FALSE, FALSE, 0);

	priv->right_vu = gtk_vumeter_new (GTK_VUMETER_VERTICAL);
	g_object_set (G_OBJECT (priv->right_vu),
		      "scale", scale,
		      NULL);
	gtk_box_pack_start (GTK_BOX (vu), priv->right_vu, FALSE, FALSE, 0);

	gtk_widget_show_all (table);

	action = gtk_action_group_get_action (priv->ui_action,
					      "ViewEqualizers");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), priv->show_vu);

	statusbar = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (priv->main_vbox), statusbar, FALSE, FALSE, 0);
	gtk_widget_show (statusbar);

	/* The status bars */
	/* Message bar */
	priv->message_status = gtk_statusbar_new ();
	priv->help_message_cid = gtk_statusbar_get_context_id (GTK_STATUSBAR (priv->message_status), "help_message");

	gtk_widget_show (priv->message_status);
	gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (priv->message_status), FALSE);

	gtk_box_pack_start (GTK_BOX (statusbar), priv->message_status, TRUE, TRUE, 0);

	/* Cursor position */
	priv->position_status = gtk_statusbar_new ();
	gtk_widget_set_size_request (priv->position_status, 100, -1);

	statusbar_set_frames (priv->position_status, window, (guint64) 0);

	ev = gtk_event_box_new ();
	gtk_container_add (GTK_CONTAINER (ev), priv->position_status);
 	attach_popup_menu (window, ev);
	gtk_widget_show_all (ev);

	gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (priv->position_status), FALSE);

	gtk_box_pack_start (GTK_BOX (statusbar), ev, FALSE, FALSE, 0);

	/* Selection start */
	priv->sel_start_status = gtk_statusbar_new ();
	gtk_widget_set_size_request (priv->sel_start_status, 100, -1);

	ev = gtk_event_box_new ();
	gtk_container_add (GTK_CONTAINER (ev), priv->sel_start_status);
 	attach_popup_menu (window, ev);
	gtk_widget_show_all (ev);

	gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (priv->sel_start_status), FALSE);

	gtk_box_pack_start (GTK_BOX (statusbar), ev, FALSE, FALSE, 0);

	priv->sel_end_status = gtk_statusbar_new ();
	gtk_widget_set_size_request (priv->sel_end_status, 100, -1);

	ev = gtk_event_box_new ();
	gtk_container_add (GTK_CONTAINER (ev), priv->sel_end_status);
	attach_popup_menu (window, ev);
	gtk_widget_show_all (ev);

	gtk_box_pack_start (GTK_BOX (statusbar), ev, FALSE, FALSE, 0);

	marlin_window_set_file_sensitive (window, FALSE);
	marlin_window_set_media_bar_sensitive (window, FALSE);
	marlin_window_set_tools_sensitive (window, FALSE);
	marlin_window_set_view_sensitive (window, FALSE);
	marlin_window_set_edit_sensitive (window, FALSE);
	marlin_window_set_selection_sensitive (window, FALSE);
	marlin_window_set_process_sensitive (window, FALSE);
	marlin_window_set_plugins_sensitive (window, FALSE);
	marlin_window_set_process_name (window);

	set_marker_popup_sensitive (window, FALSE);

	/* Connect to the clipboard signals */
	priv->clip_id = g_signal_connect (G_OBJECT (program), "clipboard-changed",
					  G_CALLBACK (clip_changed), window);

	/* Add all the popups now that the window is made */
	attach_popups (window);

	gtk_widget_show (priv->main_vbox);

	priv->jack = (MarlinJackPlay *) marlin_program_get_player (program);

	if (priv->jack) {
		/* If we have jack, then we want to have an owner ID
		   and listen for when we gain or lose ownership */
		priv->jack_owner_id =
			marlin_program_request_jack_owner_id (program);
		priv->owner_changed_id =
			g_signal_connect (program, "jack-owner-changed",
					  G_CALLBACK (jack_owner_changed_cb),
					  window);

		priv->eos_id = g_signal_connect (priv->jack, "eos",
						 G_CALLBACK (play_eos), window);

		/* Turn on the level emissions */
		g_object_set (priv->jack,
			      "emit-level", TRUE,
			      NULL);
		priv->level_id = g_signal_connect (priv->jack, "level-changed",
						   G_CALLBACK (level_cb),
						   window);

		priv->position_id = g_signal_connect (priv->jack,
						      "position-changed",
						      G_CALLBACK (position_changed),
						      window);
	} else {
		display_no_jack_window (GTK_WIDGET (window));
		set_path_sensitive (priv->ui_action, "MediaRecord", FALSE);
	}

	/* Set icon after the no jack window has been shown */
	marlin_window_set_icon (window, NULL);

	return window;
}

static int
get_int (GConfClient *client,
	 const char  *root,
	 const char  *key)
{
	int ret;
	char *fullkey;

	fullkey = g_strdup_printf ("/apps/marlin/system-state/%s/%s", root, key);
	ret = gconf_client_get_int (client, fullkey, NULL);
	g_free (fullkey);

	return ret;
}

MarlinWindow *
marlin_window_new_from_state (MarlinSample *sample,
			      const char   *uuid)
{
	MarlinWindow *window;
	GConfClient *client;
	int x, y, width, height, cursor_position;

	window = marlin_window_new ();
	g_object_set (G_OBJECT (window),
		      "sample", sample,
		      NULL);

	client = gconf_client_get_default ();

	/* Restore state */
	x = get_int (client, uuid, "x");
	y = get_int (client, uuid, "y");
	width = get_int (client, uuid, "width");
	height = get_int (client, uuid, "height");

	gtk_window_move (GTK_WINDOW (window), x, y);
	gtk_window_resize (GTK_WINDOW (window), width, height);

	cursor_position = get_int (client, uuid, "cursor-position");
	g_object_set (G_OBJECT (window->priv->view),
		      "cursor-position", cursor_position,
		      NULL);

	g_object_unref (G_OBJECT (client));

	return window;
}

void
marlin_window_set_title (MarlinWindow *window,
			 MarlinSample *sample)
{
	char *title, *filename;
	gboolean dirty, writable;

	g_object_get (G_OBJECT (sample),
		      "name", &filename,
		      "dirty", &dirty,
		      "writable", &writable,
		      NULL);

	if (filename == NULL) {
		title = g_strdup (_("Marlin Sample Editor"));
	} else {
		title = g_strdup_printf ("%s%s%s - Marlin", filename,
					 writable ? "" : _(" (read only)"),
					 dirty ? _(" (modified)") : "");
	}

	g_free (filename);

	gtk_window_set_title (GTK_WINDOW (window), title);
	g_free (title);
}

static void
maybe_save_response (GtkDialog    *dialog,
		     guint         response_id,
		     MarlinWindow *window)
{
	switch (response_id) {
	case GTK_RESPONSE_NO:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		gtk_widget_destroy (GTK_WIDGET (window));
		return;

	case GTK_RESPONSE_CANCEL:
		break;

	case GTK_RESPONSE_OK:
		/* save */
		window->priv->close_on_op_complete = TRUE;
		marlin_window_save_sample_as (window);
		break;

	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static char *
time_to_str (time_t length)
{
	int mins;

	if (length < 60) {
		return g_strdup_printf (ngettext ("If you close without saving, changes from the last second will be discarded.",
						  "If you close without saving, changes from the last %d seconds will be discarded.", length), length);
	} else {
		mins = length / 60;

		return g_strdup_printf (ngettext ("If you close without saving, changes from the last minute will be discarded.",
						  "If you close without saving, changes from the last %d minutes will be discarded", mins), mins);
	}
}

static void
maybe_save_quit (MarlinWindow *window)
{
	GtkWidget *mess;
	char *title, *filename;
	char *basename, *time_str;
	char *bold;
	time_t now;

	g_object_get (G_OBJECT (window->priv->sample),
		      "filename", &filename,
		      NULL);

	now = time (NULL);
	time_str = time_to_str (now - window->priv->last_save);

	basename = g_path_get_basename (filename);

	bold = g_strdup_printf (_("Save changes made to \"%s\" before closing?"),
				basename);
	mess = gtk_message_dialog_new (GTK_WINDOW (window), 0,
				       GTK_MESSAGE_WARNING,
				       GTK_BUTTONS_NONE,
				       "<span weight=\"bold\" size=\"larger\">%s</span>\n%s",
				       bold, time_str);
	g_free (bold);
	g_signal_connect (G_OBJECT (mess), "response",
			  G_CALLBACK (maybe_save_response), window);

	marlin_add_button_to_dialog (GTK_DIALOG (mess), _("Do_n't save"), GTK_STOCK_NO, GTK_RESPONSE_NO);
	marlin_add_button_to_dialog (GTK_DIALOG (mess), _("Cancel"), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (mess), GTK_STOCK_SAVE, GTK_RESPONSE_OK);
	gtk_dialog_set_default_response (GTK_DIALOG (mess), GTK_RESPONSE_OK);

	title = g_strdup_printf (_("Save %s?"), filename);
	gtk_window_set_title (GTK_WINDOW (mess), title);
	g_free (title);
	g_free (basename);
	g_free (filename);
	g_free (time_str);

	/* Bad evil hack */
	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (mess)->label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (GTK_MESSAGE_DIALOG (mess)->label), FALSE);
	gtk_label_set_justify (GTK_LABEL (GTK_MESSAGE_DIALOG (mess)->label), GTK_JUSTIFY_LEFT);

	gtk_widget_show (mess);
}

gboolean
marlin_window_can_close (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	gboolean dirty;

	priv = window->priv;

	g_object_get (G_OBJECT (priv->sample),
		      "dirty", &dirty,
		      NULL);

	if (dirty) {
		maybe_save_quit (window);
	}

	return dirty;
}

void
marlin_window_close (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;

	g_return_if_fail (IS_MARLIN_WINDOW (window));

	priv = window->priv;

	/* FIXME: Should stop playing here */

	if (marlin_window_can_close (window) == FALSE) {
		gtk_widget_destroy (GTK_WIDGET (window));
	} /* Otherwise the save dialog will close the window */
}

GtkActionGroup *
marlin_window_get_action_group (MarlinWindow *window)
{
	g_return_val_if_fail (IS_MARLIN_WINDOW (window), NULL);

	return window->priv->ui_action;
}

GtkActionGroup *
marlin_window_get_popup_action_group (MarlinWindow *window)
{
	g_return_val_if_fail (IS_MARLIN_WINDOW (window), NULL);

	return window->priv->popup_action;
}

void
marlin_window_move_cursor_to (MarlinWindow *window,
			      guint64 absolute_position)
{
	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", absolute_position,
		      "play_position", absolute_position,
		      NULL);
}

void
marlin_window_move_cursor (MarlinWindow *window,
			   guint64 relative_position)
{
	guint64 position;
	guint fpp;

	g_object_get (G_OBJECT (window->priv->view),
		      "cursor_position", &position,
		      "frames_per_pixel", &fpp,
		      NULL);

	position += (relative_position * fpp);
	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", (guint64) position,
		      NULL);
}

void
marlin_window_move_cursor_to_start (MarlinWindow *window)
{
	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", (guint64) 0,
		      NULL);
}

void
marlin_window_move_cursor_to_end (MarlinWindow *window)
{
	guint fpp;
	guint64 total;

	g_object_get (G_OBJECT (window->priv->view),
		      "frames_per_pixel", &fpp,
		      NULL);
	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &total,
		      NULL);

	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", (guint64) (total - fpp),
		      NULL);
}


static void
set_path_sensitive (GtkActionGroup *ag,
		    const char     *name,
		    gboolean        sensitive)
{
	GtkAction *action;

	action = gtk_action_group_get_action (ag, name);
	g_return_if_fail (action != NULL);

	g_object_set (G_OBJECT (action), "sensitive", sensitive, NULL);
}

void
marlin_window_set_file_sensitive (MarlinWindow *window,
				  gboolean sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	gboolean dirty, writable;
	GtkActionGroup *ag;

	if (priv->sample) {
		g_object_get (G_OBJECT (priv->sample),
			      "dirty", &dirty,
			      "writable", &writable,
			      NULL);
	} else {
		dirty = FALSE;
		writable = FALSE;
	}

	ag = priv->ui_action;

	set_path_sensitive (ag, "FileSave", dirty && writable);
	set_path_sensitive (ag, "FileSaveAs", sensitive);
 	set_path_sensitive (ag, "FileProperties", sensitive);
}

void
marlin_window_set_jack_sensitive (MarlinWindow *window,
				  gboolean      sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	GtkActionGroup *ag;
	guint64 total_frames;

	ag = priv->ui_action;

	/* MediaRecord only depends on jack being present */
	if (priv->jack == FALSE) {
		sensitive = FALSE;
	}

	set_path_sensitive (priv->ui_action, "MediaRecord", sensitive);

	if (priv->sample) {
		g_object_get (priv->sample,
			      "total-frames", &total_frames,
			      NULL);
	} else {
		total_frames = 0;
	}

	/* The playback depends on how many frames there are */
	if (total_frames == 0) {
		sensitive = FALSE;
	}

	set_path_sensitive (ag, "MediaPlay", sensitive);
	set_path_sensitive (ag, "MediaRepeat", sensitive);
	set_path_sensitive (ag, "MediaPause", sensitive);
	set_path_sensitive (ag, "MediaStop", sensitive);
}

void
marlin_window_set_media_bar_sensitive (MarlinWindow *window,
				       gboolean      sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	GtkActionGroup *ag;

	ag = priv->ui_action;

	marlin_window_set_jack_sensitive (window, sensitive);

	set_path_sensitive (ag, "MediaPrevious", sensitive);
	set_path_sensitive (ag, "MediaNext", sensitive);
	set_path_sensitive (ag, "MediaForward", sensitive);
	set_path_sensitive (ag, "MediaRewind", sensitive);
}

void
marlin_window_set_view_sensitive (MarlinWindow *window,
				  gboolean sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinCoverage coverage;
	GtkActionGroup *ag;

	ag = priv->ui_action;

	if (sensitive == FALSE) {
		set_path_sensitive (ag, "ZoomIn", FALSE);
		set_path_sensitive (ag, "ZoomOut", FALSE);
		set_path_sensitive (ag, "ViewVZoomIn", FALSE);
		set_path_sensitive (ag, "ViewVZoomOut", FALSE);
	} else {
		set_path_sensitive (ag, "ZoomIn",
				    marlin_sample_view_can_zoom_in (priv->view));
		set_path_sensitive (ag, "ZoomOut",
				    marlin_sample_view_can_zoom_out (priv->view));
		set_path_sensitive (ag, "ViewVZoomIn",
				    marlin_sample_view_can_vzoom_in (priv->view));
		set_path_sensitive (ag, "ViewVZoomOut",
				    marlin_sample_view_can_vzoom_out (priv->view));
	}


	if (priv->selection == NULL) {
		set_path_sensitive (ag, "ShowAll", FALSE);
		set_path_sensitive (ag, "ViewCentre", FALSE);
		coverage = MARLIN_COVERAGE_NONE;
	} else {
		set_path_sensitive (ag, "ShowAll", sensitive);
		set_path_sensitive (ag, "ViewCentre", sensitive);

		marlin_sample_selection_get (priv->selection,
					     &coverage,
					     NULL, NULL);
	}

	if (coverage == MARLIN_COVERAGE_NONE) {
		set_path_sensitive (ag, "ZoomSelection", FALSE);
	} else {
		set_path_sensitive (ag, "ZoomSelection", sensitive);
	}

	if (marlin_program_get_clipboard (program) == NULL) {
		set_path_sensitive (ag, "ViewClipboard", FALSE);
	} else {
		set_path_sensitive (ag, "ViewClipboard", sensitive);
	}
}

void
marlin_window_set_edit_sensitive (MarlinWindow *window,
				  gboolean sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	MarlinProgram *program = marlin_program_get_default ();
	GtkActionGroup *ag;
	gboolean can_remove;
	MarlinCoverage coverage;

	ag = priv->ui_action;

	if (priv->selection == NULL) {
		set_path_sensitive (ag, "EditMoveCursor", FALSE);
		set_path_sensitive (ag, "ZeroMovePrev", FALSE);
		set_path_sensitive (ag, "ZeroMoveNext", FALSE);

		coverage = MARLIN_COVERAGE_NONE;
	} else {
		set_path_sensitive (ag, "EditMoveCursor", sensitive);
		set_path_sensitive (ag, "ZeroMovePrev", sensitive);
		set_path_sensitive (ag, "ZeroMoveNext", sensitive);

		marlin_sample_selection_get (priv->selection,
					     &coverage, NULL, NULL);
	}

	if (coverage == MARLIN_COVERAGE_NONE) {
		set_path_sensitive (ag, "EditDelete", FALSE);
		set_path_sensitive (ag, "EditCopy", FALSE);
		set_path_sensitive (ag, "EditCut", FALSE);
		set_path_sensitive (ag, "EditCrop", FALSE);
	} else {
		can_remove = (coverage == MARLIN_COVERAGE_BOTH);
		set_path_sensitive (ag, "EditDelete", can_remove);
		set_path_sensitive (ag, "EditCopy", sensitive);
		set_path_sensitive (ag, "EditCut", can_remove);
		set_path_sensitive (ag, "EditCrop", can_remove);
	}

	if (marlin_program_get_clipboard (program) == NULL) {
		set_path_sensitive (ag, "EditPasteNew", FALSE);
		set_path_sensitive (ag, "EditPaste", FALSE);
		set_path_sensitive (ag, "EditPasteMix", FALSE);
		set_path_sensitive (ag, "EditReplace", FALSE);
		set_path_sensitive (ag, "EditPasteXFade", FALSE);
	} else {
		set_path_sensitive (ag, "EditPasteNew", sensitive);
		set_path_sensitive (ag, "EditPaste", sensitive);
		set_path_sensitive (ag, "EditPasteMix", sensitive);
		set_path_sensitive (ag, "EditReplace", sensitive);
		set_path_sensitive (ag, "EditPasteXFade", sensitive);
	}

	if (priv->undo) {
		if (marlin_undo_manager_can_undo (priv->undo)) {
			set_path_sensitive (ag, "EditUndo", sensitive);
		} else {
			set_path_sensitive (ag, "EditUndo", FALSE);
		}

		if (marlin_undo_manager_can_redo (priv->undo)) {
			set_path_sensitive (ag, "EditRedo", sensitive);
		} else {
			set_path_sensitive (ag, "EditRedo", FALSE);
		}
	}
}

void
marlin_window_set_selection_sensitive (MarlinWindow *window,
				       gboolean sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	GtkActionGroup *ag;
	MarlinCoverage coverage;

	ag = priv->ui_action;

	if (priv->selection == NULL) {
		set_path_sensitive (ag, "SelectAll", FALSE);
		set_path_sensitive (ag, "SelectRegion", FALSE);
		coverage = MARLIN_COVERAGE_NONE;
	} else {
		set_path_sensitive (ag, "SelectAll", sensitive);
		set_path_sensitive (ag, "SelectRegion", sensitive);
		marlin_sample_selection_get (priv->selection,
					     &coverage,
					     NULL, NULL);
	}

	if (coverage == MARLIN_COVERAGE_NONE) {
		set_path_sensitive (ag, "SelectNone", FALSE);
		set_path_sensitive (ag, "SelectHalve", FALSE);
		set_path_sensitive (ag, "SelectDouble", FALSE);
		set_path_sensitive (ag, "SelectLeft", FALSE);
		set_path_sensitive (ag, "SelectRight", FALSE);
		set_path_sensitive (ag, "SelectSnapToZero", FALSE);
	} else {
		set_path_sensitive (ag, "SelectNone", sensitive);
		set_path_sensitive (ag, "SelectHalve", TRUE);
		set_path_sensitive (ag, "SelectDouble", TRUE);
		set_path_sensitive (ag, "SelectLeft", TRUE);
		set_path_sensitive (ag, "SelectRight", TRUE);
		set_path_sensitive (ag, "SelectSnapToZero", TRUE);
	}
}

void
marlin_window_set_tools_sensitive (MarlinWindow *window,
				   gboolean sensitive)
{
	GtkActionGroup *ag;

	ag = window->priv->ui_action;
	/* FIXME ? */
}

void
marlin_window_set_process_sensitive (MarlinWindow *window,
				     gboolean sensitive)
{
	MarlinWindowPrivate *priv = window->priv;
	GtkActionGroup *ag;
	guint channels;
	MarlinMarkerModel *model;
	GList *markers;

	ag = priv->ui_action;

	if (window->priv->selection == NULL) {
		set_path_sensitive (ag, "ProcessMute", FALSE);
		set_path_sensitive (ag, "ProcessInvert", FALSE);
		set_path_sensitive (ag, "ProcessVolume", FALSE);
		set_path_sensitive (ag, "ProcessReverse", FALSE);
	} else {
		set_path_sensitive (ag, "ProcessMute", sensitive);
		set_path_sensitive (ag, "ProcessInvert", sensitive);
		set_path_sensitive (ag, "ProcessVolume", sensitive);
		set_path_sensitive (ag, "ProcessReverse", sensitive);
	}

	if (priv->sample) {
		g_object_get (G_OBJECT (priv->sample),
			      "channels", &channels,
			      "markers", &model,
			      NULL);

		g_object_get (G_OBJECT (model),
			      "markers", &markers,
			      NULL);
		g_object_unref (model);
	} else {
		channels = 1;
		markers = NULL;
	}

	if (channels != 2) {
		set_path_sensitive (ag, "ProcessSwapChannels", FALSE);
	} else {
		set_path_sensitive (ag, "ProcessSwapChannels", sensitive);
	}

	set_path_sensitive (ag, "ProcessAdjustChannels", TRUE);

	if (markers == NULL) {
		set_path_sensitive (ag, "ProcessSplitSample", FALSE);
	} else {
		set_path_sensitive (ag, "ProcessSplitSample", sensitive);
	}
}

void
marlin_window_set_plugins_sensitive (MarlinWindow *window,
				     gboolean sensitive)
{
	GList *plugins, *p;

	plugins = marlin_plugin_get_list ();
	for (p = plugins; p; p = p->next) {
		MarlinPluginInfo *info = p->data;
		MarlinPluginFuncs *funcs = info->funcs;

		if (funcs->set_sensitive) {
			funcs->set_sensitive (GTK_UI_MANAGER (window->priv->ui_merge),
					      MARLIN_BASE_WINDOW (window),
					      sensitive);
		}
	}
}

void
marlin_window_set_plugins_name (MarlinWindow *window)
{
	GList *plugins, *p;

	plugins = marlin_plugin_get_list ();
	for (p = plugins; p; p = p->next) {
		MarlinPluginInfo *info = p->data;
		MarlinPluginFuncs *funcs = info->funcs;

		if (funcs->set_name) {
			funcs->set_name (GTK_UI_MANAGER (window->priv->ui_merge),
					 MARLIN_BASE_WINDOW (window));
		}
	}
}

static void
set_marker_popup_sensitive (MarlinWindow *window,
			    gboolean sensitive)
{
	GtkActionGroup *ag;

	ag = window->priv->popup_action;

	if (window->priv->mv_in_marker) {
		set_path_sensitive (ag, "MarkerDelete", sensitive);
		set_path_sensitive (ag, "MarkerGoto", sensitive);
		set_path_sensitive (ag, "MarkerEdit", sensitive);
	} else {
		set_path_sensitive (ag, "MarkerDelete", FALSE);
		set_path_sensitive (ag, "MarkerGoto", FALSE);
		set_path_sensitive (ag, "MarkerEdit", FALSE);
	}

	if (window->priv->selection) {
		set_path_sensitive (ag, "MarkerAdd", sensitive);
	} else {
		set_path_sensitive (ag, "MarkerAdd", FALSE);
	}
}

static void
set_path_name (GtkActionGroup *ag,
	       const char *path,
	       const char *text)
{
	GtkAction *action;

	action = gtk_action_group_get_action (ag, path);
	g_object_set (action, "label", text, NULL);
}

static void
set_path_tooltip (GtkActionGroup *ag,
		  const char *path,
		  const char *text)
{
	GtkAction *action;

	action = gtk_action_group_get_action (ag, path);
	g_object_set (action, "tooltip", text, NULL);
}

void
marlin_window_set_process_name (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	GtkActionGroup *ag;
	MarlinCoverage coverage;
	int num_chans;

	ag = priv->ui_action;

	if (priv->selection) {
		marlin_sample_selection_get (priv->selection, &coverage,
					     NULL, NULL);
	} else {
		coverage = MARLIN_COVERAGE_NONE;
	}

	if (coverage == MARLIN_COVERAGE_NONE) {
		/* No selection */
		set_path_name (ag, "ProcessMute", _("Mute Sample"));
		set_path_name (ag, "ProcessInvert", _("Invert Sample"));
		set_path_name (ag, "ProcessVolume", _("Adjust Sample Volume..."));
		set_path_name (ag, "ProcessReverse", _("Reverse Sample"));
		set_path_name (ag, "ProcessNormalize", _("Normalize Sample"));
	} else {
		/* Selection */
		set_path_name (ag, "ProcessMute", _("Mute Selection"));
		set_path_name (ag, "ProcessInvert", _("Invert Selection"));
		set_path_name (ag, "ProcessVolume", _("Adjust Selection Volume..."));
		set_path_name (ag, "ProcessReverse", _("Reverse Selection"));
		set_path_name (ag, "ProcessNormalize", _("Normalize Selection"));
	}

	if (priv->sample) {
		g_object_get (G_OBJECT (priv->sample),
			      "channels", &num_chans,
			      NULL);
	} else {
		num_chans = 1;
	}

	if (num_chans == 1) {
		set_path_name (ag, "ProcessAdjustChannels", _("Add a Channel..."));
	} else {
		set_path_name (ag, "ProcessAdjustChannels", _("Remove a Channel..."));
	}
}

void
marlin_window_set_edit_name (MarlinWindow *window)
{
	GtkActionGroup *ag;
	MarlinCoverage coverage;

	ag = window->priv->ui_action;

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     NULL, NULL);
	if (coverage == MARLIN_COVERAGE_NONE) {
		/* No selection */
		set_path_name (ag, "EditPasteMix", _("Mix..."));
		set_path_name (ag, "EditReplace", _("Replace"));
	} else {
		/* Selection */
		set_path_name (ag, "EditPasteMix", _("Mix into Selection..."));
		set_path_name (ag, "EditReplace", _("Replace Selection"));
	}

#if 0
	if (marlin_undo_manager_can_undo (window->priv->undo)) {
		char *name;

		name = g_strdup_printf (_("Undo '%s'"),
					  marlin_undo_manager_get_undo_name (window->priv->undo));
		set_path_name (ag, "EditUndo", name);
		set_path_tooltip (ag, "EditUndo", name);
		g_free (name);
	} else {
		set_path_name (ag, "EditUndo", _("Undo"));
		set_path_tooltip (ag, "EditUndo", _("Undo the previous action"));
	}
#endif

	if (marlin_undo_manager_can_redo (window->priv->undo)) {
		char *name;

		name = g_strdup_printf (_("Redo '%s'"),
					  marlin_undo_manager_get_redo_name (window->priv->undo));
		set_path_name (ag, "EditRedo", name);
		set_path_tooltip (ag, "EditRedo", name);
		g_free (name);
	} else {
		set_path_name (ag, "EditRedo", _("Redo"));
		set_path_tooltip (ag, "EditRedo", _("Redo the last action"));
	}
}

MarlinSample *
marlin_window_get_sample (MarlinWindow *window)
{
	g_return_val_if_fail (IS_MARLIN_WINDOW (window), NULL);

	return window->priv->sample;
}

void
marlin_window_play_range (MarlinWindow *window,
			  guint64       start,
			  guint64       finish,
			  gboolean      repeating)
{
	MarlinWindowPrivate *priv;
	MarlinProgram *program = marlin_program_get_default ();

	g_return_if_fail (IS_MARLIN_WINDOW (window));
	priv = window->priv;

	if (priv->jack_owner_id != marlin_program_get_jack_owner (program)) {
		g_print ("Not jack owner\n");
		return;
	}

	/* Turn off processing */
	set_path_sensitive (priv->ui_action, "Process", FALSE);

	if (priv->state == MARLIN_STATE_PLAYING) {
		priv->state = MARLIN_STATE_PAUSED;
	}

	priv->start = start;
	priv->finish = finish;

	g_object_set (G_OBJECT (priv->view),
		      "show_play_cursor", TRUE,
		      NULL);

	g_object_set (priv->jack,
		      "sample", priv->sample,
		      NULL);
	marlin_jack_play_set_repeating (priv->jack, repeating);
	marlin_jack_play_set_range (priv->jack, start, finish);
	marlin_jack_start (MARLIN_JACK (priv->jack), NULL);

	priv->state = MARLIN_STATE_PLAYING;
}

void
marlin_window_play_sample (MarlinWindow *window,
			   gboolean      repeating)
{
	MarlinWindowPrivate *priv;
	guint64 start, finish;
	MarlinCoverage coverage;

	g_return_if_fail (IS_MARLIN_WINDOW (window));

	priv = window->priv;

	if (priv->state == MARLIN_STATE_PLAYING) {
		return;
	} else if (priv->state == MARLIN_STATE_PAUSED) {
		priv->state = MARLIN_STATE_PLAYING;

 		marlin_jack_continue (MARLIN_JACK (priv->jack));
		return;
	}

	/* Check if we've got a selection */
	marlin_sample_selection_get (priv->selection, &coverage,
				     &start, &finish);
	if (coverage != MARLIN_COVERAGE_NONE) {
		priv->initial = start;
	} else {
		g_object_get (G_OBJECT (priv->view),
			      "cursor_position", &priv->initial,
			      NULL);
		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &finish,
			      NULL);
		start = priv->initial;
	}

	marlin_window_play_range (window, start, finish, repeating);
}

void
marlin_window_stop_sample (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;

	priv = window->priv;

	set_path_sensitive (priv->ui_action, "Process", TRUE);
	if (priv->state == MARLIN_STATE_STOPPED) {
		return;
	}

	priv->state = MARLIN_STATE_STOPPED;

	marlin_jack_stop (MARLIN_JACK (priv->jack));

	g_object_set (G_OBJECT (priv->view),
		      "show_play_cursor", FALSE,
		      NULL);

	/* Reset the position to the initial */
	marlin_window_move_cursor_to (window, priv->initial);
	priv->repeating = FALSE;

	/* Reset the VUMeters */
	if (priv->left_vu) {
		gtk_vumeter_set_levels (GTK_VUMETER (priv->left_vu), 0.0, 0.0);
	}
	if (priv->right_vu) {
		gtk_vumeter_set_levels (GTK_VUMETER (priv->right_vu), 0.0, 0.0);
	}
}

void
marlin_window_pause_sample (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;

	priv = window->priv;

	if (priv->state == MARLIN_STATE_PAUSED) {
		return;
	}

	priv->state = MARLIN_STATE_PAUSED;
	marlin_jack_pause (MARLIN_JACK (priv->jack));
}

void
marlin_window_resume_sample (MarlinWindow *window)
{
}

void
marlin_window_sample_completed (MarlinWindow *window)
{
	if (window->priv->repeating) {
		window->priv->state = MARLIN_STATE_PLAYING;
	} else {
		marlin_window_stop_sample (window);
	}
}

#if 0
/* Save yourself :) */
static void
set_int (GConfClient *client,
	 const char *root,
	 const char *key,
	 int value)
{
	char *fullkey;

	fullkey = g_strdup_printf ("%s/%s", root, key);
	gconf_client_set_int (client, fullkey, value, NULL);
	g_free (fullkey);
}

void
marlin_window_save_state (MarlinWindow *window,
			  gpointer closure)
{
	uuid_t uuid;
	char uid[100];
	char *gconf_root, *window_root, *key;
	GConfClient *client;
	GConfValue *value, *window_value;
	GSList *window_id_list;

	int x, y, width, height, cursor;
	char *filename;

	client = gconf_client_get_default ();

	if (client == NULL) {
		g_warning ("Cannot save session");
		return;
	}

	/* Make a unique ID for this window */
	uuid_generate (uuid);
	uuid_unparse (uuid, uid);

	gconf_root = "/apps/marlin/system-state/";

	key = g_strdup_printf ("%s%s", gconf_root, "window-list");
	window_value = gconf_client_get (client, key, NULL);

	if (window_value == NULL) {

		/* Create a new LIST value */
		window_value = gconf_value_new (GCONF_VALUE_LIST);
		gconf_value_set_list_type (window_value, GCONF_VALUE_STRING);
		window_id_list = NULL;
	} else {
		window_id_list = gconf_value_get_list (window_value);
	}

	/* Create a new string */
	value = gconf_value_new (GCONF_VALUE_STRING);
	gconf_value_set_string (value, uid);

	/* Stick it at the start of the list */
	window_id_list = g_slist_prepend (window_id_list, value);

	/* Set it back in the value */

	gconf_value_set_list (window_value, window_id_list);
	gconf_client_set (client, key, window_value, NULL);
	g_free (key);

	/* Now store our state into the window_root */
	window_root = g_strdup_printf ("%s%s", gconf_root, uid);

	/* Filename */
	g_object_get (G_OBJECT (window->priv->sample),
		      "filename", &filename,
		      NULL);
	key = g_strdup_printf ("%s/filename", window_root);
	gconf_client_set_string (client, key, filename, NULL);
	g_free (key);

	/* Geometry */
	gtk_window_get_position (GTK_WINDOW (window), &x, &y);
	gtk_window_get_size (GTK_WINDOW (window), &width, &height);

	set_int (client, window_root, "x", x);
	set_int (client, window_root, "y", y);
	set_int (client, window_root, "width", width);
	set_int (client, window_root, "height", height);

	/* Cursor position */
	g_object_get (G_OBJECT (window->priv->view),
		      "cursor_position", &cursor,
		      NULL);

	set_int (client, window_root, "cursor-position", cursor);
	/* Save the selection list here */

	g_free (window_root);
	g_object_unref (G_OBJECT (client));
}

static void
remove_key (GConfClient *client,
	    const char *root,
	    const char *key)
{
	char *fullkey;

	fullkey = g_strdup_printf ("/apps/marlin/system-state/%s/%s", root, key);
	gconf_client_unset (client, fullkey, NULL);
	g_free (fullkey);
}

void
marlin_window_remove_state (const char *state_root)
{
	GConfClient *client;

	client = gconf_client_get_default ();

	remove_key (client, state_root, "filename");
	remove_key (client, state_root, "x");
	remove_key (client, state_root, "y");
	remove_key (client, state_root, "width");
	remove_key (client, state_root, "height");
	remove_key (client, state_root, "cursor-position");

	g_object_unref (G_OBJECT (client));
}

#endif

struct _load_op_data {
	MarlinOperation *op;
	MarlinPipeline *pipeline;
	MarlinWindow *window;
};

static void
load_op_started (MarlinOperation *operation,
		 struct _load_op_data *lod)
{
	MarlinWindow *window = lod->window;
	MarlinSample *sample = window->priv->sample;
	char *filename, *text, *tmp;

	marlin_window_set_media_bar_sensitive (window, FALSE);
	marlin_window_set_plugins_sensitive (window, FALSE);

	g_object_get (G_OBJECT (sample),
		      "filename", &filename,
		      NULL);
	text = g_strdup_printf (_("Opening %s"), filename);
	g_free (filename);

	tmp = g_strdup_printf ("<span weight=\"bold\">%s</span>\n%s",
			       _("Opening file"), text);
	g_free (text);
	window->priv->progress_window = marlin_progress_dialog_new (operation,
								    GTK_WINDOW (window),
								    tmp, NULL);
	g_free (tmp);

	gtk_widget_show (GTK_WIDGET (window->priv->progress_window));
}

static void
load_op_finished (MarlinOperation *operation,
		  struct _load_op_data *lod)
{
	MarlinWindow *window = lod->window;
	MarlinSample *sample;

	sample = window->priv->sample;

	/* FIXME: This unref is needed because starting the operation
	   refs the sample. Should this ref/unref pair belong elsewhere? */
	g_object_unref (G_OBJECT (sample));

	gtk_widget_destroy (GTK_WIDGET (window->priv->progress_window));
	window->priv->progress_window = NULL;

	marlin_window_set_title (window, sample);

	marlin_window_set_file_sensitive (window, TRUE);
	marlin_window_set_media_bar_sensitive (window, TRUE);
	marlin_window_set_tools_sensitive (window, TRUE);
	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_edit_sensitive (window, TRUE);
	marlin_window_set_selection_sensitive (window, TRUE);
	marlin_window_set_process_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
	marlin_window_set_process_name (window);

	set_marker_popup_sensitive (window, TRUE);

	if (window->priv->close_on_op_complete) {
		gtk_widget_destroy (GTK_WIDGET (window));
	}

	g_object_set (G_OBJECT (window->priv->overview_bar),
		      "sample", sample,
		      NULL);
	g_object_set (G_OBJECT (window->priv->view),
		      "sample", sample,
		      NULL);

	/* Kill the pipeline */
  	gst_element_set_state (GST_ELEMENT (lod->pipeline), GST_STATE_NULL);
	g_object_unref (lod->pipeline);
	g_free (lod);
}

static void
load_op_cancelled (MarlinOperation *operation,
		   struct _load_op_data *lod)
{
	MarlinWindow *window = lod->window;
	MarlinSample *sample;

	if (window->priv->progress_window) {
 		gtk_widget_destroy (GTK_WIDGET (window->priv->progress_window));
		window->priv->progress_window = NULL;
	}

	sample = g_object_new (MARLIN_SAMPLE_TYPE,
			       "filename", "untitled",
			       "channels", 2,
			       "sample-rate", 44100,
			       NULL);

	g_object_set (G_OBJECT (window),
		      "sample", sample,
		      NULL);

	/* Samples need to be unreffed once they're attached to a window */
	g_object_unref (G_OBJECT (sample));

	gst_element_set_state (GST_ELEMENT (lod->pipeline), GST_STATE_NULL);
	g_object_unref (lod->pipeline);
	g_free (lod);
}

static void
load_op_error (MarlinOperation *operation,
	       GError *error,
	       const char *debug_message,
	       struct _load_op_data *lod)
{
	MarlinWindow *window = lod->window;
	MarlinSample *sample;
	GtkWidget *dialog;
	char *markup, *message, *filename;

	if (window->priv->progress_window) {
 		gtk_widget_destroy (GTK_WIDGET (window->priv->progress_window));
		window->priv->progress_window = NULL;
	}

	g_object_get (G_OBJECT (window->priv->sample),
		      "filename", &filename,
		      NULL);

	markup = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n%s",
				  _("Could not load '%s'"),
				  error->message);
	message = g_strdup_printf (markup, filename);
	g_free (markup);
	g_free (filename);

	dialog = gtk_message_dialog_new_with_markup (NULL, 0,
						     GTK_MESSAGE_ERROR,
						     GTK_BUTTONS_OK,
						     message);

	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), FALSE);

	g_free (message);

	g_signal_connect (dialog, "response",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);

	sample = g_object_new (MARLIN_SAMPLE_TYPE,
			       "filename", "untitled",
			       "channels", 2,
			       "sample-rate", 44100,
			       NULL);

	g_object_set (G_OBJECT (window),
		      "sample", sample,
		      NULL);

	g_object_unref (G_OBJECT (sample));

	gst_element_set_state (GST_ELEMENT (lod->pipeline), GST_STATE_NULL);
	g_object_unref (lod->pipeline);
	g_free (lod);
}

static void
load_op_paused (MarlinOperation *operation,
		gboolean paused,
		struct _load_op_data *lod)
{
	gst_element_set_state (GST_ELEMENT (lod->pipeline),
			       paused ? GST_STATE_PAUSED : GST_STATE_PLAYING);
}

void
marlin_window_load_file (MarlinWindow *window,
			 const char   *uri)
{
	struct _load_op_data *lod;
	MarlinOperation *operation;
	MarlinLoadPipeline *pipeline;

	lod = g_new (struct _load_op_data, 1);

	lod->op = operation = marlin_operation_new ();
	lod->window = window;

	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (load_op_started), lod);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (load_op_finished), lod);
	g_signal_connect (G_OBJECT (operation), "cancelled",
			  G_CALLBACK (load_op_cancelled), lod);
	g_signal_connect (G_OBJECT (operation), "error",
			  G_CALLBACK (load_op_error), lod);
	g_signal_connect (G_OBJECT (operation), "paused",
			  G_CALLBACK (load_op_paused), lod);

	pipeline = marlin_load_pipeline_new (operation);
	lod->pipeline = (MarlinPipeline *) pipeline;

	g_object_set (G_OBJECT (pipeline),
		      "filename", uri,
		      "sample", window->priv->sample,
		      NULL);

	g_object_ref (G_OBJECT (window->priv->sample));
	/* Go */
	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_PLAYING);
}

struct _save_op_data {
	MarlinOperation *op;
	MarlinPipeline *pipeline;
	MarlinWindow *window;

	char *temp_filename;
	char *filename;
};

static void
save_op_started (MarlinOperation *operation,
		 struct _save_op_data *sod)
{
	MarlinWindow *window = sod->window;
	char *filename, *text, *tmp;

	g_object_get (G_OBJECT (sod->window->priv->sample),
		      "filename", &filename,
		      NULL);

	text = g_strdup_printf (_("Saving %s"), filename);
	g_free (filename);

	tmp = g_strdup_printf ("<span weight=\"bold\">%s</span>\n%s",
			       _("Saving file"), text);
	g_free (text);

	window->priv->progress_window = marlin_progress_dialog_new (operation,
								    GTK_WINDOW (window),
								    tmp, NULL);
	g_free (tmp);

	/* Show the window immediatly */
	gtk_widget_show (GTK_WIDGET (window->priv->progress_window));
}

static void
save_op_finished (MarlinOperation *operation,
		  struct _save_op_data *sod)
{
	MarlinWindow *window = sod->window;
	MarlinWindowPrivate *priv = window->priv;
	MarlinPipeline *pipeline;

	gtk_widget_destroy (GTK_WIDGET (priv->progress_window));
	window->priv->progress_window = NULL;

	pipeline = sod->pipeline;

	/* Reset the time of last save */
	window->priv->last_save = time (NULL);

	g_object_set (G_OBJECT (priv->sample),
		      "dirty", FALSE,
		      NULL);

	/* Copy the new file over the old one...only remove the temp file
	   if the copy succeeded */
	if (marlin_file_copy (sod->temp_filename, sod->filename, NULL)) {
		unlink (sod->temp_filename);
	} else {
		/* FIXME: Why did it fail? Show user a decent dialog */
		g_print ("Copying file to %s failed.\nFile has been left as %s\n", sod->filename, sod->temp_filename);
	}

	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_NULL);
	g_object_unref (pipeline);

	g_free (sod->filename);
	g_free (sod->temp_filename);
	g_free (sod);
}

static void
save_op_cancelled (MarlinOperation *operation,
		   struct _save_op_data *sod)
{
	MarlinWindow *window = sod->window;
	MarlinWindowPrivate *priv = window->priv;
	MarlinPipeline *pipeline = sod->pipeline;

	gtk_widget_destroy (GTK_WIDGET (priv->progress_window));
	priv->progress_window = NULL;

	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_NULL);
	g_object_unref (pipeline);

	g_free (sod->filename);
	g_free (sod->temp_filename);
	g_free (sod);
}

static void
save_op_paused (MarlinOperation *operation,
		gboolean paused,
		struct _save_op_data *sod)
{
	gst_element_set_state (GST_ELEMENT (sod->pipeline),
			       paused ? GST_STATE_PAUSED : GST_STATE_PLAYING);
}

/* FIXME: Pass this as MarlinBaseWindow? */
typedef void (* options_dialog) (GtkWindow *window, MarlinPipeline *pipeline);
struct _maps {
	char *mimetype;
	options_dialog function;
};

static void
marlin_default_save (GtkWindow *window,
		     MarlinPipeline *pipeline)
{
	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_PLAYING);
}

struct _maps mimetypemap[] = {
	{ "audio/x-mp3", marlin_mp3_dialog_new },
	{ "audio/mpeg", marlin_mp3_dialog_new },
	{ "application/ogg", marlin_vorbis_dialog_new },
	{ "application/x-ogg", marlin_vorbis_dialog_new },
	{ NULL, marlin_default_save },
	{ NULL, NULL }
};

struct _extensionmaps {
	char *extension;
	char *mimetype;
};

struct _extensionmaps extension_to_mime[] = {
	{ ".spx", "audio/x-speex" },
	{ NULL, NULL }
};

static void
save_sample_as (MarlinWindow *window,
		const char *filename,
		GstElement *encoder)
{
	struct _save_op_data *sod;
	MarlinOperation *operation;
	MarlinSavePipeline *pipeline;
	gboolean uncertain;
	char *mimetype;
	char *path, *name;
	int i;

	sod = g_new (struct _save_op_data, 1);
	mimetype = g_content_type_guess (filename, NULL, 0, &uncertain);

	if (strcmp (mimetype, "application/octet-stream") == 0) {
		int i;

		for (i = 0; extension_to_mime[i].extension; i++) {
			char *ext = strrchr (filename, '.');

			if (strcmp (ext, extension_to_mime[i].extension) == 0) {
				mimetype = g_strdup (extension_to_mime[i].mimetype);
				break;
			} else {
				mimetype = NULL;
			}
		}
	}

	if (mimetype == NULL) {
		/* FIXME: Return some sort of error to say
		   unknown extension and show the save dialog again */
		return;
	}

	sod->op = operation = marlin_operation_new ();
	sod->window = window;

	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (save_op_started), sod);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (save_op_finished), sod);
	g_signal_connect (G_OBJECT (operation), "cancelled",
			  G_CALLBACK (save_op_cancelled), sod);
	g_signal_connect (G_OBJECT (operation), "paused",
			  G_CALLBACK (save_op_paused), sod);
	/* FIXME: Should handle errors too */

	sod->filename = g_strdup (filename);
	path = g_path_get_dirname (filename);
	name = g_path_get_basename (filename);

	if (path == NULL) {
		sod->temp_filename = g_strdup_printf (".%s.XXXXXX", name);
	} else {
		char *t = g_strdup_printf (".%s.XXXXXX", name);
		mkstemp (t);

		sod->temp_filename = g_build_filename (path, t, NULL);
		g_free (t);
	}
	g_free (path);
	g_free (name);

	if (encoder == NULL) {
		pipeline = g_object_new (MARLIN_SAVE_PIPELINE_TYPE,
					 "operation", operation,
					 "sample", window->priv->sample,
					 "filename", sod->temp_filename,
					 "mimetype", mimetype,
					 NULL);
	} else {
		pipeline = g_object_new (MARLIN_SAVE_PIPELINE_TYPE,
					 "operation", operation,
					 "sample", window->priv->sample,
					 "filename", sod->temp_filename,
					 "encoder", encoder,
					 NULL);
	}

	sod->pipeline = (MarlinPipeline *) pipeline;

	if (encoder != NULL) {
		/* Caller has supplied an encoder, so the parameters have
		   already been set */
		marlin_default_save (GTK_WINDOW (window),
				     MARLIN_PIPELINE (pipeline));
		g_object_set (G_OBJECT (window->priv->sample),
			      "encoder", encoder,
			      NULL);

		g_free (mimetype);
		return;
	}

	for (i = 0; mimetypemap[i].function; i++) {
		if (mimetypemap[i].mimetype == NULL) {
			marlin_default_save (GTK_WINDOW (window),
					     MARLIN_PIPELINE (pipeline));

			g_free (mimetype);
			return;
		}

		if (strcmp (mimetype, mimetypemap[i].mimetype) == 0) {
			mimetypemap[i].function (GTK_WINDOW (window),
						 MARLIN_PIPELINE (pipeline));

			g_free (mimetype);
		 	return;
		}
	}
}

void
marlin_window_save_sample (MarlinWindow *window)
{
	char *filename;
	GstElement *encoder;

 	g_object_get (G_OBJECT (window->priv->sample),
		      "filename", &filename,
		      "encoder", &encoder,
		      NULL);

	if (encoder == NULL) {
		gboolean uncertain;
		char *mimetype;

		/* If there was no encoder then we need to check that we
		   can save to this format */

		mimetype = g_content_type_guess (filename, NULL, 0, &uncertain);
		if (marlin_gst_can_encode (mimetype) == FALSE) {
			GtkWidget *dialog;
			char *text;

			text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n%s",
						_("Marlin is unable to save %s\nas there is no encoder for %s."),
						_("Would you like to save as a different format?"));

			dialog = gtk_message_dialog_new (GTK_WINDOW (window),
							 GTK_DIALOG_MODAL,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_NONE,
							 text,
							 filename, mimetype);
			g_free (text);

			/* Naughty */
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
			gtk_label_set_line_wrap (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), FALSE);
			gtk_dialog_add_button (GTK_DIALOG (dialog),
					       GTK_STOCK_CANCEL,
					       GTK_RESPONSE_CANCEL);
			gtk_dialog_add_button (GTK_DIALOG (dialog),
					       GTK_STOCK_SAVE_AS,
					       GTK_RESPONSE_OK);

			switch (gtk_dialog_run (GTK_DIALOG (dialog))) {
			case GTK_RESPONSE_CANCEL:
				gtk_widget_destroy (dialog);
				break;

			case GTK_RESPONSE_OK:
				gtk_widget_destroy (dialog);
				marlin_window_save_sample_as (window);
				break;

			default:
				break;
			}

			g_free (mimetype);
			g_free (filename);
			return;
		}

		g_free (mimetype);
	}

	save_sample_as (window, filename, encoder);
	g_free (filename);
}

struct _FileSaveData {
	GtkWidget *filesel;
	MarlinWindow *window;
#ifdef HAVE_MEDIA_PROFILES
	GtkWidget *profile_hbox;
	GtkWidget *type_combo, *profiles;
#endif
};

#ifdef HAVE_MEDIA_PROFILES
enum {
	EXTENSION,
	PROFILE
};
#endif

/* Returns TRUE to write file, FALSE to not */
static gboolean
ask_overwrite_dialog (GtkWindow *window,
		      const char *name)
{
	GtkWidget *overwrite;
	char *text;
	char *backup;

	/* Ask to overwrite */
	text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n%s",
				_("The file '%s' already exists."),
				_("Do you want to make a backup called '%s' first?"));

	backup = g_strdup_printf ("%s~", name);
	overwrite = gtk_message_dialog_new_with_markup (window,
							GTK_DIALOG_MODAL,
							GTK_MESSAGE_QUESTION,
							GTK_BUTTONS_NONE,
							text, name, backup);
	g_free (text);

	/* Naughty */
	gtk_label_set_line_wrap (GTK_LABEL (GTK_MESSAGE_DIALOG (overwrite)->label), FALSE);

	gtk_dialog_add_button (GTK_DIALOG (overwrite),
			       _("Overwrite File"),
			       GTK_RESPONSE_NO);
	gtk_dialog_add_button (GTK_DIALOG (overwrite),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (overwrite),
			       _("Backup File"),
			       GTK_RESPONSE_YES);

	switch (gtk_dialog_run (GTK_DIALOG (overwrite))) {
	case GTK_RESPONSE_YES:
		gtk_widget_destroy (overwrite);

		if (marlin_file_copy (name, backup, NULL) == FALSE) {
			g_warning ("Error making backup...Not saving so that there is no data loss");
			g_free (backup);
			return FALSE;
		}

		g_free (backup);
		return TRUE;

	case GTK_RESPONSE_NO:
		gtk_widget_destroy (overwrite);
		g_free (backup);

		return TRUE;

	default:
		gtk_widget_destroy (overwrite);
		g_free (backup);

		return FALSE;
	}
}

/* Returns TRUE to write file, FALSE to not */
static gboolean
ask_nospace_write_dialog (GtkWindow *window,
			  const char *name)
{
	GtkWidget *overwrite;
	char *text;

	/* Ask to overwrite */
	text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n%s",
				_("The file '%s' already exists."),
				_("There is not enough space to create a backup.\nDo you want to overwrite the file?"));

	overwrite = gtk_message_dialog_new (window,
					    GTK_DIALOG_MODAL,
					    GTK_MESSAGE_QUESTION,
					    GTK_BUTTONS_NONE,
					    text, name);
	g_free (text);

	/* Naughty */
	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (overwrite)->label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (GTK_MESSAGE_DIALOG (overwrite)->label), FALSE);

	gtk_dialog_add_button (GTK_DIALOG (overwrite),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (overwrite),
			       _("Overwrite File"),
			       GTK_RESPONSE_YES);

	switch (gtk_dialog_run (GTK_DIALOG (overwrite))) {
	case GTK_RESPONSE_YES:
		gtk_widget_destroy (overwrite);
		return TRUE;

	default:
		gtk_widget_destroy (overwrite);
		return FALSE;
	}
}

static gboolean
is_valid_filename (GtkWindow *window,
		   const char *name)
{
	if (g_file_test (name, G_FILE_TEST_EXISTS) == FALSE) {
		return TRUE;
	}

	if (g_file_test (name, G_FILE_TEST_IS_DIR)) {
		GtkWidget *isdir;
		char *text;

		text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
					_("Unable to save file to '%s'."),
					_("'%s' is a directory."));

		isdir = gtk_message_dialog_new (window,
						0, GTK_MESSAGE_ERROR,
						GTK_BUTTONS_CLOSE,
						text,
						name, name);
		g_free (text);

		/* Naughty */
		gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (isdir)->label), TRUE);

		gtk_dialog_run (GTK_DIALOG (isdir));
		gtk_widget_destroy (isdir);

		return FALSE;
	}

	if (g_file_test (name, G_FILE_TEST_EXISTS)) {
		GFile *file;
		GFileInfo *file_info, *fs_info;
		guint64 free_size;
		goffset file_size;
		GError *error = NULL;

		file = g_file_new_for_path (name);
		file_info = g_file_query_info (file, NULL,
					       G_FILE_QUERY_INFO_NONE,
					       NULL, &error);
		if (error != NULL) {
			g_warning ("Error querying file info: %s",
				   error->message);
			g_object_unref (file);
			g_error_free (error);
			return ask_overwrite_dialog (window, name);
		}

		fs_info = g_file_query_filesystem_info
			(file, G_FILE_ATTRIBUTE_FILESYSTEM_FREE, NULL, &error);
		if (error != NULL) {
			g_warning ("Error querying filesystem info: %s",
				   error->message);
			g_error_free (error);
			g_object_unref (file_info);
			g_object_unref (file);
			return ask_overwrite_dialog (window, name);
		}

		/* Check the there's enough space */
		free_size = g_file_info_get_attribute_uint64
			(fs_info, G_FILE_ATTRIBUTE_FILESYSTEM_FREE);
		file_size = g_file_info_get_size (file_info);

		g_object_unref (file_info);
		g_object_unref (fs_info);
		g_object_unref (file);

		if (file_size >= free_size) {
			return ask_nospace_write_dialog (window, name);
		} else {
			return ask_overwrite_dialog (window, name);
		}
	}

	return FALSE;
}

static void
file_sel_save_as_response (GtkDialog *filesel,
			   int response_id,
			   struct _FileSaveData *sd)
{
	MarlinWindow *window = sd->window;
	GConfClient *client;
	GstElement *encoder = NULL;
	const char *name;
	char *dirname, *d;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (filesel));
		if (is_valid_filename (GTK_WINDOW (filesel), name) == FALSE) {
			return;
		}

		d = g_path_get_dirname (name);
		/* Add a trailing / so that the file selector will cd into it */
		dirname = g_strdup_printf ("%s/", d);

		client = gconf_client_get_default ();
		gconf_client_set_string (client,
					 "/apps/marlin/system-state/save-file-directory",
					 dirname, NULL);
		g_object_unref (G_OBJECT (client));
		g_free (dirname);
		g_free (d);

#ifdef HAVE_MEDIA_PROFILES
		{
			int type;

			type = gtk_combo_box_get_active (GTK_COMBO_BOX (sd->type_combo));
			if (type == EXTENSION) {
				encoder = NULL;
			} else {
				const char *prof;
				char *pipeline;
				GstPad *src, *sink;
				GstElement *id;
				GError *err = NULL;
				GMAudioProfile *profile = gm_audio_profile_choose_get_active (sd->profiles);

				prof = gm_audio_profile_get_pipeline (profile);
				pipeline = g_strdup_printf ("(name=profile-encoder identity name=encoder_start ! %s ! identity name=encoder_end )", prof);
				encoder = gst_parse_launch (pipeline, &err);
				if (err != NULL) {
					g_warning ("Error parsing pipeline");
					g_free (pipeline);
					g_error_free (err);
					encoder = NULL;
				} else {
					GstPad *ghost;

					/* Hook up ghost pads to the start
					   and end */
					id = gst_bin_get_by_name
						(GST_BIN (encoder),
						 "encoder_start");
					g_assert (id != NULL);

					sink = gst_element_get_pad (id, "sink");
					ghost = gst_ghost_pad_new ("sink-ghost",
								   sink);
					gst_element_add_pad (encoder, ghost);

					id = gst_bin_get_by_name
						(GST_BIN (encoder),
						 "encoder_end");
					g_assert (id != NULL);

					src = gst_element_get_pad (id, "src");
					ghost = gst_ghost_pad_new ("src-ghost",
								   src);
					gst_element_add_pad (encoder, ghost);

/* 					g_print ("Using %s as pipeline\n", pipeline); */
					g_free (pipeline);
				}
			}
		}
#endif
		/* Set the sample's filename */
		g_object_set (G_OBJECT (window->priv->sample),
			      "filename", name,
			      NULL);

		save_sample_as (window, name, encoder);
		break;

	case GTK_RESPONSE_CANCEL:
	default:
		break;

	}

	gtk_widget_destroy (GTK_WIDGET (filesel));
	g_free (sd);
}

#ifdef HAVE_MEDIA_PROFILES
static void
add_formats (struct _FileSaveData *sd)
{
	int i;
	static char *formats[] = {
		N_("From file extension"),
		N_("From profile"),
		NULL
	};

	for (i = 0; formats[i] != NULL; i++) {
		gtk_combo_box_append_text (GTK_COMBO_BOX (sd->type_combo), _(formats[i]));
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (sd->type_combo), 0);
	gtk_widget_set_sensitive (sd->profile_hbox, FALSE);
}

static void
type_combo_changed (GtkComboBox *combo,
		    struct _FileSaveData *sd)
{
	int active = gtk_combo_box_get_active (combo);

	gtk_widget_set_sensitive (sd->profile_hbox, (active == 1));
}
#endif

static void
save_dialog_add_options (struct _FileSaveData *sd)
{
#ifdef HAVE_MEDIA_PROFILES
	GtkWidget *table, *label;

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);

	label = gtk_label_new_with_mnemonic (_("Determine _Format:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	sd->type_combo = gtk_combo_box_new_text ();
	gtk_widget_show (sd->type_combo);
	PACK (table, sd->type_combo, 1, 0, GTK_FILL);

	g_signal_connect (G_OBJECT (sd->type_combo), "changed",
			  G_CALLBACK (type_combo_changed), sd);
	sd->profile_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (sd->profile_hbox);
	gtk_table_attach (GTK_TABLE (table), sd->profile_hbox,
			  1, 2, 1, 2,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	label = gtk_label_new_with_mnemonic (_("_Profile:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (sd->profile_hbox), label, FALSE, FALSE, 0);
	sd->profiles = gm_audio_profile_choose_new ();
	gtk_widget_show (sd->profiles);
	gtk_box_pack_start (GTK_BOX (sd->profile_hbox), sd->profiles,
			    TRUE, TRUE, 0);

	add_formats (sd);

	gtk_file_chooser_set_extra_widget (GTK_FILE_CHOOSER (sd->filesel), table);
#endif
}

void
marlin_window_save_sample_as (MarlinWindow *window)
{
	struct _FileSaveData *sd;
	GConfClient *client;
	char *directory, *name, *filename, *sname;

	sd = g_new (struct _FileSaveData, 1);

	g_object_get (G_OBJECT (window->priv->sample),
		      "filename", &filename,
		      "name", &name,
		      NULL);

	sd->window = window;
	sd->filesel = (GtkWidget *) marlin_file_save_dialog_new (GTK_WINDOW (window));

	sname = filename ? g_path_get_basename (filename) : g_strdup (name);
	gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (sd->filesel),
					   sname);
	g_free (sname);
	g_free (filename);
	g_free (name);

	save_dialog_add_options (sd);

	client = gconf_client_get_default ();
	directory = gconf_client_get_string (client,
					     "/apps/marlin/system-state/save-file-directory",
					     NULL);
	if (directory != NULL &&
	    *directory != 0) {
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (sd->filesel), directory);
	}
	g_free (directory);
	g_object_unref (G_OBJECT (client));

	g_signal_connect (G_OBJECT (sd->filesel), "response",
			  G_CALLBACK (file_sel_save_as_response), sd);

	gtk_widget_show (sd->filesel);
}

struct _FileOpenData {
	GtkWidget *filesel;
	MarlinWindow *window;
};

static void
file_sel_response (GtkDialog            *filesel,
		   int                   response_id,
		   struct _FileOpenData *fd)
{
	MarlinWindow *window = fd->window;
	MarlinWindowPrivate *priv = fd->window->priv;
	char *name, *uri;
	char *dirname, *d;
	guint64 n_frames;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (filesel));
		uri = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (filesel));

		d = g_path_get_dirname (name);
		g_free (name);

		/* Add a trailing / so that the file selector will cd into it */
		dirname = g_strdup_printf ("%s/", d);

		marlin_gconf_set_string ("/apps/marlin/system-state/open-file-directory", dirname);
		g_free (dirname);
		g_free (d);

		g_object_get (priv->sample,
			      "total-frames", &n_frames,
			      NULL);
		if (n_frames == 0) {
			/* Open the file in this window */
			marlin_window_load_file (window, uri);
		} else {
			marlin_open_window (uri, NULL, NULL, NULL);
		}
		g_free (uri);
		break;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (filesel));
	g_free (fd);
}

void
marlin_window_open (MarlinWindow *window)
{
	static struct _FileOpenData *fd = NULL;
	char *directory;

	if (fd != NULL) {
		gdk_window_raise (fd->filesel->window);
		gdk_window_show (fd->filesel->window);
		return;
	}

	fd = g_new (struct _FileOpenData, 1);

	fd->window = window;
	fd->filesel = (GtkWidget *) marlin_file_open_dialog_new (GTK_WINDOW (window));

	directory = marlin_gconf_get_string ("/apps/marlin/system-state/open-file-directory");
	if (directory != NULL &&
	    *directory != 0) {
#if 0
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (fd->filesel),
 						     directory);
#endif
		g_free (directory);
	}

	g_signal_connect (G_OBJECT (fd->filesel), "response",
			  G_CALLBACK (file_sel_response), fd);
	g_signal_connect (G_OBJECT (fd->filesel), "destroy",
			  G_CALLBACK (gtk_widget_destroyed), &fd);

	gtk_widget_show (fd->filesel);
}

void
marlin_window_select_all (MarlinWindow *window,
			  MarlinUndoContext *ctxt)
{
	guint64 total;

	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &total,
		      NULL);
	marlin_sample_selection_set (window->priv->selection,
				     MARLIN_COVERAGE_BOTH,
				     (guint64) 0, total - 1,
				     ctxt);
}

void
marlin_window_select_none (MarlinWindow *window,
			   MarlinUndoContext *ctxt)
{
	marlin_sample_selection_clear (window->priv->selection, ctxt);
}

void
marlin_window_zoom_in (MarlinWindow *window)
{
	guint fpp;

	g_object_get (G_OBJECT (window->priv->view),
		      "frames_per_pixel", &fpp,
		      NULL);

	fpp /= 2;

	if (fpp < 1) {
		fpp = 1;
	}

	g_object_set (G_OBJECT (window->priv->view),
		      "frames_per_pixel", fpp,
		      NULL);

	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

void
marlin_window_zoom_out (MarlinWindow *window)
{
	guint fpp;

	g_object_get (G_OBJECT (window->priv->view),
		      "frames_per_pixel", &fpp,
		      NULL);

	fpp *= 2;

	if (fpp > 16384) {
		fpp = 16384;
	}

	g_object_set (G_OBJECT (window->priv->view),
		      "frames_per_pixel", fpp,
		      NULL);

	marlin_window_set_view_sensitive (window, TRUE);
	marlin_window_set_plugins_sensitive (window, TRUE);
}

void
marlin_window_show_all (MarlinWindow *window)
{
	guint fpp;
	guint64 total;

	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &total,
		      NULL);

	fpp = (total / GTK_WIDGET (window->priv->view)->allocation.width);
	if (total % GTK_WIDGET (window->priv->view)->allocation.width != 0) {
		fpp++;
	}

	g_object_set (G_OBJECT (window->priv->view),
		      "frames_per_pixel", fpp,
		      NULL);

	marlin_sample_view_scroll_to (window->priv->view, (guint64) 0);
}

void
marlin_window_zoom_selection (MarlinWindow *window)
{
	guint fpp;
	guint64 length, start, finish;
	MarlinWindowPrivate *priv;
	MarlinCoverage coverage;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &coverage,
				     &start, &finish);
	if (coverage == MARLIN_COVERAGE_NONE) {
		return;
	}

	length = (finish - start) + 1;

	fpp = (length / GTK_WIDGET (priv->view)->allocation.width);
	if (length % GTK_WIDGET (priv->view)->allocation.width != 0) {
		fpp++;
	}

	g_object_set (G_OBJECT (window->priv->view),
		      "frames_per_pixel", fpp,
		      NULL);
	marlin_sample_view_scroll_to (priv->view, start);
}

void
marlin_window_delete (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;
	MarlinRange range;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	/* Check we're only deleting on BOTH */
	if (range.coverage != MARLIN_COVERAGE_BOTH) {
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new (GTK_WINDOW (window),
						 GTK_DIALOG_DESTROY_WITH_PARENT |
						 GTK_DIALOG_MODAL,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 _("The delete operation can only be performed on both channels of a stereo sample."));
		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (dialog);

		return;
	}

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Delete Selection"));
	marlin_sample_delete_range (priv->sample, NULL, &range, ctxt, NULL);
	marlin_undo_manager_context_end (priv->undo, ctxt);

	/* Clear the selection */
	marlin_sample_selection_clear (priv->selection, NULL);
}

struct _MuteData {
	MarlinOperation *operation;
	MarlinWindow *window;
	MarlinProgressDialog *progress;
};

static void
mute_op_started (MarlinOperation *operation,
		 struct _MuteData *md)
{
#if 0
	md->progress = marlin_progress_dialog_new (md->window->priv->sample,
						   md->operation,
						   GTK_WINDOW (md->window),
						   _("Muting"), "");
#endif
}

static void
mute_op_finished (MarlinOperation *operation,
		  struct _MuteData *md)
{
}

static void
mute_op_cancelled (MarlinOperation *operation,
		   struct _MuteData *md)
{
}

static void
mute_op_error (MarlinOperation *operation,
	       struct _MuteData *md)
{
}

static void
mute_op_paused (MarlinOperation *operation,
		struct _MuteData *md)
{
}

void
marlin_window_clear (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;
	MarlinRange range;
	struct _MuteData *md;

	priv = window->priv;

	md = g_new (struct _MuteData, 1);
	md->window = window;
	md->operation = marlin_operation_new ();

	g_signal_connect (G_OBJECT (md->operation), "started",
			  G_CALLBACK (mute_op_started), md);
	g_signal_connect (G_OBJECT (md->operation), "finished",
			  G_CALLBACK (mute_op_finished), md);
	g_signal_connect (G_OBJECT (md->operation), "cancelled",
			  G_CALLBACK (mute_op_cancelled), md);
	g_signal_connect (G_OBJECT (md->operation), "error",
			  G_CALLBACK (mute_op_error), md);
	g_signal_connect (G_OBJECT (md->operation), "paused",
			  G_CALLBACK (mute_op_paused), md);

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		guint64 frames;

		/* Clear the whole sample */
		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &frames,
			      NULL);

		ctxt = marlin_undo_manager_context_begin (priv->undo,
							  _("Mute Sample"));
		range.start = 0;
		range.finish = frames - 1;
		range.coverage = MARLIN_COVERAGE_BOTH;
		marlin_sample_clear_range (priv->sample, md->operation,
					   &range, ctxt, NULL);
		marlin_undo_manager_context_end (priv->undo, ctxt);
		return;
	}

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Mute Selection"));
	marlin_sample_clear_range (priv->sample, md->operation, &range, ctxt, NULL);
	marlin_undo_manager_context_end (priv->undo, ctxt);

	/* Clear the selection */
	marlin_sample_selection_clear (priv->selection, NULL);
}

void
marlin_window_invert (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;
	MarlinRange range;
	gboolean ret;
	GError *error = NULL;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		guint64 frames;

		/* Invert whole sample */
		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &frames,
			      NULL);

		ctxt = marlin_undo_manager_context_begin (priv->undo, _("Invert Sample"));
		range.start = 0;
		range.finish = frames - 1;
		range.coverage = MARLIN_COVERAGE_BOTH;
		ret = marlin_sample_invert_range (priv->sample, &range,
						  ctxt, &error);
		if (ret == FALSE) {
			g_warning ("marlin_sample_invert_range failed");
		}
		marlin_undo_manager_context_end (priv->undo, ctxt);

		return;
	}

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Invert Selection"));
	ret = marlin_sample_invert_range (priv->sample, &range, ctxt, &error);
	if (ret == FALSE) {
		g_warning ("marlin_sample_invert_range failed");
	}
	marlin_undo_manager_context_end (priv->undo, ctxt);
}

void
marlin_window_crop (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;
	MarlinRange range;
	GError *error = NULL;
	gboolean ret;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		return;
	}

	/* Check we're only cropping on BOTH */
	/* FIXME: For discussion - Should cropping a single channel on a
	   stereo sample make a mono sample as the result? */
	if (range.coverage != MARLIN_COVERAGE_BOTH) {
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new (GTK_WINDOW (window),
						 GTK_DIALOG_DESTROY_WITH_PARENT |
						 GTK_DIALOG_MODAL,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 _("The crop operation can only be performed on both channels of a stereo sample."));
		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (dialog);

		return;
	}

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Crop Selection"));
	ret = marlin_sample_crop_range (priv->sample, NULL, &range, ctxt, &error);
	if (ret == FALSE) {
		g_warning ("marlin_sample_crop_range failed");
	}
	marlin_undo_manager_context_end (priv->undo, ctxt);

	/* Clear the selection */
	marlin_sample_selection_clear (priv->selection, NULL);
}

void
marlin_window_paste (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinCoverage coverage;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *clipboard;
	MarlinUndoContext *ctxt;
	guint64 position;
	gboolean ret;
	GError *error = NULL;

	priv = window->priv;

	g_object_get (G_OBJECT (priv->view),
		      "cursor_coverage", &coverage,
		      "cursor_position", &position,
		      NULL);

	clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Paste"));
	ret = marlin_sample_insert (priv->sample, clipboard, position, coverage, ctxt, &error);
	if (ret == FALSE) {
		g_warning ("marlin_sample_insert failed");
	}

	marlin_undo_manager_context_end (priv->undo, ctxt);
}

/* FIXME: Pretty much everything after this needs to be in a seperate
   file in dialogs/
   if it opens a dialog, it should go there...keep the clutter out of
   marlin-window.c */
struct _mixer_data {
	GtkAdjustment *src_level, *dest_level;
	GtkWidget *expand;
	MarlinSample *src, *dest;
	MarlinWindow *window;
};

static void
mixer_response_cb (GtkDialog *mixer,
		   guint response_id,
		   struct _mixer_data *md)
{
	MarlinProgram *program = marlin_program_get_default ();
	MarlinUndoContext *ctxt;
	MarlinSample *clipboard, *dest;
	MarlinWindowPrivate *priv;
	MarlinRange range;
	gboolean clip = FALSE;

	priv = md->window->priv;
	switch (response_id) {
	case GTK_RESPONSE_CANCEL:
		break;

	case GTK_RESPONSE_OK:
		clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));
		dest = md->window->priv->sample;

		g_object_get (G_OBJECT (md->window->priv->view),
			      "cursor_position", &(range.start),
			      NULL);

		marlin_sample_selection_get (md->window->priv->selection,
					     &(range.coverage), NULL, &(range.finish));
		if (range.coverage != MARLIN_COVERAGE_NONE) {
			clip = TRUE;
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Mix Into Selection"));
		} else {
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Mix"));
			clip = FALSE;
		}

		/* If coverage == MARLIN_COVERAGE_NONE then md->expand will
		   be invalid */
		if (range.coverage == MARLIN_COVERAGE_NONE ||
		    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (md->expand)) == FALSE) {
			marlin_sample_mix (dest, clipboard,
					   md->src_level->value,
					   md->dest_level->value,
					   &range, clip,
					   NULL, ctxt, NULL);
		} else {
			MarlinRange src_range, dest_range;
			guint64 src_frames;

			marlin_sample_selection_get (md->window->priv->selection,
						     NULL,
						     &(dest_range.start),
						     &(dest_range.finish));

			g_object_get (clipboard,
				      "total_frames", &src_frames,
				      NULL);
			src_range.start = 0;
			src_range.finish = src_frames -1;

			marlin_sample_expand_mix (clipboard, dest,
						  &src_range, &dest_range,
						  md->src_level->value,
						  md->dest_level->value,
						  NULL, ctxt, NULL);
		}

		marlin_undo_manager_context_end (priv->undo, ctxt);
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-paste-mix-dialog");
		return;

	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (mixer));
	g_free (md);
}

#define PACK_FULL(t, w, l, top, x, y) gtk_table_attach (GTK_TABLE ((t)), (w), \
                                             (l), (l) + 1, \
                                             (top), top + 1, \
                                             (x), (y), 0, 0)

void
marlin_window_mix (MarlinWindow *window)
{
	MarlinProgram *program = marlin_program_get_default ();
	struct _mixer_data *md;
	MarlinRange dest_range;
	guint64 src_frames, dest_frames;
	GtkWidget *mixer, *vbox, *table;
	GtkWidget *label, *s_scale, *d_scale;
	char *name, *title, *expand_str;

	md = g_new (struct _mixer_data, 1);
	md->src = MARLIN_SAMPLE (marlin_program_get_clipboard (program));
	md->dest = window->priv->sample;
	md->window = window;

	g_object_get (G_OBJECT (window->priv->sample),
		      "name", &name,
		      NULL);
	title = g_strdup_printf (_("Mix Clipboard: %s"), name);
	g_free (name);

	mixer = gtk_dialog_new ();
	gtk_window_set_transient_for (GTK_WINDOW (mixer), GTK_WINDOW (window));
	gtk_window_set_title (GTK_WINDOW (mixer), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (mixer), FALSE);
 	gtk_window_set_default_size (GTK_WINDOW (mixer), 325, 270);
/* 	gtk_window_set_resizable (GTK_WINDOW (mixer), FALSE); */

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (mixer)->vbox), vbox);

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_table_set_row_spacings (GTK_TABLE (table), 12);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	md->src_level = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -43.1, 15.1, 0.1, 10.0, 0.1));
	md->dest_level = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -43.1, 15.1, 0.1, 10.0, 0.1));

	s_scale = gtk_vscale_new (md->src_level);
	g_signal_connect (G_OBJECT (s_scale), "format_value",
			  G_CALLBACK (marlin_set_volume_digits), NULL);
	gtk_range_set_inverted (GTK_RANGE (s_scale), TRUE);
	gtk_widget_show (s_scale);
	PACK_FULL (table, s_scale, 0, 1, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND);

	label = gtk_label_new_with_mnemonic (_("_Clipboard Level:"));
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), s_scale);
	gtk_widget_show (label);
	PACK_FULL (table, label, 0, 0, GTK_FILL | GTK_EXPAND, GTK_FILL);

	marlin_add_paired_relations (s_scale, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	d_scale = gtk_vscale_new (md->dest_level);
	g_signal_connect (G_OBJECT (d_scale), "format-value",
			  G_CALLBACK (marlin_set_volume_digits), NULL);
	gtk_range_set_inverted (GTK_RANGE (d_scale), TRUE);
	gtk_widget_show (d_scale);
	PACK_FULL (table, d_scale, 1, 1, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND);

	label = gtk_label_new_with_mnemonic (_("_Sample Level:"));
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), d_scale);
	gtk_widget_show (label);
	PACK_FULL (table, label, 1, 0, GTK_FILL | GTK_EXPAND, GTK_FILL);

	marlin_add_paired_relations (d_scale, ATK_RELATION_LABELLED_BY,
				     label, ATK_RELATION_LABEL_FOR);

	marlin_sample_selection_get (md->window->priv->selection,
				     &(dest_range.coverage),
				     &(dest_range.start),
				     &(dest_range.finish));

	/* We only want to show the time stretch option
	   if we have a selection to stretch into */
	if (dest_range.coverage != MARLIN_COVERAGE_NONE) {
		dest_frames = (dest_range.finish - dest_range.start) + 1;

		g_object_get (md->src,
			      "total_frames", &src_frames,
			      NULL);

		if (src_frames < dest_frames) {
			expand_str = N_("Expand clipboard to fill selection");
		} else {
			expand_str = N_("Shrink clipboard to fit selection");
		}

		md->expand = gtk_check_button_new_with_label (_(expand_str));
		gtk_box_pack_start (GTK_BOX (vbox), md->expand, FALSE, FALSE, 0);
		gtk_widget_show (md->expand);
	}

	gtk_dialog_add_button (GTK_DIALOG (mixer),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (mixer),
			       _("Mix"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (mixer),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (mixer), GTK_RESPONSE_OK);

	g_signal_connect (mixer, "response",
			  G_CALLBACK (mixer_response_cb), md);
	gtk_widget_show (mixer);
}

void
marlin_window_selection_to_clipboard (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *clipboard;
	MarlinRange range;
	GError *error = NULL;
	char *name;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		return;
	}

	clipboard = marlin_sample_new_from_sample_with_range (priv->sample,
							      &range, &error);
	if (clipboard == NULL) {
		g_warning ("marlin_sample_new_from_sample_with_range failed");
		return;
	}

	g_object_get (G_OBJECT (priv->sample),
		      "name", &name,
		      NULL);

	marlin_program_set_clipboard (program, clipboard, name);
	g_free (name);
	g_object_unref (clipboard);
}

GtkUIManager *
marlin_window_get_menu_merge (MarlinWindow *window)
{
	return GTK_UI_MANAGER (window->priv->ui_merge);
}

MarlinToolbar *
marlin_window_get_toolbar (MarlinWindow *window)
{
	return window->priv->toolbar;
}

MarlinSampleView *
marlin_window_get_view (MarlinWindow *window)
{
	return window->priv->view;
}

void
marlin_window_select_region (MarlinWindow *window)
{
	guint fpp;
	guint64 position;

	g_return_if_fail (IS_MARLIN_WINDOW (window));
	g_return_if_fail (window->priv->selection != NULL);

	if (window->priv->select_region_dialog != NULL) {
		gdk_window_show (window->priv->select_region_dialog->window);
		return;
	}

	g_object_get (G_OBJECT (window->priv->view),
		      "frames_per_pixel", &fpp,
		      "cursor_position", &position,
		      NULL);

	window->priv->select_region_dialog = marlin_select_region_dialog_new (MARLIN_BASE_WINDOW (window));

	g_signal_connect (window->priv->select_region_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &window->priv->select_region_dialog);

	gtk_widget_show (window->priv->select_region_dialog);
}

struct _AdjustVolData {
	MarlinWindow *window;
	GtkAdjustment *adj;
};

void
adjust_volume_response (GtkDialog *dialog,
			guint response_id,
			struct _AdjustVolData *avd)
{
	MarlinWindow *window = avd->window;
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;
	gboolean ret;
	MarlinRange range;
	GError *error = NULL;

	priv = window->priv;

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-process-adjust-volume-dialog");
		return;

	case GTK_RESPONSE_OK:
		/* Store the db value for next time */
		marlin_gconf_set_float ("/apps/marlin/system-state/adjust-volume-db", avd->adj->value);

		marlin_sample_selection_get (avd->window->priv->selection,
					     &(range.coverage), &(range.start), &(range.finish));
		if (range.coverage == MARLIN_COVERAGE_NONE) {
			guint64 frames;

			/* Adjust whole sample */
			g_object_get (G_OBJECT (priv->sample),
				      "total_frames", &frames,
				      NULL);
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Adjust Sample Volume"));

			range.coverage = MARLIN_COVERAGE_BOTH;
			range.start = 0;
			range.finish = frames - 1;
			ret = marlin_sample_adjust_volume_range (priv->sample,
								 avd->adj->value,
								 &range, ctxt,
								 &error);
		} else {
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Adjust Selection Volume"));

			ret = marlin_sample_adjust_volume_range (priv->sample,
								 avd->adj->value,
								 &range, ctxt,
								 &error);
		}

		if (ret == FALSE) {
			g_warning ("marlin_sample_adjust_volume_range failed");
		}

		marlin_undo_manager_context_end (priv->undo, ctxt);
		break;

	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (avd);
}

/* FIXME: Should be in dialogs/ */
void
marlin_window_adjust_volume (MarlinWindow *window)
{
	GtkWidget *editor, *vbox, *label, *scale;
	GtkWidget *hbox, *inner_vbox;
	struct _AdjustVolData *avd;
	char *title, *name;
	float db;

	avd = g_new (struct _AdjustVolData, 1);
	avd->window = window;

	editor = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (editor), FALSE);
/* 	gtk_window_set_default_size (GTK_WINDOW (editor), 360, 130); */
	gtk_window_set_resizable (GTK_WINDOW (editor), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (editor), GTK_WINDOW (window));

	g_object_get (G_OBJECT (window->priv->sample),
		      "name", &name,
		      NULL);

	title = g_strdup_printf (_("Adjust Volume: %s"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (editor), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (editor)->vbox), vbox);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (inner_vbox);
	gtk_box_pack_start (GTK_BOX (hbox), inner_vbox, TRUE, TRUE, 0);

	avd->adj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -43.1, 15.1, 0.1, 10.0, 0.1));
	db = marlin_gconf_get_float ("/apps/marlin/system-state/adjust-volume-db");
	gtk_adjustment_set_value (avd->adj, db);

	scale = gtk_hscale_new (avd->adj);

	g_signal_connect (G_OBJECT (scale), "format_value",
			  G_CALLBACK (marlin_set_volume_digits), NULL);
	gtk_range_set_inverted (GTK_RANGE (scale), TRUE);
	gtk_widget_show (scale);
	gtk_box_pack_start (GTK_BOX (inner_vbox), scale, TRUE, TRUE, 0);

	label = gtk_label_new (_("-\xe2\x88\x9e dB -> +15.0 dB"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);

	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       _("_Adjust"), GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (editor), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (editor), "response",
			  G_CALLBACK (adjust_volume_response), avd);
	gtk_widget_show (editor);
}

struct _SilenceData {
	MarlinWindow *window;
	GtkWidget *length;
	GtkWidget *position;
	GtkWidget *samples;
	GtkWidget *units;
	GtkWidget *marker_hbox;
	GtkWidget *markers;

	MarlinMarkerModel *marker_model;
	GList *marker_list;
};

enum {
	SILENCE_CURSOR_POSITION,
	SILENCE_START,
	SILENCE_END,
	SILENCE_MARKER,
	SILENCE_0
};

static void
insert_silence_response (GtkDialog *dialog,
			 guint response_id,
			 struct _SilenceData *sd)
{
	GtkAdjustment *adj;
	MarlinMarker *marker;
	int history, marker_no;
	guint64 position, num_frames;
	MarlinDisplay display;
	MarlinUndoContext *ctxt;
	gboolean ret;
	GError *error = NULL;

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-process-insert-silence-dialog");
		return;

	case GTK_RESPONSE_OK:
		history = gtk_combo_box_get_active (GTK_COMBO_BOX (sd->position));
		marlin_gconf_set_int ("/apps/marlin/system-state/insert-silence-history", history);

		g_object_get (G_OBJECT (sd->length),
			      "display_as", &display,
			      NULL);
		marlin_gconf_set_int ("/apps/marlin/system-state/insert-silence-display", display);

		switch (history) {
		case SILENCE_CURSOR_POSITION:
			g_object_get (G_OBJECT (sd->window->priv->view),
				      "cursor_position", &position,
				      NULL);
			break;

		case SILENCE_START:
			position = (guint64) 0;
			break;

		case SILENCE_END:
			g_object_get (G_OBJECT (sd->window->priv->sample),
				      "total_frames", &position,
				      NULL);
			break;

		case SILENCE_MARKER:
			marker_no = gtk_combo_box_get_active (GTK_COMBO_BOX (sd->markers));
			marker = g_list_nth_data (sd->marker_list, marker_no);
			g_assert (marker != NULL);
			position = marker->position;
			break;

		default:
			g_assert_not_reached ();
			break;
		}

		g_object_get (G_OBJECT (sd->length),
			      "adjustment", &adj,
			      NULL);
		num_frames = (guint64) adj->value;

		/* Can't insert 0 frames */
		if (num_frames == 0) {
			break;
		}

		ctxt = marlin_undo_manager_context_begin (sd->window->priv->undo,
							  _("Insert Silence"));
		ret = marlin_sample_insert_silence (sd->window->priv->sample, NULL, position, num_frames, ctxt, &error);
		if (ret == FALSE) {
			g_warning ("marlin_sample_insert_silence failed");
		}
		marlin_undo_manager_context_end (sd->window->priv->undo, ctxt);
		break;

	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (sd);
}

static void
make_insert_silence_menu (GtkComboBox *combo,
			  struct _SilenceData *sd)
{
	int i, history;
	static char *positions[] = {
		N_("Cursor Position"),
		N_("Start of Sample"),
		N_("End of Sample"),
		N_("At Marker"),
		NULL
	};

	for (i = 0; positions[i] != NULL; i++) {
		if (i != SILENCE_MARKER || sd->marker_list != NULL) {
			gtk_combo_box_append_text (combo, _(positions[i]));
		}
	}
	history = marlin_gconf_get_int ("/apps/marlin/system-state/insert-silence-history");

	gtk_combo_box_set_active (combo, history);
	gtk_widget_set_sensitive (sd->marker_hbox, (history == SILENCE_MARKER));
}

static void
position_combo_changed (GtkComboBox *combo,
			struct _SilenceData *sd)
{
	int active = gtk_combo_box_get_active (combo);

	gtk_widget_set_sensitive (sd->marker_hbox, (active == SILENCE_MARKER));
}

/* FIXME: Should be in dialogs/ */
void
marlin_window_insert_silence (MarlinWindow *window)
{
	GtkWidget *editor, *vbox, *label;
	GtkWidget *table;
	struct _SilenceData *sd;
	char *title, *name;
	guint rate, display;

	sd = g_new (struct _SilenceData, 1);
	sd->window = window;

	editor = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (editor), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (editor), GTK_WINDOW (window));
/* 	gtk_window_set_default_size (GTK_WINDOW (editor), 450, 110); */
	gtk_window_set_resizable (GTK_WINDOW (editor), FALSE);

	g_object_get (G_OBJECT (window->priv->sample),
		      "name", &name,
		      "sample_rate", &rate,
		      "markers", &sd->marker_model,
		      NULL);

	g_object_get (G_OBJECT (sd->marker_model),
		      "markers", &sd->marker_list,
		      NULL);

	title = g_strdup_printf (_("Insert Silence: %s"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (editor), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (editor)->vbox), vbox);

	table = gtk_table_new (2, 3, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new_with_mnemonic (_("_Duration:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	sd->length = marlin_position_spinner_new ();

	display = marlin_gconf_get_int ("/apps/marlin/system-state/insert-silence-display");
	g_object_set (G_OBJECT (sd->length),
		      "display_as", (MarlinDisplay) display,
		      "rate", rate,
		      NULL);

	gtk_widget_show (sd->length);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), sd->length);
	PACK (table, sd->length, 1, 0, GTK_FILL | GTK_EXPAND);

	sd->units = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (sd->length));
	gtk_widget_show (sd->units);
	PACK (table, sd->units, 2, 0, GTK_FILL);

	label = gtk_label_new_with_mnemonic (_("Insert _Position:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	PACK (table, label, 0, 1, GTK_FILL);

	sd->position = gtk_combo_box_new_text ();
	g_signal_connect (G_OBJECT (sd->position), "changed",
			  G_CALLBACK (position_combo_changed), sd);

	gtk_widget_show (sd->position);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), sd->position);

	gtk_table_attach (GTK_TABLE (table), sd->position,
			  1, 3, 1, 2,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	sd->marker_hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (sd->marker_hbox);
	gtk_table_attach (GTK_TABLE (table), sd->marker_hbox,
			  1, 3, 2, 3,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL | GTK_EXPAND,
			  0, 0);

	label = gtk_label_new_with_mnemonic (_("_Marker:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (sd->marker_hbox), label, FALSE, FALSE, 0);

	sd->markers = gtk_combo_box_new_text ();
	make_marker_combo_menu (GTK_COMBO_BOX (sd->markers), sd->marker_list);

	gtk_widget_show (sd->markers);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), sd->markers);

	gtk_box_pack_start (GTK_BOX (sd->marker_hbox), sd->markers, TRUE, TRUE, 0);

	if (sd->marker_list == NULL) {
		gtk_widget_hide (sd->marker_hbox);
	}

	/* Do this here, so that the marker_hbox will exist */
	make_insert_silence_menu (GTK_COMBO_BOX (sd->position), sd);

	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       _("_Insert Silence"), GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (editor),
			       GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (editor), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (editor), "response",
			  G_CALLBACK (insert_silence_response), sd);

	gtk_widget_show (editor);
}

void
marlin_window_move_cursor_dialog (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	guint64 position;
	guint fpp;

	g_return_if_fail (IS_MARLIN_WINDOW (window));

	priv = window->priv;

	if (priv->move_cursor_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->move_cursor_dialog));
		return;
	}

	g_object_get (G_OBJECT (priv->view),
		      "cursor_position", &position,
		      "frames_per_pixel", &fpp,
		      NULL);

	priv->move_cursor_dialog = marlin_move_cursor_dialog_new (MARLIN_BASE_WINDOW (window));
	g_signal_connect (priv->move_cursor_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->move_cursor_dialog);

	gtk_widget_show (priv->move_cursor_dialog);
}

void
marlin_window_replace (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	MarlinProgram *program = marlin_program_get_default ();
	MarlinSample *clipboard;
	MarlinUndoContext *ctxt;
	MarlinRange range;

	priv = window->priv;

	clipboard = MARLIN_SAMPLE (marlin_program_get_clipboard (program));

	marlin_sample_selection_get (window->priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		g_object_get (G_OBJECT (clipboard),
			      "total_frames", &(range.finish),
			      NULL);

		/* end is one less than the total number of frames */
		range.finish--;
		range.coverage = MARLIN_COVERAGE_BOTH;

		g_object_get (G_OBJECT (priv->view),
			      "cursor_position", &(range.start),
			      NULL);

		range.finish += range.start;
	}

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Replace"));
	/* Replace is really a "mix with the dest at 0%" */
	marlin_sample_mix (priv->sample, clipboard, 0.0, MARLIN_INFINITE_DB,
			   &range, TRUE, NULL, ctxt, NULL);
	marlin_undo_manager_context_end (priv->undo, ctxt);
}

void
marlin_window_selection_scale (MarlinWindow *window,
			       double scale,
			       MarlinUndoContext *ctxt)
{
	guint64 len, frames, start, finish;
	MarlinCoverage coverage;

	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &frames,
		      NULL);

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     &start, &finish);

	len = (finish - start) * scale;
	finish = MIN (start + len, frames - 1);

	marlin_sample_selection_set (window->priv->selection, coverage,
				     start, finish, ctxt);
}

void
marlin_window_selection_shift_left (MarlinWindow *window,
				    MarlinUndoContext *ctxt)
{
	guint64 len, start, finish;
	MarlinCoverage coverage;

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     &start, &finish);

	len = finish - start;
	if ( (gint64) (start - len) < 0) {
		start = (guint64) 0;
	} else {
		start -= len;
	}
	finish = start + len;

	marlin_sample_selection_set (window->priv->selection, coverage,
				     start, finish, ctxt);
}

void
marlin_window_selection_shift_right (MarlinWindow *window,
				     MarlinUndoContext *ctxt)
{
	guint64 len, frames, start, finish;
	MarlinCoverage coverage;

	g_object_get (G_OBJECT (window->priv->sample),
		      "total_frames", &frames,
		      NULL);

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     &start, &finish);
	len = finish - start;

	if ((finish + len) >= frames) {
		finish = frames - 1;
	} else {
		finish += len;
	}
	start = finish - len;

	marlin_sample_selection_set (window->priv->selection, coverage,
				     start, finish, ctxt);
}

void
marlin_window_show_properties (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	if (priv->properties_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->properties_dialog));
		return;
	}

	priv->properties_dialog = marlin_information_dialog_new (MARLIN_BASE_WINDOW (window));
	g_signal_connect (G_OBJECT (priv->properties_dialog), "destroy",
			  G_CALLBACK (gtk_widget_destroyed), &priv->properties_dialog);
	gtk_widget_show (priv->properties_dialog);
}

void
marlin_window_scroll_to_cursor (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	guint64 position;
	guint fpp;
	int width;

	g_object_get (G_OBJECT (priv->view),
		      "cursor_position", &position,
		      "frames_per_pixel", &fpp,
		      NULL);

	width = GTK_WIDGET (priv->view)->allocation.width / 2;

	marlin_sample_view_scroll_to (priv->view, position - (width * fpp));
}

void
marlin_window_vzoom_in (MarlinWindow *window)
{
	g_return_if_fail (IS_MARLIN_WINDOW (window));

	marlin_sample_view_vzoom_in (MARLIN_SAMPLE_VIEW (window->priv->view));
}

void
marlin_window_vzoom_out (MarlinWindow *window)
{
	g_return_if_fail (IS_MARLIN_WINDOW (window));

	marlin_sample_view_vzoom_out (MARLIN_SAMPLE_VIEW (window->priv->view));
}

void
marlin_window_xfade (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	guint64 start_pos;

	g_return_if_fail (IS_MARLIN_WINDOW (window));

	if (priv->xfade_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->xfade_dialog));
		return;
	}

	g_object_get (G_OBJECT (priv->view),
		      "cursor_position", &start_pos,
		      NULL);

	priv->xfade_dialog = marlin_crossfade_dialog_new (MARLIN_BASE_WINDOW (window));
	g_signal_connect (priv->xfade_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->xfade_dialog);

	gtk_widget_show (priv->xfade_dialog);
}

void
marlin_window_xfade_selection (MarlinWindow *window)
{
}

void
marlin_window_change_channels (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	int num_channels;

	if (priv->adjust_channels_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->adjust_channels_dialog));
		return;
	}

	g_object_get (G_OBJECT (window->priv->sample),
		      "channels", &num_channels,
		      NULL);

	if (num_channels == 2) {
		priv->adjust_channels_dialog = marlin_remove_channel_dialog_new (MARLIN_BASE_WINDOW (window));
	} else if (num_channels == 1) {
		priv->adjust_channels_dialog = marlin_add_channel_dialog_new (MARLIN_BASE_WINDOW (window));
	} else {
		g_warning ("Too many channels");
	}

	g_signal_connect (G_OBJECT (priv->adjust_channels_dialog), "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->adjust_channels_dialog);
	gtk_widget_show (priv->adjust_channels_dialog);
}

void
marlin_window_split_sample (MarlinWindow *window)
{
	MarlinSample *original = window->priv->sample;
	MarlinSample *sample;
	MarlinMarkerModel *model;
	GList *markers, *m;
	guint64 num_frames;
	GError *error = NULL;
	MarlinRange range;
	char *name = NULL;

	g_object_get (G_OBJECT (original),
		      "markers", &model,
		      "total_frames", &num_frames,
		      NULL);

	g_object_get (G_OBJECT (model),
		      "markers", &markers,
		      NULL);

	if (markers == NULL) {
		return;
	}

	range.start = 0;
	range.coverage = MARLIN_COVERAGE_BOTH;
	for (m = markers; m; m = m->next) {
		MarlinMarker *marker = m->data;

		range.finish = marker->position - 1;
		sample = marlin_sample_new_from_sample_with_range (original,
								   &range,
								   &error);
		if (sample == NULL) {
			g_warning ("marlin_sample_new_from_sample_with_range failed");
			return;
		}

		if (name == NULL) {
			name = "Marker 0";
		}

		g_object_set (G_OBJECT (sample),
			      "name", name,
			      "dirty", TRUE,
			      NULL);
		marlin_open_window_with_sample (sample);

		name = marker->name;
		range.start = marker->position;
	}

	/* There is one more to split */
	range.finish = num_frames - 1;
	sample = marlin_sample_new_from_sample_with_range (original, &range, &error);
	if (sample == NULL) {
		g_warning ("marlin_sample_new_from_sample_with_range_failed");
		return;
	}

	g_object_set (G_OBJECT (sample),
		      "name", name,
		      "dirty", TRUE,
		      NULL);
	marlin_open_window_with_sample (sample);
}

void
marlin_window_undo (MarlinWindow *window)
{
	marlin_undo_manager_undo (window->priv->undo);
}

void
marlin_window_redo (MarlinWindow *window)
{
	marlin_undo_manager_redo (window->priv->undo);
}

void
marlin_window_burn_sample (MarlinWindow *window)
{
}

void
marlin_window_show_equalizers (MarlinWindow *window,
			       gboolean show)
{
	GConfClient *client = gconf_client_get_default ();

	/* FIXME: Could do some disconnecting of signals
	   so that the vu meters dont get updated when they're hidden */
	if (show) {
		gtk_widget_show (window->priv->left_vu);
		gtk_widget_show (window->priv->right_vu);
	} else {
		gtk_widget_hide (window->priv->left_vu);
		gtk_widget_hide (window->priv->right_vu);
	}

	window->priv->show_vu = show;
	gconf_client_set_bool (client, "/apps/marlin/show-equalizers", show, NULL);
	g_object_unref (G_OBJECT (client));
}

void
marlin_window_move_previous_zero (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	guint64 pos, new_pos;
	MarlinCoverage coverage;

	g_object_get (G_OBJECT (priv->view),
		      "cursor_position", &pos,
		      "cursor_coverage", &coverage,
		      NULL);

	new_pos = marlin_sample_previous_zero (priv->sample, pos, coverage);

	if (pos != new_pos) {
		g_object_set (G_OBJECT (priv->view),
			      "cursor_position", new_pos,
			      NULL);
	}
}

void
marlin_window_move_next_zero (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	MarlinCoverage coverage;
	guint64 pos, new_pos;

	g_object_get (G_OBJECT (priv->view),
		      "cursor_position", &pos,
		      "cursor_coverage", &coverage,
		      NULL);

	new_pos = marlin_sample_next_zero (priv->sample, pos, coverage);

	if (pos != new_pos) {
		g_object_set (G_OBJECT (priv->view),
			      "cursor_position", new_pos,
			      NULL);
	}
}

void
marlin_window_selection_snap_to_zero (MarlinWindow *window,
				      MarlinUndoContext *ctxt)
{
	guint64 start, finish, p, n;
	MarlinCoverage coverage;

	marlin_sample_selection_get (window->priv->selection, &coverage,
				     &start, &finish);

	if (coverage == MARLIN_COVERAGE_NONE) {
		return;
	}

	p = marlin_sample_previous_zero (window->priv->sample, start, coverage);
	n = marlin_sample_next_zero (window->priv->sample, finish, coverage);

	marlin_sample_selection_set (window->priv->selection, coverage,
				     p, n, ctxt);
}

void
marlin_window_undo_history (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	if (priv->undo_history_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->undo_history_dialog));
		return;
	}

	priv->undo_history_dialog = marlin_undo_history_dialog_new (MARLIN_BASE_WINDOW (window));
	g_signal_connect (priv->undo_history_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->undo_history_dialog);

	gtk_widget_show (priv->undo_history_dialog);
}

void
marlin_window_set_level_display (MarlinWindow *window,
				 MarlinScale scale)
{
	marlin_gconf_set_int ("/apps/marlin/scale-display", scale);

	g_object_set (G_OBJECT (window->priv->vruler),
		      "scale", scale,
		      NULL);
}

void
marlin_window_set_meters_display (MarlinWindow *window,
				  MarlinScale scale)
{
	marlin_gconf_set_int ("/apps/marlin/scale-display", scale);

	g_object_set (G_OBJECT (window->priv->left_vu),
		      "scale", scale,
		      NULL);
	g_object_set (G_OBJECT (window->priv->right_vu),
		      "scale", scale,
		      NULL);
}

void
marlin_window_reverse_sample (MarlinWindow *window)
{
	MarlinRange range;
	MarlinWindowPrivate *priv;
	MarlinUndoContext *ctxt;

	priv = window->priv;

	marlin_sample_selection_get (priv->selection, &range.coverage,
				     &range.start, &range.finish);
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &range.finish,
			      NULL);

		range.coverage = MARLIN_COVERAGE_BOTH;
		range.start = 0;
	}

	--range.finish;

	ctxt = marlin_undo_manager_context_begin (priv->undo, _("Reverse Sample"));
	marlin_sample_reverse_range (priv->sample,
				     NULL, &range, ctxt, NULL);
	marlin_undo_manager_context_end (priv->undo, ctxt);
}

void
marlin_window_marker_view_snap_to_ticks (MarlinWindow *window,
					 gboolean snap)
{
	g_object_set (G_OBJECT (window->priv->marker_view),
		      "snap_to_ticks", snap,
		      NULL);
}

void
marlin_window_marker_view_remove_marker (MarlinWindow *window)
{
	MarlinUndoContext *ctxt;

	if (window->priv->current_marker == NULL) {
		return;
	}

	ctxt = marlin_undo_manager_context_begin (window->priv->undo,
						  _("Remove Marker"));
	marlin_marker_model_remove_marker (window->priv->marker_model,
					   window->priv->current_marker,
					   ctxt);
	marlin_undo_manager_context_end (window->priv->undo, ctxt);
}

void
marlin_window_marker_view_goto_marker (MarlinWindow *window)
{
	if (window->priv->current_marker == NULL) {
		return;
	}

	g_object_set (G_OBJECT (window->priv->view),
		      "cursor_position", window->priv->current_marker->position,
		      NULL);
}

void
marlin_window_marker_view_add_marker (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	if (priv->add_marker_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->add_marker_dialog));
		return;
	}

	priv->add_marker_dialog = marlin_add_marker_dialog_new (MARLIN_BASE_WINDOW (window));

	g_signal_connect (priv->add_marker_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->add_marker_dialog);
	gtk_widget_show (priv->add_marker_dialog);
}

void
marlin_window_marker_view_edit_marker (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;

	if (priv->edit_marker_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (priv->edit_marker_dialog));
		return;
	}

	if (priv->current_marker == NULL) {
		return;
	}

	priv->edit_marker_dialog = marlin_edit_marker_dialog_new (MARLIN_BASE_WINDOW (window));
	g_signal_connect (priv->edit_marker_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  &priv->edit_marker_dialog);
	gtk_widget_show (priv->edit_marker_dialog);
}

/**
 * marlin_window_is_empty:
 * @window: A #MarlinWindow object
 *
 * Checks if @window has a sample loaded or not.
 *
 * Return value: TRUE if the window is empty, FALSE otherwise.
 */
gboolean
marlin_window_is_empty (MarlinWindow *window)
{
	MarlinWindowPrivate *priv;
	guint64 frames;

	g_return_val_if_fail (IS_MARLIN_WINDOW (window), FALSE);
	priv = window->priv;

	g_object_get (priv->sample,
		      "total-frames", &frames,
		      NULL);

	return (frames == 0);
}

void
marlin_window_normalize (MarlinWindow *window)
{
	MarlinWindowPrivate *priv = window->priv;
	MarlinUndoContext *ctxt;
	MarlinRange range;
	gboolean ret;
	GError *error = NULL;

	marlin_sample_selection_get (priv->selection, &(range.coverage),
				     &(range.start), &(range.finish));
	if (range.coverage == MARLIN_COVERAGE_NONE) {
		guint64 frames;

		/* Adjust whole sample */
		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &frames,
			      NULL);
		ctxt = marlin_undo_manager_context_begin (priv->undo,
							  _("Normalize Sample"));

		range.coverage = MARLIN_COVERAGE_BOTH;
		range.start = 0;
		range.finish = frames - 1;
	} else {
		ctxt = marlin_undo_manager_context_begin (priv->undo,
							  _("Normalize Selection"));

	}

	ret = marlin_sample_normalize_range (priv->sample, 0.0,
					     &range, ctxt, &error);

	if (ret == FALSE) {
		g_warning ("marlin_sample_adjust_volume_range failed");
	}

	marlin_undo_manager_context_end (priv->undo, ctxt);
}
