/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_WINDOW_H__
#define __MARLIN_WINDOW_H__

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-view.h>

#include "marlin-window-private.h"
#include "marlin-toolbar.h"

#define MARLIN_WINDOW_TYPE (marlin_window_get_type ())
#define MARLIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_WINDOW_TYPE, MarlinWindow))
#define MARLIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_WINDOW_TYPE, MarlinWindowClass))
#define IS_MARLIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_WINDOW_TYPE))
#define IS_MARLIN_WINDOW_CLASS(klasS) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_WINDOW_TYPE))

typedef struct _MarlinWindowClass MarlinWindowClass;
typedef struct _MarlinWindowPrivate MarlinWindowPrivate;

struct _MarlinWindow {
	GtkWindow app_parent;

	MarlinWindowPrivate *priv;
};

struct _MarlinWindowClass {
	GtkWindowClass parent_class;
};

GType marlin_window_get_type (void);
MarlinWindow *marlin_window_new (void);
MarlinWindow *marlin_window_new_from_state (MarlinSample *sample,
					    const char *uuid);
void marlin_window_set_title (MarlinWindow *window,
			      MarlinSample *sample);
void marlin_window_set_media_bar_sensitive (MarlinWindow *window,
					    gboolean sensitive);

void marlin_window_save_state (MarlinWindow *window,
			       gpointer closure);
void marlin_window_remove_state (const char *state_root);
void marlin_window_load_file (MarlinWindow *window,
			      const char *filename);

gboolean marlin_window_can_close (MarlinWindow *window);
void marlin_window_close (MarlinWindow *window);

void marlin_window_play_range (MarlinWindow *window,
			       guint64       start,
			       guint64       end,
			       gboolean      repeating);
void marlin_window_play_sample (MarlinWindow *window, 
				gboolean      repeating);

char *marlin_window_get_filename (MarlinWindow *window);

void marlin_window_zoom_in (MarlinWindow *window);
void marlin_window_zoom_out (MarlinWindow *window);
void marlin_window_show_all (MarlinWindow *window);
void marlin_window_zoom_selection (MarlinWindow *window);
void marlin_window_vzoom_in (MarlinWindow *window);
void marlin_window_vzoom_out (MarlinWindow *window);

GtkActionGroup *marlin_window_get_action_group (MarlinWindow *window);
GtkActionGroup *marlin_window_get_popup_action_group (MarlinWindow *window);

GtkUIManager *marlin_window_get_menu_merge (MarlinWindow *window);
MarlinToolbar *marlin_window_get_toolbar (MarlinWindow *window);

MarlinSampleView *marlin_window_get_view (MarlinWindow *window);

void marlin_window_paste (MarlinWindow *window);
void marlin_window_move_cursor_to (MarlinWindow *window,
				   guint64 absolute_position);
void marlin_window_adjust_volume (MarlinWindow *window);
void marlin_window_insert_silence (MarlinWindow *window);
void marlin_window_mix (MarlinWindow *window);
void marlin_window_replace (MarlinWindow *window);

void marlin_window_selection_scale (MarlinWindow *window,
				    double scale,
				    MarlinUndoContext *ctxt);
void marlin_window_selection_shift_left (MarlinWindow *window,
					 MarlinUndoContext *ctxt);
void marlin_window_selection_shift_right (MarlinWindow *window,
					  MarlinUndoContext *ctxt);

void marlin_window_show_properties (MarlinWindow *window);
void marlin_window_scroll_to_cursor (MarlinWindow *window);

void marlin_window_open (MarlinWindow *window);

void marlin_window_set_icon (MarlinWindow *window, 
			     GdkPixbuf *icon);
void marlin_window_vzoom_in (MarlinWindow *window);
void marlin_window_vzoom_out (MarlinWindow *window);

void marlin_window_select_all (MarlinWindow *window,
			       MarlinUndoContext *ctxt);
void marlin_window_select_none (MarlinWindow *window,
				MarlinUndoContext *ctxt);

void marlin_window_save_sample (MarlinWindow *window);
void marlin_window_save_sample_as (MarlinWindow *window);

void marlin_window_move_cursor (MarlinWindow *window,
				guint64 relative_position);
void marlin_window_move_cursor_to_start (MarlinWindow *window);
void marlin_window_move_cursor_to_end (MarlinWindow *window);

void marlin_window_stop_sample (MarlinWindow *window);
void marlin_window_pause_sample (MarlinWindow *window);

void marlin_window_undo (MarlinWindow *window);
void marlin_window_redo (MarlinWindow *window);
void marlin_window_undo_history (MarlinWindow *window);

void marlin_window_selection_to_clipboard (MarlinWindow *window);
void marlin_window_delete (MarlinWindow *window);
void marlin_window_xfade (MarlinWindow *window);
void marlin_window_xfade_selection (MarlinWindow *window);
void marlin_window_move_previous_zero (MarlinWindow *window);
void marlin_window_move_next_zero (MarlinWindow *window);
void marlin_window_selection_snap_to_zero (MarlinWindow *window,
					   MarlinUndoContext *ctxt);

void marlin_window_crop (MarlinWindow *window);
void marlin_window_move_cursor_dialog (MarlinWindow *window);
void marlin_window_select_region (MarlinWindow *window);

void marlin_window_clear (MarlinWindow *window);
void marlin_window_invert (MarlinWindow *window);
void marlin_window_change_channels (MarlinWindow *window);
void marlin_window_split_sample (MarlinWindow *window);
void marlin_window_reverse_sample (MarlinWindow *window);

void marlin_window_show_equalizers (MarlinWindow *window,
				    gboolean show);
void marlin_window_set_level_display (MarlinWindow *window,
				      MarlinScale scale);
void marlin_window_set_meters_display (MarlinWindow *window,
				       MarlinScale scale);
void marlin_window_marker_view_add_marker (MarlinWindow *window);
void marlin_window_marker_view_remove_marker (MarlinWindow *window);
void marlin_window_marker_view_goto_marker (MarlinWindow *window);
void marlin_window_marker_view_snap_to_ticks (MarlinWindow *window,
					      gboolean snap);
void marlin_window_marker_view_edit_marker (MarlinWindow *window);
gboolean marlin_window_is_empty (MarlinWindow *window);

#endif
