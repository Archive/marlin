/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This file is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_RECENT_ACTION_H__
#define __MARLIN_RECENT_ACTION_H__

#include <gtk/gtk.h>

#define MARLIN_TYPE_RECENT_ACTION (marlin_recent_action_get_type ())
#define MARLIN_RECENT_ACTION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_TYPE_RECENT_ACTION, MarlinRecentAction))

typedef struct _MarlinRecentAction MarlinRecentAction;
typedef struct _MarlinRecentActionClass MarlinRecentActionClass;
typedef struct _MarlinRecentActionPrivate MarlinRecentActionPrivate;

struct _MarlinRecentAction {
	GtkAction parent;
};

struct _MarlinRecentActionClass {
	GtkActionClass parent_class;

	void (*item_activated) (MarlinRecentAction *action,
				GtkRecentInfo *info);
};

GType marlin_recent_action_get_type (void);
MarlinRecentAction *marlin_recent_action_new (const char *name);
MarlinRecentAction *marlin_recent_action_toolbar_new (const char *name,
						      const char *label,
						      const char *stock_id);

#endif
