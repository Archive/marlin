/*
 *  Copyright (C) 2002-2003 Marco Pesenti Gritti
 *  Copyright (c) 2006 Iain Holmes <iain@gnome.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "egg-toolbars-model.h"
#include "eggmarshalers.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <glib/gmarkup.h>

#include <gdk/gdkproperty.h>

enum
{
	ITEM_ADDED,
	ITEM_REMOVED,
	TOOLBAR_ADDED,
	TOOLBAR_CHANGED,
	TOOLBAR_REMOVED,
	LAST_SIGNAL
};

typedef struct
{
	char *name;
	EggTbModelFlags flags;
} EggToolbarsToolbar;

typedef struct
{
	char *id;
	char *type;
	gboolean separator;
} EggToolbarsItem;

static guint egg_toolbars_model_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (EggToolbarsModel, egg_toolbars_model, G_TYPE_OBJECT);

#define GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), EGG_TYPE_TOOLBARS_MODEL, EggToolbarsModelPrivate))

struct EggToolbarsModelPrivate
{
	GNode *toolbars;
};

static char *
egg_toolbars_model_to_string (EggToolbarsModel *t,
			      const char *version)
{
	GNode *l1, *l2, *tl;
	GString *doc;
	char *str;

	g_return_val_if_fail (EGG_IS_TOOLBARS_MODEL (t), NULL);

	tl = t->priv->toolbars;

	doc = g_string_new ("");
	g_string_append_printf (doc, "<toolbars version=\"%s\">\n", version);

	for (l1 = tl->children; l1 != NULL; l1 = l1->next) {
		EggToolbarsToolbar *toolbar = l1->data;

		if ((toolbar->flags & EGG_TB_MODEL_ICONS_ONLY) == EGG_TB_MODEL_ICONS_ONLY) {
			g_string_append_printf (doc, "<toolbar name=\"%s\" style=\"icons-only\">\n",
						toolbar->name);
		} else {
			g_string_append_printf (doc, "<toolbar name=\"%s\">",
						toolbar->name);
		}

		for (l2 = l1->children; l2 != NULL; l2 = l2->next) {
			EggToolbarsItem *item = l2->data;

			if (item->separator) {
				g_string_append (doc, "<separator/>\n");
			} else {
				char *name;

				name = egg_toolbars_model_get_item_name (t, item->type, item->id);
				g_string_append_printf (doc, "<toolitem type=\"%s\" name=\"%s\"/>\n",
							item->type, name);
				g_free (name);
			}
		}

		g_string_append (doc, "</toolbar>\n");
	}

	g_string_append (doc, "</toolbars>");
	str = doc->str;
	g_string_free (doc, FALSE);

	return str;
}

static gboolean
safe_save_xml (const char *xml_file,
	       const char *doc)
{
	char *tmp_file;
	char *old_file;
	gboolean old_exist;
	gboolean retval = TRUE;
	GError *error = NULL;

	tmp_file = g_strconcat (xml_file, ".tmp", NULL);
	old_file = g_strconcat (xml_file, ".old", NULL);

	retval = g_file_set_contents (tmp_file, doc, -1, &error);
	if (retval == FALSE) {
		if (error) {
			g_warning ("Failed to write XML data to %s: %s",
				   tmp_file, error->message);
			g_error_free (error);
		}

		goto failed;
	}

	old_exist = g_file_test (xml_file, G_FILE_TEST_EXISTS);

	if (old_exist) {
		if (rename (xml_file, old_file) < 0) {
			g_warning ("Failed to rename %s to %s", xml_file, old_file);
			retval = FALSE;
			goto failed;
		}
	}

	if (rename (tmp_file, xml_file) < 0) {
		g_warning ("Failed to rename %s to %s", tmp_file, xml_file);

		if (rename (old_file, xml_file) < 0) {
			g_warning ("Failed to restore %s from %s", xml_file, tmp_file);
		}
		retval = FALSE;
		goto failed;
	}

	if (old_exist) {
		if (unlink (old_file) < 0) {
			g_warning ("Failed to delete old file %s", old_file);
		}
	}

	failed:
	g_free (old_file);
	g_free (tmp_file);

	return retval;
}

void
egg_toolbars_model_save (EggToolbarsModel *t,
			 const char *xml_file,
			 const char *version)
{
	char *doc;

	g_return_if_fail (EGG_IS_TOOLBARS_MODEL (t));

	doc = egg_toolbars_model_to_string (t, version);

	safe_save_xml (xml_file, doc);
	g_free (doc);
}

static EggToolbarsToolbar *
toolbars_toolbar_new (const char *name)
{
	EggToolbarsToolbar *toolbar;

	toolbar = g_new0 (EggToolbarsToolbar, 1);
	toolbar->name = g_strdup (name);
	toolbar->flags = 0;

	return toolbar;
}

static EggToolbarsItem *
toolbars_item_new (const char *id,
		   const char *type,
		   gboolean    separator)
{
	EggToolbarsItem *item;

	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (type != NULL, NULL);

	item = g_new0 (EggToolbarsItem, 1);
	item->id = g_strdup (id);
	item->type = g_strdup (type);
	item->separator = separator;

	return item;
}

static void
free_toolbar_node (EggToolbarsToolbar *toolbar)
{
	g_return_if_fail (toolbar != NULL);

	g_free (toolbar->name);
	g_free (toolbar);
}

static void
free_item_node (EggToolbarsItem *item)
{
	g_return_if_fail (item != NULL);

	g_free (item->id);
	g_free (item->type);
	g_free (item);
}

EggTbModelFlags
egg_toolbars_model_get_flags (EggToolbarsModel *t,
			      int               toolbar_position)
{
	GNode *toolbar_node;
	EggToolbarsToolbar *toolbar;

	toolbar_node = g_node_nth_child (t->priv->toolbars, toolbar_position);
	g_return_val_if_fail (toolbar_node != NULL, -1);

	toolbar = toolbar_node->data;

	return toolbar->flags;
}

void
egg_toolbars_model_set_flags (EggToolbarsModel *t,
			      EggTbModelFlags   flags,
			      int               toolbar_position)
{
	GNode *toolbar_node;
	EggToolbarsToolbar *toolbar;

	toolbar_node = g_node_nth_child (t->priv->toolbars, toolbar_position);
	g_return_if_fail (toolbar_node != NULL);

	toolbar = toolbar_node->data;

	toolbar->flags = flags;

	g_signal_emit (G_OBJECT (t),
		       egg_toolbars_model_signals[TOOLBAR_CHANGED],
		       0, toolbar_position);
}

void
egg_toolbars_model_add_separator (EggToolbarsModel *t,
			          int		    toolbar_position,
			          int		    position)
{
	GNode *parent_node;
	GNode *node;
	EggToolbarsItem *item;
	int real_position;

	g_return_if_fail (EGG_IS_TOOLBARS_MODEL (t));

	parent_node = g_node_nth_child (t->priv->toolbars, toolbar_position);
	item = toolbars_item_new ("separator", "separator", TRUE);
	node = g_node_new (item);
	g_node_insert (parent_node, position, node);

	real_position = g_node_child_position (parent_node, node);

	g_signal_emit (G_OBJECT (t), egg_toolbars_model_signals[ITEM_ADDED], 0,
		       toolbar_position, real_position);
}

static gboolean
impl_add_item (EggToolbarsModel    *t,
	       int		    toolbar_position,
	       int		    position,
	       const char          *id,
	       const char          *type)
{
	GNode *parent_node;
	GNode *node;
	EggToolbarsItem *item;
	int real_position;

	g_return_val_if_fail (EGG_IS_TOOLBARS_MODEL (t), FALSE);
	g_return_val_if_fail (id != NULL, FALSE);
	g_return_val_if_fail (type != NULL, FALSE);

	parent_node = g_node_nth_child (t->priv->toolbars, toolbar_position);
	item = toolbars_item_new (id, type, FALSE);
	node = g_node_new (item);
	g_node_insert (parent_node, position, node);

	real_position = g_node_child_position (parent_node, node);

	g_signal_emit (G_OBJECT (t), egg_toolbars_model_signals[ITEM_ADDED], 0,
		       toolbar_position, real_position);

	return TRUE;
}

int
egg_toolbars_model_add_toolbar (EggToolbarsModel *t,
				int               position,
				const char       *name)
{
	GNode *node;
	int real_position;

	g_return_val_if_fail (EGG_IS_TOOLBARS_MODEL (t), -1);

	node = g_node_new (toolbars_toolbar_new (name));
	g_node_insert (t->priv->toolbars, position, node);

	real_position = g_node_child_position (t->priv->toolbars, node);

	g_signal_emit (G_OBJECT (t), egg_toolbars_model_signals[TOOLBAR_ADDED],
		       0, real_position);

	return g_node_child_position (t->priv->toolbars, node);
}

static const char *
get_prop (const char **attribute_names,
	  const char **attribute_values,
	  const char  *name)
{
	int i;

	for (i = 0; attribute_names[i] != NULL; i++) {
		if (strcmp (attribute_names[i], name) == 0) {
			return attribute_values[i];
		}
	}

	return NULL;
}

enum _parse_state {
	NONE,
	AVAILABLE,
	TOOLBAR
};

struct _parse_context {
	EggToolbarsModel *toolbars;
	enum _parse_state state;
	int current_toolbar;
};

static void
start_element_handler (GMarkupParseContext *ctxt,
		       const char          *element_name,
		       const char         **attribute_names,
		       const char         **attribute_values,
		       gpointer             userdata,
		       GError             **error)
{
	struct _parse_context *pc = userdata;
	EggToolbarsModel *t = pc->toolbars;

	if (strcmp (element_name, "toolbar") == 0) {
		int position;
		const char *name, *style;

		name = get_prop (attribute_names, attribute_values, "name");
		position = egg_toolbars_model_add_toolbar (t, -1, name);

		style = get_prop (attribute_names, attribute_values, "style");
		if (style && strcmp (style, "icons-only") == 0) {
			egg_toolbars_model_set_flags (t,
						      EGG_TB_MODEL_ICONS_ONLY,
						      position);
		}

		pc->state = TOOLBAR;
		pc->current_toolbar = position;
	} else if (strcmp (element_name, "toolitem") == 0) {
		const char *name, *type;
		char *id;

		if (pc->state == AVAILABLE) {
			/* We're reading the "available" section here */
			return;
		}

		name = get_prop (attribute_names, attribute_values, "name");
		if (pc->current_toolbar == -1) {
			*error = g_error_new (G_MARKUP_ERROR,
					      G_MARKUP_ERROR_INVALID_CONTENT,
					      "Toolitem '%s' not inside a toolbar", name);
			return;
		}

		type = get_prop (attribute_names, attribute_values, "type");
		if (type == NULL) {
			type = EGG_TOOLBAR_ITEM_TYPE;
		}

		id = egg_toolbars_model_get_item_id (t, type, name);
		if (id != NULL) {
			egg_toolbars_model_add_item (t, pc->current_toolbar,
						     -1, id, type);
		}
		g_free (id);
	} else if (strcmp (element_name, "separator") == 0) {
		if (pc->current_toolbar == -1) {
			*error = g_error_new (G_MARKUP_ERROR,
					      G_MARKUP_ERROR_INVALID_CONTENT,
					      "Separator is not inside a toolbar");
			return;
		}

		egg_toolbars_model_add_separator (t, pc->current_toolbar, -1);
	} else if (strcmp (element_name, "available") == 0) {
		pc->state = AVAILABLE;
	}
}

static void
end_element_handler (GMarkupParseContext *ctxt,
		     const char          *element_name,
		     gpointer             userdata,
		     GError             **error)
{
	struct _parse_context *pc = userdata;

	if (strcmp (element_name, "toolbar") == 0) {
		pc->state = NONE;
		pc->current_toolbar = -1;
	} else if (strcmp (element_name, "available") == 0) {
		pc->state = NONE;
	}
}

static void
text_handler (GMarkupParseContext *ctxt,
	      const char          *text,
	      gsize                text_len,
	      gpointer             userdata,
	      GError             **error)
{
}

static void
passthrough_handler (GMarkupParseContext *ctxt,
		     const char          *passthrough_text,
		     gsize                text_len,
		     gpointer             userdata,
		     GError             **error)
{
}

static void
error_handler (GMarkupParseContext *context,
	       GError              *error,
	       gpointer             userdata)
{
}

static GMarkupParser xml_parser = {
	start_element_handler,
	end_element_handler,
	text_handler,
	passthrough_handler,
	error_handler
};

gboolean
egg_toolbars_model_load (EggToolbarsModel *t,
			 const char *xml_file)
{
	GMarkupParseContext *ctxt;
	struct _parse_context *pc;
	char *contents;
	gsize length;
	GError *error = NULL;
	gboolean ret;

	g_return_val_if_fail (EGG_IS_TOOLBARS_MODEL (t), FALSE);

	ret = g_file_get_contents (xml_file, &contents, &length, &error);
	if (ret == FALSE) {
		if (error) {
			g_warning ("Error loading XML file %s: %s",
				   xml_file, error->message);
			g_error_free (error);
		}
		return FALSE;
	}

	pc = g_new (struct _parse_context, 1);
	pc->toolbars = t;
	pc->state = NONE;
	pc->current_toolbar = -1;

	ctxt = g_markup_parse_context_new (&xml_parser, 0, pc, g_free);

	t->priv->toolbars = g_node_new (NULL);

	error = NULL;
	ret = g_markup_parse_context_parse (ctxt, contents, length, &error);
	if (ret == FALSE) {
		if (error) {
			g_warning ("Error parsing XML file %s: %s",
				   xml_file, error->message);
			g_error_free (error);
		}

		g_node_destroy (t->priv->toolbars);
		g_markup_parse_context_free (ctxt);
		g_free (contents);
		return FALSE;
	}

	g_free (contents);
	g_markup_parse_context_free (ctxt);

	return TRUE;
}

static char *
impl_get_item_id (EggToolbarsModel *t,
		  const char       *type,
		  const char       *name)
{
	if (strcmp (type, EGG_TOOLBAR_ITEM_TYPE) == 0) {
		return g_strdup (name);
	}

	return NULL;
}

static char *
impl_get_item_name (EggToolbarsModel *t,
		    const char       *type,
		    const char       *id)
{
	if (strcmp (type, EGG_TOOLBAR_ITEM_TYPE) == 0) {
		return g_strdup (id);
	}

	return NULL;
}

static char *
impl_get_item_type (EggToolbarsModel *t,
		    GdkAtom type)
{
	if (gdk_atom_intern (EGG_TOOLBAR_ITEM_TYPE, FALSE) == type) {
		return g_strdup (EGG_TOOLBAR_ITEM_TYPE);
	}

	return NULL;
}

static void
egg_toolbars_model_finalize (GObject *object)
{
	EggToolbarsModel *t = EGG_TOOLBARS_MODEL (object);

	/* FIXME free nodes */
	g_node_destroy (t->priv->toolbars);

	G_OBJECT_CLASS (egg_toolbars_model_parent_class)->finalize (object);
}

static void
egg_toolbars_model_class_init (EggToolbarsModelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = egg_toolbars_model_finalize;

	klass->add_item = impl_add_item;
	klass->get_item_id = impl_get_item_id;
	klass->get_item_name = impl_get_item_name;
	klass->get_item_type = impl_get_item_type;

	egg_toolbars_model_signals[ITEM_ADDED] =
		g_signal_new ("item_added",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EggToolbarsModelClass, item_added),
			      NULL, NULL, _egg_marshal_VOID__INT_INT,
			      G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);
	egg_toolbars_model_signals[TOOLBAR_ADDED] =
		g_signal_new ("toolbar_added",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EggToolbarsModelClass, toolbar_added),
			      NULL, NULL, g_cclosure_marshal_VOID__INT,
			      G_TYPE_NONE, 1, G_TYPE_INT);
	egg_toolbars_model_signals[ITEM_REMOVED] =
		g_signal_new ("item_removed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EggToolbarsModelClass, item_removed),
			      NULL, NULL, _egg_marshal_VOID__INT_INT,
			      G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);
	egg_toolbars_model_signals[TOOLBAR_REMOVED] =
		g_signal_new ("toolbar_removed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EggToolbarsModelClass, toolbar_removed),
			      NULL, NULL, g_cclosure_marshal_VOID__INT,
			      G_TYPE_NONE, 1, G_TYPE_INT);
	egg_toolbars_model_signals[TOOLBAR_CHANGED] =
		g_signal_new ("toolbar_changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EggToolbarsModelClass, toolbar_changed),
			      NULL, NULL, g_cclosure_marshal_VOID__INT,
			      G_TYPE_NONE, 1, G_TYPE_INT);

	g_type_class_add_private (object_class, sizeof (EggToolbarsModelPrivate));
}

static void
egg_toolbars_model_init (EggToolbarsModel *t)
{
	t->priv = GET_PRIVATE (t);

	t->priv->toolbars = NULL;
}

EggToolbarsModel *
egg_toolbars_model_new (void)
{
	return EGG_TOOLBARS_MODEL (g_object_new (EGG_TYPE_TOOLBARS_MODEL, NULL));
}

void
egg_toolbars_model_remove_toolbar (EggToolbarsModel   *t,
				   int                 position)
{
	GNode *node;
	EggTbModelFlags flags;

	g_return_if_fail (EGG_IS_TOOLBARS_MODEL (t));

	flags = egg_toolbars_model_get_flags (t, position);

	if (!(flags & EGG_TB_MODEL_NOT_REMOVABLE)) {
		node = g_node_nth_child (t->priv->toolbars, position);
		g_return_if_fail (node != NULL);

		free_toolbar_node (node->data);
		g_node_destroy (node);

		g_signal_emit (G_OBJECT (t),
			       egg_toolbars_model_signals[TOOLBAR_REMOVED],
			       0, position);
	}
}

void
egg_toolbars_model_remove_item (EggToolbarsModel *t,
				int               toolbar_position,
				int               position)
{
	GNode *node, *toolbar;

	g_return_if_fail (EGG_IS_TOOLBARS_MODEL (t));

	toolbar = g_node_nth_child (t->priv->toolbars, toolbar_position);
	g_return_if_fail (toolbar != NULL);

	node = g_node_nth_child (toolbar, position);
	g_return_if_fail (node != NULL);

	free_item_node (node->data);
	g_node_destroy (node);

	g_signal_emit (G_OBJECT (t),
		       egg_toolbars_model_signals[ITEM_REMOVED], 0,
		       toolbar_position, position);
}

int
egg_toolbars_model_n_items (EggToolbarsModel *t,
			    int               toolbar_position)
{
	GNode *toolbar;

	toolbar = g_node_nth_child (t->priv->toolbars, toolbar_position);
	g_return_val_if_fail (toolbar != NULL, -1);

	return g_node_n_children (toolbar);
}

const char *
egg_toolbars_model_item_nth (EggToolbarsModel *t,
			     int	       toolbar_position,
			     int               position,
			     gboolean         *is_separator)
{
	GNode *toolbar;
	GNode *item;
	EggToolbarsItem *idata;

	toolbar = g_node_nth_child (t->priv->toolbars, toolbar_position);
	g_return_val_if_fail (toolbar != NULL, NULL);

	item = g_node_nth_child (toolbar, position);
	g_return_val_if_fail (item != NULL, NULL);

	idata = item->data;

	*is_separator = idata->separator;

	return idata->id;
}

int
egg_toolbars_model_n_toolbars (EggToolbarsModel *t)
{
	return g_node_n_children (t->priv->toolbars);
}

const char *
egg_toolbars_model_toolbar_nth (EggToolbarsModel *t,
				int               position)
{
	GNode *toolbar;
	EggToolbarsToolbar *tdata;

	toolbar = g_node_nth_child (t->priv->toolbars, position);
	g_return_val_if_fail (toolbar != NULL, NULL);

	tdata = toolbar->data;

	return tdata->name;
}

gboolean
egg_toolbars_model_add_item (EggToolbarsModel *t,
			     int	       toolbar_position,
			     int               position,
			     const char       *id,
			     const char       *type)
{
	EggToolbarsModelClass *klass = EGG_TOOLBARS_MODEL_GET_CLASS (t);
	return klass->add_item (t, toolbar_position, position, id, type);
}

char *
egg_toolbars_model_get_item_id (EggToolbarsModel *t,
			        const char       *type,
			        const char       *name)
{
	EggToolbarsModelClass *klass = EGG_TOOLBARS_MODEL_GET_CLASS (t);
	return klass->get_item_id (t, type, name);
}

char *
egg_toolbars_model_get_item_name (EggToolbarsModel *t,
				  const char       *type,
			          const char       *id)
{
	EggToolbarsModelClass *klass = EGG_TOOLBARS_MODEL_GET_CLASS (t);
	return klass->get_item_name (t, type, id);
}

char *
egg_toolbars_model_get_item_type (EggToolbarsModel *t,
				  GdkAtom type)
{
	EggToolbarsModelClass *klass = EGG_TOOLBARS_MODEL_GET_CLASS (t);
	return klass->get_item_type (t, type);
}
