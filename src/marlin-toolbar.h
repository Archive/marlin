/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef MARLIN_TOOLBAR_H
#define MARLIN_TOOLBAR_H

#include <egg-editable-toolbar.h>

#include "marlin-window-private.h"

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct MarlinToolbar MarlinToolbar;
typedef struct MarlinToolbarClass MarlinToolbarClass;

#define MARLIN_TOOLBAR_TYPE             (marlin_toolbar_get_type ())
#define MARLIN_TOOLBAR(obj)             (GTK_CHECK_CAST ((obj), MARLIN_TOOLBAR_TYPE, MarlinToolbar))
#define MARLIN_TOOLBAR_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), MARLIN_TOOLBAR, MarlinToolbarClass))
#define IS_MARLIN_TOOLBAR(obj)          (GTK_CHECK_TYPE ((obj), MARLIN_TOOLBAR_TYPE))
#define IS_MARLIN_TOOLBAR_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), MARLIN_TOOLBAR))

typedef struct MarlinToolbarPrivate MarlinToolbarPrivate;

struct MarlinToolbar
{
        EggEditableToolbar parent_object;
        MarlinToolbarPrivate *priv;
};

struct MarlinToolbarClass
{
        EggEditableToolbarClass parent_class;
};

GType marlin_toolbar_get_type (void);

MarlinToolbar *marlin_toolbar_new (MarlinWindow *window);

G_END_DECLS

#endif
