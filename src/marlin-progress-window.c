/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-utils.h>

#include "marlin-window.h"
#include "marlin-progress-window.h"
#include "marlin-progress-window-icons.h"

/* The delay between the window being created and it being shown...
   Allows short operations to not have to open a window at all */
#define DELAY_TIME 1200

static GdkPixbuf *empty_jar_pixbuf = NULL, *full_jar_pixbuf = NULL;

static char *
make_eta_string (const char *t)
{
	return g_strdup_printf (_("About %s remaining."), t);
}

static void
update_icon (MarlinProgressWindow *window,
	     double fraction)
{
	GdkPixbuf *pixbuf;
	int position;

	position = gdk_pixbuf_get_height (empty_jar_pixbuf) * (1 - fraction);

	if (position == window->progress_jar_position) {
		return;
	}

	window->progress_jar_position = position;

	pixbuf = gdk_pixbuf_copy (empty_jar_pixbuf);
	gdk_pixbuf_copy_area (full_jar_pixbuf,
			      0, position,
			      gdk_pixbuf_get_width (pixbuf),
			      gdk_pixbuf_get_height (pixbuf) - position,
			      pixbuf,
			      0, position);

	marlin_window_set_icon (MARLIN_WINDOW (window->parent_window), pixbuf);
	g_object_unref (pixbuf);
}

static void
update_progress (MarlinProgressWindow *window,
		 int progress)
{
	char *text, *t;
	double dt, length;
	int left;

	if (progress != -1) {
		left = 100 - progress;

		dt = g_timer_elapsed (window->timer, NULL);

		length = (dt / (double) progress) * left;

		t = marlin_ms_to_pretty_time ((int)length * 1000);

		text = make_eta_string (t);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (window->progress_bar),
					   text);
		g_free (text);
	} else {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (window->progress_bar), _("Unknown"));
		progress = 0;
	}

	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (window->progress_bar),
				       (float)progress / 100.0);

	update_icon (window, (float) progress / 100.0);
}

static void
operation_progress (MarlinOperation *operation,
		    int progress,
		    MarlinProgressWindow *window)
{
	update_progress (window, progress);
}

static void
response_callback (GtkDialog *dialog,
		   guint response_id,
		   MarlinProgressWindow *window)
{
	switch (response_id) {
	case GTK_RESPONSE_CANCEL:
		marlin_operation_cancel (window->operation);
		break;

	default:
		break;
	}
}

static void
pause_toggled (GtkToggleButton *tb,
	       MarlinProgressWindow *window)
{
	gboolean pause;

	pause = gtk_toggle_button_get_active (tb);
	if (pause) {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (window->progress_bar), _("(Paused)"));
	} else {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (window->progress_bar), "");
	}

	marlin_operation_pause (window->operation, gtk_toggle_button_get_active (tb));
}

static void
operation_notify (MarlinOperation *operation,
		  GParamSpec *pspec,
		  MarlinProgressWindow *window)
{
	const char *name;

#if 0
	name = g_param_spec_get_name (pspec);
	if (strcmp (name, "description-R") == 0) {
		char *desc;

		g_object_get (G_OBJECT (operation),
			      "description", &desc,
			      NULL);

 		gtk_label_set (GTK_LABEL (window->label), desc);
		g_free (window->format);
		window->format = desc;
	}
#endif
}

static gboolean
show_progress_window (MarlinProgressWindow *window)
{
	g_return_if_fail (window != NULL);

	gtk_widget_show (window->dialog);

	window->visible_id = 0;
	return FALSE;
}

MarlinProgressWindow *
marlin_progress_window_new (MarlinSample *sample,
			    MarlinOperation *operation,
			    GtkWindow *parent_window,
			    const char *primary,
			    const char *secondary)
{
	GtkWidget *vbox;
	char *text;
	MarlinProgressWindow *window;
	GdkCursor *cursor;

	window = g_new0 (MarlinProgressWindow, 1);

	if (empty_jar_pixbuf == NULL) {
		empty_jar_pixbuf = gdk_pixbuf_new_from_inline (-1, progress_jar_empty_icon, FALSE, NULL);
		full_jar_pixbuf = gdk_pixbuf_new_from_inline (-1, progress_jar_full_icon, FALSE, NULL);
	}

	window->sample = sample;
	window->operation = operation;
	window->op_id = g_signal_connect (G_OBJECT (window->operation), "notify",
					  G_CALLBACK (operation_notify), window);

	window->parent_window = GTK_WIDGET (parent_window);
	window->dialog = gtk_dialog_new_with_buttons (primary, parent_window,
						      GTK_DIALOG_DESTROY_WITH_PARENT |
						      GTK_DIALOG_NO_SEPARATOR,
						      GTK_STOCK_CANCEL,
						      GTK_RESPONSE_CANCEL, NULL);
	gtk_window_set_resizable (GTK_WINDOW (window->dialog), FALSE);

 	gtk_container_set_border_width (GTK_CONTAINER (window->dialog), 6);
	window->pause = gtk_toggle_button_new_with_mnemonic (_("_Pause"));
	GTK_WIDGET_SET_FLAGS (window->pause, GTK_CAN_DEFAULT);
	g_signal_connect (window->pause, "toggled",
			  G_CALLBACK (pause_toggled), window);

	gtk_widget_show (window->pause);
	gtk_dialog_add_action_widget (GTK_DIALOG (window->dialog),
				      window->pause, GTK_RESPONSE_NONE);
	gtk_widget_realize (window->dialog);
	cursor = gdk_cursor_new (GDK_WATCH);
	gdk_window_set_cursor (window->dialog->window, cursor);
	gdk_cursor_unref (cursor);

	g_signal_connect (G_OBJECT (window->dialog), "response",
			  G_CALLBACK (response_callback), window);

/* 	gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (window->dialog)->vbox), MARLIN_DIALOG_BORDER_WIDTH); */
	gtk_window_set_transient_for (GTK_WINDOW (window->dialog), parent_window);

	window->signal_id = g_signal_connect (G_OBJECT (operation), "progress",
					      G_CALLBACK (operation_progress), window);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window->dialog)->vbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);

	if (secondary != NULL) {
		text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s", primary, secondary);
	} else {
		text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>", primary);
	}

	window->label = gtk_label_new (text);
	gtk_misc_set_alignment (GTK_MISC (window->label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (window->label), TRUE);
	gtk_label_set_use_markup (GTK_LABEL (window->label), TRUE);
	g_free (text);

	gtk_box_pack_start (GTK_BOX (vbox), window->label, FALSE, FALSE, 0);
	gtk_widget_show (window->label);

	window->progress_bar = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), window->progress_bar,
			    FALSE, FALSE, 0);

	window->sub_process = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (window->sub_process), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (window->sub_process), FALSE);
	gtk_label_set_use_markup (GTK_LABEL (window->sub_process), TRUE);

	gtk_box_pack_start (GTK_BOX (vbox), window->sub_process,
			    FALSE, FALSE, 0);
	/* Hide the sub process label until there is something to display */
	gtk_widget_hide (window->sub_process);

	window->timer = g_timer_new ();

	gtk_widget_show (window->progress_bar);

	window->visible_id = g_timeout_add (DELAY_TIME, (GSourceFunc *) show_progress_window, window);

	/* Set icon for parent window as the progress is set as being
	   transient for it, so it shouldn't show up in the taskbar */
	marlin_window_set_icon (MARLIN_WINDOW (window->parent_window), empty_jar_pixbuf);

	return window;
}

void
marlin_progress_window_destroy (MarlinProgressWindow *window)
{
	g_return_if_fail (window != NULL);

	gtk_widget_destroy (window->dialog);

	g_signal_handler_disconnect (G_OBJECT (window->operation),
				     window->signal_id);
	g_signal_handler_disconnect (G_OBJECT (window->operation),
				     window->op_id);

	if (window->visible_id > 0) {
		g_source_remove (window->visible_id);
	}

	marlin_window_set_icon (MARLIN_WINDOW (window->parent_window), NULL);

	g_timer_destroy (window->timer);
	g_free (window);
}

void
marlin_progress_window_set_sub_process (MarlinProgressWindow *window,
					const char *sub)
{
	char *text;

	g_return_if_fail (window != NULL);

	text = g_strdup_printf ("<i>%s</i>", sub);
	gtk_label_set_markup (GTK_LABEL (window->sub_process), text);
	g_free (text);
}

void
marlin_progress_window_set_progress (MarlinProgressWindow *window,
				     int progress)
{
	g_return_if_fail (window != NULL);

	update_progress (window, progress);
}
