/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005-2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#if 0
#ifdef HAVE_NAUTILUS_BURN

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-stock.h>
#include <marlin/marlin-plugin.h>
#include <marlin/marlin-file-utils.h>

#include "marlin-burn.h"
#include "marlin-extract.h"

/* Plugin setup */
static void
burn_action (GtkAction *action,
	     MarlinBaseWindow *window)
{
	GtkWidget *dialog;

	dialog = marlin_burn_dialog_new (window);

	gtk_window_present (GTK_WINDOW (dialog));
}

static void
cdda_extract (GtkAction *action,
	      MarlinBaseWindow *window)
{
	static GtkWidget *dialog = NULL;

	if (dialog) {
		gtk_window_present (GTK_WINDOW (dialog));
	} else {
		dialog = cdda_extract_new (GTK_WINDOW (window));

		g_object_add_weak_pointer (G_OBJECT (dialog),
					   (gpointer) &dialog);
		gtk_window_present (GTK_WINDOW (dialog));
	}
}

static char *ui_xml =
"<ui><menubar><menu name=\"ToolsMenu\" action=\"Tools\">"
"<placeholder name=\"ToolsPlaceholder1\">"
"<menuitem name=\"ToolsExtractMenu\" action=\"ToolsExtract\"/>"
"<menuitem name=\"ToolsBurnMenu\" action=\"ToolsBurn\"/>"
"</placeholder></menu></menubar></ui>";

GtkActionEntry cd_entries[] = {
	{ "ToolsExtract", MARLIN_STOCK_CDDA_EXTRACT, 
	  N_("Extract Audio From CD..."), NULL,
	  N_("Create a sample from a CD track"),
	  G_CALLBACK (cdda_extract) },
	{ "ToolsBurn", GTK_STOCK_CDROM,
	  N_("Write Audio To CD..."), NULL,
	  N_("Write the sample to a CD"),
	  G_CALLBACK (burn_action) }
};

static void
merge_ui (GtkUIManager *ui_manager,
	  MarlinBaseWindow *window)
{
	GtkActionGroup *ag;

	ag = gtk_action_group_new ("CDActions");
	gtk_action_group_add_actions (ag, cd_entries,
				      G_N_ELEMENTS (cd_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, ag, 0);

	gtk_ui_manager_add_ui_from_string (ui_manager, ui_xml, -1, NULL);
	gtk_ui_manager_ensure_update (ui_manager);
}

static MarlinPluginFuncs funcs = {
	merge_ui,
	NULL
};

static MarlinPluginInfo info = {
	NULL,
	"CD Tools",
	"Iain Holmes <iain@gnome.org>",
	"Tools relating to CDs.",
	VERSION,
	"Copyright (C) 2003 - 2005 Iain Holmes",
	NULL,
	&funcs
};

MarlinPluginInfo *
marlin_plugin_module_register (GModule *module,
			       int *version)
{
	info.module = module;
	info.icon = marlin_file ("marlin-cd.png");

	*version = MARLIN_PLUGIN_VERSION;
	return &info;
}

#endif
#endif
