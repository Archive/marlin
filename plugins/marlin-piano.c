/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include "marlin-piano.h"

#define NOTES_PER_OCTAVE 12

enum {
	NOTE_PRESSED,
	NOTE_RELEASED,
	LAST_SIGNAL
};

enum _notes {
	MARLIN_NOTE_C,
	MARLIN_NOTE_C_SHARP,
	MARLIN_NOTE_D,
	MARLIN_NOTE_D_SHARP,
	MARLIN_NOTE_E,
	MARLIN_NOTE_F,
	MARLIN_NOTE_F_SHARP,
	MARLIN_NOTE_G,
	MARLIN_NOTE_G_SHARP,
	MARLIN_NOTE_A,
	MARLIN_NOTE_A_SHARP,
	MARLIN_NOTE_B
};

enum _key_types {
	MARLIN_LEFT_KEY,
	MARLIN_MIDDLE_KEY,
	MARLIN_RIGHT_KEY,
	MARLIN_BLACK_KEY,
};

typedef enum _key_state {
	NOTE_OFF,
	NOTE_ON
} MarlinPianoKeyState;

struct _octave {
	MarlinPianoKeyState keystate[NOTES_PER_OCTAVE];
};

struct _MarlinPianoPrivate {
	struct _octave *octave;
	int key;
	
	int xofs;
};

static GtkWidgetClass *parent_class = NULL;
static guint32 signals[LAST_SIGNAL] = { 0 };

/* static colours and gcs to be shared between all the widgets */
static gboolean gc_initialised = FALSE;
/* FIXME: Need to take these colours from the gtkrc somehow */
static GdkColor white_note[5] = {
	{ 0, 0xffff, 0xffff, 0xffff },
	{ 0, 0xc4c4, 0xc2c2, 0xbdbd },
	{ 0, 0xeeee, 0xebeb, 0xe7e7 },
	{ 0, 0x4b4b, 0x6969, 0x8383 },
	{ 0, 0xdcdc, 0xdada, 0xd5d5 }
};
static GdkColor black_note[5] = {
	{ 0, 0x0000, 0x0000, 0x0000 },
	{ 0, 0x0000, 0x0000, 0x0000 },
	{ 0, 0x0000, 0x0000, 0x0000 },
	{ 0, 0x4b4b, 0x6969, 0x8383 },
	{ 0, 0x7530, 0x7530, 0x7530 }
};	
static GdkGC *white_note_gc[5];
static GdkGC *black_note_gc[5];

/* Key widths */
#define WHITE_KEY_WIDTH 18

#define MIDDLE_STEM_WIDTH 6
#define SIDE_STEM_WIDTH 13

#define OCTAVE_WIDTH (7 * WHITE_KEY_WIDTH)
#define OCTAVE_HEIGHT 100

#define BLACK_KEY_WIDTH 13
#define WHITE_KEY_HEIGHT OCTAVE_HEIGHT
#define BLACK_KEY_HEIGHT 60

static int octave_notes[NOTES_PER_OCTAVE] = {
	MARLIN_LEFT_KEY,  /* c */
	MARLIN_BLACK_KEY, /* c# */
	MARLIN_MIDDLE_KEY,/* d */
	MARLIN_BLACK_KEY, /* d# */
	MARLIN_RIGHT_KEY, /* e */
	MARLIN_LEFT_KEY,  /* f */
	MARLIN_BLACK_KEY, /* f# */
	MARLIN_MIDDLE_KEY,/* g */
	MARLIN_BLACK_KEY, /* g# */
	MARLIN_MIDDLE_KEY,/* a */
	MARLIN_BLACK_KEY, /* a# */
	MARLIN_RIGHT_KEY  /* b */
};

/* We can use these arrays to get the type of note from the
   octave_notes array */
static int white_notes[7] = {
	MARLIN_NOTE_C,
	MARLIN_NOTE_D,
	MARLIN_NOTE_E,
	MARLIN_NOTE_F,
	MARLIN_NOTE_G,
	MARLIN_NOTE_A,
	MARLIN_NOTE_B
};
static int G_GNUC_UNUSED black_notes[5] = {
	MARLIN_NOTE_C_SHARP,
	MARLIN_NOTE_D_SHARP,
	MARLIN_NOTE_F_SHARP,
	MARLIN_NOTE_G_SHARP,
	MARLIN_NOTE_A_SHARP
};

static int map_notes[NOTES_PER_OCTAVE] = {
	0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6
};

static int note_widths[NOTES_PER_OCTAVE] = {
	SIDE_STEM_WIDTH,   /* C */
	BLACK_KEY_WIDTH - 1,   /* C# */
	MIDDLE_STEM_WIDTH, /* D */
	BLACK_KEY_WIDTH - 1,   /* D# */
	SIDE_STEM_WIDTH,   /* E */
	SIDE_STEM_WIDTH,   /* F */
	BLACK_KEY_WIDTH - 1,   /* F# */
	MIDDLE_STEM_WIDTH, /* G */
	BLACK_KEY_WIDTH - 1,   /* G# */
	MIDDLE_STEM_WIDTH, /* A */
	BLACK_KEY_WIDTH - 1,   /* A# */
	SIDE_STEM_WIDTH    /* B */
};

static char *note_names[NOTES_PER_OCTAVE] = {
	"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
};

static void
finalize (GObject *object)
{
	MarlinPiano *p = MARLIN_PIANO (object);
	MarlinPianoPrivate *priv = p->priv;

	g_free (priv);
}

/* Set up our GCs and colours */
static void
init_gc (GtkWidget *widget)
{
	GtkStateType state_type;

	if (gc_initialised) {
		return;
	}

	gc_initialised = TRUE;

	for (state_type = GTK_STATE_NORMAL; 
	     state_type <= GTK_STATE_INSENSITIVE; ++state_type) {
		GdkColormap *cmap = gtk_widget_get_colormap (widget);

		gdk_colormap_alloc_color (cmap, &white_note[state_type], FALSE, TRUE);
		gdk_colormap_alloc_color (cmap, &black_note[state_type], FALSE, TRUE);
		white_note_gc[state_type] = gdk_gc_new (widget->window);
		gdk_gc_copy (white_note_gc[state_type],
			     widget->style->fg_gc[state_type]);
		gdk_gc_set_foreground (white_note_gc[state_type],
				       &white_note[state_type]);

		black_note_gc[state_type] = gdk_gc_new (widget->window);
		gdk_gc_copy (black_note_gc[state_type],
			     widget->style->fg_gc[state_type]);
		gdk_gc_set_foreground (black_note_gc[state_type],
				       &black_note[state_type]);
	}
}

static void
uninit_gc (GtkWidget *widget)
{
	if (gc_initialised == FALSE) {
		return;
	}

	gc_initialised = FALSE;
}

static void
realize (GtkWidget *widget)
{
	/* Need to make sure our drawable is in place first */
	parent_class->realize (widget);

	/* Set up our colours and GCs */
	init_gc (widget);

	gtk_widget_add_events (widget,
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK |
			       GDK_POINTER_MOTION_MASK);
}

static void
unrealize (GtkWidget *widget)
{
	uninit_gc (widget);

	parent_class->realize (widget);
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *requisition)
{
	requisition->width = OCTAVE_WIDTH;
	requisition->height = OCTAVE_HEIGHT;
}

static void
draw_black_key (MarlinPiano *p,
		GtkStateType state_type,
		int offset)
{
	GtkWidget *widget = GTK_WIDGET (p);

	gdk_draw_rectangle (widget->window,
			    black_note_gc[state_type],
			    TRUE,
			    offset, 0,
			    BLACK_KEY_WIDTH, 
			    BLACK_KEY_HEIGHT);
}

static void
draw_left_key (MarlinPiano *p,
	       GtkStateType state_type,
	       int offset)
{
	GtkWidget *widget = GTK_WIDGET (p);

	++offset;
	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset, 1,
			    SIDE_STEM_WIDTH - 2,
			    BLACK_KEY_HEIGHT - 1);

	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset, BLACK_KEY_HEIGHT,
			    WHITE_KEY_WIDTH - 1,
			    WHITE_KEY_HEIGHT - BLACK_KEY_HEIGHT - 1);
}

static void
draw_middle_key (MarlinPiano *p,
		 GtkStateType state_type,
		 int offset)
{
	GtkWidget *widget = GTK_WIDGET (p);

	++offset;
	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset + MIDDLE_STEM_WIDTH, 1,
			    MIDDLE_STEM_WIDTH - 1,
			    BLACK_KEY_HEIGHT - 1);
	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset, BLACK_KEY_HEIGHT,
			    WHITE_KEY_WIDTH - 1,
			    WHITE_KEY_HEIGHT - BLACK_KEY_HEIGHT - 1);
}

static void
draw_right_key (MarlinPiano *p,
		GtkStateType state_type,
		int offset)
{
	GtkWidget *widget = GTK_WIDGET (p);

	++offset;
	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset + 1 + (WHITE_KEY_WIDTH - SIDE_STEM_WIDTH), 
			    1, SIDE_STEM_WIDTH - 2,
			    BLACK_KEY_HEIGHT - 1);

	gdk_draw_rectangle (widget->window,
			    white_note_gc[state_type],
			    TRUE,
			    offset, BLACK_KEY_HEIGHT,
			    WHITE_KEY_WIDTH - 1,
			    WHITE_KEY_HEIGHT - BLACK_KEY_HEIGHT - 1);
}

static void
draw_outlines (MarlinPiano *p,
	       GdkRectangle *area)
{
	GtkWidget *widget = GTK_WIDGET (p);
	GdkPoint points[4];
	int note;

	points[0].x = 0;
	points[0].y = 0;
	points[1].x = OCTAVE_WIDTH - 1;
	points[1].y = 0;
	points[2].x = OCTAVE_WIDTH - 1;
	points[2].y = OCTAVE_HEIGHT - 1;
	points[3].x = 0;
	points[3].y = OCTAVE_HEIGHT - 1;

	gdk_draw_lines (widget->window, widget->style->black_gc, 
			points, 4);
	
	for (note = 1; note <= 6; ++note) {
		gdk_draw_line (widget->window, 
			       widget->style->black_gc,
			       note * WHITE_KEY_WIDTH, 0,
			       note * WHITE_KEY_WIDTH, OCTAVE_HEIGHT - 1);
	}
}

static void
draw_octave (MarlinPiano *p,
	     struct _octave *octave,
	     GdkRectangle *area,
	     GtkStateType state_type)
{
	GtkWidget *widget = (GtkWidget *) p;
	int note, offset;

	offset = 0;
	gdk_gc_set_clip_rectangle (widget->style->black_gc, area);
	gdk_gc_set_clip_rectangle (white_note_gc[state_type], area);
	gdk_gc_set_clip_rectangle (black_note_gc[state_type], area);

	draw_outlines (p, area);

	/* FIXME: Draws everything everytime */
	for (note = MARLIN_NOTE_C; note <= MARLIN_NOTE_B; ++note) {
		switch (octave_notes[note]) {
		case MARLIN_BLACK_KEY:
			draw_black_key (p, octave->keystate[note], 
					offset - MIDDLE_STEM_WIDTH);
			break;

		case MARLIN_MIDDLE_KEY:
  			draw_middle_key (p, octave->keystate[note], offset);
			offset += WHITE_KEY_WIDTH;
			break;

		case MARLIN_LEFT_KEY:
  			draw_left_key (p, octave->keystate[note], offset);
			offset += WHITE_KEY_WIDTH;
			break;

		case MARLIN_RIGHT_KEY:
  			draw_right_key (p, octave->keystate[note], offset);
			offset += WHITE_KEY_WIDTH;
			break;

		default:
			break;
		}
	}

	gdk_gc_set_clip_rectangle (widget->style->black_gc, NULL);
	gdk_gc_set_clip_rectangle (white_note_gc[state_type], NULL);
	gdk_gc_set_clip_rectangle (black_note_gc[state_type], NULL);
}

static int
find_note (GtkWidget *widget,
	   int x,
	   int y)
{
	int note;

	if (y >= BLACK_KEY_HEIGHT) {
		note = white_notes[x / WHITE_KEY_WIDTH];
	} else {
		int cumulative_width = 0;
		for (note = MARLIN_NOTE_C; note <= MARLIN_NOTE_B; ++note) {
			cumulative_width += note_widths[note];
			if (x < cumulative_width) {
				break;
			}
		}
	}
	
	return note;
}
	
static gboolean
expose_event (GtkWidget *widget,
	      GdkEventExpose *event)
{
	MarlinPiano *p = MARLIN_PIANO (widget);
	draw_octave (p, p->priv->octave, &event->area,
		     GTK_STATE_NORMAL);

	return FALSE;
}

static gboolean
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	int x, y, note;

	x = (int) event->x;
	y = (int) event->y;

	note = find_note (widget, x, y);

	g_print ("Note is %d: '%s'\n", note, note_names[note]);

	return FALSE;
}

static gboolean
button_release_event (GtkWidget *widget,
		      GdkEventButton *event)
{
	return FALSE;
}

static void G_GNUC_UNUSED
invalidate_note (GtkWidget *widget,
		 enum _notes note)
{
	GdkRectangle rect;

	rect.x = map_notes[note] * WHITE_KEY_WIDTH;
	rect.y = 0;
	if (octave_notes[note] == MARLIN_BLACK_KEY) {
		rect.x += SIDE_STEM_WIDTH;
		rect.width = BLACK_KEY_WIDTH;
		rect.height = BLACK_KEY_HEIGHT;
	} else {
		rect.width = WHITE_KEY_WIDTH;
		rect.height = WHITE_KEY_HEIGHT;
	}
	
	gdk_window_invalidate_rect (widget->window, &rect, FALSE);
}

static gboolean
motion_notify_event (GtkWidget *widget,
		     GdkEventMotion *event)
{
#if 0
	MarlinPiano *p = (MarlinPiano *) widget;
	MarlinPianoPrivate *priv = p->priv;

	if (event->x < 0 || event->x >= OCTAVE_WIDTH) {
		return FALSE;
	}

	if (event->y > BLACK_KEY_HEIGHT) {
		int key, offset;
		/* Simple case, nowhere near the black keys */
		key = event->x / WHITE_KEY_WIDTH;
		if (key < MARLIN_NOTE_C || key > MARLIN_NOTE_B) {
			return;
		}

		if (key == priv->key) {
			return TRUE;
		}
		
		offset = key * WHITE_KEY_WIDTH;

		invalidate_note (widget, octave_notes [white_notes [priv->key]]);
		
		priv->key = key;
		invalidate_note (widget, octave_notes [white_notes [key]]);
	}
#endif
	return TRUE;
}

static void
class_init (MarlinPianoClass *klass)
{
	GObjectClass *oclass;
	GtkWidgetClass *wclass;

	oclass = G_OBJECT_CLASS (klass);
	wclass = GTK_WIDGET_CLASS (klass);

	oclass->finalize = finalize;
	
	wclass->realize = realize;
	wclass->unrealize = unrealize;
	wclass->size_request = size_request;
	wclass->expose_event = expose_event;
	wclass->button_press_event = button_press_event;
	wclass->button_release_event = button_release_event;
	wclass->motion_notify_event = motion_notify_event;

	parent_class = g_type_class_peek_parent (klass);

	signals[NOTE_PRESSED] = g_signal_new ("note-pressed",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinPianoClass, note_pressed),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__UINT,
					      G_TYPE_NONE,
					      1, G_TYPE_UINT);
	signals[NOTE_RELEASED] = g_signal_new ("note-released",
					       G_TYPE_FROM_CLASS (klass),
					       G_SIGNAL_RUN_FIRST |
					       G_SIGNAL_NO_RECURSE,
					       G_STRUCT_OFFSET (MarlinPianoClass, note_released),
					       NULL, NULL,
					       g_cclosure_marshal_VOID__UINT,
					       G_TYPE_NONE,
					       1, G_TYPE_UINT);
}

static void
init (MarlinPiano *p)
{
	GTK_WIDGET_SET_FLAGS (p, GTK_CAN_FOCUS);

	p->priv = g_new (MarlinPianoPrivate, 1);
	p->priv->octave = g_new0 (struct _octave, 1);
	p->priv->xofs = 0;
	p->priv->key = -1;
}

GType
marlin_piano_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		GTypeInfo info = {
			sizeof (MarlinPianoClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinPiano), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					       "MarlinPiano",
					       &info, 0);
	}

	return type;
}
