/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>
#if 0
#ifdef HAVE_NAUTILUS_BURN

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <gst/gst.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-sample-element.h>
#include <marlin/marlin-pipeline.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-plugin.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-progress-dialog.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-file-utils.h>

#ifdef HAVE_MUSICBRAINZ /* This stuff is only needed in the Musicbrainz code */
#include <musicbrainz/queries.h>
#include <musicbrainz/mb_c.h>

#include <marlin/marlin-mt.h>
#include <marlin/marlin-msgport.h>
#endif

#include <libnautilus-burn/nautilus-burn-drive-selection.h>

#define CD_FRAMES 75

/* A pipeline class for ripping */
#define CDDA_PIPELINE_TYPE (cdda_pipeline_get_type ())
#define CDDA_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), CDDA_PIPELINE_TYPE, CDDAPipeline))
#define CDDA_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), CDDA_PIPELINE_TYPE, CDDAPipelineClass))

typedef struct _CDDAPipeline CDDAPipeline;
typedef struct _CDDAPipelineClass CDDAPipelineClass;

struct _CDDAPipeline {
	MarlinPipeline pipeline;

	MarlinSample *sample;
	
	GstElement *cdda, *deinterleave, *audioconvert;
	GstPad *cdda_src;
	MarlinSampleElementSink *sink;

	guint notify_id;
};

struct _CDDAPipelineClass {
	MarlinPipelineClass pipeline_class;
};

static GObjectClass *parent_class = NULL;

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_DEVICE,
	PROP_SPEED,
};

static GType cdda_pipeline_get_type (void);

static void
finalize (GObject *object)
{
	CDDAPipeline *pipeline;

	pipeline = CDDA_PIPELINE (object);
	if (pipeline->cdda == NULL) {
		return;
	}

	g_print ("Finalise ripper\n");
	pipeline->cdda = NULL;
	parent_class->finalize (object);
}

static void
sample_notify (MarlinSample *sample,
	       GParamSpec *pspec,
	       MarlinPipeline *pipeline)
{
	if (strcmp (g_param_spec_get_name (pspec), "total-frames") != 0) {
		return;
	}

	marlin_pipeline_progress_changed (pipeline);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
        CDDAPipeline *pipeline;
	const char *dev;

	pipeline = CDDA_PIPELINE (object);

	switch (prop_id) {
	case PROP_SAMPLE:
		if (pipeline->sample != NULL) {
			g_signal_handler_disconnect (G_OBJECT (pipeline->sample),
						     pipeline->notify_id);
			g_object_unref (pipeline->sample);
		}

		pipeline->sample = g_value_get_object (value);
		g_object_ref (G_OBJECT (pipeline->sample));
		
		pipeline->notify_id = g_signal_connect (G_OBJECT (pipeline->sample),
							"notify",
							G_CALLBACK (sample_notify),
							pipeline);

		g_object_set (G_OBJECT (pipeline->sink),
			      "sample", pipeline->sample,
			      NULL);
		break;

	case PROP_DEVICE:
		dev = g_value_get_string (value);
		g_object_set (G_OBJECT (pipeline->cdda),
			      "location", dev,
			      NULL);
		break;

	case PROP_SPEED:
		g_object_set (G_OBJECT (pipeline->cdda),
			      "read-speed", g_value_get_int (value),
			      NULL);
		break;
		
	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	CDDAPipeline *pipeline;

	pipeline = CDDA_PIPELINE (object);

	switch (prop_id) {
	case PROP_SAMPLE:
		g_value_set_object (value, pipeline->sample);
		break;

	case PROP_DEVICE:
		break;

	case PROP_SPEED:
		break;
		
	default:
		break;
	}
}

static int
get_progress (MarlinPipeline *pipeline)
{
	CDDAPipeline *cdda_pipeline;
	gint64 position, start, finish, total;
	GstFormat format = GST_FORMAT_BYTES;
	gboolean ret;

	cdda_pipeline = CDDA_PIPELINE (pipeline);

	ret = gst_pad_query (cdda_pipeline->cdda_src, GST_QUERY_POSITION,
			     &format, &position);
	if (ret == FALSE) {
		return 0;
	}

	format = GST_FORMAT_BYTES;
	ret = gst_pad_query (cdda_pipeline->cdda_src, GST_QUERY_START,
			     &format, &start);
	if (ret == FALSE) {
		return 0;
	}

	format = GST_FORMAT_BYTES;
	ret = gst_pad_query (cdda_pipeline->cdda_src, GST_QUERY_SEGMENT_END,
			     &format, &finish);
	if (ret == FALSE) {
		return 0;
	}

	total = finish - start;
	position = position - start;

	return (int) ((float)(position * 100) / (float) (total));
}
	
static void
class_init (CDDAPipelineClass *klass)
{
	GObjectClass *oclass;
	MarlinPipelineClass *pclass;

	oclass = G_OBJECT_CLASS (klass);
	pclass = MARLIN_PIPELINE_CLASS (klass);

	oclass->finalize = finalize;
	oclass->set_property = set_property;
	oclass->get_property = get_property;

	pclass->get_progress = get_progress;

	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (oclass,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "",
							      "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (oclass,
					 PROP_DEVICE,
					 g_param_spec_string ("device",
							      "", "",
							      "/dev/cdrom",
							      G_PARAM_READWRITE));
	g_object_class_install_property (oclass,
					 PROP_SPEED,
					 g_param_spec_int ("speed",
							    "", "",
							    1, G_MAXINT,
							    40,
							    G_PARAM_READWRITE));
}

static void
oneton_new_pad (GstElement *oneton,
		GstPad *src,
		CDDAPipeline *pipeline)
{
	GstPad *sink, *qsrc, *qsink;
	GstElement *q;
	const char *name;
	char *qname;

	/* Request a new pad */
	sink = gst_element_get_request_pad (GST_ELEMENT (pipeline->sink), "sink%d");
	g_assert (sink != NULL);

	name = gst_pad_get_name (src);
	qname = g_strdup_printf ("queue.%s", name);
	q = gst_element_factory_make ("queue", qname);
	g_free (qname);

	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), q);

	qsrc = gst_element_get_pad (q, "src");
	qsink = gst_element_get_pad (q, "sink");

	if (!gst_pad_link (src, qsink)) {
		g_assert_not_reached ();
		/* FIXME: Shoud set an error here */
	}

	if (!gst_pad_link (qsrc, sink)) {
		g_warning ("Could not connect q and sink");
		/* FIXME: Should set error */
	}

	/* The pipeline is already at PLAYING, so we need to 
	   sync the state to get the queue up to speed */
	gst_element_sync_state_with_parent (q);
}

static void
rip_eos_reached (GstElement *element,
		 CDDAPipeline *pipeline)
{
	MarlinOperation *operation;

	g_object_get (G_OBJECT (pipeline),
		      "operation", &operation,
		      NULL);

	marlin_pipeline_set_eos (MARLIN_PIPELINE (pipeline));
}

static void
init (CDDAPipeline *pipeline)
{
	GstCaps *filtercaps;
	GstBin *sink_bin;

	pipeline->cdda = gst_element_factory_make ("cdparanoia", "cdda-src");
	pipeline->cdda_src = gst_element_get_pad (pipeline->cdda, "src");
	pipeline->audioconvert = gst_element_factory_make ("audioconvert", "cdda-convert");
	pipeline->deinterleave = gst_element_factory_make ("deinterleave", "cdda->deinter");
	g_signal_connect (G_OBJECT (pipeline->deinterleave), "new-pad",
			  G_CALLBACK (oneton_new_pad), pipeline);

	sink_bin = (GstBin *) gst_thread_new ("cdda-sink-thread");
	pipeline->sink = marlin_sample_element_sink_new (NULL);
	g_signal_connect (G_OBJECT (pipeline->sink), "eos",
			  G_CALLBACK (rip_eos_reached), pipeline);
	gst_bin_add (sink_bin, GST_ELEMENT (pipeline->sink));
	
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->cdda);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->audioconvert);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->deinterleave);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), GST_ELEMENT (sink_bin));

	gst_element_link (pipeline->cdda, pipeline->audioconvert);
	
	filtercaps = gst_caps_new_simple ("audio/x-raw-float", NULL);
	gst_element_link_filtered (pipeline->audioconvert,
				   pipeline->deinterleave, filtercaps);
	gst_caps_free (filtercaps);

	pipeline->sample = NULL;
}

static GType
cdda_pipeline_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static GTypeInfo info = {
			sizeof (CDDAPipelineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (CDDAPipeline), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_PIPELINE_TYPE,
					       "CDDAPipeline",
					       &info, 0);
	}

	return type;
}

enum CDDARipMode {
	CDDA_MODE_BY_TRACKS,
	CDDA_MODE_BY_CD,
	CDDA_MODE_BY_RANGE
};

typedef struct _CDDARecordData {
	GtkWidget *dialog;
	
	GtkListStore *trackmodel;
	GtkWidget *optionlist;
	GtkWidget *tracklist;
	GtkWidget *cd_length;
	GtkWidget *cd_drive;
	GtkWidget *speed;
	GtkWidget *start, *finish, *length;
	GtkWidget *range_hbox;
	GtkWidget *by_tracks, *full_cd, *range;
	GtkWidget *parent_window;

	/* Speed */
	GtkAdjustment *speed_adj;
	
	/* GStreamer pipeline elements */
	CDDAPipeline *pipeline;
	MarlinOperation *operation;
	
	/* The sample */
	MarlinSample *sample;

	/* What tracks get ripped? */
	enum CDDARipMode mode;
	
	int no_tracks;
	gint64 *track_starts;
	
	gboolean *rip_tracks;
	GList *rip_list, *rip_iter;

	/* For progress */
	guint32 start_op, end_op;
 	MarlinProgressDialog *progress_window; 

	/* For the markers */
	MarlinMarkerModel *markermodel;

	/* For the CD lookup */
	char *device;
	NautilusBurnDrive *drive;
} CDDARecordData;

enum {
	COL_RIP,
	COL_TRACK,
	COL_NAME,
	COL_ARTIST,
	COL_ALBUM,
	COL_COOLTEXT,
	COL_TYPE,
	COL_START,
	COL_END,
	COL_LENGTH,
	COL_START_FRAMES,
	COL_END_FRAMES,
	COL_EDITABLE,
	COL_LAST
};

static GstFormat track_format = 0;

static void lookup_cd (CDDARecordData *cdda);

static GtkListStore *
create_track_model (void)
{
	GtkListStore *ls;

	ls = gtk_list_store_new (COL_LAST, G_TYPE_BOOLEAN, /* Rip? */
				 G_TYPE_INT, /* Track number */
				 G_TYPE_STRING, /* Name */
				 G_TYPE_STRING, /* Artist */
				 G_TYPE_STRING, /* Album name */
				 G_TYPE_STRING, /* Layout text */
				 G_TYPE_STRING, /* Type */
				 G_TYPE_STRING, /* Start */
				 G_TYPE_STRING, /* End */
				 G_TYPE_STRING, /* Length */
				 G_TYPE_INT64, /* Start CD frames */
				 G_TYPE_INT64, /* End in CD frames */
				 G_TYPE_BOOLEAN); /* Editable? */

	return ls;
}

static char *
make_time (gint64 time_as_gst)
{
	gint64 hours, mins, secs;

	secs = time_as_gst / GST_SECOND;
	hours = secs / 3600;
	mins = secs / 60;
	secs -= ((hours * 3600) + (mins * 60));
	
	return g_strdup_printf ("%lld:%02lld:%02lld", hours, mins, secs);
}

static char *
make_cool_text (const char *artist,
		const char *album,
		int track_num,
		const char *track_name)
{
	return g_strdup_printf ("<span weight=\"bold\">%d: %s</span>\n<span size=\"smaller\">%s - %s</span>",
				track_num, track_name, artist, album);
}

static void
fill_in_model (CDDARecordData *cdda,
	       GstPad *cdda_src,
	       int number_tracks)
{
	int i;
	GstFormat t_format, f;
	gint64 time_count = 0, total_time;

	if (cdda->track_starts == NULL) {
		cdda->track_starts = g_new (gint64, number_tracks);
	}
	
	t_format = gst_format_get_by_nick ("track");

	f = GST_FORMAT_TIME;
	gst_pad_query (cdda_src, GST_QUERY_TOTAL, &f, &total_time);
	total_time /= GST_SECOND;
	
	for (i = 0; i <= number_tracks; i++) {
		GtkTreeIter iter;
		gint64 time;
		GstFormat format;
		gboolean res;

		if (i < number_tracks) {
			format = GST_FORMAT_TIME;
			res = gst_pad_convert (cdda_src, t_format, i,
					       &format, &time);
			time /= GST_SECOND;
		} else {
			time = total_time;
			res = TRUE;
		}
		if (res) {
			if (i > 0) {
				char *start_str, *finish_str, *length_str;
				gint64 length = time - time_count;;
				char *full;
				
				cdda->track_starts[i] = time;
				
				start_str = g_strdup_printf ("%lld:%02lld", time_count / 60, time_count % 60);
				finish_str = g_strdup_printf ("%lld:%02lld", time / 60, time % 60);
				length_str = g_strdup_printf ("%lld:%02lld", length / 60, length % 60);

				full = make_cool_text (_("Unknown Artist"),
						       _("Unknown Album"),
						       i, _("Unknown Track"));
				gtk_list_store_append (GTK_LIST_STORE (cdda->trackmodel), &iter);
				gtk_list_store_set (GTK_LIST_STORE (cdda->trackmodel), &iter,
						    COL_RIP, TRUE,
						    COL_TRACK, i,
						    COL_NAME, _("Unknown Track"),
						    COL_ARTIST, _("Unknown Artist"),
						    COL_ALBUM, _("Unknown Album"),
						    COL_COOLTEXT, full,
						    COL_TYPE, _("Audio"),
						    COL_START, start_str,
						    COL_END, finish_str,
						    COL_LENGTH, length_str,
						    COL_START_FRAMES, (time_count * CD_FRAMES),
						    COL_END_FRAMES, (time * CD_FRAMES),
						    COL_EDITABLE, TRUE,
						    -1);
				g_free (full);
				g_free (start_str);
				g_free (finish_str);
				g_free (length_str);
			} else {
				cdda->track_starts[0] = (gint64) 0;
			}
		} else {
			g_print ("Error getting time for %d\n", i);
		}
		
		time_count = time;
	}
}

static void
fill_in_cdda (CDDARecordData *cdda)
{
	GstElement *pipeline, *cdp, *fakesink;
	GstPad *cdda_src;
	int i;
	char *time_as_str, *label;
	const char *device;
	gint64 no_tracks, length;
	GstFormat s_format, t_format, format;
	gboolean ret;

	device = nautilus_burn_drive_selection_get_device (NAUTILUS_BURN_DRIVE_SELECTION (cdda->cd_drive));

	/* Make a new pipeline */
	pipeline = gst_pipeline_new ("cdda");
	cdp = gst_element_factory_make ("cdparanoia", "src");
	g_object_set (G_OBJECT (cdp),
		      "location", device,
		      NULL);

	fakesink = gst_element_factory_make ("fakesink", "sink");
	
	gst_bin_add (GST_BIN (pipeline), cdp);
	gst_bin_add (GST_BIN (pipeline), fakesink);

	gst_element_connect (cdp, fakesink);
	
	cdda_src = gst_element_get_pad (cdp, "src");

	gst_element_set_state (pipeline, GST_STATE_PAUSED);

	s_format = gst_format_get_by_nick ("sector");
	t_format = gst_format_get_by_nick ("track");

	format = t_format;
	ret = gst_pad_query (cdda_src, GST_QUERY_TOTAL, &format, &no_tracks);
	if (ret == FALSE) {
		g_warning ("Query for total failed.");
		return;
	}

	format = GST_FORMAT_TIME;
	ret = gst_pad_query (cdda_src, GST_QUERY_TOTAL, &format, &length);
	if (ret == FALSE) {
		g_warning ("Query for total failed");
		return;
	}
	
	if (no_tracks == 0) {
		g_print ("There are no tracks on this CD\n");
		return;
	}

	time_as_str = make_time (length);
	label = g_strdup_printf (_("CD Length: %s\n"), time_as_str);
	g_free (time_as_str);
	
	gtk_label_set (GTK_LABEL (cdda->cd_length), label);

	fill_in_model (cdda, cdda_src, no_tracks);

#if 0
	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_NULL);
	g_object_unref (G_OBJECT (pipeline));
#endif
	cdda->rip_tracks = g_new (gboolean, no_tracks);
	/* Set all tracks to be ripped */
	cdda->no_tracks = no_tracks;
	for (i = 0; i < no_tracks; i++) {
		cdda->rip_tracks[i] = TRUE;
	}

#ifdef HAVE_MUSICBRAINZ
	lookup_cd (cdda);
#endif
}

static void
fill_in_marker_model (CDDARecordData *cdda)
{
	int i;
	GtkTreeIter iter;

	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (cdda->trackmodel), &iter);
	for (i = 0; i < cdda->no_tracks; i++) {
		char *name;

		gtk_tree_model_get (GTK_TREE_MODEL (cdda->trackmodel), &iter,
				    COL_NAME, &name,
				    -1);
		if (strcmp (_("Unknown"), name) == 0) {
			g_free (name);
			name = g_strdup_printf (_("Track %d"), i + 1);
		}
		
		marlin_marker_model_add_marker (cdda->markermodel,
						cdda->track_starts[i] * 44100,
						name, NULL);
		g_free (name);
		gtk_tree_model_iter_next (GTK_TREE_MODEL (cdda->trackmodel), &iter);
	}
}

static void
device_changed_cb (GtkWidget *bcs,
		   const char *device_path,
		   CDDARecordData *cdda)
{
	g_free (cdda->track_starts);
	cdda->track_starts = NULL;
	gtk_list_store_clear (cdda->trackmodel);
	fill_in_cdda (cdda);
}

static void
track_toggled (GtkCellRendererToggle *cell,
	       char *path_string,
	       CDDARecordData *cdda)
{
	GtkTreeIter iter;
	GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
	gboolean rip;
	int track;
	
	gtk_tree_model_get_iter (GTK_TREE_MODEL (cdda->trackmodel), &iter, path);
	gtk_tree_model_get (GTK_TREE_MODEL (cdda->trackmodel), &iter,
			    COL_RIP, &rip,
			    COL_TRACK, &track,
			    -1);

	gtk_list_store_set (cdda->trackmodel, &iter, COL_RIP, !rip, -1);
	gtk_tree_path_free (path);

	cdda->rip_tracks[track - 1] = !rip;
}

static void
by_tracks_toggled (GtkToggleButton *tb,
		   CDDARecordData *cdda)
{
	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}

	cdda->mode = CDDA_MODE_BY_TRACKS;
	gtk_widget_set_sensitive (cdda->tracklist, TRUE);
	gtk_widget_set_sensitive (cdda->cd_length, FALSE);
}

static void
full_cd_toggled (GtkToggleButton *tb,
		 CDDARecordData *cdda)
{
	if (gtk_toggle_button_get_active (tb) == FALSE) {
		return;
	}

	cdda->mode = CDDA_MODE_BY_CD;
	gtk_widget_set_sensitive (cdda->tracklist, FALSE);
	gtk_widget_set_sensitive (cdda->cd_length, TRUE);
}

static void
rip_op_finished (MarlinOperation *operation,
		 CDDARecordData *cdda)
{
	g_print ("Rip finished\n");
}

static void
rip_op_started (MarlinOperation *operation,
		CDDARecordData *cdda)
{
 	gtk_widget_show (GTK_WIDGET (cdda->progress_window));
}

static void
rip_op_paused (MarlinOperation *operation,
	       gboolean paused,
	       CDDARecordData *cdda)
{
	marlin_pipeline_set_state (MARLIN_PIPELINE (cdda->pipeline),
				   paused ? GST_STATE_PAUSED : GST_STATE_PLAYING);
}

static void rip_track_in_list (CDDARecordData *cdda);
static void
eos_reached (GstElement *sink,
	     CDDARecordData *cdda)
{
	MarlinProgram *program = marlin_program_get_default ();

	marlin_pipeline_set_state (MARLIN_PIPELINE (cdda->pipeline), GST_STATE_PAUSED);

	g_object_set (G_OBJECT (cdda->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_program_request_new_view (program, cdda->sample);

	/* Rip the next track */
	if (cdda->rip_iter != NULL) {
		cdda->rip_iter = cdda->rip_iter->next;
	}

	
	if (cdda->rip_iter != NULL) {
		rip_track_in_list (cdda);
	} else {
		/* Destroy the pipeline */
		marlin_pipeline_set_state (MARLIN_PIPELINE (cdda->pipeline),
					   GST_STATE_NULL);
	}
}

static void
rip_tracks (CDDARecordData *cdda,
	    guint start,
	    guint end)
{
	char *filename;
	GstEvent *event;

	if (track_format == 0) {
		track_format = gst_format_get_by_nick ("track");
	}

	cdda->sample = marlin_sample_new ();
	filename = g_strdup_printf ("cd-track-%d", start + 1);
	if (cdda->markermodel != NULL) {
		g_object_set (G_OBJECT (cdda->sample),
			      "filename", filename,
			      "markers", cdda->markermodel,
			      NULL);
	} else {
		g_object_set (G_OBJECT (cdda->sample),
			      "filename", filename,
			      NULL);
	}
	g_free (filename);
	
	g_object_set (G_OBJECT (cdda->pipeline),
		      "sample", cdda->sample,
		      NULL);

	marlin_pipeline_set_state (MARLIN_PIPELINE (cdda->pipeline), GST_STATE_PAUSED);
	
	event = gst_event_new_segment_seek (GST_SEEK_METHOD_SET | track_format, (gint64) start, (gint64) end);
	if (gst_pad_send_event (cdda->pipeline->cdda_src, event) == FALSE) {
		g_warning ("Seek range failed");
		return;
	}
	
 	marlin_pipeline_set_state (MARLIN_PIPELINE (cdda->pipeline), GST_STATE_PLAYING);
}

static void
rip_track_in_list (CDDARecordData *cdda)
{
	int track;
	char *text, *p_str, *artist, *title;
	GtkTreePath *path;
	GtkTreeIter iter;

	track = GPOINTER_TO_INT (cdda->rip_iter->data);
	p_str = g_strdup_printf ("%d", track);
	path = gtk_tree_path_new_from_string (p_str);
	g_free (p_str);
	
	gtk_tree_model_get_iter (GTK_TREE_MODEL (cdda->trackmodel),
				 &iter, path);
	gtk_tree_path_free (path);
	
	gtk_tree_model_get (GTK_TREE_MODEL (cdda->trackmodel), &iter,
			    COL_NAME, &title,
			    COL_ARTIST, &artist,
			    -1);
	
	text = g_strdup_printf (_("Extracting %d: %s - %s"), track + 1,
				artist, title);
	g_free (artist);
	g_free (title);
	
/*  	marlin_progress_window_set_sub_process (cdda->progress_window, text); */
	g_free (text);
	
	g_print ("Ripping %d\n", track);
	rip_tracks (cdda, track, track + 1);
}

static void
pipeline_shutdown (GstElement *pipeline,
		   CDDARecordData *cdda)
{
	g_print ("Destroying rip pipeline\n");

	g_object_unref (G_OBJECT (cdda->pipeline));

	g_free (cdda->rip_tracks);
	g_list_free (cdda->rip_list);

/*  	marlin_progress_window_destroy (cdda->progress_window); */
	g_free (cdda);

}

static void
do_rip (CDDARecordData *cdda)
{
	char *text;
	int i, speed;

	speed = (int) (cdda->speed_adj->value);
	g_print ("Using %s at speed %d\n", cdda->device, speed);
	
	cdda->pipeline = g_object_new (CDDA_PIPELINE_TYPE,
 				       "operation", cdda->operation,
				       "device", cdda->device,
				       "speed", speed,
				       NULL);
	/* Connect to the shutdown signal here */
	g_signal_connect (G_OBJECT (cdda->pipeline), "shutdown",
			  G_CALLBACK (pipeline_shutdown), cdda);
	g_signal_connect (G_OBJECT (cdda->pipeline), "eos",
			  G_CALLBACK (eos_reached), cdda);

	text = g_strdup_printf ("<span weight=\"bold\">%s</span>", 
				_("Extracting Audio"));
	cdda->progress_window = marlin_progress_dialog_new (cdda->operation,
							    GTK_WINDOW (cdda->parent_window),
							    text, NULL);
	g_free (text);

      	if (cdda->mode == CDDA_MODE_BY_CD) {
		cdda->rip_iter = NULL;
		cdda->markermodel = marlin_marker_model_new ();
		fill_in_marker_model (cdda);
		
		rip_tracks (cdda, 0, cdda->no_tracks);
		return;
	}
	
	for (i = 0; i < cdda->no_tracks; i++) {
		if (cdda->rip_tracks[i] == TRUE) {
			cdda->rip_list = g_list_append (cdda->rip_list,
							GINT_TO_POINTER (i));
		}
	}

	cdda->rip_iter = cdda->rip_list;

	rip_track_in_list (cdda);
}

static void
dialog_response (GtkDialog *dialog,
		 guint response_id,
		 CDDARecordData *cdda)
{
	switch (response_id) {
	case GTK_RESPONSE_OK:
		do_rip (cdda);
		break;

	case GTK_RESPONSE_CANCEL:
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-cdda-record-dialog");
		return;
		
	default:
		break;
	}

 	gtk_widget_destroy (GTK_WIDGET (dialog));
}

#ifdef HAVE_MUSICBRAINZ
/* Musicbrainz thread stuff */
struct _mbz_msg {
	MarlinMTMsg msg;

	CDDARecordData *cdda;
	GtkListStore *trackmodel;
	
	GList *albums;
	musicbrainz_t mb;
};

typedef struct _AlbumDetails AlbumDetails;
typedef struct _TrackDetails TrackDetails;

struct _TrackDetails {
	AlbumDetails *album;
	int number; /* Track number */
	char *title;
	char *artist;
};

struct _AlbumDetails {
	char *title;
	char *artist;
	int number;
	GList *tracks;
};

static void
track_details_free (TrackDetails *track)
{
	g_free (track->title);
	g_free (track->artist);
	g_free (track);
}

static void
album_details_free (AlbumDetails *album)
{
	GList *p;
	
	g_free (album->title);
	g_free (album->artist);
	for (p = album->tracks; p; p = p->next) {
		track_details_free (p->data);
	}
	g_list_free (album->tracks);
	g_free (album);
}

static void
update_track_model (CDDARecordData *cdda,
		    AlbumDetails *album)
{
	GtkTreeIter iter;
	int i;
	GList *t;

	if (!gtk_tree_model_get_iter_first (GTK_TREE_MODEL (cdda->trackmodel), &iter)) {
		g_print ("Iter failed\n");
		return;
	}
	
	for (i = 1, t = album->tracks; t; t = t->next, i++) {
		TrackDetails *track = t->data;
		char *full;

		full = make_cool_text (track->artist, album->title, i, track->title);
		gtk_list_store_set (cdda->trackmodel, &iter,
				    COL_NAME, track->title,
				    COL_ARTIST, track->artist,
				    COL_ALBUM, album->title,
				    COL_COOLTEXT, full,
				    -1);
		g_free (full);
		gtk_tree_model_iter_next (GTK_TREE_MODEL (cdda->trackmodel), &iter);
	}
}

/* Musicbrainz side */
static void
do_op (MarlinMTMsg *mm)
{
	struct _mbz_msg *op = (struct _mbz_msg *) mm;
	NautilusBurnMediaType type;
	int num_albums, i, j;
	CDDARecordData *cdda = op->cdda;
	GList *albums = NULL;

	type = nautilus_burn_drive_get_media_type (cdda->drive);
	if (type == NAUTILUS_BURN_MEDIA_TYPE_ERROR) {
		g_print ("ERROR type\n");
		op->albums = NULL;
		return;
	}
	
	if (!mb_Query (op->mb, MBQ_GetCDInfo)) {
		g_print ("CDInfo failed\n");
		op->albums = NULL;
		return;
	}

	num_albums = mb_GetResultInt (op->mb, MBE_GetNumAlbums);
	if (num_albums < 1) {
		g_print ("Less than one album\n");
		op->albums = NULL;
		return;
	}

	for (i = 1; i <= num_albums; i++) {
		int num_tracks;
		char data[256];
		AlbumDetails *album;

		mb_Select1 (op->mb, MBS_SelectAlbum, i);
		album = g_new0 (AlbumDetails, 1);

		mb_GetResultData (op->mb, MBE_AlbumGetAlbumName, data, 256);
		album->title = g_strdup (data);

		mb_GetResultData (op->mb, MBE_AlbumGetAlbumArtistId, data, 256);
		mb_GetIDFromURL (op->mb, data, data, 256);
		if (g_ascii_strncasecmp (MBI_VARIOUS_ARTIST_ID, data, 64) == 0) {
			album->artist = g_strdup (_("Various"));
		} else {
			mb_GetResultData1 (op->mb, MBE_AlbumGetArtistName, data, 256, 1);
			album->artist = g_strdup (data);
		}

		num_tracks = mb_GetResultInt (op->mb, MBE_AlbumGetNumTracks);

		for (j = 1; j <= num_tracks; j++) {
			TrackDetails *track;

			track = g_new0 (TrackDetails, 1);

			track->album = album;

			track->number = j;

			if (mb_GetResultData1 (op->mb, MBE_AlbumGetTrackName, data, 256, j)) {
				track->title = g_strdup (data);
			}

			if (mb_GetResultData1 (op->mb, MBE_AlbumGetArtistName, data, 256, j)) {
				track->artist = g_strdup (data);
			}

			album->tracks = g_list_append (album->tracks, track);
			album->number++;
		}
		
		albums = g_list_append (albums, album);
	}
	
	op->albums = albums;
}

/* GUI side */
static void
done_op (MarlinMTMsg *mm)
{
	struct _mbz_msg *op = (struct _mbz_msg *) mm;

	g_print ("Done\n");
	
	if (op->albums == NULL) {
		g_print ("No album\n");
	} else {
		/* FIXME: Don't ignore multiple albums */
		update_track_model (op->cdda, op->albums->data);
	}
}

static void
free_op (MarlinMTMsg *mm)
{
	struct _mbz_msg *op = (struct _mbz_msg *) mm;

	if (op->albums != NULL) {
		GList *p;
		
		for (p = op->albums; p; p = p->next) {
			album_details_free (p->data);
		}
		g_list_free (op->albums);
	}

	g_object_unref (G_OBJECT (op->trackmodel));
	mb_Delete (op->mb);
}

static MarlinMTMsgOp op_funcs = {
	NULL,
	do_op,
	done_op,
	free_op
};

static void
lookup_cd (CDDARecordData *cdda)
{
	struct _mbz_msg *m;

	m = marlin_mt_msg_new (&op_funcs, NULL, sizeof (*m));
	m->cdda = cdda;
	m->trackmodel = cdda->trackmodel;
	
	/* Hold a reference to the trackmodel until we've finished */
	g_object_ref (G_OBJECT (m->trackmodel));
	
	m->mb = mb_New ();
	if (m->mb == NULL) {
		return;
	}
	mb_UseUTF8 (m->mb, TRUE);
	mb_SetDevice (m->mb, cdda->device);
	
	/* Create a new thread to do this stuff */
	marlin_thread_put (marlin_mt_thread_new, (MarlinMsg *) m);
}
#endif /* HAVE_MUSICBRAINZ */

GtkWidget *
cdda_extract_new (GtkWindow *parent_window)
{
	CDDARecordData *cdda;
	GtkWidget *vbox, *by_tracks, *full_cd, *sw;
	GtkWidget *inner_hb, *inner_vb, *inner_vb2, *spacer, *label;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
		
	cdda = g_new0 (CDDARecordData, 1);

	cdda->parent_window = GTK_WIDGET (parent_window);
	cdda->dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (cdda->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (cdda->dialog), parent_window);
	gtk_window_set_title (GTK_WINDOW (cdda->dialog), _("Extract Audio from CD"));
	gtk_window_set_default_size (GTK_WINDOW (cdda->dialog), 445, 475);

	/* VBox inside */
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (cdda->dialog)->vbox), vbox);

	/* Drive */
	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vb, FALSE, FALSE, 0);

	label = marlin_make_title_label (_("CD Drive"));
	gtk_box_pack_start (GTK_BOX (inner_vb), label, FALSE, FALSE, 0);

	inner_hb = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, TRUE, TRUE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	label = gtk_label_new_with_mnemonic (_("_Extract from:"));
	gtk_box_pack_start (GTK_BOX (inner_hb), label, FALSE, FALSE, 0);
	
	cdda->cd_drive = nautilus_burn_drive_selection_new ();
	cdda->device = g_strdup (nautilus_burn_drive_selection_get_device (NAUTILUS_BURN_DRIVE_SELECTION (cdda->cd_drive)));
	cdda->drive = (NautilusBurnDrive *) nautilus_burn_drive_selection_get_drive (NAUTILUS_BURN_DRIVE_SELECTION (cdda->cd_drive));
  	gtk_box_pack_start (GTK_BOX (inner_hb), cdda->cd_drive, TRUE, TRUE, 0); 
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), cdda->cd_drive);

	g_signal_connect (G_OBJECT (cdda->cd_drive), "device-changed",
			  G_CALLBACK (device_changed_cb), cdda);
	
	label = gtk_label_new_with_mnemonic (_("_Speed:"));
	gtk_box_pack_start (GTK_BOX (inner_hb), label, FALSE, FALSE, 0);

	cdda->speed_adj = GTK_ADJUSTMENT (gtk_adjustment_new (40.0, 1.0, 40.0, 1.0, 1.0, 10.0));
	cdda->speed = gtk_spin_button_new (cdda->speed_adj, 1, 0);
	gtk_box_pack_start (GTK_BOX (inner_hb), cdda->speed, FALSE, FALSE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), cdda->speed);

	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vb, TRUE, TRUE, 0);

	/* Track list */
	by_tracks = gtk_radio_button_new_with_mnemonic (NULL, _("Extract by Trac_k"));
	gtk_box_pack_start (GTK_BOX (inner_vb), by_tracks, FALSE, FALSE, 0);

	cdda->by_tracks = by_tracks;
	g_signal_connect (G_OBJECT (by_tracks), "toggled",
			  G_CALLBACK (by_tracks_toggled), cdda);

	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, TRUE, TRUE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	inner_vb2 = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), inner_vb2, TRUE, TRUE, 0);

	cdda->trackmodel = create_track_model ();

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (inner_vb2), sw, TRUE, TRUE, 0);

	cdda->tracklist = gtk_tree_view_new_with_model (GTK_TREE_MODEL (cdda->trackmodel));
 	gtk_container_add (GTK_CONTAINER (sw), cdda->tracklist);
	
	cell = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (cell), "toggled",
			  G_CALLBACK (track_toggled), cdda);
	
	col = gtk_tree_view_column_new_with_attributes (_("Extract"), cell,
							"active", COL_RIP,
							NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("Extract"), cell,
						     "active", COL_RIP, NULL);

	cell = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Track"), cell,
							"markup", COL_COOLTEXT, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("Track"), cell,
						     "markup", COL_COOLTEXT,
						     NULL);

#if 0
	col = gtk_tree_view_column_new_with_attributes (_("Type"), cell,
							"text", COL_TYPE, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("Type"), cell,
						     "text", COL_TYPE, NULL);

	col = gtk_tree_view_column_new_with_attributes (_("Start"), cell,
							"text", COL_START, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("Start"), cell,
						     "text", COL_START, NULL);

	col = gtk_tree_view_column_new_with_attributes (_("End"), cell,
							"text", COL_END, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("End"), cell,
						     "text", COL_END, NULL);
#endif
	col = gtk_tree_view_column_new_with_attributes (_("Length"), cell,
							"text", COL_LENGTH, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (cdda->tracklist),
						     -1, _("Length"), cell,
						     "text", COL_LENGTH, NULL);

	/* Read Full CD */
	inner_vb = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vb, FALSE, FALSE, 0);

	full_cd = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (by_tracks),
								  _("Extract Entire _CD"));
	cdda->full_cd = full_cd;
	gtk_box_pack_start (GTK_BOX (inner_vb), full_cd, FALSE, FALSE, 0);
	g_signal_connect (G_OBJECT (full_cd), "toggled",
			  G_CALLBACK (full_cd_toggled), cdda);
	
	
	inner_hb = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (inner_vb), inner_hb, TRUE, TRUE, 0);

	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (inner_hb), spacer, FALSE, FALSE, 0);

	inner_vb2 = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_hb), inner_vb2, TRUE, TRUE, 0);

	cdda->cd_length = gtk_label_new (_("CD Length: 00:00.00"));
	gtk_widget_set_sensitive (cdda->cd_length, FALSE);
	gtk_misc_set_alignment (GTK_MISC (cdda->cd_length), 0, 0.5);
	
	gtk_box_pack_start (GTK_BOX (inner_vb2), cdda->cd_length, TRUE, TRUE, 0);
	
#if 0
	/* Selected range */
	range = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (full_cd),
								"E_xtract by Range");
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (cdda->dialog)->vbox),
			    range, FALSE, FALSE, 0);

	cdda->range_hbox = gtk_hbox_new (TRUE, 2);

	frame = gtk_frame_new (_("Options"));
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (cdda->dialog)->vbox),
			    frame, FALSE, FALSE, 0);
	inner_vbox = gtk_vbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (frame), inner_vbox);
#endif

	cdda->mode = CDDA_MODE_BY_TRACKS;
	cdda->rip_iter = NULL;
	
	cdda->operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (cdda->operation), "started",
			  G_CALLBACK (rip_op_started), cdda);
	g_signal_connect (G_OBJECT (cdda->operation), "finished",
			  G_CALLBACK (rip_op_finished), cdda);
	g_signal_connect (G_OBJECT (cdda->operation), "paused",
			  G_CALLBACK (rip_op_paused), cdda);

	fill_in_cdda (cdda);

	gtk_dialog_add_button (GTK_DIALOG (cdda->dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	marlin_add_button_to_dialog (GTK_DIALOG (cdda->dialog),
				     _("Extract"), GTK_STOCK_CDROM,
				     GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (cdda->dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);

	gtk_dialog_set_default_response (GTK_DIALOG (cdda->dialog), GTK_RESPONSE_OK);

	g_signal_connect (G_OBJECT (cdda->dialog), "response",
			  G_CALLBACK (dialog_response), cdda);


	gtk_widget_show_all (cdda->dialog);
	return cdda->dialog;
}

#endif /* HAVE_NAUTILUS_BURN */
#endif
