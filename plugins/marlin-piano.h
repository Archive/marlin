/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PIANO_H__
#define __MARLIN_PIANO_H__

#include <gtk/gtk.h>

#define MARLIN_PIANO_TYPE (marlin_piano_get_type ())
#define MARLIN_PIANO(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_PIANO_TYPE, MarlinPiano))
#define MARLIN_PIANO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_PIANO_TYPE, MarlinPianoClass))

typedef struct _MarlinPiano MarlinPiano;
typedef struct _MarlinPianoClass MarlinPianoClass;
typedef struct _MarlinPianoPrivate MarlinPianoPrivate;

struct _MarlinPiano {
	GtkDrawingArea parent;

	MarlinPianoPrivate *priv;
};

struct _MarlinPianoClass {
	GtkDrawingAreaClass parent_class;

	void (* note_pressed) (MarlinPiano *piano,
			       guint note);
	void (* note_released) (MarlinPiano *piano,
				guint note);
};

GType marlin_piano_get_type (void);

#endif
