/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2007 - 2008 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include <math.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <marlin/marlin-channel.h>
#include <marlin/marlin-plugin.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-x-utils.h>

struct _green_noise {
	GtkWidget *dialog;
	MarlinSample *src, *dest;

	GtkAdjustment *sources, *length;
};

#define DEFAULT_WINDOW_WIDTH 10 /* window width in seconds */
#define DEFAULT_INPUTS 128

struct _source {
	MarlinBlock *block;
	guint64 offset;
};

static gboolean
generate_green_noise_channel (MarlinChannel *dest,
			      MarlinChannel *src,
			      int            window_length,
			      int            sources)
{
	guint64 frames_needed, insert_point, offset;
	float *out_buf, total;
	/* float cache[sources]; */

	struct _source *offsets[sources];
	MarlinBlock *block;
	int j, running;

	frames_needed = src->frames;
	insert_point = 0;

	out_buf = g_new (float, MARLIN_BLOCK_SIZE);

	block = src->first;

	offset = 0;
	running = 0;
	total = 0.0;

#if 0
	/* This is a fast algorithm, but it produces very bad green noise
	   It is more like an analogue delay */
	while (frames_needed > 0) {
		guint frames_avail;
		int i;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);

		for (i = 0; i < frames_avail; i++) {
			guint offset_in_block;
			float *buf;

			running++;

			buf = marlin_block_get_frame_data (block);
			offset_in_block = offset - block->start;

			if (running <= sources) {
				total += buf [offset_in_block];
			} else {
				float remove;

				total += buf [offset_in_block];
				if (offset_in_block < sources) {
					guint offset_in_cache;

					offset_in_cache = sources - (sources - offset_in_block);
					remove = cache[offset_in_cache];
				} else {
					remove = buf [offset_in_block - sources];
				}
				total -= remove;
			}

			out_buf[i] = total / sqrt (sources);

			offset++;
			if (offset > block->end) {
				/* Cache the last frames of the buffer so that
				   we can still reference them when we need to
				   subtract them later */
				memcpy (cache,
					buf + (block->num_frames - sources),
					sizeof (float) * sources);
				block = block->next;
			}
		}

		marlin_channel_insert_data (dest, out_buf, frames_avail,
					    insert_point, NULL, NULL);

		frames_needed -= frames_avail;
		insert_point += frames_avail;
	}
#endif

	for (j = 0; j < sources; j++) {
		struct _source *source;

		source = g_new (struct _source, 1);
		source->offset = random () % window_length;
		source->block = marlin_channel_get_block_for_frame
			(src, source->offset);

		offsets[j] = source;
	}

	while (frames_needed > 0) {
		guint frames_avail;
		int i;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		for (i = 0; i < frames_avail; i++) {
			float total = 0.0;

			for (j = 0; j < sources; j++) {
				struct _source *source = offsets[j];
				guint offset_in_block;
				float *buf;

				if (source->block == NULL) {
					/* Skip this dead source */
					continue;
				}

				buf = marlin_block_get_frame_data (source->block);

				offset_in_block = source->offset - source->block->start;
				total += buf [offset_in_block];
				source->offset++;

				if (source->offset > source->block->end) {
					source->block = source->block->next;
				}
			}

			out_buf[i] = total / sqrt (sources);
		}

		marlin_channel_insert_data (dest, out_buf, frames_avail,
					    insert_point, NULL, NULL);

		frames_needed -= frames_avail;
		insert_point += frames_avail;
	}

	g_free (out_buf);

	return TRUE;
}

static gboolean
generate_green_noise (MarlinSample *dest,
		      MarlinSample *src,
		      int           window_length,
		      int           sources)
{
	MarlinChannel *src_channel, *dest_channel = NULL;
	int channels, i;
	gboolean ret;

	g_object_get (G_OBJECT (dest),
		      "channels", &channels,
		      NULL);

	for (i = 0; i < channels; i++) {
		src_channel = marlin_sample_get_channel (src, i);
		dest_channel = marlin_sample_get_channel (dest, i);

		ret = generate_green_noise_channel (dest_channel, src_channel,
						    window_length, sources);
		if (ret == FALSE) {
			return FALSE;
		}
	}

	g_object_set (G_OBJECT (dest),
		      "dirty", TRUE,
		      "total_frames", dest_channel->frames,
		      NULL);
	return TRUE;
}

static void
dialog_response (GtkDialog           *dialog,
		 guint                response_id,
		 struct _green_noise *gnd)
{
	MarlinProgram *program;
	guint channels, rate;
	int window_length;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		g_object_get (G_OBJECT (gnd->src),
			      "channels", &channels,
			      "sample-rate", &rate,
			      NULL);

		gnd->dest = marlin_sample_new ();
		g_object_set (G_OBJECT (gnd->dest),
			      "channels", channels,
			      "sample-rate", rate,
			      NULL);

		window_length = rate * DEFAULT_WINDOW_WIDTH;
		generate_green_noise (gnd->dest, gnd->src, window_length,
				      gnd->sources->value);

		program = marlin_program_get_default ();
		marlin_program_request_new_view (program, gnd->dest);
		break;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (gnd);
}

static GtkWidget *
green_noise_plugin_new (MarlinBaseWindow *window)
{
	struct _green_noise *gnd;
  	GtkWidget *vbox, *table, *label, *scale;

	gnd = g_new (struct _green_noise, 1);
	gnd->dialog = gtk_dialog_new ();
	
	gnd->src = marlin_base_window_get_sample (window);

	gtk_dialog_set_has_separator (GTK_DIALOG (gnd->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (gnd->dialog), 
				      GTK_WINDOW (window));
	gtk_window_set_title (GTK_WINDOW (gnd->dialog),
			      _("Generate Green Noise"));
	gtk_window_set_default_size (GTK_WINDOW (gnd->dialog), 450, 152);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (gnd->dialog)->vbox), vbox);

	table = marlin_make_table (2, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

#if 0
	label = marlin_make_title_label (_("_Window Length:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 0, GTK_FILL);

	gnd->length = GTK_ADJUSTMENT (gtk_adjustment_new (DEFAULT_WINDOW_WIDTH,
							  5.0, 25.0, 1.0, 
							  5.0, 1.0));
	scale = gtk_hscale_new (gnd->length);
	gtk_scale_set_digits (GTK_SCALE (scale), 0);

	gtk_widget_show (scale);
	PACK_FULL (table, scale, 1, 0, 
		   GTK_FILL | GTK_EXPAND, 
		   GTK_FILL | GTK_EXPAND);
#endif
	label = marlin_make_title_label (_("_Number of Sources:"));
	gtk_widget_show (label);
	PACK (table, label, 0, 1, GTK_FILL);

	gnd->sources = GTK_ADJUSTMENT (gtk_adjustment_new (DEFAULT_INPUTS,
							   50.0, 25000.0,
							   1.0, 10.0, 1.0));
	scale = gtk_hscale_new (gnd->sources);
	gtk_scale_set_digits (GTK_SCALE (scale), 0);

	gtk_widget_show (scale);
	PACK_FULL (table, scale, 1, 1,
		   GTK_FILL | GTK_EXPAND,
		   GTK_FILL | GTK_EXPAND);

	gtk_dialog_add_button (GTK_DIALOG (gnd->dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);	
	gtk_dialog_add_button (GTK_DIALOG (gnd->dialog),
			       _("Generate"), GTK_RESPONSE_OK);
	g_signal_connect (G_OBJECT (gnd->dialog), "response",
			  G_CALLBACK (dialog_response), gnd);

	return gnd->dialog;
}

static void
green_noise_plugin (GtkAction        *action,
		    MarlinBaseWindow *window)
{
	static GtkWidget *dialog = NULL;

	if (dialog) {
		gtk_window_present (GTK_WINDOW (dialog));
	} else {
		dialog = green_noise_plugin_new (window);
		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);
		gtk_widget_show (dialog);
	}
}

static const char *ui_xml =
	"<ui><menubar><menu name=\"ToolsMenu\" action=\"Tools\">"
	"<menu name=\"ToolsGenerateMenu\" action=\"ToolsGenerate\">"
	"<placeholder name=\"ToolsGeneratePlaceholder\">"
	"<menuitem name=\"GreenNoiseMenu\" action=\"GenerateGreen\"/>"
	"</placeholder></menu></menu></menubar></ui>";

GtkActionEntry green_noise_entries[] = {
	{ "GenerateGreen", NULL, N_("Green Noise..."), NULL,
	  N_("Generate green noise"),
	  G_CALLBACK (green_noise_plugin) },
};

static void
merge_ui (GtkUIManager     *ui_manager,
	  MarlinBaseWindow *window)
{
	GtkActionGroup *ag;

	ag = gtk_action_group_new ("GreenNoiseActions");
	gtk_action_group_add_actions (ag, green_noise_entries,
				      G_N_ELEMENTS (green_noise_entries),
				      window);

	gtk_ui_manager_insert_action_group (ui_manager, ag, 0);

	gtk_ui_manager_add_ui_from_string (ui_manager, ui_xml, -1, NULL);
}

static MarlinPluginFuncs funcs = {
	merge_ui,
	NULL
};

static MarlinPluginInfo info = {
	NULL,
	"Green Noise Generator",
	"Iain Holmes <iain@gnome.org>",
	"Generates green noise from a sample",
	VERSION,
	"Copyright (C) 2007 Iain Holmes",
	NULL,
	&funcs
};

MarlinPluginInfo *
marlin_plugin_module_register (GModule *module,
			       int     *version)
{
	info.module = module;

	*version = MARLIN_PLUGIN_VERSION;

	return &info;
}
