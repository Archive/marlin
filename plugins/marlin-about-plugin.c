/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <marlin/marlin-plugin.h>
#include <marlin/marlin-file-utils.h>

struct _about_data {
	GtkWidget *dialog;
};

static void
dialog_response (GtkDialog *dialog,
		 guint response_id,
		 struct _about_data *ad)
{
	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free (ad);
}

enum {
	COL_PIXBUF,
	COL_DETAILS,
	COL_LAST
};

static void
fill_in_model (GtkListStore *ls)
{
	GList *p;

	for (p = marlin_plugin_get_list (); p; p = p->next) {
		MarlinPluginInfo *info = p->data;
		char *txt;
		GtkTreeIter iter;
		char *name, *author, *description, *version;
		GdkPixbuf *pb;
		
		name = g_markup_escape_text (info->name, -1);
		author = g_markup_escape_text (info->author, -1);
		version = g_markup_escape_text (info->version, -1);
		description = g_markup_escape_text (info->description, -1);

		txt = g_strdup_printf ("<span weight=\"bold\">%s</span> (%s)\n<small>%s\n%s</small>",
				       name, version, author, description);
		g_free (name);
		g_free (author);
		g_free (version);
		g_free (description);

		if (info->icon != NULL) {
			pb = gdk_pixbuf_new_from_file (info->icon, NULL);
		} else {
			pb = NULL;
		}

		gtk_list_store_append (ls, &iter);
		gtk_list_store_set (ls, &iter,
 				    COL_PIXBUF, pb,
				    COL_DETAILS, txt,
				    -1);
		if (pb) {
			g_object_unref (G_OBJECT (pb));
		}

		g_free (txt);
	}	
}

static GtkWidget *
about_plugin_new (MarlinBaseWindow *window)
{
	struct _about_data *ad;
	GtkWidget *sw, *listview, *vbox;
	GtkListStore *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;

	ad = g_new (struct _about_data, 1);
	ad->dialog = gtk_dialog_new ();

	gtk_dialog_set_has_separator (GTK_DIALOG (ad->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (ad->dialog), 
				      GTK_WINDOW (window));
	gtk_window_set_title (GTK_WINDOW (ad->dialog), _("Extension Details"));
	gtk_window_set_resizable (GTK_WINDOW (ad->dialog), TRUE);
	gtk_window_set_default_size (GTK_WINDOW (ad->dialog), -1, 185);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (ad->dialog)->vbox), vbox);
	
	model = gtk_list_store_new (COL_LAST, GDK_TYPE_PIXBUF, G_TYPE_STRING);
	listview = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	fill_in_model (model);

	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (listview), FALSE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (listview), FALSE);
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (listview));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);

	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer,
							   "pixbuf", COL_PIXBUF,
							   NULL);
	gtk_tree_view_insert_column (GTK_TREE_VIEW (listview), column, -1);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer, 
							   "markup", COL_DETAILS,
							   NULL);
	gtk_tree_view_insert_column (GTK_TREE_VIEW (listview), column, -1);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC);
	
	gtk_container_add (GTK_CONTAINER (sw), listview);
	gtk_widget_show (listview);
	
	gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);
	
	gtk_dialog_add_button (GTK_DIALOG (ad->dialog), 
			       GTK_STOCK_CLOSE,
			       GTK_RESPONSE_CLOSE);

	g_signal_connect (G_OBJECT (ad->dialog), "response",
			  G_CALLBACK (dialog_response), ad);

	return ad->dialog;
}

static void
about_plugin (GtkAction *action,
	      MarlinBaseWindow *window)
{
	static GtkWidget *dialog = NULL;

	if (dialog) {
		gtk_window_present (GTK_WINDOW (dialog));
	} else {
		dialog = about_plugin_new (window);
		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);

		gtk_widget_show (dialog);
	}
}

static char *ui_xml =
"<ui><menubar><menu name=\"HelpMenu\" action=\"Help\">"
"<placeholder name=\"HelpPlaceholder\">"
"<menuitem name=\"HelpAboutPlugin\" action=\"AboutPlugin\"/>"
"</placeholder></menu></menubar></ui>";

GtkActionEntry about_entries[] = {
	{ "AboutPlugin", GTK_STOCK_ABOUT, N_("Extension Details"), NULL,
	  N_("Show details on all the extension loaded"),
	  G_CALLBACK (about_plugin) },
};

static void
merge_ui (GtkUIManager *ui_manager,
	  MarlinBaseWindow *window)
{
	GtkActionGroup *ag;

	ag = gtk_action_group_new ("AboutActions");
	gtk_action_group_add_actions (ag, about_entries,
				      G_N_ELEMENTS (about_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, ag, 0);

	gtk_ui_manager_add_ui_from_string (ui_manager, ui_xml, -1, NULL);
	gtk_ui_manager_ensure_update (ui_manager);
}

static MarlinPluginFuncs funcs = {
	merge_ui,
	NULL
};

static MarlinPluginInfo info = {
	NULL,
	"About",
	"Iain Holmes <iain@gnome.org>",
	"Show details on the extensions currently loaded",
	VERSION,
	"Copyright (C) 2005 Iain Holmes",
	NULL,
	&funcs
};

MarlinPluginInfo *
marlin_plugin_module_register (GModule *module,
			       int *version)
{
	info.module = module;
	info.icon = marlin_file ("marlin-icon.png");
	
	*version = MARLIN_PLUGIN_VERSION;
	return &info;
}
