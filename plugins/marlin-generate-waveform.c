/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#if 0
#include <config.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <marlin/marlin-plugin.h>

#include <gst/gst.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-operation.h>

#include <marlin/marlin-pipeline.h>

#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-program.h>
#include <marlin/marlin-file-utils.h>

/* A pipeline class for generating sine source */
#define SINE_PIPELINE_TYPE (sine_pipeline_get_type ())
#define SINE_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), SINE_PIPELINE_TYPE, SinePipeline))
#define SINE_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), SINE_PIPELINE_TYPE, SinePipelineClass))

typedef struct _SinePipeline SinePipeline;
typedef struct _SinePipelineClass SinePipelineClass;

struct _SinePipeline {
	MarlinPipeline pipeline;

	MarlinSample *sample;

	GstElement *sine, *audioconvert, *oneton, *sink;
	guint rate, channels;

	guint64 required, done;
};

struct _SinePipelineClass {
	MarlinPipelineClass pipeline_class;
};

static GObjectClass *parent_class = NULL;

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_LENGTH,
	PROP_FREQ,
};

static GType sine_pipeline_get_type (void);

static void
finalize (GObject *object)
{
	SinePipeline *pipeline;

	pipeline = SINE_PIPELINE (object);

	parent_class->finalize (object);
}

static void
disconnect_pipeline (SinePipeline *sine)
{
}

static void
connect_pipeline (SinePipeline *sine)
{
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	SinePipeline *sine = SINE_PIPELINE (object);

	switch (prop_id) {
	case PROP_SAMPLE:
		if (sine->sample != NULL) {
/* 			disconnect_pipeline (sine); */
			g_object_unref (G_OBJECT (sine->sample));
		}

		sine->sample = g_value_get_object (value);
		g_object_ref (G_OBJECT (sine->sample));

		g_object_get (G_OBJECT (sine->sample),
			      "sample_rate", &sine->rate,
			      "channels", &sine->channels,
			      NULL);

		g_object_set (G_OBJECT (sine->sink),
			      "sample", sine->sample,
			      NULL);

		connect_pipeline (sine);
		
		break;

	case PROP_LENGTH:
		sine->required = g_value_get_uint64 (value);
		sine->done = 0; /* Reset value */

		g_object_set (G_OBJECT (sine->sink),
			      "limit", sine->required,
			      NULL);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	
}

static int
get_progress (MarlinPipeline *pipeline)
{
	return 0;
}

static void
class_init (SinePipelineClass *klass)
{
	GObjectClass *oclass;
	MarlinPipelineClass *pclass;

	oclass = G_OBJECT_CLASS (klass);
	pclass = MARLIN_PIPELINE_CLASS (klass);

	oclass->finalize = finalize;
	oclass->set_property = set_property;
	oclass->get_property = get_property;

	pclass->get_progress = get_progress;

	parent_class = g_type_class_peek_parent (klass);

	/* Properties */
	g_object_class_install_property (oclass,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (oclass,
					 PROP_LENGTH,
					 g_param_spec_uint64 ("frames",
							      "", "",
							      0, G_MAXUINT64,
							      0, 
							      G_PARAM_READWRITE));
	g_object_class_install_property (oclass,
					 PROP_FREQ,
					 g_param_spec_uint ("frequency",
							    "", "",
							    0, 20000,
							    440,
							    G_PARAM_READWRITE));
}

static void
init (SinePipeline *pipeline)
{
	GstPad *src, *sink;
	GstElement *sink_bin;
	GstCaps *filtercaps;
	gboolean ret;
}

static GType
sine_pipeline_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static GTypeInfo info = {
			sizeof (SinePipelineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (SinePipeline), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_PIPELINE_TYPE,
					       "SinePipeline",
					       &info, 0);
	}

	return type;
}

/* *************** */

struct _GenerateTone {
	GtkWidget *dialog, *length, *freq;
	GtkAdjustment *adj;

	SinePipeline *pipeline;
	MarlinSample *sample;
};

static void
generate_destroy (GtkWidget *widget,
		  struct _GenerateTone *gt)
{
	/* FIXME: Destroy gt->pipeline */
	g_free (gt);
}

static void
eos_reached (MarlinPipeline *pipeline,
	     struct _GenerateTone *gt)
{
	MarlinProgram *program = marlin_program_get_default ();

	marlin_program_request_new_view (program, gt->sample);

	gtk_widget_destroy (gt->dialog);
}

static void
generate_response_cb (GtkDialog *dialog,
		      guint response_id,
		      struct _GenerateTone *gt)
{
	int rate, channels, freq;
	guint64 frames;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		gt->sample = marlin_sample_new ();
		
		rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
		channels = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-channels");
		
		if (channels == 0) {
			channels = 2;
		}
		
		if (rate == 0) {
			rate = 48000;
		}
		
		g_object_set (G_OBJECT (gt->sample),
			      "sample-rate", rate,
			      "channels", channels,
			      NULL);

		frames = (guint64) GTK_SPIN_BUTTON (gt->length)->adjustment->value;
		freq = atoi (gtk_entry_get_text (GTK_ENTRY (gt->freq)));
		gt->pipeline = g_object_new (SINE_PIPELINE_TYPE,
					     "sample", gt->sample,
					     "frames", (guint64) frames,
					     "frequency", freq,
					     NULL);

		g_signal_connect (gt->pipeline, "eos",
				  G_CALLBACK (eos_reached), gt);

		marlin_pipeline_set_state (MARLIN_PIPELINE (gt->pipeline), GST_STATE_PLAYING);
		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-generate-tone-dialog");
		return;

	case GTK_RESPONSE_CANCEL:
	default:
		g_free (gt);
		break;
	}

	gtk_widget_hide (GTK_WIDGET (dialog));
}

GtkWidget *
generate_tone_new (void)
{
	struct _GenerateTone *gt;
	GtkWidget *vbox, *table, *label;
	int display, rate;

	gt = g_new (struct _GenerateTone, 1);
	
	gt->dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (gt->dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (gt->dialog), FALSE);
	gtk_window_set_title (GTK_WINDOW (gt->dialog), _("Generate Tone"));

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (gt->dialog)->vbox), vbox);

	table = marlin_make_table (2, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 6);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
	gtk_widget_show (table);

	label = marlin_make_title_label (_("Frequency:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 0, GTK_FILL);
	gtk_widget_show (label);

	/* FIXME: I'd like a sort of piano keyboard type popup widget thingy
	   for this. Maybe after my exams */
	gt->freq = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), gt->freq,
			  1, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND,
			  GTK_FILL,
			  0, 0);
	gtk_widget_show (gt->freq);

	label = marlin_make_title_label (_("Length:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 1, GTK_FILL);
	gtk_widget_show (label);

	gt->length = marlin_position_spinner_new ();
	display = marlin_gconf_get_int ("/apps/marlin/system-state/position-display");
	rate = marlin_gconf_get_int ("/apps/marlin/system-state/new-sample-rate");
	g_object_set (G_OBJECT (gt->length),
 		      "max_frames", (guint64) (rate * 120),
		      "display_as", (MarlinDisplay) display,
		      "rate", rate,
		      NULL);
	
	gtk_entry_set_activates_default (GTK_ENTRY (gt->length), TRUE);
	PACK (table, gt->length, 1, 1, GTK_FILL | GTK_EXPAND);
	gtk_widget_show (gt->length);

	label = marlin_position_spinner_label (MARLIN_POSITION_SPINNER (gt->length));
	PACK (table, label, 2, 1, GTK_FILL);
	gtk_widget_show (label);

	gtk_dialog_add_button (GTK_DIALOG (gt->dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (gt->dialog),
			       _("Generate Tone"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (gt->dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_default_response (GTK_DIALOG (gt->dialog), GTK_RESPONSE_OK);

	g_signal_connect (gt->dialog, "response",
			  G_CALLBACK (generate_response_cb), gt);
	g_signal_connect (gt->dialog, "destroy",
			  G_CALLBACK (generate_destroy), gt);

	gtk_widget_show (gt->dialog);

	return gt->dialog;
}

static void
generate_waveform (GtkAction *action,
		   MarlinBaseWindow *window)
{
	static GtkWidget *dialog = NULL;

	if (dialog) {
		gtk_window_present (GTK_WINDOW (dialog));
	} else {
		dialog = generate_tone_new ();
		g_signal_connect (dialog, "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);
	}
}

/* Plugin UI Merge stuff */
static char *ui_xml =
"<ui><menubar><menu name=\"ToolsMenu\" action=\"Tools\">"
"<menu name=\"ToolsGenerateMenu\" action=\"ToolsGenerate\">"
"<placeholder name=\"ToolsGeneratePlaceholder\">"
"<menuitem name=\"WaveformMenu\" action=\"GenerateWave\"/>"
"</placeholder></menu></menu></menubar></ui>";

#define MARLIN_PLUGIN_SINE "marlin-plugin-sine"

GtkActionEntry waveform_entries[] = {
	{ "GenerateWave", MARLIN_PLUGIN_SINE, N_("Tone..."), NULL,
	  N_("Generate a tone"),
	  G_CALLBACK (generate_waveform) },
};

static void
merge_ui (GtkUIManager *ui_manager,
	  MarlinBaseWindow *window)
{
	GtkActionGroup *ag;

	ag = gtk_action_group_new ("WaveformActions");
	gtk_action_group_add_actions (ag, waveform_entries,
				      G_N_ELEMENTS (waveform_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, ag, 0);

	gtk_ui_manager_add_ui_from_string (ui_manager, ui_xml, -1, NULL);
	gtk_ui_manager_ensure_update (ui_manager);
}

static MarlinPluginFuncs funcs = {
	merge_ui,
	NULL
};

static MarlinPluginInfo info = {
	NULL,
	"Waveform Generator",
	"Iain Holmes <iain@gnome.org>",
	"Generate sine based waveforms",
	VERSION,
	"Copyright (C) 2005 Iain Holmes",
	NULL,
	&funcs
};

MarlinPluginInfo *
marlin_plugin_module_register (GModule *module,
			       int *version)
{
	GtkIconFactory *factory;
	GtkIconSet *icon_set;
	GdkPixbuf *pixbuf;
	char *filename, *fullname;

	info.module = module;
	info.icon = marlin_file ("marlin-sine-logo.png");

	*version = MARLIN_PLUGIN_VERSION;

	/* Register our default icons */
	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);
	
	filename = g_strconcat ("marlin/", MARLIN_PLUGIN_SINE, ".png", NULL);
	fullname = marlin_file (filename);
	g_free (filename);
	
	pixbuf = gdk_pixbuf_new_from_file (fullname, NULL);
	g_free (fullname);
	
	icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
	gtk_icon_factory_add (factory, MARLIN_PLUGIN_SINE, icon_set);
	gtk_icon_set_unref (icon_set);
	
	g_object_unref (G_OBJECT (pixbuf));
	g_object_unref (G_OBJECT (factory));

	return &info;
}
#endif
