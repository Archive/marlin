/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002-2004 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#if 0
/* If we don't have ncb, then none of this needs to be built */
#ifdef HAVE_NAUTILUS_BURN

#include <unistd.h>

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <libgnomevfs/gnome-vfs-utils.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-pipeline.h>
#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-progress-dialog.h>
#include <marlin/marlin-base-window.h>
#include <marlin/marlin-sample-element.h>

#include <libnautilus-burn/nautilus-burn-recorder.h>
#include <libnautilus-burn/nautilus-burn-drive-selection.h>

/* A pipeline class for converting samples to 2 channel  44100hz wavs
   and writing them to disk */
#define BURN_PIPELINE_TYPE (burn_pipeline_get_type ())
#define BURN_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), BURN_PIPELINE_TYPE, BurnPipeline))
#define BURN_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), BURN_PIPELINE_TYPE, BurnPipelineClass))

typedef struct _BurnPipeline {
	MarlinPipeline pipeline;

	MarlinSample *sample;
	char *filename;

	GstElement *src, *interleave, *audioconvert, *wav, *sink;
} BurnPipeline;

typedef struct _BurnPipelineClass {
	MarlinPipelineClass parent_class;
} BurnPipelineClass;

static GObjectClass *parent_class = NULL;

enum {
	PROP_0,
	PROP_SAMPLE
};

static GType burn_pipeline_get_type (void);

static void
finalize (GObject *object)
{
	BurnPipeline *pipeline;

	pipeline = BURN_PIPELINE (object);

	g_object_unref (G_OBJECT (pipeline->sample));
	g_free (pipeline->filename);

	parent_class->finalize (object);
}

static void
src_new_pad (GstElement *src,
	     GstPad *pad,
	     BurnPipeline *pipeline)
{
	GstPad *isink;

	isink = gst_element_get_request_pad (pipeline->interleave,
					     "sink%d");
	g_assert (isink != NULL);

#if 0
	/* See marlin-save-pipeline.c */
	g_signal_connect (G_OBJECT (isink), "unlinked",
			  G_CALLBACK (ipad_unlinked), pipeline);
#endif

	if (!gst_pad_link (pad, isink)) {
		g_warning ("Caps nego in burn-pipeline-new_pad failed");
		return;
	}
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	BurnPipeline *pipeline = BURN_PIPELINE (object);
	char *filename;

	switch (prop_id) {
	case PROP_SAMPLE:
		if (pipeline->sample != NULL) {
			g_object_unref (G_OBJECT (pipeline->sample));
		}

		pipeline->sample = g_value_get_object (value);
		if (pipeline->sample == NULL) {
			return;
		}

		g_object_ref (G_OBJECT (pipeline->sample));

		g_object_set (G_OBJECT (pipeline->src),
			      "sample", pipeline->sample,
			      NULL);

		g_object_get (G_OBJECT (pipeline->sample),
			      "filename", &filename,
			      NULL);
		g_object_set (G_OBJECT (pipeline->sink),
			      "location", "/tmp/marlin-burn-pipeline%d",
			      NULL);
		g_free (filename);

		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
}

static int
get_progress (MarlinPipeline *pipeline)
{
	BurnPipeline *burn;
	GstPad *src;
	gint64 position, total;
	GstFormat format = GST_FORMAT_BYTES;
	gboolean ret;

	burn = BURN_PIPELINE (pipeline);
	src = gst_element_get_pad (burn->src, "src0");

	ret = gst_pad_query (src, GST_QUERY_POSITION,
			     &format, &position);
	if (ret == FALSE) {
		return 0;
	}

	format = GST_FORMAT_BYTES;
	ret = gst_pad_query (src, GST_QUERY_TOTAL,
			     &format, &total);
	if (ret == FALSE) {
		return 0;
	}

	return (int) ((float)(position * 100) / (float) (total));
}

static void
class_init (BurnPipelineClass *klass)
{
	GObjectClass *oclass;
	MarlinPipelineClass *pclass;

	oclass = G_OBJECT_CLASS (klass);
	pclass = MARLIN_PIPELINE_CLASS (klass);

	oclass->finalize = finalize;
	oclass->set_property = set_property;
	oclass->get_property = get_property;

	pclass->get_progress = get_progress;

	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (oclass,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample", "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_WRITABLE));
}

static void
eos_reached (GstElement *element,
	     MarlinPipeline *pipeline)
{
	marlin_pipeline_set_eos (pipeline);
}

static void
sink_handoff (GstElement *element,
	      MarlinPipeline *pipeline)
{
	marlin_pipeline_progress_changed (pipeline);
}

static void
new_file (GstElement *element,
	  MarlinPipeline *pipeline)
{
	char  *filename;
	struct stat buf;

	g_object_get (G_OBJECT (element),
		      "location", &filename,
		      NULL);
	stat (filename, &buf);
	g_print ("%s size %llu\n", filename, buf.st_size);

	/* THIS IS A MASSIVE UGLY HACK
	   FIXMEFIXMEFIXMEFIXME when interleave can do event compression */
	if (buf.st_size == 0) {
		/* This was an empty file, so overwrite it */
		g_print ("Overwriting %s\n", filename);
		g_object_set (G_OBJECT (element),
			      "location", filename,
			      NULL);
	}
	g_free (filename);
}

static void
init (BurnPipeline *pipeline)
{
	GstCaps *filtercaps;
	gboolean ret;

	pipeline->src = GST_ELEMENT (marlin_sample_element_src_new (NULL));
	gst_element_set_name (pipeline->src, "burn-src");
	g_signal_connect (pipeline->src, "new-pad",
			  G_CALLBACK (src_new_pad), pipeline);
	g_object_set (G_OBJECT (pipeline->src),
		      "send-new-media", TRUE,
		      NULL);

	pipeline->interleave = gst_element_factory_make ("interleave", "burn-interleave");
	pipeline->audioconvert = gst_element_factory_make ("audioconvert", "burn-convert");
	pipeline->sink = gst_element_factory_make ("multifilesink", "burn-sink");
	g_signal_connect (pipeline->sink, "eos",
			  G_CALLBACK (eos_reached), pipeline);
	g_signal_connect (pipeline->sink, "handoff",
			  G_CALLBACK (sink_handoff), pipeline);
	g_signal_connect (pipeline->sink, "newfile",
			  G_CALLBACK (new_file), pipeline);

	pipeline->wav = gst_element_factory_make ("wavenc", "burn-wav");

	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->interleave);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->audioconvert);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->wav);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->sink);
	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), pipeline->src);

	/* Connect some bits together */
	filtercaps = gst_caps_new_simple ("audio/x-raw-float", NULL);
	ret = gst_element_link_filtered (pipeline->interleave, pipeline->audioconvert, filtercaps);
	gst_caps_free (filtercaps);
	g_assert (ret);

	filtercaps = gst_caps_new_simple ("audio/x-raw-int",
					  "channels", G_TYPE_INT, 2,
					  "rate", G_TYPE_INT, 44100,
					  NULL);
	ret = gst_element_link_filtered (pipeline->audioconvert, pipeline->wav, filtercaps);
	gst_caps_free (filtercaps);
	g_assert (ret);

	ret = gst_element_link (pipeline->wav, pipeline->sink);
	g_assert (ret);

	pipeline->sample = NULL;
	pipeline->filename = NULL;
}

static GType
burn_pipeline_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static GTypeInfo info = {
			sizeof (BurnPipelineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (BurnPipeline), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_PIPELINE_TYPE,
					       "MarlinBurnPipeline",
					       &info, 0);
	}

	return type;
}

/** Real stuff **/

struct _BurnDialog {
	GtkWidget *dialog;
	GtkWindow *parent;
	MarlinSample *sample;
	MarlinMarkerModel *markers;

	GtkWidget *drive;
};

struct _BurnDetails {
	MarlinSample *sample;
	MarlinOperation *operation;
	MarlinProgressDialog *progress;
	GtkWidget *dialog;
	GtkWindow *parent;
	GList *tracks;

	BurnPipeline *pipeline;

	NautilusBurnRecorder *recorder;
	NautilusBurnDrive *drive;
};

static void
free_details (struct _BurnDetails *details)
{
	GList *p;

	g_object_unref (G_OBJECT (details->recorder));
	for (p = details->tracks; p; p = p->next) {
		NautilusBurnRecorderTrack *t = p->data;

		unlink (t->contents.audio.filename);
		nautilus_burn_recorder_track_free (t);
	}
	g_list_free (details->tracks);
	gtk_widget_destroy (details->dialog);
	g_free (details);
}

static void
recorder_progress_cb (NautilusBurnRecorder *recorder,
		      double fraction,
		      struct _BurnDetails *details)
{
/* 	marlin_progress_dialog_set_progress (details->progress, (int) (fraction * 100.0)); */
}

static void
burn_action_changed (NautilusBurnRecorder *recorder,
		     NautilusBurnRecorderActions action,
		     NautilusBurnRecorderMedia media,
		     struct _BurnDetails *details)
{
	const char *text;

	text = NULL;

	switch (action) {
	case NAUTILUS_BURN_RECORDER_ACTION_PREPARING_WRITE:
		text = _("Preparing to write CD.");
		break;

	case NAUTILUS_BURN_RECORDER_ACTION_WRITING:
		text = _("Writing CD.");
		break;

	case NAUTILUS_BURN_RECORDER_ACTION_FIXATING:
		text = _("Finishing up.");
		break;

	default:
		break;
	}

	g_object_set (G_OBJECT (details->progress),
		      "secondary_text", text,
		      NULL);
}

static gboolean
recorder_insert_cd_cb (NautilusBurnRecorder *recorder,
		       gboolean is_reload,
		       gboolean can_rewrite,
		       gboolean busy_cd,
		       struct _BurnDetails *details)
{
	GtkWidget *dialog;
	const char *msg, *title;
	char *title_esc, *msg_esc;
	int res;

	if (busy_cd) {
		msg = N_("Please make sure another application is not using the drive.");
		title = N_("Drive is busy");
	} else if (!is_reload && can_rewrite) {
		msg = N_("Please put a rewritable or blank disc into the drive.");
		title = N_("Insert rewritable or blank disc");
	} else if (!is_reload && !can_rewrite) {
		msg = N_("Please insert a blank disc into the drive.");
		title = N_("Insert blank disc");
	} else if (can_rewrite) {
		msg = N_("Please replace the disc in the drive with a rewritable or blank disc.");
		title = N_("Reload rewritable or blank disc");
	} else {
		msg = N_("Please replace the disc in the drive with a blank disc.");
		title = N_("Reload blank disc");
	}

	title_esc = g_markup_escape_text (_(title), -1);
	msg_esc = g_markup_escape_text (_(msg), -1);

	dialog = gtk_message_dialog_new (details->parent,
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_INFO,
					 GTK_BUTTONS_NONE,
					 "<span weight=\"bold\" size=\"larger\">%s</span>\n%s", title_esc, msg_esc);
	gtk_window_set_title (GTK_WINDOW (dialog), title_esc);
	g_free (title_esc);
	g_free (msg_esc);

	gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				_("Retry disc"), GTK_RESPONSE_OK,
				NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	if (res == GTK_RESPONSE_CANCEL) {
		return FALSE;
	}

	return TRUE;
}

static void
burn_shutdown (MarlinPipeline *pipeline,
	       struct _BurnDetails *details)
{
	g_object_unref (G_OBJECT (pipeline));
}

static void
burn_eos_reached (MarlinPipeline *pipeline,
		  struct _BurnDetails *details)
{
	marlin_pipeline_set_state (MARLIN_PIPELINE (details->pipeline),
				   GST_STATE_NULL);
}

#if 0
static void
write_cancelled (MarlinOperation *operation,
		 struct _BurnDetails *details)
{
}
#endif

static void
burn_op_finished (MarlinOperation *operation,
		  struct _BurnDetails *details)
{
	gboolean res;

	g_object_ref (G_OBJECT (operation));

/* 	marlin_progress_window_set_progress (details->progress, -1); */

	details->recorder = nautilus_burn_recorder_new ();
	g_signal_connect (details->recorder, "progress-changed",
			  G_CALLBACK (recorder_progress_cb), details);
	g_signal_connect (details->recorder, "insert-cd-request",
			  G_CALLBACK (recorder_insert_cd_cb), details);
	g_signal_connect (details->recorder, "action-changed",
			  G_CALLBACK (burn_action_changed), details);

	res = nautilus_burn_recorder_write_tracks (details->recorder,
						   details->drive,
						   details->tracks, 0,
						   /* Once I'm happy with this
						      I'll turn off the test */
#define BURNTEST 1
#ifdef BURNTEST
						   NAUTILUS_BURN_RECORDER_WRITE_DUMMY_WRITE |
						   NAUTILUS_BURN_RECORDER_WRITE_DISC_AT_ONCE
#else
						   NAUTILUS_BURN_RECORDER_WRITE_DISC_AT_ONCE |
						   NAUTILUS_BURN_RECORDER_WRITE_BURNPROOF
#endif
						   , NULL);

	if (res == NAUTILUS_BURN_RECORDER_RESULT_FINISHED) {
		g_print ("Finished\n");
	} else {
		g_print ("An error occurred while burning apparently: %d\n", res);
	}

	free_details (details);
}

static void
burn_op_started (MarlinOperation *operation,
		 struct _BurnDetails *details)
{
	char *text;

	text = g_strdup_printf ("<span weight=\"bold\">%s</span>\n%s",
				_("Writing sample to CD"),
				_("The sample is being written to the disc. This operation may take a long time, depending on the length of the sample and write speed."));
	details->progress = marlin_progress_dialog_new (details->operation,
							details->parent,
							text,
							_("Preparing temporary files."));
	g_free (text);

	gtk_widget_show (GTK_WIDGET (details->progress));
}

static void
burn_op_paused (MarlinOperation *operation,
		gboolean paused,
		struct _BurnDetails *details)
{
	marlin_pipeline_set_state (MARLIN_PIPELINE (details->pipeline),
				   paused ? GST_STATE_PAUSED : GST_STATE_PLAYING);
}

static void
do_burn (struct _BurnDetails *details)
{
	GList *markers, *m, *tracks = NULL;
	MarlinMarkerModel *model;
	guint64 space_required, total_frames;
	GnomeVFSFileSize size;
	GnomeVFSResult result;
	GnomeVFSURI *uri;
	char *text_uri;
	int count = 0;

	g_object_get (G_OBJECT (details->sample),
		      "total-frames", &total_frames,
		      "markers", &model,
		      NULL);
	g_object_get (G_OBJECT (model),
		      "markers", &markers,
		      NULL);
	g_object_unref (G_OBJECT (model));

	details->operation = marlin_operation_new ();
	g_signal_connect (details->operation, "started",
			  G_CALLBACK (burn_op_started), details);
	g_signal_connect (details->operation, "finished",
			  G_CALLBACK (burn_op_finished), details);
	g_signal_connect (details->operation, "paused",
			  G_CALLBACK (burn_op_paused), details);

	/* Check the first marker...
	   If the first marker is not 0
	   then we need to create an unnamed one.
	*/
	if (markers) {
		MarlinMarker *mark = markers->data;

		if (mark->position != 0) {
			NautilusBurnRecorderTrack *t = g_new0 (NautilusBurnRecorderTrack, 1);
			t->type = NAUTILUS_BURN_RECORDER_TRACK_TYPE_AUDIO;
 			t->contents.audio.filename = g_strdup ("/tmp/marlin-burn-pipeline0");
			t->contents.audio.cdtext = g_strdup ("");

			tracks = g_list_append (tracks, t);
			count = 1;
		}
	} else {
		NautilusBurnRecorderTrack *t = g_new0 (NautilusBurnRecorderTrack, 1);
		t->type = NAUTILUS_BURN_RECORDER_TRACK_TYPE_AUDIO;
		t->contents.audio.filename = g_strdup ("/tmp/marlin-burn-pipeline0");
		t->contents.audio.cdtext = g_strdup ("");
		tracks = g_list_append (tracks, t);
		count = 1;
	}

	for (m = markers; m; m = m->next) {
		MarlinMarker *mark = m->data;
		NautilusBurnRecorderTrack *t;

		t = g_new0 (NautilusBurnRecorderTrack, 1);
		t->type = NAUTILUS_BURN_RECORDER_TRACK_TYPE_AUDIO;
		t->contents.audio.filename = g_strdup_printf ("/tmp/marlin-burn-pipeline%d", count);
		t->contents.audio.cdtext = g_strdup (mark->name);

		tracks = g_list_append (tracks, t);
		count++;
	}

	details->tracks = tracks;
	/* Each frame is 16bits and there's 2 channels
	   plus 44 bytes for the header as well */
	space_required = (total_frames << 2) + 44;

	text_uri = g_strdup_printf ("file://%s", marlin_get_tmp_dir ());
	uri = gnome_vfs_uri_new (text_uri);
	g_free (text_uri);

	result = gnome_vfs_get_volume_free_space (uri, &size);
	gnome_vfs_uri_unref (uri);

	if (result == GNOME_VFS_OK &&
	    space_required > size) {
		g_warning ("Not enough space");
		g_warning ("Now leaking samples");
		return;
	}

	/* Make our inital pipeline */
	details->pipeline = g_object_new (BURN_PIPELINE_TYPE,
					  "sample", details->sample,
					  "operation", details->operation,
					  NULL);
	g_signal_connect (details->pipeline, "eos",
			  G_CALLBACK (burn_eos_reached), details);
	g_signal_connect (details->pipeline, "shutdown",
			  G_CALLBACK (burn_shutdown), details);
	marlin_pipeline_set_state (MARLIN_PIPELINE (details->pipeline),
				   GST_STATE_PLAYING);
}

static void
dialog_response (GtkDialog *dialog,
		 guint response_id,
		 struct _BurnDialog *bd)
{
	struct _BurnDetails *details;

	gtk_widget_hide (GTK_WIDGET (dialog));

	switch (response_id) {
	case GTK_RESPONSE_OK:
		details = g_new (struct _BurnDetails, 1);
		details->sample = bd->sample;
		details->drive = (NautilusBurnDrive *) nautilus_burn_drive_selection_get_drive (NAUTILUS_BURN_DRIVE_SELECTION (bd->drive));
		details->parent = bd->parent;
		details->dialog = bd->dialog;
		do_burn (details);

		break;

	case GTK_RESPONSE_HELP:
		marlin_display_help ("marlin-burn-sample-dialog");
		return;

	case GTK_RESPONSE_CANCEL:
	default:
		break;
	}

	g_object_unref (G_OBJECT (bd->markers));
	g_free (bd);
}

GtkWidget *
marlin_burn_dialog_new (MarlinBaseWindow *base)
{
	MarlinSample *sample;
	GtkWindow *parent;
	struct _BurnDialog *bd;
	GtkWidget *vbox, *hbox, *label, *table, *inner_vbox, *spacer;
	char *title, *name, *len;
	GList *markers;
	guint64 frames;
	guint32 rate, count;

	sample = marlin_base_window_get_sample (base);
	parent = GTK_WINDOW (base);

	bd = g_new (struct _BurnDialog, 1);
	bd->sample = sample;
	bd->parent = parent;

	g_object_get (G_OBJECT (sample),
		      "name", &name,
		      "sample-rate", &rate,
		      "total-frames", &frames,
		      "markers", &bd->markers,
		      NULL);

	g_object_get (G_OBJECT (bd->markers),
		      "markers", &markers,
		      NULL);
	count = g_list_length (markers) + 1;

	bd->dialog = gtk_dialog_new ();

	title = g_strdup_printf (_("Write %s to CD"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (bd->dialog), title);
	g_free (title);

	gtk_dialog_set_has_separator (GTK_DIALOG (bd->dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (bd->dialog), parent);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (bd->dialog)->vbox), vbox);
	gtk_widget_show (vbox);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = marlin_make_title_label (_("Drive:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	bd->drive = nautilus_burn_drive_selection_new ();
	g_object_set (G_OBJECT (bd->drive),
		      "show-recorders-only", TRUE,
		      NULL);
	gtk_box_pack_start (GTK_BOX (hbox), bd->drive, TRUE, TRUE, 0);
	gtk_widget_show (bd->drive);

	inner_vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), inner_vbox, TRUE, TRUE, 0);
	gtk_widget_show (inner_vbox);

	label = marlin_make_title_label (_("Information:"));
	gtk_box_pack_start (GTK_BOX (inner_vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (inner_vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	spacer = marlin_make_spacer ();
	gtk_box_pack_start (GTK_BOX (hbox), spacer, FALSE, FALSE, 0);
	gtk_widget_show (spacer);

	table = marlin_make_table (2, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);
	gtk_widget_show (table);

	label = marlin_make_title_label (_("Number of Tracks:"));
	PACK (table, label, 0, 0, GTK_FILL);
	gtk_widget_show (label);

	len = g_strdup_printf ("%d", count);
	label = marlin_make_info_label (len);
	g_free (len);

	PACK (table, label, 1, 0, GTK_FILL);
	gtk_widget_show (label);

	label = marlin_make_title_label (_("Total CD Running Time:"));
	PACK (table, label, 0, 1, GTK_FILL);
	gtk_widget_show (label);

	len = marlin_ms_to_pretty_time ((frames / rate) * 1000, TRUE);
	label = marlin_make_info_label (len);
	g_free (len);
	PACK (table, label, 1, 1, GTK_FILL);
	gtk_widget_show (label);

	gtk_dialog_add_button (GTK_DIALOG (bd->dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (bd->dialog),
			       _("Write to CD"),
			       GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (bd->dialog),
			       GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);

	g_signal_connect (bd->dialog, "response",
			  G_CALLBACK (dialog_response), bd);
	return bd->dialog;
}

#endif /* HAVE_NAUTILUS_BURN */
#endif
