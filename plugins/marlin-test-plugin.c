/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <marlin/marlin-plugin.h>

static void
test_action (GtkAction *action,
	     MarlinBaseWindow *window)
{
	g_print ("Testytesty\n");
}

static char *ui_xml =
"<ui><menubar><menu name=\"HelpMenu\" action=\"Help\">"
"<placeholder name=\"HelpPlaceholder\">"
"<menuitem name=\"TestMenu\" action=\"TestAction\"/>"
"</placeholder></menu></menubar></ui>";

GtkActionEntry test_action_entries[] = {
	{ "TestAction", GTK_STOCK_HELP, N_("Test plugin"), NULL,
	  N_("The test plugin"),
	  G_CALLBACK (test_action) },
};

static void
merge_ui (GtkUIManager *ui_manager,
	  MarlinBaseWindow *window)
{
	GtkActionGroup *ag;

	ag = gtk_action_group_new ("TestActions");
	gtk_action_group_add_actions (ag, test_action_entries,
				      G_N_ELEMENTS (test_action_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, ag, 0);

	gtk_ui_manager_add_ui_from_string (ui_manager, ui_xml, -1, NULL);
	gtk_ui_manager_ensure_update (ui_manager);
}

static void
set_sensitive (GtkUIManager *ui_manager,
	       MarlinBaseWindow *window,
	       gboolean sensitive)
{
	GList *action_groups, *p;
	GtkActionGroup *ag;
	GtkAction *action;

	action_groups = gtk_ui_manager_get_action_groups (ui_manager);
	for (p = action_groups; p; p = p->next) {
		ag = p->data;

		if (strcmp (gtk_action_group_get_name (ag), "TestActions") == 0) {
			break;
		}
	}

	action = gtk_action_group_get_action (ag, "TestAction");
	g_object_set (G_OBJECT (action), "sensitive", sensitive, NULL);
}

static MarlinPluginFuncs funcs = {
	merge_ui,
	set_sensitive
};

static MarlinPluginInfo info = {
	NULL,
	"Crap test",
	"The Mysterious Mr Nymn",
	"This plugin does nothing useful",
	VERSION,
	"Copyright (C) 2005 The Mysterious Mr Nymn",
	NULL,
	&funcs
};

MarlinPluginInfo *
marlin_plugin_module_register (GModule *module,
			       int *version)
{
	info.module = module;
	
	*version = MARLIN_PLUGIN_VERSION;
	return &info;
}
