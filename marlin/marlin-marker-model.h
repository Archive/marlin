/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_MARKER_MODEL_H__
#define __MARLIN_MARKER_MODEL_H__

#include <glib-object.h>

#include <marlin/marlin-undo-manager.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_MARKER_MODEL_TYPE (marlin_marker_model_get_type ())
#define MARLIN_MARKER_MODEL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_MARKER_MODEL_TYPE, MarlinMarkerModel))
#define MARLIN_MARKER_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_MARKER_MODEL_TYPE, MarlinMarkerModelClass))

typedef struct _MarlinMarkerModel MarlinMarkerModel;
typedef struct _MarlinMarkerModelClass MarlinMarkerModelClass;
typedef struct _MarlinMarkerModelPrivate MarlinMarkerModelPrivate;
typedef struct _MarlinMarker MarlinMarker;

struct _MarlinMarker {
	guint64 position;
	char *name;
};

struct _MarlinMarkerModel {
	GObject parent;

	MarlinMarkerModelPrivate *priv;
};

struct _MarlinMarkerModelClass {
	GObjectClass parent_class;

	void (* marker_added) (MarlinMarkerModel *model,
			       MarlinMarker *marker);
	void (* marker_removed) (MarlinMarkerModel *model,
				 MarlinMarker *marker);
	void (* marker_changed) (MarlinMarkerModel *model,
				 MarlinMarker *marker);
};

GType marlin_marker_model_get_type (void);
MarlinMarkerModel *marlin_marker_model_new (void);

void marlin_marker_model_add_marker (MarlinMarkerModel *model,
				     guint64 position,
				     const char *name,
				     MarlinUndoContext *ctxt);
void marlin_marker_model_remove_marker (MarlinMarkerModel *model,
					MarlinMarker *marker,
					MarlinUndoContext *ctxt);
void marlin_marker_model_move_marker (MarlinMarkerModel *model,
				      MarlinMarker *marker,
				      guint64 new_position,
				      MarlinUndoContext *ctxt);
void marlin_marker_model_rename_marker (MarlinMarkerModel *model,
					MarlinMarker *marker,
					const char *name,
					MarlinUndoContext *ctxt);
void marlin_marker_model_clear (MarlinMarkerModel *model,
				MarlinUndoContext *ctxt);
void marlin_marker_model_move_markers_after (MarlinMarkerModel *model,
					     guint64 position,
					     gint64 offset,
					     MarlinUndoContext *ctxt);
void marlin_marker_model_remove_markers_in_range (MarlinMarkerModel *model,
						  guint64 start,
						  guint64 end,
						  MarlinUndoContext *ctxt);
#ifdef __cplusplus
}
#endif

#endif
