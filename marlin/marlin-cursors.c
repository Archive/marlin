/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Federico Mena-Quintero <federico@gnu.org>
 *
 *  Copyright 2000 The Free Software Foundation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin-cursors.h>

#include "cursors/hand-closed-data.xbm"
#include "cursors/hand-closed-mask.xbm"
#include "cursors/hand-open-data.xbm"
#include "cursors/hand-open-mask.xbm"
#include "cursors/i_beam_data.xbm"
#include "cursors/i_beam_mask.xbm"
#include "cursors/i_beam_left_data.xbm"
#include "cursors/i_beam_left_mask.xbm"
#include "cursors/i_beam_right_data.xbm"
#include "cursors/i_beam_right_mask.xbm"
#include "cursors/i_beam_add_data.xbm"
#include "cursors/i_beam_add_mask.xbm"
#include "cursors/i_beam_minus_data.xbm"
#include "cursors/i_beam_minus_mask.xbm"

/* Keep in sync with marlin-cursors.h */
static struct {
	guchar *data;
	guchar *mask;
	GdkCursorType type;
	int data_width;
	int data_height;
	int mask_width;
	int mask_height;
	int hot_x, hot_y;
} cursors[] = {
	{ i_beam_data_bits, i_beam_mask_bits, 0,
	  i_beam_data_width, i_beam_data_height,
	  i_beam_mask_width, i_beam_mask_height,
	  4, 7 },
	{ i_beam_left_data_bits, i_beam_left_mask_bits, 0,
	  i_beam_left_data_width, i_beam_left_data_height,
	  i_beam_left_mask_width, i_beam_left_mask_height,
	  4, 9 },
	{ i_beam_right_data_bits, i_beam_right_mask_bits, 0,
	  i_beam_right_data_width, i_beam_right_data_height,
	  i_beam_right_mask_width, i_beam_right_mask_height,
	  4, 7 },
	{ NULL, NULL, GDK_SB_H_DOUBLE_ARROW, 0, 0, 0, 0, 0, 0 },
	{ i_beam_add_data_bits, i_beam_add_mask_bits, 0,
	  i_beam_add_data_width, i_beam_add_data_height,
	  i_beam_add_mask_width, i_beam_add_mask_height,
	  4, 7 },
	{ i_beam_minus_data_bits, i_beam_minus_mask_bits, 0,
	  i_beam_minus_data_width, i_beam_minus_data_height,
	  i_beam_minus_mask_width, i_beam_minus_mask_height,
	  4, 7 },
	{ hand_closed_data_bits, hand_closed_mask_bits, 0,
	  hand_closed_data_width, hand_closed_data_height,
	  hand_closed_mask_width, hand_closed_mask_height,
	  hand_closed_data_width / 2, hand_closed_data_height / 2 },
	{ hand_open_data_bits, hand_open_mask_bits, 0,
	  hand_open_data_width, hand_open_data_height,
	  hand_open_mask_width, hand_open_mask_height,
	  hand_open_data_width / 2, hand_open_data_height / 2 },
	{ NULL, NULL, 0, 0, 0, 0, 0, 0, 0 }
};

GdkCursor *
marlin_cursor_get (GtkWidget *widget,
		   MarlinCursorType type)
{
	GdkCursor *cursor;

	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (type >= 0 && type < NUM_CURSORS, NULL);

	if (cursors[type].data != NULL) {
		GdkPixmap *pmap;
		GdkBitmap *mask;
		GtkStyle *style;

		g_assert (cursors[type].data_width == cursors[type].mask_width);
		g_assert (cursors[type].data_height == cursors[type].mask_height);
	
	
		pmap = gdk_bitmap_create_from_data (widget->window,
						    (char *)cursors[type].data,
						    cursors[type].data_width,
						    cursors[type].data_height);
		mask = gdk_bitmap_create_from_data (widget->window,
						    (char *)cursors[type].mask,
						    cursors[type].mask_width,
						    cursors[type].mask_height);
		
		g_assert (pmap != NULL && mask != NULL);
		
		style = gtk_widget_get_style (widget);
		
		cursor = gdk_cursor_new_from_pixmap (pmap, mask,
						     &style->white, &style->black,
						     cursors[type].hot_x,
						     cursors[type].hot_y);
		g_object_unref (pmap);
		g_object_unref (mask);
	} else {
		cursor = gdk_cursor_new (cursors[type].type);
	}
	
	g_assert (cursor != NULL);

	return cursor;
}
		
