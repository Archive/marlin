/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_MARKER_VIEW_H__
#define __MARLIN_MARKER_VIEW_H__

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-marker-model.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_MARKER_VIEW_TYPE (marlin_marker_view_get_type ())
#define MARLIN_MARKER_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_MARKER_VIEW_TYPE, MarlinMarkerView))
#define MARLIN_MARKER_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_MARKER_VIEW_TYPE, MarlinMarkerViewClass))

typedef struct _MarlinMarkerView MarlinMarkerView;
typedef struct _MarlinMarkerViewClass MarlinMarkerViewClass;
typedef struct _MarlinMarkerViewPrivate MarlinMarkerViewPrivate;

struct _MarlinMarkerView {
	GtkWidget parent;

	MarlinMarkerViewPrivate *priv;
};

struct _MarlinMarkerViewClass {
	GtkWidgetClass parent_class;

	void (*set_scroll_adjustments) (MarlinMarkerView *view,
					GtkAdjustment *hadj,
					GtkAdjustment *vadj);
	void (*move_cursor) (MarlinMarkerView *view,
			     guint64 position);
	void (*enter_marker) (MarlinMarkerView *view,
			      MarlinMarker *marker);
	void (*leave_marker) (MarlinMarkerView *view);
};

GType marlin_marker_view_get_type (void);
MarlinMarkerView *marlin_marker_view_new (MarlinMarkerModel *model,
					  MarlinSample *sample);

MarlinMarker *marlin_marker_view_get_selected_marker (MarlinMarkerView *view);

#ifdef __cplusplus
}
#endif

#endif
