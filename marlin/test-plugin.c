/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <marlin-plugin.h>

int
main (int argc,
      char **argv)
{
	GList *list;

	if (marlin_plugin_initialise () == FALSE) {
		g_warning ("Plugin's failed to initialise");
		exit (1);
	}

	/* Print the plugins for "audio-io" */
	list = marlin_plugin_get_plugins_for_type (MARLIN_PLUGIN_TYPE_AUDIO_IO);
	for (; list; list = list->next) {
		MarlinPluginInfo *info = list->data;

		g_print ("Name: %s\n", info->name);
	}
}
