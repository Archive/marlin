/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <gst/gst.h>

#include <marlin-operation.h>
#include <marlin-marshal.h>

enum {
	STARTED,
	FINISHED,
	PROGRESS,
	CANCELLED,
	PAUSED,
	ERROR,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_DESCRIPTION
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_OPERATION_TYPE, MarlinOperationPrivate))
G_DEFINE_TYPE (MarlinOperation, marlin_operation, G_TYPE_OBJECT);
static guint signals[LAST_SIGNAL];

struct _MarlinOperationPrivate {
	int progress;

	/* FIXME: This needs to be a stack */
	char *description;
	gboolean in_op;
};

static void
finalize (GObject *object)
{
	MarlinOperation *operation;
	MarlinOperationPrivate *priv;

	operation = MARLIN_OPERATION (object);

	priv = operation->priv;
	g_free (priv->description);

	G_OBJECT_CLASS (marlin_operation_parent_class)->finalize (object);
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinOperation *operation;
	MarlinOperationPrivate *priv;

	operation = MARLIN_OPERATION (object);
	priv = operation->priv;

	switch (prop_id) {
	case PROP_DESCRIPTION:
		if (priv->description != NULL) {
			g_free (priv->description);
		}
		priv->description = g_value_dup_string (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	MarlinOperation *operation;
	MarlinOperationPrivate *priv;

	operation = MARLIN_OPERATION (object);
	priv = operation->priv;

	switch (prop_id) {
	case PROP_DESCRIPTION:
		g_value_set_string (value, priv->description);
		break;

	default:
		break;
	}
}

static void
marlin_operation_class_init (MarlinOperationClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	g_type_class_add_private (object_class, sizeof (MarlinOperationPrivate));
	g_object_class_install_property (object_class,
					 PROP_DESCRIPTION,
					 g_param_spec_string ("description",
							      "", "",
							      "",
							      G_PARAM_READWRITE));
	signals[STARTED] = g_signal_new ("started",
					 G_TYPE_FROM_CLASS (klass),
					 G_SIGNAL_RUN_FIRST |
					 G_SIGNAL_NO_RECURSE,
					 G_STRUCT_OFFSET (MarlinOperationClass, started),
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);
	signals[FINISHED] = g_signal_new ("finished",
					  G_TYPE_FROM_CLASS (klass),
					  G_SIGNAL_RUN_FIRST |
					  G_SIGNAL_NO_RECURSE,
					  G_STRUCT_OFFSET (MarlinOperationClass, finished),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__VOID,
					  G_TYPE_NONE, 0);
	signals[PROGRESS] = g_signal_new ("progress",
					  G_TYPE_FROM_CLASS (klass),
					  G_SIGNAL_RUN_FIRST |
					  G_SIGNAL_NO_RECURSE,
					  G_STRUCT_OFFSET (MarlinOperationClass, progress),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__INT,
					  G_TYPE_NONE, 1, G_TYPE_INT);
	signals[CANCELLED] = g_signal_new ("cancelled",
					   G_TYPE_FROM_CLASS (klass),
					   G_SIGNAL_RUN_FIRST |
					   G_SIGNAL_NO_RECURSE,
					   G_STRUCT_OFFSET (MarlinOperationClass, cancelled),
					   NULL, NULL,
					   g_cclosure_marshal_VOID__VOID,
					   G_TYPE_NONE, 0);
	signals[ERROR] = g_signal_new ("error",
				       G_TYPE_FROM_CLASS (klass),
				       G_SIGNAL_RUN_FIRST |
				       G_SIGNAL_NO_RECURSE,
				       G_STRUCT_OFFSET (MarlinOperationClass,
							error),
				       NULL, NULL,
				       marlin_marshal_VOID__BOXED_STRING,
				       G_TYPE_NONE, 2,
				       GST_TYPE_G_ERROR,
				       G_TYPE_STRING);
	signals[PAUSED] = g_signal_new ("paused",
					G_TYPE_FROM_CLASS (klass),
					G_SIGNAL_RUN_FIRST |
					G_SIGNAL_NO_RECURSE,
					G_STRUCT_OFFSET (MarlinOperationClass,
							 paused),
					NULL, NULL,
					marlin_marshal_VOID__BOOLEAN,
					G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

}

static void
marlin_operation_init (MarlinOperation *operation)
{
	operation->priv = GET_PRIVATE (operation);
	operation->priv->in_op = FALSE;
}

MarlinOperation *
marlin_operation_new (void)
{
	return g_object_new (MARLIN_OPERATION_TYPE, NULL);
}

void
marlin_operation_start (MarlinOperation *operation)
{
	g_signal_emit (G_OBJECT (operation), signals[STARTED], 0);
}

void
marlin_operation_finish (MarlinOperation *operation)
{
	g_signal_emit (G_OBJECT (operation), signals[FINISHED], 0);
}

void
marlin_operation_progress (MarlinOperation *operation,
			   int              progress)
{
	operation->priv->progress = progress;

	g_signal_emit (G_OBJECT (operation), signals[PROGRESS], 0, progress);
}

void
marlin_operation_cancel (MarlinOperation *operation)
{
	g_signal_emit (G_OBJECT (operation), signals[CANCELLED], 0);
}

void
marlin_operation_set_error (MarlinOperation *operation,
			    GError          *error,
			    const char      *debug)
{
	g_signal_emit (operation, signals[ERROR], 0, error, debug);
}

void
marlin_operation_pause (MarlinOperation *operation,
			gboolean         paused)
{
	g_signal_emit (operation, signals[PAUSED], 0, paused);
}
