/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2009 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <math.h>

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>

#ifdef HAVE_LADSPA
#include <ladspa.h>
#endif

#include <marlin/marlin-block.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-soundtouch.h>
#include <marlin/marlin-utils.h>


/* Ugly forward declarations */
static void lockless_unlink_range (MarlinChannel     *channel,
				   guint64            start_frame,
				   guint64            finish_frame,
				   MarlinBlock      **blocks,
				   MarlinUndoContext *ctxt);
static void lockless_link_blocks (MarlinChannel     *channel,
				  guint64            insert_frame,
				  guint64            num_frames,
				  MarlinBlock       *block_list,
				  MarlinUndoContext *ctxt);


GQuark
marlin_channel_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-channel-error-quark");
	}

	return quark;
}

/**
 * marlin_channel_new:
 * sample:
 * filename:
 * error:
 *
 * Creates a new channel for @sample, using @filename as the basename for its
 * temp files.
 *
 * Returns: Newly created MarlinChannel or NULL on error.
 */
MarlinChannel *
marlin_channel_new (const char *filename,
		    GError    **error)
{
	MarlinChannel *channel;
	char *basename, *name;

	channel = g_new0 (MarlinChannel, 1);
	channel->lock = marlin_read_write_lock_new ();

	basename = g_path_get_basename (filename);
	name = g_strdup_printf ("%s/%s.XXXXXX",
				marlin_get_tmp_dir (),
				basename);
	g_free (basename);

	channel->frame_file = marlin_file_new (name, error);
	g_free (name);

	if (channel->frame_file == NULL) {
		marlin_channel_free (channel);
		return NULL;
	}

	basename = g_path_get_basename (channel->frame_file->filename);

	name = g_strdup_printf ("%s/%s.XXXXXX",
				marlin_get_tmp_dir (),
				basename);
	g_free (basename);

	channel->peak_file = marlin_file_new (name, error);
	g_free (name);

	if (channel->peak_file == NULL) {
		marlin_channel_free (channel);
		return NULL;
	}

	channel->pages = g_queue_new ();

	/* The owning MarlinSample will set this correctly when the
	   channel is added to a sample */
	channel->position = GST_AUDIO_CHANNEL_POSITION_FRONT_MONO;

	return channel;
}

/**
 * marlin_channel_free:
 * channel: A #MarlinChannel.
 *
 * Frees all resources that @channel was using.
 */
void
marlin_channel_free (MarlinChannel *channel)
{
	g_return_if_fail (channel != NULL);

	WRITE_LOCK (channel->lock);

	if (channel->first) {
		marlin_block_free_list (channel->first);
	}

	if (channel->frame_file) {
		marlin_file_unref (channel->frame_file);
	}

	if (channel->peak_file) {
		marlin_file_unref (channel->peak_file);
	}

	g_queue_free (channel->pages);

	WRITE_UNLOCK (channel->lock);
	marlin_read_write_lock_destroy (channel->lock);

	g_free (channel);
}

/**
 * marlin_channel_create_block:
 * @channel:
 *
 * Creates a new block in this channel.
 *
 * Returns: A newly created MarlinBlock.
 */
MarlinBlock *
marlin_channel_create_block (MarlinChannel *channel)
{
	return marlin_block_new (channel, channel->frame_file,
				 channel->peak_file);
}

/* Implements an LRU paging system */
/* Max number of blocks that can be mapped for each channel
   Each page is about 2meg of data */
#define MAX_PAGE_COUNT 10

/* define this to turn off the paging system to simplify testing
   of the block/channel manipulation code. */
/* #define NO_PAGE_SYSTEM */

/**
 * marlin_channel_map_block:
 * @channel:
 * @block:
 *
 * Adds @block to the LRU cache for @channel or if it is already in
 * the cache it is moved to the start of the queue. If there isn't room
 * in the cache then the block at the end of the queue is removed and
 * unmapped.
 *
 * The channel must be write locked.
 */
void
marlin_channel_map_block (MarlinChannel *channel,
			  MarlinBlock   *block)
{
	if (block->is_mapped) {
		return;
	}

#ifndef NO_PAGE_SYSTEM
	/* Remove the block from the page system */
	g_queue_remove (channel->pages, block);

	if (g_queue_get_length (channel->pages) == MAX_PAGE_COUNT) {
		MarlinBlock *b;
		int unmappable_count = 0;

	try_again:
		b = g_queue_pop_tail (channel->pages);
		if (b->unmappable) {
			marlin_block_unmap (b);
		} else {
			g_queue_push_head (channel->pages, b);
			/* If all blocks are made unmappable,
			   we could get into a loop here */
			unmappable_count++;
			if (unmappable_count == MAX_PAGE_COUNT) {
				g_assert_not_reached ();
			}

			goto try_again;
		}
	}

	g_queue_push_head (channel->pages, block);
#endif
	marlin_block_map (block, NULL);
}

/**
 * marlin_channel_unmap_block:
 * @channel:
 * @block:
 *
 * Removes @block from the LRU cache.
 *
 * The channel must be write locked.
 */
void
marlin_channel_unmap_block (MarlinChannel *channel,
			    MarlinBlock   *block)
{
	if (!block->is_mapped) {
		return;
	}

#ifndef NO_PAGE_SYSTEM
	g_queue_remove (channel->pages, block);
#endif
	marlin_block_unmap (block);
}

/* Channel lock must be held before calling this function */
static MarlinBlock *
lockless_get_for_frame (MarlinBlock *block,
			guint64      frame)
{
	/* Yes, this is even slower */
	for (; block; block = marlin_block_next (block)) {
		if (frame >= block->start && frame <= block->end) {
			return block;
		}
	}

	return NULL;
}

/**
 * marlin_channel_get_block_for_frame:
 * channel:
 * frame:
 *
 * Gets the block from @channel that @frame is found in.
 *
 * Returns: MarlinBlock or NULL on failure.
 */
MarlinBlock *
marlin_channel_get_block_for_frame (MarlinChannel *channel,
				    guint64        frame)
{
	MarlinBlock *block;
	g_return_val_if_fail (channel != NULL, NULL);

	READ_LOCK (channel->lock);

	if (frame == channel->frames) {
		MarlinBlock *b;

		b = channel->last;
		READ_UNLOCK (channel->lock);

		return b;
	}

	if (frame == (guint64) 0) {
		MarlinBlock *b;

		b = channel->first;
		READ_UNLOCK (channel->lock);

		return b;
	}

	block = lockless_get_for_frame (channel->first, frame);

	READ_UNLOCK (channel->lock);

	return block;
}

/*
 * splits the channel at split_frame
 * 0 |--------------| 20
 * Split at 10
 * 0 |----| 9 10 |------| 20
 */
static void
lockless_split_block (MarlinChannel *channel,
		      guint64        split_frame)
{
	MarlinBlock *first, *second;

	g_return_if_fail (split_frame <= channel->frames);

	if (split_frame == channel->frames || split_frame == 0) {
		return;
	}

	first = lockless_get_for_frame (channel->first, split_frame);
	g_return_if_fail (first != NULL);

	if (split_frame != first->start) {

		second = marlin_block_split (first, split_frame);

		/* Insert second into the list after first */
		if (first->next == NULL) {
			first->next = second;
			second->previous = first;
			second->next = NULL;

			/* If first->next was NULL,
			   then first was the last block
			   Set the channel last point to second */
			channel->last = second;
		} else {
			second->next = first->next;
			second->next->previous = second;
			second->previous = first;
			first->next = second;
		}
	}
}

/**
 * marlin_channel_split_block:
 * @channel:
 * @split_frame:
 *
 * Splits the block in @channel located at @split_frame into 2 separate blocks.
 */
void
marlin_channel_split_block (MarlinChannel *channel,
			    guint64        split_frame)
{
	g_return_if_fail (channel != NULL);

	WRITE_LOCK (channel->lock);
	lockless_split_block (channel, split_frame);
	WRITE_UNLOCK (channel->lock);
}

struct _unlink_closure {
	MarlinChannel *channel;
	MarlinBlock *original;
	guint64 start_frame, end_frame;
};

/* FIXME: Do we need to copy all the block lists around? */
static void
unlink_undo (gpointer data)
{
	struct _unlink_closure *c = data;
	MarlinBlock *prev_block;
	MarlinBlock *next_block;
	MarlinBlock *first_block;
	MarlinBlock *last_block;

	WRITE_LOCK (c->channel->lock);

	if (c->channel->first == NULL) {
		c->channel->first = marlin_block_copy_list (c->original, NULL);
		c->channel->last = marlin_block_last (c->channel->first);
	} else {
		prev_block = lockless_get_for_frame (c->channel->first,
						     c->start_frame - 1);
		first_block = marlin_block_copy_list (c->original, NULL);
		last_block = marlin_block_last (first_block);

		if (prev_block == NULL) {
			next_block = c->channel->first;
			c->channel->first = first_block;
			first_block->previous = NULL;
		} else {
			next_block = prev_block->next;
			prev_block->next = first_block;
			first_block->previous = prev_block;
		}

		last_block->next = next_block;
		if (next_block) {
			next_block->previous = last_block;
		}
	}

	c->channel->frames = marlin_block_recalculate_ranges (c->channel->first);

	WRITE_UNLOCK (c->channel->lock);
}

static void
unlink_redo (gpointer data)
{
	struct _unlink_closure *c = data;
	MarlinBlock *b;

	WRITE_LOCK (c->channel->lock);
	lockless_unlink_range (c->channel, c->start_frame, c->end_frame,
			       &b, NULL);
	WRITE_UNLOCK (c->channel->lock);

	marlin_block_free_list (b);
}

static void
unlink_destroy (gpointer data)
{
	struct _unlink_closure *c = data;

	marlin_block_free_list (c->original);
	g_free (c);
}

/* Removes data between start_frame and finish_frame inclusive
   by unlinking the blocks that contain it, and then returns
   the lists of removed blocks in &blocks */
static void
lockless_unlink_range (MarlinChannel     *channel,
		       guint64            start_frame,
		       guint64            finish_frame,
		       MarlinBlock      **blocks,
		       MarlinUndoContext *ctxt)
{
	MarlinBlock *first_block, *last_block;

	g_return_if_fail (start_frame <= channel->frames);
	g_return_if_fail (finish_frame <= channel->frames);
	g_return_if_fail (finish_frame >= start_frame);

	/* Split on the start and finish frames */
	if (finish_frame != channel->frames) {
		lockless_split_block (channel, finish_frame + 1);
	}

	lockless_split_block (channel, start_frame);

	first_block = lockless_get_for_frame (channel->first, start_frame);
	g_assert (first_block != NULL);

	last_block = lockless_get_for_frame (channel->first, finish_frame);
	g_assert (last_block != NULL);

	/* Unlink these blocks */
	if (channel->first == first_block) {
		channel->first = marlin_block_next (last_block);
	}

	if (channel->last == last_block) {
		channel->last = marlin_block_previous (first_block);
	}

	/* Need to lock the blocks */
	if (first_block->previous != NULL) {
		first_block->previous->next = last_block->next;
	}

	if (last_block->next != NULL) {
		last_block->next->previous = first_block->previous;
	}

	last_block->next = NULL;

	/* Need to unlock the blocks */
	channel->frames = marlin_block_recalculate_ranges (channel->first);
	if (blocks) {
		*blocks = first_block;

		/* Reset the blocks count */
		marlin_block_recalculate_ranges (first_block);
	} else {
		/* FIXME: Destroy the blocks here? */
	}

	if (ctxt) {
		MarlinUndoable *u;
		struct _unlink_closure *c;

		c = g_new (struct _unlink_closure, 1);
		c->channel = channel;
		c->original = marlin_block_copy_list (first_block, NULL);
		c->start_frame = start_frame;
		c->end_frame = finish_frame;

		u = marlin_undoable_new (unlink_undo,
					 unlink_redo,
					 unlink_destroy,
					 c);
		marlin_undo_context_add (ctxt, u);
	}
}

struct _link_closure {
	MarlinChannel *channel;
	MarlinBlock *blocks, *l_block;
	guint64 insert_frame, num_frames;
};

static void
link_undo (gpointer data)
{
	MarlinChannel *channel;
	struct _link_closure *c = data;

	channel = c->channel;
	WRITE_LOCK (channel->lock);

	if (channel->last == c->l_block) {
		channel->last = c->blocks->previous;
	}

	if (channel->first == c->blocks) {
		channel->first = c->l_block->next;

		c->l_block->next = NULL;
		channel->first->previous = NULL;
	} else {
		MarlinBlock *n, *p;

		n = c->l_block->next;
		p = c->blocks->previous;

		n->previous = p;
		p->next = n;

		c->l_block->next = NULL;
		c->blocks->previous = NULL;
	}
	channel->frames = marlin_block_recalculate_ranges (channel->first);
	WRITE_UNLOCK (channel->lock);
}

static void
link_redo (gpointer data)
{
	struct _link_closure *c = data;

	WRITE_LOCK (c->channel->lock);
	lockless_link_blocks (c->channel,
			      c->insert_frame, c->num_frames,
			      c->blocks, NULL);
	WRITE_UNLOCK (c->channel->lock);
}

static void
link_destroy (gpointer data)
{
	struct _link_closure *c = data;
	g_free (c);
}

static void
lockless_link_blocks (MarlinChannel     *channel,
		      guint64            insert_frame,
		      guint64            num_frames,
		      MarlinBlock       *block_list,
		      MarlinUndoContext *ctxt)
{
	MarlinBlock *f_block, *s_block, *l_block;

	g_return_if_fail (insert_frame <= channel->frames);

	l_block = marlin_block_last (block_list);

	if (ctxt) {
		MarlinUndoable *u;
		struct _link_closure *c;

		c = g_new (struct _link_closure, 1);
		c->channel = channel;

		c->insert_frame = insert_frame;
		c->num_frames = num_frames;

		c->blocks = block_list;
		c->l_block = l_block;

		u = marlin_undoable_new (link_undo,
					 link_redo,
					 link_destroy,
					 c);
		marlin_undo_context_add (ctxt, u);
	}

	if (insert_frame == 0) {
		/* If we're inserting at the start, we don't really need
		   to do anything special */

		l_block->next = channel->first;
		if (channel->first == NULL) {
			channel->last = l_block;
		} else {
			channel->first->previous = l_block;
		}
		channel->first = block_list;
	} else if (insert_frame == channel->frames - 1) {
		channel->last->next = block_list;
		block_list->previous = channel->last;
		channel->last = l_block;
	} else {
		/* Split on insert frame */
		lockless_split_block (channel, insert_frame);

		f_block = lockless_get_for_frame (channel->first,
						  insert_frame - 1);
		g_assert (f_block != NULL);

		s_block = f_block->next;
		g_assert (s_block != NULL);

		/* Link in the blocks */
		f_block->next = block_list;
		block_list->previous = f_block;

		s_block->previous = l_block;
		l_block->next = s_block;
	}

	channel->frames = marlin_block_recalculate_ranges (channel->first);
}

/**
 * marlin_channel_link_blocks:
 * @channel:
 * @insert_frame:
 * @num_frames:
 * @block_list:
 * @ctxt:
 */
void
marlin_channel_link_blocks (MarlinChannel     *channel,
			    guint64            insert_frame,
			    guint64            num_frames,
			    MarlinBlock       *block_list,
			    MarlinUndoContext *ctxt)
{
	WRITE_LOCK (channel->lock);

	lockless_link_blocks (channel, insert_frame, num_frames,
			      block_list, ctxt);

	WRITE_UNLOCK (channel->lock);
}

/* Deletes between start_frame and finish_frame inclusive */
static void
lockless_delete_range (MarlinChannel     *channel,
		       guint64            start_frame,
		       guint64            finish_frame,
		       MarlinUndoContext *ctxt)
{
	MarlinBlock *blocks;

	lockless_unlink_range (channel, start_frame, finish_frame,
			       &blocks, ctxt);
	marlin_block_free_list (blocks);
}

struct _insert_data_closure {
	MarlinChannel *channel;
	MarlinBlock *blocks;
	guint64 insert_frame;
	guint64 num_frames;
};

static void
insert_data_undo (gpointer data)
{
	struct _insert_data_closure *c = data;

	WRITE_LOCK (c->channel->lock);
	lockless_unlink_range (c->channel, c->insert_frame,
			       c->insert_frame + (c->num_frames - 1),
			       NULL, NULL);
	WRITE_UNLOCK (c->channel->lock);
}

static void
insert_data_redo (gpointer data)
{
	struct _insert_data_closure *c = data;
	MarlinBlock *prev_block;
	MarlinBlock *next_block;
	MarlinBlock *first_block;
	MarlinBlock *last_block;

	WRITE_LOCK (c->channel->lock);

	if (c->channel->first == NULL) {
		c->channel->first = marlin_block_copy_list (c->blocks, NULL);
		c->channel->last = marlin_block_last (c->channel->first);
	} else {
		prev_block = lockless_get_for_frame (c->channel->first,
						     c->insert_frame - 1);
		first_block = marlin_block_copy_list (c->blocks, NULL);
		last_block = marlin_block_last (first_block);

		if (prev_block == NULL) {
			next_block = c->channel->first;
			c->channel->first = first_block;
			first_block->previous = NULL;
		} else {
			next_block = prev_block->next;
			prev_block->next = first_block;
			first_block->previous = prev_block;
		}

		last_block->next = next_block;
		if (next_block) {
			next_block->previous = last_block;
		}
	}

	c->channel->frames = marlin_block_recalculate_ranges (c->channel->first);

	WRITE_UNLOCK (c->channel->lock);
}

static void
insert_data_destroy (gpointer data)
{
	struct _insert_data_closure *c = data;

	marlin_block_free_list (c->blocks);
	g_free (c);
}

/* Splits the block on the insert_frame, so insert_frame will be first in
   new block. Then it gets the block for insert_frame - 1, and then inserts
   data */
static gboolean
lockless_insert_data (MarlinChannel     *channel,
		      float             *data,
		      guint64            num_frames,
		      guint64            insert_frame,
		      MarlinUndoContext *ctxt,
		      GError           **error)
{
	MarlinBlock *prev, *next;
	MarlinBlock *block;
	struct _insert_data_closure *c = NULL;
	guint64 frames;
	gboolean ret;

	frames = channel->frames;
	if (insert_frame >= frames) {
		insert_frame = frames;
	}

	block = marlin_channel_create_block (channel);
	ret = marlin_block_set_data (block, data, num_frames, error);

	if (ret == FALSE) {
		return FALSE;
	}

	if (ctxt) {
		MarlinUndoable *u;

		c = g_new (struct _insert_data_closure, 1);
		c->channel = channel;
		c->num_frames = num_frames;
		c->insert_frame = insert_frame;
		c->blocks = marlin_block_copy_list (block, NULL);

		u = marlin_undoable_new (insert_data_undo,
					 insert_data_redo,
					 insert_data_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);

	}

	if (channel->first == NULL) {
		channel->first = block;
		channel->last = block;
	} else {
		if (insert_frame != frames && insert_frame != 0) {
			lockless_split_block (channel, insert_frame);
		}

		if (insert_frame != 0) {
			prev = lockless_get_for_frame (channel->first,
						       insert_frame - 1);
			g_assert (prev != NULL);

			next = prev->next;

			WRITE_LOCK (prev->lock);

			prev->next = block;
			block->previous = prev;
			WRITE_UNLOCK (prev->lock);
		} else {
			next = channel->first;
			channel->first = block;
		}

		block->next = next;
		if (next != NULL) {
			WRITE_LOCK (next->lock);
			next->previous = block;
			WRITE_UNLOCK (next->lock);
		} else {
			channel->last = block;
		}
	}

	channel->frames = marlin_block_recalculate_ranges (channel->first);

	return TRUE;
}

/**
 * marlin_channel_insert_data:
 * channel:
 * data:
 * num_frames:
 * insert_frame:
 * error:
 *
 * Inserts @num_frames pointed to by @data into @channel starting at
 * @insert_frame.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_insert_data (MarlinChannel     *channel,
			    float             *data,
			    guint64            num_frames,
			    guint64            insert_frame,
			    MarlinUndoContext *ctxt,
			    GError           **error)
{
	gboolean ret;

	WRITE_LOCK (channel->lock);

	ret = lockless_insert_data (channel, data, num_frames,
				    insert_frame, ctxt, error);

	WRITE_UNLOCK (channel->lock);

	return ret;
}

static gboolean
lockless_copy_data (MarlinChannel     *dest,
		    MarlinChannel     *src,
		    MarlinRange       *range,
		    guint64            insert_frame,
		    MarlinOperation   *operation,
		    MarlinUndoContext *ctxt,
		    GError           **error)
{
	guint64 frames_needed;
	MarlinBlock *block;
	guint64 start;
	float *buf;
	gboolean ret = FALSE;

	frames_needed = (range->finish - range->start) + 1;

	start = range->start;
	block = lockless_get_for_frame (src->first, start);
	buf = g_new (float, MARLIN_BLOCK_SIZE);

	while (frames_needed > 0) {
		MarlinBlock *next_block;
		guint got_frames, frames_avail;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		got_frames = marlin_block_get_buffer (block, buf, start,
						      frames_avail,
						      &next_block);

		ret = lockless_insert_data (dest, buf, got_frames,
					    start, ctxt, error);
		if (ret == FALSE) {
			break;
		}

		block = next_block;
		frames_needed -= got_frames;
		start += got_frames;
	}

	g_free (buf);

	return ret;
}

/**
 * marlin_channel_copy_data:
 * src_channel:
 * dest_channel:
 * start_frame:
 * end_frame:
 * error:
 *
 * Copy the data between @start_frame and @end_frame (inclusive) from
 * @src_channel into @dest_channel.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_copy_data (MarlinChannel *src_channel,
			  MarlinChannel *dest_channel,
			  guint64 start_frame,
			  guint64 end_frame,
			  GError **error)
{
	MarlinRange range;
	gboolean ret = FALSE;

	g_return_val_if_fail (src_channel != dest_channel, FALSE);

	if (end_frame >= src_channel->frames) {
		end_frame = src_channel->frames - 1;
	}

	READ_LOCK (src_channel->lock);
	WRITE_LOCK (dest_channel->lock);

	range.start = start_frame;
	range.finish = end_frame;

	/* Insert the copied data at 0 */
	ret = lockless_copy_data (dest_channel, src_channel, &range, 0,
				  NULL, NULL, error);

	READ_UNLOCK (src_channel->lock);
	WRITE_UNLOCK (dest_channel->lock);

	return ret;
}

/**
 * marlin_channel_delete_range:
 * channel:
 * start_frame:
 * finish_frame:
 *
 * Deletes the data between @start_frame and @finish_frame from @channel.
 */
void
marlin_channel_delete_range (MarlinChannel     *channel,
			     guint64            start_frame,
			     guint64            finish_frame,
			     MarlinUndoContext *ctxt)
{
	g_return_if_fail (channel != NULL);

	WRITE_LOCK (channel->lock);

	if (finish_frame >= channel->frames) {
		finish_frame = channel->frames - 1;
	}

	lockless_delete_range (channel, start_frame, finish_frame, ctxt);
	WRITE_UNLOCK (channel->lock);
}

static gboolean
lockless_insert_silence (MarlinChannel     *channel,
			 MarlinOperation   *operation,
			 guint64            insert_frame,
			 guint64            num_frames,
			 MarlinUndoContext *ctxt,
			 GError           **error)
{
	float *data;
	guint64 length;

	g_return_val_if_fail (insert_frame <= channel->frames, FALSE);

	if (num_frames == 0) {
		return TRUE;
	}

	/* Make a space for the zer0 */
	data = g_new0 (float, MARLIN_BLOCK_SIZE);

	/* Make blocks out of the silence */
	length = num_frames;

	while (length > (guint64) 0) {
		guint len_to_copy;
		gboolean ret;

		len_to_copy = MIN (MARLIN_BLOCK_SIZE, length);

		ret = lockless_insert_data (channel, data,
					    len_to_copy, insert_frame,
					    ctxt, error);
		if (ret == FALSE) {
			g_free (data);
			return FALSE;
		}

		insert_frame += len_to_copy;
		length -= len_to_copy;
	}

	g_free (data);

	return TRUE;
}

/**
 * marlin_channel_insert_silence:
 * channel:
 * operation:
 * insert_frame:
 * num_frames:
 * ctxt:
 * error:
 *
 * Inserts @num_frames of silence into @channel at @insert_frame.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_insert_silence (MarlinChannel *channel,
			       MarlinOperation *operation,
			       guint64 insert_frame,
			       guint64 num_frames,
			       MarlinUndoContext *ctxt,
			       GError **error)
{
	gboolean ret;

	g_return_val_if_fail (channel != NULL, FALSE);

	WRITE_LOCK (channel->lock);
	ret = lockless_insert_silence (channel, operation, insert_frame,
				       num_frames, ctxt, error);
	WRITE_UNLOCK (channel->lock);

	return ret;
}

/**
 * marlin_channel_clear_range:
 * channel:
 * operation:
 * start_frame:
 * end_frame:
 * error:
 *
 * Clears the @channel between @start_frame and @end_frame
 * In reality it deletes the range and then adds silence
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_clear_range (MarlinChannel *channel,
			    MarlinOperation *operation,
			    guint64 start_frame,
			    guint64 finish_frame,
			    MarlinUndoContext *ctxt,
			    GError **error)
{
	gboolean ret;

	g_return_val_if_fail (channel != NULL, FALSE);

	WRITE_LOCK (channel->lock);

	lockless_delete_range (channel, start_frame, finish_frame, ctxt);
	ret = lockless_insert_silence (channel, operation, start_frame,
				       (finish_frame - start_frame) + 1,
				       ctxt, error);

	WRITE_UNLOCK (channel->lock);

	return ret;
}

/* y = mx + c
   x = position
   c = in_level

   m = y2 - y1 / x2 - x1
   y2 = out_level
   y1 = in_level
   x2 - x1 = number_frames
*/
static float
linear_fade (guint64 position,
	     guint64 number_frames,
	     double  in_level,
	     double  out_level)
{
	float m;

	m = (out_level - in_level) / (float) number_frames;
	return m * (float) position + in_level;
}

gboolean
lockless_crossfade_blocks (MarlinChannel     *dest,
			   MarlinBlock       *src_block,
			   MarlinRange       *dest_range,
			   MarlinRange       *src_range,
			   double             dest_in_level,
			   double             dest_out_level,
			   double             src_in_level,
			   double             src_out_level,
			   MarlinOperation   *operation,
			   MarlinUndoContext *ctxt,
			   GError           **error)
{
        MarlinBlock *dest_block, *d;
	guint64 src_start, src_finish, dest_start, dest_finish;
	guint64 frames_needed, frame_pos, number_frames;
        float *src_buf, *dest_buf;
        gboolean ret = TRUE;

	src_start = src_range->start;
	src_finish = src_range->finish;
	dest_start = dest_range->start;
	dest_finish = dest_range->finish < dest->frames ?
		dest_range->finish : dest->frames - 1;

	frames_needed = (src_finish - src_start) + 1;
	number_frames = frames_needed;

	/* Unlink all the blocks in dest that we're going to replace */
	lockless_unlink_range (dest, dest_start, dest_finish,
			       &dest_block, ctxt);
	d = dest_block;

	src_buf = g_new (float, MARLIN_BLOCK_SIZE);
	dest_buf = g_new (float, MARLIN_BLOCK_SIZE);

	frame_pos = 0;
	while (frames_needed > 0) {
		MarlinBlock *s_next, *d_next;
		guint s_length, d_length, frames_avail;
		int i;

		if (src_block == NULL) {
			g_warning ("src_block shoud not be NULL, still need %llu frames",
				   frames_needed);
			break;
		}

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		s_length = marlin_block_get_buffer (src_block, src_buf,
						    src_start, frames_avail,
						    &s_next);
		if (dest_block != NULL) {
			d_length = marlin_block_get_buffer (dest_block,
							    dest_buf,
							    dest_start,
							    frames_avail,
							    &d_next);
		} else {
			d_length = 0;
		}

		/* Apply the fades to the buffers */
		for (i = 0; i < s_length; i++, frame_pos++) {
			src_buf[i] = src_buf[i] * linear_fade
				(frame_pos, number_frames,
				 src_in_level, src_out_level);

			if (i < d_length) {
				dest_buf[i] = dest_buf[i] * linear_fade
					(frame_pos, number_frames,
					 dest_in_level, dest_out_level);
			}

			dest_buf[i] += src_buf[i];
		}

		ret = lockless_insert_data (dest, dest_buf, s_length,
					    dest_start, ctxt, error);
		if (ret == FALSE) {
			break;
		}

		frames_needed -= s_length;
		src_start += s_length;
		dest_start += s_length;
		src_block = s_next;
		dest_block = d_next;

		memset (src_buf, 0, MARLIN_BLOCK_SIZE_BYTES);
		memset (dest_buf, 0, MARLIN_BLOCK_SIZE_BYTES);
	}

	g_free (src_buf);
	g_free (dest_buf);

	/* Free unlinked blocks */
	marlin_block_free_list (d);

	return ret;
}

/**
 * marlin_channel_insert:
 * dest: The #MarlinChannel into which the data is inserted
 * src: The #MarlinChannel from which the data comes
 * insert_frame: The frame at which the data is inserted
 * error: A #GError for reporting errors
 *
 * Inserts the data in @src into @dest at @insert_frame.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
void
marlin_channel_insert (MarlinChannel     *dest,
		       MarlinChannel     *src,
		       guint64            insert_frame,
		       MarlinUndoContext *ctxt)
{
	MarlinBlock *blocks;
	guint64 count;

	READ_LOCK (src->lock);
	WRITE_LOCK (dest->lock);

	blocks = marlin_block_copy_list (src->first, &count);

	/* Move the blocks to the dest channel */
	marlin_block_move_blocks_list (blocks, dest);

	lockless_link_blocks (dest, insert_frame, count, blocks, ctxt);

	WRITE_UNLOCK (dest->lock);
	READ_UNLOCK (src->lock);
}

/* We take @fade_length frames from @insert_frame and insert our data.
   The removed frames are then crossfaded at the start and end. */
/**
 * marlin_channel_insert_with_crossfade:
 * @dest:
 * @src:
 * @insert_frame:
 * @range:
 * @fade_length:
 * @operation:
 * @ctxt:
 * @error
 *
 * Return values:
 */
gboolean
marlin_channel_insert_with_crossfade (MarlinChannel     *dest,
				      MarlinChannel     *src,
				      MarlinRange       *range,
				      guint64            insert_frame,
				      guint              fade_length,
				      MarlinOperation   *operation,
				      MarlinUndoContext *ctxt,
				      GError           **error)
{
	MarlinBlock *xf_section;
	gboolean ret = FALSE;
	MarlinRange src_range, dest_range;

	READ_LOCK (src->lock);
	WRITE_LOCK (dest->lock);

	/* This is the section of audio that we're going to crossfade over
	   the top of the source */
	lockless_unlink_range (dest, insert_frame, fade_length,
			       &xf_section, ctxt);

	/* Copy the source into the dest at @insert_frame */
	ret = lockless_copy_data (dest, src, range, insert_frame,
				  operation, ctxt, error);
	if (ret == FALSE) {
		/* FIXME: Needs to tidy up and relink xf_section */
		return FALSE;
	}

	/* Crossfade blocks into dest channel */
	src_range.start = 0;
	src_range.finish = fade_length;
	dest_range.start = insert_frame;
	dest_range.finish = fade_length;

#if 0
	/* Crossfade at start */
	ret = lockless_crossfade_blocks (dest, xf_section,
					 &dest_range, &src_range);
#endif

	WRITE_UNLOCK (dest->lock);
	READ_UNLOCK (src->lock);

	return TRUE;
}

/**
 * marlin_channel_crop_range:
 * channel:
 * start_frame:
 * finish_frame:
 *
 * Crops the area defined by @start_frame and @finish_frame (inclusive)
 * from @channel.
 */
void
marlin_channel_crop_range (MarlinChannel *channel,
			   guint64 start_frame,
			   guint64 finish_frame,
			   MarlinUndoContext *ctxt)
{
	g_return_if_fail (start_frame <= channel->frames);
	g_return_if_fail (finish_frame <= channel->frames);
	g_return_if_fail (finish_frame >= start_frame);

	WRITE_LOCK (channel->lock);

	if (finish_frame + 1 < channel->frames) {
		lockless_delete_range (channel, finish_frame + 1,
				       channel->frames - 1, ctxt);
	}

	if (start_frame >= (guint64) 1) {
		lockless_delete_range (channel, (guint64) 0,
				       start_frame - 1, ctxt);
	}

	WRITE_UNLOCK (channel->lock);
}

/**
 * marlin_channel_mix:
 * @dest:
 * @src:
 * @s_ratio:
 * @d_ratio:
 * @start_frame:
 * @end_frame:
 * @clip:
 * @operation:
 * @ctxt:
 * @error:
 *
 * Takes the data from @src and mixes it into @dest at @start_frame.
 * If @clip is TRUE, the data is only mixed up until @end_frame,
 * otherwise all data in @src is mixed in.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_mix (MarlinChannel *dest,
		    MarlinChannel *src,
		    double s_ratio,
		    double d_ratio,
		    guint64 start_frame,
		    guint64 end_frame,
		    gboolean clip,
		    MarlinOperation *operation,
		    MarlinUndoContext *ctxt,
		    GError **error)
{
	MarlinBlock *s_block, *d_block, *d;
	guint64 dest_length, frames_needed, limit;
	guint64 end_offset, s_frame;
	float *s_buf, *d_buf;
	gboolean ret = TRUE;

	g_return_val_if_fail (dest != NULL, FALSE);
	g_return_val_if_fail (src != NULL, FALSE);

	READ_LOCK (src->lock);
	WRITE_LOCK (dest->lock);

	end_offset = start_frame + src->frames - 1;
	/* If clip == TRUE, then we're only going to end_frame
	   otherwise we go to the end of the sample */
	limit = clip ? MIN (end_frame, end_offset) :
		MIN (dest->frames - 1, end_offset);

	dest_length = (limit - start_frame) + 1;
	if (dest_length > src->frames) {
		frames_needed = src->frames;
	} else {
		frames_needed = clip ? dest_length : src->frames;
	}

	/* Unlink all the blocks we're going to mix. */
	lockless_unlink_range (dest, start_frame, limit, &d, ctxt);
	d_block = d;

	s_block = src->first;

	s_buf = g_new (float, MARLIN_BLOCK_SIZE);
	d_buf = g_new (float, MARLIN_BLOCK_SIZE);

	s_frame = 0;
	while (frames_needed > 0) {
		guint s_length, d_length, frames_avail;
		MarlinBlock *next_s_block, *next_d_block;
		int i;

		if (s_block == NULL) {
			g_warning ("Should not be null, still need %llu",
				   frames_needed);
			break;
		}

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		s_length = marlin_block_get_buffer (s_block, s_buf,
						    s_frame, frames_avail,
						    &next_s_block);
		if (d_block != NULL) {
			d_length = marlin_block_get_buffer (d_block, d_buf,
							    start_frame,
							    frames_avail,
							    &next_d_block);
		} else {
			d_length = 0;
		}

		/* Mix the buffers */
		for (i = 0; i < frames_avail; i++) {
			d_buf[i] = (d_buf[i] * d_ratio) + (s_buf[i] * s_ratio);
		}

		ret = lockless_insert_data (dest, d_buf, s_length, start_frame,
					    ctxt, error);
		if (ret == FALSE) {
			break;
		}

		frames_needed -= s_length;
		start_frame += s_length;
		s_frame += s_length;
		s_block = next_s_block;
		d_block = next_d_block;

		memset (s_buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
		memset (d_buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
	}

	g_free (s_buf);
	g_free (d_buf);

	/* Free the unlinked blocks now */
	marlin_block_free_list (d);

	WRITE_UNLOCK (dest->lock);
	READ_UNLOCK (src->lock);

	return ret;
}

/* FIXME: Might be useful to have a fixed ratio version so that we don't
   have to make functions that just return the same number all the time */
static gboolean
lockless_channel_multiply (MarlinChannel     *channel,
			   guint64            start,
			   guint64            finish,
			   MarlinFadeFunc     ratio_func,
			   gpointer           closure,
			   MarlinUndoContext *ctxt,
			   GError           **error)
{
	MarlinBlock *block, *b;
	guint64 frames_needed;
	float *buf;
	gboolean ret = TRUE;
	guint64 block_start, insert_point;

	frames_needed = (finish - start) + 1;

	buf = g_new (float, MARLIN_BLOCK_SIZE);

	lockless_unlink_range (channel, start, finish, &block, ctxt);
	b = block;

	block_start = block->start;
	insert_point = start;
	while (frames_needed > 0 && block) {
		MarlinBlock *next_block;
		guint got_frames, frames_avail;
		int i;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		got_frames = marlin_block_get_buffer (block, buf, block_start,
						      frames_avail,
						      &next_block);

		for (i = 0; i < got_frames; i++) {
			buf[i] = buf[i] * ratio_func (i, closure);
		}

		ret = lockless_insert_data (channel, buf, got_frames,
					    insert_point, ctxt, error);
		if (ret == FALSE) {
			g_print ("Error inserting frames\n");
			break;
		}

		frames_needed -= got_frames;
		insert_point += got_frames;

		block = next_block;
		block_start += got_frames;
	}

	g_free (buf);

	marlin_block_free_list (b);

	return ret;
}

static float
invert_func (guint64 position,
	     gpointer data)
{
	return -1.0;
}

/**
 * marlin_channel_invert:
 * channel:
 * start:
 * finish:
 * error:
 *
 * Inverts the data between @start and @finish of @channel.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_invert (MarlinChannel     *channel,
		       guint64            start,
		       guint64            finish,
		       MarlinUndoContext *ctxt,
		       GError           **error)
{
	gboolean ret;
	g_return_val_if_fail (channel != NULL, FALSE);

	WRITE_LOCK (channel->lock);

	ret = lockless_channel_multiply (channel, start, finish,
					 invert_func, NULL, ctxt, error);

	WRITE_UNLOCK (channel->lock);

	return ret;
}

struct _volume_closure {
	float ratio;
};

static float
volume_func (guint64 position,
	     gpointer data)
{
	struct _volume_closure *vc = data;
	return vc->ratio;
}

/**
 * marlin_channel_adjust_volume:
 * channel:
 * db: The new volume in decibels
 * start:
 * finish:
 * error:
 *
 * Adjusts the volume of the data between @start and @finish of @channel
 * by @db decibels.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_adjust_volume (MarlinChannel *channel,
			      float db,
			      guint64 start,
			      guint64 finish,
			      MarlinUndoContext *ctxt,
			      GError **error)
{
	struct _volume_closure *vc;
	gboolean ret;

	g_return_val_if_fail (channel != NULL, FALSE);

	vc = g_new (struct _volume_closure, 1);

	if (db == MARLIN_INFINITE_DB) {
		vc->ratio = 0.0;
	} else {
		vc->ratio = pow (10.0, db / 10.0);
	}

	WRITE_LOCK (channel->lock);

	ret = lockless_channel_multiply (channel, start, finish,
					 volume_func, vc, ctxt, error);

	WRITE_UNLOCK (channel->lock);

	g_free (vc);
	return ret;
}

/**
 * marlin_channel_fade:
 * @channel: The channel.
 * @start: Start frame.
 * @finish: Finish frame.
 * @fade_func: The function to get the fade level from.
 * @closure: The data to be passed to the fade_func.
 * @operation:
 * @ctxt:
 * @error:
 *
 * Change the level of @channel between @start and @finish (inclusive)
 * using @fade_func as the function to generate new values.
 *
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_channel_fade (MarlinChannel     *channel,
		     guint64            start,
		     guint64            finish,
		     MarlinFadeFunc     fade_func,
		     gpointer           closure,
		     MarlinOperation   *operation,
		     MarlinUndoContext *ctxt,
		     GError           **error)
{
	gboolean ret;

	g_return_val_if_fail (channel != NULL, FALSE);

	WRITE_LOCK (channel->lock);

	if (finish >= channel->frames) {
		finish = channel->frames - 1;
	}

	ret = lockless_channel_multiply (channel, start, finish, fade_func, closure, ctxt, error);

	WRITE_UNLOCK (channel->lock);

	return ret;
}

/**
 * marlin_channel_crossfade:
 * @src:
 * @dest:
 * @src_range:
 * @dest_range:
 * @src_fade_func:
 * @src_closure:
 * @dest_fade_func:
 * @dest_closure:
 * @operation:
 * @ctxt:
 * @error:
 *
 */
gboolean
marlin_channel_crossfade (MarlinChannel     *src,
			  MarlinChannel     *dest,
			  MarlinRange       *src_range,
			  MarlinRange       *dest_range,
			  MarlinFadeFunc     src_fade_func,
			  gpointer           src_closure,
			  MarlinFadeFunc     dest_fade_func,
			  gpointer           dest_closure,
			  MarlinOperation   *operation,
			  MarlinUndoContext *ctxt,
			  GError           **error)
{
	guint64 frames_needed;
	float *src_buf, *dest_buf;
	MarlinBlock *src_block, *dest_block, *d;
	gboolean ret = TRUE;
	guint64 src_start, src_finish;
	guint64 dest_start, dest_finish;
	guint64 frame_start;

	g_return_val_if_fail (src != NULL, FALSE);
	g_return_val_if_fail (dest != NULL, FALSE);

	READ_LOCK (src->lock);
	WRITE_LOCK (dest->lock);

	src_start = src_range->start;
	src_finish = src_range->finish;
	dest_start = dest_range->start;
	dest_finish = dest_range->finish;

	if (src_finish >= src->frames) {
		src_finish = src->frames - 1;
	}

	if (dest_finish >= dest->frames) {
		dest_finish = dest->frames - 1;
	}

	frames_needed = (src_range->finish - src_range->start) + 1;

	src_block = lockless_get_for_frame (src->first, src_range->start);

	/* Unlink all the blocks in dest we're going to replace */
	lockless_unlink_range (dest, dest_range->start, dest_range->finish,
			       &dest_block, ctxt);
	d = dest_block;

	src_buf = g_new (float, MARLIN_BLOCK_SIZE);
	dest_buf = g_new (float, MARLIN_BLOCK_SIZE);

	frame_start = 0;
	while (frames_needed > 0) {
		MarlinBlock *s_next, *d_next;
		guint s_length, d_length, frames_avail;
		int i;

		if (src_block == NULL) {
			g_warning ("Should not be null, still need %llu",
				   frames_needed);
			break;
		}

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		s_length = marlin_block_get_buffer (src_block, src_buf,
						    src_start,
						    frames_avail,
						    &s_next);
		if (dest_block != NULL) {
			d_length = marlin_block_get_buffer (dest_block,
							    dest_buf,
							    dest_start,
							    frames_avail,
							    &d_next);
		} else {
			d_length = 0;
		}

		/* Apply the fades to the buffers */
		for (i = 0; i < s_length; i++) {
			src_buf[i] = src_buf[i] * src_fade_func
				(frame_start + i, src_closure);
			if (i < d_length) {
				dest_buf[i] = dest_buf[i] * dest_fade_func
					(frame_start + i, dest_closure);
			}

			/* Mix together */
			dest_buf[i] += src_buf[i];
		}

		ret = lockless_insert_data (dest, dest_buf, s_length,
					    dest_start, ctxt, error);
		if (ret == FALSE) {
			break;
		}

		frames_needed -= s_length;
		src_start += s_length;
		dest_start += s_length;
		frame_start += s_length;

		src_block = s_next;
		dest_block = d_next;

		memset (src_buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
		memset (dest_buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
	}

	g_free (src_buf);
	g_free (dest_buf);

	/* Free the unlinked blocks */
	marlin_block_free_list (d);

	WRITE_UNLOCK (dest->lock);
	READ_UNLOCK (src->lock);

	return ret;
}

/* FIXME: Is this function needed anymore? */
/**
 * marlin_channel_is_ready:
 * @channel: The channel.
 *
 * Checks if @channel is mmapped and peaks are generated.
 *
 * Returns: TRUE if @channel is ready, FALSE otherwise.
 */
gboolean
marlin_channel_is_ready (MarlinChannel *channel)
{
	return (channel->first != NULL);
}

/* For a zero crossing a zero is between -0.009 and 0.009 */
#define ZERO_TOLERANCE 0.009

/**
 * marlin_channel_next_zero:
 * @channel: The channel.
 * @position: Position to start from
 *
 */
guint64
marlin_channel_next_zero (MarlinChannel *channel,
			  guint64        position)
{
	MarlinBlock *block;
	float *data;
	guint64 orig, pos_in_block;

	g_return_val_if_fail (channel != NULL, position);
	g_return_val_if_fail (position < channel->frames, position);

	READ_LOCK (channel->lock);

	block = lockless_get_for_frame (channel->first, position);
	g_assert (block);

	orig = position;

	/* Move the position on one frame so that we don't just return a
	   zero at the current position */
	position++;

	READ_LOCK (block->lock);
	data = marlin_block_get_frame_data (block);

	while (block) {
		pos_in_block = position - block->start;

		/* Work forward to end of block */
		while (pos_in_block <= block->end) {
			if (data[pos_in_block] <= ZERO_TOLERANCE &&
			    data[pos_in_block] >= -ZERO_TOLERANCE) {
				READ_UNLOCK (block->lock);
				READ_UNLOCK (channel->lock);
				return position;
			}
			position++;
			pos_in_block++;
		}

		/* Get the previous block */
		READ_UNLOCK (block->lock);
		block = block->next;

		if (block) {
			READ_LOCK (block->lock);

			/* Reset everything */
			position = block->start;
			data = marlin_block_get_frame_data (block);
		}
	}

	/* If we're here, we didn't find a 0 */

	READ_UNLOCK (channel->lock);
	return orig;
}

/**
 * marlin_channel_previous_zero:
 * @channel:
 * @position:
 *
 */
guint64
marlin_channel_previous_zero (MarlinChannel *channel,
			      guint64        position)
{
	MarlinBlock *block;
	float *data;
	guint64 orig, pos_in_block;

	g_return_val_if_fail (channel != NULL, position);
	g_return_val_if_fail (position < channel->frames, position);

	READ_LOCK (channel->lock);

	block = lockless_get_for_frame (channel->first, position);
	g_assert (block);

	orig = position;

	/* Move the position back one frame so that we don't just return a
	   zero at the current position */
	position--;

	READ_LOCK (block->lock);
	data = marlin_block_get_frame_data (block);

	while (block) {
		pos_in_block = position - block->start;

		/* Work back to start of block */
		while (pos_in_block >= 0) {
			if (data[pos_in_block] <= ZERO_TOLERANCE &&
			    data[pos_in_block] >= -ZERO_TOLERANCE) {
				READ_UNLOCK (block->lock);
				READ_UNLOCK (channel->lock);

				return position;
			}

			if (pos_in_block == 0) {
				break;
			}

			position--;
			pos_in_block--;
		}

		/* Get the previous block */
		READ_UNLOCK (block->lock);
		block = block->previous;

		if (block) {
			READ_LOCK (block->lock);

			/* Reset everything */
			position = block->end;
			data = marlin_block_get_frame_data (block);
		}
	}

	/* If we're here, we didn't find a 0 */
	READ_UNLOCK (channel->lock);

	return orig;
}

/**
 * marlin_channel_reverse_range:
 * @channel:
 * @start:
 * @finish:
 * @operation:
 * @ctxt:
 * @error:
 *
 */
gboolean
marlin_channel_reverse_range (MarlinChannel *channel,
			      guint64 start,
			      guint64 finish,
			      MarlinOperation *operation,
			      MarlinUndoContext *ctxt,
			      GError **error)
{
	MarlinBlock *block, *b;
	guint64 frames_needed, insert_point, block_start;
	float *buf;
	gboolean ret;

	frames_needed = (finish - start) + 1;

	buf = g_new (float, MARLIN_BLOCK_SIZE);

	WRITE_LOCK (channel->lock);

	lockless_unlink_range (channel, start, finish, &block, ctxt);
	b = block;

	/* When the blocks are unlinked the ranges are recalculated, so
	   start no longer equals block->start */
	block_start = block->start;
	insert_point = start;

	/* We get a block size, reverse it and insert it before all the other
	   reversed blocks */
	while (frames_needed > 0 && block) {
		MarlinBlock *next_block;
		guint got_frames, frames_avail;
		int i;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		got_frames = marlin_block_get_buffer (block, buf, block_start,
						      frames_avail,
						      &next_block);

		/* Swap the first and last values, working inwards.
		   If there is an odd number we don't swap the middle
		   value, but it doesn't need swapped so that's ok */
		for (i = 0; i < got_frames / 2; i++) {
			float tmp;

			tmp = buf[i];
			buf[i] = buf[(got_frames - 1) - i];
			buf[(got_frames - 1) - i] = tmp;
		}

		/* insert_point never changes so that the block will always
		   be inserted in the reverse order that it was taken out */
		ret = lockless_insert_data (channel, buf, got_frames,
					    insert_point, ctxt, error);

		frames_needed -= got_frames;

		block = next_block;
		block_start += got_frames;

		memset (buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
	}

	g_free (buf);

	marlin_block_free_list (b);

	WRITE_UNLOCK (channel->lock);
	return TRUE;
}

/**
 * marlin_channel_expand_range:
 * @channel:
 * @soundtouch:
 * @start:
 * @finish:
 * @new_length:
 * @operation:
 * @ctxt:
 * @error:
 *
 */
gboolean
marlin_channel_expand_range (MarlinChannel     *channel,
			     MarlinSoundtouch  *soundtouch,
			     guint64            start,
			     guint64            finish,
			     guint64            new_length,
			     MarlinOperation   *operation,
			     MarlinUndoContext *ctxt,
			     GError           **error)
{
	MarlinBlock *block, *b;
	guint64 frames_needed, insert_point, block_start;
	float *in_buf, *out_buf;
	gboolean ret = TRUE;

	frames_needed = (finish - start) + 1;

	in_buf = g_new (float, MARLIN_BLOCK_SIZE);
	out_buf = g_new (float, MARLIN_BLOCK_SIZE);

	WRITE_LOCK (channel->lock);

	lockless_unlink_range (channel, start, finish, &block, ctxt);
	b = block;

	/* When the blocks are unlinked the ranges are recalculated, so
	   start no longer equals block->start */
	block_start = block->start;
	insert_point = start;

	/* We take a MARLIN_BLOCK_SIZE, feed it into soundtouch and then
	   extract samples from soundtouch, which will have expanded it to
	   the length we need */
	while (frames_needed > 0 && block) {
		MarlinBlock *next_block;
		guint got_frames, frames_avail;
		guint processed;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		got_frames = marlin_block_get_buffer (block, in_buf,
						      block_start, frames_avail,
						      &next_block);

		marlin_soundtouch_put_samples (soundtouch, in_buf, got_frames);
		do {
			processed = marlin_soundtouch_take_samples (soundtouch,
								    out_buf,
								    MARLIN_BLOCK_SIZE);

			if (processed > 0) {
				g_print ("Processed %d\n", processed);
				ret = lockless_insert_data (channel, out_buf,
							    processed,
							    insert_point, ctxt,
							    error);
				if (ret == FALSE) {
					goto err;
				}

				memset (out_buf, 0,
					MARLIN_BLOCK_SIZE * sizeof (float));
				insert_point += processed;
			}
		} while (processed > 0);

		frames_needed -= got_frames;

		block = next_block;
		block_start += got_frames;

		memset (in_buf, 0, MARLIN_BLOCK_SIZE * sizeof (float));
	}

 err:
	g_free (in_buf);
	g_free (out_buf);

	marlin_block_free_list (b);

	WRITE_UNLOCK (channel->lock);

	return TRUE;
}

/**
 * marlin_channel_expand_mix:
 * @src:
 * @dest:
 * @soundtouch:
 * @src_start:
 * @src_end:
 * @dest_start:
 * @dest_end:
 * @src_db:
 * @dest_db:
 * @operation:
 * @ctxt:
 * @error:
 *
 */
gboolean
marlin_channel_expand_mix (MarlinChannel     *src,
			   MarlinChannel     *dest,
			   MarlinSoundtouch  *soundtouch,
			   guint64            src_start,
			   guint64            src_end,
			   guint64            dest_start,
			   guint64            dest_end,
			   double             src_db,
			   double             dest_db,
			   MarlinOperation   *operation,
			   MarlinUndoContext *ctxt,
			   GError           **error)
{
	MarlinBlock *d_block, *d, *s_block;
	float *s_in_buf, *s_out_buf, *d_buf;
	float s_ratio, d_ratio;
	guint64 src_length, dest_length, block_start;
	guint64 frames_needed;
	float new_tempo;
	gboolean ret = TRUE;

	WRITE_LOCK (dest->lock);
	READ_LOCK (src->lock);

	src_length = (src_end - src_start) + 1;
	dest_length = (dest_end - dest_start) + 1;
	new_tempo = ((float) src_length) / (float) (dest_length);

	marlin_soundtouch_set_tempo (soundtouch, new_tempo);
	g_print ("new_tempo: %f\n", new_tempo);

	s_ratio = marlin_db_to_ratio (src_db);
	d_ratio = marlin_db_to_ratio (dest_db);

	/* Unlink all the block we want to mix */
	lockless_unlink_range (dest, dest_start, dest_end, &d, ctxt);
	d_block = d;

	/* Unlinking the blocks causes their ranges to be recalculated
	   so dest_start no longer equals d_block->start */
	block_start = d_block->start;

	s_block = lockless_get_for_frame (src->first, src_start);

	s_in_buf = g_new (float, MARLIN_BLOCK_SIZE);
	s_out_buf = g_new (float, MARLIN_BLOCK_SIZE);
	d_buf = g_new (float, MARLIN_BLOCK_SIZE);

	frames_needed = src_length;
	while (frames_needed > 0 && s_block && d_block) {
		guint s_length, d_length, frames_avail, processed;
		MarlinBlock *s_next, *d_next;
		int i;

		frames_avail = MIN (frames_needed, MARLIN_BLOCK_SIZE);
		s_length = marlin_block_get_buffer (s_block, s_in_buf,
						    src_start, frames_avail,
						    &s_next);

		g_print ("need: %llu, got: %u\n", frames_needed, s_length);
		marlin_soundtouch_put_samples (soundtouch, s_in_buf, s_length);

		processed = marlin_soundtouch_take_samples
			(soundtouch, s_out_buf, MARLIN_BLOCK_SIZE);

		while (processed > 0 && d_block) {
			g_print ("Processed: %u\n", processed);

			/* We take a block from the dest that is the same
			   size as the number of frames we processed and mix
			   that in */
			d_length = marlin_block_get_buffer (d_block, d_buf,
							    block_start,
							    processed,
							    &d_next);

			for (i = 0; i < d_length; i++) {
				d_buf[i] = (d_buf[i] * d_ratio) +
					(s_out_buf[i] * s_ratio);
			}

			ret = lockless_insert_data (dest, d_buf,
						    d_length,
						    dest_start,
						    ctxt, error);
			if (ret == FALSE) {
				goto err;
			}
			memset (d_buf, 0, MARLIN_BLOCK_SIZE_BYTES);
			memset (s_out_buf, 0, MARLIN_BLOCK_SIZE_BYTES);

			block_start += d_length;
			d_block = d_next;

			g_print ("Dest needs: %llu\n", dest_length - d_length);
			processed = marlin_soundtouch_take_samples
				(soundtouch, s_out_buf, MARLIN_BLOCK_SIZE);
		}

#if 0
		g_print ("Flushing\n");
		marlin_soundtouch_flush (soundtouch);
		processed = marlin_soundtouch_take_samples
			(soundtouch, s_out_buf, MARLIN_BLOCK_SIZE);
		g_print ("Need: %llu - processed: %u\n", frames_needed, processed);

		if (frames_needed > 0 && processed > 0) {
			goto process_samples;
		} else {
			g_print ("Nothing left to flush\n");
		}
#endif
		frames_needed -= s_length;
		s_block = s_next;
		src_start += s_length;

		memset (s_in_buf, 0, MARLIN_BLOCK_SIZE_BYTES);
	}

	g_print ("Amount of samples left in ST: %d\n", marlin_soundtouch_length (soundtouch));
	g_print ("Amount of unprocessed: %d\n", marlin_soundtouch_unprocessed_length (soundtouch));
 err:
	g_free (s_in_buf);
	g_free (s_out_buf);
	g_free (d_buf);

	marlin_block_free_list (d);

	WRITE_UNLOCK (dest->lock);
	READ_UNLOCK (src->lock);

	return ret;
}

struct _normalize_closure {
	float ratio;
};

static float
normalize_func (guint64 position,
		gpointer data)
{
	struct _normalize_closure *vc = data;
	return vc->ratio;
}

static double
lockless_get_max_peak (MarlinChannel   *channel,
		       MarlinRange     *range,
		       MarlinOperation *operation)
{
	guint64 peak_range;
	double max_peak = 0.0;
	MarlinBlock *block;
	MarlinPeak *peak_data;
	guint64 start, end;
	guint64 i, o;

	block = lockless_get_for_frame (channel->first, range->start);
	READ_LOCK (block->lock);

	peak_data = marlin_block_get_peak_data (block);

	start = range->start / MARLIN_FRAMES_PER_PEAK;
	end = range->finish / MARLIN_FRAMES_PER_PEAK;
	peak_range = end - start;

	for (i = 0, o = (range->start - block->start) / MARLIN_FRAMES_PER_PEAK;
	     i < peak_range; i++, o++) {
		guint64 peak_in_channel;
		short mp;
		MarlinPeak p;

		peak_in_channel = block->start + (o * MARLIN_FRAMES_PER_PEAK);
		if (peak_in_channel >= channel->frames) {
			break;
		}

		if (o >= block->num_peaks) {
			/* Next block */
			READ_UNLOCK (block->lock);

			block = marlin_block_next (block);
			g_assert (block != NULL);

			READ_LOCK (block->lock);
			o = 0;
			peak_data = marlin_block_get_peak_data (block);
		}

		p = peak_data[o];

		mp = MAX (p.high, abs (p.low));
		max_peak = MAX (max_peak, ((double) mp / 256.0));
	}

	READ_UNLOCK (block->lock);

	return max_peak;
}

gboolean
marlin_channel_normalize (MarlinChannel     *channel,
			  MarlinRange       *range,
			  double             db,
			  MarlinOperation   *operation,
			  MarlinUndoContext *ctxt,
			  GError           **error)
{
	struct _normalize_closure *nc;
	double max_peak;
	double level = 1.0;
	gboolean ret = FALSE;

	WRITE_LOCK (channel->lock);

	max_peak = lockless_get_max_peak (channel, range, operation);
	if (db == MARLIN_INFINITE_DB) {
		level = 0.0;
	} else {
		level = pow (10.0, db / 10.0);
	}

	nc = g_new (struct _normalize_closure, 1);
	nc->ratio = level / max_peak;

	ret = lockless_channel_multiply (channel, range->start, range->finish,
					 normalize_func, nc, ctxt, error);
	g_free (nc);

	WRITE_UNLOCK (channel->lock);

	return ret;
}

gboolean
marlin_channel_process_ladspa (MarlinChannel     *channel,
			       LADSPA_Descriptor *ladspa,
			       MarlinRange       *range,
			       MarlinOperation   *operation,
			       MarlinUndoContext *ctxt,
			       GError           **error)
{
#ifdef HAVE_LADSPA
	return TRUE;
#else
	return TRUE;
#endif
}
