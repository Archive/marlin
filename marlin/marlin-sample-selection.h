/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifndef __MARLIN_SAMPLE_SELECTION_H__
#define __MARLIN_SAMPLE_SELECTION_H__

#include <glib-object.h>

#include <marlin/marlin-sample.h>

#define MARLIN_SAMPLE_SELECTION_TYPE (marlin_sample_selection_get_type ())
#define MARLIN_SAMPLE_SELECTION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_SAMPLE_SELECTION_TYPE, MarlinSampleSelection))
#define MARLIN_SAMPLE_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_SAMPLE_SELECTION_TYPE, MarlinSampleSelectionClass))
#define IS_MARLIN_SAMPLE_SELECTION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_SAMPLE_SELECTION_TYPE))
#define IS_MARLIN_SAMPLE_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_SAMPLE_SELECTION_TYPE))

typedef struct _MarlinSampleSelectionPrivate MarlinSampleSelectionPrivate;
typedef struct _MarlinSampleSelection {
	GObject parent_object;
	
	MarlinSampleSelectionPrivate *priv;
} MarlinSampleSelection;

typedef struct _MarlinSampleSelectionClass {
	GObjectClass parent_class;

	void (*changed) (MarlinSampleSelection *selection);
} MarlinSampleSelectionClass;

GType marlin_sample_selection_get_type (void);
MarlinSampleSelection *marlin_sample_selection_new (void);

void marlin_sample_selection_set (MarlinSampleSelection *selection,
				  MarlinCoverage coverage,
				  guint64 start,
				  guint64 finish,
				  MarlinUndoContext *context);
void marlin_sample_selection_get (MarlinSampleSelection *selection,
				  MarlinCoverage *coverage,
				  guint64 *start,
				  guint64 *finish);
void marlin_sample_selection_clear (MarlinSampleSelection *selection,
				    MarlinUndoContext *context);
gboolean marlin_sample_selection_contains_frame (MarlinSampleSelection *selection,
						 MarlinCoverage coverage,
						 guint64 frame);

#endif
			       
