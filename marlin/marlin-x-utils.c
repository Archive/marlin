/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-marker-model.h>

GtkWidget *
marlin_make_title_label (const char *text)
{
	GtkWidget *label;
	char *full;

	full = g_strdup_printf ("<span weight=\"bold\">%s</span>", text);
	label = gtk_label_new_with_mnemonic (full);
	g_free (full);

	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);

	return label;
}

GtkWidget *
marlin_make_info_label (const char *text)
{
	GtkWidget *label;

	label = gtk_label_new_with_mnemonic (text);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (label), GTK_WRAP_WORD);

	return label;
}

void
marlin_display_help (const char *section)
{
	GError *error = NULL;

	gtk_show_uri (NULL, section, GDK_CURRENT_TIME, &error);

	if (error) {
		GtkWidget *msgbox;

		msgbox = gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 _("There was an error displaying help:\n%s"),
						 error->message);
		g_signal_connect (G_OBJECT (msgbox), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (msgbox);
		g_error_free (error);
	}
}

/**
 * marlin_set_volume_digits:
 *
 * To be attached to the set_digits signal of a GtkScale which
 * shows dB
 */
char *
marlin_set_volume_digits (GtkScale *scale,
			  double db,
			  gpointer data)
{
	double percent = marlin_db_to_percent (db);
	
	if (db == MARLIN_INFINITE_DB) {
		return g_strdup (_("-\xe2\x88\x9e dB (0.00%)"));
	}
	
	return g_strdup_printf ("%.1f dB (%.2f%%)", db, percent);
}

/**
 * marlin_make_table:
 * @rows: Number of rows.
 * @columns: Number of columns.
 * @same_size: Do all the cells have the same size.
 *
 * Creates a table with the marlin settings for row and column spacings
 *
 * Returns: The newly created GtkTable.
 */
GtkWidget *
marlin_make_table (int rows,
		   int columns,
		   gboolean same_size)
{
	GtkWidget *table;

	table = gtk_table_new (rows, columns, same_size);
	gtk_table_set_row_spacings (GTK_TABLE (table), MARLIN_ROW_SPACING);
	gtk_table_set_col_spacings (GTK_TABLE (table), MARLIN_COL_SPACING);

	return table;
}

/**
 * marlin_menu_position_under_widget:
 * from ephy-gui.c
 */
void
marlin_menu_position_under_widget (GtkMenu *menu,
				   int *x,
				   int *y,
				   gboolean *push_in,
				   gpointer user_data)
{
	GtkWidget *w = GTK_WIDGET (user_data);
	int screen_width, screen_height;
	GtkRequisition requisition;

	gdk_window_get_origin (w->window, x, y);
	*x += w->allocation.x;
	*y += w->allocation.y + w->allocation.height;

	gtk_widget_size_request (GTK_WIDGET (menu), &requisition);

	screen_width = gdk_screen_width ();
	screen_height = gdk_screen_height ();

	*x = CLAMP (*x, 0, MAX (0, screen_width - requisition.width));
	*y = CLAMP (*y, 0, MAX (0, screen_height - requisition.height));
}


GtkWidget *
marlin_make_button (const char *text,
		    const char *stock_id)
{
	GtkWidget *button;
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	button = gtk_button_new ();
	
	if (GTK_BIN (button)->child) {
		gtk_container_remove (GTK_CONTAINER (button),
				      GTK_BIN (button)->child);
	}
	
	label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), button);
	
	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
	hbox = gtk_hbox_new (FALSE, 2);
	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	gtk_container_add (GTK_CONTAINER (button), align);
	gtk_container_add (GTK_CONTAINER (align), hbox);
	gtk_widget_show_all (align);
	
	return button;
}

/**
 * marlin_add_button_to_dialog:
 * @dialog: The dialog.
 * @text: Text of the button.
 * @stock_id: The stock id of the image.
 * @response_id: The response id that this button will correspond to.
 *
 * Creates a button with @stock_id as the image and @text as the label and
 * adds it to @dialog. The button corresponds to @response_id in the
 * @dialog response callback.
 *
 * Returns: The new button.
 */
GtkWidget *
marlin_add_button_to_dialog (GtkDialog *dialog,
			     const char *text,
			     const char *stock_id,
			     int response_id)
{
	GtkWidget *button;
	
	button = marlin_make_button (text, stock_id);
	
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
	
	gtk_dialog_add_action_widget (dialog, button, response_id);
	
	return button;
}

/**
 * marlin_make_spacer:
 *
 * Makes a spacer widget for dialogs to be laid out correctly
 *
 * Returns: A widget to be used as a spacer.
 */
GtkWidget *
marlin_make_spacer (void)
{
	return gtk_label_new ("    ");
}

/**
 * marlin_make_stock_button:
 *
 */
GtkWidget *
marlin_make_image_button (const char *stock_id,
			  GCallback callback,
			  gpointer closure)
{
	GtkWidget *image, *button;

	g_return_val_if_fail (stock_id != NULL, NULL);

	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), image);

	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (callback), closure);
	return button;
}

/* Utility function that takes a combo box and a list of markers
   and fills the combo with the names of the markers */
void
make_marker_combo_menu (GtkComboBox *combo,
			GList *markers)
{
	GList *m;
	
	for (m = markers; m; m = m->next) {
		MarlinMarker *marker = m->data;
		
		gtk_combo_box_append_text (combo, marker->name);
	}
	
	gtk_combo_box_set_active (combo, 0);
}

/* Taken from deprecated function in libgnomeui - Can this be replaced
   by anthing from Gtk? FIXME*/
/* Callback used when a button is pressed in a widget attached to a popup
   menu. It decides whether the menu should be popped up and does the 
   appropriate stuff. */
static int
real_popup_button_pressed (GtkWidget *widget,
			   GdkEventButton *event,
			   GtkMenu *popup)
{
	guint button;
	guint32 timestamp;

	if (event) {
		button = event->button;
		timestamp = event->time;
	} else {
		button = 0;
		timestamp = GDK_CURRENT_TIME;
	}

	gtk_menu_popup (popup, NULL, NULL, NULL, NULL, button, timestamp);
	return TRUE;
}

static int
popup_button_pressed (GtkWidget *widget,
		      GdkEventButton *event,
		      GtkMenu *popup)
{
	if (event->button != 3) {
		return FALSE;
	}

	g_signal_stop_emission_by_name (widget, "button_press_event");
	
	return real_popup_button_pressed (widget, event, popup);
}

static int
popup_menu_pressed (GtkWidget *widget,
		    GtkMenu *popup)
{
	g_signal_stop_emission_by_name (widget, "popup_menu");

	return real_popup_button_pressed (widget, NULL, popup);
}

static int
relay_popup_button_pressed (GtkWidget *widget,
			    GdkEventButton *event,
			    gpointer data)
{
	GtkWidget *new_widget = NULL;

	if (event->button != 3) {
		return FALSE;
	}

	g_signal_stop_emission_by_name (widget, "button_press_event");
	
	if (GTK_IS_CONTAINER (widget)) {
		do {
			GList *children, *child;

			children = gtk_container_get_children (GTK_CONTAINER (widget));

			for (child = children, new_widget = NULL; child;
			     child = child->next) {
				GtkWidget *cur;

				cur = (GtkWidget *) child->data;

				if (!GTK_WIDGET_NO_WINDOW (cur)) {
					continue;
				}
				
				if (cur->allocation.x <= event->x
				    && cur->allocation.y <= event->y
				    && (cur->allocation.x + cur->allocation.width) > event->x
				    && (cur->allocation.y + cur->allocation.height) > event->y
				    && g_object_get_data (G_OBJECT (cur), "gnome_popup_menu_nowindow")) {
					new_widget = cur;
					break;
				}
			}

			if (new_widget) {
				widget = new_widget;
			} else {
				break;
			}

		} while (widget && GTK_IS_CONTAINER (widget) && GTK_WIDGET_NO_WINDOW (widget));
		
		if (!widget || !g_object_get_data (G_OBJECT (widget), "gnome_popup_menu")) {
			return TRUE;
		}
	} else {
		new_widget = widget;
	}
	
	return real_popup_button_pressed (new_widget, event, data);
}
							
/* Callback used to unref the popup menu when the widget 
   it is attached to gets destroyed */
static void
popup_attach_widget_destroyed (GtkWidget *widget,
			       gpointer data)
{
	g_object_unref (G_OBJECT (data));
}

/**
 * marlin_popup_menu_attach:
 * @menu:
 * @widget:
 */
void
marlin_popup_menu_attach (GtkWidget *popup,
			  GtkWidget *widget)
{
	GtkWidget *ev_widget;
	
	g_return_if_fail (GTK_IS_MENU (popup));
	g_return_if_fail (GTK_IS_WIDGET (widget));

	if (g_object_get_data (G_OBJECT (widget), "gnome_popup_menu")) {
		return;
	}

	g_object_set_data (G_OBJECT (widget), "gnome_popup_menu", popup);

	/* This operation can fail if someone is trying to set a popup on e.g. an uncontained label, so we do it first. */
	for (ev_widget = widget; ev_widget && GTK_WIDGET_NO_WINDOW (ev_widget); ev_widget = ev_widget->parent) {
		g_object_set_data (G_OBJECT (ev_widget), "gnome_popup_menu_nowindow", GUINT_TO_POINTER (1));
	}

	g_return_if_fail (ev_widget);

	/* Ref/sink the popup menu so that we take "ownership" of it */
	
	g_object_ref (G_OBJECT (popup));
	gtk_object_sink (GTK_OBJECT (popup));

	/* Prepare the widget to accept button presses 
	   -- the proper assertions will be shouted by 
	   gtk_widget_set_events () */

	gtk_widget_add_events (ev_widget, GDK_BUTTON_PRESS_MASK |
			       GDK_KEY_PRESS_MASK);
	g_signal_connect (widget, "button_press_event",
			  G_CALLBACK (popup_button_pressed), popup);
	g_signal_connect (widget, "popup_menu",
			  G_CALLBACK (popup_menu_pressed), popup);

	if (ev_widget != widget) {
		GClosure *closure;

		closure = g_cclosure_new (G_CALLBACK (relay_popup_button_pressed),
					  popup, NULL);
		g_object_watch_closure (G_OBJECT (widget), closure);
		g_signal_connect_closure (ev_widget, "button_press_event",
					  closure, FALSE);
	}

	/* This callback will unref the popup menu when the widget
	   it is attached to gets destroyed. */
	g_signal_connect (widget, "destroy",
			  G_CALLBACK (popup_attach_widget_destroyed),
			  popup);
}
