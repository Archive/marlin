/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 Iain Holmes
 *  Heavily based on mail-mt.c written by Michael Zucchi <notzed@ximian.com>
 *  Copyright 2000 - 2002 Ximian, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <glib.h>
#include <pthread.h>

#include <marlin-msgport.h>
#include <marlin-mt.h>

#define MARLIN_MT_LOCK(x) pthread_mutex_lock (&x)
#define MARLIN_MT_UNLOCK(x) pthread_mutex_unlock (&x)

static guint marlin_msg_seq; /* Sequence numbr of each message */
static GHashTable *marlin_msg_active; /* table of active messages,
					 must hold marlin_msg_lock to access */
static pthread_mutex_t marlin_msg_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t marlin_msg_cond = PTHREAD_COND_INITIALIZER;

pthread_t marlin_gui_thread;

void *
marlin_mt_msg_new (MarlinMTMsgOp *ops,
		   MarlinMsgPort *reply_port,
		   size_t size)
{
	MarlinMTMsg *msg;

	MARLIN_MT_LOCK (marlin_msg_lock);

	msg = g_malloc0 (size);
	msg->ops = ops;
	msg->seq = marlin_msg_seq++;
	msg->msg.reply_port = reply_port;

	g_hash_table_insert (marlin_msg_active, GINT_TO_POINTER (msg->seq), msg);

	MARLIN_MT_UNLOCK (marlin_msg_lock);
	return msg;
}

void
marlin_mt_msg_free (void *msg)
{
	MarlinMTMsg *m = msg;

	if (m->ops->destroy_msg) {
		m->ops->destroy_msg (m);
	}

	MARLIN_MT_LOCK (marlin_msg_lock);

	g_hash_table_remove (marlin_msg_active, GINT_TO_POINTER (m->seq));
	pthread_cond_broadcast (&marlin_msg_cond);

	MARLIN_MT_UNLOCK (marlin_msg_lock);

	g_free (m);
}

void
marlin_mt_msg_check_error (void *msg)
{
	/* There are no errors at the moment */
}

/* Waits for a message to be finished processing (freed)
   the messageid is from MarlinMTMsg->seq */
void
marlin_mt_msg_wait (guint msgid)
{
	MarlinMTMsg *m;
	int ismain = pthread_self () == marlin_gui_thread;

	if (ismain) {
		MARLIN_MT_LOCK (marlin_msg_lock);
		m = g_hash_table_lookup (marlin_msg_active, GINT_TO_POINTER (msgid));
		while (m) {
			MARLIN_MT_UNLOCK (marlin_msg_lock);
			/* NB: Not sure if TRUE here is good or bad */
			g_main_context_iteration (NULL, TRUE);
			MARLIN_MT_LOCK (marlin_msg_lock);
			m = g_hash_table_lookup (marlin_msg_active,
						 GINT_TO_POINTER (msgid));
		}
		MARLIN_MT_UNLOCK (marlin_msg_lock);
	} else {
		MARLIN_MT_LOCK (marlin_msg_lock);
		m = g_hash_table_lookup (marlin_msg_active, GINT_TO_POINTER (msgid));
		while (m) {
			pthread_cond_wait (&marlin_msg_cond, &marlin_msg_lock);
			m = g_hash_table_lookup (marlin_msg_active,
						 GINT_TO_POINTER (msgid));
		}
		MARLIN_MT_UNLOCK (marlin_msg_lock);
	}
}

MarlinMsgPort *marlin_gui_port;
static GIOChannel *marlin_gui_channel;
MarlinMsgPort *marlin_gui_reply_port;
static GIOChannel *marlin_gui_reply_channel;

MarlinThread *marlin_mt_thread_queued;
MarlinThread *marlin_mt_thread_new;

static gboolean
marlin_msgport_replied (GIOChannel *source,
			GIOCondition cond,
			void *d)
{
	MarlinMsgPort *port = (MarlinMsgPort *) d;
	MarlinMTMsg *m;

	while ((m = (MarlinMTMsg *) marlin_msgport_get (port))) {
		if (m->ops->reply_msg) {
			m->ops->reply_msg (m);
		}

		marlin_mt_msg_check_error (m);
		marlin_mt_msg_free (m);
	}

	return TRUE;
}

static gboolean
marlin_msgport_received (GIOChannel *source,
			 GIOCondition cond,
			 void *d)
{
	MarlinMsgPort *port = (MarlinMsgPort *) d;
	MarlinMTMsg *m;

	while ((m = (MarlinMTMsg *) marlin_msgport_get (port))) {
		if (m->ops->receive_msg) {
			m->ops->receive_msg (m);
		}

		if (m->msg.reply_port) {
			marlin_msgport_reply ((MarlinMsg *) m);
		} else {
			if (m->ops->reply_msg) {
				m->ops->reply_msg (m);
			}

			marlin_mt_msg_free (m);
		}
	}

	return TRUE;
}

static void
marlin_mt_msg_destroy (MarlinThread *t,
		       MarlinMsg *msg,
		       void *data)
{
	MarlinMTMsg *m = (MarlinMTMsg *) msg;

	marlin_mt_msg_free (m);
}

static void
marlin_mt_msg_received (MarlinThread *t,
			MarlinMsg *msg,
			void *data)
{
	MarlinMTMsg *m = (MarlinMTMsg *) msg;

	if (m->ops->receive_msg) {
		m->ops->receive_msg (m);
	}
}

static void
marlin_mt_msg_cleanup (void)
{
	marlin_thread_destroy (marlin_mt_thread_queued);
	marlin_thread_destroy (marlin_mt_thread_new);

	g_io_channel_unref (marlin_gui_channel);
	g_io_channel_unref (marlin_gui_reply_channel);
	
	marlin_msgport_destroy (marlin_gui_port);
	marlin_msgport_destroy (marlin_gui_reply_port);
}

void
marlin_mt_initialise (void)
{
	marlin_gui_reply_port = marlin_msgport_new ();
	marlin_gui_reply_channel = g_io_channel_unix_new (marlin_msgport_fd (marlin_gui_reply_port));
	g_io_add_watch (marlin_gui_reply_channel, G_IO_IN,
			marlin_msgport_replied,
			marlin_gui_reply_port);

	marlin_gui_port = marlin_msgport_new ();
	marlin_gui_channel = g_io_channel_unix_new (marlin_msgport_fd (marlin_gui_port));
	g_io_add_watch (marlin_gui_channel, G_IO_IN,
			marlin_msgport_received,
			marlin_gui_port);

	marlin_mt_thread_queued = marlin_thread_new (MARLIN_THREAD_QUEUE);
	marlin_thread_set_msg_destroy (marlin_mt_thread_queued, marlin_mt_msg_destroy, 0);
	marlin_thread_set_msg_received (marlin_mt_thread_queued, marlin_mt_msg_received, 0);
	marlin_thread_set_reply_port (marlin_mt_thread_queued, marlin_gui_reply_port);

	marlin_mt_thread_new = marlin_thread_new (MARLIN_THREAD_NEW);
	marlin_thread_set_msg_destroy (marlin_mt_thread_new, marlin_mt_msg_destroy, 0);
	marlin_thread_set_msg_received (marlin_mt_thread_new, marlin_mt_msg_received, 0);
	marlin_thread_set_reply_port (marlin_mt_thread_new, marlin_gui_reply_port);

	marlin_msg_active = g_hash_table_new (NULL, NULL);
	marlin_gui_thread = pthread_self ();

	atexit (marlin_mt_msg_cleanup);
}
	
	
