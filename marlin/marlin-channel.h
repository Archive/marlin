/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_CHANNEL_H__
#define __MARLIN_CHANNEL_H__

#include <glib.h>

#include <gst/audio/multichannel.h>

#include <marlin/marlin-types.h>
#include <marlin/marlin-read-write-lock.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-file.h>
#include <marlin/marlin-block.h>
#include <marlin/marlin-soundtouch.h>

struct _FileRegion {
	off_t offset;
	size_t length;
	guint64 frames;
};

struct _MarlinChannel {
	MarlinReadWriteLock *lock; /* The lock on the data:
				      Only access first and last when you
				      hold this lock */
	MarlinFile *frame_file;
	MarlinFile *peak_file;

	MarlinBlock *first, *last; /* We store the first and last blocks so
				      that append is a O(1) operation. */
	guint64 frames;

	/* LRU paging system */
	GQueue *pages;

	/* Channel's position in sample
	   - it is up to the owning MarlinSample to set this correctly */
	GstAudioChannelPosition position;
};

MarlinChannel *marlin_channel_new (const char *filename,
				   GError **error);
void marlin_channel_free (MarlinChannel *channel);

MarlinBlock *marlin_channel_create_block (MarlinChannel *channel);

void marlin_channel_map_block (MarlinChannel *channel,
			       MarlinBlock *block);
void marlin_channel_unmap_block (MarlinChannel *channel,
				 MarlinBlock *block);

MarlinBlock *marlin_channel_get_block_for_frame (MarlinChannel *channel,
						 guint64 frame);

gboolean marlin_channel_insert_data (MarlinChannel *channel,
				     float *data,
				     guint64 num_frames,
				     guint64 insert_frame,
				     MarlinUndoContext *ctxt,
				     GError **error);
void marlin_channel_insert_block (MarlinChannel *channel,
				  MarlinBlock *block,
				  guint64 insert_frame,
				  MarlinUndoContext *ctxt);
/* Channel manipulation functions */
void marlin_channel_split_block (MarlinChannel *channel,
				 guint64 split_frame);
void marlin_channel_delete_range (MarlinChannel *channel,
				  guint64 start,
				  guint64 finish,
				  MarlinUndoContext *ctxt);
gboolean marlin_channel_insert_silence (MarlinChannel *channel,
					MarlinOperation *operation,
					guint64 insert_frame,
					guint64 num_frames,
					MarlinUndoContext *ctxt,
					GError **error);
gboolean marlin_channel_clear_range (MarlinChannel *channel,
				     MarlinOperation *operation,
				     guint64 start_frame,
				     guint64 finish_frame,
				     MarlinUndoContext *ctxt,
				     GError **error);
void marlin_channel_crop_range (MarlinChannel *channel,
				guint64 start_frame,
				guint64 finish_frame,
				MarlinUndoContext *ctxt);
gboolean marlin_channel_copy_data (MarlinChannel *src_channel,
				   MarlinChannel *dest_channel,
				   guint64 start_frame,
				   guint64 finish_frame,
				   GError **error);
gboolean marlin_channel_mix (MarlinChannel *dest,
			     MarlinChannel *src,
			     double s_ratio,
			     double d_ratio,
			     guint64 start_frame,
			     guint64 end_frame,
			     gboolean clip,
			     MarlinOperation *operation,
			     MarlinUndoContext *ctxt,
			     GError **error);

void marlin_channel_insert (MarlinChannel     *dest,
			    MarlinChannel     *src,
			    guint64            insert_frame,
			    MarlinUndoContext *ctxt);
gboolean marlin_channel_insert_with_crossfade (MarlinChannel     *dest,
					       MarlinChannel     *src,
					       MarlinRange       *range,
					       guint64            insert_frame,
					       guint              fade_length,
					       MarlinOperation   *operation,
					       MarlinUndoContext *ctxt,
					       GError           **error);

gboolean marlin_channel_invert (MarlinChannel *channel,
				guint64 start,
				guint64 finish,
				MarlinUndoContext *ctxt,
				GError **error);
gboolean marlin_channel_adjust_volume (MarlinChannel *channel,
				       float db,
				       guint64 start,
				       guint64 finish,
				       MarlinUndoContext *ctxt,
				       GError **error);
gboolean marlin_channel_fade (MarlinChannel *channel,
			      guint64 start,
			      guint64 finish,
			      MarlinFadeFunc fade_func,
			      gpointer closure,
			      MarlinOperation *operation,
			      MarlinUndoContext *ctxt,
			      GError **error);
gboolean marlin_channel_crossfade (MarlinChannel     *src,
				   MarlinChannel     *dest,
				   MarlinRange       *src_range,
				   MarlinRange       *dest_range,
				   MarlinFadeFunc     src_fade_func,
				   gpointer           src_closure,
				   MarlinFadeFunc     dest_fade_func,
				   gpointer           dest_closure,
				   MarlinOperation   *operation,
				   MarlinUndoContext *ctxt,
				   GError           **error);

guint64 marlin_channel_next_zero (MarlinChannel *channel,
				  guint64 position);
guint64 marlin_channel_previous_zero (MarlinChannel *channel,
				      guint64 position);

gboolean marlin_channel_reverse_range (MarlinChannel *channel,
				       guint64 start,
				       guint64 finish,
				       MarlinOperation *operation,
				       MarlinUndoContext *ctxt,
				       GError **error);
gboolean marlin_channel_expand_range (MarlinChannel     *channel,
				      MarlinSoundtouch  *soundtouch,
				      guint64            start,
				      guint64            finish,
				      guint64            new_length,
				      MarlinOperation   *operation,
				      MarlinUndoContext *ctxt,
				      GError           **error);
gboolean marlin_channel_expand_mix (MarlinChannel     *src,
				    MarlinChannel     *dest,
				    MarlinSoundtouch  *soundtouch,
				    guint64            src_start,
				    guint64            src_end,
				    guint64            dest_start,
				    guint64            dest_end,
				    double             src_db,
				    double             dest_db,
				    MarlinOperation   *operation,
				    MarlinUndoContext *ctxt,
				    GError           **error);

gboolean marlin_channel_is_ready (MarlinChannel *channel);

gboolean marlin_channel_normalize (MarlinChannel     *channel,
				   MarlinRange       *range,
				   double             db,
				   MarlinOperation   *operation,
				   MarlinUndoContext *ctxt,
				   GError           **error);

gboolean marlin_channel_process_ladspa (MarlinChannel     *channel,
					LADSPA_Descriptor *ladspa,
					MarlinRange       *range,
					MarlinOperation   *operation,
					MarlinUndoContext *ctxt,
					GError           **error);

#endif
