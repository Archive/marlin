/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifndef __MARLIN_STOCK_H__
#define __MARLIN_STOCK_H__

#define MARLIN_STOCK_PLAY "marlin-play"
#define MARLIN_STOCK_PAUSE "marlin-pause"
#define MARLIN_STOCK_STOP "marlin-stop"
#define MARLIN_STOCK_PREVIOUS "marlin-previous"
#define MARLIN_STOCK_NEXT "marlin-next"
#define MARLIN_STOCK_RECORD "marlin-record"
#define MARLIN_STOCK_FFWD "marlin-ffwd"
#define MARLIN_STOCK_REWIND "marlin-rewind"
#define MARLIN_STOCK_REPEAT "marlin-repeat"
#define MARLIN_STOCK_NEW_SAMPLE "marlin-new-sample"
#define MARLIN_STOCK_SELECT_ALL "marlin-select-all"
#define MARLIN_STOCK_SELECT_NONE "marlin-select-none"
#define MARLIN_STOCK_SELECTION_SHRINK "marlin-selection-shrink"
#define MARLIN_STOCK_SELECTION_GROW "marlin-selection-grow"
#define MARLIN_STOCK_SOUND_PROPERTIES "marlin-sound-properties"
#define MARLIN_STOCK_PASTE_AS_NEW "marlin-paste-as-new"
#define MARLIN_STOCK_CDDA_EXTRACT "marlin-cdda-extract"
#define MARLIN_STOCK_VZOOM_IN "marlin-zoom-in-v"
#define MARLIN_STOCK_HZOOM_IN "marlin-zoom-in-h"
#define MARLIN_STOCK_VZOOM_OUT "marlin-zoom-out-v"
#define MARLIN_STOCK_HZOOM_OUT "marlin-zoom-out-h"
#define MARLIN_STOCK_UNDO_HISTORY "marlin-undo-history"
#define MARLIN_STOCK_PASTE_INTO "marlin-paste-into"
#define MARLIN_STOCK_NEW_VIEW "marlin-new-view"

void marlin_stock_icons_register (void);

#endif
