/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include <marlin/marlin-marker-model.h>

enum {
	MARKER_ADDED,
	MARKER_REMOVED,
	MARKER_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_MARKER_LIST,
};

struct _MarlinMarkerModelPrivate {
	GList *markers;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_MARKER_MODEL_TYPE, MarlinMarkerModelPrivate))
G_DEFINE_TYPE (MarlinMarkerModel, marlin_marker_model, G_TYPE_OBJECT);
static guint32 signals[LAST_SIGNAL];

static void
finalize (GObject *object)
{
	MarlinMarkerModel *model = (MarlinMarkerModel *) object;
	MarlinMarkerModelPrivate *priv = model->priv;
	GList *l;

	for (l = priv->markers; l; l = l->next) {
		MarlinMarker *marker = l->data;

		g_free (marker);
	}
	g_list_free (priv->markers);

	G_OBJECT_CLASS (marlin_marker_model_parent_class)->finalize (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	switch (prop_id) {
	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinMarkerModel *model = MARLIN_MARKER_MODEL (object);

	switch (prop_id) {
	case PROP_MARKER_LIST:
		g_value_set_pointer (value, model->priv->markers);
		break;

	default:
		break;
	}
}

static void
marlin_marker_model_class_init (MarlinMarkerModelClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	o_class->finalize = finalize;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	g_type_class_add_private (o_class, sizeof (MarlinMarkerModelPrivate));

	signals[MARKER_ADDED] = g_signal_new ("marker-added",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinMarkerModelClass, marker_added),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__POINTER,
					      G_TYPE_NONE, 1,
					      G_TYPE_POINTER);
	signals[MARKER_REMOVED] = g_signal_new ("marker-removed",
						G_TYPE_FROM_CLASS (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinMarkerModelClass, marker_removed),
						NULL, NULL,
						g_cclosure_marshal_VOID__POINTER,
						G_TYPE_NONE, 1,
						G_TYPE_POINTER);

	signals[MARKER_CHANGED] = g_signal_new ("marker-changed",
						G_TYPE_FROM_CLASS (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinMarkerModelClass, marker_changed),
						NULL, NULL,
						g_cclosure_marshal_VOID__POINTER,
						G_TYPE_NONE, 1,
						G_TYPE_POINTER);

	g_object_class_install_property (o_class, PROP_MARKER_LIST,
					 g_param_spec_pointer ("markers", "", "",
							       G_PARAM_READABLE));
}

static void
marlin_marker_model_init (MarlinMarkerModel *model)
{
	model->priv = GET_PRIVATE (model);
}

/**
 * marlin_marker_model_new:
 *
 * Creates a new MarlinMarkerModel.
 *
 * Returns: The new MarlinMarkerModel.
 */
MarlinMarkerModel *
marlin_marker_model_new (void)
{
	return g_object_new (MARLIN_MARKER_MODEL_TYPE, NULL);
}

static void
marker_free (MarlinMarker *marker)
{
	g_free (marker->name);
	g_free (marker);
}

static MarlinMarker *
marker_copy (MarlinMarker *marker)
{
	MarlinMarker *c = g_new (MarlinMarker, 1);
	c->name = g_strdup (marker->name);
	c->position = marker->position;

	return c;
}

static MarlinMarker *
marker_find (MarlinMarkerModel *model,
	     guint64 position,
	     const char *name)
{
	GList *p;

	/* Find the marker that matches the copy */
	for (p = model->priv->markers; p; p = p->next) {
		MarlinMarker *m = p->data;

		if (m->position == position &&
		    strcmp (m->name, name) == 0) {
			return m;
		}
	}

	return NULL;
}

struct _marker_closure {
	MarlinMarkerModel *model;
	MarlinMarker *marker;
};

static void
add_marker_undo (gpointer data)
{
	struct _marker_closure *d = data;
	MarlinMarker *m;

	m = marker_find (d->model, d->marker->position, d->marker->name);
	if (m) {
		marlin_marker_model_remove_marker (d->model, m, NULL);
	}
}

static void
add_marker_redo (gpointer data)
{
	struct _marker_closure *d = data;

	marlin_marker_model_add_marker (d->model, d->marker->position,
					d->marker->name, NULL);
}

static void
star_marker_destroy (gpointer data)
{
	struct _marker_closure *d = data;

	marker_free (d->marker);
	g_free (d);
}

static int
compare_markers (gconstpointer a,
		 gconstpointer b)
{
	const MarlinMarker *ma = a, *mb = b;

	/* Can't just do ma->position - mb->position
	   because position is a guint64 */
	if (ma->position == mb->position) {
		return 0;
	} else if (ma->position < mb->position) {
		return -1;
	} else {
		return 1;
	}
}

/**
 * marlin_marker_model_add_marker:
 * @model: The model.
 * @position: The position of the new marker.
 * @name: The name of the new marker.
 *
 * Adds a marker to @model, at @position called @name and emits the
 * marker-added signal.
 * If @name is NULL, then the marker is called "Marker %llu" with
 * the value of @position inserted.
 */
void
marlin_marker_model_add_marker (MarlinMarkerModel *model,
				guint64            position,
				const char        *name,
				MarlinUndoContext *ctxt)
{
	MarlinMarker *marker;
	MarlinMarkerModelPrivate *priv;

	g_return_if_fail (model != NULL);

	priv = model->priv;

	marker = g_new (MarlinMarker, 1);
	if (name != NULL) {
		marker->name = g_strdup (name);
	} else {
		/* Make the marker name based on the position */
		marker->name = g_strdup_printf (_("Marker %llu"), position);
	}
	marker->position = position;

	priv->markers = g_list_insert_sorted (priv->markers, marker, compare_markers);

	g_signal_emit (G_OBJECT (model), signals[MARKER_ADDED], 0, marker);

	if (ctxt) {
		MarlinUndoable *u;
		struct _marker_closure *c;

		c = g_new (struct _marker_closure, 1);
		c->model = model;
		c->marker = marker_copy (marker);

		u = marlin_undoable_new (add_marker_undo,
					 add_marker_redo,
					 star_marker_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}
}

static void
remove_marker_undo (gpointer data)
{
	struct _marker_closure *d = data;

	marlin_marker_model_add_marker (d->model, d->marker->position,
					d->marker->name, NULL);
}

static void
remove_marker_redo (gpointer data)
{
	struct _marker_closure *d = data;
	MarlinMarker *m;
	
	m = marker_find (d->model, d->marker->position, d->marker->name);
	if (m) {
		marlin_marker_model_remove_marker (d->model, m, NULL);
	}
}

/**
 * marlin_marker_model_remove_marker:
 * @model: The model.
 * @marker: The marker to remove.
 *
 * Removes @marker from @model and emits the marker-removed signal.
 * Once you return from the marker-removed signal
 * the reference to marker will be destroyed
 */
void
marlin_marker_model_remove_marker (MarlinMarkerModel *model,
				   MarlinMarker *marker,
				   MarlinUndoContext *ctxt)
{
	g_return_if_fail (model != NULL);
	g_return_if_fail (marker != NULL);

	model->priv->markers = g_list_remove (model->priv->markers, marker);

	g_signal_emit (G_OBJECT (model), signals[MARKER_REMOVED], 0, marker);

	if (ctxt) {
		MarlinUndoable *u;
		struct _marker_closure *c;
		
		c = g_new (struct _marker_closure, 1);
		c->model = model;
		c->marker = marker_copy (marker);
		
		u = marlin_undoable_new (remove_marker_undo,
					 remove_marker_redo,
					 star_marker_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	g_free (marker->name);
	g_free (marker);
}

struct _move_marker_closure {
	MarlinMarkerModel *model;
	MarlinMarker *marker;
	
	guint64 old_pos;
};

static void
move_marker_undo (gpointer data)
{
	struct _move_marker_closure *d = data;
	MarlinMarker *m;

	m = marker_find (d->model, d->marker->position, d->marker->name);
	if (m) {
		m->position = d->old_pos;
		g_signal_emit (G_OBJECT (d->model), signals[MARKER_CHANGED], 0, m);
	}
}

static void
move_marker_redo (gpointer data)
{
	struct _move_marker_closure *d = data;
	MarlinMarker *m;

	/* The real marker is now at the old position. */
	m = marker_find (d->model, d->old_pos, d->marker->name);
	if (m) {
		marlin_marker_model_move_marker (d->model, m, d->marker->position, NULL);
	}
}

static void
move_marker_destroy (gpointer data)
{
	struct _move_marker_closure *d = data;
	
	marker_free (d->marker);
	g_free (d);
}

/**
 * marlin_marker_model_move_marker:
 * @model: The model.
 * @marker: The marker to move.
 * @new_position: The new position of the marker.
 *
 * Moves @marker to @new_position and emits the marker-changed signal.
 */
void
marlin_marker_model_move_marker (MarlinMarkerModel *model,
				 MarlinMarker *marker,
				 guint64 new_position,
				 MarlinUndoContext *ctxt)
{
	guint64 old;

	g_return_if_fail (model != NULL);
	g_return_if_fail (marker != NULL);

	old = marker->position;
	marker->position = new_position;

	g_signal_emit (G_OBJECT (model), signals[MARKER_CHANGED], 0, marker);

	if (ctxt) {
		MarlinUndoable *u;
		struct _move_marker_closure *c;

		c = g_new (struct _move_marker_closure, 1);
		c->model = model;
		c->marker = marker_copy (marker);
		c->old_pos = old;

		u = marlin_undoable_new (move_marker_undo,
					 move_marker_redo,
					 move_marker_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}
}

struct _rename_marker_closure {
	MarlinMarkerModel *model;
	MarlinMarker *marker;

	char *old_name;
};

static void
rename_marker_undo (gpointer data)
{
	struct _rename_marker_closure *d = data;
	MarlinMarker *m;

	m = marker_find (d->model, d->marker->position, d->marker->name);
	if (m) {
		marlin_marker_model_rename_marker (d->model, m, d->old_name, NULL);
	}
}

static void
rename_marker_redo (gpointer data)
{
	struct _rename_marker_closure *d = data;
	MarlinMarker *m;

	/* The real marker is now called the old name. */
	m = marker_find (d->model, d->marker->position, d->old_name);
	if (m) {
		marlin_marker_model_rename_marker (d->model, m, d->marker->name, NULL);
	}
}

static void
rename_marker_destroy (gpointer data)
{
	struct _rename_marker_closure *d = data;

	marker_free (d->marker);
	g_free (d->old_name);
	g_free (d);
}
	
/**
 * marlin_marker_model_rename_marker:
 * @model: The model.
 * @marker: The marker to rename.
 * @name: The new name for the marker.
 *
 * Renames @marker to @name and emits the marker-changed signal.
 */
void
marlin_marker_model_rename_marker (MarlinMarkerModel *model,
				   MarlinMarker *marker,
				   const char *name,
				   MarlinUndoContext *ctxt)
{
	char *old;
	g_return_if_fail (model != NULL);
	g_return_if_fail (marker != NULL);
	g_return_if_fail (name != NULL);

	if (strcmp (name, marker->name) == 0) {
		return;
	}
	
	/* We don't need to free it here,
	   because it'll be freed once the undo context is destroyed */
	old = marker->name;
	marker->name = g_strdup (name);

	g_signal_emit (G_OBJECT (model), signals[MARKER_CHANGED], 0, marker);

	if (ctxt) {
		MarlinUndoable *u;
		struct _rename_marker_closure *c;

		c = g_new (struct _rename_marker_closure, 1);
		c->model = model;
		c->marker = marker_copy (marker);
		c->old_name = old;

		u = marlin_undoable_new (rename_marker_undo,
					 rename_marker_redo,
					 rename_marker_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

}

struct _clear_markers_closure {
	MarlinMarkerModel *model;
	GList *markers;
};

static void
clear_markers_undo (gpointer data)
{
	struct _clear_markers_closure *d = data;
	GList *p;

	for (p = d->markers; p; p = p->next) {
		MarlinMarker *m = p->data;

		marlin_marker_model_add_marker (d->model, m->position, m->name, NULL);
	}
}

static void
clear_markers_redo (gpointer data)
{
	struct _clear_markers_closure *d = data;

	marlin_marker_model_clear (d->model, NULL);
}

static void
clear_markers_destroy (gpointer data)
{
	struct _clear_markers_closure *d = data;
	GList *p;

	for (p = d->markers; p; p = p->next) {
		marker_free ((MarlinMarker *) p->data);
	}
	g_list_free (d->markers);
	g_free (d);
}

/**
 * marlin_marker_model_clear:
 * @model: The model.
 *
 * Removes all the markers in the model
 */
void
marlin_marker_model_clear (MarlinMarkerModel *model,
			   MarlinUndoContext *ctxt)
{
	GList *p;

	g_return_if_fail (model != NULL);

	if (ctxt) {
		MarlinUndoable *u;
		struct _clear_markers_closure *c;

		c = g_new (struct _clear_markers_closure, 1);
		c->model = model;
		c->markers = NULL;
		for (p = model->priv->markers; p; p = p->next) {
			MarlinMarker *m = marker_copy ((MarlinMarker *) p->data);
			/* We keep the list back to front so that
			   when we replay the list, it'll be played the
			   correct order, but I don't really think it
			   matters all that much to be honest. */
			c->markers = g_list_prepend (c->markers, m);
		}

		u = marlin_undoable_new (clear_markers_undo,
					 clear_markers_redo,
					 clear_markers_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	for (p = model->priv->markers; p;) {
		MarlinMarker *marker = p->data;

		p = p->next;
		marlin_marker_model_remove_marker (model, marker, ctxt);
	}
	g_list_free (model->priv->markers);
	model->priv->markers = NULL;
}

struct _move_markers_closure {
	MarlinMarkerModel *model;
	GList *markers;

	guint64 position;
	gint64 offset;
};

static void
move_markers_undo (gpointer data)
{
	struct _move_markers_closure *d = data;
	GList *p;

	for (p = d->markers; p; p = p->next) {
		MarlinMarker *m = p->data;
		MarlinMarker *r;

		r = marker_find (d->model, m->position, m->name);
		if (r) {
			marlin_marker_model_move_marker (d->model, r,
							 r->position + d->offset,
							 NULL);
		}
	}
}

static void
move_markers_redo (gpointer data)
{
	struct _move_markers_closure *d = data;

	marlin_marker_model_move_markers_after (d->model, d->position, d->offset, NULL);
}

static void
move_markers_destroy (gpointer data)
{
	struct _move_markers_closure *d = data;
	GList *p;

	for (p = d->markers; p; p = p->next) {
		MarlinMarker *m = p->data;

		marker_free (m);
	}
	g_list_free (d->markers);
	g_free (d);
}

/**
 * marlin_marker_model_move_markers_after:
 * @model: The model.
 * @position: The after position.
 * @offset: The offset to move them.
 *
 * Moves any markers after @position @offset frames.
 */
void
marlin_marker_model_move_markers_after (MarlinMarkerModel *model,
					guint64 position,
					gint64 offset,
					MarlinUndoContext *ctxt)
{
	struct _move_markers_closure *c = NULL;
	GList *p;

	g_return_if_fail (model != NULL);

	if  (ctxt) {
		c = g_new (struct _move_markers_closure, 1);
		c->model = model;
		c->markers = NULL;
		c->position = position;
		c->offset = offset;
	}

	for (p = model->priv->markers; p; p = p->next) {
		MarlinMarker *marker = p->data;

		if (marker->position >= position) {
			if (ctxt) {
				c->markers = g_list_prepend (c->markers,
							     marker_copy (marker));
			}
			marlin_marker_model_move_marker (model, marker, marker->position + offset, ctxt);
		}
	}

	if (ctxt) {
		MarlinUndoable *u;

		u = marlin_undoable_new (move_markers_undo,
					 move_markers_redo,
					 move_markers_destroy,
					 c);
		marlin_undo_context_add (ctxt, u);
	}
}

struct _remove_markers_closure {
	MarlinMarkerModel *model;
	GList *markers;
	
	guint64 start;
	guint64 end;
};

static void
remove_markers_undo (gpointer data)
{
	struct _remove_markers_closure *d = data;
	GList *p;

	for (p = d->markers; p; p = p->next) {
		MarlinMarker *m = p->data;

		marlin_marker_model_add_marker (d->model, m->position, m->name, NULL);
	}
}

static void
remove_markers_redo (gpointer data)
{
	struct _remove_markers_closure *d = data;

	marlin_marker_model_remove_markers_in_range (d->model, d->start, d->end, NULL);
}

static void
remove_markers_destroy (gpointer data)
{
	struct _remove_markers_closure *d = data;
	GList *p;
	
	for (p = d->markers; p; p = p->next) {
		MarlinMarker *m = p->data;

		marker_free (m);
	}
	g_list_free (d->markers);
	g_free (d);
}

/**
 * marlin_marker_model_remove_markers_in_range:
 * @model: The model.
 * @start: The start of the range.
 * @end: The end of the range.
 *
 * Removes any markers between @start and @end.
 */
void
marlin_marker_model_remove_markers_in_range (MarlinMarkerModel *model,
					     guint64 start,
					     guint64 end,
					     MarlinUndoContext *ctxt)
{
	struct _remove_markers_closure *c = NULL;
	GList *p;

	g_return_if_fail (model != NULL);

	if (ctxt) {
		c = g_new (struct _remove_markers_closure, 1);
		c->model = model;
		c->markers = NULL;
		c->start = start;
		c->end = end;
	}

	for (p = model->priv->markers; p;) {
		MarlinMarker *marker = p->data;

		/* We increment here, because if marker is removed
		   p is no longer valid */
		p = p->next;

		if (marker->position >= start &&
		    marker->position <= end) {
			if (ctxt) {
				c->markers = g_list_prepend (c->markers,
							     marker_copy (marker));
			}

			marlin_marker_model_remove_marker (model, marker, ctxt);
		}
	}

	if (ctxt) {
		MarlinUndoable *u;

		u = marlin_undoable_new (remove_markers_undo,
					 remove_markers_redo,
					 remove_markers_destroy,
					 c);
		marlin_undo_context_add (ctxt, u);
	}
}
