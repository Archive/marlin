/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <marlin/marlin-undo-manager.h>

#undef LOCK_DEBUG
/* #define LOCK_DEBUG 1 */
#include <marlin/marlin-read-write-lock.h>

#include <marlin/marlin-sample.h>

enum {
	CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SAMPLE,
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_UNDO_MANAGER_TYPE, MarlinUndoManagerPrivate))
G_DEFINE_TYPE (MarlinUndoManager, marlin_undo_manager, G_TYPE_OBJECT);

static guint signals[LAST_SIGNAL];

struct _MarlinUndoManagerPrivate {
	MarlinSample *sample;

	GList *contexts, *undo, *redo;

	MarlinUndoContext *working;
};

struct _MarlinUndoContext {
	MarlinReadWriteLock *lock;
	char *name;
	int count;

	GList *undoables;
};

static void
context_free (MarlinUndoContext *ctxt)
{
	GList *u;

	WRITE_LOCK (ctxt->lock);

	g_free (ctxt->name);
	for (u = ctxt->undoables; u; u = u->next) {
		MarlinUndoable *undoable = u->data;

		marlin_undoable_free (undoable);
	}

	g_list_free (ctxt->undoables);
	WRITE_UNLOCK (ctxt->lock);

	marlin_read_write_lock_destroy (ctxt->lock);
	g_free (ctxt);
}

static MarlinUndoContext *
context_new (const char *name)
{
	MarlinUndoContext *ctxt;

	ctxt = g_new (MarlinUndoContext, 1);
	ctxt->lock = marlin_read_write_lock_new ();

	ctxt->name = g_strdup (name);
	ctxt->count = 0;
	ctxt->undoables = NULL;

	return ctxt;
}

static void
context_undo (MarlinUndoContext *ctxt)
{
	GList *p;

	READ_LOCK (ctxt->lock);

	/* Work through the undoables in the context, calling their undo
	   function */
	for (p = ctxt->undoables; p; p = p->next) {
		marlin_undoable_undo (p->data);
	}
	READ_UNLOCK (ctxt->lock);
}

static void
context_redo (MarlinUndoContext *ctxt)
{
	GList *p;

	READ_LOCK (ctxt->lock);

	/* Work though the undoables in the context backwards, calling
	   their redo function */
	for (p = g_list_last (ctxt->undoables); p; p = p->prev) {
		marlin_undoable_redo (p->data);
	}
	READ_UNLOCK (ctxt->lock);
}

static void
finalize (GObject *object)
{
	MarlinUndoManager *manager;
	MarlinUndoManagerPrivate *priv;
	GList *p;

	manager = MARLIN_UNDO_MANAGER (object);
	priv = manager->priv;

	for (p = priv->contexts; p; p = p->next) {
		MarlinUndoContext *ctxt = p->data;
		context_free (ctxt);
	}
	g_list_free (priv->contexts);

	G_OBJECT_CLASS (marlin_undo_manager_parent_class)->finalize (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinUndoManager *manager;
	MarlinUndoManagerPrivate *priv;

	manager = MARLIN_UNDO_MANAGER (object);
	priv = manager->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		priv->sample = g_value_get_object (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
}

static void
marlin_undo_manager_class_init (MarlinUndoManagerClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	g_type_class_add_private (object_class,
				  sizeof (MarlinUndoManagerPrivate));
	signals[CHANGED] = g_signal_new ("changed",
					 G_TYPE_FROM_CLASS (klass),
					 G_SIGNAL_RUN_FIRST |
					 G_SIGNAL_NO_RECURSE,
					 G_STRUCT_OFFSET (MarlinUndoManagerClass, changed),
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);

	g_object_class_install_property (object_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample", "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
}

static void
marlin_undo_manager_init (MarlinUndoManager *manager)
{
	manager->priv = g_new0 (MarlinUndoManagerPrivate, 1);
}

/**
 * marlin_undo_manager_new:
 *
 * Create a new #MarlinUndoManager object which keeps track of undo/redo 
 * state.
 *
 * Return value: Creates a #MarlinUndoManager object
 */
MarlinUndoManager *
marlin_undo_manager_new (void)
{
	MarlinUndoManager *manager;

	manager = g_object_new (MARLIN_UNDO_MANAGER_TYPE, NULL);
	return manager;
}

/**
 * marlin_undo_manager_can_undo:
 * @manager: A #MarlinUndoManager object
 *
 * Checks whether @manager is able to undo.
 *
 * Return value: #TRUE if @manager can undo, #FALSE otherwise
 */
gboolean
marlin_undo_manager_can_undo (MarlinUndoManager *manager)
{
	return (manager->priv->undo != NULL);
}

/**
 * marlin_undo_manager_can_redo:
 * @manager: A #MarlinUndoManager object
 *
 * Checks whether @manager is able to redo.
 *
 * Return value: #TRUE if @manager can redo, #FALSE otherwise
 */
gboolean
marlin_undo_manager_can_redo (MarlinUndoManager *manager)
{
	return (manager->priv->redo != NULL);
}

/* FIXME: Should these return copies of the names
   so that they're threadsafe? */
/**
 * marlin_undo_manager_get_undo_name:
 * @manager: A #MarlinUndoManager object
 *
 * Gets the name of the next MarlinUndoContext that will be undone.
 *
 * Return value: A string that should not be freed
 */
const char *
marlin_undo_manager_get_undo_name (MarlinUndoManager *manager)
{
	if (manager->priv->undo) {
		MarlinUndoContext *ctxt = manager->priv->undo->data;
		return ctxt->name;
	} else {
		return "";
	}
}

/**
 * marlin_undo_manager_get_redo_name:
 * @manager: A #MarlinUndoManager object
 *
 * Gets the name of the next MarlinUndoContext that will be redone.
 *
 * Return value: A string that should not be freed
 */
const char *
marlin_undo_manager_get_redo_name (MarlinUndoManager *manager)
{
	if (manager->priv->redo) {
		MarlinUndoContext *ctxt = manager->priv->redo->data;
		return ctxt->name;
	} else {
		return "";
	}
}

/**
 * marlin_undo_manager_context_begin:
 * @manager: A #MarlinUndoManager object
 * @name: The name of the MarlinUndoContext
 *
 * Begins a new context in @manager called @name. If a context is already
 * in progress, then the current context will be returned.
 *
 * Return value: A #MarlinUndoContext struct.
 */
MarlinUndoContext *
marlin_undo_manager_context_begin (MarlinUndoManager *manager,
				   const char        *name)
{
	MarlinUndoContext *ctxt;

	/* If the manager is already in the middle of a context
	   just return the current context */
	if (manager->priv->working != NULL) {
		WRITE_LOCK (manager->priv->working->lock);

		/* Increment the ref count so that the context will not
		   be destroyed with context_end */
		manager->priv->working->count++;
		WRITE_UNLOCK (manager->priv->working->lock);

		return manager->priv->working;
	}

	ctxt = context_new (name);
	ctxt->count++;
	manager->priv->working = ctxt;

	return ctxt;
}

/**
 * marlin_undo_manager_context_end:
 * @manager: A #MarlinUndoManager object
 * @ctxt: A #MarlinUndoContext
 *
 * Ends @ctxt adding it to the undo/redo list managed by @manager, then emits
 * the changed signal.
 */
void
marlin_undo_manager_context_end (MarlinUndoManager *manager,
				 MarlinUndoContext *ctxt)
{
	MarlinUndoManagerPrivate *priv;

	priv = manager->priv;

	WRITE_LOCK (ctxt->lock);
	ctxt->count--;
	WRITE_UNLOCK (ctxt->lock);

	if (ctxt->count > 0) {
		return;
	}

	if (priv->contexts == NULL) {
		priv->contexts = g_list_append (NULL, ctxt);
		priv->undo = priv->contexts;
	} else {
		GList *pruned = priv->redo;
		GList *n;

		if (pruned != NULL) {
			GList *p;

			/* Disconnect pruned */
			if (priv->redo && priv->redo->prev) {
				priv->redo->prev->next = NULL;
			}
			priv->redo = NULL;
			pruned->prev = NULL;

			for (p = pruned; p; p = p->next) {
				MarlinUndoContext *c = p->data;

				context_free (c);
			}
			g_list_free (pruned);
		}

		/* Append our new context and move the position pointer on */
		n = g_list_append (priv->undo, ctxt);
		if (priv->undo == NULL) {
			priv->undo = n;
		} else {
			priv->undo = priv->undo->next;
		}
		priv->redo = NULL;
	}

	priv->working = NULL;

	g_signal_emit (manager, signals[CHANGED], 0);
}

/**
 * marlin_undo_manager_context_cancel:
 * @manager: A #MarlinUndoManager object
 * @ctxt: A #MarlinUndoContext
 *
 * Cancels @ctxt freeing it.
 */
void
marlin_undo_manager_context_cancel (MarlinUndoManager *manager,
				    MarlinUndoContext *ctxt)
{
	g_return_if_fail (manager->priv->working == ctxt);

	manager->priv->working = NULL;
	context_free (ctxt);
}

/**
 * marlin_undo_manager_undo:
 * @manager: A #MarlinUndoManager object
 *
 * Undoes the next item in the undo list and emits the changed signal.
 */
void
marlin_undo_manager_undo (MarlinUndoManager *manager)
{
	if (manager->priv->undo) {
		context_undo ((MarlinUndoContext *) manager->priv->undo->data);
		manager->priv->redo = manager->priv->undo;
		manager->priv->undo = manager->priv->undo->prev;

		g_signal_emit (manager, signals[CHANGED], 0);
	}
}

/**
 * marlin_undo_manager_redo:
 * @manager: A #MarlinUndoManager object
 *
 * Redoes the next item in the redo list and emits the changed signal.
 */
void
marlin_undo_manager_redo (MarlinUndoManager *manager)
{
	if (manager->priv->redo) {
		context_redo ((MarlinUndoContext *) manager->priv->redo->data);
		manager->priv->undo = manager->priv->redo;
		manager->priv->redo = manager->priv->redo->next;

		g_signal_emit (manager, signals[CHANGED], 0);
	}
}

/**
 * marlin_undo_manager_get_history:
 * @manager: A #MarlinUndoManager object
 *
 * Creates a list of #MarlinUndoHistory<!-- --> structures that represent the
 * #MarlinUndoContext<!-- -->'s in @manager.
 *
 * Return value: A #GList of #MarlinUndoHistory<!-- --> structures that should
 * be freed with #marlin_undo_manager_free_history_list.
 */
GList *
marlin_undo_manager_get_history (MarlinUndoManager *manager)
{
	GList *history = NULL;
	GList *p;
	MarlinUndoHistory *hist;

	hist = g_new (MarlinUndoHistory, 1);
	hist->name = g_strdup (_("Original Sample"));

	g_object_get (G_OBJECT (manager->priv->sample),
		      "name", &hist->info,
		      NULL);
	/* hist->info will be freed later on */
	hist->current = FALSE;
	hist->ctxt = NULL;

 	history = g_list_prepend (history, hist);

	for (p = manager->priv->contexts; p; p = p->next) {
		MarlinUndoContext *ctxt = p->data;

		hist = g_new (MarlinUndoHistory, 1);
		hist->name = g_strdup (ctxt->name);
		hist->info = NULL;
		hist->ctxt = ctxt;

 		history = g_list_append (history, hist);
		if (p == manager->priv->undo) {
			hist->current = TRUE;
		} else {
			hist->current = FALSE;
		}
	}

	return history;
}

/**
 * marlin_undo_manager_free_history_list:
 * @history_list: A #GList containing #MarlinUndoHistory structs
 *
 * Frees a list of #MarlinUndoHistory structs that is returned from
 * marlin_undo_manager_get_history.
 */
void
marlin_undo_manager_free_history_list (GList *history_list)
{
	GList *h;

	for (h = history_list; h; h = h->next) {
		MarlinUndoHistory *history = h->data;
		g_free (history->name);
		g_free (history);
	}

	g_list_free (history_list);
}

/**
 * marlin_undo_context_add:
 * @ctxt: A #MarlinUndoContext
 * @undoable: A #MarlinUndoable
 *
 * Add @undoable to @ctxt.
 */
void
marlin_undo_context_add (MarlinUndoContext *ctxt,
			 MarlinUndoable    *undoable)
{
	WRITE_LOCK (ctxt->lock);
	ctxt->undoables = g_list_prepend (ctxt->undoables, undoable);
	WRITE_UNLOCK (ctxt->lock);
}
