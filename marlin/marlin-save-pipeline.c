/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include <gst/gst.h>

#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-save-pipeline.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-channel-src.h>
#include <marlin/marlin-channel-joiner.h>

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_SAVE_PIPELINE_TYPE, MarlinSavePipelinePrivate))
G_DEFINE_TYPE (MarlinSavePipeline, marlin_save_pipeline, MARLIN_PIPELINE_TYPE);

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_ENCODER,
	PROP_MIMETYPE,
	PROP_FILENAME,
};

struct _MarlinSavePipelinePrivate {
	MarlinSample *sample;

	GstElement *sink, *audioconvert, *joiner;
	GstElement *encoder;

	GPtrArray *srcs; /* Array of MarlinChannelSrc */
};

static void
finalize (GObject *object)
{
	MarlinSavePipeline *pipeline;
	MarlinSavePipelinePrivate *priv;

	pipeline = MARLIN_SAVE_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->srcs) {
		g_ptr_array_free (priv->srcs, TRUE);
	}

	G_OBJECT_CLASS (marlin_save_pipeline_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinSavePipeline *pipeline;
	MarlinSavePipelinePrivate *priv;

	pipeline = MARLIN_SAVE_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->sample != NULL) {
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}

	G_OBJECT_CLASS (marlin_save_pipeline_parent_class)->dispose (object);
}

static gboolean
connect_pipeline (MarlinSavePipeline *pipeline,
		  GError            **error)
{
	MarlinSavePipelinePrivate *priv;
	guint channels;
	int i;

	priv = GET_PRIVATE (pipeline);

	g_object_get (priv->sample,
		      "channels", &channels,
		      NULL);

	g_print ("Connecting %d channels\n", channels);
	priv->srcs = g_ptr_array_sized_new (channels);
	for (i = 0; i < channels; i++) {
		MarlinChannel *channel;
		GstElement *src;
		GstPad *srcpad, *sinkpad;
		char *name;

		name = g_strdup_printf ("src-%d", i);
		src = gst_element_factory_make ("marlin-channel-src", name);
		g_free (name);

		channel = marlin_sample_get_channel (priv->sample, i);

		g_object_set (G_OBJECT (src),
			      "sample", priv->sample,
			      "channel", channel,
			      NULL);

		gst_bin_add (GST_BIN (pipeline), src);

		sinkpad = gst_element_get_request_pad (priv->joiner, "sink%d");
		srcpad = gst_element_get_static_pad (src, "src");

		if (gst_pad_link (srcpad, sinkpad) != GST_PAD_LINK_OK) {
			gst_bin_remove (GST_BIN (pipeline), src);
			gst_element_release_request_pad (priv->joiner, sinkpad);

			g_warning ("Link failed src joiner");
			/* Set error */
			return FALSE;
		} 

		g_ptr_array_add (priv->srcs, src);
	}

	/* srcs are now linked, link the rest of the pipeline */
	if (gst_element_link (priv->joiner, priv->audioconvert) == FALSE) {
		/* Set error */
		
		g_warning ("joiner audioconcert failed");
		return FALSE;
	}

	if (gst_element_link (priv->audioconvert, priv->encoder) == FALSE) {
		g_warning ("audioconvert encoder failed");
		/* Set error */

		return FALSE;
	}

	if (gst_element_link (priv->encoder, priv->sink) == FALSE) {
		/* Set error */
		g_warning ("encoder sink failed");
		return FALSE;
	}

	return TRUE;
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinSavePipeline *pipeline;
	MarlinSavePipelinePrivate *priv;

	pipeline = MARLIN_SAVE_PIPELINE (object);
	priv = pipeline->priv;
	GError *error = NULL;

	switch (prop_id) {
	case PROP_SAMPLE:
		if (priv->sample) {
			g_object_unref (priv->sample);
		}

		priv->sample = MARLIN_SAMPLE (g_value_dup_object (value));

		if (priv->sample && priv->encoder) {
			if (connect_pipeline (pipeline, &error) == FALSE) {
				/* Error */
				
				g_error_free (error);
			}
		}
		break;

	case PROP_ENCODER:
		if (priv->encoder) {
			gst_bin_remove (GST_BIN (pipeline), priv->encoder);
			g_object_unref (priv->encoder);
		}

		priv->encoder = GST_ELEMENT (g_value_dup_object (value));

		gst_bin_add (GST_BIN (pipeline), priv->encoder);
		if (priv->sample && priv->encoder) {
			if (connect_pipeline (pipeline, &error) == FALSE) {
				/* Error */

				if (error) {
					g_error_free (error);
				}
			}
		}
		break;

	case PROP_FILENAME:
		g_object_set (G_OBJECT (priv->sink),
			      "location", g_value_get_string (value),
			      NULL);
		break;

	case PROP_MIMETYPE:
		if (priv->encoder) {
			gst_bin_remove (GST_BIN (pipeline), priv->encoder);
			g_object_unref (priv->encoder);
		}

		priv->encoder = get_encoder_for_mime (g_value_get_string (value));
		if (priv->encoder == NULL) {
			return;
		}

		gst_bin_add (GST_BIN (pipeline), priv->encoder);

		if (priv->sample && priv->encoder) {
			if (connect_pipeline (pipeline, &error) == FALSE) {
				/* Error */
				if (error) {
					g_error_free (error);
				}
			}
		}
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
}

static guint64
report_position (MarlinPipeline *marlin_pipeline)
{
	MarlinSavePipeline *pipeline;
	MarlinSavePipelinePrivate *priv;
	GstElement *element;
	GstFormat fmt;
	gint64 pos;
	guint64 frames;
	
	pipeline = (MarlinSavePipeline *) marlin_pipeline;
	priv = pipeline->priv;

 	element = (GstElement *) pipeline;

	g_object_get (priv->sample,
		      "total_frames", &frames,
		      NULL);
	fmt = GST_FORMAT_BYTES;
	if (gst_element_query_position (element, &fmt, &pos)) {
		MarlinOperation *operation;
		float p;
		int percentage;
		
		p = ((float) (pos / sizeof (float))) / ((float) frames);
		percentage = (int) (p * 100);

		/* FIXME: This should be moved to MarlinPipeline */
		operation = marlin_pipeline_get_operation (marlin_pipeline);
		if (operation) {
			marlin_operation_progress (operation, percentage);
		}
					
		return pos;
	}

	g_print ("report failed\n");
	return 0;
}

static void
marlin_save_pipeline_class_init (MarlinSavePipelineClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	MarlinPipelineClass *p_class;

	p_class = (MarlinPipelineClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	p_class->report_position = report_position;

	g_type_class_add_private (o_class, sizeof (MarlinSavePipelinePrivate));

	/* Properties */
	g_object_class_install_property (o_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "Sample",
							      "The sample to be saved",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_WRITABLE |
							      G_PARAM_STATIC_NICK |
							      G_PARAM_STATIC_NAME |
							      G_PARAM_STATIC_BLURB));
	g_object_class_install_property (o_class,
					 PROP_ENCODER,
					 g_param_spec_object ("encoder",
							      "Encoder",
							      "The encoder element to use to encode the sample",
							      GST_TYPE_ELEMENT,
							      G_PARAM_WRITABLE |
							      G_PARAM_STATIC_NICK |
							      G_PARAM_STATIC_NAME |
							      G_PARAM_STATIC_BLURB));

	g_object_class_install_property (o_class,
					 PROP_FILENAME,
					 g_param_spec_string ("filename",
							      "Filename",
							      "What the sample should be written to",
							      "",
							      G_PARAM_WRITABLE |
							      G_PARAM_STATIC_NICK |
							      G_PARAM_STATIC_NAME |
							      G_PARAM_STATIC_BLURB));

	g_object_class_install_property (o_class,
					 PROP_MIMETYPE,
					 g_param_spec_string ("mimetype",
							      "Mimetype",
							      "What format the sample should be in",
							      "",
							      G_PARAM_WRITABLE |
							      G_PARAM_STATIC_NICK |
							      G_PARAM_STATIC_NAME |
							      G_PARAM_STATIC_BLURB));
}

static void
marlin_save_pipeline_init (MarlinSavePipeline *pipeline)
{
	MarlinSavePipelinePrivate *priv;

	pipeline->priv = GET_PRIVATE (pipeline);
	priv = pipeline->priv;

	priv->joiner = gst_element_factory_make ("marlin-channel-joiner",
						 "joiner");
	priv->audioconvert = gst_element_factory_make ("audioconvert",
						       "convert");
	priv->sink = gst_element_factory_make ("filesink", "sink");

	gst_bin_add_many (GST_BIN (pipeline), priv->joiner, priv->audioconvert,
			  priv->sink, NULL);

	/* Connect when all the complete parts are present */
}
