/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __HAVE_MARLIN_LIST_H__
#define __HAVE_MARLIN_LIST_H__

typedef struct _MarlinListNode MarlinListNode;
struct _MarlinListNode {
	MarlinListNode *previous;
	MarlinListNode *next;
};

typedef struct _MarlinList {
	MarlinListNode *first;
	MarlinListNode *last;

	GDestroyNotify destroyer;
} MarlinList;

MarlinList *marlin_list_new (GDestroyNotify destroyer);
void marlin_list_free (MarlinList *list);
void marlin_list_clear (MarlinList *list);

void marlin_list_append (MarlinList *list,
			 MarlinListNode *a);
void marlin_list_remove (MarlinList *list,
			 MarlinListNode *a);
void marlin_list_insert_after (MarlinList *list,
			       MarlinListNode *insert_point,
			       MarlinListNode *node);
MarlinListNode *marlin_list_node_next (MarlinListNode *a);
MarlinListNode *marlin_list_node_previous (MarlinListNode *a);

#endif
