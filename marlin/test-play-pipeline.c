/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gst/gst.h>
#include <gtk/gtk.h>

#include <marlin/marlin-play-pipeline.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-load-pipeline.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-gst-extras.h>

static GstState state = GST_STATE_NULL;

GtkWidget *window;
GtkWidget *progress;
MarlinPlayPipeline *pipeline;
MarlinLoadPipeline *load_pipeline;
MarlinSample *sample;
gboolean repeat = FALSE;
GMainLoop *mainloop;

static void
play_clicked (GtkButton *b,
	      gpointer d)
{
	
	switch (state) {
	case GST_STATE_PLAYING:
		break;

	case GST_STATE_PAUSED:
	case GST_STATE_NULL:
	case GST_STATE_READY:
		gst_element_set_state (GST_ELEMENT (pipeline),
				       GST_STATE_PLAYING);
		break;
	}

	state = GST_STATE_PLAYING;
}

static void
pause_clicked (GtkButton *b,
	       gpointer d)
{
	if (pipeline == NULL) {
		return;
	}
	
	switch (state) {
	case GST_STATE_PAUSED:
		gst_element_set_state (GST_ELEMENT (pipeline), 
				       GST_STATE_PLAYING);
		state = GST_STATE_PLAYING;
		return;
		
	case GST_STATE_PLAYING:
	case GST_STATE_NULL:
	case GST_STATE_READY:
		gst_element_set_state (GST_ELEMENT (pipeline), 
				       GST_STATE_PAUSED);
		break;

	}

	state = GST_STATE_PAUSED;
}

static void
stop_clicked (GtkButton *b,
	      gpointer d)
{
	if (pipeline == NULL) {
		return;
	}

	repeat = FALSE;
	switch (state) {
	case GST_STATE_PLAYING:
	case GST_STATE_PAUSED:
	case GST_STATE_NULL:
		gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_READY);
		break;

	case GST_STATE_READY:
		break;
	}

	state = GST_STATE_READY;
}

static void
repeat_clicked (GtkButton *b,
		gpointer d)
{
#if 0
	g_print ("Repeating\n");
	repeat = TRUE;
	marlin_play_pipeline_seek_range (pipeline, (guint64) 64000, (guint64) 120000);
	play_clicked (NULL, NULL);
#endif
}

static void
load_finished (MarlinOperation *operation)
{
	gtk_widget_set_sensitive (window, TRUE);

	/* Create a new pipeline */
	pipeline = marlin_play_pipeline_new_from_sample (sample);

	gst_element_set_state (GST_ELEMENT (load_pipeline), GST_STATE_NULL);
	g_object_unref (G_OBJECT (load_pipeline));
}

static void
load_progress (MarlinOperation *operation,
	       int              percentage,
	       gpointer         userdata)
{
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress),
				       ((double) percentage) / 100.0);
}

static gboolean
idle_func (gpointer userdata)
{
	char *filename = userdata;
	MarlinOperation *operation;
	
	operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (operation), "progress",
			  G_CALLBACK (load_progress), NULL);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (load_finished), NULL);

	load_pipeline = marlin_load_pipeline_new (operation);
	g_object_set (G_OBJECT (load_pipeline),
		      "sample", sample,
		      "filename", filename,
		      NULL);

	gst_element_set_state (GST_ELEMENT (load_pipeline), GST_STATE_PLAYING);
	
	return FALSE;
}

static void
destroy_cb (GObject *obj,
	    gpointer d)
{
	gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_NULL);
	g_object_unref (pipeline);
	g_object_unref (sample);

	g_main_loop_quit (mainloop);
}

int
main (int argc,
      char **argv)
{
	GtkWidget *hbox, *vbox;
	GtkWidget *play, *pause, *stop, *rep;
	
	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);

	if (argc != 2) {
		g_print ("Usage: %s <filename>\n", argv[0]);
		exit (0);
	}

	marlin_gst_register ();

	sample = marlin_sample_new ();

	mainloop = g_main_loop_new (NULL, FALSE);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (destroy_cb), NULL);
	
	gtk_window_set_title (GTK_WINDOW (window), argv[1]);

	vbox = gtk_vbox_new (TRUE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);

	hbox = gtk_hbox_new (TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	play = gtk_button_new_with_label ("play");
	gtk_box_pack_start (GTK_BOX (hbox), play, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (play), "clicked",
			  G_CALLBACK (play_clicked), NULL);

	rep = gtk_button_new_with_label ("repeat");
	gtk_box_pack_start (GTK_BOX (hbox), rep, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (rep), "clicked",
			  G_CALLBACK (repeat_clicked), NULL);
	
	pause = gtk_button_new_with_label ("pause");
	gtk_box_pack_start (GTK_BOX (hbox), pause, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (pause), "clicked",
			  G_CALLBACK (pause_clicked), NULL);

	stop = gtk_button_new_with_label ("stop");
	gtk_box_pack_start (GTK_BOX (hbox), stop, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (stop), "clicked",
			  G_CALLBACK (stop_clicked), NULL);

	progress = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), progress, FALSE, FALSE, 0);

	gtk_widget_set_sensitive (window, FALSE);

	gtk_widget_show_all (window);

	g_idle_add (idle_func, argv[1]);
	g_main_loop_run (mainloop);
}
