/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_JACK_PLAY_H__
#define __MARLIN_JACK_PLAY_H__

#include <glib-object.h>

#include <marlin/marlin-jack.h>

G_BEGIN_DECLS

#define MARLIN_JACK_PLAY_TYPE (marlin_jack_play_get_type ())
#define MARLIN_JACK_PLAY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_JACK_PLAY_TYPE, MarlinJackPlay))
#define MARLIN_JACK_PLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_JACK_PLAY_TYPE, MarlinJackPlayClass))

typedef struct _MarlinJackPlay MarlinJackPlay;
typedef struct _MarlinJackPlayPrivate MarlinJackPlayPrivate;
typedef struct _MarlinJackPlayClass MarlinJackPlayClass;

struct _MarlinJackPlay {
	MarlinJack parent;

	MarlinJackPlayPrivate *priv;
};

struct _MarlinJackPlayClass {
	MarlinJackClass parent_class;

	/* Signals */
	void (*eos) (MarlinJack *jack);
	void (*status_changed) (MarlinJack *jack,
				int status);
	void (*position_changed) (MarlinJack *jack,
				  guint64 position);
};

GType marlin_jack_play_get_type (void);
MarlinJackPlay *marlin_jack_play_new (jack_client_t *client);

void marlin_jack_play_set_repeating (MarlinJackPlay *jack,
				     gboolean        repeating);
void marlin_jack_play_set_range (MarlinJackPlay *jack,
				 guint64         start,
				 guint64         finish);


G_END_DECLS

#endif
