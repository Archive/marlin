/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <math.h>

#include <jack/jack.h>
#include <jack/ringbuffer.h>

#include <samplerate.h>

#include <marlin/marlin-block.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-jack.h>

enum {
	PROP_SAMPLE = 1,
	PROP_EMIT_LEVEL,
};

enum {
	LEVEL_CHANGED,
	LAST_SIGNAL
};

typedef struct _MarlinJackPrivate {
	MarlinSample *sample;
	guint channels;

	gboolean emit_level;
} MarlinJackPrivate;

G_DEFINE_ABSTRACT_TYPE (MarlinJack, marlin_jack, G_TYPE_OBJECT);

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_JACK_TYPE, MarlinJackPrivate))

static guint32 signals[LAST_SIGNAL] = { 0, };

GQuark
marlin_jack_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-jack-error-quark");
	}

	return quark;
}

static void
finalize (GObject *object)
{
	((GObjectClass *) marlin_jack_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinJackPrivate *priv = GET_PRIVATE (object);

	if (priv->sample) {
		g_object_unref (priv->sample);
		priv->sample = NULL;
	}

	((GObjectClass *) marlin_jack_parent_class)->dispose (object);
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (object);
	MarlinJackPrivate *priv = GET_PRIVATE (object);

	switch (prop_id) {
	case PROP_SAMPLE:
		if (priv->sample) {
			g_object_unref (priv->sample);
			priv->sample = NULL;
		}

		priv->sample = g_value_dup_object (value);

		/* FIXME: Need to hook to the notify signal */
		g_object_get (priv->sample,
			      "channels", &priv->channels,
			      NULL);

		if (j_class->set_sample) {
			j_class->set_sample (MARLIN_JACK (object),
					     priv->sample);
		}
		break;

	case PROP_EMIT_LEVEL:
		priv->emit_level = g_value_get_boolean (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
#if 0
	MarlinJackPrivate *priv = GET_PRIVATE (object);

	switch (prop_id) {
	default:
		break;
	}
#endif
}

static void
marlin_jack_class_init (MarlinJackClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	g_type_class_add_private (klass, sizeof (MarlinJackPrivate));

	g_object_class_install_property (o_class, PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_WRITABLE |
							      G_PARAM_STATIC_NAME |
							      G_PARAM_STATIC_NICK |
							      G_PARAM_STATIC_BLURB));
	g_object_class_install_property (o_class, PROP_EMIT_LEVEL,
					 g_param_spec_boolean ("emit-level",
							       "", "",
							       FALSE,
							       G_PARAM_WRITABLE |
							       G_PARAM_STATIC_NAME |
							       G_PARAM_STATIC_NICK |
							       G_PARAM_STATIC_BLURB));

	signals[LEVEL_CHANGED] = g_signal_new ("level-changed",
					       G_TYPE_FROM_CLASS (klass),
					       G_SIGNAL_RUN_LAST,
					       G_STRUCT_OFFSET (MarlinJackClass, level_changed),
					       NULL, NULL,
					       marlin_marshal_VOID__UINT_POINTER_POINTER,
					       G_TYPE_NONE, 3, G_TYPE_UINT,
					       G_TYPE_POINTER, G_TYPE_POINTER);
}

static void
marlin_jack_init (MarlinJack *jack)
{
}

/**
 * marlin_jack_start:
 * @jack: #MarlinJack client
 * @error: A #Gerror
 *
 * Starts the jack client doing whatever it does (playing/recording)
 *
 * Return value: TRUE on success, FALSE on error.
 */
gboolean
marlin_jack_start (MarlinJack *jack,
		   GError    **error)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (jack);

	if (j_class->start) {
		return j_class->start (jack, error);
	}

	g_assert_not_reached ();
}

/**
 * marlin_jack_stop:
 * @jack: #MarlinJack client
 *
 * Stops the jack client doing its thing
 */
void
marlin_jack_stop (MarlinJack *jack)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (jack);

	if (j_class->stop) {
		j_class->stop (jack);
		return;
	}

	g_assert_not_reached ();
}

/**
 * marlin_jack_pause:
 * @jack: #MarlinJack client
 *
 * Pauses the jack client.
 */
void
marlin_jack_pause (MarlinJack *jack)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (jack);

	if (j_class->pause) {
		j_class->pause (jack);
		return;
	}

	g_assert_not_reached ();
}

/**
 * marlin_jack_continue:
 * @jack: #MarlinJack client
 *
 * Continues from the paused state
 */
void
marlin_jack_continue (MarlinJack *jack)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (jack);

	if (j_class->cont) {
		j_class->cont (jack);
		return;
	}

	g_assert_not_reached ();
}

void
calculate_peak_and_rms (float  *data,
			long    n_frames,
			double *peak,
			double *rms)
{
	long i;
	double p = 0.0, total = 0.0;

	if (n_frames == 0) {
		*peak = 0.0;
		*rms = 0.0;
		return;
	}

	for (i = 0; i < n_frames; i++) {
		double squared = pow ((double) (data[i]), 2);

		p = MAX (p, squared);
		total += squared;
	}

	*peak = sqrt (p);
	*rms = sqrt (total / (double) n_frames);
}

void
marlin_jack_emit_levels (MarlinJack *jack,
			 float     **data,
			 long       *n_frames)
{
	MarlinJackPrivate *priv = GET_PRIVATE (jack);
	double p, r;
	double peak[2] = { 0.0, 0.0 };
	double rms[2] = { 0.0, 0.0 };

	if (priv->emit_level == FALSE) {
		return;
	}

	calculate_peak_and_rms (data[0], n_frames[0], &p, &r);
	peak[0] = p;
	rms[0] = r;

	if (priv->channels > 1) {
		calculate_peak_and_rms (data[1], n_frames[1], &p, &r);
		peak[1] = p;
		rms[1] = r;
	}

	g_signal_emit (jack, signals[LEVEL_CHANGED], 0,
		       priv->channels, rms, peak);
}

/**
 * marlin_jack_is_busy:
 * @jack: #MarlinJack object
 *
 * Returns if @jack is busy (playing or recording)
 *
 * Return value: TRUE if @jack is busy, FALSE otherwise
 */
gboolean
marlin_jack_is_busy (MarlinJack *jack)
{
	MarlinJackClass *j_class = MARLIN_JACK_GET_CLASS (jack);

	if (j_class->is_busy) {
		return j_class->is_busy (jack);
	}

	g_assert_not_reached ();

	/* Only for compiler really */
	return FALSE;
}
