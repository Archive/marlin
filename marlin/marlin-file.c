/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>

#include <glib/gi18n.h>

#include <marlin/marlin-file.h>

GQuark
marlin_file_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-file-error-quark");
	}

	return quark;
}

/**
 * marlin_file_new:
 * @filename:
 * @error:
 * 
 * Creates a new MarlinFile structure
 *
 * Returns: MarlinFile on success, NULL on failure with @error containing 
 * more details.
 */
MarlinFile *
marlin_file_new (const char *filename,
		 GError **error)
{
	MarlinFile *file;

	file = g_slice_new (MarlinFile);
	file->filename = g_strdup (filename);
	file->ref_count = 1;

	file->fd = mkstemp (file->filename);
	if (file->fd == -1) {
		g_print ("\n\nInternal error opening file\n\n"
			 "Error: %s (%d)\nfunction: %s\nfilename: %s\n\n", 
			 g_strerror (errno), errno, __FUNCTION__, filename);

		if (error) {
			*error = g_error_new (MARLIN_FILE_ERROR,
					      MARLIN_FILE_ERROR_IO,
					      _("Error opening file '%s'\nError: '%s'"),
					      file->filename,
					      g_strerror (errno));
		}

		g_free (file->filename);
		g_free (file);
		return NULL;
	}

	return file;
}

/**
 * marlin_file_free:
 * @file:
 *
 * Closes the file, deletes it and frees all memory allocated.
 */
void
marlin_file_free (MarlinFile *file)
{
	close (file->fd);
	
	unlink (file->filename);

	g_free (file->filename);
	g_slice_free (MarlinFile, file);
}

/* #define DEBUG_FILE_REFCOUNTING 1 */

/**
 * marlin_file_ref:
 * @file:
 *
 * Increments the ref count on @file
 */
void
marlin_file_ref (MarlinFile *file)
{
	g_return_if_fail (file != NULL);

	++file->ref_count;
#ifdef DEBUG_FILE_REFCOUNTING
	g_print ("(%p): ref -> %d\n", file, file->ref_count);
#endif
}

/**
 * marlin_file_unref:
 * @file:
 * 
 * Decrements the ref count on @file. If the ref count is 0
 * then marlin_file_free is called
 */
void
marlin_file_unref (MarlinFile *file)
{
	g_return_if_fail (file != NULL);
	
	--file->ref_count;
	
#ifdef DEBUG_FILE_REFCOUNTING
	g_print ("(%p): unref -> %d\n", file, file->ref_count);
#endif

	if (file->ref_count == 0) {
		marlin_file_free (file);
	}
}

static void G_GNUC_UNUSED
dump_region (MarlinFileRegion *region)
{
	g_print ("Region: %p\n", region);
	g_print ("offset: %llu\n", region->offset);
	g_print ("address: %p\n", region->address);
	g_print ("length: %d\n", region->length);
	g_print ("d_offset: %llu\n", region->d_offset);
	g_print ("----------\n");
}

/**
 * marlin_file_mmap_region:
 * @file:
 * @offset:
 * @length:
 * @error:
 *
 * Maps a region of @file into memory.
 *
 * Returns: A MarlinFileRegion detailing the newly mapped region
 */
MarlinFileRegion *
marlin_file_map_region (MarlinFile *file,
			off_t offset,
			size_t length,
			GError **error)
{
	MarlinFileRegion *region;
	gpointer mmap_mem;
	static long pagesize = 0;
	off_t d_offset;

	/* We get the system pagesize to align offset correctly */
	if (pagesize == 0) {
		pagesize = sysconf (_SC_PAGESIZE);
	}
	
	d_offset = offset % pagesize;
	offset -= d_offset;

	mmap_mem = mmap (0, length + d_offset,
			 PROT_READ | PROT_WRITE,
			 MAP_SHARED, file->fd,
			 offset);
	if (mmap_mem == MAP_FAILED) {
		g_print ("\n\nInternal error mapping data\n\n"
			 "Error: %s (%d)\nfunction: %s\nfile: %p\nname: %s\noffset: %lld\nlength: %d\n\n",
			 g_strerror (errno), errno, __FUNCTION__,
			 file, file->filename, offset, length);

		g_print ("pagesize: %ld\nd_offset: %lld", pagesize, d_offset);
		if (error) {
			*error = g_error_new (MARLIN_FILE_ERROR,
					      MARLIN_FILE_ERROR_MEMORY,
					      _("Failed to allocate memory."));
		}
		
		return NULL;
	}
	
	region = g_new (MarlinFileRegion, 1);
	region->file = file;
	region->offset = offset;
	region->address = mmap_mem;
	region->length = length + d_offset;
	region->d_offset = d_offset;
	
	return region;
}

/**
 * marlin_file_unmap_region:
 * @file:
 * @region:
 *
 * Unmaps the area of memory described by @region.
 * @region is freed afterwards.
 */
void
marlin_file_unmap_region (MarlinFile *file,
			  MarlinFileRegion *region)
{
	munmap (region->address, region->length);
	g_free (region);
}

/**
 * marlin_file_write_data:
 * @file:
 * @data:
 * @length:
 * @error:
 *
 * Writes @length bytes starting at @data into @file.
 *
 * Returns: Offset at which the data was written on success, -1 on failure
 * with further details in @error.
 */
off_t
marlin_file_write_data (MarlinFile *file,
			gpointer data,
			size_t length,
			GError **error)
{
	size_t size;
	off_t offset;
	int ret;
	
	g_return_val_if_fail (file != NULL, -1);
	
	/* Find the start of where the data is being written */
	offset = lseek (file->fd, 0, SEEK_END);

	/* 06-06-05: This seems to work. Remove the comment below */
	/* FIXME (25-05-05): This probably needs to be set to the nearest
	   multiple of the page size, so that mmap will succeed,
	   but I've forgotten what "to save having to munmap the last block"
	   means but I'm leaving it here so that when the rewrite is finished
	   I'll be able to work it out when it all goes wrong */
	/* FIXME: MARLIN_BLOCK_SIZE can be quite wasteful :) */
	/* Even if we have < MARLIN_BLOCK_SIZE of data, we always want
	   to write a full block out, so that we can map a full block
	   to save having to unmap the last block, plus it makes sure
	   that the offset is a multiple of the page size. */
/* 	size = MARLIN_BLOCK_SIZE * sizeof (float); */
	size = length;

 try_to_write:
	ret = write (file->fd, data, size);
	if (ret == -1) {
		g_print ("\n\nInternal error writing sample to file\n\n"
			 "Error: %s (%d)\nfunction: %s\nfile: %p\nfilename: %s\ndata: %p\nlength: %d\n\n",
			 g_strerror (errno), errno, __FUNCTION__,
			 file, file->filename, data, length);

		if (error) {
			if (errno == ENOSPC) {
				*error = g_error_new (MARLIN_FILE_ERROR,
						      MARLIN_FILE_ERROR_NO_SPACE,
						      _("There was not enough space for '%s'"),
						      file->filename);
			} else {
				*error = g_error_new (MARLIN_FILE_ERROR,
						      MARLIN_FILE_ERROR_IO,
						      _("Error writing data to '%s'\nError: %s"),
						      file->filename,
						      g_strerror (errno));
			}
		}
		
		return -1;
	}
	
	if (ret != size) {
		size = size - ret;
		data += (ret / sizeof (float));
		
		goto try_to_write;
	}
	
	return offset;
}
