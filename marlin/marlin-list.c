/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include <marlin/marlin-list.h>

/**
 * marlin_list_new:
 * @destroyer: The function called when the children need to be freed
 *
 * Creates an empty #MarlinList. @destroyer is called whenever the list is freed
 * or cleared and should clean up any allocated memory used to create the
 * MarlinListNode derived types. If @destroyer is NULL, then g_free is used
 * as the default.
 *
 * Return value: A newly allocated
 */
MarlinList *
marlin_list_new (GDestroyNotify destroyer)
{
	MarlinList *list;

	list = g_new0 (MarlinList, 1);
	if (destroyer) {
		list->destroyer = destroyer;
	} else {
		list->destroyer = g_free;
	}

	return list;
}

/**
 * marlin_list_free:
 * @list: A #MarlinList
 * 
 * Frees all memory associated with @list. Calls the list's destroyer function
 * for each element in the list.
 */
void
marlin_list_free (MarlinList *list)
{
	MarlinListNode *node;

	node = list->first;
	while (node) {
		MarlinListNode *old = node;

		node = node->next;
		if (list->destroyer) {
			list->destroyer (old);
		} else {
			g_free (old);
		}
	}

	g_free (list);
}

/**
 * marlin_list_clear:
 * @list: A #MarlinList
 *
 * Clears @list by calling the destroyer function for each list element, and
 * then resetting @list's first and last pointers
 */
void
marlin_list_clear (MarlinList *list)
{
	MarlinListNode *node;

	node = list->first;
	while (node) {
		MarlinListNode *old = node;

		node = node->next;
		list->destroyer (old);
	}

	list->first = NULL;
	list->last = NULL;
}

/**
 * marlin_list_append:
 * @list: A #MarlinList
 * @a: A #MarlinListNode
 *
 * Appends @a onto the end of @list
 */
void
marlin_list_append (MarlinList *list,
		    MarlinListNode *a)
{
	if (list->last) {
		a->previous = list->last;
		a->next = NULL;
		list->last->next = a;
		list->last = a;
	} else {
		list->first = a;
		list->last = a;
	}
}

void
marlin_list_remove (MarlinList *list,
		    MarlinListNode *a)
{
	MarlinListNode *prev, *next;

	g_return_if_fail (list != NULL);
	g_return_if_fail (a != NULL);

	prev = a->previous;
	next = a->next;

	if (list->first == a) {
		list->first = next;
	} 

	if (prev) {
		prev->next = next;
	}

	if (list->last == a) {
		list->last = prev;
	} 

	if (next) {
		next->previous = prev;
	}
}

void 
marlin_list_insert_after (MarlinList *list,
			  MarlinListNode *insert_point,
			  MarlinListNode *node)
{
	g_return_if_fail (list != NULL);
	g_return_if_fail (insert_point != NULL);
	g_return_if_fail (node != NULL);

	node->previous = insert_point;
	node->next = insert_point->next;
	insert_point->next = node;

	if (insert_point == list->last) {
		list->last = node;
	} else {
		node->next->previous = node;
	}
}

MarlinListNode *
marlin_list_node_next (MarlinListNode *a)
{
	return a->next;
}

MarlinListNode *
marlin_list_node_previous (MarlinListNode *a)
{
	return a->previous;
}
