/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-object.h>

#include <glib/gi18n.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>

#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-drawing.h>
#include <marlin/marlin-sample-view.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-block.h>
#undef LOCK_DEBUG
#include <marlin/marlin-read-write-lock.h>
#include <marlin/marlin-cursors.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-base-window.h>
#include <marlin/marlin-program.h>

enum {
	FRAMES_PER_PIXEL_CHANGED,
	MOVE_CURSOR,
	PAGE_START_CHANGED,
	VZOOM_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SAMPLE_DATA,
	PROP_FRAMES_PER_PIXEL,
	PROP_CURSOR_POSITION,
	PROP_PLAY_POSITION,
	PROP_CURSOR_COVERAGE,
	PROP_PLAY_COVERAGE,
	PROP_SHOW_PLAY_CURSOR,
	PROP_BASE_OFFSET,
	PROP_UNDO_MANAGER,
};

typedef struct _MarlinCursorInfo {
	guint64 position; /* Position of the cursor in frames */
	guint32 id; /* Timeout for the flashing */

	MarlinCoverage coverage; /* How much of the sample does the
				    cursor cover? */
	gboolean visible; /* Is the cursor visible */
	GdkGC *gc;
} MarlinCursorInfo;

struct _VMarker {
	MarlinMarker *marker;
	guint64 real_position; /* The location that the marker is really at
				  on the view. */
	char *real_name; /* The name that the marker really has on the view */
};

struct _MarlinSampleViewPrivate {
	MarlinSample *sample; /* The sample data */
	guint32 notify_id;

	MarlinUndoManager *undo;

	MarlinMarkerModel *model; /* Taken from the sample */
	guint32 add_id, remove_id, move_id; /* Signal handlers for the
					       add-marker, remove-marker and
					       move-marker signals */

	MarlinSampleSelection *selection; /* Taken from the sample
					     FIXME: Should this be
					     a property of the view
					     rather than the sample? */
	guint32 changed_id; /* Signal handler for selection-changed */

	guint64 number_of_frames; /* Taken from the sample */
	guint number_of_channels; /* Taken from the sample */

	guint frames_per_pixel; /* Number of frames represented in one pixel */

	int xofs; /* The x offset in pixels */
	GtkAdjustment *hadj; /* Adjustment for scrolling */

	MarlinCursorInfo *cursor; /* Structure to hold information about
				     the cursor */
	MarlinCursorInfo *play_cursor; /* Information on the play head */

	/* Different cursors we might need */
	GdkCursor *i_bar, *i_bar_left, *i_bar_right;
	GdkCursor *i_bar_add, *i_bar_minus;

	gboolean in_selection;
	gboolean expand; /* Are we expanding an already added selection? */
	gboolean made_selection; /* We made a selection when we clicked */

	guint32 scroll_timeout_id;

	/* List of struct _VMarkers */
	GList *markers;
	GHashTable *marker_to_view, *position_to_markers;

	PangoLayout *marker_layout; /* Layout to use for marker text */
	int digital_offset; /* FIXME: What? */
	GdkColor marker_colour; /* Marker colour */

	float vmax, vmin; /* The upper and lower volume limits of the view */

	gboolean moving_selection; /* Are we grabbing the selection? */
	guint64 grab_position; /* Position we grabbed the selection at
				  when we are moving it */

	gboolean using_hand; /* Is the hand being shown? */

	MarlinCoverage sel_coverage, orig_coverage;
	guint64 sel_start, sel_finish, orig_start, orig_finish;
	guint64 sel_initial;
	MarlinUndoContext *sel_ctxt; /* For when we start and finish drags */

	int base_offset;

	MarlinSampleDrawContext *dc;

	/* For dnd stuff */
	guint maybe_drag : 1;
	guint drag_in_progress : 1;

	int drag_x, drag_y;
	gulong grab_notify_handler_id;
	gulong toplevel_grab_broken_handler_id;
	gulong toplevel_motion_notify_handler_id;
	gulong toplevel_button_release_handler_id;

	gboolean show_drop_point;
	int drag_drop_point;
};

static guint signals[LAST_SIGNAL];

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_SAMPLE_VIEW_TYPE, MarlinSampleViewPrivate))
G_DEFINE_TYPE (MarlinSampleView, marlin_sample_view, GTK_TYPE_WIDGET);

#define DEFAULT_FRAMES_PER_PIXEL 4096
#define DEFAULT_CHANNELS 2

#define DEFAULT_VMAX (1.0)
#define DEFAULT_VMIN (-1.0)

#define MIN_ZOOM 1

#define DEBUG 1

#ifdef DEBUG
# define d(x) x
#else
# define d(x)
#endif

#define NOT_IN_APP_WINDOWS -2

static void move_cursor (MarlinSampleView *view,
			 guint64 new_position,
			 gboolean extend_selection);
static void scroll_to (MarlinSampleView *view,
		       int x);

static MarlinCursorInfo *
initialise_cursor (GtkWidget *widget,
		   gboolean visible)
{
	MarlinCursorInfo *cursor;

	cursor = g_new0 (MarlinCursorInfo, 1);
	cursor->position = 0;
	cursor->coverage = MARLIN_COVERAGE_BOTH;
	cursor->visible = visible;

	cursor->gc = gdk_gc_new (widget->window);
	gdk_gc_copy (cursor->gc, widget->style->black_gc);

	gdk_gc_set_function (cursor->gc, GDK_INVERT);

	return cursor;
}

static void
destroy_cursor (MarlinCursorInfo *cursor)
{
	if (cursor->id > 0) {
		g_source_remove (cursor->id);
	}

	g_object_unref (G_OBJECT (cursor->gc));

	g_free (cursor);
}

static void
clear_markers (MarlinSampleView *view)
{
	GList *p;

	for (p = view->priv->markers; p; p = p->next) {
		struct _VMarker *vm = p->data;

		g_free (vm->real_name);
		g_free (vm);
	}
	g_list_free (view->priv->markers);
	view->priv->markers = NULL;

	g_hash_table_destroy (view->priv->marker_to_view);
	view->priv->marker_to_view = NULL;
	g_hash_table_destroy (view->priv->position_to_markers);
	view->priv->position_to_markers = NULL;
}

static void
add_markers (MarlinSampleView *view,
	     GList *marks)
{
	GList *p;

	if (view->priv->marker_to_view == NULL) {
		view->priv->marker_to_view = g_hash_table_new (NULL, NULL);
	}

	if (view->priv->position_to_markers == NULL) {
		view->priv->position_to_markers = g_hash_table_new (NULL, NULL);
	}

	for (p = marks; p; p = p->next) {
		struct _VMarker *vm = g_new (struct _VMarker, 1);

		vm->marker = p->data;
		vm->real_position = vm->marker->position;
		vm->real_name = g_strdup (vm->marker->name);

		view->priv->markers = g_list_prepend (view->priv->markers, vm);
		g_hash_table_insert (view->priv->marker_to_view, p->data, vm);
	}
}

static void
finalize (GObject *object)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;

	view = MARLIN_SAMPLE_VIEW (object);

	priv = view->priv;
	gdk_cursor_unref (priv->i_bar);
	gdk_cursor_unref (priv->i_bar_left);
	gdk_cursor_unref (priv->i_bar_right);
	gdk_cursor_unref (priv->i_bar_add);
	gdk_cursor_unref (priv->i_bar_minus);

	destroy_cursor (priv->cursor);
	destroy_cursor (priv->play_cursor);

	if (priv->changed_id > 0) {
		g_signal_handler_disconnect (G_OBJECT (priv->selection),
					     priv->changed_id);
	}

	if (priv->move_id > 0) {
		g_signal_handler_disconnect (G_OBJECT (priv->model),
					     priv->move_id);
	}
	if (priv->add_id > 0) {
		g_signal_handler_disconnect (G_OBJECT (priv->model),
					     priv->add_id);
	}
	if (priv->remove_id > 0) {
		g_signal_handler_disconnect (G_OBJECT (priv->model),
					     priv->remove_id);
	}

	if (priv->sample != NULL) {
		g_signal_handler_disconnect (priv->sample,
					     priv->notify_id);
		g_object_unref (G_OBJECT (priv->sample));
	}

	if (priv->undo != NULL) {
		g_object_unref (G_OBJECT (priv->undo));
	}

	if (priv->model != NULL) {
		g_object_unref (G_OBJECT (priv->model));
	}

	clear_markers (view);

	g_object_unref (G_OBJECT (priv->marker_layout));

	marlin_sample_draw_context_free (priv->dc);

	G_OBJECT_CLASS (marlin_sample_view_parent_class)->finalize (object);
}

static void
set_scroll_values (MarlinSampleView *view,
		   int dw)
{
	GtkWidget *widget;
	MarlinSampleViewPrivate *priv;
	int xofs;

	widget = GTK_WIDGET (view);
	priv = view->priv;

	if (priv->hadj != NULL) {
		priv->hadj->page_size = widget->allocation.width;
		priv->hadj->page_increment = widget->allocation.width / 2;
		priv->hadj->step_increment = 20;

		priv->hadj->lower = 0;
		if (priv->number_of_frames == 0) {
			priv->hadj->upper = widget->allocation.width;
		} else {
			priv->hadj->upper = (guint) (priv->number_of_frames /
						     priv->frames_per_pixel);
		}

		xofs = MAX (priv->xofs - dw, 0);

		gtk_adjustment_changed (priv->hadj);

		if (priv->hadj->value != xofs) {
			gtk_adjustment_set_value (priv->hadj, (double) xofs);
			priv->xofs = xofs;

			g_signal_emit (G_OBJECT (view), signals[PAGE_START_CHANGED], 0, (guint64) (xofs * priv->frames_per_pixel));
		}
	}
}

static void
invalidate_widget (GtkWidget *widget)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		/* Redraw the whole window */
		rect.x = 0;
		rect.y = 0;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window,
					    &rect, FALSE);
	}
}

static void
sample_frames_changed (MarlinSampleView *view,
		       guint64 frames)
{
	view->priv->number_of_frames = frames;

	invalidate_widget (GTK_WIDGET (view));

	set_scroll_values (view, 0);
}

static void
sample_notify (MarlinSample *sample,
	       GParamSpec   *pspec,
	       MarlinSampleView *view)
{
	GtkWidget *widget;
	guint64 frames;

	widget = GTK_WIDGET (view);

	if (strcmp (pspec->name, "total-frames") == 0) {
		g_object_get (G_OBJECT (sample),
			      "total_frames", &frames,
			      NULL);
		sample_frames_changed (view, frames);
		return;
	}

	if (strcmp (pspec->name, "dirty") == 0) {
		/* Redraw... */
		invalidate_widget (widget);

		return;
	}

	if (strcmp (pspec->name, "channels") == 0) {
		g_object_get (G_OBJECT (sample),
			      "channels", &view->priv->number_of_channels,
			      NULL);

		invalidate_widget (widget);
		return;
	}
}

static void
sample_selection_changed (MarlinSampleSelection *selection,
			  MarlinSampleView *view)
{
	invalidate_widget (GTK_WIDGET (view));
}

static void
add_marker (MarlinMarkerModel *model,
	    MarlinMarker *marker,
	    MarlinSampleView *view)
{
	MarlinSampleViewPrivate *priv;
	struct _VMarker *vm;
	GtkWidget *widget = (GtkWidget *) view;

	priv = view->priv;

	vm = g_new (struct _VMarker, 1);
	vm->marker = marker;
	vm->real_position = marker->position;
	vm->real_name = g_strdup (marker->name);

	g_hash_table_insert (priv->marker_to_view, marker, vm);
	priv->markers = g_list_prepend (priv->markers, vm);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;
		PangoRectangle logical;
		int w, h;

		/* Invalidate for the dotted line */
		area.x = vm->real_position / priv->frames_per_pixel - priv->xofs;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

		/* Invalidate for the label */
		pango_layout_set_text (priv->marker_layout,
				       marker->name, -1);
		pango_layout_get_extents (priv->marker_layout,
					  &logical, NULL);
		pango_layout_get_size (priv->marker_layout, &w, &h);

		area.x += 5;
		area.y = 0;
		area.width = PANGO_PIXELS (w) + 5;
/* 		area.height = PANGO_PIXELS (logical.height + priv->digital_offset); */
		area.height = PANGO_PIXELS (h);

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

static void
remove_marker (MarlinMarkerModel *model,
	       MarlinMarker *marker,
	       MarlinSampleView *view)
{
	MarlinSampleViewPrivate *priv;
	GtkWidget *widget = (GtkWidget *) view;
	struct _VMarker *vm;
	guint64 pos;

	priv = view->priv;

	vm = g_hash_table_lookup (priv->marker_to_view, marker);
	g_assert (vm != NULL);

	g_hash_table_remove (priv->marker_to_view, marker);
	priv->markers = g_list_remove (priv->markers, vm);

	pos = vm->real_position;
	g_free (vm->real_name);
	g_free (vm);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;
		PangoRectangle logical;

		/* Invalidate for the dotted line */
		area.x = pos / priv->frames_per_pixel - priv->xofs;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

		/* Invalidate for the label */
		pango_layout_set_text (priv->marker_layout,
				       marker->name, -1);
		pango_layout_get_extents (priv->marker_layout,
					  &logical, NULL);

		area.x += 5;
		area.y = PANGO_PIXELS (logical.y);
		area.width = PANGO_PIXELS (logical.width);
		area.height = PANGO_PIXELS (logical.height + priv->digital_offset);

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

static void
change_marker (MarlinMarkerModel *model,
	       MarlinMarker *marker,
	       MarlinSampleView *view)
{
	GtkWidget *widget = GTK_WIDGET (view);
	struct _VMarker *vm;
	guint64 pos;
	char *name;

	vm = g_hash_table_lookup (view->priv->marker_to_view, marker);
	g_assert (vm != NULL);

	pos = vm->real_position;
	vm->real_position = marker->position;

	name = vm->real_name;
	vm->real_name = g_strdup (marker->name);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;
		PangoRectangle logical;

		/* First redraw the old position */

		/* Invalidate for the dotted line */
		area.x = (pos / view->priv->frames_per_pixel) - view->priv->xofs;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

		/* Invalidate for the label */
		pango_layout_set_text (view->priv->marker_layout, name, -1);
		pango_layout_get_extents (view->priv->marker_layout,
					  &logical, NULL);

		area.x += 5;
		area.y = PANGO_PIXELS (logical.y);
		area.width = PANGO_PIXELS (logical.width);
		area.height = PANGO_PIXELS (logical.height + view->priv->digital_offset);

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

		area.x = (marker->position / view->priv->frames_per_pixel) - view->priv->xofs;
		area.y = 0;
		area.height = widget->allocation.height;

		pango_layout_set_text (view->priv->marker_layout,
				       marker->name, -1);
		pango_layout_get_extents (view->priv->marker_layout,
					  &logical, NULL);

		area.width = PANGO_PIXELS (logical.width) + 5;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

#define SCROLL_THRESHOLD 50
static void
move_play_cursor (MarlinSampleView *view,
		  guint64 position)
{
	int pos, cdx;
	guint64 dx;

	dx = position - view->priv->play_cursor->position;
	view->priv->play_cursor->position = position;

	if (!view->priv->play_cursor->visible) {
		return;
	}

	cdx = dx / view->priv->frames_per_pixel;

	pos = position / view->priv->frames_per_pixel;
	if (pos - view->priv->xofs > GTK_WIDGET (view)->allocation.width - SCROLL_THRESHOLD) {
		scroll_to (view, view->priv->xofs + cdx);
	}
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	GtkWidget *widget;
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	guint old_frames_per_pixel;
	int width, best_fit, factor;
	float dofs;
	GList *marks;

	widget = GTK_WIDGET (object);
	view = MARLIN_SAMPLE_VIEW (object);
	priv = view->priv;

	switch (prop_id) {
	case PROP_SAMPLE_DATA:
		if (priv->changed_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->selection),
						     priv->changed_id);
		}

		if (priv->move_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->move_id);
		}
		if (priv->add_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->add_id);
		}
		if (priv->remove_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->remove_id);
		}

		if (priv->sample != NULL) {
			g_object_unref (G_OBJECT (priv->sample));
		}

		priv->sample = g_value_get_object (value);
		g_object_ref (G_OBJECT (priv->sample));

		/* Change it in the DrawContext too */
		priv->dc->sample = priv->sample;

		clear_markers (view);

		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &priv->number_of_frames,
			      "channels", &priv->number_of_channels,
			      "selection", &priv->selection,
			      "markers", &priv->model,
			      NULL);

		g_object_get (G_OBJECT (priv->model),
			      "markers", &marks,
			      NULL);

		add_markers (view, marks);

		priv->changed_id = g_signal_connect (G_OBJECT (priv->selection),
						     "changed",
						     G_CALLBACK (sample_selection_changed),
						     view);
		priv->add_id = g_signal_connect (G_OBJECT (priv->model),
						 "marker-added",
						 G_CALLBACK (add_marker), view);
		priv->remove_id = g_signal_connect (G_OBJECT (priv->model),
						    "marker-removed",
						    G_CALLBACK (remove_marker),
						    view);
		priv->move_id = g_signal_connect (G_OBJECT (priv->model),
						  "marker-changed",
						  G_CALLBACK (change_marker),
						  view);

		width = GTK_WIDGET (view)->allocation.width;
		if (priv->number_of_frames == 0) {
			best_fit = DEFAULT_FRAMES_PER_PIXEL;
		} else {
			best_fit = CLAMP (priv->number_of_frames / width, 1, 4096);

			for (factor = 1; factor <= 4096; factor *= 2) {
				if (best_fit <= factor) {
					best_fit = factor;
					break;
				}
			}
		}

		priv->frames_per_pixel = best_fit;
		priv->dc->fpp = best_fit;

		g_signal_emit (G_OBJECT (view),
			       signals[FRAMES_PER_PIXEL_CHANGED], 0,
			       priv->frames_per_pixel);

		sample_frames_changed (view, priv->number_of_frames);

		/* And we can listen for the frame length changing */
		priv->notify_id = g_signal_connect (G_OBJECT (priv->sample),
						    "notify",
						    G_CALLBACK (sample_notify),
						    view);

		break;

	case PROP_FRAMES_PER_PIXEL:
		old_frames_per_pixel = priv->frames_per_pixel;
		priv->frames_per_pixel = g_value_get_uint (value);

		if (old_frames_per_pixel == priv->frames_per_pixel) {
			break;
		}

		dofs = (float) (old_frames_per_pixel) / (float) (priv->frames_per_pixel);

		priv->xofs *= dofs;

		priv->dc->fpp = priv->frames_per_pixel;

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle rect;

			rect.x = 0;
			rect.y = 0;
			rect.width = widget->allocation.width;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window,
						    &rect, FALSE);
		}

		set_scroll_values (view, 0);

		g_signal_emit (object, signals[FRAMES_PER_PIXEL_CHANGED], 0,
			       priv->frames_per_pixel);
		break;

	case PROP_CURSOR_POSITION:
		move_cursor (view, g_value_get_uint64 (value), FALSE);
		priv->play_cursor->position = g_value_get_uint64 (value);
		break;

	case PROP_CURSOR_COVERAGE:
		priv->cursor->coverage = g_value_get_enum (value);
		if (GTK_WIDGET_DRAWABLE (widget) &&
		    priv->play_cursor->visible) {
			GdkRectangle rect;

			rect.x = priv->play_cursor->position / priv->frames_per_pixel;
			rect.y = 0;
			rect.width = 1;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &rect, FALSE);
		}
		break;

	case PROP_PLAY_POSITION:
		move_play_cursor (view, g_value_get_uint64 (value));

		invalidate_widget (widget);
		break;

	case PROP_PLAY_COVERAGE:
		priv->play_cursor->coverage = g_value_get_enum (value);
		invalidate_widget (widget);
		break;

	case PROP_SHOW_PLAY_CURSOR:
		priv->play_cursor->visible = g_value_get_boolean (value);
		invalidate_widget (widget);
		break;

	case PROP_BASE_OFFSET:
		priv->base_offset = g_value_get_int (value);
		priv->dc->base_offset = priv->base_offset;

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle rect;

			rect.x = 0;
			rect.y = 0;
			rect.width = widget->allocation.width;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &rect, FALSE);
		}

		break;

	case PROP_UNDO_MANAGER:
		priv->undo = (MarlinUndoManager *) g_value_dup_object (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinSampleView *view;

	view = MARLIN_SAMPLE_VIEW (object);

	switch (prop_id) {
	case PROP_SAMPLE_DATA:
		g_value_set_object (value, view->priv->sample);
		break;

	case PROP_FRAMES_PER_PIXEL:
		g_value_set_uint (value, view->priv->frames_per_pixel);
		break;

	case PROP_CURSOR_POSITION:
		g_value_set_uint64 (value, view->priv->cursor->position);
		break;

	case PROP_CURSOR_COVERAGE:
		g_value_set_enum (value, view->priv->cursor->coverage);
		break;

	default:
		break;
	}
}

static void
size_allocate (GtkWidget *widget,
	       GtkAllocation *allocation)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	int old_width, dw;
	GdkRectangle area;

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;

	old_width = widget->allocation.width;
	widget->allocation = *allocation;

	if (GTK_WIDGET_REALIZED (widget)) {
		gdk_window_move_resize (widget->window,
					allocation->x,
					allocation->y,
					allocation->width,
					allocation->height);

		area.x = 0;
		area.y = 0;
		area.width = allocation->width;
		area.height = allocation->height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}

	dw = (allocation->width > old_width ? allocation->width - old_width : 0);
	set_scroll_values (view, dw);

#if 0
	if (priv->vadj != NULL) {
		priv->vadj->page_size = allocation->height;
		priv->vadj->lower = 0;
		priv->vadj->upper = allocation->height;
	}
#endif
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *requisition)
{
}

#define ON_MULTIPLIER 0.66
#define OFF_MULTIPLIER 0.34

static int
get_time (GtkWidget *widget)
{
	GtkSettings *settings;
	int time;

	settings = gtk_widget_get_settings (widget);
	g_object_get (G_OBJECT (settings),
		      "gtk-cursor-blink-time", &time,
		      NULL);
        /* g_object_unref (G_OBJECT (settings)); */

	return time;
}

static void
real_redraw_cursor (MarlinSampleView *view)
{
	GtkWidget *widget = GTK_WIDGET (view);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		guint position;
		GdkRectangle rect;

		if ((view->priv->xofs * view->priv->frames_per_pixel) > view->priv->cursor->position) {
			return;
		}

		if (((view->priv->xofs + widget->allocation.width) * view->priv->frames_per_pixel) < view->priv->cursor->position) {
			return;
		}

		position = ((guint)(view->priv->cursor->position /
				    view->priv->frames_per_pixel)) -
			view->priv->xofs;

 		rect.x = (int) (view->priv->cursor->position / view->priv->frames_per_pixel) - view->priv->xofs;
		rect.width = 1;
		rect.y = 0;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}
}

static gboolean
redraw_cursor (gpointer data)
{
	MarlinSampleView *view;
	GtkWidget *widget;
	int time;

	widget = GTK_WIDGET (data);
	view = MARLIN_SAMPLE_VIEW (data);

	view->priv->cursor->visible = (view->priv->cursor->visible == TRUE ?
				       FALSE : TRUE);

	/* Remove the timeout, and then recreate it so that
	   the on blink can be longer than the off one */
	g_source_remove (view->priv->cursor->id);
	time = get_time (widget);

	if (view->priv->cursor->visible) {
		view->priv->cursor->id = g_timeout_add (time * ON_MULTIPLIER,
							redraw_cursor, view);
	} else {
		view->priv->cursor->id = g_timeout_add (time * OFF_MULTIPLIER,
							redraw_cursor, view);
	}

	real_redraw_cursor (view);

	return FALSE;
}

static void
move_cursor (MarlinSampleView *view,
	     guint64 new_position,
	     gboolean extend_selection)
{
	MarlinSampleViewPrivate *priv;
	guint64 old_position;
	int dx;

	priv = view->priv;

	old_position = priv->cursor->position;
	if (old_position == new_position) {
		return;
	}

	dx = (int) ((gint64)new_position - (gint64)old_position);
	if (dx == 0) {
		/* No change is necessary */
		return;
	}

	g_signal_emit (G_OBJECT (view), signals[MOVE_CURSOR], 0,
		       GTK_MOVEMENT_LOGICAL_POSITIONS,
		       (int) (dx / view->priv->frames_per_pixel), extend_selection);
}

static void
real_move_cursor (MarlinSampleView *view,
		  GtkMovementStep step,
		  int count,
		  gboolean extend_selection)
{
	GtkWidget *widget;
	MarlinSampleViewPrivate *priv;
	GdkRectangle rect;
	guint64 old_position, position;
	guint pixpos, xofs;
	int time;

	priv = view->priv;

	widget = GTK_WIDGET (view);

	old_position = position = priv->cursor->position;

	switch (step) {
	case GTK_MOVEMENT_LOGICAL_POSITIONS:
		position += (int) (count * priv->frames_per_pixel);
		break;

	case GTK_MOVEMENT_PAGES:
		position += (int) (count * (widget->allocation.width * priv->frames_per_pixel));
		break;

	case GTK_MOVEMENT_BUFFER_ENDS:
		if (count == -1) {
			position = 0;
		} else {
			position = priv->number_of_frames - priv->frames_per_pixel;
		}

		break;

	case GTK_MOVEMENT_DISPLAY_LINE_ENDS:
		if (count == -1) {
			position = priv->xofs * priv->frames_per_pixel;
		} else {
			position = (priv->xofs + widget->allocation.width - 1) * priv->frames_per_pixel;
		}

		break;

	default:
		break;
	}

	/* FIXME: This will break when position is legitamitly > gint64 */
	if ((gint64) position < 0) {
		priv->cursor->position = 0;
	} else if (position > priv->number_of_frames - priv->frames_per_pixel) {
		priv->cursor->position = priv->number_of_frames - priv->frames_per_pixel;
	} else {
		priv->cursor->position = position;
	}

	/* Extend the selection */
	if (extend_selection) {
		gboolean in_selection;
		/* Kinda a silly way to extend the selection.
		   Look for the subselection that falls on the frame
		   and extend that */
		in_selection = marlin_sample_selection_contains_frame (priv->selection, priv->cursor->coverage, old_position);
		if (in_selection == FALSE) {
			guint32 min, max;
			MarlinUndoContext *ctxt;

			min = MIN (old_position, position);
			max = MAX (old_position, position);
			
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Select Region"));
			marlin_sample_selection_set (priv->selection,
						     priv->cursor->coverage,
						     min, max,
						     ctxt);
			marlin_undo_manager_context_end (priv->undo, ctxt);
		} else {
			guint64 old_start, old_end, ms, me;
			guint64 new_start, new_end;
			MarlinCoverage old_coverage;
			GdkRectangle win_area, rect, inter;
			
			marlin_sample_selection_get (priv->selection,
						     &old_coverage, 
						     &old_start, &old_end);

			new_start = MIN (position, priv->sel_start);
			new_end = MAX (position, priv->sel_finish);

			ms = MIN (old_start, old_position);
			me = MAX (old_end, old_position);

			/* FIXME: Add context */
			marlin_sample_selection_set (priv->selection,
						     old_coverage,
						     new_start, new_end,
						     NULL);

			/* Calculate the redraw area */
			win_area.x = 0;
			win_area.y = 0;
			win_area.width = widget->allocation.width;
			win_area.height = widget->allocation.height;

			rect.x = (int) (ms / priv->frames_per_pixel) - priv->xofs;
			rect.width = (int) (me - ms) / priv->frames_per_pixel;
			rect.y = 0;
			rect.height = widget->allocation.height;

			if (gdk_rectangle_intersect (&win_area, &rect, &inter)) {
				gdk_window_invalidate_rect (widget->window,
							    &inter, FALSE);
			}
		}
	}
	
	pixpos = (guint) priv->cursor->position / priv->frames_per_pixel;
	priv->cursor->visible = TRUE;
	g_source_remove (priv->cursor->id);
	
	time = get_time (widget);
	priv->cursor->id = g_timeout_add (time * ON_MULTIPLIER,
					  redraw_cursor, view);

	rect.x = (int) (old_position / priv->frames_per_pixel) - priv->xofs;
	rect.y = 0;
	rect.width = 1;
	rect.height = widget->allocation.height;

	gdk_window_invalidate_rect (widget->window, &rect, FALSE);

	rect.x = (int) (priv->cursor->position / priv->frames_per_pixel) - priv->xofs;
	gdk_window_invalidate_rect (widget->window, &rect, FALSE);

	/* Forcably redraw the updates for the flash */
	gdk_window_process_updates (widget->window, FALSE);
	
	if (priv->hadj != NULL) {
		if (pixpos < priv->xofs) {
			xofs = pixpos;
			gtk_adjustment_set_value (priv->hadj, (double) xofs);
		} else if (pixpos > (priv->xofs + widget->allocation.width - 1)) {
			xofs = pixpos - (widget->allocation.width - 1);
			gtk_adjustment_set_value (priv->hadj, (double) xofs);
		}
	}
}       

static void
realize (GtkWidget *widget)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	GdkWindowAttr attr;
	int attr_mask, time;

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;
	
	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attr.window_type = GDK_WINDOW_CHILD;
	attr.x = widget->allocation.x;
	attr.y = widget->allocation.y;
	attr.width = widget->allocation.width;
	attr.height = widget->allocation.height;
	attr.wclass = GDK_INPUT_OUTPUT;
	attr.visual = gtk_widget_get_visual (widget);
	attr.colormap = gtk_widget_get_colormap (widget);
	attr.event_mask = (gtk_widget_get_events (widget)
			   | GDK_EXPOSURE_MASK
			   | GDK_BUTTON_PRESS_MASK
			   | GDK_BUTTON_RELEASE_MASK
			   | GDK_POINTER_MOTION_MASK
			   | GDK_KEY_PRESS_MASK
			   | GDK_KEY_RELEASE_MASK);

	attr_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_COLORMAP | GDK_WA_VISUAL;

	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
					 &attr, attr_mask);
	priv->dc->drawable = widget->window;

	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
	gdk_window_set_back_pixmap (widget->window, NULL, FALSE);

	/* Create the cursors we need */
	priv->i_bar = marlin_cursor_get (widget, I_BEAM);
	priv->i_bar_left = marlin_cursor_get (widget, I_BEAM_LEFT);
	priv->i_bar_right = marlin_cursor_get (widget, I_BEAM_RIGHT);
	priv->i_bar_add = marlin_cursor_get (widget, I_BEAM_ADD);
	priv->i_bar_minus = marlin_cursor_get (widget, I_BEAM_MINUS);

	priv->cursor = initialise_cursor (widget, TRUE);
	priv->play_cursor = initialise_cursor (widget, FALSE);

	/* Start the cursor flashing */
	time = get_time (widget);
 	priv->cursor->id = g_timeout_add (time * ON_MULTIPLIER, redraw_cursor, view);
}

static void
draw_selections (MarlinSampleView *view,
		 GdkRectangle     *area,
		 int               channel_num)
{
	MarlinSampleViewPrivate *priv;
	GtkWidget *widget;
	MarlinCoverage coverage, sel_coverage;
	guint64 start, finish;
	int chan_height, extra, chan_offset;

	widget = GTK_WIDGET (view);
	priv = view->priv;

	extra = priv->number_of_channels - 1;
	chan_height = (widget->allocation.height - extra) / priv->number_of_channels;
	chan_offset = (chan_height * channel_num) + channel_num;

	coverage = channel_num == 0 ? MARLIN_COVERAGE_LEFT : MARLIN_COVERAGE_RIGHT;
	marlin_sample_selection_get (priv->selection, &sel_coverage,
				     &start, &finish);

	if (sel_coverage == MARLIN_COVERAGE_BOTH ||
	    sel_coverage == coverage) {
		GdkRectangle rect, inter;

		rect.x = (int) (start / priv->frames_per_pixel) - priv->xofs;
		rect.width = (int) (finish - start) / priv->frames_per_pixel;
		rect.y = chan_offset;
		rect.height = chan_height;

		if (gdk_rectangle_intersect (area, &rect, &inter)) {
			cairo_t *ct;
			GdkColor base;

			ct = gdk_cairo_create (widget->window);

			if (GTK_WIDGET_HAS_FOCUS (widget)) {
				base = widget->style->base[GTK_STATE_SELECTED];
			} else {
				base = widget->style->base[GTK_STATE_ACTIVE];
			}
			
			cairo_rectangle (ct, inter.x, inter.y,
					 inter.width, inter.height);
			cairo_clip (ct);

			cairo_rectangle (ct, rect.x, rect.y,
					 rect.width, rect.height);
			cairo_set_source_rgba (ct, (double) base.red / 65535, 
					       (double) base.green / 65535, 
					       (double) base.blue / 65535,
					       0.5);
			cairo_fill_preserve (ct);

			cairo_destroy (ct);
		}
	}
}

#define YPOS(v) chan_area.y + chan_height - ((((v) - view->priv->vmin) * chan_height) / (view->priv->vmax - view->priv->vmin))

static void
draw_channel (MarlinSampleView *view,
	      GdkRectangle *area,
	      GtkStateType state_type,
	      int channel_num)
{
	GtkWidget *widget;
	MarlinSampleViewPrivate *priv;
	GdkRectangle chan_area;
	int quart, half;
	int chan_height, chan_offset, extra;
	int yv;
	
	widget = GTK_WIDGET (view);

	priv = view->priv;
	gdk_draw_rectangle (widget->window,
			    widget->style->base_gc[state_type],
			    TRUE, area->x, area->y,
			    area->width, area->height);

	extra = view->priv->number_of_channels - 1;
	chan_height = (widget->allocation.height - extra) / view->priv->number_of_channels;
	chan_offset = (chan_height * channel_num) + channel_num;
	
	quart = (int) (chan_height / 4);
	half = quart * 2;

	chan_area.x = 0;
	chan_area.y = (chan_height * channel_num) + channel_num;
	chan_area.width = widget->allocation.width;
	chan_area.height = chan_height;
	
	gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], &chan_area);
	gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], &chan_area);

	/* FIXME: These should only be drawn if the area intersects them */
	yv = YPOS (0.0) + view->priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->text_gc[state_type],
		       area->x, yv,
		       area->x + area->width, yv);
	yv = YPOS (0.5) + view->priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->dark_gc[state_type],
		       area->x, yv,
		       area->x + area->width, yv);
	yv = YPOS (-0.5) + view->priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->dark_gc[state_type],
		       area->x, yv,
		       area->x + area->width, yv);

	gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);
	gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], NULL);

	marlin_sample_draw (priv->dc, widget,
			    area, state_type,
			    channel_num,
			    priv->xofs);

	draw_selections (view, area, channel_num);
}

static void
draw_cursor (MarlinSampleView *view,
	     GdkRectangle *area,
	     GtkStateType state_type,
	     MarlinCursorInfo *cursor,
	     guint chan_height)
{
	GtkWidget *widget = (GtkWidget *) view;

	if (cursor->visible) {
		int position, start, end;
		
		gdk_gc_set_clip_rectangle (cursor->gc, area);
		switch (cursor->coverage) {
		case MARLIN_COVERAGE_BOTH:
			start = 0;
			end = widget->allocation.height;
			break;
			
		case MARLIN_COVERAGE_LEFT:
			start = 0;
			end = chan_height;
			break;

		case MARLIN_COVERAGE_RIGHT:
			start = chan_height + 1;
			end = widget->allocation.height;
			break;

		default:
			return;
		}
		
		position = (int) (cursor->position / view->priv->frames_per_pixel) - view->priv->xofs;
		gdk_draw_line (widget->window, cursor->gc,
			       position, start,
			       position, end);
	}
}

static void
draw_sample_area (MarlinSampleView *view,
		  GdkRectangle *area,
		  GtkStateType state_type)
{
	GtkWidget *widget = GTK_WIDGET (view);
	int i, chan_height, num_channels, extra;

	num_channels = view->priv->number_of_channels;
	extra = num_channels - 1;
	chan_height = (widget->allocation.height - extra) / num_channels;

	for (i = 0; i < num_channels; i++) {
		GdkRectangle chan_area, inter;
		
		chan_area.x = 0;
		chan_area.y = (chan_height * i) + i;
		chan_area.width = widget->allocation.width;
		chan_area.height = chan_height;

		if (gdk_rectangle_intersect (area, &chan_area, &inter)) {
			draw_channel (view, &inter, state_type, i);
		}

		if (i > 0) {
			gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], area);
			gdk_draw_line (widget->window,
				       widget->style->text_gc[state_type],
				       area->x, (chan_height * i),
				       area->x + area->width,
				       chan_height * i);
			gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);
		}
	}

	draw_cursor (view, area, state_type, view->priv->cursor, chan_height);
	draw_cursor (view, area, state_type, view->priv->play_cursor, chan_height);
}
	
/* This is a global object, do not free it
   FIXME: Should this be a utility function? */
static GdkPixmap *
stipple_pixmap (void)
{
	static GdkPixmap *stipple = NULL;

	if (stipple == NULL) {
		char stipple_bits[] = { 0x02, 0x01 };
		
		stipple = gdk_bitmap_create_from_data (NULL, stipple_bits, 2, 2);
	}

	return stipple;
}

static void
draw_drop_point (MarlinSampleView *view,
		 GdkRectangle *area,
		 GtkStateType state_type)
{
	MarlinSampleViewPrivate *priv = view->priv;
	GtkWidget *widget = GTK_WIDGET (view);
	static GdkGC *drop_gc;

	if (drop_gc == NULL) {
		GdkColor green = {0, 0, 65535, 0};
		GdkColormap *cmap = gdk_colormap_get_system ();
		
		drop_gc = gdk_gc_new (widget->window);
		
		gdk_gc_copy (drop_gc, widget->style->black_gc);
		
		gdk_colormap_alloc_color (cmap, &green, FALSE, TRUE);
		gdk_gc_set_foreground (drop_gc, &green);
#if 0
		gdk_gc_set_line_attributes (drop_gc, 1,
					    GDK_LINE_ON_OFF_DASH,
					    GDK_CAP_BUTT,
					    GDK_JOIN_MITER);
#endif
	}

	gdk_gc_set_clip_rectangle (drop_gc, area);
	gdk_draw_line (widget->window, drop_gc,
		       priv->drag_drop_point, 0,
		       priv->drag_drop_point, widget->allocation.height - 1);
	gdk_gc_set_clip_rectangle (drop_gc, NULL);
}

static void
draw_dead_area (MarlinSampleView *view,
		GdkRectangle *area,
		GtkStateType state_type)
{
	MarlinSampleViewPrivate *priv;
	GtkWidget *widget;
	GdkGC *gc;
	int i, num_channels, quart, chan_height, extra;
	
	priv = view->priv;
	widget = GTK_WIDGET (view);

 	gc = gdk_gc_new (widget->window);
	gdk_gc_copy (gc, widget->style->bg_gc[state_type]);
	
	gdk_gc_set_stipple (gc, stipple_pixmap ());
	gdk_gc_set_fill (gc, GDK_STIPPLED);
	gdk_gc_set_clip_rectangle (gc, area);
	
	gdk_draw_rectangle (widget->window,
			    gc,
			    TRUE, area->x, area->y,
			    area->width, area->height);
	g_object_unref (G_OBJECT (gc));

	num_channels = priv->number_of_channels;
	extra = num_channels - 1;
	chan_height = (widget->allocation.height - extra) / num_channels;
	
	quart = (int) (chan_height / 4);

	for (i = 0; i < num_channels; i++) {
		GdkRectangle chan_area, inter;
		int yv;
		
		chan_area.x = 0;
		chan_area.y = (chan_height * i) + i;
		chan_area.width = widget->allocation.width;
		chan_area.height = chan_height;

		gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], area);
		gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], area);
		
		if (i > 0) {
			gdk_draw_line (widget->window,
				       widget->style->text_gc[state_type],
				       area->x, (chan_height * i),
				       area->x + area->width,
				       chan_height * i);
		}

		if (!gdk_rectangle_intersect (area, &chan_area, &inter)){
			continue;
		}

		gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], 
					   &chan_area);
		gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], 
					   &chan_area);

		/* FIXME: These should only be drawn if they intersect
		   with the area */
		yv = YPOS (0.0) + priv->base_offset;
		gdk_draw_line (widget->window,
			       widget->style->text_gc[state_type],
			       inter.x, yv,
			       inter.x + inter.width, yv);

		yv = YPOS (0.5) + priv->base_offset;
		gdk_draw_line (widget->window,
			       widget->style->dark_gc[state_type],
			       inter.x, yv,
			       inter.x + inter.width, yv);

		yv = YPOS (-0.5) + priv->base_offset;
		gdk_draw_line (widget->window,
			       widget->style->dark_gc[state_type],
			       inter.x, yv,
			       inter.x + inter.width, yv);

		gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);
		gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], NULL);
	}
}

static void
draw_markers (MarlinSampleView *view,
	      GdkRectangle *area,
	      GtkStateType state_type)
{
	MarlinSampleViewPrivate *priv = view->priv;
	GList *m;
	static GdkGC *marker_gc = NULL;
	GtkWidget *widget = GTK_WIDGET (view);
	
	for (m = priv->markers; m; m = m->next) {
		struct _VMarker *vm = m->data;
		PangoRectangle logical_rect;
		GdkRectangle text_rect, inter;
		int position = (vm->real_position / priv->frames_per_pixel) - priv->xofs;

		/* FIXME: This needs optimised bit better */
#if 0
		if (position < area->x ||
		    position > area->x + area->width) {
			continue;
		}
#endif
		if (marker_gc == NULL) {
			marker_gc = gdk_gc_new (widget->window);
			
			gdk_gc_copy (marker_gc, widget->style->black_gc);
			
			gdk_gc_set_foreground (marker_gc, 
					       &priv->marker_colour);
			gdk_gc_set_line_attributes (marker_gc, 1,
						    GDK_LINE_ON_OFF_DASH,
						    GDK_CAP_BUTT,
						    GDK_JOIN_MITER);
		}

		gdk_gc_set_clip_rectangle (marker_gc, area);
		gdk_draw_line (widget->window, marker_gc,
			       position, 0,
			       position, widget->allocation.height - 1);
		gdk_gc_set_clip_rectangle (marker_gc, NULL);

		pango_layout_set_text (priv->marker_layout,
				       vm->marker->name, -1);
		pango_layout_get_extents (priv->marker_layout,
					  &logical_rect, NULL);

		text_rect.x = position + 5;
		text_rect.y = PANGO_PIXELS (logical_rect.y);
		text_rect.width = PANGO_PIXELS (logical_rect.width) + 5;
		text_rect.height = PANGO_PIXELS (logical_rect.height + priv->digital_offset);

		if (gdk_rectangle_intersect (area, &text_rect, &inter)) {
  			gdk_gc_set_clip_rectangle (marker_gc, &inter);
			gdk_draw_layout (widget->window, marker_gc,
					 position + 5,
					 PANGO_PIXELS (logical_rect.y - priv->digital_offset),
					 priv->marker_layout);
  			gdk_gc_set_clip_rectangle (marker_gc, NULL);
		}
	}
}

static void
_marlin_sample_view_paint (MarlinSampleView *view,
			   GdkRectangle *area,
			   GtkStateType state_type)
{
	GtkWidget *widget;
	MarlinSampleViewPrivate *priv;
	GdkRectangle rect, inter;
	int pixel_width;

	priv = view->priv;
	widget = GTK_WIDGET (view);

	pixel_width = (int) (priv->number_of_frames / priv->frames_per_pixel);

	/* Area of samples */
	rect.x = 0;
	rect.y = 0;
	rect.width = pixel_width - priv->xofs;
	rect.height = widget->allocation.height;

	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		draw_sample_area (view, &inter, state_type);
	}

	/* Draw the dead area at the end */
	rect.x = (pixel_width - priv->xofs);
	rect.y = 0;
	rect.width = widget->allocation.width - rect.x;
	rect.height = widget->allocation.height;

	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		draw_dead_area (view, &inter, state_type);
	}

	/* Draw markers */
	draw_markers (view, area, state_type);

	/* Draw the drag drop point marker */
	if (priv->show_drop_point) {
		int x_root, y_root;

		/* Only draw the drop point if the pointer is actually in
		   the window */
		if (gdk_window_at_pointer (&x_root, &y_root) == widget->window) {
			draw_drop_point (view, area, state_type);
		} else {
			priv->show_drop_point = FALSE;
		}
	}
}

static int
expose_event (GtkWidget      *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		MarlinSampleView *view = MARLIN_SAMPLE_VIEW (widget);
		GdkRectangle *rects;
		int i, n_rects;

		gdk_region_get_rectangles (event->region, &rects, &n_rects);
		for (i = 0; i < n_rects; i++) {
			_marlin_sample_view_paint (view, &rects[i],
						   GTK_WIDGET_STATE (widget));
		}
		g_free (rects);
	}

	return FALSE;
}

#define THRESHOLD 4

static gboolean
can_expand_selection (MarlinSampleView *view,
		      guint64 position)
{
	guint64 start, finish;
	int ds, de;

	marlin_sample_selection_get (view->priv->selection, NULL,
				     &start, &finish);

	ds = (position - start) / view->priv->frames_per_pixel;
	de = (position -  finish) / view->priv->frames_per_pixel;

	if (abs (ds) <= THRESHOLD ||
	    abs (de) <= THRESHOLD) {
		return TRUE;
	}

	return FALSE;
}

static void
move_selection (MarlinSampleView *view,
		gint64 dp)
{
	GtkWidget *widget = GTK_WIDGET (view);
	MarlinSampleViewPrivate *priv;
	GdkRectangle window_area, sub_area, inter;
	guint64 old_start, old_end, length, min_start, max_end;

	priv = view->priv;

	length = priv->sel_finish - priv->sel_start;
	old_start = priv->sel_start;;
	old_end = priv->sel_finish;

	/* If we don't go under 0 we can move it */
	if ((gint64)(priv->sel_finish + dp) - (gint64) length > 0) {
		priv->sel_finish = MIN (priv->sel_finish + dp, priv->number_of_frames);
		priv->sel_start = MAX ((gint64)priv->sel_finish - (gint64)length, 0);
	} else {
		return;
	}

	marlin_sample_selection_set (priv->selection, priv->sel_coverage,
				     priv->sel_start, priv->sel_finish,
				     NULL);

	window_area.x = 0;
	window_area.width = widget->allocation.width;
	window_area.y = 0;
	window_area.height = widget->allocation.height;

	/* Redraw the area from min_start -> max_end */
	min_start = MIN (priv->sel_start, old_start);
	max_end = MAX (priv->sel_finish, old_end);

	sub_area.x = (int) (min_start / priv->frames_per_pixel) - priv->xofs;
	sub_area.width = (int) (max_end - min_start) / priv->frames_per_pixel;
	sub_area.y = 0;
	sub_area.height = widget->allocation.height;

	if (gdk_rectangle_intersect (&window_area, &sub_area, &inter)) {
		gdk_window_invalidate_rect (widget->window, &inter, FALSE);
	}
}

/* FIXME: There is a bug in this function
   that means the selection can jump around
   when the mouse is moved very quickly back
   and forth */
static void
set_selection (MarlinSampleView *view,
	       guint64 position,
	       MarlinCoverage coverage)
{
	GtkWidget *widget = GTK_WIDGET (view);
	MarlinSampleViewPrivate *priv;
	GdkRectangle window_area, sub_area, inter;
	guint64 old_start, old_end, min_start, max_end;
	guint64 start, finish;

	priv = view->priv;

	position = MIN (position, priv->number_of_frames - 1);
	/* FIXME: When we probably shouldn't move the cursor... */
	/* Move the cursor first */
#if 0
	priv->cursor->coverage = coverage;
	move_cursor (view, position, FALSE);
#endif

	old_start = priv->sel_start;
	old_end = priv->sel_finish;

	priv->sel_coverage = coverage;

	if (position < priv->sel_start ||
	    position > priv->sel_finish) {
		start = MIN (position, old_start);
		finish = MAX (position, old_end);
	} else {
		if (priv->in_selection == FALSE) {
			guint64 ds, de;

			ds = position - priv->sel_start;
			de = priv->sel_finish - position;

			if (ds < de) {
				priv->sel_start = start = position;
				finish = priv->sel_finish;
			} else {
				start = priv->sel_start;
				priv->sel_finish = finish = position;
			}
		} else {
			if (position > priv->sel_start) {
				start = priv->sel_start;
				finish = position;
			} else {
				start = position;
				finish = priv->sel_finish;
			}
		}
	}

	marlin_sample_selection_set (priv->selection, priv->sel_coverage,
				     start, finish,
				     NULL);

	window_area.x = 0;
	window_area.width = widget->allocation.width;
	window_area.y = 0;
	window_area.height = widget->allocation.height;

	/* Redraw the area from min_start -> max_end */
	min_start = MIN (start, old_start);
	max_end = MAX (finish, old_end);

	sub_area.x = (int) (min_start / priv->frames_per_pixel) - priv->xofs;
	sub_area.width = (int) (max_end - min_start) / priv->frames_per_pixel;
	sub_area.y = 0;
	sub_area.height = widget->allocation.height;

	if (gdk_rectangle_intersect (&window_area, &sub_area, &inter)) {
		gdk_window_invalidate_rect (widget->window, &inter, FALSE);
	}
}

#define SCROLL_TIMEOUT_INTERVAL 10
#define SCROLL_THRESHOLD 50

static gboolean
scroll_timeout_cb (gpointer data)
{
	int x, y, x_scroll;
	GtkWidget *widget = GTK_WIDGET (data);
	MarlinSampleView *view = MARLIN_SAMPLE_VIEW (data);
	MarlinSampleViewPrivate *priv;
	int max, doc_width, channel_height;
	float value;
	MarlinCoverage coverage;
	gint64 position;

	priv = view->priv;

	gdk_window_get_pointer (widget->window, &x, &y, NULL);

	if (x < SCROLL_THRESHOLD) {
		x_scroll = MIN (x, 0);
		x = SCROLL_THRESHOLD;
	} else if (x >= widget->allocation.width - SCROLL_THRESHOLD) {
		x_scroll = x - (widget->allocation.width - SCROLL_THRESHOLD) + 1;
		x = widget->allocation.width - SCROLL_THRESHOLD;
	} else {
		x_scroll = 0;
	}
	x_scroll /= 2;

	value = priv->hadj->value + x_scroll;
	doc_width = priv->number_of_frames / priv->frames_per_pixel;

	if (doc_width > widget->allocation.width) {
		max = doc_width - widget->allocation.width;
	} else {
		max = 0;
	}

	value = CLAMP (value, 0.0, (float) max);
	gtk_adjustment_set_value (priv->hadj, value);
	priv->xofs = value;

	channel_height = widget->allocation.height / priv->number_of_channels;
	if (priv->number_of_channels == 1) {
		coverage = MARLIN_COVERAGE_BOTH;
	} else {
		if (y < (channel_height / 2)) {
			coverage = MARLIN_COVERAGE_LEFT;
		} else if (y > (channel_height + (channel_height / 2))) {
			coverage = MARLIN_COVERAGE_RIGHT;
		} else {
			coverage = MARLIN_COVERAGE_BOTH;
		}
	}

	position = (value + x) * priv->frames_per_pixel;

	if (priv->expand) {
		set_selection (view, (guint64) MAX (0, position), coverage);
	} else if (priv->moving_selection) {
		move_selection (view, position - priv->grab_position);
		priv->grab_position = position;
	}

	return TRUE;
}

static void
setup_scroll_timeout (MarlinSampleView *view)
{
	if (view->priv->scroll_timeout_id != 0) {
		return;
	}

	view->priv->scroll_timeout_id = g_timeout_add (SCROLL_TIMEOUT_INTERVAL,
						       scroll_timeout_cb, view);

	scroll_timeout_cb (view);
}

static void
remove_scroll_timeout (MarlinSampleView *view)
{
	if (view->priv->scroll_timeout_id == 0) {
		return;
	}

	g_source_remove (view->priv->scroll_timeout_id);
	view->priv->scroll_timeout_id = 0;
}

static void
get_closest_markers (MarlinSampleView *view,
		     guint64 position,
		     guint64 *start_mark,
		     guint64 *end_mark)
{
	GList *markers;

	*start_mark = 0;
	*end_mark = view->priv->number_of_frames - 1;

	for (markers = view->priv->markers; markers; markers = markers->next) {
		struct _VMarker *vm = markers->data;

		if (vm->real_position > position) {
			*end_mark = MIN (vm->real_position, *end_mark);
		} else if (vm->real_position < position) {
			*start_mark = MAX (vm->real_position, *start_mark);
		} else {
			/* FIXME: If we click on a mark, ignore it? */
		}
	}
}

static void
set_drop_point (MarlinSampleView *view,
		guint position,
		gboolean show)
{
	GtkWidget *widget = GTK_WIDGET (view);
	GdkRectangle rect;

	view->priv->show_drop_point = show;
	if (GTK_WIDGET_DRAWABLE (widget)) {
		rect.x = view->priv->drag_drop_point;
		rect.y = 0;
		rect.width = 1;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);

		if (show) {
			view->priv->drag_drop_point = position;
			rect.x = position;
			
			gdk_window_invalidate_rect (widget->window, &rect, FALSE);
		}
	} else {
		view->priv->drag_drop_point = position;
	}
}

static void
drop_selection (MarlinSampleView *view,
		MarlinSample *sample,
		guint64 position)
{
	if (position >= view->priv->number_of_frames) {
		position = view->priv->number_of_frames - 1;
	}

	marlin_sample_insert (view->priv->sample, sample, position,
			      MARLIN_COVERAGE_BOTH, NULL, NULL);

	/* Hide the drop point in the destination view 
	   drag_stop only turns it off in the src view 
	   FIXME: What about other views that may have been passed
	   and don't loose the drop mark */
	set_drop_point (view, 0, FALSE);
}

static MarlinSampleView *
find_view_at_pointer (int abs_x,
		      int abs_y)
{
	GdkWindow *win_at_pointer, *toplevel_win;
	gpointer toplevel = NULL;
	int x, y;

	/* FIXME: Multihead */
	win_at_pointer = gdk_window_at_pointer (&x, &y);
	if (win_at_pointer == NULL) {
		/* Outside all windows containing a view */
		return NULL;
	}

	toplevel_win = gdk_window_get_toplevel (win_at_pointer);
	
	gdk_window_get_user_data (toplevel_win, &toplevel);

	/* toplevel should be a MarlinBaseWindow */
	if (toplevel != NULL && MARLIN_IS_BASE_WINDOW (toplevel)) {
		return marlin_base_window_get_sample_view (MARLIN_BASE_WINDOW (toplevel));
	}

	return NULL;
}

static int
find_view_at_pos (int abs_x,
		  int abs_y,
		  MarlinSampleView **view)
{
	*view = find_view_at_pointer (abs_x, abs_y);
	if (*view == NULL) {
		return NOT_IN_APP_WINDOWS;
	}

	return 0;
}

static void
drag_stop (MarlinSampleView *view,
	   guint32 time)
{
	MarlinSampleViewPrivate *priv = view->priv;
	GtkWidget *widget = GTK_WIDGET (view);
	GtkWidget *toplevel, *child;

	if (priv->drag_in_progress) {
		toplevel = gtk_widget_get_toplevel (widget);
		g_return_if_fail (GTK_WIDGET_TOPLEVEL (toplevel));

		child = gtk_bin_get_child (GTK_BIN (toplevel));
		g_return_if_fail (child != NULL);

		if (priv->toplevel_grab_broken_handler_id != 0) {
			g_signal_handler_disconnect (toplevel,
						     priv->toplevel_grab_broken_handler_id);
			priv->toplevel_grab_broken_handler_id = 0;
		}
		if (priv->grab_notify_handler_id != 0) {
			g_signal_handler_disconnect (view,
						     priv->grab_notify_handler_id);
			priv->grab_notify_handler_id = 0;
		}
		if (priv->toplevel_motion_notify_handler_id != 0) {
			g_signal_handler_disconnect (toplevel,
						     priv->toplevel_motion_notify_handler_id);
			priv->toplevel_motion_notify_handler_id = 0;
		}
		if (priv->toplevel_button_release_handler_id != 0) {
			g_signal_handler_disconnect (toplevel,
						     priv->toplevel_button_release_handler_id);
			priv->toplevel_button_release_handler_id = 0;
		}

		gdk_pointer_ungrab (time);
		gtk_grab_remove (toplevel);
	}

	priv->drag_in_progress = FALSE;
	priv->maybe_drag = FALSE;
	priv->drag_x = 0;
	priv->drag_y = 0;

	set_drop_point (view, 0, FALSE);
}

static gboolean
grab_broken_event_cb (GtkWidget *widget,
		      GdkEventGrabBroken *event,
		      MarlinSampleView *view)
{
	drag_stop (view, GDK_CURRENT_TIME);

	return FALSE;
}

static void
grab_notify_cb (GtkWidget *widget,
		gboolean was_grabbed,
		MarlinSampleView *view)
{
	drag_stop (view, GDK_CURRENT_TIME);
}

static gboolean
toplevel_motion_notify_cb (GtkWidget *toplevel,
			   GdkEventMotion *event,
			   MarlinSampleView *view)
{
	MarlinSampleView *dest = NULL;
	int result;

	result = find_view_at_pos ((int) event->x_root, (int) event->y_root,
				   &dest);

	if (result != NOT_IN_APP_WINDOWS) {
		guint position, dx;
		int x, y;
		GtkWidget *widget = GTK_WIDGET (dest);

		gdk_window_get_origin (widget->window, &x, &y);
		dx = event->x_root - x;
		
		position = dest->priv->xofs + dx;

		set_drop_point (dest, position, TRUE);
	}

	return FALSE;
}

static gboolean
toplevel_button_release_cb (GtkWidget *toplevel,
			    GdkEventButton *event,
			    MarlinSampleView *view)
{
	MarlinSampleViewPrivate *priv = view->priv;

	if (priv->drag_in_progress) {
		MarlinSampleView *dest;
		int result;

		result = find_view_at_pos ((int) event->x_root,
					   (int) event->y_root,
					   &dest);

		if (result == NOT_IN_APP_WINDOWS) {
			MarlinSample *sample;
			MarlinProgram *program;

			/* Make a new window for the selection */
			sample = marlin_sample_new_from_selection (priv->sample,
								   NULL);
			g_object_set (G_OBJECT (sample),
				      "dirty", TRUE,
				      NULL);

			program = marlin_program_get_default ();

			marlin_program_request_new_view (program, sample);
		} else {
			MarlinSample *sample;
			GtkWidget *widget = GTK_WIDGET (dest);
			guint position;
			int x, y, dx;

			sample = marlin_sample_new_from_selection (priv->sample,
								   NULL);

			gdk_window_get_origin (widget->window, &x, &y);
			dx = event->x_root - x;
			
			position = dest->priv->xofs + dx;
			drop_selection (dest, sample, 
					position * dest->priv->frames_per_pixel);
		}
	}

	drag_stop (view, event->time);

	return FALSE;
}

#define MAX_CURSOR_WIDTH 300
static GdkCursor *
make_drag_cursor (MarlinSampleView *view)
{
	MarlinSampleViewPrivate *priv;
	MarlinSampleDrawContext *dc;
	GtkWidget *widget = GTK_WIDGET (view);
	GdkRectangle rect;
	guint64 start, finish, total;
	MarlinCoverage coverage;
	int width, fpp, i;
	GdkPixbuf *cursor_pixbuf, *tmp;
	GdkCursor *cursor;
	int xofs;
	guint max_h, max_w;

	priv = view->priv;

	gdk_display_get_maximal_cursor_size (gtk_widget_get_display (widget),
					     &max_w, &max_h);
	marlin_sample_selection_get (view->priv->selection,
				     &coverage, &start, &finish);
	if (coverage == MARLIN_COVERAGE_BOTH &&
	    priv->number_of_channels == 1) {
		coverage = MARLIN_COVERAGE_LEFT;
	}
	    
	total = finish - start;
	max_w = MAX_CURSOR_WIDTH;
 	width = MIN (total / view->priv->frames_per_pixel, max_w);
	if (width == max_w) {
		fpp = (int) (total / width);
		if (fpp == 0) {
			fpp = 1;
		}
	} else {
		fpp = view->priv->frames_per_pixel;
	}

	/* Draw the sample to an offscreen pixmap */
	dc = marlin_sample_draw_context_new (view->priv->sample, fpp, FALSE);
	dc->drawable = gdk_pixmap_new (widget->window, width, max_h, -1);
	gdk_draw_rectangle (dc->drawable, 
			    widget->style->base_gc[GTK_STATE_NORMAL], TRUE,
			    0, 0, width, max_h);
	dc->height = max_h;

	xofs = start / dc->fpp;

	switch (coverage) {
	case MARLIN_COVERAGE_LEFT:
 		rect.x = 0;
		rect.x = 0;
		rect.y = 0;
		rect.width = total / dc->fpp;
		rect.height = max_h;

		marlin_sample_draw (dc, widget, &rect, 
				    GTK_STATE_NORMAL, 
				    MARLIN_CHANNEL_LEFT, xofs);
		break;

	case MARLIN_COVERAGE_RIGHT:
		rect.x = 0;
		rect.y = 0;
		rect.width = total / dc->fpp;
		rect.height = max_h;

		marlin_sample_draw (dc, widget, &rect,
				    GTK_STATE_NORMAL,
				    MARLIN_CHANNEL_RIGHT, xofs);
		break;

	case MARLIN_COVERAGE_BOTH:
		for (i = MARLIN_CHANNEL_LEFT; i <= MARLIN_CHANNEL_RIGHT; i++) {
			int chan_height;

			chan_height = max_h / 2;

			rect.x = 0;
			rect.y = (chan_height * i) + i;
			rect.width = total / dc->fpp;
			rect.height = chan_height;

			marlin_sample_draw (dc, widget, &rect,
					    GTK_STATE_NORMAL,
					    i, xofs);
		}
		break;

	case MARLIN_COVERAGE_NONE:
	default:
		break;
	}

	/* Create a pixbuf from the drawable and a cursor from the pixbuf */
	tmp = gdk_pixbuf_get_from_drawable (NULL, dc->drawable,
					    NULL, 0, 0,
					    0, 0, width, max_h);
	/* Pixbufs need to have an alpha channel to be turned into cursors */
	cursor_pixbuf = gdk_pixbuf_add_alpha (tmp, FALSE, 0, 0, 0);
	cursor = gdk_cursor_new_from_pixbuf (gtk_widget_get_display (widget),
					     cursor_pixbuf, 0, 0);
	g_object_unref (cursor_pixbuf);
	g_object_unref (dc->drawable);
	marlin_sample_draw_context_free (dc);

	return cursor;
}

static gboolean
drag_start (MarlinSampleView *view,
	    guint32 time)
{
	MarlinSampleViewPrivate *priv = view->priv;
	GtkWidget *widget = GTK_WIDGET (view);
	GtkWidget *toplevel, *child;
	GdkCursor *cursor;

	if (priv->drag_in_progress || 
	    gdk_pointer_is_grabbed ()) {
		return FALSE;
	}

	priv->maybe_drag = FALSE;
	priv->drag_in_progress = TRUE;
/*  	cursor = marlin_cursor_get (widget, HAND_CLOSED); */
	cursor = make_drag_cursor (view);

	toplevel = gtk_widget_get_toplevel (widget);
	g_return_val_if_fail (GTK_WIDGET_TOPLEVEL (toplevel), FALSE);

	child = gtk_bin_get_child (GTK_BIN (toplevel));
	g_return_val_if_fail (child != NULL, FALSE);

	/* grab the pointer */
	gtk_grab_add (toplevel);

	if (gdk_pointer_grab (toplevel->window,
			      FALSE,
			      GDK_BUTTON1_MOTION_MASK |
			      GDK_BUTTON_RELEASE_MASK,
			      NULL, cursor, time) != GDK_GRAB_SUCCESS) {
		gdk_cursor_unref (cursor);
		drag_stop (view, time);
		return FALSE;
	}
	gdk_cursor_unref (cursor);

	priv->toplevel_grab_broken_handler_id =
		g_signal_connect (toplevel, "grab-broken-event",
				  G_CALLBACK (grab_broken_event_cb), view);
	priv->toplevel_motion_notify_handler_id =
		g_signal_connect (toplevel, "motion-notify-event",
				  G_CALLBACK (toplevel_motion_notify_cb), view);
	priv->toplevel_button_release_handler_id =
		g_signal_connect (toplevel, "button-release-event",
				  G_CALLBACK (toplevel_button_release_cb), view);
	priv->grab_notify_handler_id =
		g_signal_connect (view, "grab-notify",
				  G_CALLBACK (grab_notify_cb), view);

	return TRUE;
}

/* FIXME: I hate this function. Its a mess */
static int
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	MarlinSampleSelection *selection;
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	MarlinUndoContext *ctxt;
	int y, channel_height;
	guint64 position;
	guint64 start_mark, end_mark;

	/* Grab focus back */
	gtk_widget_grab_focus (widget);

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;

	if (priv->sample == NULL) {
		return TRUE;
	}

	channel_height = widget->allocation.height / priv->number_of_channels;

	y = event->y;
	if (priv->number_of_channels == 1) {
		priv->cursor->coverage = MARLIN_COVERAGE_BOTH;
	} else {
		if (y < (channel_height / 2)) {
			priv->cursor->coverage = MARLIN_COVERAGE_LEFT;
		} else if (y > (channel_height + (channel_height / 2))) {
			priv->cursor->coverage = MARLIN_COVERAGE_RIGHT;
		} else {
			priv->cursor->coverage = MARLIN_COVERAGE_BOTH;
		}
	}

	position = (event->x + priv->xofs) * priv->frames_per_pixel;

	if (position > priv->number_of_frames) {
		/* We're in the dead area...
		   We don't care about clicks here */
		return TRUE;
	}

	switch (event->button) {
	case 1:
		switch (event->type) {
		case GDK_BUTTON_PRESS:
			selection = priv->selection;
			if (event->state & GDK_CONTROL_MASK) {
				/* Don't do anything */
			} else if (can_expand_selection (view, position)) {
				GdkCursor *cursor;

				cursor = marlin_cursor_get (widget, LEFT_RIGHT_ARROW);
				gdk_pointer_grab (widget->window, FALSE,
						  GDK_POINTER_MOTION_MASK |
						  GDK_BUTTON1_MOTION_MASK |
						  GDK_BUTTON_RELEASE_MASK,
						  NULL, cursor, event->time);
				gdk_cursor_unref (cursor);

				priv->in_selection = FALSE;
				priv->expand = TRUE;

 				priv->sel_initial = position;
				priv->sel_ctxt = marlin_undo_manager_context_begin (priv->undo, _("Select Region"));
				marlin_sample_selection_get (priv->selection,
							     &priv->sel_coverage,
							     &priv->sel_start,
							     &priv->sel_finish);
				priv->orig_coverage = priv->sel_coverage;
				priv->orig_start = priv->sel_start;
				priv->orig_finish = priv->sel_finish;
			} else if (marlin_sample_selection_contains_frame (selection,
									   priv->cursor->coverage,
									   position) == FALSE) {
				GdkCursor *cursor;

				move_cursor (view, position, FALSE);
				/* Clear the previous selection */
				if (event->state & GDK_SHIFT_MASK) {
					/* Nothing yet...*/
				} else {
					priv->sel_ctxt = marlin_undo_manager_context_begin (priv->undo, _("Select Region"));
 					marlin_sample_selection_clear (selection, priv->sel_ctxt);
				}

				/* Start a grab for creating a selection */
				cursor = marlin_cursor_get (widget, LEFT_RIGHT_ARROW);
				gdk_pointer_grab (widget->window, FALSE,
						  GDK_POINTER_MOTION_MASK |
						  GDK_BUTTON1_MOTION_MASK |
						  GDK_BUTTON_RELEASE_MASK,
						  NULL, cursor, event->time);
				gdk_cursor_unref (cursor);

				priv->in_selection = TRUE;
				priv->expand = TRUE;

#if 0
				/* FIXME: Implement custom undo */
				marlin_sample_selection_set (selection,
							     priv->cursor->coverage,
							     position,

							     position + priv->frames_per_pixel - 1,
							     NULL);
#endif
				priv->sel_coverage = priv->cursor->coverage;
				priv->sel_start = position;
				priv->sel_finish = position + priv->frames_per_pixel - 1;
				priv->sel_initial = position;

				priv->orig_coverage = priv->sel_coverage;
				priv->orig_start = priv->sel_start;
				priv->orig_finish = priv->sel_finish;

			} else  {
				/* Maybe start drag? */

				if (event->state & GDK_SHIFT_MASK) {
					GdkCursor *cursor;

					cursor = marlin_cursor_get (widget, HAND_CLOSED);
					gdk_pointer_grab (widget->window,
							  FALSE,
							  GDK_POINTER_MOTION_MASK |
							  GDK_BUTTON1_MOTION_MASK |
							  GDK_BUTTON_RELEASE_MASK,
							  NULL, cursor, event->time);
					gdk_cursor_unref (cursor);

					priv->moving_selection = TRUE;
					priv->grab_position = position;

					priv->sel_ctxt = marlin_undo_manager_context_begin (priv->undo, _("Move Selection"));
					marlin_sample_selection_get (selection,
								     &priv->sel_coverage,
								     &priv->sel_start,
								     &priv->sel_finish);
					priv->orig_coverage = priv->sel_coverage;
					priv->orig_start = priv->sel_start;
					priv->orig_finish = priv->sel_finish;
				} else {
					g_print ("Maybe start drag?\n");
					priv->maybe_drag = TRUE;
					priv->drag_x = event->x_root;
					priv->drag_y = event->y_root;
				}
			}

			break;

		case GDK_2BUTTON_PRESS:
			get_closest_markers (view, position, &start_mark, &end_mark);

			/* Select the region between the markers */
			selection = priv->selection;

			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Select Region"));
			marlin_sample_selection_set (selection,
						     priv->cursor->coverage,
						     start_mark, end_mark,
						     ctxt);
			marlin_undo_manager_context_end (priv->undo, ctxt);
			priv->made_selection = TRUE;
			break;

		case GDK_3BUTTON_PRESS:
			/* Select all the sample */
			selection = view->priv->selection;

			priv->expand = TRUE;
			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Select All"));
			marlin_sample_selection_set (selection,
						     priv->cursor->coverage,
						     0, priv->number_of_frames - 1,
						     ctxt);
			marlin_undo_manager_context_end (priv->undo, ctxt);
			priv->made_selection = TRUE;
			break;

		default:
			break;
		}

		break;

	case 2:
		g_print ("Button 2\n");
		return TRUE;

	case 3:
		g_print ("Button 3\n");
		return TRUE;

	}

	return FALSE;
}

struct _view_selection_closure {
	MarlinSampleSelection *selection;
	guint64 start;
	guint64 finish;
	MarlinCoverage coverage;
};

static void
view_selection_undo_redo (gpointer data)
{
	struct _view_selection_closure *c = data;
	guint64 old_s, old_f;
	MarlinCoverage old_c;
	
	marlin_sample_selection_get (c->selection, &old_c, &old_s, &old_f);
	marlin_sample_selection_set (c->selection, c->coverage, c->start, c->finish, NULL);
	
	c->coverage = old_c;
	c->start = old_s;
	c->finish = old_f;
}
	
static void
view_selection_destroy (gpointer data)
{
	g_free (data);
}

static int
button_release_event (GtkWidget *widget,
		      GdkEventButton *event)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	guint64 position;

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;

	if (priv->sample == NULL) {
		return TRUE;
	}

	remove_scroll_timeout (view);
	
	position = (MAX (event->x, 0) + priv->xofs) * priv->frames_per_pixel;

	switch (event->button) {
	case 1:
		drag_stop (view, event->time);

		if (priv->expand && position != priv->orig_start) {
			priv->expand = FALSE;
		} else if (priv->moving_selection) {
			priv->moving_selection = FALSE;
			priv->grab_position = 0;
		} else {
			if (priv->made_selection == FALSE) {
				MarlinUndoContext *ctxt;
				
				ctxt = marlin_undo_manager_context_begin (priv->undo,
									  _("Clear Selected Region"));
				marlin_sample_selection_clear (priv->selection,
							       ctxt);
				marlin_undo_manager_context_end (priv->undo, 
								 ctxt);
			
				/* FIXME: Need to free the context */
				/* 			priv->sel_ctxt = NULL; */
			}
			priv->made_selection = FALSE;
			priv->expand = FALSE;
		}
		
		if (gdk_pointer_is_grabbed ()) {
			gdk_pointer_ungrab (GDK_CURRENT_TIME);
		}

		break;

	default:
		break;
	}

	/* If position == priv->orig_start then its just a move cursor
	   so we need to just cancel the context if one exists */
	if (priv->sel_ctxt && position != priv->orig_start) {
		struct _view_selection_closure *c;
		MarlinUndoable *u;

		c = g_new (struct _view_selection_closure, 1);
		c->selection = priv->selection;
		c->coverage = priv->orig_coverage;
		c->start = priv->orig_start;
		c->finish = priv->orig_finish;

		u = marlin_undoable_new (view_selection_undo_redo,
					 view_selection_undo_redo,
					 view_selection_destroy,
					 c);

		marlin_undo_context_add (priv->sel_ctxt, u);

		marlin_undo_manager_context_end (priv->undo, priv->sel_ctxt);
		priv->sel_ctxt = NULL;
	} else if (priv->sel_ctxt) {
		marlin_undo_manager_context_cancel (priv->undo, priv->sel_ctxt);
		priv->sel_ctxt = NULL;
	}

	return FALSE;
}

static int
scroll_event (GtkWidget *widget,
	      GdkEventScroll *event)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	guint frames_per_pixel;

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;

	switch (event->direction) {
	case GDK_SCROLL_UP:

		if (event->state & GDK_SHIFT_MASK) {
			marlin_sample_view_vzoom_in (view);
			return FALSE;
		}
		
		frames_per_pixel = priv->frames_per_pixel / 2;
		if (frames_per_pixel < MIN_ZOOM) {
			frames_per_pixel = MIN_ZOOM;
		}
		
		g_object_set (G_OBJECT (view),
			      "frames_per_pixel", frames_per_pixel,
			      NULL);
		
		break;

	case GDK_SCROLL_DOWN:

		if (event->state & GDK_SHIFT_MASK) {
			marlin_sample_view_vzoom_out (view);
			return FALSE;
		}
		
		frames_per_pixel = priv->frames_per_pixel * 2;
		if (priv->number_of_frames / frames_per_pixel < widget->allocation.width) {
			/* Take it back to the previous zoom */
			frames_per_pixel /= 2;
		}

		g_object_set (G_OBJECT (view),
			      "frames_per_pixel", frames_per_pixel,
			      NULL);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	return FALSE;
}

static int
motion_notify_event (GtkWidget *widget,
		     GdkEventMotion *event)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;
	MarlinCoverage coverage;
	guint64 position;
	int channel_height, x, y, doc_width;

	view = MARLIN_SAMPLE_VIEW (widget);
	priv = view->priv;

	if (priv->sample == NULL) {
		return TRUE;
	}
	
	channel_height = widget->allocation.height / priv->number_of_channels;

	x = event->x;
	y = event->y;

	doc_width = priv->number_of_frames / priv->frames_per_pixel;

	if (gdk_pointer_is_grabbed () &&
	    ((x < SCROLL_THRESHOLD && priv->xofs > 0) ||
	     (x >= widget->allocation.width - SCROLL_THRESHOLD &&
	      priv->xofs < doc_width - widget->allocation.width))) {
		setup_scroll_timeout (view);
 		return FALSE;
	} else {
		remove_scroll_timeout (view);
	}

	if (priv->number_of_channels == 1) {
		coverage = MARLIN_COVERAGE_BOTH;
	} else if (y < channel_height / 2) {
		coverage = MARLIN_COVERAGE_LEFT;
	} else if (y > (channel_height + (channel_height / 2))) {
		coverage = MARLIN_COVERAGE_RIGHT;
	} else {
		coverage = MARLIN_COVERAGE_BOTH;
	}
		   
	x = MAX (x, 0);
	position = (x + priv->xofs) * priv->frames_per_pixel;

	if (priv->maybe_drag) {
		if (gtk_drag_check_threshold (widget,
					      priv->drag_x, priv->drag_y,
					      event->x_root, event->y_root)) {
			return drag_start (view, event->time);
		}

		return FALSE;
	}

	if (priv->drag_in_progress) {
		return FALSE;
	}

	if (priv->expand) {
		set_selection (view, position, coverage);
	} else if (priv->moving_selection) {
		move_selection (view, (gint64) position - (gint64)priv->grab_position);
		/* Now we've moved it, we can update the grab position */
		priv->grab_position = position;
	} else if (can_expand_selection (view, position)) {
		GdkCursor *cursor;
		
		priv->using_hand = FALSE;
		cursor = marlin_cursor_get (widget, LEFT_RIGHT_ARROW);
		gdk_window_set_cursor (widget->window, cursor);
		gdk_cursor_unref (cursor);
	} else if (marlin_sample_selection_contains_frame (priv->selection,
							   coverage,
							   position)) {
		GdkCursor *cursor;
		
		if (priv->using_hand == FALSE) {
			/* Only change the cursor if we need to
			   otherwise it flickers */
			cursor = marlin_cursor_get (widget, HAND_OPEN);
			gdk_window_set_cursor (widget->window, cursor);
			gdk_cursor_unref (cursor);
			priv->using_hand = TRUE;
		}
	} else {
		if (priv->number_of_channels == 1) {
			GdkCursor *cursor;
			
			if (event->state & GDK_CONTROL_MASK) {
				cursor = priv->i_bar_minus;
			} else if (event->state & GDK_SHIFT_MASK) {
				cursor = priv->i_bar_add;
			} else {
				cursor = priv->i_bar;
			}
			gdk_window_set_cursor (widget->window, cursor);
			priv->using_hand = FALSE;
		} else {
			if (y < (channel_height / 2)) {
				gdk_window_set_cursor (widget->window,
						       priv->i_bar_left);
				priv->using_hand = FALSE;
			} else if (y > (channel_height + (channel_height / 2))) {
				gdk_window_set_cursor (widget->window,
						       priv->i_bar_right);
				priv->using_hand = FALSE;
			} else {
				GdkCursor *cursor;
				
				if (event->state & GDK_CONTROL_MASK) {
					cursor = view->priv->i_bar_minus;
				} else if (event->state & GDK_SHIFT_MASK) {
					cursor = view->priv->i_bar_add;
				} else {
					cursor = view->priv->i_bar;
				}
				gdk_window_set_cursor (widget->window, cursor);
				priv->using_hand = FALSE;
			}
		}
	}
	
	return TRUE;
}

static gboolean
focus_in_event (GtkWidget *widget,
		GdkEventFocus *event)
{
	MarlinSampleView *view = MARLIN_SAMPLE_VIEW (widget);
	int time;

	time = get_time (widget);
	
	view->priv->cursor->id = g_timeout_add (time * ON_MULTIPLIER,
						redraw_cursor, view);

	/* Show the cursor again */
	view->priv->cursor->visible = TRUE;
	real_redraw_cursor (view);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		/* Redraw it all
		   FIXME: Should probably just redraw any selection */

		rect.x = 0;
		rect.y = 0;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}

	return TRUE;
}

static gboolean
focus_out_event (GtkWidget *widget,
		 GdkEventFocus *event)
{
	MarlinSampleView *view = MARLIN_SAMPLE_VIEW (widget);

	/* Stop the cursor when we lose focus */
	g_source_remove (view->priv->cursor->id);

	/* Hide the cursor */
	view->priv->cursor->visible = FALSE;
	real_redraw_cursor (view);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		/* Redraw it all
		   FIXME: Should probably just redraw any selection */

		rect.x = 0;
		rect.y = 0;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}

	return TRUE;
}

static void
scroll_to (MarlinSampleView *view,
	   int x)
{
	MarlinSampleViewPrivate *priv;
	GtkWidget *widget;
	int xofs;
	int width, height;
	int src_x, dest_x;
	
	widget = GTK_WIDGET (view);
	priv = view->priv;
	
	xofs = x - priv->xofs;

	if (xofs == 0) {
		return;
	}

	priv->xofs = x;
	g_signal_emit (G_OBJECT (view), signals[PAGE_START_CHANGED], 0, (guint64) (priv->xofs * priv->frames_per_pixel));
	
	if (GTK_WIDGET_DRAWABLE (widget) == FALSE) {
		return;
	}
	
	width = widget->allocation.width;
	height = widget->allocation.height;
	
	if (abs (xofs) >= width) {
		GdkRectangle area;
		
		area.x = 0;
		area.y = 0;
		area.width = width;
		area.height = height;
		
		gdk_window_invalidate_rect (widget->window, &area, FALSE);
		return;
	}
	
	/* Copy the window area */
	src_x = xofs < 0 ? 0 : xofs;
	dest_x = xofs < 0 ? -xofs : 0;

	gdk_window_scroll (widget->window, dest_x - src_x, 0);
}

static void
adjustment_changed_cb (GtkAdjustment *adj,
		       gpointer data)
{
	MarlinSampleView *view;
	MarlinSampleViewPrivate *priv;

	view = MARLIN_SAMPLE_VIEW (data);
	priv = view->priv;

	scroll_to (view, priv->hadj->value);
}

static void
set_scroll_adjustments (MarlinSampleView *view,
			GtkAdjustment *hadj,
			GtkAdjustment *vadj)
{
	MarlinSampleViewPrivate *priv;
	gboolean need_adjust;

	priv = view->priv;

	if (hadj == NULL ||
	    vadj == NULL) {
		return;
	}
	   
	if (priv->hadj && priv->hadj != hadj) {
		g_signal_handlers_disconnect_matched (G_OBJECT (priv->hadj),
						      G_SIGNAL_MATCH_DATA,
						      0, 0, NULL,
						      NULL, view);
		g_object_unref (G_OBJECT (priv->hadj));
	}

#if 0
	if (priv->vadj && priv->vadj != vadj) {
		g_object_unref (G_OBJECT (priv->vadj));
	}
#endif
	need_adjust = FALSE;

	if (priv->hadj != hadj) {
		priv->hadj = hadj;
		g_object_ref (G_OBJECT (priv->hadj));

		g_signal_connect (G_OBJECT (priv->hadj), "value-changed",
				  G_CALLBACK (adjustment_changed_cb), view);
		need_adjust = TRUE;
	}
#if 0
	if (priv->vadj != vadj) {
		priv->vadj = vadj;
		g_object_ref (G_OBJECT (priv->vadj));
	}
#endif
	if (need_adjust == TRUE) {
		adjustment_changed_cb (NULL, view);
	}
}

static void
add_move_binding (GtkBindingSet *binding_set,
		  guint keyval,
		  guint modmask,
		  GtkMovementStep step,
		  int count)
{
	g_return_if_fail ((modmask & GDK_SHIFT_MASK) == 0);

	gtk_binding_entry_add_signal (binding_set, keyval, modmask,
				      "move_cursor", 3,
				      GTK_TYPE_ENUM, step,
				      G_TYPE_INT, count,
				      G_TYPE_BOOLEAN, FALSE);

	/* Selection-extending version */
	gtk_binding_entry_add_signal (binding_set, keyval,
				      modmask | GDK_SHIFT_MASK,
				      "move_cursor", 3,
				      GTK_TYPE_ENUM, step,
				      G_TYPE_INT, count,
				      G_TYPE_BOOLEAN, TRUE);
}

static void
marlin_sample_view_class_init (MarlinSampleViewClass *klass)
{
	GObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkBindingSet *binding_set;

	object_class = G_OBJECT_CLASS (klass);
	widget_class = GTK_WIDGET_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->size_allocate = size_allocate;
	widget_class->size_request = size_request;
	widget_class->realize = realize;
	widget_class->expose_event = expose_event;
	widget_class->button_press_event = button_press_event;
	widget_class->button_release_event = button_release_event;
	widget_class->scroll_event = scroll_event;
	widget_class->motion_notify_event = motion_notify_event;
	widget_class->focus_in_event = focus_in_event;
	widget_class->focus_out_event = focus_out_event;

	klass->set_scroll_adjustments = set_scroll_adjustments;
	klass->move_cursor = real_move_cursor;

	g_type_class_add_private (object_class, sizeof (MarlinSampleViewPrivate));

	/* Properties */
	g_object_class_install_property (object_class,
					 PROP_SAMPLE_DATA,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_FRAMES_PER_PIXEL,
					 g_param_spec_uint ("frames_per_pixel",
							    "", "",
							    0,
							    G_MAXUINT,
							    0,
							    G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_CURSOR_POSITION,
					 g_param_spec_uint64 ("cursor_position",
							      "", "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PLAY_POSITION,
					 g_param_spec_uint64 ("play_position",
							      "", "",
							      0, G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_CURSOR_COVERAGE,
					 g_param_spec_enum ("cursor_coverage",
							    "", "",
							    MARLIN_TYPE_COVERAGE,
							    MARLIN_COVERAGE_BOTH,
							    G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PLAY_COVERAGE,
					 g_param_spec_enum ("play_coverage",
							    "", "",
							    MARLIN_TYPE_COVERAGE,
							    MARLIN_COVERAGE_BOTH,
							    G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_SHOW_PLAY_CURSOR,
					 g_param_spec_boolean ("show_play_cursor",
							       "", "",
							       FALSE,
							       G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_BASE_OFFSET,
					 g_param_spec_int ("base_offset",
							   "", "",
							   G_MININT, G_MAXINT, 0,
							   G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_UNDO_MANAGER,
					 g_param_spec_object ("undo_manager",
							      "", "",
							      MARLIN_UNDO_MANAGER_TYPE,
							      G_PARAM_WRITABLE));

	widget_class->set_scroll_adjustments_signal =
		g_signal_new ("set_scroll_adjustments",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MarlinSampleViewClass, set_scroll_adjustments),
			      NULL, NULL,
			      marlin_marshal_VOID__OBJECT_OBJECT,
			      G_TYPE_NONE, 2,
			      GTK_TYPE_ADJUSTMENT,
			      GTK_TYPE_ADJUSTMENT);

	signals[FRAMES_PER_PIXEL_CHANGED] = g_signal_new ("frames-per-pixel-changed",
							  G_TYPE_FROM_CLASS (klass),
							  G_SIGNAL_RUN_FIRST |
							  G_SIGNAL_NO_RECURSE,
							  G_STRUCT_OFFSET (MarlinSampleViewClass, frames_per_pixel_changed),
							  NULL, NULL,
							  g_cclosure_marshal_VOID__UINT,
							  G_TYPE_NONE,
							  1, G_TYPE_UINT);
	signals[PAGE_START_CHANGED] = g_signal_new ("page-start-changed",
						    G_TYPE_FROM_CLASS (klass),
						    G_SIGNAL_RUN_FIRST |
						    G_SIGNAL_NO_RECURSE,
						    G_STRUCT_OFFSET (MarlinSampleViewClass, page_start_changed),
						    NULL, NULL,
						    marlin_marshal_VOID__UINT64,
						    G_TYPE_NONE,
						    1, G_TYPE_UINT64);
	signals[MOVE_CURSOR] = g_signal_new ("move-cursor",
					     G_TYPE_FROM_CLASS (klass),
					     G_SIGNAL_RUN_FIRST |
					     G_SIGNAL_ACTION,
					     G_STRUCT_OFFSET (MarlinSampleViewClass, move_cursor),
					     NULL, NULL,
					     marlin_marshal_VOID__ENUM_INT_BOOLEAN,
					     G_TYPE_NONE,
					     3,
					     GTK_TYPE_MOVEMENT_STEP,
					     GTK_TYPE_INT,
					     GTK_TYPE_BOOL);
	signals[VZOOM_CHANGED] = g_signal_new ("vzoom-changed",
					       G_TYPE_FROM_CLASS (klass),
					       G_SIGNAL_RUN_FIRST |
					       G_SIGNAL_NO_RECURSE,
					       G_STRUCT_OFFSET (MarlinSampleViewClass, vzoom_changed),
					       NULL, NULL,
					       marlin_marshal_VOID__FLOAT_FLOAT,
					       G_TYPE_NONE,
					       2, G_TYPE_FLOAT,
					       G_TYPE_FLOAT);

	/* Key bindings */
	binding_set = gtk_binding_set_by_class (klass);

	/* Move the insertion point */
	add_move_binding (binding_set, GDK_Right, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 1);
	add_move_binding (binding_set, GDK_Left, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -1);
	add_move_binding (binding_set, GDK_KP_Right, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 1);
	add_move_binding (binding_set, GDK_KP_Left, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -1);

	add_move_binding (binding_set, GDK_Right, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 5);
	add_move_binding (binding_set, GDK_Left, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -5);
	add_move_binding (binding_set, GDK_KP_Right, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 5);
	add_move_binding (binding_set, GDK_KP_Left, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -5);

	add_move_binding (binding_set, GDK_Page_Up, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 50);
	add_move_binding (binding_set, GDK_Page_Down, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -50);
	add_move_binding (binding_set, GDK_KP_Page_Up, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 50);
	add_move_binding (binding_set, GDK_KP_Page_Down, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -50);

	add_move_binding (binding_set, GDK_Page_Up, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1);
	add_move_binding (binding_set, GDK_Page_Down, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1);
	add_move_binding (binding_set, GDK_KP_Page_Up, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1);
	add_move_binding (binding_set, GDK_KP_Page_Down, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1);

	/* Using GTK_MOVEMENT_DISPLAY_LINE_ENDS as if it were
	   GTK_MOVEMENT_DISPLAY_PAGE_ENDS */
	add_move_binding (binding_set, GDK_Home, 0,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1);
	add_move_binding (binding_set, GDK_End, 0,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1);
	add_move_binding (binding_set, GDK_KP_Home, 0,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, -1);
	add_move_binding (binding_set, GDK_KP_End, 0,
			  GTK_MOVEMENT_DISPLAY_LINE_ENDS, 1);

	add_move_binding (binding_set, GDK_Home, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_BUFFER_ENDS, -1);
	add_move_binding (binding_set, GDK_End, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_BUFFER_ENDS, 1);
	add_move_binding (binding_set, GDK_KP_Home, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_BUFFER_ENDS, -1);
	add_move_binding (binding_set, GDK_KP_End, GDK_CONTROL_MASK,
			  GTK_MOVEMENT_BUFFER_ENDS, 1);
}

static void
marlin_sample_view_init (MarlinSampleView *view)
{
	MarlinSampleViewPrivate *priv;
	PangoRectangle rect;

	GTK_WIDGET_SET_FLAGS (view, GTK_CAN_FOCUS);

	view->priv = GET_PRIVATE (view);
	priv = view->priv;

	priv->frames_per_pixel = DEFAULT_FRAMES_PER_PIXEL;
	priv->number_of_channels = DEFAULT_CHANNELS;
	priv->xofs = 0;

	priv->marker_to_view = g_hash_table_new (NULL, NULL);
	priv->position_to_markers = g_hash_table_new (NULL, NULL);

	priv->marker_layout = gtk_widget_create_pango_layout (GTK_WIDGET (view),
							      "0123456789");

	pango_layout_get_extents (priv->marker_layout, &rect, NULL);
	priv->digital_offset = rect.y;

	priv->vmax = DEFAULT_VMAX;
	priv->vmin = DEFAULT_VMIN;
	priv->using_hand = FALSE;

	priv->base_offset = 0;

	priv->dc = marlin_sample_draw_context_new (NULL,
						   DEFAULT_FRAMES_PER_PIXEL,
						   TRUE);

	if (!gtk_style_lookup_color (GTK_WIDGET (view)->style, "marker",
				     &priv->marker_colour)) {
		GdkColormap *cmap = gdk_colormap_get_system ();

		g_print ("Using default\n");
		priv->marker_colour.red = 65535;

		gdk_colormap_alloc_color (cmap, &priv->marker_colour,
					  FALSE, TRUE);
	}
}

GtkWidget *
marlin_sample_view_new (guint frames_per_pixel)
{
	MarlinSampleView *view;

	view = g_object_new (MARLIN_SAMPLE_VIEW_TYPE,
			     "frames_per_pixel", frames_per_pixel,
			     NULL);

	return GTK_WIDGET (view);
}

void
marlin_sample_view_scroll_to (MarlinSampleView *view,
			      guint64 start_frame)
{
	int xoff;
	
	g_return_if_fail (IS_MARLIN_SAMPLE_VIEW (view));
	
	xoff = (guint) (start_frame / view->priv->frames_per_pixel);
	gtk_adjustment_set_value (view->priv->hadj, (double) xoff);
}

gboolean
marlin_sample_view_can_zoom_in (MarlinSampleView *view)
{
	g_return_val_if_fail (IS_MARLIN_SAMPLE_VIEW (view), FALSE);
	
	if (view->priv->sample == NULL) {
		return FALSE;
	}

	if (view->priv->frames_per_pixel == MIN_ZOOM) {
		return FALSE;
	}

	return TRUE;
}

gboolean
marlin_sample_view_can_zoom_out (MarlinSampleView *view)
{
	GtkWidget *widget = (GtkWidget *) view;
	MarlinSampleViewPrivate *priv = view->priv;

	g_return_val_if_fail (IS_MARLIN_SAMPLE_VIEW (view), FALSE);

	if (view->priv->sample == NULL) {
		return FALSE;
	}

	/* If the width of the sample < width of the view, then
	   we can't zoom in anymore */
	if (priv->number_of_frames / priv->frames_per_pixel < widget->allocation.width) {
		return FALSE;
	}

	return TRUE;
}

gboolean
marlin_sample_view_can_vzoom_out (MarlinSampleView *view)
{
	g_return_val_if_fail (IS_MARLIN_SAMPLE_VIEW (view), FALSE);

	if (view->priv->sample == NULL) {
		return FALSE;
	}

	if (view->priv->vmin == DEFAULT_VMIN) {
		return FALSE;
	}

	return TRUE;
}

gboolean
marlin_sample_view_can_vzoom_in (MarlinSampleView *view)
{
	g_return_val_if_fail (IS_MARLIN_SAMPLE_VIEW (view), FALSE);

	if (view->priv->sample == NULL) {
		return FALSE;
	}

	/* Can always zoom in if there's a sample,
	   there's no theoretical limit on a vzoom
	   FIXME: Maybe we should impose one? */
	return TRUE;
}

void
marlin_sample_view_set_vzoom (MarlinSampleView *view,
			      double vmin,
			      double vmax)
{
	GtkWidget *widget;
	double length;
	
	widget = GTK_WIDGET (view);

	length = vmax - vmin;

	if (length > 2.0) {
		vmin = -1.0;
		vmax = 1.0;
	} else if (vmin < -1.0) {
		vmin = -1.0;
		vmax = vmin + length;
	} else if (vmax > 1.0) {
		vmax = 1.0;
		vmin = vmax - length;
	}
	
	view->priv->vmin = vmin;
	view->priv->vmax = vmax;

	view->priv->dc->vmin = vmin;
	view->priv->dc->vmax = vmax;

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;
		
		rect.x = 0;
		rect.y = 0;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;
		
		gdk_window_invalidate_rect (widget->window,
					    &rect, FALSE);
	}

	g_signal_emit (view, signals[VZOOM_CHANGED], 0, vmax, vmin);
}

void
marlin_sample_view_vzoom_in (MarlinSampleView *view)
{
	double delta;
	double vmax, vmin;

	delta = view->priv->vmax - view->priv->vmin;

	vmax = view->priv->vmax - delta / 8;
	vmin = view->priv->vmin + delta / 8;
	
	marlin_sample_view_set_vzoom (view, vmin, vmax);
}

void
marlin_sample_view_vzoom_out (MarlinSampleView *view)
{
	double delta;
	double vmax, vmin;

	delta = view->priv->vmax - view->priv->vmin;

	vmin = view->priv->vmin - delta / 8;
	vmax = view->priv->vmax + delta / 8;

	marlin_sample_view_set_vzoom (view, vmin, vmax);
}
