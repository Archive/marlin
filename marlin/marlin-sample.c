/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <glib/gi18n.h>

#include <gst/gst.h>

#ifdef HAVE_LADSPA
#include <ladspa.h>
#endif

#include <marlin/marlin-block.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-read-write-lock.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-marker-model.h>

#define MAX_NUM_CHANNELS 16

enum {
	PROP_0,
	PROP_NUMBER_OF_FRAMES,
	PROP_SAMPLE_RATE,
	PROP_FILENAME,
	PROP_REALNAME,
	PROP_CHANNELS,
	PROP_DEPTH,
	PROP_LENGTH,
	PROP_DIRTY,
	PROP_WRITABLE,
	PROP_LOADING,
	PROP_SELECTION,
	PROP_MARKER_MODEL,
	PROP_TAGLIST,
	PROP_ENCODER,
	PROP_BPM,
};

enum {
	DATA_CHANGED,
	LAST_SIGNAL
};

struct _MarlinSamplePrivate {
	MarlinReadWriteLock *lock; /* RWLock protecting data */
 	GPtrArray *channel_data; /* Array of the channel data */

	/* File IO information */
	char *filename; /* The filename that this was created from */
	char *realname; /* The realname of the sample */
	gboolean dirty; /* Has the sample been edited since the last save? */
	gboolean writable; /* Is the filename writable? */
	gboolean revertable; /* Has the sample got revert info? */

	/* Sample information */
	guint64 number_of_frames; /* Total number of frames */
	int channels; /* Number of channels */
	guint depth; /* Bit depth */
	guint sample_rate; /* Sample rate */
	float length; /* Length of sample in seconds */

	/* Selection information */
	/* FIXME: Should this be in a view? */
	MarlinSampleSelection *selection; /* The selections active on this
					     sample */

	/* Model for the markers */
	MarlinMarkerModel *markers;
	guint change_id, add_id, remove_id;

	GstTagList *taglist; /* The tag list */

	GstElement *encoder; /* The encoder pipeline that was used
				to save last time */
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_SAMPLE_TYPE, MarlinSamplePrivate))
G_DEFINE_TYPE (MarlinSample, marlin_sample, G_TYPE_OBJECT);
static guint32 signals[LAST_SIGNAL] = {0};

GQuark
marlin_sample_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-sample-error-quark");
	}

	return quark;
}

static void
finalize (GObject *object)
{
	MarlinSample *sample;
	MarlinSamplePrivate *priv;
	int i;

	sample = MARLIN_SAMPLE (object);
	priv = sample->priv;

	/* Lock ... */
	WRITE_LOCK (priv->lock);
	g_print ("Destroying sample %p\n", sample);

	for (i = 0; i < priv->channel_data->len; i++) {
		marlin_channel_free (priv->channel_data->pdata[i]);
	}

	g_free (priv->filename);

	if (priv->taglist != NULL) {
		gst_structure_free (priv->taglist);
	}

	WRITE_UNLOCK (priv->lock);
	marlin_read_write_lock_destroy (priv->lock);

	G_OBJECT_CLASS (marlin_sample_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinSample *sample = (MarlinSample *) object;
	MarlinSamplePrivate *priv = sample->priv;

	if (priv->selection) {
		g_object_unref (priv->selection);
		priv->selection = NULL;
	}

	if (priv->markers) {
		if (priv->change_id > 0) {
			g_signal_handler_disconnect (priv->markers,
						     priv->change_id);
		}
		if (priv->add_id > 0) {
			g_signal_handler_disconnect (priv->markers,
						     priv->add_id);
		}
		if (priv->remove_id > 0) {
			g_signal_handler_disconnect (priv->markers,
						     priv->remove_id);
		}

		g_object_unref (G_OBJECT (priv->markers));
		priv->markers = NULL;
	}

	G_OBJECT_CLASS (marlin_sample_parent_class)->dispose (object);
}

static void
marker_changed (MarlinMarkerModel *model,
		MarlinMarker *marker,
		MarlinSample *sample)
{
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinSample *sample;
	MarlinSamplePrivate *priv;
	MarlinMarkerModel *model;
	GstStructure *taglist, *old_tags;
	GstElement *encoder;
	int i, old, dc;
	guint64 frames;
	guint sample_rate;
	GError *error = NULL;

	sample = MARLIN_SAMPLE (object);
	priv = sample->priv;

	switch (prop_id) {
	case PROP_NUMBER_OF_FRAMES:
		WRITE_LOCK (priv->lock);

		frames = g_value_get_uint64 (value);
		if (priv->number_of_frames == frames) {
			WRITE_UNLOCK (priv->lock);
			return;
		}

		priv->number_of_frames = frames;
		if (priv->sample_rate != 0) {
			priv->length = (float) priv->number_of_frames / priv->sample_rate;
		}

		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_SAMPLE_RATE:
		WRITE_LOCK (priv->lock);

		sample_rate = g_value_get_uint (value);
		if (priv->sample_rate == sample_rate) {
			WRITE_UNLOCK (priv->lock);
			return;
		}

		priv->sample_rate = sample_rate;
		if (priv->sample_rate != 0) {
			priv->length = (float) priv->number_of_frames / priv->sample_rate;
		}

		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_FILENAME:
		WRITE_LOCK (priv->lock);

		if (priv->filename != NULL) {
			g_free (priv->filename);
		}
		priv->filename = g_strdup (g_value_get_string (value));

		if (g_file_test (priv->filename, G_FILE_TEST_EXISTS)) {
			if (access (priv->filename, W_OK) == 0) {
				priv->writable = TRUE;
			} else {
				priv->writable = FALSE;
			}
		} else {
			priv->writable = TRUE;
		}

		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_REALNAME:
		WRITE_LOCK (priv->lock);

		if (priv->realname != NULL) {
			g_free (priv->realname);
		}
		priv->realname = g_strdup (g_value_get_string (value));

		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_CHANNELS:
		WRITE_LOCK (priv->lock);

		old = priv->channels;
		priv->channels = g_value_get_uint (value);

		dc = old - priv->channels;
		if (dc == 0) {
			WRITE_UNLOCK (priv->lock);
			break;
		} else if (dc > 0) {
			for (i = old; i > priv->channels; i--) {
				marlin_channel_free (priv->channel_data->pdata[i - 1]);
				g_ptr_array_remove_index (priv->channel_data,
							  i - 1);
			}
		} else {
			for (i = 0; i < abs (dc); i++) {
				MarlinChannel *channel;

				channel = marlin_channel_new (priv->filename, &error);
				if (channel == NULL) {
					g_warning ("Error making channel");
					WRITE_UNLOCK (priv->lock);
					return;
				}

				g_ptr_array_add (priv->channel_data, channel);
			}
		}

		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_DEPTH:
		WRITE_LOCK (priv->lock);
		priv->depth = g_value_get_int (value);
		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_DIRTY:
		WRITE_LOCK (priv->lock);
		priv->dirty = g_value_get_boolean (value);
		WRITE_UNLOCK (priv->lock);
		break;

	case PROP_MARKER_MODEL:
		model = g_value_get_object (value);

		if (priv->markers == model) {
			/* Don't need to do anything */
			return;
		}

		g_signal_handler_disconnect (priv->markers,
					     priv->change_id);
		g_signal_handler_disconnect (priv->markers,
					     priv->add_id);
		g_signal_handler_disconnect (priv->markers,
					     priv->remove_id);
		g_object_unref (G_OBJECT (priv->markers));
		priv->markers = model;

		priv->change_id = g_signal_connect (priv->markers,
						    "marker-changed",
						    G_CALLBACK (marker_changed),
						    sample);
		priv->add_id = g_signal_connect (priv->markers,
						 "marker-added",
						 G_CALLBACK (marker_changed),
						 sample);
		priv->remove_id = g_signal_connect (priv->markers,
						    "marker-removed",
						    G_CALLBACK (marker_changed),
						    sample);
		g_object_ref (G_OBJECT (priv->markers));

		break;

	case PROP_TAGLIST:
		taglist = g_value_get_pointer (value);
		if (taglist == priv->taglist) {
			return;
		}

		/* Setting NULL for taglist will free the tags */
		if (taglist == NULL) {
			gst_structure_free (priv->taglist);
			priv->taglist = NULL;
		}

		/* Otherwise, the tags will all be merged */
		old_tags = priv->taglist;
		priv->taglist = gst_tag_list_merge (old_tags, taglist,
						    GST_TAG_MERGE_APPEND);
		gst_structure_free (old_tags);
		break;

	case PROP_ENCODER:
		encoder = g_value_get_object (value);

		if (priv->encoder == encoder) {
			/* Don't need to do anything */
			return;
		}

		g_object_unref (G_OBJECT (priv->encoder));
		priv->encoder = encoder;
		g_object_ref (G_OBJECT (priv->encoder));

		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinSample *sample;
	MarlinSamplePrivate *priv;
	char *title = NULL;

	sample = MARLIN_SAMPLE (object);
	priv = sample->priv;

	READ_LOCK (sample->priv->lock);
	switch (prop_id) {
	case PROP_NUMBER_OF_FRAMES:
		g_value_set_uint64 (value, priv->number_of_frames);
		break;

	case PROP_SAMPLE_RATE:
		g_value_set_uint (value, priv->sample_rate);
		break;

	case PROP_FILENAME:
		g_value_set_string (value, sample->priv->filename);
		break;

	case PROP_REALNAME:
		if (sample->priv->taglist != NULL) {
			gboolean ret;

			ret = gst_tag_list_get_string (sample->priv->taglist,
						       GST_TAG_TITLE,
						       &title);
			if (ret && title != NULL && *title != 0) {
				g_value_set_string (value, title);
				g_free (title);
			} else {
				if (sample->priv->realname) {
					g_value_set_string (value, sample->priv->realname);
				} else {
					char *realname;

					realname = g_path_get_basename (sample->priv->filename);
					g_value_set_string (value, realname);
					g_free (realname);
				}
			}
		} else {
			char *realname;

			realname = g_path_get_basename (sample->priv->filename);
			g_value_set_string (value, realname);
			g_free (realname);
		}
		break;

	case PROP_CHANNELS:
		g_value_set_uint (value, priv->channels);
		break;

	case PROP_DEPTH:
		g_value_set_int (value, priv->depth);
		break;

	case PROP_LENGTH:
		g_value_set_float (value, priv->length);
		break;

	case PROP_DIRTY:
		g_value_set_boolean (value, sample->priv->dirty);
		break;

	case PROP_WRITABLE:
		g_value_set_boolean (value, sample->priv->writable);
		break;

	case PROP_SELECTION:
		g_value_set_object (value, sample->priv->selection);
		break;

	case PROP_MARKER_MODEL:
		g_value_set_object (value, sample->priv->markers);
		break;

	case PROP_TAGLIST:
		g_value_set_pointer (value, sample->priv->taglist);
		break;

	case PROP_ENCODER:
		g_value_set_pointer (value, sample->priv->encoder);
		break;

	case PROP_BPM: {
		guint bpm = 120;
		gst_tag_list_get_uint (sample->priv->taglist, MARLIN_TAG_BPM,
				       &bpm);
		g_value_set_uint (value, bpm);
		break;
	}

	default:
		break;
	}

	READ_UNLOCK (sample->priv->lock);
}

static void
marlin_sample_class_init (MarlinSampleClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	g_type_class_add_private (o_class, sizeof (MarlinSamplePrivate));
	/* Properties */
	g_object_class_install_property (o_class, PROP_NUMBER_OF_FRAMES,
					 g_param_spec_uint64 ("total_frames",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_SAMPLE_RATE,
					 g_param_spec_uint ("sample_rate",
							    "",
							    "",
							    0,
							    G_MAXUINT,
							    0,
							    G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_FILENAME,
					 g_param_spec_string ("filename",
							      "",
							      "",
							      "Untitled",
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_REALNAME,
					 g_param_spec_string ("name",
							      "", "",
							      "Untitled",
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_CHANNELS,
					 g_param_spec_uint ("channels",
							    "",
							    "",
							    1,
							    MAX_NUM_CHANNELS,
							    2,
							    G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_DEPTH,
					 g_param_spec_int ("depth",
							   "",
							   "",
							   1,
							   G_MAXINT,
							   1,
							   G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_LENGTH,
					 g_param_spec_float ("length",
							     "",
							     "",
							     0,
							     G_MAXFLOAT,
							     0,
							     G_PARAM_READABLE));
	g_object_class_install_property (o_class, PROP_DIRTY,
					 g_param_spec_boolean ("dirty",
							       "",
							       "",
							       FALSE,
							       G_PARAM_READWRITE));

	g_object_class_install_property (o_class, PROP_WRITABLE,
					 g_param_spec_boolean ("writable",
							       "", "",
							       FALSE,
							       G_PARAM_READABLE));
	g_object_class_install_property (o_class, PROP_SELECTION,
					 g_param_spec_object ("selection",
							      "",
							      "",
							      MARLIN_SAMPLE_SELECTION_TYPE,
							      G_PARAM_READABLE));
	g_object_class_install_property (o_class, PROP_MARKER_MODEL,
					 g_param_spec_object ("markers",
							      "", "",
							      MARLIN_MARKER_MODEL_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_TAGLIST,
					 g_param_spec_pointer ("tags",
							       "", "",
							       G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_ENCODER,
					 g_param_spec_object ("encoder",
							      "", "",
							      GST_TYPE_ELEMENT,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_BPM,
					 g_param_spec_uint ("bpm",
							    "", "",
							    1, G_MAXUINT, 120,
							    G_PARAM_READABLE));

	signals[DATA_CHANGED] = g_signal_new ("data-changed",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinSampleClass, data_changed),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__POINTER,
					      G_TYPE_NONE,
					      1, G_TYPE_POINTER);
}

static void
marlin_sample_init (MarlinSample *sample)
{
	MarlinSamplePrivate *priv;

	priv = sample->priv = GET_PRIVATE (sample);

	/* Create the lock */
	priv->lock = marlin_read_write_lock_new ();

	priv->channel_data = g_ptr_array_new ();

	priv->selection = marlin_sample_selection_new ();

	priv->markers = marlin_marker_model_new ();
	priv->change_id = g_signal_connect (priv->markers, "marker-changed",
					    G_CALLBACK (marker_changed),
					    sample);
	priv->add_id = g_signal_connect (priv->markers, "marker-added",
					 G_CALLBACK (marker_changed),
					 sample);
	priv->remove_id = g_signal_connect (priv->markers, "marker-removed",
					    G_CALLBACK (marker_changed),
					    sample);

	priv->realname = NULL;

	priv->taglist = gst_tag_list_new ();
}

/**
 * marlin_sample_new:
 *
 * Creates a new stereo sample at 44100hz.
 *
 * Return value: A newly created #MarlinSample.
 */
MarlinSample *
marlin_sample_new (void)
{
	MarlinSample *sample;

	sample = g_object_new (MARLIN_SAMPLE_TYPE,
			       "filename", "untitled",
			       "channels", 2,
			       "sample-rate", 44100,
			       NULL);

	return sample;
}

/**
 * marlin_sample_new_from_sample_with_range:
 * @src: Original #MarlinSample
 * @range: A #MarlinRange.
 * @error: A #GError.
 *
 * Creates a new #MarlinSample with the data from @src defined by @range.
 *
 * Return value: A newly created #MarlinSample or NULL on failure.
 */
MarlinSample *
marlin_sample_new_from_sample_with_range (MarlinSample *src,
					  MarlinRange *range,
					  GError **error)
{
	MarlinSample *sample;
	MarlinChannel *src_channel, *dest_channel;
	guint sample_rate, channels;
	gboolean ret;
	guint64 total_frames;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), NULL);

	READ_LOCK (src->priv->lock);
	sample_rate = src->priv->sample_rate;
	channels = src->priv->channels;
	total_frames = src->priv->number_of_frames;
	READ_UNLOCK (src->priv->lock);

	sample = g_object_new (MARLIN_SAMPLE_TYPE,
			       "filename", "untitled",
			       "sample_rate", sample_rate,
			       "channels", range->coverage == MARLIN_COVERAGE_BOTH ? channels : 1,
			       NULL);

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < channels; i++) {

			src_channel = src->priv->channel_data->pdata[i];
			dest_channel = sample->priv->channel_data->pdata[i];

			ret = marlin_channel_copy_data (src_channel,
							dest_channel,
							range->start,
							range->finish, error);
			if (ret == FALSE) {
				return NULL;
			}
		}
		break;

	case MARLIN_COVERAGE_LEFT:
		src_channel = src->priv->channel_data->pdata[0];
		dest_channel = sample->priv->channel_data->pdata[0];

		ret = marlin_channel_copy_data (src_channel, dest_channel,
						range->start, range->finish,
						error);
		if (ret == FALSE) {
			return NULL;
		}

		break;

	case MARLIN_COVERAGE_RIGHT:
		src_channel = src->priv->channel_data->pdata[1];
		dest_channel = sample->priv->channel_data->pdata[1];

		ret = marlin_channel_copy_data (src_channel, dest_channel,
						range->start, range->finish,
						error);
		if (ret == FALSE) {
			return NULL;
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	g_object_set (G_OBJECT (sample),
		      "total_frames", (guint64) range->finish - range->start,
		      NULL);
	return sample;
}

/**
 * marlin_sample_new_from_sample:
 * @src: Original #MarlinSample
 * @error: A #GError
 *
 * Clones @src.
 *
 * Returns: A newly allocated #MarlinSample or NULL on failure.
 */
MarlinSample *
marlin_sample_new_from_sample (MarlinSample *src,
			       GError **error)
{
	MarlinRange range;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), NULL);

	READ_LOCK (src->priv->lock);
	range.start = 0;
	range.finish = src->priv->number_of_frames - 1;
	range.coverage = MARLIN_COVERAGE_BOTH;
	READ_UNLOCK (src->priv->lock);

	return marlin_sample_new_from_sample_with_range (src, &range, error);
}

/**
 * marlin_sample_new_from_selection:
 * @src: Original #MarlinSample
 * @error: A #GError
 *
 * Creates new #MarlinSample from the selection on @src.
 *
 * Returns: A newly allocated #MarlinSample or NULL on failure.
 */
MarlinSample *
marlin_sample_new_from_selection (MarlinSample *src,
				  GError **error)
{
	MarlinRange range;

	if (src->priv->selection == NULL) {
		return NULL;
	}

	marlin_sample_selection_get (src->priv->selection,
				     &range.coverage,
				     &range.start, &range.finish);

	return marlin_sample_new_from_sample_with_range (src, &range, error);
}

/**
 * marlin_sample_get_channel:
 * @sample: A #MarlinSample.
 * @channel_num: The position of the channel to retrieve.
 *
 * Retrieves the channel specified by @channel_num from @sample.
 *
 * Return value: A #MarlinChannel or NULL if @channel_num is greater than
 * the number of channels in the sample.
 */
MarlinChannel *
marlin_sample_get_channel (MarlinSample *sample,
			   guint channel_num)
{
	MarlinChannel *channel;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), NULL);
	g_return_val_if_fail (channel_num < sample->priv->channels, NULL);

	READ_LOCK (sample->priv->lock);
	channel = g_ptr_array_index (sample->priv->channel_data, channel_num);
	READ_UNLOCK (sample->priv->lock);

	return channel;
}

/* A generic closure for basic undo/redo contexts */
struct _sample_closure {
	MarlinSample *sample;
	MarlinRange range;
};

static void
delete_range_undo (gpointer data)
{
	struct _sample_closure *c = data;
	MarlinSample *sample = c->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel ? channel->frames : (guint64) 0,
		      NULL);

	marlin_sample_data_changed (sample, &(c->range));
}

static void
delete_range_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_delete_range:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Deletes the data in @sample specified in @range. Progress is reported via
 * @operation which can be NULL if no progress is to be reported.
 *
 * This operation can be undone by passing a #MarlinUndoContext in @ctxt,
 * or this can be NULL.
 *
 * Return value: TRUE on success, FALSE on failure.
 */
gboolean
marlin_sample_delete_range (MarlinSample *sample,
			    MarlinOperation *operation,
			    MarlinRange *range,
			    MarlinUndoContext *ctxt,
			    GError **error)
{
	MarlinChannel *channel;
	struct _sample_closure *c;
	MarlinUndoable *u;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;

	u = marlin_undoable_new (delete_range_undo,
				 NULL,
				 delete_range_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	for (i = 0; i < sample->priv->channel_data->len; i++) {
		marlin_channel_delete_range (sample->priv->channel_data->pdata[i], range->start, range->finish, ctxt);
	}

	channel = sample->priv->channel_data->pdata[0];

	marlin_marker_model_remove_markers_in_range (sample->priv->markers,
						     range->start, range->finish, ctxt);
	marlin_marker_model_move_markers_after (sample->priv->markers,
						range->start, -(range->finish - range->start), ctxt);
	WRITE_UNLOCK (sample->priv->lock);

	u = marlin_undoable_new (NULL,
				 delete_range_undo,
				 NULL,
				 c);
	marlin_undo_context_add (ctxt, u);

	g_object_set (G_OBJECT (sample),
		      "total-frames", channel ? channel->frames : (guint64) 0,
		      "dirty", TRUE,
		      NULL);

	return TRUE;
}

static void
clear_range_undo (gpointer data)
{
	struct _sample_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
clear_range_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_clear_range:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Clears the data in @sample that is specified by @range by setting it to 0.
 * Progress will be reported through @operation, or this can be NULL if no
 * reporting is required.
 *
 * This operation can be undo by passing a #MarlinUndoContext to @ctxt or it
 * can be NULL.
 *
 * Return value: TRUE on success, FALSE on failure
 */
gboolean
marlin_sample_clear_range (MarlinSample *sample,
			   MarlinOperation *operation,
			   MarlinRange *range,
			   MarlinUndoContext *ctxt,
			   GError **error)
{
	MarlinUndoable *u;
	struct _sample_closure *c;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = range->start;
	c->range.finish = range->finish;
	c->range.coverage = range->coverage;

	u = marlin_undoable_new (clear_range_undo,
				 clear_range_undo,
				 clear_range_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	if (operation != NULL) {
		marlin_operation_start (operation);
	}

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < sample->priv->channel_data->len; i++) {
			MarlinChannel *channel = sample->priv->channel_data->pdata[i];
			marlin_channel_clear_range (channel, operation,
						    range->start,
						    range->finish,
						    ctxt, error);
		}

		break;

	case MARLIN_COVERAGE_LEFT:
		marlin_channel_clear_range (sample->priv->channel_data->pdata[MARLIN_CHANNEL_LEFT],
					    operation,
					    range->start, range->finish,
					    ctxt, error);
		break;

	case MARLIN_COVERAGE_RIGHT:
		if (sample->priv->channels != 2) {
			g_warning ("Editting right channel on mono sample?");

			WRITE_UNLOCK (sample->priv->lock);
			if (operation != NULL) {
				marlin_operation_finish (operation);
			}

			return FALSE;
		}

		marlin_channel_clear_range (sample->priv->channel_data->pdata[MARLIN_CHANNEL_RIGHT],
					    operation,
					    range->start, range->finish,
					    ctxt, error);
		break;

	default:
		break;
	}

	WRITE_UNLOCK (sample->priv->lock);

	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);

	marlin_sample_data_changed (sample, range);
	return TRUE;
}

static void
crop_range_undo (gpointer data)
{
	struct _sample_closure *c = data;
	MarlinSample *sample = c->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel ? channel->frames : (guint64) 0,
		      NULL);
	marlin_sample_data_changed (sample, &(c->range));
}

static void
crop_range_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_crop_range:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @range: The range to be cropped
 * @ctxt: A #MarlinUndoContext
 * @GError: A #GError
 *
 * Deletes the data in @sample before and after @range.
 *
 * Return value: TRUE on success, FALSE on failure
 */
gboolean
marlin_sample_crop_range (MarlinSample *sample,
			  MarlinOperation *operation,
			  MarlinRange *range,
			  MarlinUndoContext *ctxt,
			  GError **error)
{
	MarlinChannel *channel;
	struct _sample_closure *c;
	MarlinUndoable *u;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;

	u = marlin_undoable_new (crop_range_undo,
				 crop_range_undo,
				 crop_range_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	marlin_marker_model_remove_markers_in_range (sample->priv->markers,
						     (guint64) 0, range->start, ctxt);
	marlin_marker_model_remove_markers_in_range (sample->priv->markers,
						     range->finish,
						     sample->priv->number_of_frames, ctxt);
	marlin_marker_model_move_markers_after (sample->priv->markers, range->start,
						-range->start, ctxt);

	for (i = 0; i < sample->priv->channel_data->len; i++) {
		marlin_channel_crop_range (sample->priv->channel_data->pdata[i], range->start, range->finish, ctxt);
	}

	channel = sample->priv->channel_data->pdata[0];

	WRITE_UNLOCK (sample->priv->lock);

	/* This is an ugly hack... FIXMEFIXME */
	c = g_new (struct _sample_closure, 1);
	c->sample = sample;

	u = marlin_undoable_new (crop_range_undo,
				 crop_range_undo,
				 crop_range_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	g_object_set (G_OBJECT (sample),
		      "total-frames", channel ? channel->frames : (guint64) 0,
		      "dirty", TRUE,
		      NULL);

	return TRUE;
}

static void
swap_channels_undo (gpointer data)
{
	struct _sample_closure *c = data;

	marlin_sample_swap_channels (c->sample, NULL, NULL, NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
swap_channels_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_swap_channels:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Swaps the left and right channels of @sample.
 *
 * Return value: TRUE on success, FALSE on failure with more details in @error.
 */
gboolean
marlin_sample_swap_channels (MarlinSample *sample,
			     MarlinOperation *operation,
			     MarlinUndoContext *ctxt,
			     GError **error)
{
	MarlinSamplePrivate *priv;
	struct _sample_closure *c = NULL;
	MarlinChannel *tmp;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	priv = sample->priv;
	WRITE_LOCK (priv->lock);

	if (priv->channels != 2) {
		WRITE_UNLOCK (priv->lock);

		if (*error != NULL) {
			/* Make GError */
		}

		return FALSE;
	}

	tmp = priv->channel_data->pdata[0];
	priv->channel_data->pdata[0] = priv->channel_data->pdata[1];
	priv->channel_data->pdata[1] = tmp;

	WRITE_UNLOCK (priv->lock);

	if (ctxt) {
		MarlinUndoable *u;

		c = g_new (struct _sample_closure, 1);
		c->sample = sample;
		c->range.start = 0;
		c->range.finish = priv->number_of_frames - 1;
		c->range.coverage = MARLIN_COVERAGE_BOTH;

		u = marlin_undoable_new (swap_channels_undo,
					 swap_channels_undo,
					 swap_channels_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	if (priv->number_of_frames != 0) {
		g_object_set (G_OBJECT (sample),
			      "dirty", TRUE,
			      NULL);
	}

	marlin_sample_data_changed (sample, &(c->range));

	return TRUE;
}

static void
mix_undo_redo (gpointer data)
{
	struct _sample_closure *c = data;
	MarlinChannel *channel = c->sample->priv->channel_data->pdata[0];

	g_object_set (G_OBJECT (c->sample),
		      "total-frames", channel->frames,
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
mix_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_mix:
 * @dest: A #MarlinSample
 * @src: A #MarlinSample
 * @src_db: The volume of @src in the mix
 * @dest_db: The volume of @dest in the mix
 * @range: The range to be mixed
 * @clip: Whether or not @src will be clipped by the length of @dest
 * @operation: A #MarlinOperation
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Mixes @src into @dest
 * FIXME: How?
 *
 * Return value: TRUE on success, FALSE on failure
 */
gboolean
marlin_sample_mix (MarlinSample *dest,
		   MarlinSample *src,
		   double src_db,
		   double dest_db,
		   MarlinRange *range,
		   gboolean clip,
		   MarlinOperation *operation,
		   MarlinUndoContext *ctxt,
		   GError **error)
{
	int chan;
	MarlinSamplePrivate *s_priv, *d_priv;
	MarlinChannel *channel;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (dest), FALSE);
	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), FALSE);

	s_priv = src->priv;
	d_priv = dest->priv;

	if (s_priv->channels != d_priv->channels) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH,
					      _("The number of channels on the src and destination samples did not match"));
		}

		return FALSE;
	}

	if (ctxt) {
		struct _sample_closure *c;
		MarlinUndoable *u;

		c = g_new (struct _sample_closure, 1);
		c->sample = dest;

		u = marlin_undoable_new (mix_undo_redo,
					 mix_undo_redo,
					 mix_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_LOCK (s_priv->lock);
	READ_LOCK (d_priv->lock);

	for (chan = 0; chan < s_priv->channels; chan++) {
		MarlinChannel *s_chan, *d_chan;
		double s_ratio, d_ratio;

		s_chan = s_priv->channel_data->pdata[chan];
		d_chan = d_priv->channel_data->pdata[chan];

		s_ratio = marlin_db_to_ratio (src_db);
		d_ratio = marlin_db_to_ratio (dest_db);
		marlin_channel_mix (d_chan, s_chan, s_ratio, d_ratio,
				    range->start, range->finish, clip,
				    operation, ctxt, error);
	}

	channel = d_priv->channel_data->pdata[0];

	WRITE_UNLOCK (s_priv->lock);
	READ_UNLOCK (d_priv->lock);

	g_object_set (G_OBJECT (dest),
 		      "total-frames", channel->frames,
		      "dirty", TRUE,
		      NULL);

	return TRUE;
}

static void
insert_undo (gpointer data)
{
	struct _sample_closure *c = data;
	MarlinSample *sample = c->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "total_frames", channel->frames,
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (sample, &(c->range));
}

static void
insert_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_insert:
 * @dest: A #MarlinSample
 * @src: A #MarlinSample
 * @position: The insert position
 * @coverage: The coverage
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Inserts the channels defined in @coverage from @src into @dest at @position.
 *
 * Return value: TRUE on success, FALSE on failure
 */
gboolean
marlin_sample_insert (MarlinSample *dest,
		      MarlinSample *src,
		      guint64 position,
		      MarlinCoverage coverage,
		      MarlinUndoContext *ctxt,
		      GError **error)
{
	guint src_channels, dest_channels, i;
	MarlinChannel *channel;
	struct _sample_closure *c = NULL;
	MarlinUndoable *u;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), FALSE);
	g_return_val_if_fail (IS_MARLIN_SAMPLE (dest), FALSE);

	src_channels = src->priv->channels;
	dest_channels = dest->priv->channels;

	if (src_channels != dest_channels) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH,
					      _("The number of channels on the src and destination samples did not match"));
		}
		return FALSE;
	}

	if (ctxt) {
		c = g_new (struct _sample_closure, 1);
		c->sample = dest;

		u = marlin_undoable_new (insert_undo,
					 NULL,
					 insert_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	READ_LOCK (src->priv->lock);
	WRITE_LOCK (dest->priv->lock);

	for (i = 0; i < src_channels; i++) {
		MarlinChannel *s_channel = src->priv->channel_data->pdata[i];
		MarlinChannel *d_channel = dest->priv->channel_data->pdata[i];

		marlin_channel_insert (d_channel, s_channel, position, ctxt);
	}

	WRITE_UNLOCK (dest->priv->lock);
	READ_UNLOCK (src->priv->lock);

	if (ctxt) {
		u = marlin_undoable_new (NULL,
					 insert_undo,
					 NULL,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	channel = dest->priv->channel_data->pdata[0];

	g_object_set (G_OBJECT (dest),
		      "total_frames", channel->frames,
		      "dirty", TRUE,
		      NULL);

	return TRUE;
}

static void
invert_range_undo (gpointer data)
{
	struct _sample_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
invert_range_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_invert_range:
 * @sample: A #MarlinSample
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Inverts the samples in @sample as defined by @range.
 *
 * Return value: TRUE on success, FALSE on failure.
 */
gboolean
marlin_sample_invert_range (MarlinSample *sample,
			    MarlinRange *range,
			    MarlinUndoContext *ctxt,
			    GError **error)
{
	int i;
	gboolean ret;
	MarlinUndoable *u;
	struct _sample_closure *c;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = range->start;
	c->range.finish = range->finish;
	c->range.coverage = range->coverage;

	u = marlin_undoable_new (invert_range_undo,
				 invert_range_undo,
				 invert_range_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < sample->priv->channels; i++) {
			ret = marlin_channel_invert (sample->priv->channel_data->pdata[i], range->start, range->finish, ctxt, error);
			if (ret == FALSE) {
				WRITE_UNLOCK (sample->priv->lock);
				return FALSE;
			}
		}
		break;

	case MARLIN_COVERAGE_LEFT:
		ret = marlin_channel_invert (sample->priv->channel_data->pdata[0],
					     range->start, range->finish,
					     ctxt, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	case MARLIN_COVERAGE_RIGHT:
		if (sample->priv->channels != 2) {
			g_warning ("Attempting to invert RIGHT in a mono sample");
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}
		ret = marlin_channel_invert (sample->priv->channel_data->pdata[1],
					     range->start, range->finish,
					     ctxt, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	default:
		break;
	}

	WRITE_UNLOCK (sample->priv->lock);
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (sample, &(c->range));

	return TRUE;
}

static void
adjust_volume_undo (gpointer data)
{
	struct _sample_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
adjust_volume_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_adjust_volume_range:
 * @sample: A #MarlinSample
 * @db: The volume to be set
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Adjusts the volume of @sample to @db, within @range.
 *
 * Return value: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_sample_adjust_volume_range (MarlinSample *sample,
				   float db,
				   MarlinRange *range,
				   MarlinUndoContext *ctxt,
				   GError **error)
{
	int i;
	gboolean ret;
	struct _sample_closure *c;
	MarlinUndoable *u;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = range->start;
	c->range.finish = range->finish;
	c->range.coverage = range->coverage;

	u = marlin_undoable_new (adjust_volume_undo,
				 adjust_volume_undo,
				 adjust_volume_destroy,
				 c);

	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < sample->priv->channels; i++) {
			ret = marlin_channel_adjust_volume (sample->priv->channel_data->pdata[i], db, range->start, range->finish, ctxt, error);

			if (ret == FALSE) {
				WRITE_UNLOCK (sample->priv->lock);
				return FALSE;
			}
		}
		break;

	case MARLIN_COVERAGE_LEFT:
		ret = marlin_channel_adjust_volume (sample->priv->channel_data->pdata[0], db, range->start, range->finish, ctxt, error);

		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	case MARLIN_COVERAGE_RIGHT:
		if (sample->priv->channels != 2) {
			g_warning ("Attempting to adjust volume on RIGHT in a mono sample.");
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		ret = marlin_channel_adjust_volume (sample->priv->channel_data->pdata[1], db, range->start, range->finish, ctxt, error);

		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	default:
		break;
	}

	WRITE_UNLOCK (sample->priv->lock);

	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);

	marlin_sample_data_changed (sample, range);

	return TRUE;
}

static void
insert_silence_undo (gpointer closure)
{
	struct _sample_closure *isc = closure;
	MarlinSample *sample = isc->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);
}

static void
insert_silence_destroy (gpointer closure)
{
	g_free (closure);
}

/* FIXME: Should this take a MarlinRange or a coverage? */
/**
 * marlin_sample_insert_silence:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @position: The insert position
 * @length: The length of the silence
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Inserts @length frames of silence into @sample at @position.
 *
 * Return value: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_sample_insert_silence (MarlinSample *sample,
			      MarlinOperation *operation,
			      guint64 position,
			      guint64 length,
			      MarlinUndoContext *ctxt,
			      GError **error)
{
	MarlinChannel *channel;
	int i;
	gboolean ret;
	MarlinUndoable *undo;
	struct _sample_closure *isc;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	isc = g_new (struct _sample_closure, 1);
	isc->sample = sample;

	undo = marlin_undoable_new (insert_silence_undo,
				    insert_silence_undo,
				    insert_silence_destroy,
				    isc);

	marlin_undo_context_add (ctxt, undo);

	WRITE_LOCK (sample->priv->lock);

	for (i = 0; i < sample->priv->channels; i++) {
		ret = marlin_channel_insert_silence (sample->priv->channel_data->pdata[i],
						     operation, position,
						     length, ctxt, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}
	}

	marlin_marker_model_move_markers_after (sample->priv->markers,
						position, (gint64) length, ctxt);
	WRITE_UNLOCK (sample->priv->lock);

	isc = g_new (struct _sample_closure, 1);
	isc->sample = sample;

	undo = marlin_undoable_new (insert_silence_undo,
				    insert_silence_undo,
				    insert_silence_destroy,
				    isc);

	marlin_undo_context_add (ctxt, undo);

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);

	return TRUE;
}

struct _remove_channel_closure {
	MarlinSample *sample;
	MarlinChannel *channel;
};

static void
remove_channel_undo (gpointer data)
{
	struct _remove_channel_closure *c = data;
	MarlinChannel *channel;
	gboolean ret;

	g_object_set (G_OBJECT (c->sample),
		      "channels", 2,
		      NULL);

	/* The newly added channel is always the second one */
	channel = c->sample->priv->channel_data->pdata[1];
	ret = marlin_channel_copy_data (c->channel, channel, 0,
					c->channel->frames - 1, NULL);
	if (ret == FALSE) {
		g_warning ("marlin_channel_copy_data failed");
	}

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
}

static void
remove_channel_redo (gpointer data)
{
	struct _remove_channel_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "channels", 1,
		      "dirty", TRUE,
		      NULL);
}

static void
remove_channel_destroy (gpointer data)
{
	struct _remove_channel_closure *c = data;

	marlin_channel_free (c->channel);
	g_free (c);
}

/**
 * marlin_sample_remove_channel:
 * @sample: A #MarlinSample
 * @channel: Position of the channel to be removed
 * @mix: Whether to mix the channels together.
 * @operation:
 * @ctxt:
 * @error:
 *
 * Removes the channel indicated by @channel from @sample. If @mix is TRUE
 * then the data in @channel is mixed into the remaining channel.
 *
 * Returns: TRUE on success, FALSE on error with further details in @error.
 */
gboolean
marlin_sample_remove_channel (MarlinSample *sample,
			      MarlinChannelPosition channel,
			      gboolean mix,
			      MarlinOperation *operation,
			      MarlinUndoContext *ctxt,
			      GError **error)
{
	struct _remove_channel_closure *c;
	MarlinUndoable *u;
	MarlinChannel *chan;
	gboolean ret;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	WRITE_LOCK (sample->priv->lock);

	if (sample->priv->channels == 1) {
		WRITE_UNLOCK (sample->priv->lock);
		return TRUE;
	}

	if (channel == MARLIN_CHANNEL_LEFT) {
		WRITE_UNLOCK (sample->priv->lock);
		/* Swap the channels around */
		ret = marlin_sample_swap_channels (sample, operation, ctxt, error);

		if (ret == FALSE) {
			return FALSE;
		}

		WRITE_LOCK (sample->priv->lock);
	}

	if (mix) {
		MarlinChannel *src, *dest;

		dest = sample->priv->channel_data->pdata[MARLIN_CHANNEL_LEFT];
		src = sample->priv->channel_data->pdata[MARLIN_CHANNEL_RIGHT];

		ret = marlin_channel_mix (dest, src, 0.5, 0.5,
					  (guint64) 0, src->frames, FALSE,
					  operation, ctxt, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}
	}

	WRITE_UNLOCK (sample->priv->lock);

	if (ctxt) {
		c = g_new (struct _remove_channel_closure, 1);
		c->sample = sample;

		/* Create a new channel and copy the old one */
		chan = sample->priv->channel_data->pdata[1];
		c->channel = marlin_channel_new (sample->priv->filename, NULL);
		marlin_channel_copy_data (chan, c->channel, 0, chan->frames - 1, NULL);
		u = marlin_undoable_new (remove_channel_undo,
					 remove_channel_redo,
					 remove_channel_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	/* Can't be dirty if there's no samples */
	if (sample->priv->number_of_frames == 0) {
		g_object_set (G_OBJECT (sample),
			      "channels", 1,
			      NULL);
	} else {
		g_object_set (G_OBJECT (sample),
			      "channels", 1,
			      "dirty", TRUE,
			      NULL);
	}

	return TRUE;
}

struct _add_channel_closure {
	MarlinSample *sample;
	MarlinChannelPosition channel;
	gboolean clone;
};

static void
add_channel_undo (gpointer data)
{
	struct _add_channel_closure *c = data;

	marlin_sample_remove_channel (c->sample, c->channel, FALSE,
				      NULL, NULL, NULL);
}

static void
add_channel_redo (gpointer data)
{
	struct _add_channel_closure *c = data;

	marlin_sample_add_channel (c->sample, c->channel, c->clone,
				   NULL, NULL, NULL);
}

static void
add_channel_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_add_channel:
 * @sample: A #MarlinSample
 * @channel: The position of the channel to be added
 * @clone: Whether to clone the data from the original channel
 * @operation: A #MarlinOperation
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Adds a channel to @sample in the position indicated by @channel. If @clone
 * is TRUE, then the data from the other channel is cloned in the new channel.
 *
 * Return value: TRUE on success, FALSE on error, with extra details in @error.
 */
gboolean
marlin_sample_add_channel (MarlinSample *sample,
			   MarlinChannelPosition channel,
			   gboolean clone,
			   MarlinOperation *operation,
			   MarlinUndoContext *ctxt,
			   GError **error)
{
	gboolean ret;
	struct _add_channel_closure *c = NULL;
	MarlinUndoable *u;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	if (sample->priv->channels == 2) {
		return TRUE;
	}

	/* Add our new channel */
	g_object_set (G_OBJECT (sample),
		      "channels", 2,
		      NULL);

	if (ctxt) {
		c = g_new (struct _add_channel_closure, 1);
		c->sample = sample;
		c->clone = clone;
		c->channel = channel;

		u = marlin_undoable_new (add_channel_undo,
					 add_channel_redo,
					 add_channel_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_LOCK (sample->priv->lock);

	if (clone == FALSE) {
		MarlinChannel *src;

		src = sample->priv->channel_data->pdata[MARLIN_CHANNEL_LEFT];

		/* Fill the new channel with 0 data */
		ret = marlin_channel_insert_silence (sample->priv->channel_data->pdata[MARLIN_CHANNEL_RIGHT],
						     operation, (guint64) 0,
						     src->frames, NULL, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		WRITE_UNLOCK (sample->priv->lock);

		if (channel == MARLIN_CHANNEL_LEFT) {
			ret = marlin_sample_swap_channels (sample, operation, NULL, error);
			if (ret == FALSE) {
				return FALSE;
			}
		}
	} else {
		MarlinChannel *src, *dest;

		src = sample->priv->channel_data->pdata[MARLIN_CHANNEL_LEFT];
		dest = sample->priv->channel_data->pdata[MARLIN_CHANNEL_RIGHT];

		ret = marlin_channel_copy_data (src, dest, (guint64) 0, src->frames - 1, error);

		WRITE_UNLOCK (sample->priv->lock);
		if (ret == FALSE) {
			return FALSE;
		}
	}

	/* Can't be dirty if there's no data */
	if (sample->priv->number_of_frames != 0) {
		g_object_set (G_OBJECT (sample),
			      "dirty", TRUE,
			      NULL);
	}

	return TRUE;
}

/**
 * marlin_sample_read_lock
 * sample: A #MarlinSample
 *
 * Locks @sample for reading.
 */
void
marlin_sample_read_lock (MarlinSample *sample)
{
	g_return_if_fail (IS_MARLIN_SAMPLE (sample));

	READ_LOCK (sample->priv->lock);
}

/**
 * marlin_sample_read_unlock
 * sample: A #MarlinSample
 *
 * Unlocks @sample
 */
void
marlin_sample_read_unlock (MarlinSample *sample)
{
	g_return_if_fail (IS_MARLIN_SAMPLE (sample));

	READ_UNLOCK (sample->priv->lock);
}

/**
 * marlin_sample_split_on_markers:
 * @sample: A #MarlinSample
 * @error: A #GError
 *
 * Splits @sample into many smaller #MarlinSample objects with the ranges of
 * these samples taken from marker positions of @sample.
 *
 * Return value: A GList containing #MarlinSamples.
 */
GList *
marlin_sample_split_on_markers (MarlinSample *sample,
				GError **error)
{
	GList *samples = NULL, *markers, *m;
	MarlinSample *ns;
	MarlinRange range;

	g_object_get (G_OBJECT (sample->priv->markers),
		      "markers", &markers,
		      NULL);

	range.start = 0;
	range.coverage = MARLIN_COVERAGE_BOTH;

	for (m = g_list_reverse (markers); m; m = m->next) {
		MarlinMarker *marker = m->data;

		range.finish = marker->position - 1;
		ns = marlin_sample_new_from_sample_with_range (sample, &range,
							       error);
		if (ns == NULL) {
			g_warning ("marlin_sample_new_from_sample_with_range failed");
			return NULL;
		}

		samples = g_list_prepend (samples, ns);

		g_object_set (G_OBJECT (ns),
			      "name", marker->name,
			      "dirty", TRUE,
			      NULL);

		range.start = marker->position;
	}

	/* There is one more to split */
	range.finish = sample->priv->number_of_frames - 1;
	ns = marlin_sample_new_from_sample_with_range (sample, &range, error);
	if (ns == NULL) {
		g_warning ("marlin_sample_new_from_sample_with_range_failed");
		return NULL;
	}

	samples = g_list_prepend (samples, ns);
	return g_list_reverse (samples);
}

static void
fade_undo (gpointer closure)
{
	struct _sample_closure *c = closure;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
fade_destroy (gpointer closure)
{
	g_free (closure);
}

static double
sample_fade_func (guint64 position,
		  gpointer data)
{
	MarlinSampleFade *fade = data;

	if (position <= fade->fade_start) {
		return fade->in_level;
	} else if (position >= fade->fade_end) {
		return fade->out_level;
	} else {
		float m;

		/*
		   y = mx+c
		   x = (position - fade_start)
		   c = fade->in_level

		   m = y2 - y1 / x2 - x1
		   y2 = out_level
		   y1 = in_level
		   x2 = fade_end
		   x1 = fade_start
		*/

		m = (fade->out_level - fade->in_level) / (((float) fade->fade_end) - ((float) fade->fade_start));

		return m * (position - fade->fade_start) + fade->in_level;
	}
}

/**
 * marlin_sample_fade:
 * sample: A #MarlinSample
 * fade: A #MarlinSampleFade
 * operation: A #MarlinOperation
 * ctxt: A #MarlinUndoContext
 * error: A #GError
 *
 * Applies @fade to @sample.
 *
 * Progress can be reported via @operation or this can be NULL if no reporting
 * is required.
 *
 * Return value: TRUE on success, FALSE on error with further details in @error.
 */
gboolean
marlin_sample_fade (MarlinSample *sample,
		    MarlinSampleFade *fade,
		    MarlinOperation *operation,
		    MarlinUndoContext *ctxt,
		    GError **error)
{
	MarlinUndoable *u;
	struct _sample_closure *c;
	gboolean ret;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = fade->fade_start;
	c->range.finish = fade->fade_end;
	c->range.coverage = MARLIN_COVERAGE_BOTH;

	u = marlin_undoable_new (fade_undo,
				 fade_undo,
				 fade_destroy,
				 c);
	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	for (i = 0; i < sample->priv->channels; i++) {
		ret = marlin_channel_fade (sample->priv->channel_data->pdata[i],
					   fade->fade_start, fade->fade_end,
					   (MarlinFadeFunc) sample_fade_func,
					   fade, operation, ctxt, error);
		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}
	}

	WRITE_UNLOCK (sample->priv->lock);
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);

	marlin_sample_data_changed (sample, &c->range);
	return TRUE;
}

static void
crossfade_undo (gpointer closure)
{
	struct _sample_closure *isc = closure;
	MarlinSample *sample = isc->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);
}

static void
crossfade_destroy (gpointer closure)
{
	g_free (closure);
}

/**
 * marlin_sample_crossfade:
 * @src:
 * @dest:
 * @src_fade:
 * @dest_fade:
 * @operation:
 * @ctxt:
 * @error:
 *
 */
gboolean
marlin_sample_crossfade (MarlinSample      *src,
			 MarlinSample      *dest,
			 MarlinSampleFade  *src_fade,
			 MarlinSampleFade  *dest_fade,
			 MarlinOperation   *operation,
			 MarlinUndoContext *ctxt,
			 GError           **error)
{
	MarlinSamplePrivate *src_priv, *dest_priv;
	MarlinChannel *channel;
	struct _sample_closure *c = NULL;
	MarlinUndoable *u;
	MarlinRange src_range, dest_range;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), FALSE);
	g_return_val_if_fail (IS_MARLIN_SAMPLE (dest), FALSE);

	src_priv = src->priv;
	dest_priv = dest->priv;

	if (src_priv->channels != dest_priv->channels) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH,
					      _("The number of channels on the src and destination samples did not match"));
		}

		return FALSE;
	}

	if (ctxt) {
		c = g_new (struct _sample_closure, 1);
		c->sample = dest;

		u = marlin_undoable_new (crossfade_undo,
					 NULL,
					 crossfade_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	READ_LOCK (src_priv->lock);
	WRITE_LOCK (dest_priv->lock);

	src_range.start = src_fade->fade_start;
	src_range.finish = src_fade->fade_end;
	dest_range.start = dest_fade->fade_start;
	dest_range.finish = dest_fade->fade_end;

	for (i = 0; i < src_priv->channels; i++) {
		gboolean ret;
		ret = marlin_channel_crossfade (src_priv->channel_data->pdata[i],
						dest_priv->channel_data->pdata[i],
						&src_range, &dest_range,
						(MarlinFadeFunc) sample_fade_func,
						src_fade,
						(MarlinFadeFunc) sample_fade_func,
						dest_fade, operation,
						ctxt, error);

		if (ret == FALSE) {
			WRITE_UNLOCK (dest_priv->lock);
			READ_UNLOCK (src_priv->lock);
			return FALSE;
		}
	}

	WRITE_UNLOCK (dest_priv->lock);
	READ_UNLOCK (src_priv->lock);

	if (ctxt) {
		u = marlin_undoable_new (NULL,
					 crossfade_undo,
					 NULL,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	channel = dest->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (dest),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);

	return TRUE;
}

/**
 * marlin_sample_paste_with_crossfade:
 */
gboolean
marlin_sample_paste_with_crossfade (MarlinSample      *dest,
				    MarlinSample      *src,
				    guint64            insert_position,
				    MarlinRange       *range,
				    guint              fade_length,
				    MarlinOperation   *operation,
				    MarlinUndoContext *ctxt,
				    GError           **error)
{
	guint src_channels, dest_channels, i;
	guint fade_frames;
	float fpms;
	MarlinChannel *channel;
/* 	struct _sample_closure *c = NULL; */
/* 	MarlinUndoable *u; */

	g_return_val_if_fail (IS_MARLIN_SAMPLE (src), FALSE);
	g_return_val_if_fail (IS_MARLIN_SAMPLE (dest), FALSE);

	src_channels = src->priv->channels;
	dest_channels = dest->priv->channels;

	if (src_channels != dest_channels) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH,
					      _("The number of channels on the source and destination samples did not match"));
		}

		return FALSE;
	}

	READ_LOCK (src->priv->lock);
	WRITE_LOCK (dest->priv->lock);

	/* create undoable */

	fpms = (float) dest->priv->sample_rate / 1000.0f;
	fade_frames = fpms * fade_length;

	for (i = 0; i < src_channels; i++) {
		MarlinChannel *s_channel = src->priv->channel_data->pdata[i];
		MarlinChannel *d_channel = dest->priv->channel_data->pdata[i];

		marlin_channel_insert_with_crossfade (d_channel, s_channel,
						      range, insert_position,
						      fade_frames,
						      operation, ctxt, error);
	}

	/* create undoable */

	channel = dest->priv->channel_data->pdata[0];

	g_object_set (G_OBJECT (dest),
		      "total_frames", channel->frames,
		      "dirty", TRUE,
		      NULL);

	return TRUE;
}

/**
 * marlin_sample_next_zero:
 * @sample: A #MarlinSample to search
 * @position: Start position of the search
 * @coverage: Coverage  of the search.
 *
 * Find the next zero crossing after @position
 *
 * Return value: The next position or @position if it is the last.
 */
guint64
marlin_sample_next_zero (MarlinSample *sample,
			 guint64 position,
			 MarlinCoverage coverage)
{
	MarlinSamplePrivate *priv = sample->priv;
	guint64 pos, new_pos;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), position);

	switch (coverage) {
	case MARLIN_COVERAGE_BOTH:
		pos = position;
		for (i = 0; i < priv->channels; i++) {
			new_pos = marlin_channel_next_zero (priv->channel_data->pdata[i],
							    position);

			if (pos == position) {
				pos = new_pos;
			} else {
				pos = MIN (new_pos, pos);
			}
		}

		break;

	case MARLIN_COVERAGE_LEFT:
		pos = marlin_channel_next_zero (priv->channel_data->pdata[0],
						position);
		break;

	case MARLIN_COVERAGE_RIGHT:
		pos = marlin_channel_next_zero (priv->channel_data->pdata[1],
						position);
		break;

	default:
		pos = position;
		break;
	}

	return pos;
}

/**
 * marlin_sample_previous_zero:
 * @sample: A #MarlinSample to search
 * @position: Start position of the search
 * @coverage: Coverage of the search
 *
 * Find the previous zer0 crossing before @position
 *
 * Return value: The previous position or @position if it is the first.
 */
guint64
marlin_sample_previous_zero (MarlinSample *sample,
			     guint64 position,
			     MarlinCoverage coverage)
{
	MarlinSamplePrivate *priv = sample->priv;
	guint64 pos, new_pos;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), position);

	switch (coverage) {
	case MARLIN_COVERAGE_BOTH:
		pos = position;
		for (i = 0; i < priv->channels; i++) {
			new_pos = marlin_channel_previous_zero (priv->channel_data->pdata[i],
								position);
			if (new_pos != position) {
				pos = MIN (new_pos, pos);
			}
		}

		g_print ("Found zero at: %llu\n", pos);
		break;

	case MARLIN_COVERAGE_LEFT:
		pos = marlin_channel_previous_zero (priv->channel_data->pdata[0],
						    position);
		break;

	case MARLIN_COVERAGE_RIGHT:
		pos = marlin_channel_previous_zero (priv->channel_data->pdata[1],
						    position);
		break;

	default:
		pos = position;
		break;
	}

	return pos;
}

static void
reverse_range_undo (gpointer data)
{
	struct _sample_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
reverse_range_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_reverse_range:
 * @sample: A #MarlinSample
 * @operation: A #MarlinOperation
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 *
 * Reverses the data specified by @range in @sample.
 *
 * Progress can be reported via @operation, or this can be NULL if no progress
 * reporting is required.
 *
 * This operation can be undone by passing a #MarlinUndoContext to @ctxt.
 *
 * Return value: TRUE on success, FALSE on failure
 */
gboolean
marlin_sample_reverse_range (MarlinSample *sample,
			     MarlinOperation *operation,
			     MarlinRange *range,
			     MarlinUndoContext *ctxt,
			     GError **error)
{
	struct _sample_closure *c;
	MarlinUndoable *u;
	int i;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = range->start;
	c->range.finish = range->finish;
	c->range.coverage = range->coverage;

	u = marlin_undoable_new (reverse_range_undo,
				 reverse_range_undo,
				 reverse_range_destroy,
				 c);

	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < sample->priv->channels; i++) {
			marlin_channel_reverse_range (sample->priv->channel_data->pdata[i],
						      range->start,
						      range->finish,
						      operation, ctxt, error);
		}
		break;

	case MARLIN_COVERAGE_LEFT:
		marlin_channel_reverse_range (sample->priv->channel_data->pdata[0],
					      range->start, range->finish,
					      operation, ctxt, error);
		break;

	case MARLIN_COVERAGE_RIGHT:
		if (sample->priv->channels != 2) {
			g_warning ("Editting right channel on mono sample?");
			WRITE_UNLOCK (sample->priv->lock);

			return FALSE;
		}

		marlin_channel_reverse_range (sample->priv->channel_data->pdata[1],
					      range->start, range->finish,
					      operation, ctxt, error);
		break;

	default:
		break;
	}

	WRITE_UNLOCK (sample->priv->lock);

	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);

	marlin_sample_data_changed (sample, range);

	return TRUE;
}

static void
expand_range_undo (gpointer closure)
{
	struct _sample_closure *isc = closure;
	MarlinSample *sample = isc->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);
}

static void
expand_range_destroy (gpointer closure)
{
	g_free (closure);
}

/**
 * marlin_sample_expand_range:
 * @sample:
 * @range:
 * new_length:
 * operation:
 * ctxt:
 * error:
 *
 */
gboolean
marlin_sample_expand_range (MarlinSample      *sample,
			    MarlinRange       *range,
			    guint64            new_length,
			    MarlinOperation   *operation,
			    MarlinUndoContext *ctxt,
			    GError           **error)
{
	MarlinSamplePrivate *priv;
	MarlinChannel *channel;
	MarlinSoundtouch *soundtouch;
	struct _sample_closure *c = NULL;
	MarlinUndoable *u;
	guint64 old_length;
	gboolean ret;
	int i;

	g_return_val_if_fail (sample != NULL, FALSE);

	priv = sample->priv;

	if (ctxt) {
		c = g_new (struct _sample_closure, 1);
		c->sample = sample;

		u = marlin_undoable_new (expand_range_undo,
					 NULL,
					 expand_range_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_LOCK (priv->lock);

	old_length = (range->finish - range->start) + 1;

	soundtouch = marlin_soundtouch_new ();
	marlin_soundtouch_set_channels (soundtouch, 1);
	marlin_soundtouch_set_sample_rate (soundtouch, priv->sample_rate);
	marlin_soundtouch_set_tempo (soundtouch,
				     ((float)old_length) / (float)new_length);
	for (i = 0; i < priv->channels; i++) {
		ret = marlin_channel_expand_range (priv->channel_data->pdata[i],
						   soundtouch, range->start,
						   range->finish, new_length,
						   operation, ctxt, error);
		if (ret) {
			/* FIXME: We should pop off the last undoable
			   otherwise it will screwup the undo system */
			WRITE_UNLOCK (priv->lock);
			return FALSE;
		}

		marlin_soundtouch_clear (soundtouch);
	}

	marlin_soundtouch_free (soundtouch);

	if (ctxt) {
		u = marlin_undoable_new (NULL,
					 expand_range_undo,
					 NULL,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_UNLOCK (priv->lock);

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);

	marlin_sample_data_changed (sample, range);

	return TRUE;
}

static void
expand_mix_undo (gpointer closure)
{
	struct _sample_closure *isc = closure;
	MarlinSample *sample = isc->sample;
	MarlinChannel *channel;

	channel = sample->priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);
}

static void
expand_mix_destroy (gpointer closure)
{
	g_free (closure);
}

/**
 * marlin_sample_expand_mix:
 */
gboolean
marlin_sample_expand_mix (MarlinSample      *src,
			  MarlinSample      *dest,
			  MarlinRange       *src_range,
			  MarlinRange       *dest_range,
			  double             src_db,
			  double             dest_db,
			  MarlinOperation   *operation,
			  MarlinUndoContext *ctxt,
			  GError           **error)
{
	MarlinSamplePrivate *src_priv, *dest_priv;
	MarlinChannel *channel;
	MarlinSoundtouch *soundtouch;
	struct _sample_closure *c = NULL;
	MarlinUndoable *u;
	gboolean ret;
	int i;

	g_return_val_if_fail (src != NULL, FALSE);
	g_return_val_if_fail (dest != NULL, FALSE);

	src_priv = src->priv;
	dest_priv = dest->priv;

	if (src_priv->channels != dest_priv->channels) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH,
					      _("The number of channels on the src and destination samples did not match"));
		}

		return FALSE;
	}

	if (src_priv->sample_rate != dest_priv->sample_rate) {
		if (error) {
			*error = g_error_new (MARLIN_SAMPLE_ERROR,
					      MARLIN_SAMPLE_ERROR_RATE_MISMATCH,
					      _("The sample rate of the src and destination samples did not match"));
		}

		return FALSE;
	}

	if (ctxt) {
		c = g_new (struct _sample_closure, 1);
		c->sample = dest;

		u = marlin_undoable_new (expand_mix_undo,
					 NULL,
					 expand_mix_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_LOCK (dest_priv->lock);
	READ_LOCK (src_priv->lock);

	soundtouch = marlin_soundtouch_new ();
	marlin_soundtouch_set_channels (soundtouch, 1);
	marlin_soundtouch_set_sample_rate (soundtouch, dest_priv->sample_rate);

	for (i = 0; i < dest_priv->channels; i++) {
		ret = marlin_channel_expand_mix (src_priv->channel_data->pdata[i],
						 dest_priv->channel_data->pdata[i],
						 soundtouch,
						 src_range->start,
						 src_range->finish,
						 dest_range->start,
						 dest_range->finish,
						 src_db, dest_db,
						 operation, ctxt, error);
		if (ret == FALSE) {
			/* FIXME: We should pop off the last undoable
			   otherwise it will screwup the undo system */
			READ_UNLOCK (src_priv->lock);
			WRITE_UNLOCK (dest_priv->lock);

			return FALSE;
		}

		marlin_soundtouch_clear (soundtouch);
	}

	marlin_soundtouch_free (soundtouch);

	if (ctxt) {
		u = marlin_undoable_new (NULL,
					 expand_mix_undo,
					 NULL,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_UNLOCK (dest_priv->lock);
	READ_UNLOCK (src_priv->lock);

	channel = dest_priv->channel_data->pdata[0];
	g_object_set (G_OBJECT (dest),
		      "dirty", TRUE,
		      "total_frames", channel->frames,
		      NULL);

	marlin_sample_data_changed (dest, dest_range);

	return TRUE;
}

static void
normalize_undo (gpointer data)
{
	struct _sample_closure *c = data;

	g_object_set (G_OBJECT (c->sample),
		      "dirty", TRUE,
		      NULL);
	marlin_sample_data_changed (c->sample, &(c->range));
}

static void
normalize_destroy (gpointer data)
{
	g_free (data);
}

/**
 * marlin_sample_normalize_range:
 * @sample: A #MarlinSample
 * @db: The volume to be set
 * @range: A #MarlinRange
 * @ctxt: A #MarlinUndoContext
 * @error: A #GError
 *
 * Adjusts the volume of @sample to @db, within @range.
 *
 * Return value: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_sample_normalize_range (MarlinSample *sample,
			       float db,
			       MarlinRange *range,
			       MarlinUndoContext *ctxt,
			       GError **error)
{
	int i;
	gboolean ret;
	struct _sample_closure *c;
	MarlinUndoable *u;

	g_return_val_if_fail (IS_MARLIN_SAMPLE (sample), FALSE);

	c = g_new (struct _sample_closure, 1);
	c->sample = sample;
	c->range.start = range->start;
	c->range.finish = range->finish;
	c->range.coverage = range->coverage;

	u = marlin_undoable_new (normalize_undo,
				 normalize_undo,
				 normalize_destroy,
				 c);

	marlin_undo_context_add (ctxt, u);

	WRITE_LOCK (sample->priv->lock);

	switch (range->coverage) {
	case MARLIN_COVERAGE_BOTH:
		for (i = 0; i < sample->priv->channels; i++) {
			ret = marlin_channel_normalize
				(sample->priv->channel_data->pdata[i],
				 range, db, NULL, ctxt, error);

			if (ret == FALSE) {
				WRITE_UNLOCK (sample->priv->lock);
				return FALSE;
			}
		}
		break;

	case MARLIN_COVERAGE_LEFT:
		ret = marlin_channel_normalize
			(sample->priv->channel_data->pdata[0],
			 range, db, NULL, ctxt, error);

		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	case MARLIN_COVERAGE_RIGHT:
		if (sample->priv->channels != 2) {
			g_warning ("Attempting to adjust volume on RIGHT in a mono sample.");
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		ret = marlin_channel_normalize
			(sample->priv->channel_data->pdata[1],
			 range, db, NULL, ctxt, error);

		if (ret == FALSE) {
			WRITE_UNLOCK (sample->priv->lock);
			return FALSE;
		}

		break;

	default:
		break;
	}

	WRITE_UNLOCK (sample->priv->lock);

	g_object_set (G_OBJECT (sample),
		      "dirty", TRUE,
		      NULL);

	marlin_sample_data_changed (sample, range);

	return TRUE;
}

/**
 * marlin_sample_data_changed:
 * @sample: A #MarlinSample
 * @range: A #MarlinRange
 *
 * Emits the data changed signal on @sample with @range.
 */
void
marlin_sample_data_changed (MarlinSample *sample,
			    MarlinRange *range)
{
	g_signal_emit (sample, signals[DATA_CHANGED], 0, range);
}

/**
 * marlin_sample_frame_count_changed:
 * @sample: #MarlinSample.
 *
 * Updates the frame count of the sample. Checks all the channels have the same
 * number of frames. Warns if they don't, and takes the smallest number.
 */
void
marlin_sample_frame_count_changed (MarlinSample *sample)
{
	MarlinSamplePrivate *priv;
	int i;
	guint64 frames;

	priv = sample->priv;

	WRITE_LOCK (priv->lock);

	frames = 0;
	for (i = 0; i < priv->channel_data->len; i++) {
		MarlinChannel *channel = priv->channel_data->pdata[i];

		if (i > 0) {
			/* Check all the frames are equal
			   - if not, take the min */
			if (frames != channel->frames) {
				g_warning ("Frame mismatch: %llu vs %llu (%d)\n",
					   frames, channel->frames, i);
			} else {
				frames = MIN (frames, channel->frames);
			}
		} else {
			frames = channel->frames;
		}
	}

	priv->number_of_frames = frames;

	WRITE_UNLOCK (priv->lock);
}

gboolean
marlin_sample_process_ladspa (MarlinSample *sample,
			       LADSPA_Descriptor *ladspa,
			       MarlinRange       *range,
			       MarlinOperation   *operation,
			       MarlinUndoContext *ctxt,
			       GError           **error)
{
#ifdef HAVE_LADSPA
	return TRUE;
#else
	return TRUE;
#endif
}
