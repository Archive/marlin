/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PIPELINE_H__
#define __MARLIN_PIPELINE_H__

#include <glib-object.h>

#include <gst/gst.h>

#include <marlin/marlin-operation.h>

#define MARLIN_PIPELINE_TYPE (marlin_pipeline_get_type ())
#define MARLIN_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_PIPELINE_TYPE, MarlinPipeline))
#define MARLIN_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_PIPELINE_TYPE, MarlinPipelineClass))
#define IS_MARLIN_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_PIPELINE_TYPE))
#define IS_MARLIN_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_PIPELINE_TYPE))
#define MARLIN_PIPELINE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), MARLIN_PIPELINE_TYPE, MarlinPipelineClass))

typedef struct _MarlinPipeline MarlinPipeline;
typedef struct _MarlinPipelineClass MarlinPipelineClass;
typedef struct _MarlinPipelinePrivate MarlinPipelinePrivate;

struct _MarlinPipeline {
	GstPipeline pipeline;

	MarlinPipelinePrivate *priv;
};

struct _MarlinPipelineClass {
	GstPipelineClass parent_class;

	/* vtable */
	guint64 (* report_position) (MarlinPipeline *pipeline);
};

GType marlin_pipeline_get_type (void);

MarlinOperation *marlin_pipeline_get_operation (MarlinPipeline *pipeline);

#endif
