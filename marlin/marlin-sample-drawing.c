/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <marlin/marlin-sample-drawing.h>
#include <marlin/marlin-sample-selection.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-block.h>

#undef LOCK_DEBUG
#include <marlin/marlin-read-write-lock.h>

static gboolean
get_min_max_values (MarlinChannel *channel,
		    guint64        start_frame,
		    float         *min_value,
		    float         *max_value,
		    MarlinBlock  **last_block)
{
	guint64 offset_in_block;
	MarlinBlock *block;
	float *frame_data;

	/* When frames_per_pixel == 1,
	   we draw current frame -1 to current frame */

	/* If start_frame - 1 is in the previous block,
	   fetch it and get the correct data. If this is the
	   first block, then just go to the next frame */
	
	if (*last_block == NULL) {
		block = marlin_channel_get_block_for_frame (channel,
							    start_frame);
	} else if (start_frame >= (*last_block)->start &&
		   start_frame <= (*last_block)->end) {
		/* We're still in the same block */
		block = *last_block;
	} else {
		/* We're in a different block */
		block = marlin_channel_get_block_for_frame (channel,
							    start_frame);
	}
	
	if (block == NULL) {
		return FALSE;
	}
	
	READ_LOCK (block->lock);

	WRITE_LOCK (channel->lock);
	offset_in_block = start_frame - block->start;
	frame_data = marlin_block_get_frame_data (block);
	*min_value = frame_data[offset_in_block];

	if (offset_in_block == 0) {
		MarlinBlock *tmp, *prev;

		/* We need the previous block */
		prev = block->previous;

		if (prev == NULL) {
			WRITE_UNLOCK (channel->lock);

			READ_UNLOCK (block->lock);
			return FALSE;
		} 

		tmp = prev;

		READ_LOCK (tmp->lock);
		frame_data = marlin_block_get_frame_data (tmp);
		*max_value = frame_data[tmp->end];
		READ_UNLOCK (tmp->lock);
	} else {
		*max_value = frame_data[offset_in_block - 1];
	}

	*last_block = block;

	WRITE_UNLOCK (channel->lock);

	READ_UNLOCK (block->lock);

	return TRUE;
}

/* Turning off peak usage makes the sample view slower to redraw 
   but is sometimes useful for debugging */
#define USE_PEAKS
/* #undef USE_PEAKS */

static MarlinBlock *
get_peaks_from_frame_data (MarlinChannel *channel,
			   MarlinBlock   *block,
			   guint64        start_frame,
			   int            frames_per_pixel,
			   MarlinPeak    *peak)
{
	guint64 o;
	int i, total_positive, total_negative;
	float *frame_data;
	
	frame_data = marlin_block_get_frame_data (block);
	
	/* Scan through the real sample data to get the peaks */
	total_positive = 0;
	total_negative = 0;
	
	/* Scan through until the end */
	for (i = 0, o = start_frame; 
	     o < channel->frames && i < frames_per_pixel; 
	     ++i, ++o) {
		guint64 frame_in_block;
		
		if (o > block->end) {
			MarlinBlock *old_block = block;
			/* Get the next block and reset o to the
			   first frame in the block */
			READ_UNLOCK (block->lock);
			
			block = marlin_block_next (block);
			if (block == NULL) {
				g_print ("\n\nInternal error getting next block\n\n"
					 "block: %p\nblock->start: %llu\nblock->end: %llu\nblock->num_frames: %llu\no: %llu\ni: %d\nchannel->frames: %llu\n\n",
					 old_block, old_block->start, old_block->end,
					 old_block->num_frames, o, i, channel->frames);
				g_assert_not_reached ();
			}
			
			READ_LOCK (block->lock);
			
			o = block->start;
			frame_data = marlin_block_get_frame_data (block);
		}
		
		frame_in_block = o - block->start;
		peak->low = MIN (peak->low, 
				 (short) (frame_data[frame_in_block] * 256));
		peak->high = MAX (peak->high, 
				  (short) (frame_data[frame_in_block] * 256));
		if (frame_data[frame_in_block] >= 0.0) {
			peak->avg_positive += (short) (frame_data[frame_in_block] * 256);
			++total_positive;
		} else {
			peak->avg_negative += (short) (frame_data[frame_in_block] * 256);
			++total_negative;
		}
	}
	
	if (total_positive > 0) {
		peak->avg_positive /= total_positive;
	} else {
		peak->avg_positive = 0;
	}
	
	if (total_negative > 0) {
		peak->avg_negative /= total_negative;
	} else {
		peak->avg_negative = 0;
	}

	/* return the block we ended on */
	return block;
}
			
static MarlinBlock *
get_peaks_from_peak_data (MarlinChannel *channel,
			  MarlinBlock   *block,
			  guint64        start_frame,
			  int            frames_per_pixel,
			  MarlinPeak    *peak)
{
	MarlinPeak *peak_data;
	int i, start_peak, peak_range;
	guint64 o;
	
	peak_data = marlin_block_get_peak_data (block);
	
	start_peak = (start_frame - block->start) / MARLIN_FRAMES_PER_PEAK;
	peak_range = frames_per_pixel / MARLIN_FRAMES_PER_PEAK;
	
	for (i = 0, o = start_peak; i < peak_range; i++, o++) {
		guint64 peak_in_channel;
		MarlinPeak p;
		
		peak_in_channel = block->start + (o * MARLIN_FRAMES_PER_PEAK);
		if (peak_in_channel >= channel->frames) {
			break;
		}
		
		if (peak_in_channel > block->end) {
			/* Next block */
			READ_UNLOCK (block->lock);
			
			block = marlin_block_next (block);
			g_assert (block != NULL);
			
			READ_LOCK (block->lock);
			o = 0;
			peak_data = marlin_block_get_peak_data (block);
		}
		
		p = peak_data[o];
		
		peak->low = MIN (peak->low, p.low);
		peak->high = MAX (peak->high, p.high);
		peak->avg_positive += p.avg_positive;
		peak->avg_negative += p.avg_negative;
	}
	
	peak->avg_positive /= peak_range;
	peak->avg_negative /= peak_range;
	
	return block;
}

static void
get_min_max_peaks (MarlinChannel *channel,
		   guint64        start_frame,
		   int            frames_per_pixel,
		   MarlinPeak    *peak,
		   MarlinBlock  **last_block)
{
	MarlinBlock *block;

	peak->low = 0;
	peak->high = 0;
	peak->avg_positive = 0;
	peak->avg_negative = 0;

	if (*last_block == NULL) {
		block = marlin_channel_get_block_for_frame (channel,
							    start_frame);
	} else if (start_frame >= (*last_block)->start &&
		   start_frame <= (*last_block)->end) {
		/* We're still in the same block */
		block = *last_block;
	} else {
		/* We're in a different block */
		block = marlin_channel_get_block_for_frame (channel,
							    start_frame);
	}

	READ_LOCK (block->lock);

	WRITE_LOCK (channel->lock);
#ifdef USE_PEAKS
	if (frames_per_pixel < MARLIN_FRAMES_PER_PEAK) {
		block = get_peaks_from_frame_data (channel, block, start_frame,
						   frames_per_pixel, peak);
	} else {
		block = get_peaks_from_peak_data (channel, block, start_frame,
						  frames_per_pixel, peak);
	}
#else
	block = get_peaks_from_frame_data (channel, block, start_frame,
					   frames_per_pixel, peak);
#endif

	*last_block = block;
		
	WRITE_UNLOCK (channel->lock);

	READ_UNLOCK (block->lock);
}

#define YPOS(v) chan_area.y + chan_height - ((((v) - dc->vmin) * chan_height) / (dc->vmax - dc->vmin))

/**
 * marlin_sample_draw:
 * @dc:
 * @widget:
 * @area:
 * @state_type:
 * @channel_num:
 * @xofs:
 */
void
marlin_sample_draw (MarlinSampleDrawContext *dc,
		    GtkWidget *widget,
		    GdkRectangle *area,
		    GtkStateType state_type,
		    guint channel_num,
		    int xofs)
{
	MarlinSample *sample = dc->sample;
	MarlinChannel *channel;
	GdkRectangle chan_area;
	guint64 start_frame;
	int quart, half, i, num_channels;
	int chan_height, chan_offset, extra;
	static MarlinBlock *last_block = NULL;
	
	g_object_get (G_OBJECT (sample),
		      "channels", &num_channels,
		      NULL);

	extra = num_channels - 1;
	if (dc->height == 0) {
		chan_height = (widget->allocation.height - extra) / num_channels;
	} else {
		chan_height = dc->height;
	}
	chan_offset = (chan_height * channel_num) + channel_num;

	chan_area.x = 0;
	chan_area.y = (chan_height * channel_num) + channel_num;
	chan_area.width = widget->allocation.width;
	chan_area.height = chan_height;

	quart = (int) (chan_height / 4);
	half = quart * 2;

	start_frame = (xofs + area->x) * dc->fpp;

	channel = marlin_sample_get_channel (sample, channel_num);

	if (channel == NULL || marlin_channel_is_ready (channel) == FALSE) {
		return;
	}
	
	/* Reset the block before we go into the loop so that we're only
	   storing blocks over the course of the loop */
	last_block = NULL;
	for (i = 0; i < area->width; i++) {
		int xoff = 0;
		float y1, y2;
		float avg_pos, avg_neg;
		int sy1, sy2;
		gboolean draw_average;
		
		if (dc->fpp > 1) {
			MarlinPeak peak;

			get_min_max_peaks (channel, start_frame,
					   dc->fpp, &peak,
					   &last_block);
			y1 = (float) peak.low / 256.0;
			y2 = (float) peak.high / 256.0;
			avg_pos = (float) peak.avg_positive / 256.0;
			avg_neg = (float) peak.avg_negative / 256.0;

			draw_average = TRUE;
		} else {
			gboolean ret;

			draw_average = FALSE;

			ret = get_min_max_values (channel, start_frame, &y1, &y2, &last_block);
			if (ret == FALSE) {
				goto cont;
			}

			avg_neg = 0.0;
			avg_pos = 0.0;
			xoff = -1;
		}

		/* Drawing averages under this value just makes the sample
		   look like a mess */
		/* FIXME: Maybe make drawing of max/min or averages be a 
		   toggle option on the view? */
		if (dc->fpp < 512) {
			draw_average = FALSE;
		}

		/* Scale the line up and adjust for centre line */
		sy2 = (guint) YPOS (y2) + dc->base_offset;
		sy1 = (guint) YPOS (y1) + dc->base_offset;

		/* Draw the line */
  		gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], &chan_area);

		gdk_draw_line (dc->drawable,
			       widget->style->text_gc[state_type],
			       area->x + i + xoff, sy2,
			       area->x + i, sy1);

  		gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);

		if (draw_average && dc->draw_average) {
			sy2 = (guint) YPOS (avg_neg) + dc->base_offset;
			sy1 = (guint) YPOS (avg_pos) + dc->base_offset;

 			gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], &chan_area);
			
			gdk_draw_line (dc->drawable,
				       widget->style->dark_gc[state_type],
				       area->x + i + xoff, sy2,
				       area->x + i, sy1);
 			gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], NULL);
		}
	cont:
		start_frame += dc->fpp;
		if (start_frame >= channel->frames) {
			break;
		}
	}		
}

/* FIXME: Needs a better name */
/**
 * marlin_sample_draw_to_buffer:
 * @dc:
 * @buffer:
 * @range:
 */
void
marlin_sample_draw_to_buffer (MarlinSampleDrawContext *dc,
			      MarlinPeak **buffer,
			      GdkRectangle *area)
{
	int c, channels;

	if (dc->fpp == 1) {
		return;
	}

	g_object_get (G_OBJECT (dc->sample),
		      "channels", &channels,
		      NULL);

	for (c = 0; c < channels; ++c) {
		MarlinChannel *channel;
		MarlinBlock *last_block;
		MarlinPeak *peaks;
		int i, f;

		channel = marlin_sample_get_channel (dc->sample, c);

		if (channel == NULL || 
		    marlin_channel_is_ready (channel) == FALSE) {
			return;
		}

		last_block = NULL;
		i = 0;

		peaks = buffer[c];

		for (f = area->x; f < area->x + area->width; ++f, ++i) {
			guint64 frame;
			
			frame = f * dc->fpp;
			if (frame >= channel->frames) {
				peaks[i].high = 0;
				peaks[i].low = 0;
				peaks[i].avg_negative = 0;
				peaks[i].avg_positive = 0;

				continue;
			}

			get_min_max_peaks (channel, frame,
					   dc->fpp, &peaks[i], 
					   &last_block);
		}
	}
}

/**
 * marlin_sample_draw_buffer:
 */
void
marlin_sample_draw_buffer (MarlinSampleDrawContext *dc,
			   GtkWidget *widget,
			   MarlinPeak **buffer,
			   GdkRectangle *area)
{
	GdkRectangle chan_area;
	int chan_height, chan_offset, extra;
	int c, channels;

	g_object_get (G_OBJECT (dc->sample),
		      "channels", &channels,
		      NULL);

	extra = channels - 1;
	chan_height = (widget->allocation.height - extra) / channels;

	for (c = 0; c < channels; ++c) {
		int i;
		MarlinPeak *peaks = buffer[c];

		chan_offset = (chan_height * c) + c;
		
		chan_area.x = 0;
		chan_area.y = (chan_height * c) + c;
		chan_area.width = widget->allocation.width;
		chan_area.height = chan_height;
		
		for (i = area->x; i < area->width; ++i) {
			float y1, y2;
			int sy1, sy2;
			gboolean draw_average = TRUE;
			MarlinPeak peak = peaks[i];
				
			/* Drawing averages under this value 
			   just makes the sample look like a mess */
			/* FIXME: Maybe make drawing of max/min 
			   or averages be a toggle option on 
			   the view? */
			if (dc->fpp < 512) {
				draw_average = FALSE;
			}
			
			/* Scale the line up and adjust for centre line */
			y1 = ((float) peak.low) / 256.0;
			y2 = ((float) peak.high) / 256.0;
			sy2 = (guint) YPOS (y2) + dc->base_offset;
			sy1 = (guint) YPOS (y1) + dc->base_offset;

			/* Draw the line */
			gdk_draw_line (dc->drawable,
				       widget->style->text_gc[GTK_STATE_NORMAL],
				       i, sy2,
				       i, sy1);
			
			if (draw_average && dc->draw_average) {
				float avg_neg, avg_pos;

				avg_neg = ((float) peak.avg_negative) / 256.0;
				avg_pos = ((float) peak.avg_positive) / 256.0;

				sy2 = (guint) YPOS (avg_neg) + dc->base_offset;
				sy1 = (guint) YPOS (avg_pos) + dc->base_offset;
				
				gdk_draw_line (dc->drawable,
					       widget->style->dark_gc[GTK_STATE_NORMAL],
					       i, sy2,
					       i, sy1);
			}
		}		
	}
}

#define DEFAULT_VMAX 1.0
#define DEFAULT_VMIN -1.0

/**
 * marlin_sample_draw_context_new:
 * @sample:
 * @fpp:
 * @draw_average:
 *
 * Creates a new #MarlinSampleDrawContext.
 */
MarlinSampleDrawContext *
marlin_sample_draw_context_new (MarlinSample *sample,
				guint fpp,
				gboolean draw_average)
{
	MarlinSampleDrawContext *dc;

	dc = g_new0 (MarlinSampleDrawContext, 1);
	dc->sample = sample;
	dc->drawable = NULL;
	dc->fpp = fpp;
	dc->draw_average = draw_average;

	dc->base_offset = 0;
	dc->vmax = DEFAULT_VMAX;
	dc->vmin = DEFAULT_VMIN;

	return dc;
}

/**
 * marlin_sample_draw_context_free:
 * @dc:
 *
 * Frees all the resources used with @dc.
 */
void
marlin_sample_draw_context_free (MarlinSampleDrawContext *dc)
{
	g_free (dc);
}
