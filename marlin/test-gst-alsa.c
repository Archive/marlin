/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gst/gst.h>

GstElement *pipeline;
GMainLoop *mainloop;

static gboolean
pause_function (gpointer data)
{
	static gboolean pause = FALSE;

	if (pause == FALSE) {
		g_print ("Stopping pipeline.\n");
		gst_element_set_state (pipeline, GST_STATE_READY);
		pause = TRUE;
	} else {
		g_print ("Restarting pipeline\n");
		gst_element_set_state (pipeline, GST_STATE_PLAYING);
		pause = FALSE;
	}
}

static void
oneton_new_pad (GstElement *oneton,
		GstPad *src,
		GstPipeline *pipeline)
{
	GstPad *sink, *intsrc, *intsink;
	GstElement *i2f, *f2i;
	const char *name;
	char *ifname;

	g_print ("Making new pad\n");
/*	gst_element_set_state (pipeline, GST_STATE_PAUSED);*/

	name = gst_pad_get_name (src);
	ifname = g_strdup_printf ("intfloat.%s", name);
	i2f = gst_element_factory_make ("int2float", ifname);
	g_free (ifname);
	
	gst_bin_add (GST_BIN (pipeline), i2f);

	intsink = gst_element_get_pad (i2f, "sink");
	g_assert (intsink != NULL);

	gst_pad_connect (src, intsink);

	intsrc = gst_element_get_request_pad (i2f, "src%d");
	g_assert (intsrc != NULL);
	
	f2i = gst_bin_get_by_name (GST_BIN (pipeline), "f2i");
	sink = gst_element_get_request_pad (f2i, "sink%d");
	g_assert (sink != NULL);

	if (!gst_pad_connect (intsrc, sink)) {
		g_warning ("Error will robinson");
	}

/*	gst_element_set_state (pipeline, GST_STATE_PLAYING);*/
}

int
main (int argc,
      char **argv)
{
	GstElement *filesrc, *mad, *audiosink, *i2f, *f2i;
	GstElement *oneton;
	
	gst_init (&argc, &argv);

	pipeline = gst_thread_new ("pipeline");
	filesrc = gst_element_factory_make ("filesrc", "filesrc");
	g_object_set (G_OBJECT (filesrc),
		      "location", argv[1],
		      NULL);
	gst_bin_add (GST_BIN (pipeline), filesrc);

	mad = gst_element_factory_make ("mad", "mad");
	gst_bin_add (GST_BIN (pipeline), mad);

	oneton = gst_element_factory_make ("oneton", "oneton");
	gst_bin_add (GST_BIN (pipeline), oneton);
	
	f2i = gst_element_factory_make ("float2int", "f2i");
	gst_bin_add (GST_BIN (pipeline), f2i);
	
	g_signal_connect (G_OBJECT (oneton), "new-pad",
			  G_CALLBACK (oneton_new_pad), pipeline);
	audiosink = gst_element_factory_make (argv[2], "audiosink");
	gst_bin_add (GST_BIN (pipeline), audiosink);

	gst_element_connect (filesrc, mad);
	gst_element_connect (mad, oneton);
	gst_element_connect (f2i, audiosink);

	gst_element_set_state (pipeline, GST_STATE_PLAYING);

	mainloop = g_main_loop_new (NULL, FALSE);
	g_timeout_add (3*1000, pause_function, NULL);
	g_main_loop_run (mainloop);
}
