/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Notzed <notzed@ximian.com>
 *
 *  Copyright 2001, 2002 Ximian, Inc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <marlin-msgport.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <pthread.h>

#include <glib.h>

#define m(x) /* msgport debug */
#define t(x) /* thread debug */

void
marlin_dlist_init (MarlinDList *v)
{
	v->head = (MarlinDListNode *) &v->tail;
	v->tail = 0;
	v->tailpred = (MarlinDListNode *)&v->head;
}

MarlinDListNode *
marlin_dlist_addhead (MarlinDList *l,
		      MarlinDListNode *n)
{
	n->next = l->head;
	n->prev = (MarlinDListNode *)&l->head;
	l->head->prev = n;
	l->head = n;
	return n;
}

MarlinDListNode *
marlin_dlist_addtail (MarlinDList *l,
		      MarlinDListNode *n)
{
	n->next = (MarlinDListNode *)&l->tail;
	n->prev = l->tailpred;
	l->tailpred->next = n;
	l->tailpred = n;

	return n;
}

MarlinDListNode *
marlin_dlist_remove (MarlinDListNode *n)
{
	n->next->prev = n->prev;
	n->prev->next = n->next;
	return n;
}

MarlinDListNode *
marlin_dlist_remhead (MarlinDList *l)
{
	MarlinDListNode *n, *nn;

	n = l->head;
	nn = n->next;
	if (nn) {
		nn->prev = n->prev;
		l->head = nn;
		return n;
	}
	return NULL;
}

MarlinDListNode *
marlin_dlist_remtail (MarlinDList *l)
{
	MarlinDListNode *n, *np;

	n = l->tailpred;
	np = n->prev;
	if (np) {
		np->next = n->next;
		l->tailpred = np;
		return n;
	}
	return NULL;
}

int
marlin_dlist_empty (MarlinDList *l)
{
	return (l->head == (MarlinDListNode *)&l->tail);
}

int
marlin_dlist_length (MarlinDList *l)
{
	MarlinDListNode *n, *nn;
	int count = 0;

	n = l->head;
	nn = n->next;
	while (nn) {
		count++;
		n = nn;
		nn = n->next;
	}

	return count;
}

struct _MarlinMsgPort {
	MarlinDList queue;
	int condwait; /* how many waiting in condwait */
	union {
		int pipe[2];
		struct {
			int read;
			int write;
		} fd;
	} pipe;

	/* @#@$#$ glib stuff */
	GCond *cond;
	GMutex *lock;
};

MarlinMsgPort *
marlin_msgport_new (void)
{
	MarlinMsgPort *mp;

	mp = g_malloc (sizeof (*mp));
	marlin_dlist_init (&mp->queue);
	mp->lock = g_mutex_new ();
	mp->cond = g_cond_new ();
	mp->pipe.fd.read = -1;
	mp->pipe.fd.write = -1;
	mp->condwait = 0;

	return mp;
}

void
marlin_msgport_destroy (MarlinMsgPort *mp)
{
	g_mutex_free (mp->lock);
	g_cond_free (mp->cond);
	if (mp->pipe.fd.read != -1) {
		close (mp->pipe.fd.read);
		close (mp->pipe.fd.write);
	}
	g_free (mp);
}

/* Get an fd that can be used to wait on the port asynchronously */
int
marlin_msgport_fd (MarlinMsgPort *mp)
{
	int fd;

	g_mutex_lock (mp->lock);
	fd = mp->pipe.fd.read;
	if (fd == -1) {
		pipe (mp->pipe.pipe);
		fd = mp->pipe.fd.read;
	}

	g_mutex_unlock (mp->lock);

	return fd;
}

void
marlin_msgport_put (MarlinMsgPort *mp,
		    MarlinMsg *msg)
{
	int fd;

	m(printf("put:\n"));
	g_mutex_lock (mp->lock);
	marlin_dlist_addtail (&mp->queue, &msg->ln);
	if (mp->condwait > 0) {
		m(printf ("put: condwait > 0, waking up\n"));
		g_cond_signal (mp->cond);
	}
	fd = mp->pipe.fd.write;
	g_mutex_unlock (mp->lock);

	if (fd != -1) {
		m(printf("put: have pipe, writing notification to it\n"));
		write (fd, "", 1);
	}

	m(printf ("put: done\n"));
}

static void
msgport_cleanlock (void *data)
{
	MarlinMsgPort *mp = data;

	g_mutex_unlock (mp->lock);
}

MarlinMsg *
marlin_msgport_wait (MarlinMsgPort *mp)
{
	MarlinMsg *msg;

	m(printf("wait:\n"));
	g_mutex_lock (mp->lock);
	while (marlin_dlist_empty (&mp->queue)) {
		if (mp->pipe.fd.read == -1) {
			m(printf("wait: waiting on condition\n"));
			mp->condwait++;

			/* If we are cancelled in the cond-wait, then
			   we need to unlock out lock when we clean up */
			pthread_cleanup_push (msgport_cleanlock, mp);
			g_cond_wait (mp->cond, mp->lock);
			pthread_cleanup_pop (0);
			m(printf("wait: got condition\n"));
			mp->condwait--;
		} else {
			fd_set rfds;
			int retry;

			m(printf ("wait: waiting on pipe\n"));
			g_mutex_unlock(mp->lock);
			do {
				FD_ZERO (&rfds);
				FD_SET (mp->pipe.fd.read, &rfds);
				retry = select (mp->pipe.fd.read + 1, &rfds, NULL, NULL, NULL) == -1 && errno == EINTR;
				pthread_testcancel ();
			} while (retry);
			g_mutex_lock (mp->lock);
			m(printf("wait: got pipe\n"));
		}
	}
	
	msg = (MarlinMsg *) mp->queue.head;
	m(printf("wait: message = %p\n", msg));
	g_mutex_unlock(mp->lock);
	m(printf ("wait: done\n"));
	return msg;
}

MarlinMsg *
marlin_msgport_get (MarlinMsgPort *mp)
{
	MarlinMsg *msg;
	char dummy[1];

	g_mutex_lock(mp->lock);
	msg = (MarlinMsg *) marlin_dlist_remhead (&mp->queue);
	if (msg && mp->pipe.fd.read != -1) {
		read (mp->pipe.fd.read, dummy, 1);
	}
	m(printf("get: message = %p\n", msg));
	g_mutex_unlock (mp->lock);

	return msg;
}

void
marlin_msgport_reply (MarlinMsg *msg)
{
	if (msg->reply_port) {
		marlin_msgport_put (msg->reply_port, msg);
	}
	/* else lost ? */
}

struct _thread_info {
	pthread_t id;
	int busy;
};

struct _MarlinThread {
	struct _MarlinThread *next;
	struct _MarlinThread *prev;

	MarlinMsgPort *server_port;
	MarlinMsgPort *reply_port;
	pthread_mutex_t mutex;
	marlin_thread_t type;
	int queue_limit;

	int waiting; /* If we are waiting for a new message,
			count waiting processes */
	pthread_t id; /* id of our running child thread */
	GList *id_list; /* if THREAD_NEW, then a list of our child threads in
			   thread_info structs */
	
	MarlinThreadFunc destroy;
	void *destroy_data;

	MarlinThreadFunc received;
	void *received_data;

	MarlinThreadFunc lost;
	void *lost_data;
};

/* All active threads */
static MarlinDList marlin_thread_list = MARLIN_DLIST_INITIALISER(marlin_thread_list);
static pthread_mutex_t marlin_thread_lock = PTHREAD_MUTEX_INITIALIZER;

#define MARLIN_THREAD_NONE ((pthread_t)~0)
#define MARLIN_THREAD_QUIT_REPLYPORT ((struct _MarlinMsgPort *)~0)

static void thread_destroy_msg (MarlinThread *t,
				MarlinMsg *m);

static struct _thread_info *
thread_find (MarlinThread *t,
	     pthread_t id)
{
	GList *node;
	struct _thread_info *info;

	node = t->id_list;
	while (node) {
		info = node->data;
		if (info->id == id) {
			return info;
		}

		node = node->next;
	}

	return NULL;
}

#if 0
static void
thread_remove (MarlinThread *t,
	       pthread_t id)
{
	GList *node;
	struct _thread_info *info;

	node = t->id_list;
	while (node) {
		info = node->data;

		if (info->id == id) {
			t->id_list = g_list_remove (t->id_list, info);
			g_free (info);
		}

		node = node->next;
	}
}
#endif

MarlinThread *
marlin_thread_new (marlin_thread_t type)
{
	MarlinThread *m;

	m = g_malloc0 (sizeof (*m));
	pthread_mutex_init (&m->mutex, 0);
	m->type = type;
	m->server_port = marlin_msgport_new ();
	m->id = MARLIN_THREAD_NONE;
	m->queue_limit = INT_MAX;

	pthread_mutex_lock (&marlin_thread_lock);
	marlin_dlist_addtail (&marlin_thread_list, (MarlinDListNode *)m);
	pthread_mutex_unlock (&marlin_thread_lock);

	return m;
}

/* close down the threads and resources etc. */
void
marlin_thread_destroy (MarlinThread *m)
{
	int busy = FALSE;
	MarlinMsg *msg;
	struct _thread_info *info;
	GList *l;

	/* make sure we soak up all the messages first */
	while ( (msg = marlin_msgport_get (m->server_port)) ) {
		thread_destroy_msg (m, msg);
	}

	pthread_mutex_lock (&m->mutex);

	switch (m->type) {
	case MARLIN_THREAD_QUEUE:
	case MARLIN_THREAD_DROP:
		/* if we have a thread, 'kill' it */
		if (m->id != MARLIN_THREAD_NONE) {
			pthread_t id = m->id;

			t(printf ("Sending thread '%d' quit message\n", id));

			m->id = MARLIN_THREAD_NONE;

			msg = g_malloc0(sizeof (*msg));
			msg->reply_port = MARLIN_THREAD_QUIT_REPLYPORT;
			marlin_msgport_put (m->server_port, msg);

			pthread_mutex_unlock (&m->mutex);
			t(printf ("Joining thread '%d'\n", id));
			pthread_join (id, 0);
			t(printf ("Joined thread '%d'!\n", id));
			pthread_mutex_lock (&m->mutex);
		}

		busy = m->id != MARLIN_THREAD_NONE;
		break;

	case MARLIN_THREAD_NEW:
		/* First send everyone a quit message */
		l = m->id_list;
		while (l) {
			info = l->data;
			t(printf("Sending thread '%d' quit message\n", info->id));
			msg = g_malloc0 (sizeof (*msg));
			msg->reply_port = MARLIN_THREAD_QUIT_REPLYPORT;
			marlin_msgport_put (m->server_port, msg);
			l = l->next;
		}

		/* then, wait for everyone to quit */
		while (m->id_list) {
			info = m->id_list->data;
			m->id_list = g_list_remove (m->id_list, info);
			pthread_mutex_unlock (&m->mutex);
			t(printf ("Joining thread '%d'\n", info->id));
			pthread_join (info->id, 0);
			t(printf ("Joined thread '%d'!\n", info->id));
			pthread_mutex_lock (&m->mutex);
			g_free (info);
		}
		
		busy = (g_list_length (m->id_list) != 0);
		break;
	}

	pthread_mutex_unlock (&m->mutex);

	/* and clean up, if we can */
	if (busy) {
		g_warning ("Threads were busy, leaked MarlinThread");
		return;
	}

	pthread_mutex_lock (&marlin_thread_lock);
	marlin_dlist_remove ((MarlinDListNode *) m);
	pthread_mutex_unlock (&marlin_thread_lock);

	pthread_mutex_destroy (&m->mutex);
	marlin_msgport_destroy (m->server_port);
	g_free (m);
}

/* set the queue maximum depth, what happens when the queue
   fills up depends on the queue type */
void
marlin_thread_set_queue_limit (MarlinThread *m,
			       int limit)
{
	m->queue_limit = limit;
}

/* set a msg destroy callback,
   this cannot call any marlin_thread functions on @t */
void
marlin_thread_set_msg_destroy (MarlinThread *m,
			       MarlinThreadFunc destroy,
			       void *data)
{
	pthread_mutex_lock (&m->mutex);
	m->destroy = destroy;
	m->destroy_data = data;
	pthread_mutex_unlock (&m->mutex);
}

/* set a message lost callback,
   called if any message is discarded */
void
marlin_thread_set_msg_lost (MarlinThread *m,
			    MarlinThreadFunc lost,
			    void *data)
{
	pthread_mutex_lock (&m->mutex);
	m->lost = lost;
	m->lost_data = data;
	pthread_mutex_unlock (&m->mutex);
}

/* set a reply port,
   if set, then sends messages back once finished */
void
marlin_thread_set_reply_port (MarlinThread *m,
			      MarlinMsgPort *reply_port)
{
	m->reply_port = reply_port;
}

/* set a received data callback */
void
marlin_thread_set_msg_received (MarlinThread *m,
				MarlinThreadFunc received,
				void *data)
{
	pthread_mutex_lock (&m->mutex);
	m->received = received;
	m->received_data = data;
	pthread_mutex_unlock (&m->mutex);
}

/* find out if we're busy doing work,
   m == NULL, check for all work */
int
marlin_thread_busy (MarlinThread *m)
{
	int busy = FALSE;

	if (m == NULL) {
		pthread_mutex_lock (&marlin_thread_lock);
		m = (MarlinThread *) marlin_thread_list.head;
		while (m->next && !busy) {
			busy = marlin_thread_busy (m);
			m = m->next;
		}
		pthread_mutex_unlock (&marlin_thread_lock);
	} else {
		pthread_mutex_lock (&m->mutex);
		switch (m->type) {
		case MARLIN_THREAD_QUEUE:
		case MARLIN_THREAD_DROP:
			busy = m->waiting != 1 && m->id != MARLIN_THREAD_NONE;
			break;
			
		case MARLIN_THREAD_NEW:
			busy = m->waiting != g_list_length (m->id_list);
			break;
		}
		pthread_mutex_unlock (&m->mutex);
	}

	return busy;
}

static void
thread_destroy_msg (MarlinThread *t,
		    MarlinMsg *m)
{
	MarlinThreadFunc func;
	void *func_data;

	/* We do this so we never get an incomplete/unmatched
	   callback + data */
	pthread_mutex_lock (&t->mutex);
	func = t->destroy;
	func_data = t->destroy_data;
	pthread_mutex_unlock (&t->mutex);

	if (func) {
		func (t, m, func_data);
	}
}

static void
thread_received_msg (MarlinThread *t,
		     MarlinMsg *m)
{
	MarlinThreadFunc func;
	void *func_data;

	pthread_mutex_lock (&t->mutex);
	func = t->received;
	func_data = t->received_data;
	pthread_mutex_unlock (&t->mutex);

	if (func) {
		func (t, m, func_data);
	} else {
		g_warning ("No processing callback for MarlinThread, message unprocessed");
	}
}

static void
thread_lost_msg (MarlinThread *t,
		 MarlinMsg *m)
{
	MarlinThreadFunc func;
	void *func_data;

	pthread_mutex_lock (&t->mutex);
	func = t->lost;
	func_data = t->lost_data;
	pthread_mutex_unlock (&t->mutex);

	if (func) {
		func (t, m, func_data);
	}
}

/* The actual thread dispatcher */
static void *
thread_dispatch (void *din)
{
	MarlinThread *t = din;
	MarlinMsg *m;
	struct _thread_info *info;
	pthread_t self = pthread_self ();

	t(printf ("Dispatch thread started: %ld\n", pthread_self ()));

	while (1) {
		pthread_mutex_lock (&t->mutex);
		m = marlin_msgport_get (t->server_port);
		if (m == NULL) {
			/* nothing to do? If we are a 'new' type thread,
			   just quit. Otherwise, go into waiting (can be
			   cancelled here) */
			info = NULL;
			switch (t->type) {
			case MARLIN_THREAD_NEW:
			case MARLIN_THREAD_QUEUE:
			case MARLIN_THREAD_DROP:
				info = thread_find (t, self);
				if (info) {
					info->busy = FALSE;
				}
				t->waiting++;

				pthread_mutex_unlock (&t->mutex);
				marlin_msgport_wait (t->server_port);
				pthread_mutex_lock (&t->mutex);
				t->waiting--;
				pthread_mutex_unlock (&t->mutex);
				break;
#if 0
			case MARLIN_THREAD_NEW:
				t->id_list = g_list_remove (t->id_list, (void *) pthread_self ());
				pthread_mutex_unlock (&t->mutex);
				return 0;
#endif
			}

			continue;
		} else if (m->reply_port == MARLIN_THREAD_QUIT_REPLYPORT) {
			t(printf ("Thread %d got quit message\n", self));
			/* Handle a quit message,
			   say we're quitting,
			   free the message
			   and break out of the loop */
			info = thread_find (t, self);
			if (info) {
				info->busy = 2;
			}
			pthread_mutex_unlock (&t->mutex);
			g_free (m);
			break;
		} else {
			info = thread_find (t, self);
			if (info) {
				info->busy = TRUE;
			}
		}
		pthread_mutex_unlock (&t->mutex);

		t(printf ("Got message in dispatch thread\n"));

		/* process it */
		thread_received_msg (t, m);

		/* If we have a reply port, send it back,
		   otherwise, lose it */
		if (m->reply_port) {
			marlin_msgport_reply (m);
		} else {
			thread_destroy_msg (t, m);
		}
	}

	return NULL;
}

/* Send a message to the thread,
   start thread if necessary */
void
marlin_thread_put (MarlinThread *t,
		   MarlinMsg *msg)
{
	pthread_t id;
	MarlinMsg *dmsg = NULL;

	pthread_mutex_lock (&t->mutex);

	/* The caller forgot to tell us what to do,
	   well, we can't do anything can we? */
	if (t->received == NULL) {
		pthread_mutex_unlock (&t->mutex);
		g_warning ("MarlinThread called with no receiver function, no work to do!");
		thread_destroy_msg (t, msg);
		return;
	}

	msg->reply_port = t->reply_port;

	switch (t->type) {
	case MARLIN_THREAD_QUEUE:
		/* If the queue is full, lose this new addition */
		if (marlin_dlist_length (&t->server_port->queue) < t->queue_limit) {
			marlin_msgport_put (t->server_port, msg);
		} else {
			printf ("Queue limit reached, dropping new message\n");
			dmsg = msg;
		}
		break;

	case MARLIN_THREAD_DROP:
		/* If the queue is full, lose the oldest (unprocessed) message */
		if (marlin_dlist_length (&t->server_port->queue) < t->queue_limit) {
			marlin_msgport_put (t->server_port, msg);
		} else {
			printf ("Queue limit reached, dropping old message\n");
			marlin_msgport_put (t->server_port, msg);
			dmsg = marlin_msgport_get (t->server_port);
		}
		break;

	case MARLIN_THREAD_NEW:
		/* It is possible that an existing thread can catch this message
		   so we might create a thread with no work to do
		   but that doesn't matter, the other alternative that it be
		   lost is worse */
		marlin_msgport_put (t->server_port, msg);
		if (t->waiting == 0
		    && g_list_length (t->id_list) < t->queue_limit
		    && pthread_create (&id, NULL, thread_dispatch, t) == 0) {
			struct _thread_info *info = g_malloc0 (sizeof (*info));
			t(printf ("created NEW thread %ld\n", id));

			info->id = id;
			info->busy = TRUE;
			t->id_list = g_list_append (t->id_list, info);
		}

		pthread_mutex_unlock (&t->mutex);
		return;
	}

	/* create the thread, if there is none to receive it yet */
	if (t->id == MARLIN_THREAD_NONE) {
		if (pthread_create (&t->id, NULL, thread_dispatch, t) == -1) {
			g_warning ("Could not create dispatcher thread, message queued?: %s", strerror (errno));
			t->id = MARLIN_THREAD_NONE;
		}
	}

	pthread_mutex_unlock (&t->mutex);

	if (dmsg) {
		thread_lost_msg (t, dmsg);
		thread_destroy_msg (t, dmsg);
	}
}

/* yet-another-mutex interface */
struct _MarlinMutex {
	int type;
	pthread_t owner;
	short waiters;
	short depth;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
};

/* sigh, this is just painful to have to need, but recursive
   read/write, etc mutexes just aren't very common in thread
   implementations */
/* TODO: Just make it use recursive mutexes if they are available */
MarlinMutex *
marlin_mutex_new (marlin_mutex_t type)
{
	struct _MarlinMutex *m;

	m = g_malloc (sizeof (*m));
	m->type = type;
	m->waiters = 0;
	m->depth = 0;
	m->owner = MARLIN_THREAD_NONE;

	switch (type) {
	case MARLIN_MUTEX_SIMPLE:
		pthread_mutex_init (&m->mutex, 0);
		break;

	case MARLIN_MUTEX_REC:
		pthread_mutex_init (&m->mutex, 0);
		pthread_cond_init (&m->cond, 0);
		break;
		
		/* read / write? flags for same? */
	}

	return m;
}

int
marlin_mutex_destroy (MarlinMutex *m)
{
	int ret = 0;

	switch (m->type) {
	case MARLIN_MUTEX_SIMPLE:
		ret = pthread_mutex_destroy (&m->mutex);
		if (ret == -1) {
			g_warning ("MarlinMutex destroy failed: %s", strerror (errno));
		}

		g_free (m);
		break;

	case MARLIN_MUTEX_REC:
		ret = pthread_mutex_destroy (&m->mutex);
		if (ret == -1) {
			g_warning ("MarlinMutex destroy failed: %s", strerror (errno));
		}

		ret = pthread_cond_destroy (&m->cond);
		if (ret == -1) {
			g_warning ("MarlinMutex destroy failed: %s", strerror (errno));
		}
		
		g_free (m);
	}

	return ret;
}

int
marlin_mutex_lock (MarlinMutex *m)
{
	pthread_t id;

	switch (m->type) {
	case MARLIN_MUTEX_SIMPLE:
		return pthread_mutex_lock (&m->mutex);

	case MARLIN_MUTEX_REC:
		id = pthread_self ();
		if (pthread_mutex_lock (&m->mutex) == -1) {
			return -1;
		}

		while (1) {
			if (m->owner == MARLIN_THREAD_NONE) {
				m->owner = id;
				m->depth = 1;
				break;
			} else if (id == m->owner) {
				m->depth++;
				break;
			} else {
				m->waiters++;
				if (pthread_cond_wait (&m->cond, &m->mutex) == -1) {
					return -1;
				}

				m->waiters--;
			}
		}
		
		return pthread_mutex_unlock (&m->mutex);
	}

	errno = EINVAL;
	return -1;
}

int
marlin_mutex_unlock (MarlinMutex *m)
{
	switch (m->type) {
	case MARLIN_MUTEX_SIMPLE:
		return pthread_mutex_unlock (&m->mutex);

	case MARLIN_MUTEX_REC:
		if (pthread_mutex_lock (&m->mutex) == -1) {
			return -1;
		}

		g_assert (m->owner == pthread_self ());

		m->depth--;
		if (m->depth == 0) {
			m->owner = MARLIN_THREAD_NONE;
			if (m->waiters > 0) {
				pthread_cond_signal (&m->cond);
			}
		}

		return pthread_mutex_unlock (&m->mutex);
	}

	errno = EINVAL;
	return -1;
}

void
marlin_mutex_assert_locked (MarlinMutex *m)
{
	g_return_if_fail (m->type == MARLIN_MUTEX_REC);

	pthread_mutex_lock (&m->mutex);
	g_assert (m->owner == pthread_self ());
	pthread_mutex_unlock (&m->mutex);
}

		
		
