/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib.h>
#include <gst/gst.h>

#include "marlin-channel-splitter.h"

GMainLoop *mainloop;
GstElement *pipeline, *src, *decoder, *splitter, *level, *sink;

#if 0
static void
handoff (GstElement *sink,
	 GstBuffer *buffer,
	 GstPad *pad,
	 gpointer ud)
{
	g_print ("%d\n", GST_BUFFER_SIZE (buffer));
}
#endif
#if 0
static void
new_splitter_pad (GstElement *split,
		  GstPad *pad,
		  gpointer userdata)
{
	GstElement *sink, *q;
	GstPad *sink_pad;
	GstCaps *caps;
	GstState state;
	char *name;

	caps = gst_pad_get_caps (pad);
	g_print ("New sink - %s\n", gst_caps_to_string (caps));

	name = g_strdup_printf ("load-q-%d", channel_count);
	q = gst_element_factory_make ("queue", name);
	g_free (name);

	gst_bin_add (GST_BIN (pipeline), q);
	sink_pad = gst_element_get_pad (q, "sink");
	if (gst_pad_link (pad, sink_pad) != GST_PAD_LINK_OK) {
		g_warning ("Failed to link pad to queue");

		gst_bin_remove (GST_BIN (pipeline), q);
		return;
	}

	name = g_strdup_printf ("load-sink-%d", channel_count++);
	sink = gst_element_factory_make ("fakesink", name);
	g_signal_connect (G_OBJECT (sink), "handoff",
			  G_CALLBACK (handoff), NULL);
	g_object_set (G_OBJECT (sink),
		      "signal-handoffs", TRUE,
		      NULL);
	g_free (name);

	gst_bin_add (GST_BIN (pipeline), sink);

	if (gst_element_link (q, sink) == FALSE) {
		g_warning ("Failed to link queue to sink");
		return;
	}

	if (gst_element_sync_state_with_parent (q) == FALSE) {
		g_warning ("Could not sync queue");
		return;
	}

	if (gst_element_sync_state_with_parent (sink) == FALSE) {
		g_warning ("Could not sync sink");
		return;
	}

	/* Wait a second just to let the elements sync */
       	gst_element_get_state (GST_ELEMENT (pipeline), &state, NULL, 0);
}
#endif
static gboolean
position_tracker (gpointer userdata)
{
	GstFormat fmt;
	GstState state, pending;
	gint64 pos, duration;

	fmt = GST_FORMAT_TIME;
	if (gst_element_query_position (pipeline, &fmt, &pos) &&
	    gst_element_query_duration (pipeline, &fmt, &duration)) {
		float p;
		int percentage;

		p = ((float) pos) / ((float) duration);
		percentage = (int) (p * 100);
		
		g_print ("%d%% - (%lld of %lld)\n", percentage, pos, duration);
	}

	gst_element_get_state (pipeline, &state, &pending, 0);
	return (state == GST_STATE_PLAYING);
}

static gboolean
register_elements (GstPlugin *plugin)
{
	gboolean ret;

	ret = gst_element_register (plugin, "marlin-channel-splitter", 
				    GST_RANK_NONE, 
				    MARLIN_CHANNEL_SPLITTER_TYPE);
	return ret;
}

static GstPluginDesc plugin_desc = {
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	"marlin-plugins",
	"GStreamer plugins for Marlin",
	register_elements,
	"0.1",
	"LGPL",
	"test",
	"marlin",
	"http://marlin.sf.net",
	GST_PADDING_INIT
};

int
main (int argc,
      char **argv)
{
	/* Init gobject system */
	g_type_init ();
	
	/* Init GStreamer and Marlin gstreamer plugins */
	gst_init (&argc, &argv);
	
	_gst_plugin_register_static (&plugin_desc);
	
	if (argc != 2) {
		g_print ("Usage: %s <sample file>\n", argv[0]);
		exit (1);
	}
	
	pipeline = gst_pipeline_new ("test-split-pipeline");
	src = gst_element_factory_make ("filesrc", "src");
	g_object_set (G_OBJECT (src),
		      "location", argv[1],
		      NULL);
	decoder = gst_element_factory_make ("mad", "decoder");
	level = gst_element_factory_make ("level", "level");

	gst_bin_add_many (GST_BIN (pipeline), src, decoder, splitter, NULL);

	g_assert (gst_element_link (src, decoder));
	g_assert (gst_element_link (decoder, splitter));

	mainloop = g_main_loop_new (NULL, FALSE);
	
	g_timeout_add (1000, position_tracker, NULL);

	gst_element_set_state (pipeline, GST_STATE_PLAYING);
	g_main_loop_run (mainloop);

	return 0;
}
