/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_OBJECT_H__
#define __MARLIN_OBJECT_H__

#include <glib-object.h>

#define MARLIN_OBJECT_TYPE (marlin_object_get_type ())

typedef struct _MarlinObject MarlinObject;
typedef struct _MarlinObjectPrivate MarlinObjectPrivate;
typedef struct _MarlinObjectClass MarlinObjectClass;

struct _MarlinObject {
	GObject parent;
};

struct _MarlinObjectClass {
	GObjectClass parent_class;

	void (* safe_notify) (MarlinObject *object,
			      const char *name);
};

GType marlin_object_get_type (void);

void marlin_object_notify (GObject *object,
			   const char *name);

#endif
