/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/audio/multichannel.h>

#include <marlin/marlin-block.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-channel-sink.h>
#include <marlin/marlin-file-utils.h>
#include <marlin/marlin-sample.h>

#define MARLIN_CHANNEL_SINK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_CHANNEL_SINK_TYPE, MarlinChannelSinkPrivate))

struct _MarlinChannelSinkPrivate {
	MarlinSample *sample;
	MarlinChannel *channel;
	GstAudioChannelPosition position;

	float *data;
	guint32 frames_in_data;

	/* FIXME: Might it be useful to have a MarlinBlockList ADT? */
	MarlinBlock *block_list, *bl_end;
};

static const GstElementDetails sink_details =
GST_ELEMENT_DETAILS ("Marlin channel sink",
		     "Sink",
		     "Store data in a MarlinChannel",
		     "Iain Holmes <iain@gnome.org>");

enum {
	PROP_0,
	PROP_SAMPLE
};

static GstStaticPadTemplate sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
			 GST_PAD_SINK,
			 GST_PAD_ALWAYS,
			 GST_STATIC_CAPS ("audio/x-raw-float, "
					  "rate = (int) [1, MAX], "
					  "channels = (int) 1"));

static GstBaseSinkClass *parent_class = NULL;

static void
base_init (gpointer g_class)
{
	GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

	gst_element_class_set_details (element_class, &sink_details);

	gst_element_class_add_pad_template
		(element_class,
		 gst_static_pad_template_get (&sink_factory));
}

static void
finalize (GObject *object)
{
 	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;

	sink = MARLIN_CHANNEL_SINK (object);
	priv = sink->priv;

	if (priv->data) {
		g_free (priv->data);
		priv->data = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;

	sink = MARLIN_CHANNEL_SINK (object);
	priv = sink->priv;

	if (priv->sample) {
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static gboolean
set_caps (GstBaseSink *basesink,
	  GstCaps     *caps)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;
	GstStructure *structure;
	GstAudioChannelPosition *positions;

	sink = MARLIN_CHANNEL_SINK (basesink);
	priv = sink->priv;

	structure = gst_caps_get_structure (caps, 0);
	positions = gst_audio_get_channel_positions (structure);
	priv->position = positions[0];
	g_free (positions);

	return TRUE;
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;

	sink = MARLIN_CHANNEL_SINK (object);
	priv = sink->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		if (priv->sample != NULL) {
			g_object_unref (priv->sample);
		}

		priv->sample = MARLIN_SAMPLE (g_value_dup_object (value));
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	switch (prop_id) {
	default:
		break;
	}
}

static MarlinChannel *
sink_get_channel (MarlinChannelSink *sink)
{
	MarlinChannelSinkPrivate *priv;

	priv = sink->priv;

	if (priv->position == GST_AUDIO_CHANNEL_POSITION_FRONT_MONO ||
	    priv->position == GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT) {
		return marlin_sample_get_channel (priv->sample,
						  MARLIN_CHANNEL_LEFT);
	} else {
		return marlin_sample_get_channel (priv->sample,
						  MARLIN_CHANNEL_RIGHT);
	}
}

static void
store_block (MarlinChannelSink *sink,
	     float             *data,
	     guint              number_of_frames)
{
	MarlinChannelSinkPrivate *priv;
	MarlinBlock *block;
	GError *error = NULL;
	gboolean ret;

	priv = sink->priv;

	sink->frame_count += number_of_frames;
	if (sink->no_space) {
		return;
	}

	if (priv->channel == NULL) {
		/* FIXME: This should be done in change_state */
		priv->channel = sink_get_channel (sink);
	}

	block = marlin_channel_create_block (priv->channel);
	ret = marlin_block_set_data (block, data, number_of_frames, &error);

	if (ret == FALSE) {
		if (error && error->code == MARLIN_FILE_ERROR_NO_SPACE) {
			sink->no_space = TRUE;
			sink->no_space_error = error;
			sink->no_space_frames = sink->frame_count;
		} else if (error) {
			GST_ELEMENT_ERROR (GST_ELEMENT (sink), RESOURCE,
					   WRITE, (_("Error writing data")),
					   ("%s", error->message));
			g_error_free (error);
		}

		/* FIXME: Free blocklist */
		marlin_block_free (block);
		return;
	}

	/* Store the new block in the list for linking
	   once the element has finished */
	if (priv->block_list == NULL) {
		priv->block_list = block;
		priv->bl_end = block;
	} else {
		marlin_block_append (priv->bl_end, block);
		priv->bl_end = block;
	}
}

static gboolean
flush_buffer (MarlinChannelSink *sink)
{
	MarlinChannelSinkPrivate *priv;
	MarlinChannel *channel;

	priv = sink->priv;

	if (priv->frames_in_data > 0) {
		/* We still have some data to store */
		store_block (sink, priv->data, priv->frames_in_data);
	}

	if (sink->no_space) {
		guint64 bytes_needed, bytes_have;
		char *need, *have;

		bytes_needed = (sink->frame_count * sizeof (float)) +
			((sink->frame_count / 128) * (sizeof (float) * 4));
		bytes_have = sink->no_space_frames * sizeof (float);

		need = g_format_size_for_display (bytes_needed);
		have = g_format_size_for_display (bytes_have);

		GST_ELEMENT_ERROR (GST_ELEMENT (sink), RESOURCE, WRITE,
				   ("Marlin ran out of temporary space.\n%s of free space is needed on '%s' but only %s is available", need,
				    marlin_get_tmp_dir (), have),
				   ("%s", sink->no_space_error->message));
		g_free (need);
		g_free (have);

		return TRUE;
	}

	channel = priv->channel;
	if (channel == NULL) {
		g_print ("Channel is null\n");
		return FALSE;
	}

	WRITE_LOCK (channel->lock);
	channel->first = priv->block_list;
	channel->last = priv->bl_end;
	channel->frames = marlin_block_recalculate_ranges (channel->first);
	WRITE_UNLOCK (channel->lock);

	marlin_sample_frame_count_changed (priv->sample);
	return TRUE;
}

static GstFlowReturn
sink_render (GstBaseSink *basesink,
	     GstBuffer   *buffer)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;
	guint size;
	guint32 frames;
	float *in;

	sink = (MarlinChannelSink *) basesink;
	priv = sink->priv;

	size = GST_BUFFER_SIZE (buffer);
	frames = size / sizeof (float);

	in = (float *) GST_BUFFER_DATA (buffer);

	if (priv->frames_in_data + frames > MARLIN_BLOCK_SIZE) {
		guint32 needed, remain;

		needed = MARLIN_BLOCK_SIZE - priv->frames_in_data;
		remain = frames - needed;

		memcpy (priv->data + priv->frames_in_data,
			in, needed * sizeof (float));
		store_block (sink, priv->data, MARLIN_BLOCK_SIZE);

		/* Clear the block */
		memset (priv->data, 0, MARLIN_BLOCK_SIZE * sizeof (float));

		/* Move the rest of the data across */
		if (remain > 0) {
			memmove (priv->data, in + needed,
				 remain * sizeof (float));
		}

		priv->frames_in_data = remain;
	} else {
		memcpy (priv->data + priv->frames_in_data, in, size);
		priv->frames_in_data += frames;
	}

	return GST_FLOW_OK;
}

static GstStateChangeReturn
change_state (GstElement     *element,
	      GstStateChange  transition)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;

	sink = MARLIN_CHANNEL_SINK (element);
	priv = sink->priv;

	switch (transition) {
	case GST_STATE_CHANGE_NULL_TO_READY:
		break;

	case GST_STATE_CHANGE_READY_TO_PAUSED:
		break;

	case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
		if (priv->sample == NULL) {
			g_warning ("Attempting to play without sample");
			return GST_STATE_CHANGE_FAILURE;
		}

		if (priv->position == GST_AUDIO_CHANNEL_POSITION_FRONT_MONO ||
		    priv->position == GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT) {
			priv->channel = marlin_sample_get_channel
				(priv->sample, MARLIN_CHANNEL_LEFT);
		} else {
			priv->channel = marlin_sample_get_channel
				(priv->sample, MARLIN_CHANNEL_RIGHT);
		}
		break;

	default:
		break;
	}

	if (GST_ELEMENT_CLASS (parent_class)->change_state) {
		return GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
	}

	return GST_STATE_CHANGE_SUCCESS;
}

static gboolean
sink_event (GstBaseSink *basesink,
	    GstEvent    *event)
{
	MarlinChannelSink *sink;
	MarlinChannelSinkPrivate *priv;

	sink = MARLIN_CHANNEL_SINK (basesink);
	priv = sink->priv;

	switch (GST_EVENT_TYPE (event)) {
	case GST_EVENT_EOS:
		flush_buffer (sink);
		break;

	default:
		break;
	}

	return TRUE;
}

static void
class_init (MarlinChannelSinkClass *klass)
{
	GObjectClass *o_class;
	GstElementClass *e_class;
	GstBaseSinkClass *b_class;

	g_type_class_add_private (klass, sizeof (MarlinChannelSinkPrivate));

	o_class = (GObjectClass *) klass;
	e_class = (GstElementClass *) klass;
	b_class = (GstBaseSinkClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	e_class->change_state = change_state;

 	b_class->set_caps = set_caps;
	b_class->render = sink_render;
	b_class->event = sink_event;

	g_object_class_install_property (o_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
}

static void
init (MarlinChannelSink *sink)
{
	MarlinChannelSinkPrivate *priv;

	sink->priv = MARLIN_CHANNEL_SINK_GET_PRIVATE (sink);
	priv = sink->priv;

	priv->data = g_new0 (float, MARLIN_BLOCK_SIZE);
	priv->position = GST_AUDIO_CHANNEL_POSITION_FRONT_MONO;

	GST_BASE_SINK (sink)->sync = FALSE;
}

GType
marlin_channel_sink_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MarlinChannelSinkClass),
			base_init,
			NULL, (GClassInitFunc) class_init,
			NULL, NULL, sizeof (MarlinChannelSink),
			0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (GST_TYPE_BASE_SINK,
					       "MarlinChannelSink",
					       &info, 0);
	}

	return type;
}
