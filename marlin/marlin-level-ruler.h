/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_LEVEL_RULER_H__
#define __MARLIN_LEVEL_RULER_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_LEVEL_RULER_TYPE (marlin_level_ruler_get_type ())
#define MARLIN_LEVEL_RULER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_LEVEL_RULER_TYPE, MarlinLevelRuler))
#define IS_MARLIN_LEVEL_RULER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_LEVEL_RULER_TYPE))

typedef struct _MarlinLevelRuler MarlinLevelRuler;
typedef struct _MarlinLevelRulerClass MarlinLevelRulerClass;
typedef struct _MarlinLevelRulerPrivate MarlinLevelRulerPrivate;

struct _MarlinLevelRuler {
	GtkWidget parent;

	MarlinLevelRulerPrivate *priv;
};

struct _MarlinLevelRulerClass {
	GtkWidgetClass parent_class;

	void (*baseline_changed) (MarlinLevelRuler *ruler,
				  int baseline_offset);
};

GType marlin_level_ruler_get_type (void) G_GNUC_CONST;

void marlin_level_ruler_set_levels (MarlinLevelRuler *ruler,
				    float vmin,
				    float vmax);

#endif
