/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib-object.h>

#include <glib/gi18n.h>

#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtklabel.h>

#include <gst/media-info/media-info.h>
#include <gst/gconf/gconf.h>

#include <marlin/marlin-file-selection.h>
#include <marlin/marlin-stock.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-a11y-helper.h>

enum {
	PROP_0,
	PROP_SHOW_INFO,
	PROP_SHOW_OPTIONS
};

struct _MarlinFileSelectionPrivate {
	GtkWidget *options_frame;
	GtkWidget *info_frame;

	/* Info widgets */
	GtkWidget *length;
	GtkWidget *sample_rate;
	GtkWidget *name;
	GtkWidget *channels;
	GtkWidget *mimetype;
	
	/* Transport controls */
	GtkWidget *play;
	GtkWidget *stop;

	/* For the metadata */
	GstMediaInfo *info;
	GstMediaInfoStream *stream;
	guint32 info_id;
	
	/* Filesel hooks */
	GtkTreeSelection *selection;
	guint32 change_id;
	
	/* Preview */
	GstElement *player;
	GstElement *src;
	GstElement *decoder;
	GstElement *sink;
	guint32 play_id;
};

static GtkFileChooserDialogClass *parent_class = NULL;

static void
finalize (GObject *object)
{
	MarlinFileSelection *fs;
	MarlinFileSelectionPrivate *priv;
	
	fs = MARLIN_FILE_SELECTION (object);

	if (fs->priv == NULL) {
		return;
	}

	priv = fs->priv;
	
	if (priv->info_id > 0) {
		g_source_remove (priv->info_id);
	}

	if (priv->info != NULL) {
		g_object_unref (G_OBJECT (priv->info));
	}

	if (priv->stream != NULL) {
		g_free (priv->stream);
	}

	if (priv->play_id > 0) {
		gst_element_set_state (priv->player, GST_STATE_READY);
		g_source_remove (priv->play_id);
		g_object_unref (G_OBJECT (priv->player));
	}
	
	g_free (fs->priv);
	fs->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
clear_info (MarlinFileSelection *fs)
{
	gtk_label_set_text (GTK_LABEL (fs->priv->length), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (fs->priv->sample_rate), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (fs->priv->channels), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (fs->priv->name), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (fs->priv->mimetype), _("Unknown"));
}

static void
set_info (MarlinFileSelection *fs,
	  GstMediaInfoStream *stream)
{
	char *str_val;
	int int_val;
	GstMediaInfoTrack *track;
	
	str_val = marlin_ms_to_pretty_time ((stream->length_time / GST_SECOND) * 1000);
	gtk_label_set_text (GTK_LABEL (fs->priv->length), str_val);
	g_free (str_val);

	if (stream->tracks == NULL) {
		/* Erk? */
		return;
	}

	track = stream->tracks->data;
	
	int_val = get_int_from_caps (track->format, "channels");
	str_val = g_strdup_printf ("%d (%s)", int_val, int_val == 1 ? _("mono") : _("stereo"));
	gtk_label_set_text (GTK_LABEL (fs->priv->channels), str_val);
	g_free (str_val);

	int_val = get_int_from_caps (track->format, "rate");
	str_val = g_strdup_printf ("%d hz", int_val);
	gtk_label_set_text (GTK_LABEL (fs->priv->sample_rate), str_val);
	g_free (str_val);

	str_val = get_string_from_caps (track->metadata, "title");
	gtk_label_set_text (GTK_LABEL (fs->priv->name), str_val);
	g_free (str_val);

	gtk_label_set_text (GTK_LABEL (fs->priv->mimetype), stream->mime);
}
	
static gboolean
get_media_info (gpointer data)
{
	MarlinFileSelection *fs = data;

	if (gst_media_info_read_idler (fs->priv->info, &fs->priv->stream) &&
	    fs->priv->stream == NULL) {
		/* Keep idling */
		return TRUE;
	}

	if (fs->priv->stream) {
		set_info (fs, fs->priv->stream);
	} else {
		clear_info (fs);
	}

	return FALSE;
}
		
static void
selection_changed (GtkTreeSelection *selection,
		   MarlinFileSelection *fs)
{
#if 0
	const char *filename;
	
	if (fs->priv->info_id > 0) {
		g_source_remove (fs->priv->info_id);
	}

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs));
	if (g_file_test (filename, G_FILE_TEST_IS_DIR)) {
		gtk_widget_set_sensitive (fs->priv->play, FALSE);
		clear_info (fs);
		return;
	}

	gtk_widget_set_sensitive (fs->priv->play, TRUE);

	if (fs->priv->play_id > 0) {
		gst_element_set_state (fs->priv->player, GST_STATE_READY);
		g_source_remove (fs->priv->play_id);
		fs->priv->play_id = 0;

		gtk_widget_show (fs->priv->play);
		gtk_widget_hide (fs->priv->stop);
	}
	
	if (fs->priv->info != NULL) {
		g_object_unref (G_OBJECT (fs->priv->info));
	}

	if (fs->priv->stream != NULL) {
		g_free (fs->priv->stream);
		fs->priv->stream = NULL;
	}
	
	fs->priv->info = g_object_new (GST_MEDIA_INFO_TYPE, NULL);
	gst_media_info_read_with_idler (fs->priv->info, filename, GST_MEDIA_INFO_ALL);
	
	fs->priv->info_id = g_idle_add (get_media_info, fs);
#endif
}

static void
set_property (GObject *object,
	      guint32 prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinFileSelection *fs = MARLIN_FILE_SELECTION (object);
	gboolean val;
	
	switch (prop_id) {
	case PROP_SHOW_INFO:
		val = g_value_get_boolean (value);

		if (val) {
			fs->priv->change_id = g_signal_connect (fs->priv->selection, "changed",
								G_CALLBACK (selection_changed), fs);
			gtk_widget_show (fs->priv->info_frame);
		} else {
			if (fs->priv->change_id > 0) {
				g_signal_handler_disconnect (fs->priv->selection,
							     fs->priv->change_id);
				fs->priv->change_id = 0;
			}
			
			gtk_widget_hide (fs->priv->info_frame);
		}
		
		break;

	case PROP_SHOW_OPTIONS:
		val = g_value_get_boolean (value);

		if (val) {
			gtk_widget_show (fs->priv->options_frame);
		} else {
			gtk_widget_hide (fs->priv->options_frame);
		}

		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint32 prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinFileSelection *fs = MARLIN_FILE_SELECTION (object);

	switch (prop_id) {
	case PROP_SHOW_INFO:
		break;

	case PROP_SHOW_OPTIONS:
		break;

	default:
		break;
	}
}

static void
class_init (MarlinFileSelectionClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (object_class,
					 PROP_SHOW_INFO,
					 g_param_spec_boolean ("show_info",
							       "", "",
							       FALSE,
							       G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
					 PROP_SHOW_OPTIONS,
					 g_param_spec_boolean ("show_options",
							       "", "",
							       FALSE,
							       G_PARAM_WRITABLE));
}

static gboolean
play_iterate (gpointer data)
{
	MarlinFileSelection *fs = data;

	return gst_bin_iterate (GST_BIN (fs->priv->player));
}

static void
eos_reached (GstElement *element,
	     MarlinFileSelection *fs)
{
	gtk_widget_show (fs->priv->play);
	gtk_widget_hide (fs->priv->stop);
}

static void
start_playing (GtkWidget *button,
	       MarlinFileSelection *fs)
{
#if 0
	const char *filename;
	char *mimetype;

	/* Tear it down */
	if (fs->priv->player != NULL) {
		g_object_unref (G_OBJECT (fs->priv->player));
		fs->priv->player = NULL;
	}
	
	/* Build it up */
 	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs));
	mimetype = gnome_vfs_get_mime_type (filename);
	
	if (mimetype == NULL) {
		g_warning ("Mimetype is NULL");
		return;
	}
	
	fs->priv->decoder = get_decoder_for_mime (mimetype, "decoder");
	g_free (mimetype);
	
	if (fs->priv->decoder == NULL) {
		g_warning ("Decoder is NULL");
		return;
	}

	fs->priv->player = gst_pipeline_new ("previewer");

	fs->priv->src = gst_element_factory_make ("filesrc", "src");
	fs->priv->sink = gst_gconf_get_default_audio_sink ();
	g_signal_connect (fs->priv->sink, "eos",
			  G_CALLBACK (eos_reached), fs);

	gst_bin_add_many (GST_BIN (fs->priv->player), fs->priv->src,
			  fs->priv->sink, fs->priv->decoder, NULL);

	gst_element_link (fs->priv->src, fs->priv->decoder);
	gst_element_link (fs->priv->decoder, fs->priv->sink);
	
	g_object_set (G_OBJECT (fs->priv->src),
		      "location", filename,
		      NULL);
	
	gst_element_set_state (fs->priv->player, GST_STATE_PLAYING);
	fs->priv->play_id = g_idle_add (play_iterate, fs);

	gtk_widget_hide (fs->priv->play);
	gtk_widget_show (fs->priv->stop);
#endif
}

static void
stop_playing (GtkWidget *button,
	      MarlinFileSelection *fs)
{
	gst_element_set_state (fs->priv->player, GST_STATE_READY);
	g_source_remove (fs->priv->play_id);
	fs->priv->play_id = 0;
	
	gtk_widget_hide (fs->priv->stop);
	gtk_widget_show (fs->priv->play);
}

static void
build_info_contents (MarlinFileSelection *fs)
{
	MarlinFileSelectionPrivate *priv = fs->priv;
	GtkWidget *table, *label, *vbox, *spacer, *hbox;

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (priv->info_frame), vbox, TRUE, FALSE, 0);

	table = marlin_make_table (5, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = marlin_make_title_label (_("Name:"));
	PACK (table, label, 0, 0, GTK_FILL);

	priv->name = marlin_make_info_label (_("Unknown"));
	gtk_label_set_line_wrap (GTK_LABEL (priv->name), TRUE);
	PACK (table, priv->name, 1, 0, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     priv->name, ATK_RELATION_LABELLED_BY);
	
	label = marlin_make_title_label (_("Mime Type:"));
	PACK (table, label, 0, 1, GTK_FILL);

	priv->mimetype = marlin_make_info_label (_("Unknown"));
	PACK (table, priv->mimetype, 1, 1, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     priv->mimetype, ATK_RELATION_LABELLED_BY);
	
	label = marlin_make_title_label (_("Length:"));
	PACK (table, label, 0, 2, GTK_FILL);

	priv->length = marlin_make_info_label (_("Unknown"));
	PACK (table, priv->length, 1, 2, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     priv->length, ATK_RELATION_LABELLED_BY);
	
	label = marlin_make_title_label (_("Sample Rate:"));
	PACK (table, label, 0, 3, GTK_FILL);

	priv->sample_rate = marlin_make_info_label (_("Unknown"));
	PACK (table, priv->sample_rate, 1, 3, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     priv->sample_rate, ATK_RELATION_LABELLED_BY);
	
	label = marlin_make_title_label (_("Channels:"));
	PACK (table, label, 0, 4, GTK_FILL);

	priv->channels = marlin_make_info_label (_("Unknown"));
	PACK (table, priv->channels, 1, 4, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     priv->channels, ATK_RELATION_LABELLED_BY);
	
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	priv->play = marlin_make_button (_("Play"), MARLIN_STOCK_PLAY);
	gtk_widget_set_sensitive (priv->play, FALSE);
	g_signal_connect (G_OBJECT (priv->play), "clicked",
			  G_CALLBACK (start_playing), fs);
	gtk_box_pack_start (GTK_BOX (hbox), priv->play, FALSE, FALSE, 0);

	priv->stop = marlin_make_button (_("Stop"), MARLIN_STOCK_STOP);
	g_signal_connect (G_OBJECT (priv->stop), "clicked",
			  G_CALLBACK (stop_playing), fs);
	gtk_box_pack_start (GTK_BOX (hbox), priv->stop, FALSE, FALSE, 0);
	
	gtk_widget_show_all (vbox);
	gtk_widget_hide (priv->stop);
}

#if 0
static void
caps_print (GstCaps *caps)
{
	if (caps == NULL) return;

	if (TRUE) {
		GstProps *props = caps->properties;
		GList *walk;
		
		if (props == NULL) {
			g_print ("    none\n");
			return;
		}
		walk = props->properties;
		
		while (walk) {
			GstPropsEntry *entry = (GstPropsEntry *) walk->data;
			const gchar *name;
			gchar *str_val;
			gint int_val;
			GstPropsType type;
			
			name = gst_props_entry_get_name (entry);
			type = gst_props_entry_get_props_type (entry);
			switch (type) {
			case GST_PROPS_STRING_TYPE:
				gst_props_entry_get_string (entry, &str_val);
				g_print ("      %s='%s'\n", name, str_val);
				break;
			case GST_PROPS_INT_TYPE:
				gst_props_entry_get_int (entry, &int_val);
				g_print ("      %s=%d\n", name, int_val);
				break;
			default:
				break;
			}
			walk = g_list_next (walk);
		}
	}
	else {
		g_print (" unkown caps type\n");
	}
}

static void
info_print (GstMediaInfoStream *stream)
{
	int i;
	GList *p;
	GstMediaInfoTrack *track;
	
	g_print ("- mime type: %s\n", stream->mime);
	g_print ("- length: %.3f seconds\n",
		 (gdouble) stream->length_time / GST_SECOND);
	g_print ("- bitrate: %.3f kbps\n", stream->bitrate / 1000.0);
	g_print ("- number of tracks: %ld\n", stream->length_tracks);
	p = stream->tracks;
	if (p == NULL) {
		g_print ("- no track information, probably an error\n");
		return;
	}
	
	for (i = 0; i < stream->length_tracks; ++i) {
		g_print ("- track %d\n", i);
		track = (GstMediaInfoTrack *) p->data;
		g_print ("  - metadata:\n");
		caps_print (track->metadata);
		g_print ("  - streaminfo:\n");
		caps_print (track->streaminfo);
		g_print ("  - format:\n");
		caps_print (track->format);
		p = p->next;
	}
}
#endif

static void
init (MarlinFileSelection *fs)
{
	GtkWidget *hbox;
	
	fs->priv = g_new0 (MarlinFileSelectionPrivate, 1);

	
#if 0	
	fs->priv->selection = gtk_tree_view_get_selection (GTK_FILE_SELECTION (fs)->file_list);

	fs->priv->options_frame = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (fs)->main_vbox),
			    fs->priv->options_frame, FALSE, FALSE, 0);

	fs->priv->info_frame = gtk_vbox_new (FALSE, 0);
	/* Oh this is a hack if ever there was one...
	   Blame the GIMP boys for this... */
	for (hbox = GTK_FILE_SELECTION (fs)->dir_list; ! GTK_IS_HBOX (hbox); hbox = hbox->parent);
	
	gtk_box_pack_end (GTK_BOX (hbox), fs->priv->info_frame, FALSE, FALSE, 0);

	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (fs));
	build_info_contents (fs);
#endif
}

GType
marlin_file_selection_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		GTypeInfo info = {
			sizeof (MarlinFileSelectionClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinFileSelection), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (GTK_TYPE_FILE_CHOOSER_DIALOG,
					       "MarlinFileSelection",
					       &info, 0);
	}

	return type;
}

MarlinFileSelection *
marlin_file_selection_new (gboolean show_options,
			   gboolean show_info)
{
	MarlinFileSelection *fs;

	fs = g_object_new (MARLIN_FILE_SELECTION_TYPE,
			   "show_info", show_info,
			   "show_options", show_options,
			   NULL);

	return fs;
}

void
marlin_file_selection_set_options (MarlinFileSelection *fs,
				   GtkWidget *widget)
{
	g_return_if_fail (IS_MARLIN_FILE_SELECTION (fs));

	gtk_container_add (GTK_CONTAINER (fs->priv->options_frame), widget);
}

void
marlin_file_selection_set_info (MarlinFileSelection *fs,
				GtkWidget *widget)
{
	g_return_if_fail (IS_MARLIN_FILE_SELECTION (fs));

	gtk_box_pack_start (GTK_BOX (fs->priv->info_frame), widget, FALSE, FALSE, 0);
}
