/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dirent.h>
#include <sys/types.h>

#include <gmodule.h>
#include <glib.h>

#include <glib/gi18n.h>

#include <marlin/marlin-plugin.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-file-utils.h>

static GList *plugins_info = NULL;

GQuark
marlin_plugin_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-plugin-error-quark");
	}

	return quark;
}

static GSList *
get_plugins_in_dir (const char *path,
		    GSList *plugins,
		    int *count)
{
	DIR *dir;
	struct dirent *d;

	g_print ("Loading plugins from: %s\n", path);
	dir = opendir (path);
	if (dir == NULL) {
		g_warning ("Could not find the plugins dir: %s", path);

		return plugins;
	}

	while ((d = readdir (dir))) {
		char *p, *ext;

		ext = strrchr (d->d_name, '.');
		if (ext == NULL || strcmp (ext, ".so") != 0) {
			continue;
		}

		p = g_build_filename (path, d->d_name, NULL);
		plugins = g_slist_prepend (plugins, p);
		(*count)++;
	}

	closedir (dir);
	return plugins;
}

static GSList *
get_plugin_list (int *count)
{
	char *userdir;
	GSList *plugins = NULL;
	gboolean allow_user;

	plugins = get_plugins_in_dir (MARLIN_PLUGINS_DIR,
				      plugins, count);


	allow_user = marlin_gconf_get_bool ("/apps/marlin/allow-user-plugins");
	if (!allow_user) {
		return plugins;
	}

	userdir = g_build_filename (marlin_dot_dir (), "plugins", VERSION, NULL);
	plugins = get_plugins_in_dir (userdir, plugins, count);
	g_free (userdir);
	
	return plugins;
}
	
static void
free_plugin_list (GSList *list)
{
	GSList *l;

	for (l = list; l; l = l->next) {
		g_free (l->data);
	}

	g_slist_free (list);
}

/**
 * marlin_plugin_initialise:
 * @error:
 *
 * Initialises the plugin system, and loads all the plugins found in the
 * plugin directory.
 *
 * Returns: TRUE on success, FALSE on failure with more details in @error.
 */
gboolean
marlin_plugin_initialise (GError **error)
{
	GSList *plugins, *p;
	int count = 0;
	
	if (!g_module_supported ()) {
		if (error) {
			*error = g_error_new (MARLIN_PLUGIN_ERROR,
					      MARLIN_PLUGIN_NOT_SUPPORTED,
					      _("Plugins are not supported on this system"));
		}

		return FALSE;
	}

	/* Scan through the plugin directories */
	plugins = get_plugin_list (&count);

	for (p = plugins; p; p = p->next) {
		GModule *module;
		gpointer func;
		MarlinPluginInfo *(*marlin_plugin_module_init) (GModule *module,
								int *version);
		MarlinPluginInfo *info;
		int version;
		
		module = g_module_open (p->data, G_MODULE_BIND_LAZY);
		if (module == NULL) {
			g_warning ("Could not load: %s\n%s", (char *) p->data,
				   g_module_error ());
			continue;
		}

		if (!g_module_symbol (module, "marlin_plugin_module_register",
				      &func)) {
			g_warning ("Could not load %s: No registration function",
				   (char *) p->data);
			g_module_close (module);
			continue;
		}

		marlin_plugin_module_init = func;

		info = marlin_plugin_module_init (module, &version);
		/* Check version first*/
		if (version != MARLIN_PLUGIN_VERSION) {
			g_print ("Plugin was compiled for a different version %d:%d: %s\n",
				 version, MARLIN_PLUGIN_VERSION, 
				 (char *) p->data);
			g_module_close (module);
			continue;
		} else {
			g_print ("%s version matches\n", (char *) p->data);
		}

		if (info == NULL) {
			g_warning ("Invalid info returned from init: %s",
				   (char *) p->data);
			g_module_close (module);
			continue;
		}

		plugins_info = g_list_prepend (plugins_info, info);
	}

	free_plugin_list (plugins);

	return TRUE;
}

/**
 * marlin_plugin_get_list:
 *
 * Returns: A GList containing pointers to the MarlinPluginInfo structures for
 * each plugin.
 */
GList *
marlin_plugin_get_list (void)
{
	return plugins_info;
}
