/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib/gi18n.h>

#include <marlin/marlin-utils.h>

static GConfClient *client = NULL;

/**
 * marlin_ms_to_frames:
 * @ms: The length in milliseconds
 * @rate: The sample rate.
 *
 * Converts milliseconds into frames, at a given sample rate
 *
 * Returns: The number of frames.
 */
guint64
marlin_ms_to_frames (guint64 ms,
		     guint rate)
{
	float rate_ms;

	rate_ms = rate / 1000.0;

	return (guint64) (ms * rate_ms);
}

/**
 * marlin_frames_to_ms:
 * @frames: The length in frames.
 * @rate: The sample rate.
 *
 * Converts the frames into milliseconds at a given sample rate.
 *
 * Returns: The length in milliseconds.
 */
guint64
marlin_frames_to_ms (guint64 frames,
		     guint rate)
{
	float rate_ms;

	rate_ms = rate / 1000.0;

	return (guint64) (frames / rate_ms);
}

#define MS_PER_HOUR 3600000
#define MS_PER_MINUTE 60000
#define MS_PER_SECOND 1000
/**
 * marlin_ms_to_time_string:
 * @ms: The number of milliseconds
 *
 * Converts milliseconds to a string representation like 0:00:00.000
 *
 * Returns: A newly allocated string containing the time.
 */
char *
marlin_ms_to_time_string (guint64 ms)
{
	int hours, mins, secs, milli;

	hours = ms / MS_PER_HOUR;
        ms -= (hours * MS_PER_HOUR);
	
        mins = ms / MS_PER_MINUTE;
        ms -= (mins * MS_PER_MINUTE);
	
        secs = ms / MS_PER_SECOND;
        milli = ms - (secs * MS_PER_SECOND);

        return g_strdup_printf ("%d:%02d:%02d.%03d", hours, mins, secs, milli);
}

/**
 * marlin_ms_to_time_frame_string:
 * @ms: The number of milliseconds.
 * @rate: The sample rate.
 *
 * Converts milliseconds to a string representation like 0:00:00.000
 *
 * Returns: A newly allocated string containing the time.
 */
char *
marlin_ms_to_time_frame_string (guint64 ms,
				guint rate)
{
	int hours, mins, secs, frames;

	hours = ms / MS_PER_HOUR;
	ms -= (hours * MS_PER_HOUR);

	mins = ms / MS_PER_MINUTE;
	ms -= (mins * MS_PER_MINUTE);

	secs = ms / MS_PER_SECOND;
	ms -= (secs * MS_PER_SECOND);

	if (ms != 0) {
		frames = rate / ms;
	} else {
		frames = 0;
	}

        return g_strdup_printf ("%d:%02d:%02d.%d", hours, mins, secs, frames);
}

/**
 * marlin_ms_to_pretty_time:
 * @ms: The number of milliseconds
 * @include_milli: Should the string include milliseconds.
 *
 * Converts milliseconds to a string representation like
 * 1 hour 12 minutes 36.072 seconds
 *
 * Returns: A newly allocated string containing the time.
 */
char *
marlin_ms_to_pretty_time (guint64 ms,
			  gboolean include_milli)
{
	int hours, mins, secs, milli;
	char *ret, *s, *h, *m;
	
	hours = ms / 3600000;
        ms -= (hours * 3600000);
	
        mins = ms / 60000;
        ms -= (mins * 60000);
	
        secs = ms / 1000;
        milli = ms - (secs * 1000);

	if (milli == 0 || include_milli == FALSE) {
		s = g_strdup_printf (ngettext ("%d second", "%d seconds", secs), secs);
	} else {
		/* I'm not sure about this use of ngettext */
		s = g_strdup_printf (ngettext ("%d.%03d second", "%d.%03d seconds", milli), secs, milli);
	}

	m = g_strdup_printf (ngettext ("%d minute", "%d minutes", mins), mins);
	h = g_strdup_printf (ngettext ("%d hour", "%d hours", hours), hours);
	
	if (hours > 0) {
		if (mins > 0) {
			if (secs > 0) {
				ret = g_strdup_printf ("%s %s %s", h, m, s);
			} else {
				ret = g_strdup_printf ("%s %s", h, m);
			}
		} else {
			if (secs > 0) {
				ret = g_strdup_printf ("%s %s", h, s);
			} else {
				ret = g_strdup_printf ("%s", h);
			}
		}

		goto end;
	} else {
		if (mins > 0) {
			if (secs > 0) {
				ret = g_strdup_printf ("%s %s", m, s);
			} else {
				ret = g_strdup_printf ("%s", m);
			}
		} else {
			if (secs > 0) {
				ret = g_strdup (s);
			} else {
				ret = NULL;
			}
		}
		
		goto end;
	}

	ret = NULL;
 end:
	g_free (s);
	g_free (h);
	g_free (m);
	return ret;
}
	
/**
 * marlin_time_string_to_ms:
 * @str: The time represented as a string like hh:mm:ss.uuu
 *
 * Converts a time string into milliseconds.
 * The string is converted like so:
 * a) Anything after '.' are milliseconds
 * b) If there is no ':' in the string then the format is ss.uuu
 * c) If there is 1 ':' then the format is mm:ss.uuu
 * d) If there are 2 ':' then the format is hh:mm:ss.uuu
 *
 * Returns: The number of milliseconds.
 */
guint64
marlin_time_string_to_ms (const char *str)
{
	guint64 ms = (guint64) 0;
	char *point;
	int num_of_colons = 0, i;
	
	/* Search for point */
	point = strrchr (str, '.');
	if (point != NULL) {
		char ms_str[4];

		/* Make a string with 3 digits after the . */
		ms_str[3] = 0;
		if (point[1] == 0) {
			ms_str[0] = '0';
			ms_str[1] = '0';
			ms_str[2] = '0';
		} else {
			ms_str[0] = point[1];

			if (point[2] == 0) {
				ms_str[1] = '0';
				ms_str[2] = '0';
			} else {
				ms_str[1] = point[2];

				if (point[3] == 0) {
					ms_str[2] = '0';
				} else {
					ms_str[2] = point[3];
				}
			}
		}

		ms += (guint64) atoi (ms_str);
	}

	for (i = 0; str[i]; i++) {
		if (str[i] == ':') {
			num_of_colons++;
		}
	}

	if (num_of_colons == 0) {
		int seconds;
		/* We only have seconds */

		seconds = atoi (str);
		ms += (guint64) seconds * 1000;
		
	} else if (num_of_colons == 1) {
		int seconds, minutes;
		char *colon;
		/* We've got minutes up to : and seconds after. */

		minutes = atoi (str);
		colon = strchr (str, ':');
		seconds = atoi (colon + 1);

		ms += (guint64) ((minutes * 60000) + (seconds * 1000));
	} else {
		int seconds, minutes, hours;
		char *colon;

		/* Up to : is hours,
		   up to next : is minutes
		   seconds after */
		hours = atoi (str);
		colon = strchr (str, ':');
		minutes = atoi (colon + 1);
		colon = strchr (colon + 1, ':');
		seconds = atoi (colon + 1);

		ms += (guint64) ((hours * 60 * 60000) + (minutes * 60000) + (seconds * 1000));
	}
		
	return ms;
}
/**
 * marlin_time_frame_string_to_ms:
 * @str: The time represented as a string like hh:mm:ss.uuu
 *
 * Converts a time string into milliseconds.
 * The string is converted like so:
 * a) Anything after '.' are frames
 * b) If there is no ':' in the string then the format is ss.f
 * c) If there is 1 ':' then the format is mm:ss.f
 * d) If there are 2 ':' then the format is hh:mm:ss.f
 *
 * Returns: The number of milliseconds.
 */
guint64
marlin_time_frame_string_to_ms (const char *str)
{
	guint64 ms = (guint64) 0;
	char *point;
	int num_of_colons = 0, i;
	
	/* Search for point */
	point = strrchr (str, '.');
	if (point != NULL) {
		if (point[1] != 0) {
			ms += (guint64) atoi (point + 1);
		}
	}

	for (i = 0; str[i]; ++i) {
		if (str[i] == ':') {
			++num_of_colons;
		}
	}

	if (num_of_colons == 0) {
		int seconds;
		/* We only have seconds */

		seconds = atoi (str);
		ms += (guint64) seconds * 1000;
		
	} else if (num_of_colons == 1) {
		int seconds, minutes;
		char *colon;
		/* We've got minutes up to : and seconds after. */

		minutes = atoi (str);
		colon = strchr (str, ':');
		seconds = atoi (colon + 1);

		ms += (guint64) ((minutes * 60000) + (seconds * 1000));
	} else {
		int seconds, minutes, hours;
		char *colon;

		/* Up to : is hours,
		   up to next : is minutes
		   seconds after */
		hours = atoi (str);
		colon = strchr (str, ':');
		minutes = atoi (colon + 1);
		colon = strchr (colon + 1, ':');
		seconds = atoi (colon + 1);

		ms += (guint64) ((hours * 60 * 60000) + (minutes * 60000) + (seconds * 1000));
	}
		
	return ms;
}

/**
 * marlin_db_to_ratio:
 * @db: The number of decibels
 *
 * Converts db to a ratio
 *
 * Return value: The ratio.
 */
double
marlin_db_to_ratio (double db)
{
	if (db == MARLIN_INFINITE_DB) {
		return 0.0;
	} else {
		return pow (10.0, db / 20.0);
	}
}

/**
 * marlin_db_to_percent:
 * @db: The number of decibels
 *
 * Converts db to a percentage.
 *
 * Returns: The percentage.
 */
double
marlin_db_to_percent (double db)
{
	return 100.0 * marlin_db_to_ratio (db);
}

/**
 * marlin_percent_to_db:
 * @percent: The percentage level
 *
 * Converts a percentage to a db level
 *
 * Returns: The db
 */
double
marlin_percent_to_db (double percentage)
{
	return 20 * log (abs (percentage) / 100.0);
}

double
marlin_ratio_to_db (double ratio)
{
	return 20 * log (abs (ratio));
}

/**
 * marlin_gconf_get_int:
 * @key: The int key.
 *
 * Get an integer from GConf
 *
 * Returns: The integer at @key.
 */
int
marlin_gconf_get_int (const char *key)
{
	int result;
	
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	result = gconf_client_get_int (client, key, NULL);

	return result;
}

/**
 * marlin_gconf_set_int:
 * @key: The GConf key
 * @value: The value to set.
 *
 * Sets the value of @key to @value
 */
void
marlin_gconf_set_int (const char *key,
		      int value)
{
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	gconf_client_set_int (client, key, value, NULL);
}

/**
 * marlin_gconf_get_float:
 * @key: The GConf key.
 *
 * Gets a float from GConf.
 *
 * Returns: The value at @key.
 */
float
marlin_gconf_get_float (const char *key)
{
	float result;

	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	result = gconf_client_get_float (client, key, NULL);

	return result;
}

static GConfValue *
get_gconf_value (const char *key)
{
	GConfValue *val;	

	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	val = gconf_client_get (client, key, NULL);

	return val;
}

float
marlin_gconf_get_float_with_default (const char *key,
				     float       def)
{
	GConfValue *val;
	float result;

	val = get_gconf_value (key);

	if (val == NULL) {
		return def;
	}

	result = gconf_value_get_float (val);
	gconf_value_free (val);

	return result;
}

/**
 * marlin_gconf_set_float:
 * @key: The GConf key.
 * @value: The value to set.
 *
 * Sets the value of @key to @value.
 */
void
marlin_gconf_set_float (const char *key,
			float value)
{
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	gconf_client_set_float (client, key, value, NULL);
}

/**
 * marlin_gconf_get_string:
 * @key: The GConf key.
 *
 * Get a string out of GConf.
 *
 * Returns the string at @key.
 */
char *
marlin_gconf_get_string (const char *key)
{
	char *result;
	
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	result = gconf_client_get_string (client, key, NULL);

	return result;
}

/**
 * marlin_gconf_set_string:
 * @key: The GConf key.
 * @value: The value to set.
 *
 * Sets the value of @key to @value.
 */
void
marlin_gconf_set_string (const char *key,
			 const char *value)
{
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	gconf_client_set_string (client, key, value, NULL);
}

/**
 * marlin_gconf_get_bool:
 * @key: The GConf key.
 *
 * Gets the value associated with @key.
 *
 * Returns: A boolean.
 */
gboolean
marlin_gconf_get_bool (const char *key)
{
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	return gconf_client_get_bool (client, key, NULL);
}

/**
 * marlin_gconf_get_default:
 *
 * Gets the default GConf client that Marlin is using.
 *
 * Returns: A GConfClient that isn't to be unreffed.
 */
GConfClient *
marlin_gconf_get_default (void)
{
	if (client == NULL) {
		client = gconf_client_get_default ();
	}

	return client;
}

/**
 * marlin_gconf_destroy_default:
 *
 * Destroys Marlin's default GConf client
 *
 */
void
marlin_gconf_destroy_default (void)
{
	if (client != NULL) {
		g_object_unref (G_OBJECT (client));
	}

	client = NULL;
}

#define NUM_CHANNEL_DESCRIPTIONS 3
static const char *channel_descriptions [NUM_CHANNEL_DESCRIPTIONS] = {
	N_("Mono"),
	N_("Stereo"),
	NULL
};

/**
 * marlin_channels_to_string:
 * @channels: The number of channels.
 *
 * Returns: a string representing the number of channels.
 */
char *
marlin_channels_to_string (guint channels)
{
	if (channels >= NUM_CHANNEL_DESCRIPTIONS) {
		return g_strdup_printf ("%d", channels);
	}

	return g_strdup (_(channel_descriptions[channels - 1]));
}
