/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <marlin/marlin-channel.h>
#include <marlin/marlin-sample.h>

#include "marlin-channel-src.h"

#define MARLIN_CHANNEL_SRC_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_CHANNEL_SRC_TYPE, MarlinChannelSrcPrivate))

struct _MarlinChannelSrcPrivate {
	MarlinSample *sample;
	MarlinChannel *channel;

	GstCaps *caps;

	guint64 offset;
	guint32 offset_in_block;
	guint64 total_out;

	MarlinBlock *current_block;
};

static const GstElementDetails src_details =
	GST_ELEMENT_DETAILS ("Marlin channel src",
			     "Src",
			     "Outputs data from a MarlinChannel",
			     "Iain Holmes <iain@gnome.org>");

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_CHANNEL
};

static GstStaticPadTemplate src_factory =
	GST_STATIC_PAD_TEMPLATE ("src",
				 GST_PAD_SRC,
				 GST_PAD_ALWAYS,
				 GST_STATIC_CAPS ("audio/x-raw-float, "
						  "rate = (int) [1, MAX], "
						  "channels = (int) 1"));

static GstBaseSrcClass *parent_class = NULL;

static void
base_init (gpointer g_class)
{
	GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

	gst_element_class_set_details (element_class, &src_details);

	gst_element_class_add_pad_template 
		(element_class,
		 gst_static_pad_template_get (&src_factory));
}

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (object);
	priv = src->priv;

	if (priv->sample) {
		g_object_unref (priv->sample);
		priv->sample = NULL;
	}

	priv->channel = NULL;

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (object);
	priv = src->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		priv->sample = (MarlinSample *) g_value_dup_object (value);
		break;

	case PROP_CHANNEL:
		priv->channel = g_value_get_pointer (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (object);
	priv = src->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		break;

	case PROP_CHANNEL:
		break;
		
	default:
		break;
	}
}

static GstCaps *
src_get_caps (GstBaseSrc *basesrc)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;
	GstCaps *caps;
	guint rate;

	src = MARLIN_CHANNEL_SRC (basesrc);
	priv = src->priv;

	/* If we don't have a sample, then we don't know the rate */
	if (priv->sample == NULL) {
		caps = gst_caps_new_simple ("audio/x-raw-float",
					    "channels", G_TYPE_INT, 1,
					    "rate", GST_TYPE_INT_RANGE, 1, G_MAXINT,
					    NULL);
		return caps;
	}

	g_object_get (priv->sample,
		      "sample_rate", &rate,
		      NULL);

	if (priv->caps) {
		gst_caps_unref (priv->caps);
	}

	priv->caps = gst_caps_new_simple ("audio/x-raw-float",
					  "channels", G_TYPE_INT, 1,
					  "rate", G_TYPE_INT, rate,
					  "width", G_TYPE_INT, 32,
					  "endianness", G_TYPE_INT, G_BYTE_ORDER,
					  NULL);
	gst_caps_ref (priv->caps);

	return priv->caps;
}

static gboolean
src_is_seekable (GstBaseSrc *basesrc)
{
	/* Always seekable */
	return TRUE;
}

static gboolean
src_start (GstBaseSrc *basesrc)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (basesrc);
	priv = src->priv;

	priv->current_block = NULL;
	priv->total_out = 0;
	priv->offset = 0;
	priv->offset_in_block = 0;

	return TRUE;
}

static gboolean
src_stop (GstBaseSrc *basesrc)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (basesrc);
	priv = src->priv;

	priv->current_block = NULL;

	return TRUE;
}

static gboolean
src_event (GstBaseSrc *basesrc,
	   GstEvent   *event)
{
	return TRUE;
}

static gboolean
check_get_range (GstBaseSrc *basesrc)
{
	return TRUE;
}

static gboolean
src_get_size (GstBaseSrc *basesrc,
	      guint64    *size)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;

	src = MARLIN_CHANNEL_SRC (basesrc);
	priv = src->priv;

	if (G_UNLIKELY (priv->channel == NULL)) {
		return FALSE;
	}

	*size = (priv->channel->frames * sizeof (float));
	return TRUE;
}

static GstFlowReturn
src_create (GstBaseSrc *basesrc,
	    guint64     offset,
	    guint       length,
	    GstBuffer **ret)
{
	MarlinChannelSrc *src;
	MarlinChannelSrcPrivate *priv;
	GstBuffer *buf;
	float *out, *frame_data;
	guint64 frame_offset, left_in_block;
	guint frames_needed, frames_to_copy, copied;
	
	src = MARLIN_CHANNEL_SRC (basesrc);
	priv = src->priv;

	if (G_UNLIKELY (priv->channel == NULL)) {
		return GST_FLOW_NOT_NEGOTIATED;
	}

	frame_offset = (offset / sizeof (float));
	if (G_UNLIKELY (priv->offset != frame_offset)) {
		if (frame_offset > priv->channel->frames) {
			return GST_FLOW_ERROR;
		}

		priv->offset = frame_offset;

		/* Reset here so we pick up the new block later */
		priv->current_block = NULL;
	}

	/* Get the next block */
	if (G_UNLIKELY (priv->current_block == NULL)) {
		priv->current_block = marlin_channel_get_block_for_frame
			(priv->channel, priv->offset);
		g_assert (priv->current_block != NULL);
	}
	
	if (priv->offset > priv->current_block->end &&
	    priv->offset < priv->channel->frames) {
		priv->current_block = marlin_block_next (priv->current_block);
		g_assert (priv->current_block != NULL);

		priv->offset_in_block = 0;
	} else {
		priv->offset_in_block = priv->offset - 
			priv->current_block->start;
	}

	left_in_block = priv->current_block->num_frames - priv->offset_in_block;

	buf = gst_buffer_new_and_alloc (length);
	out = (float *) GST_BUFFER_DATA (buf);

	WRITE_LOCK (priv->channel->lock);

	copied = 0;
	frames_needed = length / sizeof (float);

 copy_data:
	frame_data = marlin_block_get_frame_data (priv->current_block);

	frames_to_copy = MIN (frames_needed, left_in_block);
	memcpy (out, frame_data + priv->offset_in_block,
		frames_to_copy * sizeof (float));
	copied += frames_to_copy;

	/* We didn't fill the whole buffer because there weren't 
	   enough frames left in priv->current_block */
	if (left_in_block < frames_needed) {
		out += frames_to_copy;

		frames_needed -= frames_to_copy;
		if (frames_needed > 0) {
			priv->current_block = marlin_block_next (priv->current_block);
			g_assert (priv->current_block != NULL);

			priv->offset_in_block = 0;
		
			left_in_block = priv->current_block->num_frames;

			/* Copy a bit more */
			goto copy_data;
		}
	}

	/* Update the block pointer */
	priv->offset += copied;
	left_in_block -= copied;
	
	priv->total_out += copied;
	
	WRITE_UNLOCK (priv->channel->lock);
	
	GST_BUFFER_SIZE (buf) = copied * sizeof (float);
	GST_BUFFER_OFFSET (buf) = offset;
	GST_BUFFER_OFFSET_END (buf) = offset + GST_BUFFER_SIZE (buf);
	gst_buffer_set_caps (buf, priv->caps);
	*ret = buf;

	return GST_FLOW_OK;
}

static void
class_init (MarlinChannelSrcClass *klass)
{
	GObjectClass *o_class;
	GstElementClass *e_class;
	GstBaseSrcClass *b_class;

	g_type_class_add_private (klass, sizeof (MarlinChannelSrcPrivate));

	o_class = (GObjectClass *) klass;
	e_class = (GstElementClass *) klass;
	b_class = (GstBaseSrcClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	b_class->get_caps = src_get_caps;
	b_class->is_seekable = src_is_seekable;
	b_class->start = src_start;
	b_class->stop = src_stop;
	b_class->event = src_event;
	b_class->get_size = src_get_size;
	b_class->check_get_range = check_get_range;
	b_class->create = src_create;

	g_object_class_install_property (o_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class,
					 PROP_CHANNEL,
					 g_param_spec_pointer ("channel",
							       "", "",
							       G_PARAM_READWRITE));
}

static void
init (MarlinChannelSrc *src)
{
	MarlinChannelSrcPrivate *priv;

	src->priv = MARLIN_CHANNEL_SRC_GET_PRIVATE (src);
	priv = src->priv;
}

GType
marlin_channel_src_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MarlinChannelSrcClass),
			base_init,
			NULL, (GClassInitFunc) class_init,
			NULL, NULL, sizeof (MarlinChannelSrc),
			0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (GST_TYPE_BASE_SRC,
					       "MarlinChannelSrc",
					       &info, 0);
	}

	return type;
}
