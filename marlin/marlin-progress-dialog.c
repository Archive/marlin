/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-utils.h>

#include <marlin/marlin-progress-dialog.h>
#include <marlin/marlin-progress-dialog-icons.h>

enum {
	PROP_0,
	PROP_OPERATION,
	PROP_PRIMARY,
	PROP_SECONDARY,
};

struct _MarlinProgressDialogPrivate {
	MarlinOperation *operation;
	gulong signal_id;
	gulong op_id;

	GtkWidget *label;
	GtkWidget *progress_bar;
	GtkWidget *parent_window;
	GtkWidget *sub_process;
	GtkWidget *pause;

	GTimer *timer;
	int progress_jar_position;
};

static GdkPixbuf *empty_jar_pixbuf = NULL, *full_jar_pixbuf = NULL;
#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_PROGRESS_DIALOG_TYPE, MarlinProgressDialogPrivate))
G_DEFINE_TYPE (MarlinProgressDialog, marlin_progress_dialog, GTK_TYPE_DIALOG);

static void
finalize (GObject *object)
{
	MarlinProgressDialog *dialog;
	MarlinProgressDialogPrivate *priv;

	dialog = MARLIN_PROGRESS_DIALOG (object);
	priv = dialog->priv;

	if (priv->timer) {
		g_timer_destroy (priv->timer);
	}

	if (priv->operation) {
		g_signal_handler_disconnect (priv->operation,
					     priv->op_id);
	}

	G_OBJECT_CLASS (marlin_progress_dialog_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinProgressDialog *dialog;
	MarlinProgressDialogPrivate *priv;

	dialog = MARLIN_PROGRESS_DIALOG (object);
	priv = dialog->priv;

	if (priv->operation) {
		g_object_unref (G_OBJECT (priv->operation));
		priv->operation = NULL;
	}

	G_OBJECT_CLASS (marlin_progress_dialog_parent_class)->dispose (object);
}

static char *
make_eta_string (const char *t)
{
	return g_strdup_printf (_("About %s remaining."), t);
}

static void
update_icon (MarlinProgressDialog *dialog,
	     double fraction)
{
	MarlinProgressDialogPrivate *priv = dialog->priv;
	GdkPixbuf *pixbuf;
	int position;

	position = gdk_pixbuf_get_height (empty_jar_pixbuf) * (1 - fraction);

	if (position == priv->progress_jar_position) {
		return;
	}

	priv->progress_jar_position = position;

	pixbuf = gdk_pixbuf_copy (empty_jar_pixbuf);
	gdk_pixbuf_copy_area (full_jar_pixbuf,
			      0, position,
			      gdk_pixbuf_get_width (pixbuf),
			      gdk_pixbuf_get_height (pixbuf) - position,
			      pixbuf,
			      0, position);

 	gtk_window_set_icon (GTK_WINDOW (dialog), pixbuf);
	g_object_unref (pixbuf);
}

static void
update_progress (MarlinProgressDialog *dialog,
		 int progress)
{
	MarlinProgressDialogPrivate *priv = dialog->priv;
	char *text, *t;
	double dt, length;
	int left;

	if (progress != -1) {
		left = 100 - progress;

		dt = g_timer_elapsed (priv->timer, NULL);

		length = (dt / (double) progress) * left;

		t = marlin_ms_to_pretty_time ((guint64) ((length + 1) * 1000), FALSE);

		text = make_eta_string (t);
		g_free (t);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (priv->progress_bar),
					   text);
		g_free (text);
	} else {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (priv->progress_bar), _("Unknown"));
		progress = 0;
	}

	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (priv->progress_bar),
				       (float)progress / 100.0);

	update_icon (dialog, (float) progress / 100.0);
}

static void
operation_progress (MarlinOperation *operation,
		    int progress,
		    MarlinProgressDialog *dialog)
{
	update_progress (dialog, progress);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinProgressDialog *dialog = (MarlinProgressDialog *) object;
	MarlinProgressDialogPrivate *priv = dialog->priv;
	const char *text;

	switch (prop_id) {
	case PROP_OPERATION:
		priv->operation = g_value_get_object (value);
		priv->op_id = g_signal_connect (priv->operation, "progress",
						G_CALLBACK (operation_progress),
						dialog);
		break;

	case PROP_PRIMARY:
		text = g_value_get_string (value);
		gtk_label_set_markup (GTK_LABEL (priv->label), text);
		break;

	case PROP_SECONDARY:
		text = g_value_get_string (value);
		gtk_label_set_markup (GTK_LABEL (priv->sub_process), text);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
}

static void
dialog_response (GtkDialog *dialog,
		 int response_id)
{
	MarlinProgressDialog *pd = MARLIN_PROGRESS_DIALOG (dialog);

	switch (response_id) {
	case GTK_RESPONSE_CANCEL:
		marlin_operation_cancel (pd->priv->operation);
		break;

	default:
		break;
	}
}

static void
marlin_progress_dialog_class_init (MarlinProgressDialogClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	GtkDialogClass *dialog_class = (GtkDialogClass *) klass;

	object_class->finalize = finalize;
	object_class->dispose = dispose;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	dialog_class->response = dialog_response;

	g_type_class_add_private (object_class, sizeof (MarlinProgressDialogPrivate));
	g_object_class_install_property (object_class,
					 PROP_OPERATION,
					 g_param_spec_object ("operation",
							      "", "",
							      MARLIN_OPERATION_TYPE,
							      G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
					 PROP_PRIMARY,
					 g_param_spec_string ("primary_text",
							      "", "",
							      "",
							      G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
					 PROP_SECONDARY,
					 g_param_spec_string ("secondary_text",
							      "", "",
							      "",
							      G_PARAM_WRITABLE));
}

static void
pause_toggled (GtkToggleButton *tb,
	       MarlinProgressDialog *dialog)
{
	gboolean pause;
	MarlinProgressDialogPrivate *priv = dialog->priv;

	pause = gtk_toggle_button_get_active (tb);
	if (pause) {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (priv->progress_bar), _("(Paused)"));
	} else {
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (priv->progress_bar), "");
	}

	marlin_operation_pause (priv->operation, gtk_toggle_button_get_active (tb));
}

static void
marlin_progress_dialog_init (MarlinProgressDialog *dialog)
{
	MarlinProgressDialogPrivate *priv;
	GtkWidget *vbox;
	GdkCursor *cursor;

	priv = dialog->priv = GET_PRIVATE (dialog);

	if (empty_jar_pixbuf == NULL) {
		empty_jar_pixbuf = gdk_pixbuf_new_from_inline (-1, progress_jar_empty_icon, FALSE, NULL);
		full_jar_pixbuf = gdk_pixbuf_new_from_inline (-1, progress_jar_full_icon, FALSE, NULL);
	}

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

 	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);

	priv->pause = gtk_toggle_button_new_with_mnemonic (_("_Pause"));
	GTK_WIDGET_SET_FLAGS (priv->pause, GTK_CAN_DEFAULT);
	g_signal_connect (priv->pause, "toggled",
			  G_CALLBACK (pause_toggled), dialog);
	gtk_widget_show (priv->pause);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      priv->pause, GTK_RESPONSE_NONE);

	/* Realise dialog do that it has a GdkWindow associated with it */
	gtk_widget_realize (GTK_WIDGET (dialog));

	cursor = gdk_cursor_new (GDK_WATCH);
	gdk_window_set_cursor (GTK_WIDGET (dialog)->window, cursor);
	gdk_cursor_unref (cursor);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);

	priv->label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (priv->label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (priv->label), TRUE);
	gtk_label_set_use_markup (GTK_LABEL (priv->label), TRUE);

	gtk_box_pack_start (GTK_BOX (vbox), priv->label, FALSE, FALSE, 0);
	gtk_widget_show (priv->label);

	priv->progress_bar = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), priv->progress_bar,
			    FALSE, FALSE, 0);

	priv->sub_process = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (priv->sub_process), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (priv->sub_process), FALSE);
	gtk_label_set_use_markup (GTK_LABEL (priv->sub_process), TRUE);

	gtk_box_pack_start (GTK_BOX (vbox), priv->sub_process,
			    FALSE, FALSE, 0);
	/* Hide the sub process label until there is something to display */
	gtk_widget_hide (priv->sub_process);

	priv->timer = g_timer_new ();

	gtk_widget_show (priv->progress_bar);
}

/**
 * marlin_progress_dialog_new:
 * @operation:
 * @parent_window:
 * @primary:
 * @secondary:
 *
 * Creates a new MarlinProgressDialog.
 *
 * Returns: A newly created MarlinProgressDialog.
 */
MarlinProgressDialog *
marlin_progress_dialog_new (MarlinOperation *operation,
			    GtkWindow *parent_window,
			    const char *primary,
			    const char *secondary)
{
	MarlinProgressDialog *dialog;

	dialog = g_object_new (MARLIN_PROGRESS_DIALOG_TYPE,
			       "operation", operation,
			       "primary_text", primary,
			       "secondary_text", secondary,
			       NULL);

	return dialog;
}
