/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_RECORD_PIPELINE_H__
#define __MARLIN_RECORD_PIPELINE_H__

#include <marlin/marlin-pipeline.h>

#define MARLIN_RECORD_PIPELINE_TYPE (marlin_record_pipeline_get_type ())
#define MARLIN_RECORD_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_RECORD_PIPELINE_TYPE, MarlinRecordPipeline))
#define MARLIN_RECORD_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_RECORD_PIPELINE_TYPE, MarlinRecordPipelineClass))
#define IS_MARLIN_RECORD_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_RECORD_PIPELINE_TYPE))
#define IS_MARLIN_RECORD_PIPELINE_CLASS(klasS) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_RECORD_PIPELINE_TYPE))

typedef struct _MarlinRecordPipeline MarlinRecordPipeline;
typedef struct _MarlinRecordPipelineClass MarlinRecordPipelineClass;
typedef struct _MarlinRecordPipelinePrivate MarlinRecordPipelinePrivate;

struct _MarlinRecordPipeline {
	MarlinPipeline parent_pipeline;

	MarlinRecordPipelinePrivate *priv;
};

struct _MarlinRecordPipelineClass {
	MarlinPipelineClass parent_class;

	void (*level) (GstElement *element,
		       double time, 
		       int channel,
		       double rms,
		       double peak,
		       double decay);
};

GType marlin_record_pipeline_get_type (void);
MarlinRecordPipeline *marlin_record_pipeline_new (void);
void marlin_record_pipeline_set_eos (MarlinRecordPipeline *pipeline);

#endif
