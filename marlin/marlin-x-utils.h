/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_X_UTILS_H__
#define __MARLIN_X_UTILS_H__

#include <gtk/gtk.h>

#define PACK(t, w, l, top, x) gtk_table_attach (GTK_TABLE ((t)), (w), \
                                             (l), (l) + 1, \
                                             (top), top + 1, \
                                             (x), GTK_FILL, 0, 0)
#define PACK_FULL(t, w, l, top, x, y) gtk_table_attach (GTK_TABLE ((t)), (w), \
                                             (l), (l) + 1, \
                                             (top), top + 1, \
                                             (x), (y), 0, 0)

#define MARLIN_ROW_SPACING 6
#define MARLIN_COL_SPACING 6

GtkWidget *marlin_make_title_label (const char *text);
GtkWidget *marlin_make_info_label (const char *text);

void marlin_display_help (const char *section);

char *marlin_set_volume_digits (GtkScale *scale,
				double db,
				gpointer data);
GtkWidget * marlin_make_table (int rows,
			       int columns,
			       gboolean same_size);
void marlin_menu_position_under_widget (GtkMenu *menu,
					int *x,
					int *y,
					gboolean *push_in,
					gpointer user_data);
GtkWidget *marlin_make_button (const char *text,
			       const char *stock_id);
GtkWidget *marlin_add_button_to_dialog (GtkDialog *dialog,
					const char *text,
					const char *stock_id,
					int response_id);
GtkWidget *marlin_make_spacer (void);
GtkWidget *marlin_make_image_button (const char *stock_id,
				     GCallback callback,
				     gpointer closure);

void make_marker_combo_menu (GtkComboBox *combo,
			     GList *markers);

void marlin_popup_menu_attach (GtkWidget *popup,
			       GtkWidget *widget);
#endif
