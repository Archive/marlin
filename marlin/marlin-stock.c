/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <gtk/gtk.h>

#include <marlin-file-utils.h>
#include <marlin-stock.h>

static const char *items [] =
{
	MARLIN_STOCK_PLAY,
	MARLIN_STOCK_PAUSE,
	MARLIN_STOCK_STOP,
	MARLIN_STOCK_PREVIOUS,
	MARLIN_STOCK_NEXT,
	MARLIN_STOCK_RECORD,
	MARLIN_STOCK_FFWD,
	MARLIN_STOCK_REWIND,
	MARLIN_STOCK_REPEAT,
	MARLIN_STOCK_NEW_SAMPLE,
	MARLIN_STOCK_SELECT_ALL,
	MARLIN_STOCK_SELECT_NONE,
	MARLIN_STOCK_SELECTION_SHRINK,
	MARLIN_STOCK_SELECTION_GROW,
	MARLIN_STOCK_SOUND_PROPERTIES,
	MARLIN_STOCK_PASTE_AS_NEW,
	MARLIN_STOCK_CDDA_EXTRACT,
	MARLIN_STOCK_VZOOM_IN,
	MARLIN_STOCK_HZOOM_IN,
	MARLIN_STOCK_VZOOM_OUT,
	MARLIN_STOCK_HZOOM_OUT,
	MARLIN_STOCK_UNDO_HISTORY,
	MARLIN_STOCK_PASTE_INTO,
	MARLIN_STOCK_NEW_VIEW
};

void
marlin_stock_icons_register (void)
{
	GtkIconFactory *factory;
	int i;

	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);

	for (i = 0; i < (int) G_N_ELEMENTS (items); i++) {
		GtkIconSet *icon_set;
		GdkPixbuf *pixbuf;
		char *filename, *fullname;

		filename = g_strconcat ("marlin/", items[i], ".png", NULL);
		fullname = marlin_file (filename);
		g_free (filename);
		
		pixbuf = gdk_pixbuf_new_from_file (fullname, NULL);
		g_free (fullname);

		icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
		gtk_icon_factory_add (factory, items[i], icon_set);
		gtk_icon_set_unref (icon_set);

		g_object_unref (G_OBJECT (pixbuf));
	}

	g_object_unref (G_OBJECT (factory));
}
