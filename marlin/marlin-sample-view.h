/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_SAMPLE_VIEW_H__
#define __MARLIN_SAMPLE_VIEW_H__

#include <gtk/gtk.h>

#include <marlin/marlin-types.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_SAMPLE_VIEW_TYPE (marlin_sample_view_get_type ())
#define MARLIN_SAMPLE_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_SAMPLE_VIEW_TYPE, MarlinSampleView))
#define MARLIN_SAMPLE_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_SAMPLE_VIEW_TYPE, MarlinSampleViewClass))
#define IS_MARLIN_SAMPLE_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_SAMPLE_VIEW_TYPE))
#define IS_MARLIN_SAMPLE_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_SAMPLE_VIEW_TYPE))

typedef struct _MarlinSampleView MarlinSampleView;
typedef struct _MarlinSampleViewClass MarlinSampleViewClass;
typedef struct _MarlinSampleViewPrivate MarlinSampleViewPrivate;

struct _MarlinSampleView {
	GtkWidget parent;

	MarlinSampleViewPrivate *priv;
};

struct _MarlinSampleViewClass {
	GtkWidgetClass parent_class;

	void (*set_scroll_adjustments) (MarlinSampleView *view,
					GtkAdjustment *hadjustment,
					GtkAdjustment *vadjustment);

	void (*frames_per_pixel_changed) (MarlinSampleView *view,
					  guint frames_per_pixel);
	void (*page_start_changed) (MarlinSampleView *view,
				    guint64 page_start);
	void (*move_cursor) (MarlinSampleView *view,
			     GtkMovementStep step,
			     int count,
			     gboolean extend_selection);
	void (*vzoom_changed) (MarlinSampleView *view,
			       float vmax,
			       float vmin);
};

GType marlin_sample_view_get_type ();
GtkWidget *marlin_sample_view_new (guint frames_per_pixel);
void marlin_sample_view_scroll_to (MarlinSampleView *view,
				   guint64 start_frame);
gboolean marlin_sample_view_can_zoom_in (MarlinSampleView *view);
gboolean marlin_sample_view_can_zoom_out (MarlinSampleView *view);
gboolean marlin_sample_view_can_vzoom_in (MarlinSampleView *view);
gboolean marlin_sample_view_can_vzoom_out (MarlinSampleView *view);

void marlin_sample_view_vzoom_in (MarlinSampleView *view);
void marlin_sample_view_vzoom_out (MarlinSampleView *view);



void marlin_sample_view_open_move_cursor_dialog (MarlinSampleView *view,
						 GtkWindow *parent);
#ifdef __cplusplus
}
#endif

#endif
