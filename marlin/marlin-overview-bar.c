/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright (C) 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib-object.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>

#include <marlin/marlin-cursors.h>
#include <marlin/marlin-overview-bar.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-drawing.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-types.h>

enum {
	CURSOR_CHANGED,
	PAGE_START_CHANGED,
	PLAY_REQUEST,
	MOVE_PAGE,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_PER_PAGE,
	PROP_POSITION,
	PROP_PAGE_START,
	PROP_PER_PIXEL,
};

struct _VMarker {
	MarlinMarker *marker;
	guint64 real_position;
};

struct _MarlinOverviewBarPrivate {
	MarlinSample *sample; /* The sample */
	guint notify_id;

	MarlinMarkerModel *model;
	guint32 add_id, remove_id, move_id;

	guint64 number_of_frames; /* The number of frame that are in
				    the sample */
	guint64 frames_per_page; /* The number of frames that are shown
				   in one page of the overview_bar */
	guint frames_per_pixel; /* The number of frames that one pixel
				    represents */
	guint num_channels; /* The number of channels in the sample */

	guint64 position; /* The position of the cursor */
	guint64 start, finish; /* The position of the page */

	MarlinSampleSelection *selection; /* Selection reference */
	guint changed_id;

	gboolean in_drag; /* Is the overview bar wearing woman's clothing? */
	int drag_offset; /* The offset from the point the page was grabbed
			    to the start of the page in frames */
	gboolean in_page; /* Is the cursor inside the page boundary, but the
			     button hasn't been clicked? */

	GList *markers;
	GHashTable *marker_to_view;

	MarlinSampleDrawContext *dc;

	MarlinPeak **peaks;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_OVERVIEW_BAR_TYPE, MarlinOverviewBarPrivate))
G_DEFINE_TYPE (MarlinOverviewBar, marlin_overview_bar, GTK_TYPE_DRAWING_AREA);
static guint signals[LAST_SIGNAL];

#define DEFAULT_FRAMES_PER_PIXEL 64

static void
clear_markers (MarlinOverviewBar *bar)
{
	GList *p;

	for (p = bar->priv->markers; p; p = p->next) {
		g_free (p->data);
	}
	g_list_free (bar->priv->markers);

	if (bar->priv->marker_to_view != NULL) {
		g_hash_table_destroy (bar->priv->marker_to_view);
		bar->priv->marker_to_view = NULL;
	}
}

static void
add_markers (MarlinOverviewBar *bar,
	     GList *marks)
{
	MarlinOverviewBarPrivate *priv;
	GList *p;

	priv = bar->priv;

	if (priv->marker_to_view == NULL) {
		priv->marker_to_view = g_hash_table_new (NULL, NULL);
	}

	for (p = marks; p; p = p->next) {
		struct _VMarker *vm = g_new (struct _VMarker, 1);

		vm->marker = p->data;
		vm->real_position = vm->marker->position;

		priv->markers = g_list_prepend (priv->markers, vm);
		g_hash_table_insert (priv->marker_to_view, p->data, vm);
	}
}

static void
free_peaks (MarlinOverviewBar *bar)
{
	int i;

	for (i = 0; i < bar->priv->num_channels; i++) {
		g_free (bar->priv->peaks[i]);
	}
 	g_free (bar->priv->peaks);
}

static void
create_peaks (MarlinOverviewBar *bar)
{
	GtkWidget *widget = (GtkWidget *) bar;
	int i;

	bar->priv->peaks = g_new (MarlinPeak *, bar->priv->num_channels);
	for (i = 0; i < bar->priv->num_channels; ++i) {
		bar->priv->peaks[i] = g_new0 (MarlinPeak, widget->allocation.width);
	}
}

static void
finalize (GObject *object)
{
	MarlinOverviewBar *overview_bar;
	MarlinOverviewBarPrivate *priv;

	overview_bar = MARLIN_OVERVIEW_BAR (object);

	priv = overview_bar->priv;

	if (priv->selection != NULL) {
		g_signal_handler_disconnect (G_OBJECT (priv->selection),
					     priv->changed_id);
		g_object_unref (G_OBJECT (priv->selection));
	}

	if (priv->move_id > 0) {
		g_signal_handler_disconnect (priv->model, priv->move_id);
	}
	if (priv->add_id > 0) {
		g_signal_handler_disconnect (priv->model, priv->add_id);
	}
	if (priv->remove_id > 0) {
		g_signal_handler_disconnect (priv->model, priv->remove_id);
	}

	if (priv->model != NULL) {
		g_object_unref (G_OBJECT (priv->model));
	}

	if (priv->peaks != NULL) {
		free_peaks (overview_bar);
	}

	clear_markers (overview_bar);
	if (priv->sample) {
		g_signal_handler_disconnect (priv->sample, priv->notify_id);
		g_object_unref (G_OBJECT (priv->sample));
	}

	marlin_sample_draw_context_free (priv->dc);

	G_OBJECT_CLASS (marlin_overview_bar_parent_class)->finalize (object);
}

static void
redraw_backing_store (MarlinOverviewBar *bar)
{
	GdkRectangle area;

	area.x = 0;
	area.y = 0;
	area.width = ((GtkWidget *) bar)->allocation.width;
	area.height = ((GtkWidget *) bar)->allocation.height;

	marlin_sample_draw_to_buffer (bar->priv->dc,
				      bar->priv->peaks,
				      &area);
}

static void
invalidate_widget (GtkWidget *widget)
{
	GdkRectangle window;

	if (GTK_WIDGET_DRAWABLE (widget)) {
		window.x = 0;
		window.y = 0;
		window.width = widget->allocation.width;
		window.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window,
					    &window, FALSE);
	}
}

static void
selection_changed (MarlinSampleSelection *selection,
		   MarlinOverviewBar *overview_bar)
{
	invalidate_widget (GTK_WIDGET (overview_bar));
}

static void
sample_notify (MarlinSample *sample,
	       GParamSpec   *pspec,
	       MarlinOverviewBar *view)
{
	GtkWidget *widget = GTK_WIDGET (view);
	MarlinOverviewBarPrivate *priv = view->priv;

	if (strcmp (pspec->name, "total-frames") == 0) {
		g_object_get (G_OBJECT (sample),
			      "total_frames", &priv->number_of_frames,
			      NULL);
		priv->frames_per_pixel = (int) ((priv->number_of_frames / widget->allocation.width) + 0.5);
		if (priv->frames_per_pixel == 0) {
			priv->frames_per_pixel = 1;
		}
		priv->dc->fpp = priv->frames_per_pixel;

		redraw_backing_store (view);
		if (GTK_WIDGET_DRAWABLE (widget)) {
			invalidate_widget (widget);
		}

		return;
	}

	if (strcmp (pspec->name, "channels") == 0) {
		free_peaks (view);

		g_object_get (G_OBJECT (sample),
			      "channels", &priv->num_channels,
			      NULL);

		create_peaks (view);

		redraw_backing_store (view);
		if (GTK_WIDGET_DRAWABLE (widget)) {
			invalidate_widget (widget);
		}

		return;
	}

	if (strcmp (pspec->name, "dirty") == 0) {
		redraw_backing_store (view);
		if (GTK_WIDGET_DRAWABLE (widget)) {
			invalidate_widget (widget);
		}

		return;
	}
}

static void
add_marker (MarlinMarkerModel *model,
	    MarlinMarker *marker,
	    MarlinOverviewBar *bar)
{
	MarlinOverviewBarPrivate *priv;
	struct _VMarker *vm;
	GtkWidget *widget = (GtkWidget *) bar;

	priv = bar->priv;

	vm = g_new (struct _VMarker, 1);
	vm->marker = marker;
	vm->real_position = marker->position;

	g_hash_table_insert (priv->marker_to_view, marker, vm);
	priv->markers = g_list_prepend (priv->markers, vm);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;

		area.x = vm->real_position / priv->frames_per_pixel;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

static void
remove_marker (MarlinMarkerModel *model,
	       MarlinMarker *marker,
	       MarlinOverviewBar *bar)
{
	MarlinOverviewBarPrivate *priv;
	GtkWidget *widget = (GtkWidget *) bar;
	struct _VMarker *vm;
	guint64 pos;

	priv = bar->priv;

	vm = g_hash_table_lookup (priv->marker_to_view, marker);
	g_assert (vm != NULL);

	g_hash_table_remove (priv->marker_to_view, marker);
	priv->markers = g_list_remove (priv->markers, vm);

	pos = vm->real_position;
	g_free (vm);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;

		area.x = pos / priv->frames_per_pixel;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

static void
change_marker (MarlinMarkerModel *model,
	       MarlinMarker *marker,
	       MarlinOverviewBar *bar)
{
	MarlinOverviewBarPrivate *priv;
	GtkWidget *widget = (GtkWidget *) bar;
	struct _VMarker *vm;
	guint64 pos;

	priv = bar->priv;

	vm = g_hash_table_lookup (priv->marker_to_view, marker);
	g_assert (vm != NULL);

	pos = vm->real_position;
	vm->real_position = marker->position;

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;

		area.x = pos / priv->frames_per_pixel;
		area.y = 0;
		area.width = 1;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	GtkWidget *widget;
	MarlinOverviewBar *overview_bar;
	MarlinOverviewBarPrivate *priv;
	MarlinSample *new_sample;
	int old_finish, min_finish, max_finish;
	int old_start, min_start, max_start;
	int old_position;
	GList *marks;

	widget = GTK_WIDGET (object);
	overview_bar = MARLIN_OVERVIEW_BAR (object);
	priv = overview_bar->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		new_sample = g_value_get_object (value);
		if (priv->sample == new_sample) {
			if (priv->peaks) {
				free_peaks (overview_bar);
			}

			g_object_get (G_OBJECT (priv->sample),
				      "total_frames", &priv->number_of_frames,
				      "channels", &priv->num_channels,
				      NULL);

			priv->frames_per_pixel = (int)(priv->number_of_frames / widget->allocation.width);
			if (priv->frames_per_pixel == 0) {
				priv->frames_per_pixel = 1;
			}
			priv->dc->fpp = priv->frames_per_pixel;

			create_peaks (overview_bar);
			redraw_backing_store (overview_bar);

			if (GTK_WIDGET_DRAWABLE (widget)) {
				invalidate_widget (widget);
			}

			return;
		}

		if (priv->sample != NULL) {
			g_object_unref (G_OBJECT (priv->sample));
		}

		if (priv->move_id > 0) {
			g_signal_handler_disconnect (priv->model, priv->move_id);
		}
		if (priv->add_id > 0) {
			g_signal_handler_disconnect (priv->model, priv->add_id);
		}
		if (priv->remove_id > 0) {
			g_signal_handler_disconnect (priv->model, priv->remove_id);
		}

		if (priv->model != NULL) {
			g_object_unref (G_OBJECT (priv->model));
		}

		clear_markers (overview_bar);

		if (priv->selection != NULL) {
			g_signal_handler_disconnect (G_OBJECT (priv->selection),
						     priv->changed_id);
			g_object_unref (priv->selection);
		}

		if (priv->peaks) {
			free_peaks (overview_bar);
		}

		priv->sample = new_sample;
		g_object_ref (G_OBJECT (priv->sample));

		g_object_get (G_OBJECT (priv->sample),
			      "total_frames", &priv->number_of_frames,
			      "selection", &priv->selection,
			      "channels", &priv->num_channels,
			      "markers", &priv->model,
			      NULL);

		create_peaks (overview_bar);

		priv->dc->sample = new_sample;

/* 		g_object_ref (priv->sample); */
		priv->notify_id = g_signal_connect (G_OBJECT (priv->sample),
						    "notify",
						    G_CALLBACK (sample_notify),
						    overview_bar);
/* 		g_object_ref (priv->selection); */
		priv->changed_id = g_signal_connect (G_OBJECT (priv->selection),
						     "changed",
						     G_CALLBACK (selection_changed),
						     overview_bar);
		g_object_get (G_OBJECT (priv->model),
			      "markers", &marks,
			      NULL);

		add_markers (overview_bar, marks);

		priv->add_id = g_signal_connect (priv->model, "marker-added",
						 G_CALLBACK (add_marker),
						 overview_bar);
		priv->remove_id = g_signal_connect (priv->model, "marker-removed",
						    G_CALLBACK (remove_marker),
						    overview_bar);
		priv->move_id = g_signal_connect (priv->model, "marker-changed",
						  G_CALLBACK (change_marker),
						  overview_bar);

		priv->frames_per_pixel = (guint) (priv->number_of_frames / widget->allocation.width);
		if (priv->frames_per_pixel == 0) {
			priv->frames_per_pixel = 1;
		}
		priv->dc->fpp = priv->frames_per_pixel;

		redraw_backing_store (overview_bar);
		if (GTK_WIDGET_DRAWABLE (widget)) {
			invalidate_widget (widget);
		}
		break;

	case PROP_PER_PAGE:
		old_finish = priv->finish;

		priv->frames_per_page = g_value_get_uint64 (value);

		if (priv->sample) {
			g_object_get (G_OBJECT (priv->sample),
				      "total_frames", &priv->number_of_frames,
				      "channels", &priv->num_channels,
				      NULL);
		} else {
			priv->number_of_frames = 0;
			priv->num_channels = 1;
		}

		/* Limit the number of frames to the total number of frames */
		if (priv->frames_per_page > priv->number_of_frames) {
			priv->frames_per_page = priv->number_of_frames;
		}

		priv->finish = priv->start + priv->frames_per_page;

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;
			int min_x, max_x;

			min_finish = MIN (old_finish, priv->finish);
			max_finish = MAX (old_finish, priv->finish);

			min_x = (int) (min_finish / priv->frames_per_pixel) - 1;
			max_x = (int) (max_finish / priv->frames_per_pixel) + 1;

			area.x = MAX (min_x, 0);
			area.y = 0;
			area.width = MIN (max_x - min_x, widget->allocation.width);
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}
		break;

	case PROP_POSITION:
		old_position = priv->position;
		priv->position = g_value_get_uint64 (value);

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;

			area.x = (int) (old_position / priv->frames_per_pixel);
			area.y = 0;
			area.width = 1;
			area.height = widget->allocation.height;

 			gdk_window_invalidate_rect (widget->window, &area, FALSE);

			/* Only need to change the x,
			   because the rest is the same */
			area.x = widget->allocation.x + (int) (priv->position / priv->frames_per_pixel);
 			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		g_signal_emit (object, signals[CURSOR_CHANGED], 0, priv->position);
		break;

	case PROP_PAGE_START:
		old_start = priv->start;
		priv->start = g_value_get_uint64 (value);
		priv->finish = priv->start + priv->frames_per_page;

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;

			min_start = MIN (old_start, priv->start);
			max_start = MAX (old_start, priv->start);

			area.x = (int) (min_start / priv->frames_per_pixel);
			area.y = 0;
			area.width = (int) (((max_start - min_start) + priv->frames_per_page) / priv->frames_per_pixel) + 1;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		g_signal_emit (object, signals[PAGE_START_CHANGED], 0, (guint64) priv->start);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinOverviewBar *overview_bar;
	MarlinOverviewBarPrivate *priv;

	overview_bar = MARLIN_OVERVIEW_BAR (object);
	priv = overview_bar->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		g_value_set_object (value, priv->sample);
		break;

	case PROP_POSITION:
		g_value_set_uint64 (value, priv->position);
		break;

	case PROP_PAGE_START:
		g_value_set_uint64 (value, priv->start);
		break;

	case PROP_PER_PAGE:
		g_value_set_uint64 (value, priv->frames_per_page);
		break;

	case PROP_PER_PIXEL:
		g_value_set_uint (value, priv->frames_per_pixel);
		break;

	default:
		break;
	}
}

static void
size_allocate (GtkWidget *widget,
	       GtkAllocation *allocation)
{
	MarlinOverviewBar *overview_bar;
	MarlinOverviewBarPrivate *priv;

	overview_bar = MARLIN_OVERVIEW_BAR (widget);
	priv = overview_bar->priv;

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);

		rect.x = 0;
		rect.y = 0;
		rect.width = allocation->width;
		rect.height = allocation->height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}

	priv->frames_per_pixel = (int)(priv->number_of_frames / allocation->width);
	if (priv->frames_per_pixel == 0) {
		priv->frames_per_pixel = 1;
	}
	priv->dc->fpp = priv->frames_per_pixel;

	/* Chain up */
	GTK_WIDGET_CLASS (marlin_overview_bar_parent_class)->size_allocate (widget, allocation);

	if (priv->peaks) {
 		free_peaks (overview_bar);
	}

	create_peaks (overview_bar);
	redraw_backing_store (overview_bar);
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *requisition)
{
	MarlinOverviewBar *overview_bar;
	MarlinOverviewBarPrivate *priv;

	overview_bar = MARLIN_OVERVIEW_BAR (widget);
	priv = overview_bar->priv;

	requisition->width = (int) (priv->number_of_frames / priv->frames_per_pixel);

	requisition->height = 30;
}

static void
realize (GtkWidget *widget)
{
	MarlinOverviewBar *bar = (MarlinOverviewBar *) widget;

	/* Add an event to receive the clicks */
	gtk_widget_add_events (widget,
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK |
			       GDK_POINTER_MOTION_MASK |
			       GDK_KEY_PRESS_MASK);

	GTK_WIDGET_CLASS (marlin_overview_bar_parent_class)->realize (widget);

	bar->priv->dc->drawable = widget->window;
}

static void
unrealize (GtkWidget *widget)
{
	GTK_WIDGET_CLASS (marlin_overview_bar_parent_class)->unrealize (widget);
}

static void
draw_markers (MarlinOverviewBar *bar,
	      GdkRectangle *area)
{
	GList *m;
	static GdkGC *red_dotted = NULL;
	GtkWidget *widget = (GtkWidget *) bar;

	for (m = bar->priv->markers; m; m = m->next) {
		struct _VMarker *vm = m->data;
		int position = vm->real_position / bar->priv->frames_per_pixel;

		if (red_dotted == NULL) {
			GdkColor red = {0, 65535, 0, 0};
			GdkColormap *cmap = gdk_colormap_get_system ();

			red_dotted = gdk_gc_new (widget->window);

			gdk_gc_copy (red_dotted, widget->style->black_gc);

			gdk_colormap_alloc_color (cmap, &red, FALSE, TRUE);
			gdk_gc_set_foreground (red_dotted, &red);
			gdk_gc_set_line_attributes (red_dotted, 1,
						    GDK_LINE_ON_OFF_DASH,
						    GDK_CAP_BUTT,
						    GDK_JOIN_MITER);
		}

		gdk_gc_set_clip_rectangle (red_dotted, area);
		gdk_draw_line (widget->window, red_dotted,
			       position, 0,
			       position, widget->allocation.height - 1);
		gdk_gc_set_clip_rectangle (red_dotted, NULL);
	}
}

/*
 * There are 3 parts to the overview_bar.
 * A: The part that represents the bit before the visible page
 * B: The part that represents the visible page
 * C: The part that represents the bit after the visible page.
 *
 * |==========[         ]==============================|
 * ^^^^^A^^^^^ ^^^^B^^^^ ^^^^^^^^^^^^^^^^^^C^^^^^^^^^^^
 */
static void
_marlin_overview_bar_paint (MarlinOverviewBar *overview_bar,
			    GdkRectangle *area,
			    GtkStateType state_type)
{
	GtkWidget *widget;
	MarlinOverviewBarPrivate *priv;
	GdkRectangle rect, inter;
	int width, height;
	int x, y;
	int chan_height;

	priv = overview_bar->priv;
	widget = GTK_WIDGET (overview_bar);

	x = 0;
	y = 0;
	width = widget->allocation.width;
	height = widget->allocation.height;

	/* Special case when there's no frames */
	if (priv->number_of_frames == 0) {
		gdk_draw_rectangle (widget->window,
				    widget->style->dark_gc[state_type],
				    TRUE, area->x, area->y,
				    area->width, area->height);
		return;
	}

	/* Generate the rectangle for A */
	rect.x = x;
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->start / priv->frames_per_pixel);

	/* Check if the area intersects with A */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->dark_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Generate the rectangle for B */
	rect.x = x + (int) (priv->start / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->frames_per_page / priv->frames_per_pixel);

	/* Check if area intersects with B and draw only the background */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->base_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Generate the rectangle for C */
	rect.x = x + (int) (priv->finish / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = width - rect.x;

	/* Check if the area intersects with C */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->dark_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Draw the selections */
	if (priv->selection != NULL) {
		MarlinCoverage coverage;
		guint64 sel_start, sel_finish;
		GdkGC *gc;

		marlin_sample_selection_get (priv->selection, &coverage,
					     &sel_start, &sel_finish);

		rect.x = (int) (sel_start) / priv->frames_per_pixel;
		switch (coverage) {
		case MARLIN_COVERAGE_BOTH:
			rect.y = 0;
			rect.height = height;
			break;

		case MARLIN_COVERAGE_LEFT:
			rect.y = 0;
			rect.height = (int) (height / 2);
			break;

		case MARLIN_COVERAGE_RIGHT:
			rect.y = (int) (height / 2);
			rect.height = (int) (height / 2);
			break;

		default:
			break;
		}

		gc = widget->style->base_gc[GTK_STATE_SELECTED];

		/* Do the divisions before the subtraction due to
		   rounding errors that occur the other way round */
		rect.width = (int) (sel_finish / priv->frames_per_pixel) - (sel_start / priv->frames_per_pixel);

		if (gdk_rectangle_intersect (area, &rect, &inter)) {
			gdk_draw_rectangle (widget->window, gc,
					    TRUE, inter.x, inter.y,
					    inter.width, inter.height);
		}
	}

	/* Draw the sample */
	chan_height = widget->allocation.height / priv->num_channels;
#if 0
	for (i = 0; i < priv->num_channels; i++) {
		GdkRectangle chan_rect;

		chan_rect.x = 0;
		chan_rect.y = chan_height * i;
		chan_rect.width = widget->allocation.width;
		chan_rect.height = chan_height;

		if (gdk_rectangle_intersect (area, &chan_rect, &inter)) {
			marlin_sample_draw (priv->dc, widget,
					    &inter, state_type,
					    i, 0);
		}
	}

	gdk_draw_drawable (widget->window,
			   priv->non_gr_exp_gc,
			   priv->backing_store,
			   0, 0, 0, 0,
			   widget->allocation.width,
			   widget->allocation.height);
#endif
	inter.x = 0;
	inter.y = 0;
	inter.width = widget->allocation.width;
	inter.height = widget->allocation.height;

	marlin_sample_draw_buffer (priv->dc, widget, priv->peaks, &inter);

	draw_markers (overview_bar, area);

	/* Draw cursor */
	gdk_draw_line (widget->window,
		       widget->style->text_gc[state_type],
		       x + (int) (priv->position / priv->frames_per_pixel),
		       y,
		       x + (int) (priv->position / priv->frames_per_pixel),
		       height);

	/* Generate the rectangle for B to draw the decals */
	rect.x = x + (int) (priv->start / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->frames_per_page / priv->frames_per_pixel);

	/* Check if area intersects with B */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		GdkPoint points[4];

		gdk_draw_line (widget->window,
			       widget->style->text_gc[state_type],
			       rect.x, rect.y,
			       (rect.x + rect.width) - 1, rect.y);

		gdk_draw_line (widget->window,
			       widget->style->text_gc[state_type],
			       rect.x, (rect.y + height) - 1,
			       (rect.x + rect.width) - 1,
			       (rect.y + height) - 1);

		/* Draw page end */
		points[0].x = rect.x;
		points[0].y = rect.y + 1;
		points[1].x = rect.x;
		points[1].y = (rect.y + height) - 2;
 		points[2].x = (rect.x + rect.width) - 1;
		points[2].y = rect.y + 1;

		points[3].x = (rect.x + rect.width) - 1;
		points[3].y = (rect.y + height) - 2;

		gdk_draw_points (widget->window,
				 widget->style->text_gc[state_type],
				 points, 4);
	}
}

static int
expose_event (GtkWidget *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		MarlinOverviewBar *overview_bar = MARLIN_OVERVIEW_BAR (widget);

		_marlin_overview_bar_paint (overview_bar, &event->area,
					 GTK_WIDGET_STATE (widget));
	}

	return FALSE;
}

static int
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	MarlinOverviewBar *overview_bar;
	int x;
	guint64 position;

	/* Take back focus */
	gtk_widget_grab_focus (widget);

	overview_bar = MARLIN_OVERVIEW_BAR (widget);

	x = event->x;
	position = (guint64) (x * overview_bar->priv->frames_per_pixel);

	switch (event->button) {
	case 1:
		g_object_set (G_OBJECT (widget),
			      "cursor_position", position,
			      NULL);

		/* Move the page as well */
		if (event->type == GDK_2BUTTON_PRESS) {
			guint64 start, half, end;

			half = (guint) (overview_bar->priv->frames_per_page / 2);

			/* Check it is always >= 0 */
			start = MAX ((int) (position - half), 0);

			/* Check it's never > end */
			end = overview_bar->priv->number_of_frames -
				overview_bar->priv->frames_per_page;
			start = MIN (start, end);

			g_object_set (G_OBJECT (widget),
				      "page_start", start,
				      NULL);
		}

		if (overview_bar->priv->in_page) {
			GdkCursor *hand = marlin_cursor_get (widget, HAND_CLOSED);

			overview_bar->priv->in_drag = TRUE;
			overview_bar->priv->drag_offset = position - overview_bar->priv->start;
			gdk_pointer_grab (widget->window, FALSE,
					  GDK_BUTTON_RELEASE_MASK |
					  GDK_BUTTON1_MOTION_MASK, NULL,
					  hand, event->time);
			gdk_cursor_unref (hand);
		}
		break;

	case 3:
		/* Start/Stop playback at position */
		g_signal_emit (widget, signals[PLAY_REQUEST], 0, position);
		break;

	default:
		break;
	}

	return FALSE;
}

static int
button_release_event (GtkWidget *widget,
		      GdkEventButton *event)
{
	MarlinOverviewBar *overview_bar = MARLIN_OVERVIEW_BAR (widget);

	switch (event->button) {
	case 1:
		if (overview_bar->priv->in_drag) {
			gdk_pointer_ungrab (event->time);
			overview_bar->priv->in_drag = FALSE;
			overview_bar->priv->drag_offset = 0;
		}
		break;

	default:
		break;
	}

	return FALSE;
}

static gboolean
can_page_move (MarlinOverviewBar *bar)
{
	if (bar->priv->start == 0 &&
	    bar->priv->finish == bar->priv->number_of_frames) {
		return FALSE;
	}

	return TRUE;
}

static int
motion_notify_event (GtkWidget *widget,
		     GdkEventMotion *event)
{
	MarlinOverviewBar *overview_bar = MARLIN_OVERVIEW_BAR (widget);
	int x = event->x;
	gint64 position;

	position = x * (gint64) overview_bar->priv->frames_per_pixel;

	if (position >= overview_bar->priv->start &&
	    position <= overview_bar->priv->start + overview_bar->priv->frames_per_page) {
		if (overview_bar->priv->in_page == FALSE &&
		    can_page_move (overview_bar)) {
			GdkCursor *hand = marlin_cursor_get (widget, HAND_OPEN);

			gdk_window_set_cursor (widget->window, hand);
			gdk_cursor_unref (hand);
			overview_bar->priv->in_page = TRUE;
		}
	} else {
		overview_bar->priv->in_page = FALSE;
		gdk_window_set_cursor (widget->window, NULL);
	}

	if (overview_bar->priv->in_drag) {
		guint64 new_pos;
		gint64 pos = position - overview_bar->priv->drag_offset;

		if (pos < 0) {
			new_pos = (guint64) 0;
		} else if (pos > (gint64)overview_bar->priv->number_of_frames - (gint64) overview_bar->priv->frames_per_page) {
			new_pos = overview_bar->priv->number_of_frames - overview_bar->priv->frames_per_page;
		} else {
			new_pos = (guint64) pos;
		}

		g_object_set (G_OBJECT (widget),
			      "page-start", new_pos,
			      NULL);
	}

	return FALSE;
}

static gboolean
focus_out_event (GtkWidget *widget,
		 GdkEventFocus *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		invalidate_widget (widget);
	}

	return TRUE;
}

static gboolean
focus_in_event (GtkWidget *widget,
		GdkEventFocus *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		invalidate_widget (widget);
	}

	return TRUE;
}

static void
real_move_page (MarlinOverviewBar *bar,
		GtkMovementStep step,
		int count,
		gboolean extend_selection)
{
	MarlinOverviewBarPrivate *priv;
	guint64 position;

	priv = bar->priv;

	position = priv->start;

	switch (step) {
	case GTK_MOVEMENT_LOGICAL_POSITIONS:
		position += (int) (count * priv->frames_per_pixel);
		break;

	case GTK_MOVEMENT_PAGES:
		position += (int) (count * priv->frames_per_page);
		break;

	case GTK_MOVEMENT_BUFFER_ENDS:
		if (count == -1) {
			position = 0;
		} else {
			position = priv->number_of_frames - priv->frames_per_page;
		}

		break;

	default:
		break;
	}

	/* FIXME: This will break when position is legitamitly > gint64 */
	if ((gint64) position < 0) {
		position = 0;
	} else if (position > priv->number_of_frames - priv->frames_per_page) {
		position = priv->number_of_frames - priv->frames_per_page;
	}

	priv->start = position;
	priv->finish = position + priv->frames_per_page;

	/* Redraw */
	invalidate_widget (GTK_WIDGET (bar));

	g_signal_emit (bar, signals[PAGE_START_CHANGED], 0, (guint64) priv->start);
}

static void
add_move_binding (GtkBindingSet *binding_set,
		  guint keyval,
		  guint modmask,
		  GtkMovementStep step,
		  int count)
{
	gtk_binding_entry_add_signal (binding_set, keyval, modmask,
				      "move_page", 3,
				      GTK_TYPE_ENUM, step,
				      G_TYPE_INT, count,
				      G_TYPE_BOOLEAN, FALSE);
}

static void
marlin_overview_bar_class_init (MarlinOverviewBarClass *klass)
{
	GObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkBindingSet *binding_set;

	object_class = G_OBJECT_CLASS (klass);
	widget_class = GTK_WIDGET_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->size_allocate = size_allocate;
	widget_class->size_request = size_request;
	widget_class->realize = realize;
	widget_class->unrealize = unrealize;
	widget_class->expose_event = expose_event;
	widget_class->button_press_event = button_press_event;
	widget_class->button_release_event = button_release_event;
	widget_class->motion_notify_event = motion_notify_event;
	widget_class->focus_in_event = focus_in_event;
	widget_class->focus_out_event = focus_out_event;

	klass->move_page = real_move_page;

	g_type_class_add_private (object_class, sizeof (MarlinOverviewBarPrivate));
	g_object_class_install_property (object_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PER_PAGE,
					 g_param_spec_uint64 ("frames_per_page",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_POSITION,
					 g_param_spec_uint64 ("cursor_position",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_PAGE_START,
					 g_param_spec_uint64 ("page_start",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_PER_PIXEL,
					 g_param_spec_uint ("frames_per_pixel",
							    "",
							    "",
							    0,
							    G_MAXUINT,
							    0,
							    G_PARAM_READABLE));

	signals[CURSOR_CHANGED] = g_signal_new ("cursor-changed",
						G_TYPE_FROM_CLASS (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinOverviewBarClass, cursor_changed),
						NULL, NULL,
						marlin_marshal_VOID__UINT64,
						G_TYPE_NONE,
						1, G_TYPE_UINT64);
	signals[PAGE_START_CHANGED] = g_signal_new ("page-start-changed",
						    G_TYPE_FROM_CLASS (klass),
						    G_SIGNAL_RUN_FIRST |
						    G_SIGNAL_NO_RECURSE,
						    G_STRUCT_OFFSET (MarlinOverviewBarClass, page_start_changed),
						    NULL, NULL,
						    marlin_marshal_VOID__UINT64,
						    G_TYPE_NONE,
						    1, G_TYPE_UINT64);
	signals[PLAY_REQUEST] = g_signal_new ("play-request",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinOverviewBarClass, play_request),
					      NULL, NULL,
					      marlin_marshal_VOID__UINT64,
					      G_TYPE_NONE,
					      1, G_TYPE_UINT64);

	signals[MOVE_PAGE] = g_signal_new ("move-page",
					   G_TYPE_FROM_CLASS (klass),
					   G_SIGNAL_RUN_FIRST |
					   G_SIGNAL_ACTION,
					   G_STRUCT_OFFSET (MarlinOverviewBarClass, move_page),
					   NULL, NULL,
					   marlin_marshal_VOID__ENUM_INT_BOOLEAN,
					   G_TYPE_NONE,
					   3,
					   GTK_TYPE_MOVEMENT_STEP,
					   G_TYPE_INT,
					   G_TYPE_BOOLEAN);

	/* Key bindings */
	binding_set = gtk_binding_set_by_class (klass);

	/* Move the cursor */
	/* Left/Right - Move page by 1 pix */
	add_move_binding (binding_set, GDK_Right, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 1);
	add_move_binding (binding_set, GDK_Left, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -1);
	add_move_binding (binding_set, GDK_KP_Right, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, 1);
	add_move_binding (binding_set, GDK_KP_Left, 0,
			  GTK_MOVEMENT_LOGICAL_POSITIONS, -1);

	/* PgUp/PgDn - Move page over 1 whole page */
	add_move_binding (binding_set, GDK_Page_Up, 0,
			  GTK_MOVEMENT_PAGES, 1);
	add_move_binding (binding_set, GDK_Page_Down, 0,
			  GTK_MOVEMENT_PAGES, -1);
	add_move_binding (binding_set, GDK_KP_Page_Up, 0,
			  GTK_MOVEMENT_PAGES, 1);
	add_move_binding (binding_set, GDK_KP_Page_Down, 0,
			  GTK_MOVEMENT_PAGES, -1);

	/* Home/End - Move to start/end of sample */
	add_move_binding (binding_set, GDK_Home, 0,
			  GTK_MOVEMENT_BUFFER_ENDS, -1);
	add_move_binding (binding_set, GDK_End, 0,
			  GTK_MOVEMENT_BUFFER_ENDS, 1);
	add_move_binding (binding_set, GDK_KP_Home, 0,
			  GTK_MOVEMENT_BUFFER_ENDS, -1);
	add_move_binding (binding_set, GDK_KP_End, 0,
			  GTK_MOVEMENT_BUFFER_ENDS, 1);
}

static void
marlin_overview_bar_init (MarlinOverviewBar *overview_bar)
{
	MarlinOverviewBarPrivate *priv;

	GTK_WIDGET_SET_FLAGS (overview_bar, GTK_CAN_FOCUS);

	priv = overview_bar->priv = GET_PRIVATE (overview_bar);
	priv->frames_per_pixel = DEFAULT_FRAMES_PER_PIXEL;
	priv->peaks = NULL;

	priv->dc = marlin_sample_draw_context_new (NULL,
						   DEFAULT_FRAMES_PER_PIXEL,
						   FALSE);
}

GtkWidget *
marlin_overview_bar_new (MarlinSample *sample,
			 guint64 frames_per_page			 )
{
	MarlinOverviewBar *overview_bar;

	overview_bar = g_object_new (MARLIN_OVERVIEW_BAR_TYPE,
				     "sample", sample,
				     "frames_per_page", frames_per_page,
				     NULL);
	return GTK_WIDGET (overview_bar);
}
