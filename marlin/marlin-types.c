/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>

#include <marlin/marlin-types.h>

GType
marlin_display_get_type (void)
{
	static GType type = 0;
	static GEnumValue coverage[] = {
		{ MARLIN_DISPLAY_FRAMES, "0", "Display as frames" },
		{ MARLIN_DISPLAY_TIME_LONG, "1", "Display time in long format" },
		{ MARLIN_DISPLAY_SECONDS, "2", "Display as seconds" },
		{ MARLIN_DISPLAY_TIME_FRAMES, "3", "Frames and time" },
		{ MARLIN_DISPLAY_BEATS, "4", "Beats" },
		{ 0, NULL, NULL },
	};
	
	if (type == 0) {
		type = g_enum_register_static ("MarlinDisplay", coverage);
	}
	
	return type;
}

GType
marlin_scale_get_type (void)
{
	static GType type = 0;
	static GEnumValue level[] = {
		{ MARLIN_SCALE_LINEAR, "0", "Display a linear scale" },
		{ MARLIN_SCALE_LOG, "1", "Display a logarithmic scale" },
		{ 0, NULL, NULL },
	};

	if (type == 0) {
		type = g_enum_register_static ("MarlinScale", level);
	}

	return type;
}

GType
marlin_coverage_get_type (void) {
	static GType type = 0;
	static GEnumValue coverage[] = {
		{ MARLIN_COVERAGE_BOTH, "0", "Both" },
		{ MARLIN_COVERAGE_LEFT, "1", "Left channel only" },
		{ MARLIN_COVERAGE_RIGHT, "2", "Right channel only" },
		{ 0, NULL, NULL },
	};

	if (type == 0) {
		type = g_enum_register_static ("MarlinCoverage", coverage);
	}

	return type;
}

GType
marlin_timesig_get_type (void)
{
	static GType type = 0;
	static GEnumValue timesig[] = {
		{ MARLIN_TIMESIG_TWO_FOUR, "0", "2/4" },
		{ MARLIN_TIMESIG_THREE_FOUR, "1", "3/4" },
		{ MARLIN_TIMESIG_FOUR_FOUR, "2", "4/4" },
		{ MARLIN_TIMESIG_FIVE_FOUR, "3", "5/4" },
		{ MARLIN_TIMESIG_SIX_EIGHT, "4", "6/8" },
		{ MARLIN_TIMESIG_SEVEN_EIGHT, "5", "7/8" },
		{ MARLIN_TIMESIG_NINE_EIGHT, "6", "9/8" },
		{ 0, NULL, NULL },
	};

	if (type == 0) {
		type = g_enum_register_static ("MarlinTimesig", timesig);
	}

	return type;
}
