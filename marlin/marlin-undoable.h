/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_UNDOABLE_H__
#define __MARLIN_UNDOABLE_H__

#include <glib.h>
#include <glib-object.h>

typedef void (*MarlinUndoableFunc) (gpointer closure);

typedef struct _MarlinUndoable {
	/* The functions that will undo/redo the action if they are passed
	 * the data packet that is stored in closure */
	MarlinUndoableFunc undo; 
	MarlinUndoableFunc redo; 
	MarlinUndoableFunc destroy;

	gpointer closure; /* This is enough data for the undo/redo functions
			     to perform their actions */
} MarlinUndoable;

MarlinUndoable *marlin_undoable_new (MarlinUndoableFunc undo,
				     MarlinUndoableFunc redo,
				     MarlinUndoableFunc destroy,
				     gpointer closure);
void marlin_undoable_free (MarlinUndoable *undoable);
void marlin_undoable_undo (MarlinUndoable *undoable);
void marlin_undoable_redo (MarlinUndoable *undoable);

#endif
