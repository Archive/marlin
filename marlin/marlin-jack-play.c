/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <jack/jack.h>
#include <jack/ringbuffer.h>

#include <samplerate.h>

#include <marlin/marlin-block.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-jack-play.h>

enum {
	EOS,
	STATUS_CHANGED,
	POSITION_CHANGED,
	LAST_SIGNAL
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_JACK_PLAY_TYPE, MarlinJackPlayPrivate))

typedef enum {
	MARLIN_JACK_PLAY_MODE_NOTHING,
	MARLIN_JACK_PLAY_MODE_STOPPED,
	MARLIN_JACK_PLAY_MODE_PREROLLING,
	MARLIN_JACK_PLAY_MODE_PLAYING,
} MarlinJackPlayMode;

#define DEFAULT_RB_SIZE 65536
static const size_t frame_size = sizeof (jack_default_audio_sample_t);

struct _port_data {
	MarlinJackPlay *jack;

	MarlinChannel *channel; /* The channel associated with the port */
	MarlinBlock *current_block; /* A pointer to the current block */
	guint64 offset; /* Where we are in the channel */
	guint64 offset_in_block; /* Where we are inside current_block */

	jack_port_t *playback; /* The playback port */

	jack_ringbuffer_t *rb;
	float *rb_buffer, *tmp_buffer;

	SRC_STATE *state;

	gboolean writer_finished;
};

struct _MarlinJackPlayPrivate {
	jack_client_t *client;
	jack_nframes_t bufsize;
	jack_default_audio_sample_t *jack_buf;

	MarlinSample *sample;
	gulong sample_notify_id;

	MarlinJackPlayMode mode;

	guint channels;

	guint sample_rate, jack_rate;
	double src_ratio;

	struct _port_data **port_data;

	guint writer_id;

	gboolean eos;
	guint64 position;

	guint64 start;
	guint64 finish;
	gboolean repeating;
};

G_DEFINE_TYPE (MarlinJackPlay, marlin_jack_play, MARLIN_JACK_TYPE);

static guint32 signals[LAST_SIGNAL] = { 0, };
static long process_more_frames (gpointer userdata, float **data);

static void
finalize (GObject *object)
{
	MarlinJackPlayPrivate *priv;

	priv = GET_PRIVATE (object);

	/* free_ports ((MarlinJackPlay *) object); */
	/* jack_client_close (priv->client); */

	((GObjectClass *) marlin_jack_play_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinJackPlay *jack;
	MarlinJackPlayPrivate *priv;

	jack = (MarlinJackPlay *) object;
	priv = jack->priv;

	if (priv->writer_id > 0) {
		g_source_remove (priv->writer_id);
		priv->writer_id = 0;
	}

	if (priv->sample) {
		g_signal_handler_disconnect (priv->sample,
					     priv->sample_notify_id);
		priv->sample_notify_id = 0;
		g_object_unref (priv->sample);
		priv->sample = NULL;
	}

	((GObjectClass *) marlin_jack_play_parent_class)->dispose (object);
}

static double
calculate_rate_ratio (guint src_rate,
		      guint dest_rate)
{
	double ratio;

	if (src_rate == 0) {
		return 1.0;
	}

	ratio = (1.0 * dest_rate) / src_rate;
	if (src_is_valid_ratio (ratio) == FALSE) {
		ratio = 1.0;
	}

	return ratio;
}

static void
sample_changed (GObject        *object,
		GParamSpec     *pspec,
		MarlinJackPlay *jack)
{
	MarlinJackPlayPrivate *priv = jack->priv;

	if (strcmp (pspec->name, "sample-rate") == 0) {
		g_object_get (object,
			      "sample-rate", &priv->sample_rate,
			      NULL);

		/* sample rate has changed so we need to change the
		   resample ratio */
		priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
							priv->jack_rate);
		return;
	}

	if (strcmp (pspec->name, "channels") == 0) {
		g_object_get (object,
			      "channels", &priv->channels,
			      NULL);
		return;
	}
}

static void
set_sample (MarlinJack   *jack,
	    MarlinSample *sample)
{
	MarlinJackPlayPrivate *priv;

	priv = GET_PRIVATE (jack);

	/* Don't need to do anything if its the same sample */
	if (priv->sample == sample) {
		return;
	}

	if (priv->sample) {
		int i;

		/* Sample is changed so we need to reset the SRC */
		for (i = 0; i < priv->channels; i++) {
			struct _port_data *pd = priv->port_data[i];

			if (pd->state) {
				src_reset (pd->state);
			}
		}

		g_signal_handler_disconnect (priv->sample,
					     priv->sample_notify_id);
		g_object_unref (priv->sample);
	}

	if (sample == NULL) {
		return;
	}

	priv->sample = g_object_ref (sample);
	g_object_get (G_OBJECT (priv->sample),
		      "channels", &priv->channels,
		      "sample_rate", &priv->sample_rate,
		      NULL);

	priv->sample_notify_id = g_signal_connect (priv->sample,
						   "notify",
						   G_CALLBACK (sample_changed),
						   jack);

	priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
						priv->jack_rate);
}

/**
 * How the process functions work.
 * There are two threads: Thread A is the main GUI thread, thread B is the
 * JACK process thread. Thread B is not able to do anything that is not
 * realtime safe. This includes the necessary things to access the frame
 * data, which is mmapped from disk and may require mmap calls and locking.
 *
 * When start_playing is called, an timeout is started that calls
 * process_writer. This is run in thread A and is able to do all the mmapping
 * safely. Because it is run in the main thread, the frame data can be accessed
 * without locking. process_writer copies enough frame data to fill the ring
 * buffer. Once the ring buffer is filled, thread B is allowed to process the
 * data by reading from the ring buffer. As thread B removes data from the ring
 * buffer, thread A fills it back up.
 */

static void
process_playing (MarlinJackPlay    *jack,
		 jack_nframes_t     nframes)
{
	MarlinJackPlayPrivate *priv;
	gboolean eos = FALSE;
	guint frames_needed = 0;
	guint64 tmp_pos;
	int i;

	priv = jack->priv;

	/* Always output in stereo, even when we've got a mono sample */
	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		jack_default_audio_sample_t *o_buf;
		size_t bytes_avail;
		guint frames_avail, blank;

		/* Check if we have enough frames in the ringbuffer to
		   fill the buffer */
		bytes_avail = jack_ringbuffer_read_space (pd->rb);
		frames_avail = bytes_avail / frame_size;
		frames_needed = MIN (frames_avail, nframes);

		if (frames_needed == 0 && pd->writer_finished) {
			eos = TRUE;
		}

		o_buf = jack_port_get_buffer (pd->playback, nframes);
		jack_ringbuffer_read (pd->rb, (char *) o_buf,
				      frames_needed * frame_size);

		/* How many blank frames are needed at the end? */
		blank = nframes - frames_needed;
		if (blank > 0) {
			/* Need to zero the end of the buffer */
			memset (o_buf + frames_needed, 0, blank * frame_size);
		}
	}

	/* Fun aside: because the frames_needed is at Jack's sample rate
	   and priv->position needs them at the sample's sample rate
	   we need to estimate here */
	tmp_pos = priv->position;
	tmp_pos += (frames_needed / priv->src_ratio);
	if (tmp_pos > priv->finish) {
		tmp_pos = (tmp_pos - priv->finish) + priv->start;
	}
	priv->position = tmp_pos;

	priv->eos = eos;
}

static void
process_silence (MarlinJackPlay    *jack,
		 jack_nframes_t     nframes)
{
	MarlinJackPlayPrivate *priv = jack->priv;
	int i;

	/* Always output in stereo even when we've got mono data */
	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		jack_default_audio_sample_t *o_buf;

		o_buf = jack_port_get_buffer (pd->playback, nframes);
		/* Zero this so that there is silence */
		memset (o_buf, 0, nframes * frame_size);
	}
}

static int
process_reader (jack_nframes_t nframes,
		void          *arg)
{
	MarlinJackPlay *jack = (MarlinJackPlay *) arg;
	MarlinJackPlayPrivate *priv = jack->priv;

	switch (priv->mode) {
	case MARLIN_JACK_PLAY_MODE_PLAYING:
		process_playing (jack, nframes);
		break;

	case MARLIN_JACK_PLAY_MODE_PREROLLING:
	case MARLIN_JACK_PLAY_MODE_STOPPED:
		process_silence (jack, nframes);
		break;

	case MARLIN_JACK_PLAY_MODE_NOTHING:
	default:
		break;
	}

	return 0;
}

static long
process_more_frames (gpointer userdata,
		     float  **data)
{
	struct _port_data *pd = userdata;
	MarlinJackPlay *jack = pd->jack;
	MarlinJackPlayPrivate *priv = jack->priv;
	guint frames_needed, copied;

	frames_needed = MIN (priv->finish - pd->offset, DEFAULT_RB_SIZE);

	if (frames_needed == 0 && priv->repeating == FALSE) {
		/* This will tell the reader that we've finished writing */
		pd->writer_finished = TRUE;

		return 0;
	}

	copied = marlin_block_get_buffer (pd->current_block, pd->tmp_buffer,
					  pd->offset, frames_needed,
					  &pd->current_block);

	if (frames_needed < DEFAULT_RB_SIZE && priv->repeating) {
		/* Reset back to the start of the loop */
		pd->offset = priv->start;

		/* Check if we're still in the current block before searching */
		if (!MARLIN_FRAME_IN_BLOCK (pd->current_block, pd->offset)) {
			pd->current_block = marlin_channel_get_block_for_frame (pd->channel, priv->start);
		}
	} else {
		pd->offset += copied;
	}

	/* If pd->current_block == NULL then we've finished */
	if (pd->current_block) {
		pd->offset_in_block = pd->offset - pd->current_block->start;
	}
	*data = pd->tmp_buffer;

	return copied;
}

static gboolean
process_writer (gpointer data)
{
	MarlinJackPlay *jack = data;
	MarlinJackPlayPrivate *priv = jack->priv;
	float *tmp_frames[2];
	long gen_frames[2];
	int i;

	if (priv->mode != MARLIN_JACK_PLAY_MODE_PLAYING &&
	    priv->mode != MARLIN_JACK_PLAY_MODE_PREROLLING) {
		return TRUE;
	}

	if (priv->eos == TRUE) {
		marlin_jack_stop ((MarlinJack *) jack);
 		g_signal_emit (jack, signals[EOS], 0);
		return FALSE;
	}

  	g_signal_emit (jack, signals[POSITION_CHANGED], 0, priv->position);

	/* Always output in stereo even when we've got mono data */
	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		size_t bytes_free;
		guint frames_needed;

		bytes_free = jack_ringbuffer_write_space (pd->rb);

		frames_needed = bytes_free / frame_size;
		if (frames_needed == 0) {
			gen_frames[i] = 0;
			tmp_frames[i] = NULL;
			continue;
		}

		/* Left and right do not always require the
		   same number of frames */
		gen_frames[i] = src_callback_read (pd->state, priv->src_ratio,
						   frames_needed,
						   pd->rb_buffer);
		if (gen_frames[i] == 0) {
			tmp_frames[i] = NULL;
			continue;
		}

		tmp_frames[i] = pd->rb_buffer;
		jack_ringbuffer_write (pd->rb, (char *) pd->rb_buffer,
				       gen_frames[i] * frame_size);
	}

	/* If any frames were generated, calculate the levels */
	if (gen_frames[0] > 0 || gen_frames[1] > 0) {
		marlin_jack_emit_levels ((MarlinJack *) jack,
					 (float **) tmp_frames, gen_frames);
	}

	priv->mode = MARLIN_JACK_PLAY_MODE_PLAYING;

	return TRUE;
}

static int
srate_changed (jack_nframes_t nframes,
	       gpointer       data)
{
	MarlinJackPlay *jack = data;
	MarlinJackPlayPrivate *priv = jack->priv;

	priv->jack_rate = nframes;
	priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
						priv->jack_rate);

	return 0;
}

static int
bufsize_changed (jack_nframes_t nframes,
		 gpointer       data)
{
	MarlinJackPlay *jack = data;
	MarlinJackPlayPrivate *priv = jack->priv;

	priv->bufsize = nframes;

	g_free (priv->jack_buf);
	priv->jack_buf = g_new0 (jack_default_audio_sample_t, nframes * 16);

	return 0;
}

static gboolean
play_start (MarlinJack *jack,
	    GError    **error)
{
	MarlinJackPlayPrivate *priv;
	const char **ports;
	int i;

	priv = GET_PRIVATE (jack);

	if (priv->sample == NULL) {
		if (error && *error != NULL) {
			*error = g_error_new (MARLIN_JACK_ERROR,
					      MARLIN_JACK_ERROR_NO_SAMPLE,
					      "No sample to play");
		} else {
			g_warning ("No sample to play");
		}

		return FALSE;
	}

	jack_set_process_callback (priv->client, process_reader, jack);
	jack_set_buffer_size_callback (priv->client, bufsize_changed, jack);

	jack_set_sample_rate_callback (priv->client, srate_changed, jack);
	priv->jack_rate = jack_get_sample_rate (priv->client);

	if (jack_activate (priv->client)) {
		if (error && *error != NULL) {
			*error = g_error_new (MARLIN_JACK_ERROR,
					      MARLIN_JACK_ERROR_NO_SERVER,
					      "Jack activate returned an error");
		} else {
			g_warning ("Jack activate returned an error");
		}
		return FALSE;
	}

	marlin_sample_read_lock (priv->sample);

	ports = jack_get_ports (priv->client, NULL, NULL,
				JackPortIsPhysical | JackPortIsInput);
	/* Even if we've got mono data, we output in stereo */
	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		int err;

		if (ports[i] == NULL) {
			marlin_sample_read_unlock (priv->sample);
			return FALSE;
		}

		if (jack_connect (priv->client,
				  jack_port_name (pd->playback),
				  ports[i])) {
			marlin_sample_read_unlock (priv->sample);
			if (error && *error != NULL) {
				*error = g_error_new (MARLIN_JACK_ERROR,
						      MARLIN_JACK_ERROR_NO_SERVER,
						      "Jack connect returned an error");
			} else {
				g_warning ("Jack connect returned an error");
			}

			return FALSE;
		}

		if (i < priv->channels) {
			pd->channel = marlin_sample_get_channel (priv->sample,
								 i);
		} else {
			pd->channel = marlin_sample_get_channel (priv->sample, 0);
		}
		pd->current_block = marlin_channel_get_block_for_frame (pd->channel, priv->start);
		pd->offset = priv->start;
		pd->offset_in_block = pd->offset - pd->current_block->start;

		pd->writer_finished = FALSE;

		/* Create the ring buffer */
		pd->rb = jack_ringbuffer_create (frame_size * DEFAULT_RB_SIZE);

		pd->rb_buffer = g_new (float, DEFAULT_RB_SIZE);
		pd->tmp_buffer = g_new (float, DEFAULT_RB_SIZE);

		pd->state = src_callback_new (process_more_frames,
					      SRC_SINC_BEST_QUALITY,
					      1, &err, pd);
	}
	free (ports);

	priv->eos = FALSE;
	priv->position = priv->start;
	priv->mode = MARLIN_JACK_PLAY_MODE_PREROLLING;

	/* Start the idle process to fill the ring buffer with data */
	priv->writer_id = g_timeout_add (50, process_writer, jack);

	return TRUE;
}

void
play_pause (MarlinJack *jack)
{
	MarlinJackPlayPrivate *priv;

	priv = GET_PRIVATE (jack);

	priv->mode = MARLIN_JACK_PLAY_MODE_STOPPED;
}

void
play_continue (MarlinJack *jack)
{
	MarlinJackPlayPrivate *priv;

	priv = GET_PRIVATE (jack);

	priv->mode = MARLIN_JACK_PLAY_MODE_PLAYING;
}

static void
play_stop (MarlinJack *jack)
{
	MarlinJackPlayPrivate *priv;
	const char **ports;
	int i;

	priv = GET_PRIVATE (jack);

	priv->mode = MARLIN_JACK_PLAY_MODE_NOTHING;

	jack_deactivate (priv->client);

	marlin_sample_read_unlock (priv->sample);

	g_source_remove (priv->writer_id);
	priv->writer_id = 0;

	ports = jack_get_ports (priv->client, NULL, NULL,
				JackPortIsPhysical | JackPortIsInput);
	for (i = 0; i < priv->channels; i++) {
		struct _port_data *pd = priv->port_data[i];

		jack_disconnect (priv->client,
				 jack_port_name (pd->playback), ports[i]);
		pd->channel = NULL;
		pd->current_block = NULL;

		if (pd->rb) {
			jack_ringbuffer_free (pd->rb);
			g_free (pd->rb_buffer);
			g_free (pd->tmp_buffer);

			pd->rb = NULL;
			pd->rb_buffer = NULL;
			pd->tmp_buffer = NULL;
		}

		src_delete (pd->state);
		pd->state = NULL;

/* 		src_reset (pd->state); */
	}
	free (ports);
}

static gboolean
is_busy (MarlinJack *jack)
{
	MarlinJackPlay *player = (MarlinJackPlay *) jack;
	MarlinJackPlayPrivate *priv = player->priv;

	return (priv->mode != MARLIN_JACK_PLAY_MODE_NOTHING);
}

static void
marlin_jack_play_class_init (MarlinJackPlayClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	MarlinJackClass *j_class = (MarlinJackClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;

	j_class->set_sample = set_sample;
	j_class->start = play_start;
	j_class->stop = play_stop;
	j_class->pause = play_pause;
	j_class->cont = play_continue;
	j_class->is_busy = is_busy;

	g_type_class_add_private (klass, sizeof (MarlinJackPlayPrivate));

	signals[EOS] = g_signal_new ("eos",
				     G_TYPE_FROM_CLASS (klass),
				     G_SIGNAL_RUN_FIRST |
				     G_SIGNAL_NO_RECURSE,
				     G_STRUCT_OFFSET (MarlinJackPlayClass, eos),
				     NULL, NULL,
				     g_cclosure_marshal_VOID__VOID,
				     G_TYPE_NONE, 0);
	signals[STATUS_CHANGED] = g_signal_new ("status-changed",
						G_TYPE_FROM_CLASS (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinJackPlayClass,
status_changed),
						NULL, NULL,
						g_cclosure_marshal_VOID__INT,
						G_TYPE_NONE,
						1, G_TYPE_INT);
	signals[POSITION_CHANGED] = g_signal_new ("position-changed",
						  G_TYPE_FROM_CLASS (klass),
						  G_SIGNAL_RUN_FIRST |
						  G_SIGNAL_NO_RECURSE,
						  G_STRUCT_OFFSET (MarlinJackPlayClass, position_changed),
						  NULL, NULL,
						  marlin_marshal_VOID__UINT64,
						  G_TYPE_NONE,
						  1, G_TYPE_UINT64);
}

static void
setup_ports (MarlinJackPlay *jack)
{
	MarlinJackPlayPrivate *priv;
	int i;

	priv = jack->priv;

	/* We always want to set up two ports, even for mono samples */
	priv->port_data = g_new0 (struct _port_data *, 2);
	for (i = 0; i < 2; i++) {
		struct _port_data *pd;
		char *name;

		pd = g_new0 (struct _port_data, 1);

		pd->jack = jack;
		/* Create the ports */
		name = g_strdup_printf ("playback%d", i + 1);
		pd->playback = jack_port_register (priv->client, name,
						   JACK_DEFAULT_AUDIO_TYPE,
						   JackPortIsOutput, 0);
                g_free (name);

		priv->port_data[i] = pd;
	}
}

static void
marlin_jack_play_init (MarlinJackPlay *jack)
{
	MarlinJackPlayPrivate *priv;

	jack->priv = GET_PRIVATE (jack);
	priv = jack->priv;

	priv->mode = MARLIN_JACK_PLAY_MODE_NOTHING;

	priv->eos = FALSE;
	priv->position = 0;
	priv->start = 0;
	priv->finish = 0;
	priv->repeating = FALSE;
}

/**
 * marlin_jack_play_new:
 * @client:
 *
 * Creates a new #MarlinJackPlay structure using @client.
 */
MarlinJackPlay *
marlin_jack_play_new (jack_client_t *client)
{
	MarlinJackPlay *jack;
	MarlinJackPlayPrivate *priv;

	jack = g_object_new (MARLIN_JACK_PLAY_TYPE, NULL);
	priv = jack->priv;

	priv->client = client;
	setup_ports (jack);

	priv->bufsize = jack_get_buffer_size (priv->client);
	priv->jack_buf = g_new0 (jack_default_audio_sample_t,
				 priv->bufsize * 16);
	return jack;
}

/**
 * marlin_jack_play_set_repeating:
 * @jack: A #MarlinJackPlay
 * @repeating: Whether the play should loop
 *
 * Sets whether or not @jack should loop when it is being played
 */
void
marlin_jack_play_set_repeating (MarlinJackPlay *jack,
				gboolean        repeating)
{
	MarlinJackPlayPrivate *priv;

	priv = jack->priv;

	priv->repeating = repeating;
}

/**
 * marlin_jack_play_set_range:
 * @jack: A #MarlinJackPlay
 * @start: Start of the playback range
 * @finish: End of the playback range
 *
 * Sets @jack to playback between @start and @finish
 */
void
marlin_jack_play_set_range (MarlinJackPlay *jack,
			    guint64         start,
			    guint64         finish)
{
	MarlinJackPlayPrivate *priv;

	priv = jack->priv;
	priv->start = start;
	priv->finish = finish;
}

