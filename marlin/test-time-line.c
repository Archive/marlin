/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gtk/gtk.h>
#include <marlin-time-line.h>

static void
change (GtkButton *button,
	MarlinTimeLine *timeline)
{
	/* Change the number of page start */
	g_object_set (G_OBJECT (timeline),
		      "page_start", (guint64) 24000,
		      NULL);
}

int
main (int argc,
      char **argv)
{
	GtkWidget *window, *timeline, *vbox, *button;

	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (gtk_main_quit), NULL);
	
/* 	gtk_container_set_border_width (GTK_CONTAINER (window), 1); */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	
	timeline = marlin_time_line_new (50000, 10000);
	gtk_box_pack_start (GTK_BOX (vbox), timeline, FALSE, FALSE, 0);

	button = gtk_button_new_with_label ("Click to change");
	gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (change), timeline);

	gtk_widget_show_all (window);

	gtk_main ();
}
