/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <jack/jack.h>
#include <jack/ringbuffer.h>

#include <samplerate.h>

#include <marlin/marlin-block.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-jack-record.h>

enum {
	FINISHED,
	LAST_SIGNAL
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_JACK_RECORD_TYPE, MarlinJackRecordPrivate))

typedef enum {
	MARLIN_JACK_RECORD_MODE_NOTHING,
	MARLIN_JACK_RECORD_MODE_STOPPED,
	MARLIN_JACK_RECORD_MODE_PREROLLING,
	MARLIN_JACK_RECORD_MODE_RECORDING,
	MARLIN_JACK_RECORD_MODE_DRAINING,
} MarlinJackRecordMode;

#define DEFAULT_RB_SIZE 262144
static const size_t frame_size = sizeof (jack_default_audio_sample_t);

struct _port_data {
	MarlinJackRecord *jack;

	MarlinChannel *channel; /* The channel associated with the port */
	MarlinBlock *block_list, *bl_end;

	jack_port_t *record; /* The record port */

	jack_ringbuffer_t *rb;
	float *rb_buffer, *tmp_buffer, *data;
	guint frames_in_data;

	SRC_STATE *state;

	gboolean writer_finished;
	gboolean eos; /* Is this port finished? */
};

struct _MarlinJackRecordPrivate {
	jack_client_t *client;
	jack_nframes_t bufsize;
	jack_default_audio_sample_t *jack_buf;

	MarlinSample *sample;
	gulong sample_notify_id;

	MarlinJackRecordMode mode;

	guint channels;

	guint sample_rate, jack_rate;
	double src_ratio;

	struct _port_data **port_data;

	guint reader_id;
};

G_DEFINE_TYPE (MarlinJackRecord, marlin_jack_record, MARLIN_JACK_TYPE);
static guint32 signals[LAST_SIGNAL] = {0, };

static long process_more_frames (gpointer userdata, float **data);

static void
free_ports (MarlinJackRecord *jack)
{
	MarlinJackRecordPrivate *priv = jack->priv;
	int i;

	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];

		jack_port_unregister (priv->client, pd->record);

		if (pd->rb) {
			jack_ringbuffer_free (pd->rb);
		}

		g_free (pd->rb_buffer);
		g_free (pd->tmp_buffer);
		g_free (pd->data);

		if (pd->state) {
			src_delete (pd->state);
		}
		g_free (pd);
	}

	g_free (priv->port_data);
}

static void
setup_ports (MarlinJackRecord *jack)
{
	MarlinJackRecordPrivate *priv = jack->priv;
	int i;

	/* We always want to set up two ports, even for mono samples */
	priv->port_data = g_new0 (struct _port_data *, 2);
	for (i = 0 ; i < 2; i++) {
		struct _port_data *pd;
		char *name;

		pd = g_new0 (struct _port_data, 1);
		pd->jack = jack;

		/* Create the ports */
		name = g_strdup_printf ("record%d", i + 1);
		pd->record = jack_port_register (priv->client, name,
						 JACK_DEFAULT_AUDIO_TYPE,
						 JackPortIsInput, 0);
		g_free (name);

		priv->port_data[i] = pd;
	}
}

static void
finalize (GObject *object)
{
	MarlinJackRecordPrivate *priv;

	priv = GET_PRIVATE (object);

	if (priv->port_data != NULL) {
		free_ports ((MarlinJackRecord *) object);
	}

	((GObjectClass *) marlin_jack_record_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinJackRecordPrivate *priv;

	priv = GET_PRIVATE (object);

	if (priv->reader_id > 0) {
		g_source_remove (priv->reader_id);
		priv->reader_id = 0;
	}

	if (priv->sample) {
		g_signal_handler_disconnect (priv->sample,
					     priv->sample_notify_id);
		priv->sample_notify_id = 0;
		g_object_unref (priv->sample);
		priv->sample = NULL;
	}

	((GObjectClass *) marlin_jack_record_parent_class)->dispose (object);
}

static double
calculate_rate_ratio (guint src_rate,
		      guint dest_rate)
{
	double ratio;

	if (src_rate == 0) {
		return 1.0;
	}

	ratio = (1.0 * dest_rate) / src_rate;
	if (src_is_valid_ratio (ratio) == FALSE) {
		ratio = 1.0;
	}

	return ratio;
}

static void
sample_changed (GObject          *object,
		GParamSpec       *pspec,
		MarlinJackRecord *jack)
{
	MarlinJackRecordPrivate *priv = jack->priv;

	if (strcmp (pspec->name, "sample_rate") == 0) {
		g_object_get (object,
			      "sample_rate", &priv->sample_rate,
			      NULL);
		priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
							priv->jack_rate);
		return;
	}

	if (strcmp (pspec->name, "channels") == 0) {
		free_ports (jack);
		g_object_get (object,
			      "channels", &priv->channels,
			      NULL);
		setup_ports (jack);
		return;
	}
}

static void
set_sample (MarlinJack   *jack,
	    MarlinSample *sample)
{
	MarlinJackRecordPrivate *priv;

	priv = GET_PRIVATE (jack);

	if (priv->sample) {
		free_ports ((MarlinJackRecord *) jack);
		g_signal_handler_disconnect (priv->sample,
					     priv->sample_notify_id);
		g_object_unref (priv->sample);
	}

	priv->sample = g_object_ref (sample);
	g_object_get (G_OBJECT (priv->sample),
		      "channels", &priv->channels,
		      "sample_rate", &priv->sample_rate,
		      NULL);

	priv->sample_notify_id = g_signal_connect (priv->sample, "notify",
						   G_CALLBACK (sample_changed),
						   jack);

	priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
						priv->jack_rate);

	setup_ports ((MarlinJackRecord *) jack);
}

/* recording process simply copies the data from jack into the ring buffer. */
static void
process_recording (MarlinJackRecord *jack,
		   jack_nframes_t   nframes)
{
	MarlinJackRecordPrivate *priv;
	int i;
	int overruns = 0;

	priv = jack->priv;

	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		jack_default_audio_sample_t *i_buf;
		size_t bytes_avail;

		i_buf = jack_port_get_buffer (pd->record, nframes);
		bytes_avail = nframes * frame_size;

		/* Write the data into the ringbuffer */
		if (jack_ringbuffer_write (pd->rb, (void *) i_buf,
					   bytes_avail) < bytes_avail) {
			/* Not sure what to do with this... signal? */
			g_print ("xrun...\n");
			overruns++;
		}
	}
}

static int
process_writer (jack_nframes_t nframes,
		void          *arg)
{
	MarlinJackRecord *jack = (MarlinJackRecord *) arg;
	MarlinJackRecordPrivate *priv = jack->priv;

	switch (priv->mode) {
	case MARLIN_JACK_RECORD_MODE_RECORDING:
	case MARLIN_JACK_RECORD_MODE_PREROLLING:
		process_recording (jack, nframes);
		break;

	default:
		break;
	}

	return 0;
}

static void
store_block (MarlinJackRecord  *jack,
	     struct _port_data *pd,
	     float             *data,
	     guint              number_of_frames)
{
	MarlinJackRecordPrivate *priv;
	MarlinBlock *block;
	GError *error = NULL;
	gboolean ret;

	priv = jack->priv;

	/* Write the data into a block */
	block = marlin_channel_create_block (pd->channel);
	ret = marlin_block_set_data (block, data, number_of_frames, &error);
	if (ret == FALSE) {
		marlin_block_free (block);
		return;
	}

	/* Add it to the block_list */
	if (pd->block_list == NULL) {
		pd->block_list = block;
		pd->bl_end = block;
	} else {
		marlin_block_append (pd->bl_end, block);
		pd->bl_end = block;
	}
}

static long
process_more_frames (gpointer userdata,
		     float  **data)
{
	struct _port_data *pd = userdata;
	MarlinJackRecord *jack = pd->jack;
	size_t bytes_avail;

	/* We want to put as much into the SRC as possible */
	bytes_avail = jack_ringbuffer_read_space (pd->rb);
	if (bytes_avail == 0) {
		return 0;
	}

	jack_ringbuffer_read (pd->rb, (char *) pd->tmp_buffer, bytes_avail);

	*data = pd->tmp_buffer;
	return bytes_avail / sizeof (float);
}

/* Reader function: Reads data from the ringbuffer and puts it in the sample */
static gboolean
process_reader (gpointer data)
{
	MarlinJackRecord *jack = data;
	MarlinJackRecordPrivate *priv = jack->priv;
	int i;

	if (priv->mode != MARLIN_JACK_RECORD_MODE_RECORDING &&
	    priv->mode != MARLIN_JACK_RECORD_MODE_PREROLLING &&
	    priv->mode != MARLIN_JACK_RECORD_MODE_DRAINING) {
		return TRUE;
	}

	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		guint frames;
		long frames_needed;
		size_t bytes_avail;

		/* Do nothing if the port has finished */
		if (pd->eos) {
			continue;
		}

		bytes_avail = jack_ringbuffer_read_space (pd->rb);
		if (priv->mode == MARLIN_JACK_RECORD_MODE_DRAINING &&
		    bytes_avail == 0) {
			/* Store any extra data that might be there */
			store_block (MARLIN_JACK_RECORD (jack), pd,
				     pd->data, pd->frames_in_data);

			/* Move the recorded data into the sample */
			pd->channel->first = pd->block_list;
			pd->channel->last = pd->bl_end;
			pd->channel->frames = marlin_block_recalculate_ranges (pd->channel->first);

			marlin_sample_frame_count_changed (priv->sample);

			pd->channel = NULL;
			pd->block_list = NULL;
			pd->bl_end = NULL;

			pd->eos = TRUE;
			continue;
		}

		/* Roll until we've got half the ringbuffer filled
		   unless we're draining the ring buffer on shutdown */
		if (priv->mode != MARLIN_JACK_RECORD_MODE_DRAINING &&
		    bytes_avail < DEFAULT_RB_SIZE / 2) {
			return TRUE;
		}

		frames_needed = MIN (MARLIN_BLOCK_SIZE - pd->frames_in_data,
				     bytes_avail / sizeof (float));
		if (frames_needed == 0) {
			continue;
		}

		frames = src_callback_read (pd->state, priv->src_ratio,
					    frames_needed, pd->rb_buffer);
		if (frames == 0) {
			continue;
		}

		/* The amount of frames available will be fill a block */
		if (pd->frames_in_data + frames >= MARLIN_BLOCK_SIZE) {
			guint32 needed, remain;
			float *fd;

			needed = MARLIN_BLOCK_SIZE - pd->frames_in_data;
			remain = frames - needed;

			/* Read from the ringbuffer into the next free space
			   in the frame buffer */
			fd = pd->data + pd->frames_in_data;
			memcpy (fd, pd->rb_buffer, needed * sizeof (float));

			/* Now we have a whole block, we can store it */
			store_block (jack, pd, pd->data,
				     MARLIN_BLOCK_SIZE);

			memset (pd->data, 0,
				MARLIN_BLOCK_SIZE * sizeof (float));

			if (remain > 0) {
				memcpy (pd->data, pd->rb_buffer + needed,
					remain * sizeof (float));
			}
			pd->frames_in_data = remain;
		} else {
			float *fd;

			fd = pd->data + pd->frames_in_data;
			memcpy (fd, pd->rb_buffer, frames * sizeof (float));
			pd->frames_in_data += frames;
		}
	}

	if (priv->mode == MARLIN_JACK_RECORD_MODE_DRAINING) {
		/* Check if both of the channels are finished */
		for (i = 0; i < 2; i++) {
			struct _port_data *pd = priv->port_data[i];
			if (pd->eos == FALSE) {
				return TRUE;
			}
		}

		/* If we've got here then we're finished */
		priv->reader_id = 0;
		g_signal_emit (jack, signals[FINISHED], 0);

		priv->mode = MARLIN_JACK_RECORD_MODE_STOPPED;
		return FALSE;
	} else {
		priv->mode = MARLIN_JACK_RECORD_MODE_RECORDING;
	}

	return TRUE;
}

static int
srate_changed (jack_nframes_t nframes,
	       gpointer       data)
{
	MarlinJackRecord *jack = data;
	MarlinJackRecordPrivate *priv = jack->priv;

	priv->jack_rate = nframes;
	priv->src_ratio = calculate_rate_ratio (priv->sample_rate,
						priv->jack_rate);

	return 0;
}

static int
bufsize_changed (jack_nframes_t nframes,
		 gpointer       data)
{
	MarlinJackRecord *jack = data;
	MarlinJackRecordPrivate *priv = jack->priv;

	priv->bufsize = nframes;
	return 0;
}

static gboolean
record_start (MarlinJack *jack,
	      GError    **error)
{
	MarlinJackRecordPrivate *priv;
	const char **ports;
	int i;

	priv = GET_PRIVATE (jack);

	if (priv->sample == NULL) {
		if (error && *error != NULL) {
			*error = g_error_new (MARLIN_JACK_ERROR,
					      MARLIN_JACK_ERROR_NO_SAMPLE,
					      "No sample to record");
		} else {
			g_warning ("No sample to record");
		}

		return FALSE;
	}

	if (jack_activate (priv->client)) {
		if (error && *error != NULL) {
			*error = g_error_new (MARLIN_JACK_ERROR,
					      MARLIN_JACK_ERROR_NO_SERVER,
					      "Jack activate returned an error");
		} else {
			g_warning ("Jack activate returned an error");
		}
		return FALSE;
	}

	ports = jack_get_ports (priv->client, NULL, NULL,
				JackPortIsPhysical | JackPortIsOutput);
	if (ports == NULL) {
		return FALSE;
	}

	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
		int err;

		if (ports[i] == NULL) {
			return FALSE;
		}

		if (jack_connect (priv->client, ports[i],
				  jack_port_name (pd->record))) {
			if (error && *error != NULL) {
				*error = g_error_new (MARLIN_JACK_ERROR,
						      MARLIN_JACK_ERROR_NO_SERVER,
						      "Jack connect returned an error");
			} else {
				g_warning ("Jack connect returned an error");
			}
			return FALSE;
		}

		pd->channel = marlin_sample_get_channel (priv->sample, i);
		pd->rb = jack_ringbuffer_create (frame_size * DEFAULT_RB_SIZE);

		pd->rb_buffer = g_new (float, DEFAULT_RB_SIZE);
		pd->tmp_buffer = g_new (float, DEFAULT_RB_SIZE);
		pd->data = g_new (float, MARLIN_BLOCK_SIZE);

		pd->state = src_callback_new (process_more_frames,
					      SRC_SINC_BEST_QUALITY,
					      1, &err, pd);
	}

	free (ports);

	priv->mode = MARLIN_JACK_RECORD_MODE_PREROLLING;

	/* Start an timeout to read from the ringbuffer and write data
	   to a sample, don't really care about how fast this happens
	   the important thing is not to starve the writer process:
	   really the balance is between this timeout and the size of the
	   ringbuffer */
	priv->reader_id = g_timeout_add (250, process_reader, jack);

	return TRUE;
}

static void
record_pause (MarlinJack *jack)
{
}

static void
record_continue (MarlinJack *jack)
{
}

static void
record_stop (MarlinJack *jack)
{
	MarlinJackRecordPrivate *priv;
	const char **ports;
	int i;

	priv = GET_PRIVATE (jack);

	priv->mode = MARLIN_JACK_RECORD_MODE_DRAINING;

	jack_deactivate (priv->client);

	/* Once the client is deactivated the ring buffer stops getting filled
	   up, but we want the reader to keep spinning until the ring buffer is
	   completely empty */

#if 0
	g_source_remove (priv->reader_id);
	priv->reader_id = 0;

/*
	ports = jack_get_ports (priv->client, NULL, NULL,
				JackPortIsPhysical | JackPortIsOutput);
*/
	for (i = 0; i < 2; i++) {
		struct _port_data *pd = priv->port_data[i];
/*
		jack_disconnect (priv->client,
				 jack_port_name (pd->record), ports[i]);
*/
	}

	/* free (ports); */
#endif
}

static gboolean
is_busy (MarlinJack *jack)
{
	MarlinJackRecord *recorder = (MarlinJackRecord *) jack;
	MarlinJackRecordPrivate *priv = recorder->priv;

	if (priv->mode == MARLIN_JACK_RECORD_MODE_DRAINING ||
	    priv->mode == MARLIN_JACK_RECORD_MODE_RECORDING ||
	    priv->mode == MARLIN_JACK_RECORD_MODE_PREROLLING) {
		return TRUE;
	}

	return FALSE;
}

static void
marlin_jack_record_class_init (MarlinJackRecordClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	MarlinJackClass *j_class = (MarlinJackClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;

	j_class->set_sample = set_sample;
	j_class->start = record_start;
	j_class->stop = record_stop;
	j_class->pause = record_pause;
	j_class->cont = record_continue;
	j_class->is_busy = is_busy;

	g_type_class_add_private (klass, sizeof (MarlinJackRecordPrivate));

	signals[FINISHED] = g_signal_new ("record-finished",
					  G_TYPE_FROM_CLASS (klass),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (MarlinJackRecordClass, record_finished),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__VOID,
					  G_TYPE_NONE, 0);
}

static void
marlin_jack_record_init (MarlinJackRecord *jack)
{
	MarlinJackRecordPrivate *priv;

	jack->priv = GET_PRIVATE (jack);
	priv = jack->priv;

}

/**
 * marlin_jack_record_new:
 * @client:
 *
 * Creates a new #MarlinJackRecord object.
 *
 * Return value: A #MarlinJackRecord object
 */
MarlinJackRecord *
marlin_jack_record_new (jack_client_t *client)
{
	MarlinJackRecord *jack;
	MarlinJackRecordPrivate *priv;

	jack = g_object_new (MARLIN_JACK_RECORD_TYPE, NULL);
	priv = jack->priv;

	priv->client = client;

	jack_set_process_callback (priv->client, process_writer, jack);
	jack_set_buffer_size_callback (priv->client, bufsize_changed, jack);
	priv->bufsize = jack_get_buffer_size (priv->client);
	priv->jack_buf = g_new0 (jack_default_audio_sample_t, priv->bufsize * 16);

	jack_set_sample_rate_callback (priv->client, srate_changed, jack);
	priv->jack_rate = jack_get_sample_rate (priv->client);

	return jack;
}
