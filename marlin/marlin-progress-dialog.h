/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PROGRESS_WINDOW_H__
#define __MARLIN_PROGRESS_WINDOW_H__

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-operation.h>

#define MARLIN_PROGRESS_DIALOG_TYPE (marlin_progress_dialog_get_type ())
#define MARLIN_PROGRESS_DIALOG(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_PROGRESS_DIALOG_TYPE, MarlinProgressDialog))
#define MARLIN_PROGRESS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_PROGRESS_DIALOG_TYPE, MarlinProgressDialogClass))
#define IS_MARLIN_PROGRESS_DIALOG(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_PROGRESS_DIALOG_TYPE))
#define IS_MARLIN_PROGRESS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_PROGRESS_DIALOG_TYPE))

typedef struct _MarlinProgressDialog MarlinProgressDialog;
typedef struct _MarlinProgressDialogClass MarlinProgressDialogClass;
typedef struct _MarlinProgressDialogPrivate MarlinProgressDialogPrivate;

struct _MarlinProgressDialog {
	GtkDialog parent;

	MarlinProgressDialogPrivate *priv;
};

struct _MarlinProgressDialogClass {
	GtkDialogClass parent_class;
};

GType marlin_progress_dialog_get_type (void);
MarlinProgressDialog *marlin_progress_dialog_new (MarlinOperation *operation,
						  GtkWindow *parent_window,
						  const char *primary,
						  const char *secondary);
#endif
