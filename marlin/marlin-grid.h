/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_GRID_H__
#define __MARLIN_GRID_H__

#include <gtk/gtk.h>

#include <marlin/marlin-list.h>
#include <marlin/marlin-grid-point.h>
#include <marlin/marlin-types.h>

G_BEGIN_DECLS

#define MARLIN_GRID_TYPE (marlin_grid_get_type ())
#define MARLIN_GRID(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_GRID_TYPE, MarlinGrid))
#define MARLIN_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_GRID_TYPE, MarlinGridClass))
#define MARLIN_GRID_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), MARLIN_GRID_TYPE, MarlinGridClass))

typedef struct _MarlinGrid MarlinGrid;
typedef struct _MarlinGridClass MarlinGridClass;
typedef struct _MarlinGridPrivate MarlinGridPrivate;

typedef struct _MarlinGridCurve {
	MarlinRange range;
	GdkColor colour;

	char *name;

	/* <private> */
	gboolean selected;
	MarlinList *points;
} MarlinGridCurve;

struct _MarlinGrid {
	GtkDrawingArea parent;

	MarlinGridPrivate *priv;
};

struct _MarlinGridClass {
	GtkDrawingAreaClass parent_class;

	void (* draw_x_axis) (MarlinGrid   *grid,
			      cairo_t      *cr,
			      GdkRectangle *rect);
	void (* draw_y_axis) (MarlinGrid   *grid,
			      cairo_t      *cr,
			      GdkRectangle *rect);
};

GType marlin_grid_get_type (void);

void marlin_grid_add_curve (MarlinGrid *grid,
			    MarlinGridCurve *curve);
MarlinGridCurve *marlin_grid_curve_new (MarlinRange *range);
void marlin_grid_curve_free (MarlinGridCurve *curve);
void marlin_grid_curve_add_point (MarlinGridCurve *curve,
				  MarlinGridPoint *point,
				  guint32          movability);

G_END_DECLS

#endif
