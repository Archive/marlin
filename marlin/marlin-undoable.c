/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin/marlin-undoable.h>

/**
 * marlin_undoable_new:
 * @undo: A #MarlinUndoableFunc
 * @redo: A #MarlinUndoableFunc
 * @destroy: A #MarlinUndoableFunc
 * @closure: Userdata that will be passed to the functions
 *
 * Creates a #MarlinUndoable struct that will be undone by calling @undo,
 * redone via @redo and @destroy will be called when it is freed. @closure
 * will be passed into the functions.
 *
 * Return value: A newly allocated #MarlinUndoable
 */
MarlinUndoable *
marlin_undoable_new (MarlinUndoableFunc undo,
		     MarlinUndoableFunc redo,
		     MarlinUndoableFunc destroy,
		     gpointer           closure)
{
	MarlinUndoable *u;

	u = g_slice_new0 (MarlinUndoable);
	u->undo = undo;
	u->redo = redo;
	u->destroy = destroy;
	u->closure = closure;

	return u;
}

void
marlin_undoable_free (MarlinUndoable *undoable)
{
	if (undoable->destroy) {
		undoable->destroy (undoable->closure);
	}

	g_slice_free (MarlinUndoable, undoable);
}

void
marlin_undoable_undo (MarlinUndoable *undoable)
{
	if (undoable->undo) {
		undoable->undo (undoable->closure);
	}
}

void
marlin_undoable_redo (MarlinUndoable *undoable)
{
	if (undoable->redo) {
		undoable->redo (undoable->closure);
	}
}
