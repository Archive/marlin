/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2006 Iain Holmes
 *
 *  Based on GStreamer adder plugin:
 *  Copyright (C) 1999,2000 Erik Walthinsen,
 *                     2001 Thomas
 *                2005,2006 Wim Taymans
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>

#include <gst/gst.h>
#include <gst/base/gstcollectpads.h>
#include <gst/audio/multichannel.h>

#include "marlin-channel-joiner.h"

#define MARLIN_CHANNEL_JOINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_CHANNEL_JOINER_TYPE, MarlinChannelJoiner))

typedef struct _MarlinChannelJoiner MarlinChannelJoiner;
typedef struct _MarlinChannelJoinerClass MarlinChannelJoinerClass;

typedef void (*MarlinChannelJoinerFunction) (gpointer out,
					     gpointer in,
					     guint size);
struct _MarlinChannelJoiner {
	GstElement element;

	GstPad *srcpad;
	GstCaps *sinkcaps;
	GstCollectPads *collect;
	int padcount;

	GList *channels;

	gboolean is_int;

	/* number of bytes per sample, actually width/8 * channels */
	int bps;
	int width;
	int rate;

	/* function to add samples */
	MarlinChannelJoinerFunction func;

	/* counters to keep track of timestamps */
	gint64 timestamp;
	gint64 offset;

	/* Sink event handling */
	GstPadEventFunction collect_event;
	GstSegment segment;
	gboolean segment_pending;
	guint64 segment_position;
};

struct _MarlinChannelJoinerClass {
	GstElementClass parent_class;
};

static const GstElementDetails joiner_details = 
GST_ELEMENT_DETAILS ("Marlin Channel Joiner",
		     "Filter/Converter/Audio",
		     "Folds many mono channels into one interleaved audio stream",
		     "Iain Holmes <iain@gnome.org>");

static GstStaticPadTemplate sink_template =
GST_STATIC_PAD_TEMPLATE ("sink%d",
			 GST_PAD_SINK,
			 GST_PAD_REQUEST,
			 GST_STATIC_CAPS ("audio/x-raw-int, "
					  "rate = (int) [ 1, MAX ], "
					  "channels = (int) 1, "
					  "endianness = (int) BYTE_ORDER, "
					  "width = (int) 32, "
					  "depth = (int) 32, "
					  "signed = (boolean) TRUE; "
					  "audio/x-raw-float, "
					  "rate = (int) [ 1, MAX ], "
					  "channels = (int) 1, "
					  "endianness = (int) BYTE_ORDER, "
					  "width = (int) 32"));

static GstStaticPadTemplate src_template = 
GST_STATIC_PAD_TEMPLATE ("src",
			 GST_PAD_SRC,
			 GST_PAD_ALWAYS,
			 GST_STATIC_CAPS ("audio/x-raw-int, "
					  "rate = (int) [ 1, MAX ], "
					  "channels = (int) [ 1, MAX ], "
					  "endianness = (int) BYTE_ORDER, "
					  "width = (int) 32, "
					  "depth = (int) 32, "
					  "signed = (boolean) TRUE; "
					  "audio/x-raw-float, "
					  "rate = (int) [ 1, MAX ], "
					  "channels = (int) [ 1, MAX ], "
					  "endianness = (int) BYTE_ORDER, "
					  "width = (int) 32"));
static GstElementClass *parent_class = NULL;

static void
base_init (MarlinChannelJoinerClass *klass)
{
	GstElementClass *e_class = GST_ELEMENT_CLASS (klass);

	gst_element_class_add_pad_template 
		(e_class, gst_static_pad_template_get (&src_template));
	gst_element_class_add_pad_template
		(e_class, gst_static_pad_template_get (&sink_template));
	gst_element_class_set_details (e_class, &joiner_details);
}

static void
finalize (GObject *object)
{
	MarlinChannelJoiner *joiner;
	
	joiner = MARLIN_CHANNEL_JOINER (object);
	
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static gboolean
setcaps (GstPad  *pad,
	 GstCaps *caps)
{
	MarlinChannelJoiner *joiner;
	GstStructure *s, *s_src;
	GstCaps *srccaps;
	const char *media_type;

	joiner = MARLIN_CHANNEL_JOINER (GST_PAD_PARENT (pad));

	if (joiner->sinkcaps != NULL) {
		gst_caps_unref (joiner->sinkcaps);
	}

	joiner->sinkcaps = gst_caps_copy (caps);

	srccaps = gst_caps_copy (caps);
	s_src = gst_caps_get_structure (srccaps, 0);
	gst_structure_set (s_src, 
			   "channels", G_TYPE_INT, joiner->padcount,
			   NULL);
	gst_caps_replace (&GST_PAD_CAPS (joiner->srcpad), srccaps);
	gst_caps_unref (srccaps);

	s = gst_caps_get_structure (caps, 0);
	media_type = gst_structure_get_name (s);
	if (strcmp (media_type, "audio/x-raw-int") == 0) {
		joiner->is_int = TRUE;
		GST_DEBUG_OBJECT (joiner, "unsupported format set as caps");
		return FALSE;
	} else {
		joiner->is_int = FALSE;
		gst_structure_get_int (s, "width", &joiner->width);
		gst_structure_get_int (s, "rate", &joiner->rate);
	} 
	
	joiner->bps = (joiner->width / 8);
	
	return TRUE;
	
}

static gboolean
forward_event_func (GstPad   *pad,
		    GValue   *ret,
		    GstEvent *event)
{
	gst_event_ref (event);

	if (!gst_pad_push_event (pad, event)) {
		g_value_set_boolean (ret, FALSE);
	}
	gst_object_unref (pad);
	return TRUE;
}

static gboolean
forward_event (MarlinChannelJoiner *joiner,
	       GstEvent            *event)
{
	gboolean ret;
	GstIterator *it;
	GValue vret  = { 0 };

	ret = TRUE;
	
	g_value_init (&vret, G_TYPE_BOOLEAN);
	g_value_set_boolean (&vret, TRUE);
	it = gst_element_iterate_sink_pads (GST_ELEMENT_CAST (joiner));
	gst_iterator_fold (it, (GstIteratorFoldFunction) forward_event_func,
			   &vret, event);
	gst_iterator_free (it);
	gst_event_unref (event);

	ret = g_value_get_boolean (&vret);

	return ret;
}

static gboolean
src_event (GstPad *pad,
	   GstEvent *event)
{
	MarlinChannelJoiner *joiner;
	gboolean result;

	joiner = MARLIN_CHANNEL_JOINER (gst_pad_get_parent (pad));

	switch (GST_EVENT_TYPE (event)) {
	case GST_EVENT_QOS:
		/* QoS might be tricky */
		result = FALSE;
		break;

	case GST_EVENT_SEEK: {
		GstSeekFlags flags;
		GstSeekType curtype;
		gint64 cur;

		/* parse the flushing flag */
		gst_event_parse_seek (event, NULL, NULL, 
				      &flags, &curtype, &cur, 
				      NULL, NULL);

		/* If we are not flushing, just forward */
		if (!flags & GST_SEEK_FLAG_FLUSH) {
			goto done;
		}

		/* make sure we accept nothing anymore and return 
		 * WRONG_STATE */
		gst_collect_pads_set_flushing (joiner->collect, TRUE);

		/* flushing seek, start flush downstream, the flush will be
		 * done when all pads received a FLUSH_STOP. */
		gst_pad_push_event (joiner->srcpad, 
				    gst_event_new_flush_start ());

		/* now wait for the collected to be finished and mark a new
		 * segment */
		GST_OBJECT_LOCK (joiner->collect);
		if (curtype == GST_SEEK_TYPE_SET) {
			joiner->segment_position = cur;
		} else {
			joiner->segment_position = 0;
		}
		joiner->segment_pending = TRUE;
		GST_OBJECT_UNLOCK (joiner->collect);

		done:
		result = forward_event (joiner, event);
		break;
	}

	case GST_EVENT_NAVIGATION:
		/* navigation is rather pointless. */
		result = FALSE;
		break;

	default:
		/* just forward the rest for now */
		result = forward_event (joiner, event);
		break;
	}
	gst_object_unref (joiner);

	return result;
}

static gboolean
sink_event (GstPad   *pad,
	    GstEvent *event)
{
	MarlinChannelJoiner *joiner;
	gboolean ret;

	joiner = MARLIN_CHANNEL_JOINER (gst_pad_get_parent (pad));

	switch (GST_EVENT_TYPE (event)) {
	case GST_EVENT_FLUSH_STOP:
		/* mark a pending new segment. This event is synchronized
		 * with the streaming thread so we can safely update the
		 * variable without races. It's somewhat weird because we
		 * assume the collectpads forwarded the FLUSH_STOP past us
		 * and downstream (using our source pad, the bastard!).
		 *
		 * copied from gstadder.c
		 * I dunno what it means either.
		 */
		joiner->segment_pending = TRUE;
		break;

	default:
		break;
	}

	/* now GstCollectPads can take care of the rest, e.g. EOS */
	ret = joiner->collect_event (pad, event);

 	gst_object_unref (joiner);
	return ret;
}

static GstPad *
request_new_pad (GstElement     *element,
		 GstPadTemplate *templ,
		 const char     *unused)
{
	MarlinChannelJoiner *joiner;
	char *name;
	GstPad *newpad;
	int padcount;

	if (templ->direction != GST_PAD_SINK) {
		goto not_sink;
	}

	joiner = MARLIN_CHANNEL_JOINER (element);

	/* increment pad counter */
	padcount = g_atomic_int_exchange_and_add (&joiner->padcount, 1);

	name = g_strdup_printf ("sink%d", padcount);
	newpad = gst_pad_new_from_template (templ, name);
	g_free (name);

/* 	gst_pad_set_getcaps_function (newpad, gst_pad_proxy_getcaps); */
	gst_pad_set_setcaps_function (newpad, setcaps);
	gst_collect_pads_add_pad (joiner->collect, newpad, 
				  sizeof (GstCollectData));

	if (joiner->sinkcaps) {
		GstCaps *srccaps;
		GstStructure *s_src;

		/* Update the channels on the src */
		srccaps = gst_caps_copy (joiner->sinkcaps);
		s_src = gst_caps_get_structure (srccaps, 0);
		gst_structure_set (s_src, 
				   "channels", G_TYPE_INT, joiner->padcount,
				   NULL);
		gst_caps_replace (&GST_PAD_CAPS (joiner->srcpad), srccaps);
		gst_caps_unref (srccaps);
	}

	/* FIXME: hacked way to override/extend the event function of
	 * GstCollectPads; because it sets its own event function giving the
	 * element no access to events */
	joiner->collect_event = (GstPadEventFunction) GST_PAD_EVENTFUNC (newpad);
	gst_pad_set_event_function (newpad, sink_event);

	/* takes ownership of the pad */
	if (!gst_element_add_pad (element, newpad)) {
		goto could_not_add;
	}

	return newpad;

 not_sink:
	{
		g_warning ("marlin-channel-joiner: Request new pad this is not a SINK pad");
		return NULL;
	}

 could_not_add:
	{
		g_warning ("marlin-channel-joiner: Could not add pad");
		gst_collect_pads_remove_pad (joiner->collect, newpad);
		gst_object_unref (newpad);
		return NULL;
	}
}

static void
release_pad (GstElement *element,
	     GstPad     *pad)
{
	MarlinChannelJoiner *joiner;

	joiner = MARLIN_CHANNEL_JOINER (element);

	gst_collect_pads_remove_pad (joiner->collect, pad);
	gst_element_remove_pad (element, pad);
}

static void
mix_f32_poly (gpointer in,
	      gpointer out,
	      guint    size,
	      guint    count)
{
	float **f_in, *f_out;
	int frames, i, c;

	f_in = (float **) in;
	f_out = (float *) out;

	frames = size / sizeof (float);

	for (i = 0; i < frames; ++i) {
		for (c = 0; c < count; ++c) {
			f_out[(i * count) + c] = f_in[c][i];
		}
	}
}

static void
mix_f32_stereo (gpointer in,
		gpointer out,
		guint    size)
{
	float **f_in, *f_out;
	int frames, i;

	f_in = (float **) in;
	f_out = (float *) out;

	frames = size / sizeof (float);

	for (i = 0; i < frames; i++) {
		f_out[i * 2] = f_in[0][i];
		f_out[(i * 2) + 1] = f_in[1][i];
	}
}

#if 0
static void
mix_f64_poly (gpointer in,
	      gpointer out,
	      guint    size,
	      guint    count)
{
}

static void
mix_f64_stereo (gpointer in,
		gpointer out,
		guint    size)
{
}
#endif
static GstBuffer *
join_channels (MarlinChannelJoiner *joiner,
	       gpointer            *data_in,
	       guint                length)
{
	GstBuffer *outbuf;
	gpointer outbytes;

	outbuf = gst_buffer_new_and_alloc (length * joiner->padcount);
	outbytes = GST_BUFFER_DATA (outbuf);
/* 	gst_buffer_set_caps (outbuf, GST_PAD_CAPS (joiner->srcpad)); */

	switch (joiner->padcount) {
	case 1:
		memcpy (outbytes, data_in[0], length);
		break;

	case 2:
 		mix_f32_stereo (data_in, outbytes, length); 
#if 0
		memcpy (outbytes, data_in[0], length);
		memcpy (outbytes + length, data_in[1], length);
#endif
		break;

	default:
		mix_f32_poly (data_in, outbytes, length, joiner->padcount);
		break;
	}

	return outbuf;
}

static GstFlowReturn
joiner_collected (GstCollectPads *pads,
		  gpointer        userdata)
{
	MarlinChannelJoiner *joiner;
	GstFlowReturn ret;
	GstBuffer *outbuf;
	gpointer *data_in;
	guint size;
	GSList *collected;
	guint len = 0;
	int i;

	joiner = MARLIN_CHANNEL_JOINER (userdata);

	/* fast allocate on the stack, no need to free */
	data_in = g_newa (gpointer, joiner->padcount);

	size = gst_collect_pads_available (pads);

	for (collected = pads->data, i = 0; collected; 
	     collected = collected->next, i++) {
		GstCollectData *data;
		guint8 *bytes;
		
		data = (GstCollectData *) collected->data;

		/* get pointer to copy size bytes */
		len = gst_collect_pads_read (pads, data, &bytes, size);

		/* length 0 means EOS or an empty buffer so we still need to
		 * flush in case of an empty buffer. */
		if (len == 0) {
			goto next;
		}

		data_in[i] = bytes;
	next:
		gst_collect_pads_flush (pads, data, len);
	}

	if (len == 0) {
		gst_pad_push_event (joiner->srcpad, 
				    gst_event_new_eos ());
		return GST_FLOW_UNEXPECTED;
	}

	/* This assumes that len is the same for every pad
	   Maybe it is...FIXME */
	outbuf = join_channels (joiner, data_in, len);
	gst_buffer_set_caps (outbuf, GST_PAD_CAPS (joiner->srcpad));

	GST_BUFFER_TIMESTAMP (outbuf) = joiner->timestamp;
	GST_BUFFER_OFFSET (outbuf) = joiner->offset;

	/* for the next timestamp, use the sample counter, which will
	 * never accumulate rounding errors */
	joiner->offset += size / joiner->bps;
	joiner->timestamp = gst_util_uint64_scale_int (joiner->offset,
						       GST_SECOND,
						       joiner->rate);

	GST_BUFFER_DURATION (outbuf) = joiner->timestamp - 
		GST_BUFFER_TIMESTAMP (outbuf);

	ret = gst_pad_push (joiner->srcpad, outbuf);

	return ret;
}

static gboolean
joiner_activate (GstPad *pad)
{
	return TRUE;
}

static GstStateChangeReturn
change_state (GstElement    *element,
	      GstStateChange transition)
{
	MarlinChannelJoiner *joiner;
	GstStateChangeReturn ret;

	joiner = MARLIN_CHANNEL_JOINER (element);

	switch (transition) {
	case GST_STATE_CHANGE_NULL_TO_READY:
		break;

	case GST_STATE_CHANGE_READY_TO_PAUSED:
		joiner->timestamp = 0;
		joiner->offset = 0;
		joiner->segment_pending = TRUE;

		gst_segment_init (&joiner->segment, GST_FORMAT_UNDEFINED);
		gst_collect_pads_start (joiner->collect);
		break;

	case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
		break;

	case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
		break;

	case GST_STATE_CHANGE_PAUSED_TO_READY:
		/* need to unblock the collectpads before calling the
		 * parent change_state so that streaming can finish */
		gst_collect_pads_stop (joiner->collect);
		break;

	case GST_STATE_CHANGE_READY_TO_NULL:
		break;

	default:
		break;
	}

	ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, 
							      transition);

	return ret;
}

static void
class_init (MarlinChannelJoiner *klass)
{
	GObjectClass *o_class;
	GstElementClass *e_class;

	o_class = G_OBJECT_CLASS (klass);
	e_class = GST_ELEMENT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;

	e_class->request_new_pad = request_new_pad;
	e_class->release_pad = release_pad;
	e_class->change_state = change_state;
}

static void
init (MarlinChannelJoiner *joiner)
{
	GstPad *pad;

	pad = gst_pad_new_from_static_template (&src_template, "src");
	joiner->srcpad = pad;

	gst_pad_set_getcaps_function (pad, gst_pad_proxy_getcaps);
	gst_pad_set_setcaps_function (pad, setcaps);
	gst_pad_set_event_function (pad, src_event);
	gst_pad_set_activate_function (pad, joiner_activate);
	gst_element_add_pad (GST_ELEMENT (joiner), pad);

	joiner->padcount = 0;

	joiner->collect = gst_collect_pads_new ();
	gst_collect_pads_set_function (joiner->collect, 
				       joiner_collected, joiner);
}

GType 
marlin_channel_joiner_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (MarlinChannelJoinerClass),
			(GBaseInitFunc) base_init, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinChannelJoiner), 0, 
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (GST_TYPE_ELEMENT,
					       "MarlinChannelJoiner",
					       &info, 0);
	}

	return type;
}
