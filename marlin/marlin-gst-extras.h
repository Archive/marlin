/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_GST_EXTRAS_H__
#define __MARLIN_GST_EXTRAS_H__

#include <gst/gst.h>

#define MARLIN_TAG_BPM "bpm"

GstElementFactory *get_decoder_factory_for_mime (const char *mimetype);
GstElement *get_decoder_for_mime (const char *mimetype,
				  const char *name);
GstElement *get_encoder_for_mime (const char *mimetype);
GstElementFactory *get_encoder_factory_for_mime (const char *mimetype);
char *get_string_from_caps (GstCaps *caps,
			    const char *key);
int get_int_from_caps (GstCaps *caps,
		       const char *key);
gboolean marlin_gst_can_encode (const char *mimetype);
GstElement *marlin_make_gst_source (const char *name);

GList *get_mime_types (void);

void marlin_gst_register ();
#endif
