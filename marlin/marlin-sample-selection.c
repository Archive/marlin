/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin/marlin-sample-selection.h>

#undef LOCK_DEBUG
/* #define LOCK_DEBUG 1 */
#include <marlin-read-write-lock.h>
enum {
	CHANGED,
	LAST_SIGNAL
};

struct _MarlinSampleSelectionPrivate {
	MarlinReadWriteLock *lock;
	MarlinCoverage coverage;
	guint64 start;
	guint64 finish;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_SAMPLE_SELECTION_TYPE, MarlinSampleSelectionPrivate))
G_DEFINE_TYPE (MarlinSampleSelection, marlin_sample_selection, G_TYPE_OBJECT);

static guint signals[LAST_SIGNAL];

static void
finalize (GObject *object)
{
	MarlinSampleSelection *selection;

	selection = MARLIN_SAMPLE_SELECTION (object);

	marlin_read_write_lock_destroy (selection->priv->lock);

	G_OBJECT_CLASS (marlin_sample_selection_parent_class)->finalize (object);
}

static void
marlin_sample_selection_class_init (MarlinSampleSelectionClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = finalize;

	g_type_class_add_private (object_class, sizeof (MarlinSampleSelectionPrivate));

	signals[CHANGED] = g_signal_new ("changed",
					 G_TYPE_FROM_CLASS (klass),
					 G_SIGNAL_RUN_FIRST |
					 G_SIGNAL_NO_RECURSE,
					 G_STRUCT_OFFSET (MarlinSampleSelectionClass, changed),
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);
}

static void
marlin_sample_selection_init (MarlinSampleSelection *selection)
{
	MarlinSampleSelectionPrivate *priv;

	priv = GET_PRIVATE (selection);
	selection->priv = priv;

	priv->lock = marlin_read_write_lock_new ();
	priv->coverage = MARLIN_COVERAGE_NONE;
}

MarlinSampleSelection *
marlin_sample_selection_new (void)
{
	return (MarlinSampleSelection *) g_object_new (MARLIN_SAMPLE_SELECTION_TYPE, NULL);
}

struct _selection_closure {
	MarlinSampleSelection *selection;
	guint64 start;
	guint64 finish;
	MarlinCoverage coverage;
};

static void
selection_undo_redo (gpointer data)
{
	struct _selection_closure *c = data;
	guint64 old_s, old_f;
	MarlinCoverage old_c;

	/* FIXME: Do this as lockless? */
	marlin_sample_selection_get (c->selection, &old_c, &old_s, &old_f);
	marlin_sample_selection_set (c->selection, c->coverage, c->start, c->finish, NULL);

	c->coverage = old_c;
	c->start = old_s;
	c->finish = old_f;
}

static void
selection_destroy (gpointer data)
{
	g_free (data);
}

void
marlin_sample_selection_set (MarlinSampleSelection *selection,
			     MarlinCoverage coverage,
			     guint64 start,
			     guint64 finish,
			     MarlinUndoContext *ctxt)
{
	g_return_if_fail (IS_MARLIN_SAMPLE_SELECTION (selection));

	if (ctxt) {
		MarlinUndoable *u;
		struct _selection_closure *c;

		c = g_new (struct _selection_closure, 1);
		c->selection = selection;

		marlin_sample_selection_get (selection, &c->coverage,
					     &c->start, &c->finish);

		u = marlin_undoable_new (selection_undo_redo,
					 selection_undo_redo,
					 selection_destroy,
					 c);

		marlin_undo_context_add (ctxt, u);
	}

	WRITE_LOCK (selection->priv->lock);

	selection->priv->coverage = coverage;
	selection->priv->start = start;
	selection->priv->finish = finish;

	WRITE_UNLOCK (selection->priv->lock);

	g_signal_emit (selection, signals[CHANGED], 0);
}

void
marlin_sample_selection_clear (MarlinSampleSelection *selection,
			       MarlinUndoContext *ctxt)
{
	marlin_sample_selection_set (selection, MARLIN_COVERAGE_NONE,
				     0, 0, ctxt);
}

void
marlin_sample_selection_get (MarlinSampleSelection *selection,
			     MarlinCoverage *coverage,
			     guint64 *start,
			     guint64 *finish)
{
	g_return_if_fail (IS_MARLIN_SAMPLE_SELECTION (selection));

	READ_LOCK (selection->priv->lock);

	if (coverage) {
		*coverage = selection->priv->coverage;
	}
	if (start) {
		*start = selection->priv->start;
	}
	if (finish) {
		*finish = selection->priv->finish;
	}

	READ_UNLOCK (selection->priv->lock);
}

gboolean
marlin_sample_selection_contains_frame (MarlinSampleSelection *selection,
					MarlinCoverage coverage,
					guint64 frame)
{
	gboolean ret = FALSE;

	g_return_val_if_fail (IS_MARLIN_SAMPLE_SELECTION (selection), FALSE);

	READ_LOCK (selection->priv->lock);

	if (selection->priv->coverage == MARLIN_COVERAGE_BOTH ||
	    selection->priv->coverage == coverage) {
		if (selection->priv->start <= frame &&
		    selection->priv->finish >= frame) {
			ret = TRUE;
		}
	}

	READ_UNLOCK (selection->priv->lock);

	return ret;
}
