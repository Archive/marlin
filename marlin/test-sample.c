/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <glib.h>
#include <gst/gst.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-channel.h>
#include <marlin/marlin-load-pipeline.h>
#include <marlin/marlin-gst-extras.h>

MarlinSample *sample;
MarlinLoadPipeline *load_pipeline;
GMainLoop *mainloop;

char *new_filename, *mimetype;

struct _idle_data {
	MarlinSample *sample;
	char *filename;
};

static void
load_started (MarlinOperation *operation,
	      gpointer data)
{
}

static void
load_finished (MarlinOperation *operation,
	       gpointer data)
{
	int number, rate;
	int channels;
	char *filename;

	g_object_get (sample,
		      "total_frames", &number,
		      "sample_rate", &rate,
		      "channels", &channels,
		      "filename", &filename,
		      NULL);
	
	g_print ("Information for %s\n", filename);
	g_print ("--------------------\n");
	g_print ("Number of samples: %d\n", number);
	g_print ("Sample rate: %d\n", rate);
	g_print ("Length: %d seconds\n", number / rate);
	g_print ("Channels: %d\n\n", channels);
	
	g_free (filename);
	
	gst_element_set_state (GST_ELEMENT (load_pipeline), GST_STATE_NULL);
	g_object_unref (G_OBJECT (load_pipeline));

	g_object_unref (G_OBJECT (sample));
	g_main_loop_quit (mainloop);
}

static void
load_error (MarlinOperation *operation,
	    GError *error,
	    const char *debug,
	    gpointer ud)
{
	g_print ("Message: %s\n", error->message);
	g_print ("Debug: %s\n", debug);
}

static void
load_progress (MarlinOperation *operation,
	       int percentage,
	       gpointer ud)
{
}

gboolean
idle_func (gpointer ud)
{
	struct _idle_data *data = ud;
	MarlinOperation *operation;
	
	operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (load_started), NULL);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (load_finished), NULL);
	g_signal_connect (G_OBJECT (operation), "error",
			  G_CALLBACK (load_error), NULL);
	g_signal_connect (G_OBJECT (operation), "progress",
			  G_CALLBACK (load_progress), NULL);
	
	load_pipeline = marlin_load_pipeline_new (operation);
	g_object_set (G_OBJECT (load_pipeline),
		      "filename", data->filename,
		      "sample", data->sample,
		      NULL);

	gst_element_set_state (GST_ELEMENT (load_pipeline), GST_STATE_PLAYING);
	return FALSE;
}

int
main (int argc,
      char **argv)
{
	struct _idle_data data;

	g_type_init ();

	gst_init (&argc, &argv);
	marlin_gst_register ();

	if (argc != 2) {
		g_print ("Usage: %s <sample file>\n", argv[0]);
		exit (1);
	}

	sample = marlin_sample_new ();

	data.sample = sample;
	data.filename = argv[1];

	new_filename = argv[2];
	mimetype = argv[3];
	
	mainloop = g_main_loop_new (NULL, FALSE);
	
	g_idle_add (idle_func, &data);
	g_main_loop_run (mainloop);

	return 0;
}
