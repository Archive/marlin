/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>

#include <marlin/marlin-block.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-channel.h>

#undef LOCK_DEBUG

/**
 * marlin_block_new:
 * @channel: The #MarlinChannel on which the block is made.
 * @frame_file: A #MarlinFile to use as the frame backing store.
 * @peak_file: A #MarlinFile to use as the peak backing store.
 *
 * Creates a new MarlinBlock.
 *
 * Return value: Newly allocated MarlinBlock.
 */
MarlinBlock *
marlin_block_new (MarlinChannel *channel,
		  MarlinFile *frame_file,
		  MarlinFile *peak_file)
{
	MarlinBlock *block;
	
	block = g_slice_new0 (MarlinBlock);

	block->lock = marlin_read_write_lock_new ();
	block->channel = channel;
	block->frame_file = frame_file;
	block->peak_file = peak_file;

	block->unmappable = TRUE;

	marlin_file_ref (block->frame_file);
	marlin_file_ref (block->peak_file);

	return block;
}

/**
 * marlin_block_free:
 * @block: A #MarlinBlock.
 *
 * Frees all resources used by @block.
 */
void
marlin_block_free (MarlinBlock *block)
{
	g_return_if_fail (block != NULL);

	WRITE_LOCK (block->lock);

	if (block->is_mapped) {
		marlin_channel_unmap_block (block->channel, block);
	}

	marlin_file_unref (block->frame_file);
	marlin_file_unref (block->peak_file);

	WRITE_UNLOCK (block->lock);

	marlin_read_write_lock_destroy (block->lock);
	g_slice_free (MarlinBlock, block);
}

/**
 * marlin_block_append:
 * @a: A #MarlinBlock.
 * @b: A #MarlinBlock.
 * 
 * Inserts @b into the list after @a.
 */
void
marlin_block_append (MarlinBlock *a,
		     MarlinBlock *b)
{
	g_return_if_fail (a != NULL);
	g_return_if_fail (b != NULL);

	WRITE_LOCK (a->lock);
	WRITE_LOCK (b->lock);

	if (a->next != NULL) {
		b->next = a->next;
		a->next->previous = b;
	} else {
		b->next = NULL;
	}

	a->next = b;
	b->previous = a;

	WRITE_UNLOCK (b->lock);
	WRITE_UNLOCK (a->lock);
}

/**
 * marlin_block_next:
 * @block: A #MarlinBlock.
 *
 * Gets the block after @block in the list.
 *
 * Returns: The next MarlinBlock.
 */
MarlinBlock *
marlin_block_next (MarlinBlock *block)
{
	MarlinBlock *next;

	READ_LOCK (block->lock);

	next = block->next;

	READ_UNLOCK (block->lock);
	
	return next;
}

/** 
 * marlin_block_previous:
 * @block: A #MarlinBlock.
 *
 * Gets the block before @block.
 *
 * Returns: The previous MarlinBlock.
 */
MarlinBlock *
marlin_block_previous (MarlinBlock *block)
{
	MarlinBlock *prev;

	READ_LOCK (block->lock);

	prev = block->previous;

	READ_UNLOCK (block->lock);

	return prev;
}

/**
 * marlin_block_last:
 * @block: A #MarlinBlock.
 *
 * Follows the list to find the last block in the list.
 *
 * Returns: The last MarlinBlock in the list.
 */
MarlinBlock *
marlin_block_last (MarlinBlock *block)
{
	READ_LOCK (block->lock);

	while (block->next) {
		MarlinBlock *old;
		
		old = block;
		block = old->next;

		READ_UNLOCK (old->lock);
		READ_LOCK (block->lock);
	}

	READ_UNLOCK (block->lock);

	return block;
}

static void G_GNUC_UNUSED
dump_peak (MarlinPeak *peak)
{
	g_print ("%d %d\n", (int)peak->high, (int)peak->low);
}

static void G_GNUC_UNUSED
dump_block (MarlinBlock *block)
{
	g_print ("-------\n");
	g_print ("Block: %p\n", block);
	g_print ("start: %llu\n", block->start);
	g_print ("end: %llu\n", block->end);
	g_print ("num_frames: %llu\n", block->num_frames);
	g_print ("num_peaks: %llu\n", block->num_peaks);
	g_print ("frame_offset: %llu\n", block->frame_offset);
	g_print ("peak_offset: %llu\n", block->peak_offset);
	g_print ("%s\n", block->is_mapped ? "Mapped" : "Unmapped");
}

static gpointer
create_peak_data (gpointer data,
		  guint64 num_frames,
		  guint64 *num_peaks)
{
	float *frame_data = (float *) data;
	MarlinPeak *peaks;
	guint64 p, peak_num, total;
	guint64 total_positive, total_negative;

	*num_peaks = num_frames / (guint64) MARLIN_FRAMES_PER_PEAK;
	if (num_frames % (guint64) MARLIN_FRAMES_PER_PEAK != 0) {
		(*num_peaks)++;
	}
	
	peaks = g_new0 (MarlinPeak, *num_peaks);
	
	total = 0;
	
	for (p = 0, peak_num = 0; p < *num_peaks; p++) {
		int j, frames_in_peak;
		float pos, neg;
		
		frames_in_peak = 0;
		total_positive = 0;
		total_negative = 0;
		pos = neg = 0.0;

		for (j = 0; j < MARLIN_FRAMES_PER_PEAK && total < num_frames; 
		     j++, total++) {
			float d;
			short s;
			
			d = frame_data[total];
			
			if (d < -1.0 || d > 1.0) {
				/* Data clipped so discount it */
				d = frame_data[total] = 0.0;
			}
			
			s = (short) (d * 256);

			if (d < 0.0) {
				neg += d;
				++total_negative;
			} else {
				pos += d;
				++total_positive;
			}
			
			peaks[peak_num].high = MAX (peaks[peak_num].high, s);
			peaks[peak_num].low = MIN (peaks[peak_num].low, s);
			frames_in_peak++;
		}
		
		/* Calculate the average of the positive and negatives */
		if (total_negative > 0) {
			peaks[peak_num].avg_negative = (short) ((neg / total_negative) * 256);
		} else {
			peaks[peak_num].avg_negative = 0;
		}

		if (total_positive > 0) {
			peaks[peak_num].avg_positive = (short) ((pos / total_positive) * 256);
		} else {
			peaks[peak_num].avg_positive = 0;
		}

/* 		dump_peak (&(peaks[peak_num])); */
		neg = pos = 0.0;
		peak_num++;
	}

	return peaks;
}

/* FIXME: Take an optional MarlinPeak data that can be used to set the peak
   data. */
/**
 * marlin_block_set_data:
 * @block: A #MarlinBlock.
 * @data: A pointer to float data.
 * @num_frames: The number of frames in @data.
 * @error: Pointer to a #GError for error returns.
 *
 * Sets the data in @block. Caller must hold lock.
 * 
 * Returns: TRUE on success, FALSE on failure with details in @error.
 */
gboolean
marlin_block_set_data (MarlinBlock *block,
		       gpointer data,
		       guint64 num_frames,
		       GError **error)
{
	gpointer peak_data;

	block->frame_offset = marlin_file_write_data (block->frame_file, 
						      data, 
						      num_frames * sizeof (float), 
						      error);
	if (block->frame_offset == -1) {
		return FALSE;
	}

	block->num_frames = num_frames;

	/* Create the peak data as we go */
	peak_data = create_peak_data (data, num_frames, &block->num_peaks);
	block->peak_offset = marlin_file_write_data (block->peak_file,
						     peak_data, 
						     block->num_peaks * sizeof (MarlinPeak), 
						     error);
	g_free (peak_data);

	if (block->peak_offset == -1) {
		return FALSE;
	}

	return TRUE;
}

/**
 * marlin_block_map:
 * @block: A #MarlinBlock.
 * @error: A pointer to a #GError for error returns.
 * 
 * Maps the block's temporary file into main memory.
 *
 * Returns: TRUE on success, FALSE on failure, with extra details in @error.
 */
gboolean
marlin_block_map (MarlinBlock *block,
		  GError **error)
{
	if (block->is_mapped) {
		return TRUE;
	}

	block->frame_region = marlin_file_map_region (block->frame_file,
						      block->frame_offset,
						      block->num_frames * sizeof (float),
						      NULL);
	if (block->frame_region == NULL) {
		g_warning ("Error mapping frames");
		return FALSE;
	}

	block->peak_region = marlin_file_map_region (block->peak_file,
						     block->peak_offset,
						     block->num_peaks * sizeof (MarlinPeak),
						     NULL);
	if (block->peak_region == NULL) {
		g_warning ("Error mapping peaks");
		return FALSE;
	} 

	block->is_mapped = TRUE;

	return TRUE;
}

/**
 * marlin_block_unmap:
 * @block: A #MarlinBlock
 *
 * Unmaps the memory used in the block.
 */	
void
marlin_block_unmap (MarlinBlock *block)
{
	if (!block->is_mapped) {
		return;
	}

	marlin_file_unmap_region (block->frame_file,
				  block->frame_region);
	marlin_file_unmap_region (block->peak_file,
				  block->peak_region);

	block->is_mapped = FALSE;
}

/**
 * marlin_block_get_frame_data:
 * @block: A #MarlinBlock.
 * @error: A pointer to a #GError for error returns.
 *
 * Gets the frame data for a block, mapping the block if required.
 * Caller needs to hold the lock.
 *
 * Returns: The frame data for @block, or NULL on failure.
 */
float *
marlin_block_get_frame_data (MarlinBlock *block)
{
	marlin_channel_map_block (block->channel, block);

	return (block->frame_region->address + block->frame_region->d_offset);
}

/**
 * marlin_block_get_peak_data:
 * @block: A #MarlinBlock.
 * @error: A pointer to a #GError for error returns.
 * 
 * Gets the peak data from a block, mapping the block if required.
 *
 * Returns: The MarlinPeak data for @block or NULL on failure.
 */
MarlinPeak *
marlin_block_get_peak_data (MarlinBlock *block)
{
	marlin_channel_map_block (block->channel, block);
	return (block->peak_region->address + block->peak_region->d_offset);
}
	
/**
 * marlin_block_split:
 * @block: A #MarlinBlock.
 * @split_frame: The frame on which the block is to be split.
 *
 * Splits @block on @split_frame and returns the newly created block,
 * using the same files as @block.
 * Given a block
 * 0 |--------------------| 19 (20 frames)
 * and splitting on frame 10
 * will give you 2 blocks
 * 0 |----------| 9 (10 frames)
 * 10|----------| 19 (10 frames)
 *
 * Returns: Newly created MarlinBlock.
 */
MarlinBlock *
marlin_block_split (MarlinBlock *block,
		    guint64 split_frame)
{
	MarlinBlock *new_block;
	guint64 frame_in_block;
	
	g_return_val_if_fail (split_frame <= block->end, NULL);
	g_return_val_if_fail (split_frame > block->start, NULL);
	
	WRITE_LOCK (block->lock);

	marlin_channel_unmap_block (block->channel, block);

	frame_in_block = (guint64) (split_frame - block->start);

	new_block = marlin_block_new (block->channel,
				      block->frame_file,
				      block->peak_file);
	
	new_block->start = split_frame;
	new_block->end = block->end;
	new_block->num_frames = (new_block->end - new_block->start) + 1;
	new_block->num_peaks = new_block->num_frames / (guint64) MARLIN_FRAMES_PER_PEAK;
	if (new_block->num_frames % (guint64) MARLIN_FRAMES_PER_PEAK != 0) {
 		++new_block->num_peaks;
	}

	block->end = split_frame - 1;
	block->num_frames = (block->end - block->start) + 1;
	block->num_peaks = block->num_frames / (guint64) MARLIN_FRAMES_PER_PEAK;
	if (block->num_frames % (guint64) MARLIN_FRAMES_PER_PEAK != 0) {
 		++block->num_peaks;
	}

 	new_block->frame_offset = block->frame_offset + (block->num_frames * sizeof (float));
	/* FIXME: the peaks should really be recalculated */
	new_block->peak_offset = block->peak_offset + (block->num_peaks * sizeof (MarlinPeak));

	WRITE_UNLOCK (block->lock);

	return new_block;
}

/**
 * marlin_block_copy:
 * @block: A #MarlinBlock.
 *
 * Makes a copy of @block. The copies share the same data
 * 
 * Returns: The newly created MarlinBlock copy.
 */
MarlinBlock *
marlin_block_copy (MarlinBlock *block)
{
	MarlinBlock *new_block;

	g_return_val_if_fail (block != NULL, NULL);

	READ_LOCK (block->lock);

	/* New blocks share the data that is written on the disk
	   because mmap can map the same disk region multiple times
	   and because we do non-destructive editing, the data will never
	   be touched */	
	new_block = marlin_block_new (block->channel,
				      block->frame_file,
				      block->peak_file);
	new_block->num_frames = block->num_frames;
	new_block->num_peaks = block->num_peaks;
	new_block->frame_offset = block->frame_offset;
	new_block->peak_offset = block->peak_offset;

	READ_UNLOCK (block->lock);

	return new_block;
}

/* List operations */

/**
 * marlin_block_copy_list:
 * @start_block: A #MarlinBlock.
 * @count: A pointer to a guint64 to store the number of frames copied.
 *
 * Creates a copy of the list starting with @start_block.
 *
 * Returns: The newly copied list of MarlinBlocks.
 */
MarlinBlock *
marlin_block_copy_list (MarlinBlock *start_block,
			guint64 *count)
{
	MarlinBlock *p;
	MarlinBlock *out_list = NULL, *ol;

	if (count) {
		*count = (guint64) 0;
	}

	ol = out_list;
	for (p = start_block; p; p = p->next) {
		MarlinBlock *block = marlin_block_copy (p);
		
		if (count) {
			*count += block->num_frames;
		}

		if (ol) {
			marlin_block_append (ol, block);
			
			ol = block;
		} else {
			out_list = ol = block;
		}
	}

	return out_list;
}

/**
 * marlin_block_free_list:
 * @block: A #MarlinBlock.
 *
 * Frees an entire list of blocks starting with @block.
 */
void
marlin_block_free_list (MarlinBlock *block)
{
	g_return_if_fail (block != NULL);

	while (block) {
		MarlinBlock *old_block;

		old_block = block;

		block = marlin_block_next (old_block);
		marlin_block_free (old_block);
	}
}

/**
 * marlin_block_move_blocks_list:
 * @block: A #MarlinBlock.
 * @channel: A #MarlinChannel.
 *
 * Moves @block from its parent channel to @channel.
 *
 * FIXME: This function name sucks.
 */
void
marlin_block_move_blocks_list (MarlinBlock *block,
			       MarlinChannel *new_channel)
{
	while (block) {
		block->channel = new_channel;
		block = marlin_block_next (block);
	}
}

/**
 * marlin_block_recalculate_ranges:
 * @block: A #MarlinBlock.
 *
 * Recalculates the ranges of the blocks, starting with
 * @block.
 * 
 * Returns: The number of frames in the list.
 */
guint64
marlin_block_recalculate_ranges (MarlinBlock *block)
{
	guint64 last_frame = (guint64) 0;

	while (block) {
		block->start = last_frame;
		block->end = block->start + block->num_frames - 1;

		last_frame = block->end + 1;
		block = block->next;
	}

	return last_frame;
}

guint
marlin_block_get_buffer (MarlinBlock  *block,
			 float        *buffer,
			 guint64       start,
			 guint         frames_needed,
			 MarlinBlock **end_block)
{
	guint copied = 0;
	guint offset_in_block, left_in_block, frames_to_copy;
	float *frame_data;

	g_return_val_if_fail (block != NULL, 0);

	READ_LOCK (block->lock);

	if (start > block->end) {
		g_print ("%llu > %llu\n", start, block->end);
		g_assert_not_reached ();
	}

	offset_in_block = start - block->start;
	left_in_block = block->num_frames - offset_in_block;

	frame_data = marlin_block_get_frame_data (block);

 copy_data:
	frames_to_copy = MIN (frames_needed, left_in_block);

	memcpy (buffer, frame_data + offset_in_block,
		frames_to_copy * sizeof (float));

	copied += frames_to_copy;

	if (left_in_block < frames_needed) {
		MarlinBlock *tmp_block;

		frames_needed -= frames_to_copy;
		buffer += frames_to_copy;

		tmp_block = block->next;
		if (tmp_block == NULL) {
			if (end_block) {
				*end_block = NULL;
			}

			READ_UNLOCK (block->lock);
			return copied;
		}

		READ_LOCK (tmp_block->lock);
		READ_UNLOCK (block->lock);

		block = tmp_block;

		offset_in_block = 0;

		frame_data = marlin_block_get_frame_data (block);
		left_in_block = block->num_frames;

		/* copy a bit more */
		goto copy_data;
	}

	if (end_block) {
		if (start + copied >= block->end) {
			*end_block = block->next;
		} else {
			*end_block = block;
		}
	}

	READ_UNLOCK (block->lock);
	return copied;
}
