/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>

#include <glib.h>
#include <pthread.h>

#include <marlin-read-write-lock.h>

struct _MarlinReadWriteLock {
	GMutex *mutex;
 	GCond *read; /* Wait for read */
	GCond *write; /* Wait for write */
	
	int r_active; /* Readers active */
	gboolean w_active; /* Writer active */
	int r_wait; /* Readers waiting */
	int w_wait; /* Writers waiting */
};

MarlinReadWriteLock *
marlin_read_write_lock_new (void)
{
	MarlinReadWriteLock *rwl;

	rwl = g_slice_new0 (MarlinReadWriteLock);
	
	rwl->mutex = g_mutex_new ();
	rwl->read = g_cond_new ();
	rwl->write = g_cond_new ();

	return rwl;
}

int
marlin_read_write_lock_destroy (MarlinReadWriteLock *rwl)
{
	/* Lock the lock before modifying anything */
	g_mutex_lock (rwl->mutex);

	if (rwl->r_active > 0 || rwl->w_active) {
		g_mutex_unlock (rwl->mutex);
		return EBUSY;
	}

	/* Unlock before destroying */
	g_mutex_unlock (rwl->mutex);

	g_mutex_free (rwl->mutex);
	g_cond_free (rwl->read);
	g_cond_free (rwl->write);

	g_slice_free (MarlinReadWriteLock, rwl);

	return 0;
}

static void
rwl_read_cleanup (void *data)
{
	MarlinReadWriteLock *rwl = (MarlinReadWriteLock *) data;

	rwl->r_wait--;
	g_mutex_unlock (rwl->mutex);
}

static void
rwl_write_cleanup (void *data)
{
	MarlinReadWriteLock *rwl = (MarlinReadWriteLock *) data;

	rwl->w_wait--;
	g_mutex_unlock (rwl->mutex);
}

void
marlin_read_write_lock_lock (MarlinReadWriteLock *rwl,
			     MarlinReadWriteLockMode mode)
{

	g_mutex_lock (rwl->mutex);

	switch (mode) {
	case MARLIN_READ_WRITE_LOCK_MODE_READ:
		if (rwl->w_active) {
			rwl->r_wait++;

			pthread_cleanup_push (rwl_read_cleanup, (void *) rwl);
			while (rwl->w_active) {
				g_cond_wait (rwl->read, rwl->mutex);
			}
			pthread_cleanup_pop (0);
			
			rwl->r_wait--;
		}

		rwl->r_active++;
		break;

	case MARLIN_READ_WRITE_LOCK_MODE_WRITE:
		if (rwl->w_active || rwl->r_active > 0) {
			rwl->w_wait++;

			pthread_cleanup_push (rwl_write_cleanup, (void *) rwl);
			while (rwl->w_active || rwl->r_active > 0) {
				g_cond_wait (rwl->write, rwl->mutex);
			}
			pthread_cleanup_pop (0);

			rwl->w_wait--;
		}

		rwl->w_active = 1;
		break;
	}

	g_mutex_unlock (rwl->mutex);
}

void
marlin_read_write_lock_unlock (MarlinReadWriteLock *rwl,
			       MarlinReadWriteLockMode mode)
{
	g_mutex_lock (rwl->mutex);

	switch (mode) {
	case MARLIN_READ_WRITE_LOCK_MODE_READ:
		rwl->r_active--;
		
		if (rwl->r_active == 0 && rwl->w_wait > 0) {
			g_cond_signal (rwl->write);
		}
		
		break;

	case MARLIN_READ_WRITE_LOCK_MODE_WRITE:
		rwl->w_active = 0;
		
		if (rwl->r_wait > 0) {
			g_cond_broadcast (rwl->read);
		} else if (rwl->w_wait > 0) {
			g_cond_signal (rwl->write);
		}
		
		break;
	}

	g_mutex_unlock (rwl->mutex);
}
