/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_POSITION_SPINNER_H__
#define __MARLIN_POSITION_SPINNER_H__

#include <gtk/gtk.h>

#define MARLIN_POSITION_SPINNER_TYPE (marlin_position_spinner_get_type ())
#define MARLIN_POSITION_SPINNER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_POSITION_SPINNER_TYPE, MarlinPositionSpinner))
#define MARLIN_POSITION_SPINNER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_POSITION_SPINNER_TYPE, MarlinPositionSpinnerClass))

typedef struct _MarlinPositionSpinner MarlinPositionSpinner;
typedef struct _MarlinPositionSpinnerPrivate MarlinPositionSpinnerPrivate;
typedef struct _MarlinPositionSpinnerClass MarlinPositionSpinnerClass;

struct _MarlinPositionSpinner {
	GtkSpinButton spin_button;

	MarlinPositionSpinnerPrivate *priv;
};

struct _MarlinPositionSpinnerClass {
	GtkSpinButtonClass parent_class;

	void (* display_changed) (MarlinPositionSpinner *spinner);
};

GType marlin_position_spinner_get_type (void);
GtkWidget *marlin_position_spinner_new (void);
GtkWidget *marlin_position_spinner_label (MarlinPositionSpinner *spinner);

#endif
