/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PLAY_PIPELINE_H__
#define __MARLIN_PLAY_PIPELINE_H__

#include <marlin/marlin-pipeline.h>
#include <marlin/marlin-sample.h>

#define MARLIN_PLAY_PIPELINE_TYPE (marlin_play_pipeline_get_type ())
#define MARLIN_PLAY_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_PLAY_PIPELINE_TYPE, MarlinPlayPipeline))
#define MARLIN_PLAY_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_PLAY_PIPELINE_TYPE, MarlinPlayPipelineClass))
#define IS_MARLIN_PLAY_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_PLAY_PIPELINE_TYPE))
#define IS_MARLIN_PLAY_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_PLAY_PIPELINE_TYPE))

typedef struct _MarlinPlayPipeline MarlinPlayPipeline;
typedef struct _MarlinPlayPipelineClass MarlinPlayPipelineClass;
typedef struct _MarlinPlayPipelinePrivate MarlinPlayPipelinePrivate;

struct _MarlinPlayPipeline {
	MarlinPipeline parent_pipeline;

	MarlinPlayPipelinePrivate *priv;
};

struct _MarlinPlayPipelineClass {
	MarlinPipelineClass parent_class;

	void (*level) (GstElement *element,
		       double time,
		       int channels,
		       double *rms,
		       double *peak,
		       double *decay);
	void (*eos) (GstElement *element);
	void (*position) (GstElement *element,
			  guint64 position);
};

GType marlin_play_pipeline_get_type (void);

MarlinPlayPipeline *marlin_play_pipeline_new (void);
MarlinPlayPipeline *marlin_play_pipeline_new_from_sample (MarlinSample *sample);
void marlin_play_pipeline_seek (MarlinPlayPipeline *pipeline,
				guint64 position);
void marlin_play_pipeline_seek_range (MarlinPlayPipeline *pipeline,
				      guint64 start,
				      guint64 end);
void marlin_play_pipeline_set_repeat (MarlinPlayPipeline *pipeline,
				      gboolean repeat);

#endif
