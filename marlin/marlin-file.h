/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_FILE_H__
#define __MARLIN_FILE_H__

#include <sys/types.h>

#include <glib.h>

#define MARLIN_FILE_ERROR marlin_file_error_quark ()

typedef struct _MarlinFile {
	int fd; /* File handle */
	char *filename; /* Full pathname */

	int ref_count;
} MarlinFile;

typedef struct _MarlinFileRegion {
	MarlinFile *file; /* File to which this region belongs */
	off_t offset;
	off_t d_offset;

	gpointer address;
	size_t length;
} MarlinFileRegion;

typedef enum {
	MARLIN_FILE_ERROR_IO, /* Something failed while disk IO...*/
	MARLIN_FILE_ERROR_NO_SPACE, /* Disk IO failed with no space */
	MARLIN_FILE_ERROR_MEMORY, /* MMap failed */
} MarlinFileError;

MarlinFile *marlin_file_new (const char *filename,
			     GError **error);
void marlin_file_free (MarlinFile *file);

void marlin_file_ref (MarlinFile *file);
void marlin_file_unref (MarlinFile *file);

MarlinFileRegion *marlin_file_map_region (MarlinFile *file,
					  off_t offset,
					  size_t length,
					  GError **error);

void marlin_file_unmap_region (MarlinFile *file,
			       MarlinFileRegion *region);

off_t marlin_file_write_data (MarlinFile *file,
			      gpointer data,
			      size_t length,
			      GError **error);

#endif
