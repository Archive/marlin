/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gst/gst.h>

#include <gtk/gtk.h>
#include <gnome.h>

#include <marlin/marlin-record-pipeline.h>
#include <marlin/marlin-play-pipeline.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-stock.h>

/* Evil globals */
GstElementState state = GST_STATE_NULL;

GtkWidget *window;
MarlinRecordPipeline *pipeline;
MarlinSample *sample;

static void
record_clicked (GtkButton *b,
		gpointer data)
{
	marlin_pipeline_set_state (pipeline, GST_STATE_PLAYING);
	state = GST_STATE_PLAYING;
}

static void
play_eos (MarlinPipeline *pp,
	  MarlinSample *sample)
{
	g_print ("Play finished\n");

	g_object_unref (G_OBJECT (pp));

	g_object_unref (G_OBJECT (sample));

	gtk_main_quit ();
}

static void
record_eos (MarlinPipeline *pipeline,
	    MarlinSample *sample)
{
	MarlinPlayPipeline *play_pipeline;
	char *filename;
	guint64 frames;
	guint channels, samplerate;

	marlin_pipeline_set_state (pipeline, GST_STATE_READY);
	state = GST_STATE_READY;

	g_object_unref (G_OBJECT (pipeline));
	
	g_object_get (G_OBJECT (sample),
		      "filename", &filename,
		      "channels", &channels,
		      "total-frames", &frames,
		      "sample-rate", &samplerate,
		      NULL);
	
	g_print ("Information\n");
	g_print ("-----------\n");
	g_print ("filename: %s\n", filename);
	g_free (filename);
	g_print ("Total frames: %llu\n", frames);
	g_print ("Channels: %d\n", channels);
	g_print ("Sample Rate: %d\n", samplerate);
	
	play_pipeline = marlin_play_pipeline_new_from_sample (sample);
	g_signal_connect (G_OBJECT (play_pipeline), "eos",
			  G_CALLBACK (play_eos), sample);
	marlin_pipeline_set_state (play_pipeline, GST_STATE_PLAYING);
}
	
static void
stop_clicked (GtkButton *b,
	      gpointer data)
{
	marlin_record_pipeline_set_eos (MARLIN_RECORD_PIPELINE (pipeline));
}

static void
destroy_cb (GObject *obj,
	    gpointer d)
{
	gtk_main_quit ();
}

int
main (int argc,
      char **argv)
{
	GtkWidget *hbox;
	GtkWidget *record, *stop;

	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);

	marlin_mt_initialise ();

	/* Create a sample to record into */
	sample = marlin_sample_new ();
	g_object_set (G_OBJECT (sample),
		      "filename", "tempfile",
		      NULL);
	/* Create the record pipeline */
	pipeline = marlin_record_pipeline_new_with_sample (sample);
	g_signal_connect (G_OBJECT (pipeline), "eos",
			  G_CALLBACK (record_eos), sample);
	
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (destroy_cb), NULL);

	gtk_window_set_title (GTK_WINDOW (window), argv[1]);
	hbox = gtk_hbox_new (TRUE, 0);
	gtk_container_add (GTK_CONTAINER (window), hbox);

	record = gtk_button_new_with_label ("record");
	gtk_box_pack_start (GTK_BOX (hbox), record, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (record), "clicked",
			  G_CALLBACK (record_clicked), NULL);

	stop = gtk_button_new_with_label ("stop");
	gtk_box_pack_start (GTK_BOX (hbox), stop, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (stop), "clicked",
			  G_CALLBACK (stop_clicked), NULL);

	gtk_widget_show_all (window);

	gtk_main ();
}
