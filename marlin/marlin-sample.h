/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifndef __MARLIN_SAMPLE_H__
#define __MARLIN_SAMPLE_H__

#include <glib-object.h>
#include <gst/gst.h>

#include <marlin/marlin-types.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-undo-manager.h>
#include <marlin/marlin-utils.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_SAMPLE_ERROR marlin_sample_error_quark ()

#define MARLIN_SAMPLE_TYPE (marlin_sample_get_type ())
#define MARLIN_SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_SAMPLE_TYPE, MarlinSample))
#define MARLIN_SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_SAMPLE_TYPE, MarlinSampleClass))
#define IS_MARLIN_SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_SAMPLE_TYPE))
#define IS_MARLIN_SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), MARLIN_SAMPLE_TYPE))

typedef struct _MarlinSampleClass MarlinSampleClass;
typedef struct _MarlinSamplePrivate MarlinSamplePrivate;

typedef struct _MarlinSampleFade {
	MarlinRange range;

	float in_level; /* The level [0, 1] before the fade starts */
	float out_level; /* The level [0, 1] after the fade ends */

	/* These are offset from the range.start */
	guint64 fade_start; /* The sample position the fade starts at */
	guint64 fade_end; /* The position the fade ends at */
} MarlinSampleFade;

struct _MarlinSample {
	GObject parent;

	MarlinSamplePrivate *priv;
};

struct _MarlinSampleClass {
	GObjectClass parent_class;

	void (*data_changed) (MarlinSample *sample,
			      MarlinRange *range);
};

typedef enum {
	MARLIN_SAMPLE_ERROR_CHANNEL_MISMATCH, /* Number of channels in the src
						 and destination samples did
						 not match */
	MARLIN_SAMPLE_ERROR_RATE_MISMATCH, /* Sample rate of the src and
					      destination samples did not
					      match */
} MarlinSampleError;

GType marlin_sample_get_type (void);
MarlinSample *marlin_sample_new (void);
MarlinSample *marlin_sample_new_from_sample_with_range (MarlinSample *src,
							MarlinRange *range,
							GError **error);
MarlinSample *marlin_sample_new_from_sample (MarlinSample *src,
					     GError **error);
MarlinSample *marlin_sample_new_from_selection (MarlinSample *src,
						GError **error);

MarlinChannel *marlin_sample_get_channel (MarlinSample *sample,
					  guint channel_num);

/* Sample editting functions */
gboolean marlin_sample_delete_range (MarlinSample *sample,
				     MarlinOperation *operation,
				     MarlinRange *range,
				     MarlinUndoContext *ctxt,
				     GError **error);
gboolean marlin_sample_clear_range (MarlinSample *sample,
				    MarlinOperation *operation,
				    MarlinRange *range,
				    MarlinUndoContext *ctxt,
				    GError **error);
gboolean marlin_sample_crop_range (MarlinSample *sample,
				   MarlinOperation *operation,
				   MarlinRange *range,
				   MarlinUndoContext *ctxt,
				   GError **error);

gboolean marlin_sample_swap_channels (MarlinSample *sample,
				      MarlinOperation *operation,
				      MarlinUndoContext *ctxt,
				      GError **error);
gboolean marlin_sample_mix (MarlinSample *dest,
			    MarlinSample *src,
			    double src_db,
			    double dest_db,
			    MarlinRange *range,
			    gboolean clip,
			    MarlinOperation *operation,
			    MarlinUndoContext *ctxt,
			    GError **error);
gboolean marlin_sample_insert (MarlinSample *dest,
			       MarlinSample *src,
			       guint64 position,
			       MarlinCoverage coverage,
			       MarlinUndoContext *ctxt,
			       GError **error);
gboolean marlin_sample_invert_range (MarlinSample *sample,
				     MarlinRange *range,
				     MarlinUndoContext *ctxt,
				     GError **error);
gboolean marlin_sample_adjust_volume_range (MarlinSample *sample,
					    float db,
					    MarlinRange *range,
					    MarlinUndoContext *ctxt,
					    GError **error);
gboolean marlin_sample_insert_silence (MarlinSample *sample,
				       MarlinOperation *operation,
				       guint64 position,
				       guint64 length,
				       MarlinUndoContext *ctxt,
				       GError **error);

gboolean marlin_sample_remove_channel (MarlinSample *sample,
				       MarlinChannelPosition channel,
				       gboolean mix,
				       MarlinOperation *operation,
				       MarlinUndoContext *ctxt,
				       GError **error);
gboolean marlin_sample_add_channel (MarlinSample *sample,
				    MarlinChannelPosition channel,
				    gboolean clone,
				    MarlinOperation *operation,
				    MarlinUndoContext *ctxt,
				    GError **error);
gboolean marlin_sample_fade (MarlinSample *sample,
			     MarlinSampleFade *fade,
			     MarlinOperation *operation,
			     MarlinUndoContext *ctxt,
			     GError **error);
gboolean marlin_sample_crossfade (MarlinSample      *src,
				  MarlinSample      *dest,
				  MarlinSampleFade  *src_fade,
				  MarlinSampleFade  *dest_fade,
				  MarlinOperation   *operation,
				  MarlinUndoContext *ctxt,
				  GError           **error);
gboolean marlin_sample_paste_with_crossfade (MarlinSample      *dest,
					     MarlinSample      *src,
					     guint64            insert_position,
					     MarlinRange       *range,
					     guint              fade_length,
					     MarlinOperation   *operation,
					     MarlinUndoContext *ctxt,
					     GError           **error);

guint64 marlin_sample_next_zero (MarlinSample *sample,
				 guint64 position,
				 MarlinCoverage coverage);
guint64 marlin_sample_previous_zero (MarlinSample *sample,
				     guint64 position,
				     MarlinCoverage coverage);

gboolean marlin_sample_reverse_range (MarlinSample *sample,
				      MarlinOperation *operation,
				      MarlinRange *range,
				      MarlinUndoContext *ctxt,
				      GError **error);
gboolean marlin_sample_expand_range (MarlinSample      *sample,
				     MarlinRange       *range,
				     guint64            new_length,
				     MarlinOperation   *operation,
				     MarlinUndoContext *ctxt,
				     GError           **error);
gboolean marlin_sample_expand_mix (MarlinSample      *src,
				   MarlinSample      *dest,
				   MarlinRange       *src_range,
				   MarlinRange       *dest_range,
				   double             src_db,
				   double             dest_db,
				   MarlinOperation   *operation,
				   MarlinUndoContext *ctxt,
				   GError           **error);
gboolean marlin_sample_normalize_range (MarlinSample *sample,
					float db,
					MarlinRange *range,
					MarlinUndoContext *ctxt,
					GError **error);
void marlin_sample_read_lock (MarlinSample *sample);
void marlin_sample_read_unlock (MarlinSample *sample);

void marlin_sample_data_changed (MarlinSample *sample,
				 MarlinRange *range);

void marlin_sample_frame_count_changed (MarlinSample *sample);

gboolean marlin_sample_process_ladspa (MarlinSample *sample,
				       LADSPA_Descriptor *ladspa,
				       MarlinRange       *range,
				       MarlinOperation   *operation,
				       MarlinUndoContext *ctxt,
				       GError           **error);

#ifdef __cplusplus
}
#endif

#endif
