/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include <string.h>

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-cross-fader.h>
#include <marlin/marlin-cursors.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-utils.h>

enum {
	PROP_0,
	PROP_SRC,
	PROP_DEST,
	PROP_SCALE,
};

enum _box {
	SRC_IN,
	SRC_OUT,
	DEST_IN,
	DEST_OUT,
	NONE
};

struct _MarlinCrossFaderPrivate {
	MarlinSample *src, *dest; /* You mix src into the dest. */
	MarlinSampleFade *src_fader, *dest_fader; /* The faders for each
						     thing to be mixed */
	MarlinRange src_range, dest_range;

	MarlinScale scale;

	guint ms; /* Length in milliseconds */
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_CROSS_FADER_TYPE, MarlinCrossFaderPrivate))
G_DEFINE_TYPE (MarlinCrossFader, marlin_cross_fader, MARLIN_GRID_TYPE);

static void
finalize (GObject *object)
{
	MarlinCrossFader *xfade;
	MarlinCrossFaderPrivate *priv;

	xfade = MARLIN_CROSS_FADER (object);
	priv = xfade->priv;

	g_free (priv->src_fader);
	g_free (priv->dest_fader);

	G_OBJECT_CLASS (marlin_cross_fader_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinCrossFader *xfade;
	MarlinCrossFaderPrivate *priv;

	xfade = (MarlinCrossFader *) object;
	priv = xfade->priv;

	if (priv->src != NULL) {
		g_object_unref (G_OBJECT (priv->src));
		priv->src = NULL;
	}

	if (priv->dest != NULL) {
		g_object_unref (G_OBJECT (priv->dest));
		priv->dest = NULL;
	}

	G_OBJECT_CLASS (marlin_cross_fader_parent_class)->dispose (object);
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	MarlinCrossFader *xfade;
	MarlinCrossFaderPrivate *priv;
	MarlinSample *sample;

	xfade = MARLIN_CROSS_FADER (object);
	priv = xfade->priv;

	switch (prop_id) {
	case PROP_SRC:
		sample = g_value_get_object (value);
		if (priv->src == sample) {
			return;
		} else {
			if (priv->src) {
				g_object_unref (G_OBJECT (priv->src));
			}

			priv->src = sample;
			g_object_ref (G_OBJECT (sample));
		}
		break;

	case PROP_DEST:
		sample = g_value_get_object (value);
		if (priv->dest == sample) {
			return;
		} else {
			if (priv->dest) {
				g_object_unref (G_OBJECT (priv->dest));
			}

			priv->dest = sample;
			g_object_ref (G_OBJECT (sample));
		}

		break;

	case PROP_SCALE:
		priv->scale = g_value_get_enum (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	MarlinCrossFader *xfade;
	MarlinCrossFaderPrivate *priv;

	xfade = MARLIN_CROSS_FADER (object);
	priv = xfade->priv;

	switch (prop_id) {

	case PROP_SRC:
		g_value_set_object (value, priv->src);
		break;

	case PROP_DEST:
		g_value_set_object (value, priv->dest);
		break;

	case PROP_SCALE:
		g_value_set_enum (value, priv->scale);
		break;

	default:
		break;
	}
}

static void
draw_x_axis (MarlinGrid   *grid,
	     cairo_t      *cr,
	     GdkRectangle *rect)
{
	MarlinCrossFader *fader = (MarlinCrossFader *) grid;
	MarlinCrossFaderPrivate *priv = fader->priv;
	cairo_text_extents_t extents;
	float divwidth = (float) rect->width / 10.0f;
	char *label;
	int text_x, text_y;

	cairo_save (cr);

	if (priv->ms % 1000 == 0) {
		label = g_strdup_printf (_("%d seconds"), priv->ms / 1000);
	} else {
		label = g_strdup_printf (_("%.3f seconds"),
					 (float) priv->ms / 1000.0f);
	}
	cairo_text_extents (cr, label, &extents);

	text_x = rect->x + (rect->width - extents.width) / 2;
	text_y = rect->y + rect->height + 8 + (15 - extents.height) / 2;
	cairo_move_to (cr, text_x, text_y);
	cairo_show_text (cr, label);
	g_free (label);

	cairo_set_line_width (cr, 1);
	cairo_set_antialias (cr, CAIRO_ANTIALIAS_NONE);

	cairo_move_to (cr, rect->x + divwidth,
		       rect->y + rect->height + 15);
	cairo_line_to (cr, rect->x + divwidth,
		       rect->y + rect->height);
	cairo_stroke (cr);

	cairo_move_to (cr, rect->x + divwidth,
		       rect->y + rect->height + 8);
	cairo_line_to (cr, text_x - 3,
		       rect->y + rect->height + 8);
	cairo_stroke (cr);

	cairo_move_to (cr, text_x + extents.width + 3,
		       rect->y + rect->height + 8);
	cairo_line_to (cr, rect->x + rect->width - divwidth,
		       rect->y + rect->height + 8);
	cairo_stroke (cr);

	cairo_move_to (cr, rect->x + rect->width - divwidth,
		       rect->y + rect->height + 15);
	cairo_line_to (cr, rect->x + rect->width - divwidth,
		       rect->y + rect->height);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
marlin_cross_fader_class_init (MarlinCrossFaderClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	MarlinGridClass *g_class = (MarlinGridClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	g_class->draw_x_axis = draw_x_axis;

	g_type_class_add_private (o_class, sizeof (MarlinCrossFaderPrivate));
	g_object_class_install_property (o_class, PROP_SRC,
					 g_param_spec_object ("src", "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_DEST,
					 g_param_spec_object ("dest", "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	g_object_class_install_property (o_class, PROP_SCALE,
					 g_param_spec_enum ("scale", "", "",
							    MARLIN_TYPE_SCALE,
							    MARLIN_SCALE_LOG,
							    G_PARAM_READWRITE));
}

#define CROSSFADE_WIDTH 100000
#define TENTH_WIDTH 10000
#define NINTY_WIDTH 90000

static void
make_fader_curve (MarlinGridCurve *curve,
		  MarlinGridPoint *in,
		  MarlinGridPoint *out)
{
	MarlinGridPoint *slave;

	slave = marlin_grid_point_new (0, 0.0);
	marlin_grid_curve_add_point (curve, slave, MARLIN_GRID_POINT_FIXED);

	marlin_grid_point_set_slave (in, slave);
	marlin_grid_curve_add_point (curve, in, MARLIN_GRID_POINT_FREE_Y);

	marlin_grid_curve_add_point (curve, out, MARLIN_GRID_POINT_FREE_Y);

	slave = marlin_grid_point_new (CROSSFADE_WIDTH, 0.0);
	marlin_grid_point_set_slave (out, slave);
	marlin_grid_curve_add_point (curve, slave, MARLIN_GRID_POINT_FIXED);
}

static void
marlin_cross_fader_init (MarlinCrossFader *xfade)
{
	MarlinCrossFaderPrivate *priv;
	MarlinGridCurve *curve;
	MarlinRange range;

	xfade->priv = GET_PRIVATE (xfade);
	priv = xfade->priv;

	priv->scale = MARLIN_SCALE_LOG;

	range.start = 0;
	range.finish = CROSSFADE_WIDTH;

	xfade->src_in = marlin_grid_point_new (TENTH_WIDTH, 0.0);
	xfade->src_out = marlin_grid_point_new (NINTY_WIDTH, 1.0);

	curve = marlin_grid_curve_new (&range);
	curve->colour.red = 65535;
	make_fader_curve (curve, xfade->src_in, xfade->src_out);
	marlin_grid_add_curve (MARLIN_GRID (xfade), curve);

	xfade->dest_in = marlin_grid_point_new (TENTH_WIDTH, 1.0);
	xfade->dest_out = marlin_grid_point_new (NINTY_WIDTH, 0.0);

	curve = marlin_grid_curve_new (&range);
	curve->colour.blue = 65535;
	make_fader_curve (curve, xfade->dest_in, xfade->dest_out);
	marlin_grid_add_curve (MARLIN_GRID (xfade), curve);
}

void
marlin_cross_fader_set_length (MarlinCrossFader *fader,
			       guint             ms)
{
	MarlinCrossFaderPrivate *priv = fader->priv;
	GtkWidget *widget = GTK_WIDGET (fader);

	priv->ms = ms;

	/* Redraw the grid: It'd be nice to be able to force
	   a redraw of only the x axis? */
	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		rect.x = widget->allocation.x;
		rect.y = widget->allocation.y;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}
}

void
marlin_cross_fader_set_levels (MarlinCrossFader *fader,
			       float             src_in,
			       float             src_out,
			       float             dest_in,
			       float             dest_out)
{
	marlin_grid_point_set (fader->src_in,
			       fader->src_in->x, src_in);
	marlin_grid_point_set (fader->src_out,
			       fader->src_out->x, src_out);
	marlin_grid_point_set (fader->dest_in,
			       fader->dest_in->x, dest_in);
	marlin_grid_point_set (fader->dest_out,
			       fader->dest_out->x, dest_out);
}

void
marlin_cross_fader_get_levels (MarlinCrossFader *fader,
			       float            *src_in,
			       float            *src_out,
			       float            *dest_in,
			       float            *dest_out)
{
	*src_in = fader->src_in->level;
	*src_out = fader->src_out->level;
	*dest_in = fader->dest_in->level;
	*dest_out = fader->dest_out->level;
}
