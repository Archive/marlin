/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin/marlin-grid-point.h>

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_GRID_POINT_TYPE, MarlinGridPointPrivate))

enum {
	MOVED,
	LAST_SIGNAL
};

struct _MarlinGridPointPrivate {
	/* <private> */
	gboolean selected;
	MarlinGridPoint *slave;
	MarlinGridPoint *lower_bound;
	MarlinGridPoint *upper_bound;
};

G_DEFINE_TYPE (MarlinGridPoint, marlin_grid_point, G_TYPE_OBJECT);
static guint32 signals[LAST_SIGNAL] = {0, };

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (marlin_grid_point_parent_class)->finalize (object);
}

static void
marlin_grid_point_class_init (MarlinGridPointClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	o_class->finalize = finalize;

	g_type_class_add_private (klass, sizeof (MarlinGridPointPrivate));

	signals[MOVED] = g_signal_new ("data-changed",
				       G_TYPE_FROM_CLASS (klass),
				       G_SIGNAL_RUN_FIRST |
				       G_SIGNAL_NO_RECURSE,
				       G_STRUCT_OFFSET (MarlinGridPointClass, moved),
				       NULL, NULL,
				       g_cclosure_marshal_VOID__VOID,
				       G_TYPE_NONE, 0);
}

static void
marlin_grid_point_init (MarlinGridPoint *point)
{
	point->priv = GET_PRIVATE (point);
}

MarlinGridPoint *
marlin_grid_point_new (guint64 x,
		       float   level)
{
	MarlinGridPoint *point;

	point = g_object_new (MARLIN_GRID_POINT_TYPE, NULL);
	point->x = x;
	point->level = level;

	return point;
}

void
marlin_grid_point_set (MarlinGridPoint *point,
		       guint64          x,
		       float            level)
{
	point->x = x;
	point->level = level;

	if (point->priv->slave) {
		MarlinGridPoint *slave = point->priv->slave;

		marlin_grid_point_set (slave, slave->x, level);
	}

	g_signal_emit (point, signals[MOVED], 0);
}

/**
 * marlin_grid_point_set_slave:
 * @master: The master #MarlinGridPoint
 * @slave: The slave #MarlinGridPoint
 *
 * Sets @slave to be controlled by @master. When @master's level changes,
 * so does @slave's
 */
void
marlin_grid_point_set_slave (MarlinGridPoint *master,
			     MarlinGridPoint *slave)
{
	master->priv->slave = slave;
	slave->level = master->level;
}
