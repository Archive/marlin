/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Notzed <notzed@ximian.com>
 *
 *  Copyright 2001 - 2002 Ximian, Inc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_MSGPORT_H__
#define __MARLIN_MSGPORT_H__

/* double-linked list yeah another one, deal */
typedef struct _MarlinDListNode {
	struct _MarlinDListNode *next;
	struct _MarlinDListNode *prev;
} MarlinDListNode;

typedef struct _MarlinDList {
	struct _MarlinDListNode *head;
	struct _MarlinDListNode *tail;
	struct _MarlinDListNode *tailpred;
} MarlinDList;

#define MARLIN_DLIST_INITIALISER(l) { (MarlinDListNode *)&l.tail, 0, (MarlinDListNode *)&l.head }

void marlin_dlist_init (MarlinDList *v);
MarlinDListNode *marlin_dlist_addhead (MarlinDList *l,
				       MarlinDListNode *n);
MarlinDListNode *marlin_dlist_addtail (MarlinDList *l,
				       MarlinDListNode *n);
MarlinDListNode *marlin_dlist_remove (MarlinDListNode *n);
MarlinDListNode *marlin_dlist_remhead (MarlinDList *l);
MarlinDListNode *marlin_dlist_remtail (MarlinDList *l);
int marlin_dlist_empty (MarlinDList *l);
int marlin_dlist_length (MarlinDList *l);

/* message ports - a simple inter-thread 'ipc' primitive */
/* opaque handle */
typedef struct _MarlinMsgPort MarlinMsgPort;

/* Header for any message */
typedef struct _MarlinMsg {
	MarlinDListNode ln;
	MarlinMsgPort *reply_port;
} MarlinMsg;

MarlinMsgPort *marlin_msgport_new (void);
void marlin_msgport_destroy (MarlinMsgPort *mp);

/* get a fd that can be used to wait on the port asynchronously */
int marlin_msgport_fd (MarlinMsgPort *mp);

void marlin_msgport_put (MarlinMsgPort *mp,
			 MarlinMsg *msg);
MarlinMsg *marlin_msgport_wait (MarlinMsgPort *mp);
MarlinMsg *marlin_msgport_get (MarlinMsgPort *mp);
void marlin_msgport_reply (MarlinMsg *msg);

/* marlin threads,
   a server thread with a message based request-response,
   and flexible queuing */
typedef struct _MarlinThread MarlinThread;

typedef enum {
	MARLIN_THREAD_QUEUE = 0,
	MARLIN_THREAD_DROP,
	MARLIN_THREAD_NEW,
} marlin_thread_t;

typedef void (*MarlinThreadFunc) (MarlinThread *thread,
				  MarlinMsg *msg,
				  void *data);

MarlinThread *marlin_thread_new (marlin_thread_t type);
void marlin_thread_destroy (MarlinThread *t);
void marlin_thread_set_queue_limit (MarlinThread *t,
				    int limit);
void marlin_thread_set_msg_lost (MarlinThread *t,
				 MarlinThreadFunc lost,
				 void *data);
void marlin_thread_set_msg_received (MarlinThread *m,
				     MarlinThreadFunc received,
				     void *data);
void marlin_thread_set_msg_destroy (MarlinThread *t,
				    MarlinThreadFunc destroy,
				    void *data);
void marlin_thread_set_reply_port (MarlinThread *t,
				   MarlinMsgPort *reply_port);
void marlin_thread_msg_received (MarlinThread *t,
				 MarlinThreadFunc received,
				 void *data);
void marlin_thread_put (MarlinThread *t,
			MarlinMsg *msg);
int marlin_thread_busy (MarlinThread *t);

typedef struct _MarlinMutex MarlinMutex;

typedef enum _marlin_mutex_t {
	MARLIN_MUTEX_SIMPLE,
	MARLIN_MUTEX_REC,
} marlin_mutex_t;

MarlinMutex *marlin_mutex_new (marlin_mutex_t type);
int marlin_mutex_destroy (MarlinMutex *m);
int marlin_mutex_lock (MarlinMutex *m);
int marlin_mutex_unlock (MarlinMutex *m);
void marlin_mutex_assert_locked (MarlinMutex *m);

#endif
