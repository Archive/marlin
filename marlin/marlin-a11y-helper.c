/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2006 Iain Holmes and others
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#include <marlin/marlin-a11y-helper.h>
#include <atk/atk.h>

static void
add_relation (AtkRelationSet *set,
	      AtkRelationType type,
	      AtkObject *target)
{
        AtkRelation *relation;

	relation = atk_relation_set_get_relation_by_type (set, type);

	if (relation != NULL) {
		GPtrArray *array = atk_relation_get_target (relation);

		g_ptr_array_remove (array, target);
		g_ptr_array_add (array, target);
	} else {
		/* Relation hasn't been created yet */
		relation = atk_relation_new (&target, 1, type);

		atk_relation_set_add (set, relation);
		g_object_unref (relation);
	}
}

/**
 * marlin_add_paired_relations:
 * @target1:
 * @target1_type:
 * @target2:
 * @target2_type:
 *
 * Sets the relationships between @target1 and @target2 to
 * @target1_type and @target2_type respectively 
 */
void
marlin_add_paired_relations (GtkWidget *target1,
			     AtkRelationType target1_type,
			     GtkWidget *target2,
			     AtkRelationType target2_type)
{
	AtkObject *atk_target1, *atk_target2;
	AtkRelationSet *set1, *set2;

	atk_target1 = gtk_widget_get_accessible (target1);
	atk_target2 = gtk_widget_get_accessible (target2);

	set1 = atk_object_ref_relation_set (atk_target1);
	add_relation (set1, target1_type, atk_target2);

	set2 = atk_object_ref_relation_set (atk_target2);
	add_relation (set2, target2_type, atk_target1);
}

/**
 * marlin_widget_add_description:
 * @widget:
 * @description:
 * 
 * Sets the accessibility description on @widget to @description.
 */
void
marlin_widget_add_description (GtkWidget *widget,
			       const char *description)
{
	AtkObject *atk_widget;

	atk_widget = gtk_widget_get_accessible (widget);
	atk_object_set_description (atk_widget, description);
}
