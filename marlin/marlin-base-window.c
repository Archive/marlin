/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin/marlin-base-window.h>

static void
base_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (!initialized) {
		initialized = TRUE;
	}
}

GType
marlin_base_window_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MarlinBaseWindowClass), base_init, NULL
		};

		type = g_type_register_static (G_TYPE_INTERFACE,
					       "MarlinBaseWindow",
					       &info, 0);
		/* Only objects derived from GtkWindow are allowed to 
		   implement MarlinBaseWindow */
		g_type_interface_add_prerequisite (type, GTK_TYPE_WINDOW);
	}

	return type;
}

/**
 * marlin_base_window_get_sample:
 * @base:
 *
 * Obtains a pointer to the MarlinSample associated with @base.
 *
 * Returns: A MarlinSample
 */
MarlinSample *
marlin_base_window_get_sample (MarlinBaseWindow *base)
{
	MarlinBaseWindowClass *klass;

	g_return_val_if_fail (MARLIN_IS_BASE_WINDOW (base), NULL);

	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->get_sample) {
		return klass->get_sample (base);
	} else {
		g_warning ("get_sample is not implemented.");

		return NULL;
	}
}

/**
 * marlin_base_window_get_position:
 * @base:
 *
 * Obtains the current position of the cursor in @base.
 *
 * Returns: A guint64
 */
guint64
marlin_base_window_get_position (MarlinBaseWindow *base)
{
	MarlinBaseWindowClass *klass;

	g_return_val_if_fail (MARLIN_IS_BASE_WINDOW (base), 0);

	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->get_position) {
		return klass->get_position (base);
	} else {
		g_warning ("get_position is not implemented");

		return 0;
	}
}

/**
 * marlin_base_window_set_position:
 * @base:
 * @position:
 *
 * Sets the position of the cursor in @base to @position.
 */
void
marlin_base_window_set_position (MarlinBaseWindow *base,
				 guint64 position)
{
	MarlinBaseWindowClass *klass;

	g_return_if_fail (MARLIN_IS_BASE_WINDOW (base));

	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->set_position) {
		klass->set_position (base, position);
	} else {
		g_warning ("set_position is not implemented");
	}
}

/**
 * marlin_base_window_get_marker_view:
 * @base:
 *
 * Obtains the #MarlinMarkerView in @base.
 *
 * Returns: A #MarlinMarkerView.
 */
MarlinMarkerView *
marlin_base_window_get_marker_view (MarlinBaseWindow *base)
{
	MarlinBaseWindowClass *klass;

	g_return_val_if_fail (MARLIN_IS_BASE_WINDOW (base), NULL);

	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->get_marker_view) {
		return klass->get_marker_view (base);
	} else {
		g_warning ("get_marker_view is not implemented");
			   
		return NULL;
	}
}

/**
 * marlin_base_window_get_sample_view:
 * @base:
 *
 */
MarlinSampleView *
marlin_base_window_get_sample_view (MarlinBaseWindow *base)
{
	MarlinBaseWindowClass *klass;

	g_return_val_if_fail (MARLIN_IS_BASE_WINDOW (base), NULL);
	
	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->get_sample_view) {
		return klass->get_sample_view (base);
	} else {
		g_warning ("get_sample_view is not implemented");

		return NULL;
	}
}

/**
 * marlin_base_window_get_undo_manager:
 * @base:
 *
 */
MarlinUndoManager *
marlin_base_window_get_undo_manager (MarlinBaseWindow *base)
{
	MarlinBaseWindowClass *klass;

	g_return_val_if_fail (MARLIN_IS_BASE_WINDOW (base), NULL);

	klass = MARLIN_BASE_WINDOW_GET_CLASS (base);
	if (klass->get_undo_manager) {
		return klass->get_undo_manager (base);
	} else {
		g_warning ("get_undo_manager is not implemented");

		return NULL;
	}
}
