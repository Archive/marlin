/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright (C) 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-types.h>

enum {
	DISPLAY_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_RATE,
	PROP_ADJUSTMENT,
	PROP_DISPLAY
};

struct _MarlinPositionSpinnerPrivate {
	GtkAdjustment *adj;

	guint rate;
	MarlinDisplay display;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_POSITION_SPINNER_TYPE, MarlinPositionSpinnerPrivate))
static void editable_init (GtkEditableClass *iface);
G_DEFINE_TYPE_EXTENDED (MarlinPositionSpinner, marlin_position_spinner,
			GTK_TYPE_SPIN_BUTTON, 0,
			G_IMPLEMENT_INTERFACE (GTK_TYPE_EDITABLE,
					       editable_init));
static guint signals[LAST_SIGNAL];

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (marlin_position_spinner_parent_class)->finalize (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (object);
	MarlinPositionSpinnerPrivate *priv = spinner->priv;
	guint64 max;

	switch (prop_id) {
	case PROP_SAMPLE:
		max = g_value_get_uint64 (value);

		gtk_adjustment_clamp_page (priv->adj, 0.0, (float) max);
		priv->adj->upper = (double) max;
		break;

	case PROP_RATE:
		priv->rate = g_value_get_uint (value);
		priv->adj->step_increment = (double) priv->rate / 10;
		priv->adj->page_increment = (double) priv->rate;;

		break;

	case PROP_DISPLAY:
		priv->display = g_value_get_enum (value);

		switch (priv->display) {
		case MARLIN_DISPLAY_FRAMES:
			priv->adj->step_increment = 1.0;
			priv->adj->page_increment = 10.0;
			break;

		case MARLIN_DISPLAY_TIME_LONG:
			priv->adj->step_increment = (double) priv->rate / 10;
			priv->adj->page_increment = priv->rate;
			break;

		case MARLIN_DISPLAY_SECONDS:
			priv->adj->step_increment = (double) priv->rate;
			priv->adj->page_increment = (double) priv->rate * 10;
			break;

		case MARLIN_DISPLAY_BEATS:
			break;

		default:
			break;
		}

		g_signal_emit (G_OBJECT (spinner), signals[DISPLAY_CHANGED], 0);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (object);
	MarlinPositionSpinnerPrivate *priv = spinner->priv;

	switch (prop_id) {
	case PROP_ADJUSTMENT:
		g_value_set_object (value, priv->adj);
		break;

	case PROP_RATE:
		g_value_set_uint (value, priv->rate);
		break;

	case PROP_DISPLAY:
		g_value_set_enum (value, priv->display);
		break;

	default:
		break;
	}
}

static void
display_toggled (GtkCheckMenuItem      *item,
		 MarlinPositionSpinner *spinner)
{
	MarlinDisplay pd;

	if (gtk_check_menu_item_get_active (item) == FALSE) {
		return;
	}

	pd = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "display"));
	g_object_set (G_OBJECT (spinner),
		      "display_as", pd,
		      NULL);
}

static char *names[MARLIN_DISPLAY_NULL] = {
	N_("_Frames"),
	N_("_Time"),
	N_("_Seconds"),
	N_("T_ime & Frames"),
	N_("_Beats"),
};

static void
populate_popup (GtkEntry *entry,
		GtkMenu  *menu)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (entry);
	int pd;
	GSList *group = NULL;

	for (pd = MARLIN_DISPLAY_FRAMES; pd < MARLIN_DISPLAY_NULL; pd++) {
		GtkWidget *item;

		item = gtk_radio_menu_item_new_with_mnemonic (group, _(names[pd]));
		g_object_set_data (G_OBJECT (item), "display", GINT_TO_POINTER (pd));
		if (pd == spinner->priv->display) {
			gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), TRUE);
		}
		g_signal_connect (G_OBJECT (item), "toggled",
				  G_CALLBACK (display_toggled), entry);

		group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (item));
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}
}

static gint
spin_output (GtkSpinButton *spin)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (spin);
	MarlinPositionSpinnerPrivate *priv = spinner->priv;
	char *buf;
	guint64 ms;

	switch (priv->display) {
	case MARLIN_DISPLAY_FRAMES:
		buf = g_strdup_printf ("%llu", (guint64) spin->adjustment->value);
		break;

	case MARLIN_DISPLAY_TIME_LONG:
		ms = marlin_frames_to_ms ((guint64) spin->adjustment->value,
					  priv->rate);
		buf = marlin_ms_to_time_string (ms);
		break;

	case MARLIN_DISPLAY_SECONDS:
		ms = marlin_frames_to_ms ((guint64) spin->adjustment->value,
					  priv->rate);
		buf = g_strdup_printf ("%llu", ms / 1000);
		break;

	case MARLIN_DISPLAY_TIME_FRAMES:
		ms = marlin_frames_to_ms ((guint64) spin->adjustment->value,
					  priv->rate);
		buf = marlin_ms_to_time_frame_string (ms, priv->rate);
		break;

	case MARLIN_DISPLAY_BEATS:
		buf = g_strdup ("1");
		break;

	default:
		buf = g_strdup ("FIXME");
		break;
	}

	if (strcmp (buf, gtk_entry_get_text (GTK_ENTRY (spin))) != 0) {
		gtk_entry_set_text (GTK_ENTRY (spin), buf);
	}
	g_free (buf);

	return TRUE;
}

static int
spin_input (GtkSpinButton *spin_button,
	    double        *new_val)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (spin_button);
	MarlinPositionSpinnerPrivate *priv = spinner->priv;
	guint64 ms;
	char *err = NULL;
	const char *text = gtk_entry_get_text (GTK_ENTRY (spinner));

	switch (priv->display) {
	case MARLIN_DISPLAY_FRAMES:
		*new_val = strtod (text, &err);
		if (*err) {
			return GTK_INPUT_ERROR;
		}

		break;

	case MARLIN_DISPLAY_TIME_LONG:
		ms = marlin_time_string_to_ms (text);

		*new_val = (((double) ms + 0.5) / 1000) * priv->rate;
		break;

	case MARLIN_DISPLAY_SECONDS:
		*new_val = strtod (text, &err);
		if (*err) {
			return GTK_INPUT_ERROR;
		}

		/* Convert seconds into frames */
		*new_val *= priv->rate;
		break;

	case MARLIN_DISPLAY_TIME_FRAMES:
		ms = marlin_time_frame_string_to_ms (text);

		*new_val = (((double) ms + 0.5) / 1000) * priv->rate;
		break;

	case MARLIN_DISPLAY_BEATS:
		*new_val = 4;
		break;

	default:
		return GTK_INPUT_ERROR;
	}

	/* Return true when this has parsed */
	return TRUE;
}

static void
marlin_position_spinner_class_init (MarlinPositionSpinnerClass *klass)
{
	GObjectClass *object_class;
	GtkEntryClass *entry_class;
	GtkSpinButtonClass *spin_class;

	object_class = G_OBJECT_CLASS (klass);
	entry_class = GTK_ENTRY_CLASS (klass);
	spin_class = GTK_SPIN_BUTTON_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	entry_class->populate_popup = populate_popup;

	spin_class->input = spin_input;
	spin_class->output = spin_output;

	g_type_class_add_private (object_class, sizeof (MarlinPositionSpinnerPrivate));

	g_object_class_install_property (object_class,
					 PROP_SAMPLE,
					 g_param_spec_uint64 ("max_frames",
							      "", "",
							      0, G_MAXUINT64, 0,
							      G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
					 PROP_RATE,
					 g_param_spec_uint ("rate",
							    "", "",
							    0, G_MAXUINT, 48000,
							    G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_ADJUSTMENT,
					 g_param_spec_object ("adjustment",
							      "", "",
							      GTK_TYPE_ADJUSTMENT,
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class,
					 PROP_DISPLAY,
					 g_param_spec_enum ("display_as",
							    "", "",
							    MARLIN_TYPE_DISPLAY,
							    MARLIN_DISPLAY_FRAMES,
							    G_PARAM_READWRITE));
	signals[DISPLAY_CHANGED] = g_signal_new ("display-changed",
						 G_TYPE_FROM_CLASS (klass),
						 G_SIGNAL_RUN_FIRST |
						 G_SIGNAL_NO_RECURSE,
						 G_STRUCT_OFFSET (MarlinPositionSpinnerClass, display_changed),
						 NULL, NULL,
						 g_cclosure_marshal_VOID__VOID,
						 G_TYPE_NONE, 0);
}

static void
marlin_position_spinner_init (MarlinPositionSpinner *spinner)
{
	MarlinPositionSpinnerPrivate *priv;

	priv = spinner->priv = GET_PRIVATE (spinner);
	priv->adj = GTK_ADJUSTMENT (gtk_adjustment_new
				    (0.0, 0.0, (float) G_MAXUINT64,
				     1.0, 10.0, 1.0));
	gtk_spin_button_set_adjustment (GTK_SPIN_BUTTON (spinner), priv->adj);
	priv->display = MARLIN_DISPLAY_TIME_LONG;
}

static void
insert_text (GtkEditable *editable,
	     const char *text,
	     int length,
	     int *pos)
{
	MarlinPositionSpinner *spinner = MARLIN_POSITION_SPINNER (editable);
	MarlinPositionSpinnerPrivate *priv = spinner->priv;
	int i = 0;
	gboolean string_ok = TRUE;
	GtkEditableClass *parent_iface = g_type_interface_peek
		(marlin_position_spinner_parent_class, GTK_TYPE_EDITABLE);

	while ((i < length) && (string_ok)) {
		if (g_ascii_isdigit (text[i])) {
			++i;
		} else if (text[i] == ':' || text[i] == '.') {
			if (priv->display == MARLIN_DISPLAY_TIME_LONG ||
			    priv->display == MARLIN_DISPLAY_TIME_FRAMES) {
				string_ok = TRUE;
				++i;
			} else {
				string_ok = FALSE;
			}
		} else {
			string_ok = FALSE;
		}
	}

	if (string_ok) {
		parent_iface->insert_text (editable, text, length, pos);
	} else {
		g_print ("Failed - %s - %c\n", text, text[i]);
		return;
	}
}

static void
editable_init (GtkEditableClass *iface)
{
	iface->insert_text = insert_text;
}

GtkWidget *
marlin_position_spinner_new (void)
{
	return g_object_new (MARLIN_POSITION_SPINNER_TYPE, NULL);
}

static const char *
get_display_name (MarlinDisplay display)
{
	char *text;

	switch (display) {
	case MARLIN_DISPLAY_FRAMES:
		text = _("frames");
		break;

	case MARLIN_DISPLAY_TIME_LONG:
		text = _("hrs:mins:secs");
		break;

	case MARLIN_DISPLAY_SECONDS:
		text = _("seconds");
		break;

	case MARLIN_DISPLAY_TIME_FRAMES:
		text = _("secs:frames");
		break;

	case MARLIN_DISPLAY_BEATS:
		text = _("beats");
		break;

	default:
		g_assert_not_reached ();
	}

	return text;
}

static void
display_changed (MarlinPositionSpinner *spinner,
		 GtkLabel *label)
{
	MarlinPositionSpinnerPrivate *priv = spinner->priv;

	const char *text = get_display_name (priv->display);
	gtk_label_set_text (label, text);
}

GtkWidget *
marlin_position_spinner_label (MarlinPositionSpinner *spinner)
{
	MarlinPositionSpinnerPrivate *priv = spinner->priv;
	GtkWidget *label;
	const char *text;

	text = get_display_name (priv->display);
	label = gtk_label_new (text);

	g_signal_connect (G_OBJECT (spinner), "display-changed",
			  G_CALLBACK (display_changed), label);

	return label;
}
