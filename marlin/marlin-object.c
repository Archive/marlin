/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <marlin/marlin-object.h>
#include <marlin/marlin-mt.h>

enum {
	NOTIFY,
	LAST_SIGNAL
};

#define MARLIN_OBJECT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_OBJECT_TYPE, MarlinObjectPrivate))

struct _MarlinObjectPrivate {
	gboolean frozen;

	GList *queue;
};

static guint32 signals[LAST_SIGNAL] = {0};

/* This is a wrapper object so that we can have our own notifies
   which go to the main GUI thread */
static void
class_init (MarlinObjectClass *klass)
{
	g_type_class_add_private (klass, sizeof (MarlinObjectPrivate));

	signals[NOTIFY] = g_signal_new ("safe-notify",
					G_TYPE_FROM_CLASS (klass),
					G_SIGNAL_RUN_FIRST |
					G_SIGNAL_NO_RECURSE,
					G_STRUCT_OFFSET (MarlinObjectClass, safe_notify),
					NULL, NULL,
					g_cclosure_marshal_VOID__STRING,
					G_TYPE_NONE,
					1, G_TYPE_STRING);
}

static void
init (MarlinObject *object)
{
	MarlinObjectPrivate *priv;

	priv = MARLIN_OBJECT_GET_PRIVATE (object);

	priv->frozen = FALSE;
	priv->queue = NULL;
}

GType
marlin_object_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (MarlinObjectClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinObject), 0, (GInstanceInitFunc) init, NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "MarlinObject",
					       &info, G_TYPE_FLAG_ABSTRACT);
	}

	return type;
}

/* Notify an object on the GUI thread */
struct _notify_msg {
	MarlinMTMsg msg;

	GObject *object;
	char *name;
};

/* Notify */
static void
do_set_notify (MarlinMTMsg *mm)
{
	struct _notify_msg *notify = (struct _notify_msg *) mm;

	g_signal_emit (notify->object, signals[NOTIFY], 0, notify->name);
}

static void
do_free_notify (MarlinMTMsg *mm)
{
	struct _notify_msg *notify = (struct _notify_msg *) mm;

	g_free (notify->name);
	g_object_unref (notify->object);
}

static MarlinMTMsgOp notify_op = {
	NULL,
	do_set_notify,
	NULL,
	do_free_notify,
};

void
marlin_object_notify (GObject *object,
		      const char *name)
{
	struct _notify_msg *m;
	MarlinObjectPrivate *priv = MARLIN_OBJECT_GET_PRIVATE (object);

	m = marlin_mt_msg_new (&notify_op, NULL, sizeof (*m));
	m->object = object;
	m->name = g_strdup (name);
	g_object_ref (object);
	
	if (priv->frozen == FALSE) {
		marlin_msgport_put (marlin_gui_port, (MarlinMsg *) m);
	} else {
		priv->queue = g_list_append (priv->queue, m);
	}
}

void
marlin_object_freeze (MarlinObject *object)
{
	MarlinObjectPrivate *priv = MARLIN_OBJECT_GET_PRIVATE (object);

	priv->frozen = TRUE;
	priv->queue = NULL;
}

void
marlin_object_thaw (MarlinObject *object)
{
	MarlinObjectPrivate *priv = MARLIN_OBJECT_GET_PRIVATE (object);
	GList *p;

	priv->frozen = FALSE;

	for (p = priv->queue; p; p = p->next) {
		marlin_msgport_put (marlin_gui_port, (MarlinMsg *) p->data);
	}

	g_list_free (priv->queue);
	priv->queue = NULL;
}
