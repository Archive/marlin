/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PLUGIN_H__
#define __MARLIN_PLUGIN_H__

#include <gmodule.h>

#include <gtk/gtk.h>

#include <marlin/marlin-base-window.h>

/* Update this whenever the plugin interface changes
   so that the plugin loader can skip plugins that were compiled for
   a different interface */
#define MARLIN_PLUGIN_VERSION 6

#define MARLIN_PLUGIN_ERROR marlin_plugin_error_quark ()

typedef struct _MarlinPluginFuncs {
	void (* merge_ui) (GtkUIManager *ui_manager,
			   MarlinBaseWindow *window);
	void (* set_sensitive) (GtkUIManager *ui,
				MarlinBaseWindow *window,
				gboolean sensitive);
	void (* set_name) (GtkUIManager *ui,
			   MarlinBaseWindow *window);
} MarlinPluginFuncs;

typedef struct _MarlinPluginInfo {
	GModule *module;

	char *name; /* The name of the plugin */
	char *author; /* Author of the plugin */
	char *description; /* Description of the plugin */
	char *version; /* Version string */
	char *copyright; /* Copyright message */
	char *icon; /* Full pathname to the icon for this plugin */

	MarlinPluginFuncs *funcs;
} MarlinPluginInfo;

typedef enum {
	MARLIN_PLUGIN_NOT_SUPPORTED, /* Plugins are not supported
					on this system */
	MARLIN_PLUGIN_IO_ERROR, /* Could not load the plugin */
	MARLIN_PLUGIN_WRONG_VERSION, /* Plugin was compiled for a different
					interface version */
	MARLIN_PLUGIN_INVALID, /* No registration function */
	MARLIN_PLUGIN_INIT_FAILURE, /* Failed to init */
} MarlinPluginError;

gboolean marlin_plugin_initialise (GError **error);
GList *marlin_plugin_get_list (void);
#endif
