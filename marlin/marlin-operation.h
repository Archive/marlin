/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_OPERATION_H__
#define __MARLIN_OPERATION_H__

#include <glib-object.h>

#define MARLIN_OPERATION_TYPE (marlin_operation_get_type ())
#define MARLIN_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_OPERATION_TYPE, MarlinOperation))
#define MARLIN_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((obj), MARLIN_OPERATION_TYPE, MarlinOperationClass))
#define IS_MARLIN_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_OPERATION_TYPE))
#define IS_MARLIN_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_OPERATION_TYPE))

typedef struct _MarlinOperation MarlinOperation;
typedef struct _MarlinOperationPrivate MarlinOperationPrivate;
typedef struct _MarlinOperationClass MarlinOperationClass;

struct _MarlinOperation {
	GObject object;

	MarlinOperationPrivate *priv;
};

struct _MarlinOperationClass {
	GObjectClass parent_class;

	void (* started) (MarlinOperation *operation);
	void (* finished) (MarlinOperation *operation);
	void (* progress) (MarlinOperation *operation,
			   int percentage);
	void (* cancelled) (MarlinOperation *operation);
	void (* error) (MarlinOperation *pipeline,
			GError *error,
			const char *error_string);
	void (* paused) (MarlinOperation *operation,
			 gboolean paused);
};

GType marlin_operation_get_type (void);
MarlinOperation *marlin_operation_new (void);

void marlin_operation_do (MarlinOperation *operation,
			  gboolean complete);
void marlin_operation_start (MarlinOperation *operation);
void marlin_operation_finish (MarlinOperation *operation);
void marlin_operation_progress (MarlinOperation *operation,
				int progress);
void marlin_operation_cancel (MarlinOperation *operation);
void marlin_operation_pause (MarlinOperation *operation,
			     gboolean paused);

void marlin_operation_set_error (MarlinOperation *operation,
				 GError *error,
				 const char *debug);
#endif
