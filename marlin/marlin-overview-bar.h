/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_OVERVIEW_BAR_H__
#define __MARLIN_OVERVIEW_BAR_H__

#include <gtk/gtk.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-sample-selection.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_OVERVIEW_BAR_TYPE (marlin_overview_bar_get_type ())
#define MARLIN_OVERVIEW_BAR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_OVERVIEW_BAR_TYPE, MarlinOverviewBar))
#define MARLIN_OVERVIEW_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_OVERVIEW_BAR_TYPE, MarlinOverviewBarClass))
#define IS_MARLIN_OVERVIEW_BAR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_OVERVIEW_BAR_TYPE))
#define IS_MARLIN_OVERVIEW_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_OVERVIEW_BAR_TYPE))

typedef struct _MarlinOverviewBar MarlinOverviewBar;
typedef struct _MarlinOverviewBarClass MarlinOverviewBarClass;
typedef struct _MarlinOverviewBarPrivate MarlinOverviewBarPrivate;

struct _MarlinOverviewBar {
	GtkDrawingArea parent;

	MarlinOverviewBarPrivate *priv;
};

struct _MarlinOverviewBarClass {
	GtkDrawingAreaClass parent_class;

	void (*cursor_changed) (MarlinOverviewBar *timeline,
				guint64 position);
	void (*page_start_changed) (MarlinOverviewBar *timeline,
				    guint64 start);
	void (*play_request) (MarlinOverviewBar *overview_bar,
			      guint64 start);
	void (*move_page) (MarlinOverviewBar *bar,
			   GtkMovementStep step,
			   int count,
			   gboolean extend_selection);
};

GType marlin_overview_bar_get_type (void);
GtkWidget *marlin_overview_bar_new (MarlinSample *sample,
				    guint64 samples_per_page);

#ifdef __cplusplus
}
#endif

#endif
