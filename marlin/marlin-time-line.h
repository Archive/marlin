/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_TIME_LINE_H__
#define __MARLIN_TIME_LINE_H__

#include <gtk/gtkdrawingarea.h>

#include <marlin/marlin-sample-selection.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define MARLIN_TIME_LINE_TYPE (marlin_time_line_get_type ())
#define MARLIN_TIME_LINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_TIME_LINE_TYPE, MarlinTimeLine))
#define MARLIN_TIME_LINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_TIME_LINE_TYPE, MarlinTimeLineClass))
#define IS_MARLIN_TIME_LINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_TIME_LINE_TYPE))
#define IS_MARLIN_TIME_LINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_TIME_LINE_TYPE))

typedef struct _MarlinTimeLine MarlinTimeLine;
typedef struct _MarlinTimeLineClass MarlinTimeLineClass;
typedef struct _MarlinTimeLinePrivate MarlinTimeLinePrivate;

struct _MarlinTimeLine {
	GtkDrawingArea parent;

	MarlinTimeLinePrivate *priv;
};

struct _MarlinTimeLineClass {
	GtkDrawingAreaClass parent_class;

	void (*cursor_changed) (MarlinTimeLine *timeline,
				guint64 position);
	void (*page_start_changed) (MarlinTimeLine *timeline,
				    guint64 start);
};

GType marlin_time_line_get_type (void);
GtkWidget *marlin_time_line_new (guint64 total_samples,
				 guint64 samples_per_page);

#ifdef __cplusplus
}
#endif

#endif
