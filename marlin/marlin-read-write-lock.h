/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_READ_WRITE_LOCK_H__
#define __MARLIN_READ_WRITE_LOCK_H__

/* Lock debugging stuff - define LOCK_DEBUG before including this header */
#ifdef LOCK_DEBUG

# define WRITE_LOCK(x) {g_print ("Write locking %p - %s(%d)\n", x, __FUNCTION__, __LINE__); marlin_read_write_lock_lock (x, MARLIN_READ_WRITE_LOCK_MODE_WRITE);}
# define WRITE_UNLOCK(x) {g_print ("Write unlocking %p - %s(%d)\n", x, __FUNCTION__, __LINE__); marlin_read_write_lock_unlock (x, MARLIN_READ_WRITE_LOCK_MODE_WRITE);}

# define READ_LOCK(x) {g_print ("Read locking %p - %s(%d)\n", x, __FUNCTION__, __LINE__); marlin_read_write_lock_lock (x, MARLIN_READ_WRITE_LOCK_MODE_READ);}
# define READ_UNLOCK(x) {g_print ("Read unlocking %p - %s(%d)\n", x, __FUNCTION__, __LINE__); marlin_read_write_lock_unlock (x, MARLIN_READ_WRITE_LOCK_MODE_READ);}

#else /* LOCK_DEBUG */

# define WRITE_LOCK(x) {marlin_read_write_lock_lock (x, MARLIN_READ_WRITE_LOCK_MODE_WRITE);}
# define WRITE_UNLOCK(x) {marlin_read_write_lock_unlock (x, MARLIN_READ_WRITE_LOCK_MODE_WRITE);}

# define READ_LOCK(x) {marlin_read_write_lock_lock (x, MARLIN_READ_WRITE_LOCK_MODE_READ);}
# define READ_UNLOCK(x) {marlin_read_write_lock_unlock (x, MARLIN_READ_WRITE_LOCK_MODE_READ);}

#endif /* LOCK_DEBUG */

typedef struct _MarlinReadWriteLock MarlinReadWriteLock;

typedef enum _MarlinReadWriteLockMode {
	MARLIN_READ_WRITE_LOCK_MODE_READ,
	MARLIN_READ_WRITE_LOCK_MODE_WRITE
} MarlinReadWriteLockMode;

MarlinReadWriteLock *marlin_read_write_lock_new (void);
int marlin_read_write_lock_destroy (MarlinReadWriteLock *rwl);
void marlin_read_write_lock_lock (MarlinReadWriteLock *rwl,
				  MarlinReadWriteLockMode mode);
void marlin_read_write_lock_unlock (MarlinReadWriteLock *rwl,
				    MarlinReadWriteLockMode mode);

#endif
