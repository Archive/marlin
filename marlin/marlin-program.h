/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_PROGRAM_H__
#define __MARLIN_PROGRAM_H__

#include <glib-object.h>

#include <marlin/marlin-jack.h>
#include <marlin/marlin-sample.h>

#define MARLIN_PROGRAM_TYPE (marlin_program_get_type ())
#define MARLIN_PROGRAM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_PROGRAM_TYPE, MarlinProgram))
#define MARLIN_PROGRAM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_PROGRAM_TYPE, MarlinProgramClass))

#define MARLIN_PROGRAM_JACK_UNOWNED 0

typedef struct _MarlinProgramPrivate MarlinProgramPrivate;
typedef struct _MarlinProgram {
	GObject parent;

	MarlinProgramPrivate *priv;
} MarlinProgram;

typedef struct _MarlinProgramClass {
	GObjectClass parent_class;

	void (*new_view) (MarlinProgram *program,
			  MarlinSample *sample);
	void (*clipboard_changed) (MarlinProgram *program);
	void (*jack_owner_changed) (MarlinProgram *program,
				    int            owner_id);
} MarlinProgramClass;

GType marlin_program_get_type (void);
MarlinProgram *marlin_program_get_default (void);
void marlin_program_destroy_default (void);
MarlinSample *marlin_program_get_clipboard (MarlinProgram *program);
void marlin_program_set_clipboard (MarlinProgram *program,
				   MarlinSample *clipboard_object,
				   const char *src);
const char *marlin_program_get_clip_source (MarlinProgram *program);

void marlin_program_play_clipboard (MarlinProgram *program);
void marlin_program_stop_clipboard (MarlinProgram *program);
void marlin_program_request_new_view (MarlinProgram *program,
				      MarlinSample *sample);

guint marlin_program_request_jack_owner_id (MarlinProgram *program);
void marlin_program_acquire_jack (MarlinProgram *program,
				  guint          owner_id,
				  gboolean       locked);
void marlin_program_release_jack (MarlinProgram *program,
				  guint          owner_id);

guint marlin_program_get_jack_owner (MarlinProgram *program);

MarlinJack *marlin_program_get_player (MarlinProgram *program);
MarlinJack *marlin_program_get_recorder (MarlinProgram *program);

#endif
