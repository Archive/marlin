/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include <gst/gst.h>

#include <marlin/marlin-play-pipeline.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample.h>

#define MARLIN_PLAY_PIPELINE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_PLAY_PIPELINE_TYPE, MarlinPlayPipelinePrivate))

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_POSITION
};

enum {
	LEVEL,
	EOS,
	POSITION,
	LAST_SIGNAL
};

struct _MarlinPlayPipelinePrivate {
	MarlinSample *sample;

	GstElement *sink, *audioconvert, *joiner, *level;

	GPtrArray *srcs; /* Array of MarlinChannelSrc */
};

static GObjectClass *parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

static void
finalize (GObject *object)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;

	pipeline = MARLIN_PLAY_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->srcs) {
		g_ptr_array_free (priv->srcs, TRUE);
	}

	parent_class->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;

	pipeline = MARLIN_PLAY_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->sample != NULL) {
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}
	
 	parent_class->dispose (object);
}

static gboolean
connect_pipeline (MarlinPlayPipeline *pipeline,
		  GError            **error)
{
	MarlinPlayPipelinePrivate *priv;
	GstCaps *filtercaps;
	guint channels;
	int i;

	priv = pipeline->priv;

	g_object_get (priv->sample,
		      "channels", &channels,
		      NULL);

	g_print ("Connecting %d channels\n", channels);
	if (priv->srcs) {
		for (i = 0; i < priv->srcs->len; i++) {
			GstElement *src = priv->srcs->pdata[i];
			
			gst_element_unlink (src, priv->joiner);
			gst_bin_remove (GST_BIN (pipeline), src);
		}
		g_ptr_array_free (priv->srcs, TRUE);
	}

	priv->srcs = g_ptr_array_sized_new (channels);
	
	for (i = 0; i < channels; i++) {
		MarlinChannel *channel;
		GstElement *src;
		GstPad *srcpad, *sinkpad;
		char *name;

		name = g_strdup_printf ("src-%d", i);
		src = gst_element_factory_make ("marlin-channel-src", name);
		g_free (name);

		channel = marlin_sample_get_channel (priv->sample, i);
		
		g_object_set (G_OBJECT (src),
			      "sample", priv->sample,
			      "channel", channel,
			      NULL);

		gst_bin_add (GST_BIN (pipeline), src);

		sinkpad = gst_element_get_request_pad (priv->joiner, "sink%d");
		srcpad = gst_element_get_static_pad (src, "src");

		if (gst_pad_link (srcpad, sinkpad) != GST_PAD_LINK_OK) {
			gst_bin_remove (GST_BIN (pipeline), src);
			gst_element_release_request_pad (priv->joiner, sinkpad);

			g_warning ("Link failed src joiner");
			/* Set error */
			return FALSE;
		} 

		g_ptr_array_add (priv->srcs, src);
	}

	/* srcs are now linked, link the rest of the pipeline */
	if (gst_element_link (priv->joiner, priv->audioconvert) == FALSE) {
		/* Set error */

		g_warning ("joiner audioconcert failed");
		return FALSE;
	}

	filtercaps = gst_caps_new_simple ("audio/x-raw-int", NULL);
	if (gst_element_link_filtered (priv->audioconvert, priv->level,
				       filtercaps) == FALSE) {
		gst_caps_unref (filtercaps);

		g_warning ("audioconvert level failed");
		/* Set error */
		return FALSE;
	}
	gst_caps_unref (filtercaps);

	if (gst_element_link (priv->level, priv->sink) == FALSE) {
		/* Set error */
		g_warning ("level sink failed");
		return FALSE;
	}

	return TRUE;
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;
	GError *error = NULL;
	
	pipeline = MARLIN_PLAY_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_POSITION:
		break;

	case PROP_SAMPLE:
		if (priv->sample) {
			g_object_unref (priv->sample);
		}

		priv->sample = MARLIN_SAMPLE (g_value_dup_object (value));

		if (connect_pipeline (pipeline, &error) == FALSE) {
			/* Error */
		}
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;

	pipeline = MARLIN_PLAY_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_POSITION:
		break;

	case PROP_SAMPLE:
		g_value_set_object (value, priv->sample);
		break;

	default:
		break;
	}
}

static guint64
report_position (MarlinPipeline *marlin_pipeline)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;
	GstElement *element;
	GstFormat fmt;
	gint64 pos;
	
	pipeline = (MarlinPlayPipeline *) marlin_pipeline;
	priv = pipeline->priv;

	element = (GstElement *) pipeline;

	fmt = GST_FORMAT_BYTES;
	if (gst_element_query_position (element, &fmt, &pos)) {
		g_signal_emit (pipeline, signals[POSITION], 0, 
			       pos / sizeof (float));

		return pos;
	}

	return 0;

}

static void
class_init (GObjectClass *klass)
{
	MarlinPipelineClass *p_class;

	p_class = (MarlinPipelineClass *) klass;

	klass->finalize = finalize;
	klass->dispose = dispose;
	klass->set_property = set_property;
	klass->get_property = get_property;

	p_class->report_position = report_position;

	parent_class = g_type_class_peek_parent (klass);
	g_type_class_add_private (klass, sizeof (MarlinPlayPipelinePrivate));
	/* Properties */
	g_object_class_install_property (klass,
					 PROP_POSITION,
					 g_param_spec_ulong ("position",
							     "Position",
							     "Position of playback",
							     0, G_MAXULONG, 0,
							     G_PARAM_READWRITE));
	g_object_class_install_property (klass,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "Sample",
							      "The sample to be played",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	
	signals[LEVEL] = g_signal_new ("level",
				       G_TYPE_FROM_CLASS (klass),
				       G_SIGNAL_RUN_LAST,
				       G_STRUCT_OFFSET (MarlinPlayPipelineClass, level),
				       NULL, NULL,
				       marlin_marshal_VOID__DOUBLE_INT_POINTER_POINTER_POINTER,
				       G_TYPE_NONE, 5,
				       G_TYPE_DOUBLE, 
				       G_TYPE_INT,
				       G_TYPE_POINTER,
				       G_TYPE_POINTER,
				       G_TYPE_POINTER);

	signals[EOS] = g_signal_new ("eos",
				     G_TYPE_FROM_CLASS (klass),
				     G_SIGNAL_RUN_LAST |
				     G_SIGNAL_NO_RECURSE,
				     G_STRUCT_OFFSET (MarlinPlayPipelineClass, eos),
				     NULL, NULL,
				     g_cclosure_marshal_VOID__VOID,
				     G_TYPE_NONE,
				     0);
	
	signals[POSITION] = g_signal_new ("position",
					  G_TYPE_FROM_CLASS (klass),
					  G_SIGNAL_RUN_LAST |
					  G_SIGNAL_NO_RECURSE,
					  G_STRUCT_OFFSET (MarlinPlayPipelineClass, position),
					  NULL, NULL,
					  marlin_marshal_VOID__UINT64,
					  G_TYPE_NONE,
					  1, G_TYPE_UINT64);
}

static gboolean
bus_message_cb (GstBus     *bus,
		GstMessage *message,
		gpointer    data)
{
	MarlinPlayPipeline *pipeline;
	MarlinPlayPipelinePrivate *priv;
	const GstStructure *s;

	pipeline = (MarlinPlayPipeline *) data;
	priv = pipeline->priv;

	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_EOS:
		if (GST_MESSAGE_SRC (message) == (GstObject *) pipeline) {
			g_signal_emit (pipeline, signals[EOS], 0);
		}
		break;

	case GST_MESSAGE_ELEMENT: 
		s = gst_message_get_structure (message);
		if (strcmp (gst_structure_get_name (s), "level") == 0) {
			int channels, i;
			double *rms, *peak, *decay;
			const GValue *r, *p, *d;
			const GValue *value;

			/* Making this faster would be great */
			r = gst_structure_get_value (s, "rms");
			p = gst_structure_get_value (s, "peak");
			d = gst_structure_get_value (s, "decay");

			channels = gst_value_list_get_size (r);

			rms = g_newa (double, channels);
			peak = g_newa (double, channels);
			decay = g_newa (double, channels);

			for (i = 0; i < channels; i++) {
				value = gst_value_list_get_value (r, i);
				rms[i] = g_value_get_double (value);

				value = gst_value_list_get_value (p, i);
				peak[i] = g_value_get_double (value);

				value = gst_value_list_get_value (d, i);
				decay[i] = g_value_get_double (value);
			}

			g_signal_emit (pipeline, signals[LEVEL], 0,
				       0.0, channels, rms, peak, decay);
		}
		break;

	default:
		break;
	}

	return TRUE;
}

static void
init (MarlinPlayPipeline *pipeline)
{
	MarlinPlayPipelinePrivate *priv;
	GstBus *bus;

	pipeline->priv = MARLIN_PLAY_PIPELINE_GET_PRIVATE (pipeline);
	priv = pipeline->priv;

	priv->joiner = gst_element_factory_make ("marlin-channel-joiner",
						 "joiner");
	priv->audioconvert = gst_element_factory_make ("audioconvert", 
						       "convert");
	priv->level = gst_element_factory_make ("level", "level");
	g_object_set (G_OBJECT (priv->level),
		      "message", TRUE,
		      NULL);
	priv->sink = gst_element_factory_make ("autoaudiosink", "sink");

	gst_bin_add_many (GST_BIN (pipeline), priv->joiner, priv->audioconvert, 
			  priv->level, priv->sink, NULL);
			  
	/* Connect the pipeline when the sample is set */
	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	gst_bus_add_signal_watch (bus);
	g_signal_connect (bus, "message",
			  G_CALLBACK (bus_message_cb), pipeline);
}

GType
marlin_play_pipeline_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static GTypeInfo info = {
			sizeof (MarlinPlayPipelineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinPlayPipeline), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_PIPELINE_TYPE,
					       "MarlinPlayPipeline",
					       &info, 0);
	}

	return type;
}

/**
 * marlin_play_pipeline_new:
 *
 */
MarlinPlayPipeline *
marlin_play_pipeline_new (void)
{
	return g_object_new (MARLIN_PLAY_PIPELINE_TYPE, NULL);
}

/**
 * marlin_play_pipeline_new_from_sample:
 *
 */
MarlinPlayPipeline *
marlin_play_pipeline_new_from_sample (MarlinSample *sample)
{
	return g_object_new (MARLIN_PLAY_PIPELINE_TYPE,
			     "sample", sample,
			     NULL);
}

/**
 * marlin_play_pipeline_seek:
 * @pipeline: The pipeline.
 * @position: Seek position.
 *
 * Seeks in the sample stream to @position
 */
void
marlin_play_pipeline_seek (MarlinPlayPipeline *pipeline,
			   guint64 position)
{
}

/**
 * marlin_play_pipeline_seek_range:
 * @pipeline: The pipeline.
 * @start: Start of the range
 * @end: End of the range
 *
 * Sets the range of playback to be between @start and @end.
 */
void
marlin_play_pipeline_seek_range (MarlinPlayPipeline *pipeline,
				 guint64 start,
				 guint64 end)
{
}

/**
 * marlin_play_pipeline_set_repeat:
 * @pipeline: The pipeline.
 * @repeat: Should the pipeline loop constantly?
 *
 * Toggles looping on @pipeline
 */
void
marlin_play_pipeline_set_repeat (MarlinPlayPipeline *pipeline,
				 gboolean repeat)
{
}
