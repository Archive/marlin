/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_CURSORS_H__
#define __MARLIN_CURSORS_H__

#include <gtk/gtk.h>

typedef enum {
	I_BEAM,
	I_BEAM_LEFT,
	I_BEAM_RIGHT,
	LEFT_RIGHT_ARROW,
	I_BEAM_ADD,
	I_BEAM_MINUS,
	HAND_CLOSED,
	HAND_OPEN,
	NUM_CURSORS
} MarlinCursorType;

GdkCursor *marlin_cursor_get (GtkWidget *widget,
			      MarlinCursorType type);

#endif
