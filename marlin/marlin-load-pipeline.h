/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_LOAD_PIPELINE_H__
#define __MARLIN_LOAD_PIPELINE_H__

#include <marlin/marlin-pipeline.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-sample.h>

#define MARLIN_LOAD_PIPELINE_ERROR marlin_load_pipeline_error_quark ()

#define MARLIN_LOAD_PIPELINE_TYPE (marlin_load_pipeline_get_type ())
#define MARLIN_LOAD_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_LOAD_PIPELINE_TYPE, MarlinLoadPipeline))
#define MARLIN_LOAD_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_LOAD_PIPELINE_TYPE, MarlinLoadPipelineClass))
#define IS_MARLIN_LOAD_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_LOAD_PIPELINE_TYPE))
#define IS_MARLIN_LOAD_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_LOAD_PIPELINE_TYPE))

typedef struct _MarlinLoadPipeline MarlinLoadPipeline;
typedef struct _MarlinLoadPipelineClass MarlinLoadPipelineClass;
typedef struct _MarlinLoadPipelinePrivate MarlinLoadPipelinePrivate;

struct _MarlinLoadPipeline {
	MarlinPipeline parent_pipeline;

	MarlinLoadPipelinePrivate *priv;
};

struct _MarlinLoadPipelineClass {
	MarlinPipelineClass parent_class;
};

typedef enum {
	MARLIN_LOAD_PIPELINE_ERROR_UNKNOWN_TYPE, /* Can't decode this format */
} MarlinLoadPipelineError;

GType marlin_load_pipeline_get_type (void);
MarlinLoadPipeline *marlin_load_pipeline_new (MarlinOperation *operation);
MarlinLoadPipeline *marlin_load_pipeline_new_from_sample (MarlinOperation *operation,
							  MarlinSample *sample);
#endif
