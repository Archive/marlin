/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_JACK_H__
#define __MARLIN_JACK_H__

#include <glib-object.h>

#include <jack/jack.h>

#include <marlin/marlin-sample.h>

G_BEGIN_DECLS

#define MARLIN_JACK_TYPE (marlin_jack_get_type ())
#define MARLIN_JACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_JACK_TYPE, MarlinJack))
#define MARLIN_JACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_JACK_TYPE, MarlinJackClass))
#define MARLIN_JACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), MARLIN_JACK_TYPE, MarlinJackClass))

#define MARLIN_JACK_ERROR marlin_jack_error_quark ()

typedef enum {
	MARLIN_JACK_ERROR_NO_SERVER, /* Server did not exist */
	MARLIN_JACK_ERROR_NO_SAMPLE, /* No sample to playback */
} MarlinJackError;

typedef struct _MarlinJack MarlinJack;
typedef struct _MarlinJackClass MarlinJackClass;

struct _MarlinJack {
	GObject parent;
};

struct _MarlinJackClass {
	GObjectClass parent_class;

	/* Methods to be overwritten */
	void (*set_sample) (MarlinJack *jack,
			    MarlinSample *sample);
	gboolean (*start) (MarlinJack *jack,
			   GError    **error);
	void (*stop) (MarlinJack *jack);
	void (*pause) (MarlinJack *jack);
	void (*cont) (MarlinJack *jack); /* This should be continue, but thats
					    a reserved word...maybe it should
					    just be merged with pause? FIXME */
	gboolean (*is_busy) (MarlinJack *jack);

	/* signals */
	void (*level_changed) (MarlinJack *jack,
			       guint       channels,
			       double     *rms,
			       double     *peak);
};

GQuark marlin_jack_error_quark (void);
GType marlin_jack_get_type (void);

gboolean marlin_jack_start (MarlinJack *jack,
			    GError    **error);
void marlin_jack_stop (MarlinJack *jack);
void marlin_jack_pause (MarlinJack *jack);
void marlin_jack_continue (MarlinJack *jack);

void marlin_jack_emit_levels (MarlinJack *jack,
			      float     **data,
			      long       *n_frames);
gboolean marlin_jack_is_busy (MarlinJack *jack);
G_END_DECLS

#endif
