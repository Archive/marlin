/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include <gst/gst.h>

#include <marlin/marlin-sample-sink.h>

static GstElementDetails sink_details =
GST_ELEMENT_DETAILS ("Marlin sample sink",
		     "Sink/Audio",
		     "Store data in a MarlinSample",
		     "Iain Holmes <iain@gnome.org>");

enum {
	PROP_0,
};

static GstStaticPadTemplate sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink%d",
			 GST_PAD_SINK,
			 GST_PAD_REQUEST,
			 GST_STATIC_CAPS ("audio/x-raw-float, "
					  "rate = (int) [1, MAX], "
					  "channels = (int) 1;"));

static GstElementClass *parent_class = NULL;

static void
finalize (GObject *object)
{
 	MarlinSampleSink *sink = MARLIN_SAMPLE_SINK (object);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
base_init (gpointer g_class)
{
	GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

	gst_element_class_set_details (element_class, &sink_details);

	gst_element_class_add_pad_template 
		(element_class,
		 gst_static_pad_template_get (&sink_factory));
}

static void
class_init (MarlinSampleSinkClass *klass)
{
	GObjectClass *o_class;
	GstBaseSinkClass *b_class;

	o_class = (GObjectClass *) klass;
	b_class = (GstBaseSinkClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	b_class->get_caps = get_caps;
}

GType
marlin_sample_sink_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo ifo = {
			sizeof (MarlinSampleSink),
			base_init,
			NULL, (GClassInitFunc) class_init,
			NULL, NULL, sizeof (MarlinSampleSink),
			0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_SAMPLE_SINK_TYPE,
					       "MarlinSampleSink",
					       &info, 0);
	}

	return type;
}
