/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <marlin/marlin-file-utils.h>

static char *dot_dir = NULL;

char *
marlin_file (const char *filename)
{
	char *fullname;
	int i;

	static char *paths[] = {
		"./",
		"./src/",
		"./pixmaps/",
		DATADIR "/",
		DATADIR "/pixmaps/",
		DATADIR "/pixmaps/marlin/",
		DATADIR "/marlin/",
		DATADIR "/marlin/ui/",
		DATADIR "/marlin/pixmaps/"
	};

	for (i = 0; i < (int) G_N_ELEMENTS (paths); i++) {
		fullname = g_strconcat (paths[i], filename, NULL);
		if (g_file_test (fullname, G_FILE_TEST_EXISTS) == TRUE) {
			return fullname;
		}

		g_free (fullname);
	}

	g_warning ("Failed to find %s", filename);
	return NULL;
}

const char *
marlin_dot_dir (void)
{
	if (dot_dir == NULL) {
		char *tmp;

		/* We do this to free the extra space that was allocated by
		   the GString used in g_build_filename. */
		tmp = g_build_filename (g_get_home_dir (), ".marlin", NULL);
		dot_dir = g_strdup (tmp);
		g_free (tmp);
	}

	return dot_dir;
}

void
marlin_ensure_dir_exists (const char *dir)
{
	if (g_file_test (dir, G_FILE_TEST_IS_DIR) == FALSE) {
		if (g_file_test (dir, G_FILE_TEST_EXISTS) == TRUE) {
			g_error ("%s exists, please move it out of the way.", dir);
		}

		if (mkdir (dir, 488) != 0) {
			g_error ("Failed to create directory %s.", dir);
		}
	}
}

/* Should rename (3) just be used instead of this */
#define BUFSIZE 8192
gboolean
marlin_file_copy (const char *src,
		  const char *dest,
		  GError **error)
{
	char buf[BUFSIZE];
	int s_fd, d_fd, bytes_read;

	s_fd = open (src, O_RDONLY);
	if (s_fd == -1) {
		g_warning ("Error opening %s for reading", src);
		return FALSE;
	}

	/* How do you know the default mode? */
	d_fd = open (dest, O_WRONLY | O_CREAT | O_TRUNC, 
		     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
	if (d_fd == -1) {
		g_warning ("Error opening %s for writing", dest);
		close (s_fd);
		return FALSE;
	}

	while ((bytes_read = read (s_fd, buf, BUFSIZE)) > 0) {
		if (write (d_fd, buf, bytes_read) != bytes_read) {
			g_warning ("Error writing data.");
			close (d_fd);
			close (s_fd);
			return FALSE;
		}
	}

	if (bytes_read < 0) {
		g_warning ("Error reading data");
		close (d_fd);
		close (s_fd);
		return FALSE;
	}

	close (d_fd);
	close (s_fd);
	return TRUE;
}

const char *
marlin_get_tmp_dir (void)
{
	static char *tmp_dir = NULL;

	if (tmp_dir == NULL) {
		const char *tmp;

		tmp = g_getenv ("MARLIN_TMP_DIR");
		if (tmp != NULL) {
			tmp_dir = g_strdup (tmp);
		} else {
			tmp_dir = g_strdup (g_get_tmp_dir ());
		}
	}

	return tmp_dir;
}

gboolean
marlin_check_tmp_dir (void)
{
	const char *tmp_dir = marlin_get_tmp_dir ();

	if (access (tmp_dir, W_OK) == 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}
