/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>

#include <gst/gst.h>
#include <gst/audio/multichannel.h>

#include "marlin-channel-splitter.h"

#define MARLIN_CHANNEL_SPLITTER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_CHANNEL_SPLITTER_TYPE, MarlinChannelSplitter))

typedef struct _MarlinChannelSplitter MarlinChannelSplitter;
typedef struct _MarlinChannelSplitterClass MarlinChannelSplitterClass;

struct _MarlinChannelSplitter {
	GstElement element;

	GstPad *sinkpad;
	GstCaps *caps;

	int channels;
	int chans_pending; /* While we're creating the pads, this contains
			      how many pads we will end up with */
	gboolean is_int;

	GstBuffer **out_buffers;
	gpointer *out_data;

	GList *srcpads;

	gboolean cache_events;
	GList *events;
};

struct _MarlinChannelSplitterClass {
	GstElementClass parent_class;
};

static const GstElementDetails splitter_details = 
	GST_ELEMENT_DETAILS ("Marlin Channel Splitter",
			     "Filter/Converter/Audio",
			     "Splits one interleaved multichannel audio stream into many mono audio streams",
			     "Iain Holmes <iain@gnome.org>");
static GstStaticPadTemplate sink_template =
	GST_STATIC_PAD_TEMPLATE ("sink",
				 GST_PAD_SINK,
				 GST_PAD_ALWAYS,
				 GST_STATIC_CAPS 
				 ("audio/x-raw-int, "
				  "endianness = (int) BYTE_ORDER, "
				  "signed = (boolean) TRUE, "
				  /*
				  "width = (int) 16, "
				  "depth = (int) 16, "
				  */
				  "rate = (int) [ 1, MAX ], "
				  "channels = (int) [ 1, MAX ];"
				  "audio/x-raw-float, "
				  "rate = (int) [ 1, MAX ], "
				  "channels = (int) [ 1, MAX ], "
				  "endianness = (int) BYTE_ORDER, "
				  "width = (int) 32 "));

static GstStaticPadTemplate src_template = 
	GST_STATIC_PAD_TEMPLATE ("src_%d",
				 GST_PAD_SRC,
				 GST_PAD_SOMETIMES,
				 GST_STATIC_CAPS
				 ("audio/x-raw-int, "
				  "endianness = (int) BYTE_ORDER, "
				  "signed = (boolean) TRUE, "
				  /*
				  "width = (int) 16, "
				  "depth = (int) 16, "
				  */
				  "rate = (int) [ 1, MAX ], "
				  "channels = (int) 1;"
				  "audio/x-raw-float, "
				  "rate = (int) [ 1, MAX ], "
				  "channels = (int) 1, "
				  "endianness = (int) BYTE_ORDER, "
				  "width = (int) 32 "));
static GstElementClass *parent_class = NULL;

static void
base_init (MarlinChannelSplitterClass *klass)
{
	GstElementClass *e_class = GST_ELEMENT_CLASS (klass);

	gst_element_class_add_pad_template 
		(e_class, gst_static_pad_template_get (&src_template));
	gst_element_class_add_pad_template
		(e_class, gst_static_pad_template_get (&sink_template));
	gst_element_class_set_details (e_class, &splitter_details);
}

static void
alloc_channels_data (MarlinChannelSplitter *splitter)
{
	if (!splitter->out_buffers && splitter->channels) {
		splitter->out_buffers = g_new0 (GstBuffer *, splitter->channels);
		splitter->out_data = g_new (gpointer, splitter->channels);
	}
}

static void
free_channels_data (MarlinChannelSplitter *splitter)
{
	g_free (splitter->out_buffers);
	g_free (splitter->out_data);
	splitter->out_buffers = NULL;
	splitter->out_data = NULL;
}

static GstStateChangeReturn
change_state (GstElement    *element,
	      GstStateChange transition)
{
	MarlinChannelSplitter *splitter;

	splitter = MARLIN_CHANNEL_SPLITTER (element);

	switch (transition) {
	case GST_STATE_CHANGE_READY_TO_PAUSED:
		break;

	case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
		break;

	case GST_STATE_CHANGE_READY_TO_NULL:
		break;

	default:
		break;
	}

	if (parent_class->change_state) {
		return parent_class->change_state (element, transition);
	} else {
		return GST_STATE_CHANGE_SUCCESS;
	}
}

static void
finalize (GObject *object)
{
	MarlinChannelSplitter *splitter;
	
	splitter = MARLIN_CHANNEL_SPLITTER (object);

	free_channels_data (splitter);

	g_list_free (splitter->srcpads);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinChannelSplitter *splitter;

	splitter = MARLIN_CHANNEL_SPLITTER (object);

	if (splitter->caps) {
		gst_caps_unref (splitter->caps);
		splitter->caps = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
class_init (MarlinChannelSplitter *klass)
{
	GObjectClass *o_class;
	GstElementClass *e_class;

	o_class = G_OBJECT_CLASS (klass);
	e_class = GST_ELEMENT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;

	e_class->change_state = change_state;
}

static GstCaps * 
make_src_caps (MarlinChannelSplitter *splitter,
	       GstPad                *pad)
{
 	GstAudioChannelPosition *channel_position; 
	GstStructure *structure;
	GstCaps *caps;
	char *name;
	int chan_num;

	caps = gst_caps_copy (splitter->caps);
	structure = gst_caps_get_structure (caps, 0);
	gst_structure_set (structure, "channels", G_TYPE_INT, 1, NULL);

	channel_position = g_new (GstAudioChannelPosition, 1);

	switch (splitter->chans_pending) {
	case 1:
		channel_position[0] = GST_AUDIO_CHANNEL_POSITION_FRONT_MONO;
		gst_audio_set_channel_positions (structure, channel_position);
		break;

	case 2:
		/* FIXME: Find a better way to parse this */
		name = gst_pad_get_name (pad);
		if (strlen (name) < 4) {
			g_warning ("Got invalid pad name: %s", name);
			
			g_free (name);
			g_object_unref (splitter);
			g_free (channel_position);
			return caps;
		}

		chan_num = atoi (&name[4]); /* FIXME: Yeah, seriously */
		g_free (name);

		if (chan_num == 0) {
			channel_position[0] = GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT;
		} else {
			channel_position[0] = GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT;
		}
		gst_audio_set_channel_positions (structure, channel_position);
		break;

	default:
		/* FIXME: need to work out some way to do this */
		g_warning ("Setting the channel position > 2 channels is not supported at this time.");
		break;
	}

	g_free (channel_position);
	return caps;
}

static gboolean
sink_set_caps (GstPad  *pad,
	       GstCaps *caps)
{
	MarlinChannelSplitter *splitter;
	GstStructure *structure;
	int i, new_chans;

	splitter = MARLIN_CHANNEL_SPLITTER (gst_pad_get_parent (pad));

	if (splitter->caps) {
		gst_caps_unref (splitter->caps);
	}

	splitter->caps = caps;
	gst_caps_ref (splitter->caps);

	structure = gst_caps_get_structure (caps, 0);

	/* Get the number of channels coming in */
	gst_structure_get_int (structure, "channels", &new_chans);
	splitter->chans_pending = new_chans;

	splitter->is_int = (strcmp (gst_structure_get_name (structure),
				    "audio/x-raw-int") == 0);
	if (new_chans != splitter->channels) {
		int need;

		need = new_chans - splitter->channels;

		if (need < 0) {
			GList *p;

			for (p = g_list_last (splitter->srcpads); p;) {
				GstPad *peer, *opad;
				GList *old;

				opad = p->data;

				/* Check if they're connected */
				peer = GST_PAD_PEER (opad);
				if (peer) {
					gst_pad_unlink (opad, peer);
				}

				gst_element_remove_pad (GST_ELEMENT (splitter),
							opad);

				old = p;
				p = p->prev;
				g_list_free_1 (old);

				/* Remove link to the next */
				if (p != NULL) {
					p->next = NULL;
				}
			}
		} else {
			/* Create that number of src_pads */
			for (i = splitter->channels; i < new_chans; i++) {
				GstPad *npad;
 				GstCaps *caps;
				char *pad_name;
				
				pad_name = g_strdup_printf ("src_%d", i);
				npad = gst_pad_new_from_template
					(gst_static_pad_template_get (&src_template),
					 pad_name);
				gst_pad_use_fixed_caps (npad);
				g_free (pad_name);

				caps = make_src_caps (splitter, npad);
				gst_pad_set_caps (npad, caps);
				gst_caps_unref (caps);
				GST_PAD_UNSET_FLUSHING (npad);
				gst_element_add_pad (GST_ELEMENT (splitter), 
						     npad);

				splitter->srcpads = g_list_append (splitter->srcpads, npad);
			}
		}

		splitter->channels = new_chans;
		free_channels_data (splitter);
		alloc_channels_data (splitter);
	}

	g_object_unref (splitter);
	return TRUE;
}

/* Can these be replaced by liboil? */
static void inline
do_int_deinterleave (gint16  *in_data, 
		     int      channels,
		     gint16 **out_data, 
		     guint    numframes)
{
	guint i, k;
	
	for (i = 0; i < numframes; i++) {
		for (k = 0; k < channels; k++) {
			out_data[k][i] = in_data[channels * i + k];
		}
	}
}

static void inline
do_float_deinterleave (gfloat   *in_data, 
		       int       channels,
		       gfloat  **out_data, 
		       guint     numframes)
{
	guint i, k;
	
	for (i = 0; i < numframes; i++) {
		for (k = 0; k < channels; k++) {
			out_data[k][i] = in_data[channels * i + k];
		}
	}
}

static void
push_events (MarlinChannelSplitter *splitter)
{
	GList *e;

	for (e = splitter->events; e; e = e->next) {
		GstEvent *event;

		event = GST_EVENT (e->data);

		gst_pad_event_default (splitter->sinkpad, event);
		/* Don't need to unref the event because it will 
		   be unreffed after its sent */
	}

	g_list_free (splitter->events);
	splitter->events = NULL;
}

static GstFlowReturn
sink_chain (GstPad    *pad,
	    GstBuffer *buffer)
{
	MarlinChannelSplitter *splitter;
	gpointer in_data;
	gpointer *out_data;
	GstBuffer **out_bufs;
	GList *p;
	int i;

	splitter = MARLIN_CHANNEL_SPLITTER (gst_pad_get_parent (pad));

	out_data = splitter->out_data;
	out_bufs = splitter->out_buffers;

	if (splitter->channels == 0) {
		GST_ELEMENT_ERROR (splitter, CORE, NEGOTIATION, (NULL),
				   ("format wasn't negotiated before chain function"));
		g_object_unref (splitter);
		return GST_FLOW_NOT_LINKED;
	} else if (splitter->channels == 1) {
		GstCaps *caps;
		GstPad *pad;
		
		pad = GST_PAD (splitter->srcpads->data);
		if (splitter->events) {
			push_events (splitter);
			splitter->cache_events = FALSE;
		}

		caps = gst_pad_get_caps (pad);
		gst_buffer_set_caps (buffer, caps);
		gst_caps_unref (caps);

/* 		g_print ("%f\n", ((float *) GST_BUFFER_DATA (buffer))[0]); */
		gst_pad_push (pad, buffer);
		g_object_unref (splitter);
		return GST_FLOW_OK;
	}

	in_data = (gpointer) GST_BUFFER_DATA (buffer);

	/* Create our buffers */
	for (i = 0; i < splitter->channels; i++) {
		out_bufs[i] = gst_buffer_new_and_alloc 
			(GST_BUFFER_SIZE (buffer) / splitter->channels);
		gst_buffer_stamp (out_bufs[i], buffer);
		out_data[i] = (gpointer) GST_BUFFER_DATA (out_bufs[i]);
	}

	if (splitter->is_int) {
		do_int_deinterleave ((gint16 *) in_data,
				     splitter->channels,
				     (gint16 **) out_data,
				     GST_BUFFER_SIZE (buffer) / splitter->channels / sizeof (gint16));
	} else {
		do_float_deinterleave ((gfloat *) in_data,
				       splitter->channels,
				       (gfloat **) out_data,
				       GST_BUFFER_SIZE (buffer) / splitter->channels / sizeof (gfloat));
	}

	gst_buffer_unref (buffer);

	for (i = 0, p = splitter->srcpads; p; p = p->next, i++) {
		GstPad *srcpad;
		GstCaps *caps;

		srcpad = GST_PAD (p->data);

		if (splitter->events) {
			push_events (splitter);
			splitter->cache_events = FALSE;
		}

		caps = gst_pad_get_caps (srcpad);
 		gst_buffer_set_caps (out_bufs[i], caps);
		gst_caps_unref (caps);

		gst_pad_push (srcpad, out_bufs[i]);
	}

	g_object_unref (splitter);
	return GST_FLOW_OK;
}

static gboolean
sink_event (GstPad   *pad,
	    GstEvent *event)
{
	MarlinChannelSplitter *splitter;
	gboolean ret = TRUE;

	splitter = MARLIN_CHANNEL_SPLITTER (gst_pad_get_parent (pad));

	/* FIXME: not thread-safe, there could be out-of-band
	 * events from a thread other than the streaming thread */
	if (splitter->srcpads == NULL) {
		splitter->events = g_list_append (splitter->events, event);
	} else {
		ret = gst_pad_event_default (pad, event);
	}

	g_object_unref (splitter);

	return ret;
}

static gboolean
sink_activate_push (GstPad  *pad,
		    gboolean active)
{
	MarlinChannelSplitter *splitter;

	splitter = MARLIN_CHANNEL_SPLITTER (gst_pad_get_parent (pad));

	g_object_unref (splitter);
	return TRUE;
}

static void
init (MarlinChannelSplitter *splitter)
{
	GstPad *sinkpad;

	splitter->cache_events = TRUE;

	splitter->channels = 0;
	splitter->chans_pending = 0;
	sinkpad = gst_pad_new_from_static_template (&sink_template, "sink");
	splitter->sinkpad = sinkpad;

	gst_pad_set_activatepush_function (sinkpad, sink_activate_push);
	gst_pad_set_setcaps_function (sinkpad, sink_set_caps);
	gst_pad_set_getcaps_function (sinkpad, gst_pad_proxy_getcaps);
	gst_pad_set_chain_function (sinkpad, sink_chain);
	gst_pad_set_event_function (sinkpad, sink_event);

	gst_element_add_pad (GST_ELEMENT (splitter), sinkpad);
}			       
		
GType
marlin_channel_splitter_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MarlinChannelSplitterClass),
			(GBaseInitFunc) base_init, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinChannelSplitter), 0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (GST_TYPE_ELEMENT,
					       "MarlinChannelSplitter",
					       &info, 0);
	}

	return type;
}
			
