/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <marlin/marlin-gst-extras.h>

static void
print_type (guint16 type_id)
{
	GstType *type = gst_type_find_by_id (type_id);

	g_print ("%d: %s: %s\n", type->id, type->mime, type->exts);
}

int
main (int argc,
      char **argv)
{
	GList *mimetypes;
	
	gst_init (&argc, &argv);

	mimetypes = get_encoder_mime_types_for_raw ();
	for (; mimetypes; mimetypes = mimetypes->next) {
		GstElementFactory *factory = mimetypes->data;
		GList *templs;

		templs = factory->padtemplates;

		for (; templs; templs = templs->next) {
			GstPadTemplate *t = GST_PAD_TEMPLATE (templs->data);

			if (GST_PAD_TEMPLATE_DIRECTION (t) == GST_PAD_SRC) {
				GstCaps *caps = GST_PAD_TEMPLATE_CAPS (t);

				if (caps == NULL) {
					continue;
				}
				
				print_type (caps->id);
			}
		}
	}
}
