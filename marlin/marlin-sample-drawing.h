/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_SAMPLE_DRAWING_H__
#define __MARLIN_SAMPLE_DRAWING_H__

#include <gtk/gtk.h>
#include <marlin/marlin-sample.h>

typedef struct _MarlinSampleDrawContext {
	MarlinSample *sample;

	GdkDrawable *drawable;

	gboolean draw_average;

	guint fpp;
	int base_offset;
	float vmax, vmin;
	guint height;
} MarlinSampleDrawContext;

void marlin_sample_draw (MarlinSampleDrawContext *dc,
			 GtkWidget *widget,
			 GdkRectangle *area,
			 GtkStateType state_type,
			 guint channel_num,
			 int xofs);
void marlin_sample_draw_buffer (MarlinSampleDrawContext *dc,
				GtkWidget *widget,
				MarlinPeak **buffer,
				GdkRectangle *area);
void marlin_sample_draw_to_buffer (MarlinSampleDrawContext *dc,
				   MarlinPeak **buffer,
				   GdkRectangle *area);


MarlinSampleDrawContext *marlin_sample_draw_context_new (MarlinSample *sample,
							 guint fpp,
							 gboolean draw_average);
void marlin_sample_draw_context_free (MarlinSampleDrawContext *dc);

#endif
