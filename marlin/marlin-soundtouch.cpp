/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <cstdio>
#include <ctime>

#include <soundtouch/SoundTouch.h>

#include "marlin-soundtouch.h"

G_BEGIN_DECLS

MarlinSoundtouch *
marlin_soundtouch_new (void)
{
	return (MarlinSoundtouch *) (new SoundTouch ());
}

void
marlin_soundtouch_free (MarlinSoundtouch *soundtouch)
{
	delete (SoundTouch *) soundtouch;
}

void
marlin_soundtouch_set_channels (MarlinSoundtouch *soundtouch,
				int               channels)
{
	((SoundTouch *) soundtouch)->setChannels (channels);
}

void
marlin_soundtouch_set_sample_rate (MarlinSoundtouch *soundtouch,
				   int               sample_rate)
{
	((SoundTouch *) soundtouch)->setSampleRate (sample_rate);
}

void
marlin_soundtouch_set_rate (MarlinSoundtouch *soundtouch,
			    float             rate)
{
	((SoundTouch *) soundtouch)->setRate (rate);
}

void 
marlin_soundtouch_set_rate_change (MarlinSoundtouch *soundtouch,
				   float             rate)
{
	((SoundTouch *) soundtouch)->setRateChange (rate);
}

void 
marlin_soundtouch_set_tempo (MarlinSoundtouch *soundtouch,
			     float             tempo)
{
	((SoundTouch *) soundtouch)->setTempo (tempo);
}

void
marlin_soundtouch_set_tempo_change (MarlinSoundtouch *soundtouch,
				    float             tempo)
{
	((SoundTouch *) soundtouch)->setTempoChange (tempo);
}

void
marlin_soundtouch_set_pitch (MarlinSoundtouch *soundtouch,
			     float             pitch)
{
	((SoundTouch *) soundtouch)->setPitch (pitch);
}

void 
marlin_soundtouch_set_pitch_octaves (MarlinSoundtouch *soundtouch,
				     float             pitch)
{
	((SoundTouch *) soundtouch)->setPitchOctaves (pitch);
}

void
marlin_soundtouch_set_pitch_semitones (MarlinSoundtouch *soundtouch,
				       float             pitch)
{
	((SoundTouch *) soundtouch)->setPitchSemiTones (pitch);
}

int
marlin_soundtouch_set (MarlinSoundtouch       *soundtouch,
		       MarlinSoundtouchSetting setting,
		       guint                   value)
{
	return ((SoundTouch *) soundtouch)->setSetting ((guint) setting, 
							value);
}

guint
marlin_soundtouch_get (MarlinSoundtouch       *soundtouch,
		       MarlinSoundtouchSetting setting)
{
	return ((SoundTouch *) soundtouch)->getSetting ((guint) setting);
}

void
marlin_soundtouch_put_samples (MarlinSoundtouch *soundtouch,
			       float            *samples,
			       int               length)
{
	((SoundTouch *) soundtouch)->putSamples (samples, length);
}

guint
marlin_soundtouch_take_samples (MarlinSoundtouch *soundtouch,
				float            *samples,
				guint             max_samples)
{
	return ((SoundTouch *) soundtouch)->receiveSamples (samples, 
							    max_samples);
}

void
marlin_soundtouch_flush (MarlinSoundtouch *soundtouch)
{
	((SoundTouch *) soundtouch)->flush ();
}

void
marlin_soundtouch_clear (MarlinSoundtouch *soundtouch)
{
	((SoundTouch *) soundtouch)->clear ();
}

guint
marlin_soundtouch_length (MarlinSoundtouch *soundtouch)
{
	((SoundTouch *) soundtouch)->numSamples ();
}

guint
marlin_soundtouch_unprocessed_length (MarlinSoundtouch *soundtouch)
{
	((SoundTouch *) soundtouch)->numUnprocessedSamples ();
}

gboolean
marlin_soundtouch_empty (MarlinSoundtouch *soundtouch)
{
	return ((SoundTouch *) soundtouch)->isEmpty ();
}

G_END_DECLS
