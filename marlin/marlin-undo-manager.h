/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_UNDO_MANAGER_H__
#define __MARLIN_UNDO_MANAGER_H__

#include <glib-object.h>
#include <glib.h>

#include <marlin/marlin-undoable.h>

#define MARLIN_UNDO_MANAGER_TYPE (marlin_undo_manager_get_type ())
#define MARLIN_UNDO_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_UNDO_MANAGER_TYPE, MarlinUndoManager))
#define IS_MARLIN_UNDO_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_UNDO_MANAGER_TYPE))

typedef struct _MarlinUndoContext MarlinUndoContext;
typedef struct _MarlinUndoHistory {
	char *name;
	char *info;

	gboolean current; /* Is this the position we are at in the history */
	MarlinUndoContext *ctxt; 
} MarlinUndoHistory;

typedef struct _MarlinUndoManager MarlinUndoManager;
typedef struct _MarlinUndoManagerClass MarlinUndoManagerClass;
typedef struct _MarlinUndoManagerPrivate MarlinUndoManagerPrivate;

struct _MarlinUndoManager {
	GObject object;

	MarlinUndoManagerPrivate *priv;
};

struct _MarlinUndoManagerClass {
	GObjectClass parent_class;

	void (*changed) (MarlinUndoManager *manager);
};

GType marlin_undo_manager_get_type (void);
MarlinUndoManager *marlin_undo_manager_new (void);

gboolean marlin_undo_manager_can_undo (MarlinUndoManager *manager);
gboolean marlin_undo_manager_can_redo (MarlinUndoManager *manager);

MarlinUndoContext *marlin_undo_manager_context_begin (MarlinUndoManager *manager,
						      const char *name);
void marlin_undo_manager_context_end (MarlinUndoManager *manager,
				      MarlinUndoContext *ctxt);
void marlin_undo_manager_context_cancel (MarlinUndoManager *manager,
					 MarlinUndoContext *ctxt);

void marlin_undo_context_add (MarlinUndoContext *context,
			      MarlinUndoable *undoable);
const char *marlin_undo_manager_get_undo_name (MarlinUndoManager *manager);
const char *marlin_undo_manager_get_redo_name (MarlinUndoManager *manager);

void marlin_undo_manager_undo (MarlinUndoManager *manager);
void marlin_undo_manager_redo (MarlinUndoManager *manager);

GList *marlin_undo_manager_get_history (MarlinUndoManager *manager);
void marlin_undo_manager_free_history_list (GList *history_list);
#endif
