/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include <gst/gst.h>

#include <marlin-load-pipeline.h>
#include <marlin-gst-extras.h>
#include <marlin-sample.h>

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_FILENAME
};

struct _MarlinLoadPipelinePrivate {
	MarlinSample *sample;
	char *filename;

	/* Pipeline elements */
	GstElement *src;
	GstElement *decoder;
	GstElement *audioconvert;
	GstElement *splitter;

	GPtrArray *sinks; /* Array of MarlinChannelSink */

	GstPad *audio_pad;

	guint32 bus_id;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_LOAD_PIPELINE_TYPE, MarlinLoadPipelinePrivate))
G_DEFINE_TYPE (MarlinLoadPipeline, marlin_load_pipeline, MARLIN_PIPELINE_TYPE)

GQuark
marlin_load_pipeline_error_quark (void)
{
	static GQuark quark = 0;
	if (quark == 0) {
		quark = g_quark_from_static_string ("marlin-load-pipeline-error-quark");
	}

	return quark;
}

static void
finalize (GObject *object)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;

	pipeline = MARLIN_LOAD_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->filename) {
		g_free (priv->filename);
	}

	g_ptr_array_free (priv->sinks, TRUE);

	G_OBJECT_CLASS (marlin_load_pipeline_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;

	pipeline = MARLIN_LOAD_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->sample) {
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}

	if (priv->bus_id > 0) {
		GstBus *bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
		g_signal_handler_disconnect (bus, priv->bus_id);
		g_object_unref (bus);
		priv->bus_id = 0;
	}

	G_OBJECT_CLASS (marlin_load_pipeline_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;

	pipeline = MARLIN_LOAD_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		if (priv->sample) {
			g_object_unref (G_OBJECT (priv->sample));
		}
		priv->sample = MARLIN_SAMPLE (g_value_dup_object (value));

		if (priv->filename) {
			g_object_set (G_OBJECT (priv->sample),
				      "filename", priv->filename,
				      NULL);
		}
		break;

	case PROP_FILENAME:
		if (priv->filename) {
			g_free (priv->filename);
		}
		priv->filename = g_value_dup_string (value);
		if (priv->sample) {
			g_object_set (G_OBJECT (priv->sample),
				      "filename", priv->filename,
				      NULL);
		}

		g_object_set (G_OBJECT (priv->src),
			      "location", priv->filename,
			      NULL);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;

	pipeline = MARLIN_LOAD_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_SAMPLE:
		g_value_set_object (value, priv->sample);
		break;

	case PROP_FILENAME:
		g_value_set_string (value, priv->filename);
		break;

	default:
		break;
	}
}

static guint64
report_position (MarlinPipeline *marlin_pipeline)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;
	GstElement *element;
	GstFormat fmt;
	gint64 pos, duration;

	pipeline = (MarlinLoadPipeline *) marlin_pipeline;
	priv = pipeline->priv;

 	element = (GstElement *) pipeline;

	fmt = GST_FORMAT_TIME;
	if (gst_element_query_position (element, &fmt, &pos) &&
	    gst_element_query_duration (element, &fmt, &duration)) {
		MarlinOperation *operation;
		float p;
		int percentage;

		p = ((float) pos) / ((float) duration);
		percentage = (int) (p * 100);

		operation = marlin_pipeline_get_operation (marlin_pipeline);
		if (operation) {
			marlin_operation_progress (operation, percentage);
		}

		return pos;
	}

	return 0;
}

static void
marlin_load_pipeline_class_init (MarlinLoadPipelineClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	MarlinPipelineClass *p_class;

	p_class = (MarlinPipelineClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	p_class->report_position = report_position;

	g_type_class_add_private (o_class, sizeof (MarlinLoadPipelinePrivate));

	/* Properties */
	g_object_class_install_property (o_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));

	g_object_class_install_property (o_class,
					 PROP_FILENAME,
					 g_param_spec_string ("filename",
							      "", "",
							      "Untitled",
							      G_PARAM_READWRITE));
}

static void
new_splitter_pad (GstElement         *splitter,
		  GstPad             *pad,
		  MarlinLoadPipeline *pipeline)
{
	MarlinLoadPipelinePrivate *priv;
	GstElement *sink, *q;
	GstPad *sink_pad;
	char *name;

	priv = pipeline->priv;

	name = g_strdup_printf ("load-q-%d", priv->sinks->len);
	q = gst_element_factory_make ("queue", name);
	g_free (name);

	gst_bin_add (GST_BIN (pipeline), q);
	sink_pad = gst_element_get_pad (q, "sink");
	if (gst_pad_link (pad, sink_pad) != GST_PAD_LINK_OK) {
		g_warning ("Failed to link pad to queue");

		gst_bin_remove (GST_BIN (pipeline), q);
		return;
	}

	name = g_strdup_printf ("load-sink-%d", priv->sinks->len);
 	sink = gst_element_factory_make ("marlin-channel-sink", name);
	g_free (name);

	if (priv->sample) {
		g_object_set (G_OBJECT (sink),
			      "sample", priv->sample,
			      NULL);
	}

	gst_bin_add (GST_BIN (pipeline), sink);

	if (gst_element_link (q, sink) == FALSE) {
		g_warning ("Failed to link queue to sink");

 		gst_bin_remove (GST_BIN (pipeline), q);
		gst_bin_remove (GST_BIN (pipeline), sink);
		return;
	}

 	gst_element_set_state (q, GST_STATE_PLAYING);
	gst_element_set_state (sink, GST_STATE_PLAYING);

	/* It linked ok, put it in the bin */
	g_ptr_array_add (priv->sinks, sink);
}

static void
new_decoder_pad (GstElement         *decoder,
		 GstPad             *pad,
		 gboolean            final,
		 MarlinLoadPipeline *pipeline)
{
	MarlinLoadPipelinePrivate *priv;
	GstCaps *caps;
	GstStructure *structure;

	priv = pipeline->priv;

	caps = gst_pad_get_caps (pad);

	structure = gst_caps_get_structure (caps, 0);
	if (strncmp (gst_structure_get_name (structure), "audio/", 6) == 0) {
		GstPad *convert_sink;
		GstCaps *filtercaps;

		/* We like this pad */
		priv->audio_pad = pad;

		/* Lets connect it */
		priv->audioconvert = gst_element_factory_make ("audioconvert",
							       "audioconvert");
		priv->splitter = gst_element_factory_make ("marlin-channel-splitter",
							   "splitter");
		g_signal_connect (priv->splitter, "pad-added",
				  G_CALLBACK (new_splitter_pad), pipeline);

		gst_bin_add_many (GST_BIN (pipeline), priv->audioconvert,
				  priv->splitter, NULL);

		convert_sink = gst_element_get_pad (priv->audioconvert,
						    "sink");
		if (gst_pad_link (priv->audio_pad,
				  convert_sink) != GST_PAD_LINK_OK) {
			g_print ("Failed to link audio_pad to converter\n");
			return;
		}

		filtercaps = gst_caps_new_simple ("audio/x-raw-float",
						  NULL);
		if (gst_element_link_filtered (priv->audioconvert,
					       priv->splitter,
					       filtercaps) == FALSE) {
			g_print ("Failed to link audioconvert and splitter\n");
			gst_caps_unref (filtercaps);
			return;
		}
		gst_caps_unref (filtercaps);

		gst_element_set_state (priv->audioconvert, GST_STATE_PLAYING);
		gst_element_set_state (priv->splitter, GST_STATE_PLAYING);
	}

	gst_caps_unref (caps);

	if (final && priv->audio_pad == NULL) {
		g_print ("All pads created: No audio\n");
	}
}

static gboolean
connect_pipeline (MarlinLoadPipeline *pipeline,
		  GstCaps            *caps)
{
	MarlinLoadPipelinePrivate *priv;
	GstStructure *structure;
	int channels, rate;

	priv = pipeline->priv;

	structure = gst_caps_get_structure (caps, 0);

	if (gst_structure_get_int (structure, "rate", &rate) == FALSE) {
		char *cap_string;

		cap_string = gst_caps_to_string (caps);
		g_print ("No rate set in these caps\n%s", cap_string);
		g_free (cap_string);
		return FALSE;
	}

	if (gst_structure_get_int (structure, "channels", &channels) == FALSE) {
		char *cap_string;

		cap_string = gst_caps_to_string (caps);
		g_print ("No channels set in these caps\n%s", cap_string);
		g_free (cap_string);
		return FALSE;
	}

	/* Set the values on the sample */
	if (priv->sample) {
		g_object_set (G_OBJECT (priv->sample),
			      "channels", channels,
			      "sample_rate", rate,
			      NULL);
	} else {
		g_print ("No sample set on load-pipeline\n");
		return FALSE;
	}

	return TRUE;
}

static gboolean
bus_message_cb (GstBus     *bus,
		GstMessage *message,
		gpointer    data)
{
	MarlinLoadPipeline *pipeline;
	MarlinLoadPipelinePrivate *priv;

	pipeline = MARLIN_LOAD_PIPELINE (data);
	priv = pipeline->priv;

	switch (GST_MESSAGE_TYPE (message)) {
		break;

	case GST_MESSAGE_EOS:
/* 		marlin_sample_frame_count_changed (priv->sample); */
		break;

	case GST_MESSAGE_STATE_CHANGED: {
		GstState old, new, pending;

		gst_message_parse_state_changed (message, &old, &new, &pending);
		if (old == GST_STATE_READY &&
		    new == GST_STATE_PAUSED &&
		    GST_MESSAGE_SRC (message) == (GstObject *) pipeline) {
			if (priv->audio_pad) {
				GstCaps *caps;

				caps = gst_pad_get_caps (priv->audio_pad);
 				connect_pipeline (pipeline, caps);
				gst_caps_unref (caps);
			} else {
				g_print ("Pipeline has reached paused, but has not audio pad yet\n");
			}
		}
		break;
	}

	case GST_MESSAGE_TAG: {
		GstTagList *tags;

		gst_message_parse_tag (message, &tags);
		g_object_set (G_OBJECT (priv->sample),
			      "tags", tags,
			      NULL);
		gst_tag_list_free (tags);
		break;
	}

	default:
		break;
	}

	return TRUE;
}

static void
marlin_load_pipeline_init (MarlinLoadPipeline *pipeline)
{
	MarlinLoadPipelinePrivate *priv;
	GstBus *bus;

	pipeline->priv = GET_PRIVATE (pipeline);
	priv = pipeline->priv;

	priv->sinks = g_ptr_array_new ();

	priv->src = marlin_make_gst_source ("src");
	if (priv->src == NULL) {
		g_warning ("No source available for load pipeline.");
	}
	priv->decoder = gst_element_factory_make ("decodebin", "decoder");
	g_signal_connect (G_OBJECT (priv->decoder), "new-decoded-pad",
			  G_CALLBACK (new_decoder_pad), pipeline);

	/* We wait til the decoder has worked out what the file is
	   before creating the rest of the pipeline */
	gst_bin_add_many (GST_BIN (pipeline), priv->src, priv->decoder, NULL);

	if (gst_element_link (priv->src, priv->decoder) == FALSE) {
		g_error ("Cannot link src and decoder");
	}

	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	gst_bus_add_signal_watch (bus);
	priv->bus_id = g_signal_connect (bus, "message",
					 G_CALLBACK (bus_message_cb), pipeline);
	g_object_unref (bus);
}

MarlinLoadPipeline *
marlin_load_pipeline_new (MarlinOperation *operation)
{
	return g_object_new (MARLIN_LOAD_PIPELINE_TYPE,
			     "operation", operation,
			     NULL);
}

MarlinLoadPipeline *
marlin_load_pipeline_new_from_sample (MarlinOperation *operation,
				      MarlinSample *sample)
{
	return g_object_new (MARLIN_LOAD_PIPELINE_TYPE,
			     "operation", operation,
			     "sample", sample,
			     NULL);
}
