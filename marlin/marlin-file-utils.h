/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_FILE_UTILS_H__
#define __MARLIN_FILE_UTILS_H__

#include <glib.h>

char *marlin_file (const char *filename);
const char *marlin_dot_dir (void);
void marlin_ensure_dir_exists (const char *dir);
gboolean marlin_file_copy (const char *src,
			   const char *dest,
			   GError **error);

const char *marlin_get_tmp_dir (void);
gboolean marlin_check_tmp_dir (void);

#endif
