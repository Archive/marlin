/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gtk/gtk.h>
#include <gst/gst.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-load-pipeline.h>
#include <marlin/marlin-save-pipeline.h>

GObject *sample;
char *in_filename, *out_filename;

static void
load_started (MarlinOperation *operation,
	      gpointer data)
{
	g_print ("Load started\n");
}

static void
save_started (MarlinOperation *operation,
	      gpointer data)
{
	g_print ("Save started\n");
}

static void
save_finished (MarlinOperation *operation,
	       gpointer data)
{
	g_print ("Save finished\n");
}

static gboolean
timeout_func (gpointer data)
{
	MarlinOperation *operation;
	MarlinSavePipeline *save;
	
	operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (save_started), NULL);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (save_finished), NULL);

	save = g_object_new (MARLIN_SAVE_PIPELINE_TYPE,
			     "operation", operation,
			     "sample", sample,
			     "filename", out_filename,
			     "mimetype", "audio/x-mp3",
			     NULL);

	g_print ("About to run\n");
	marlin_pipeline_set_state (MARLIN_PIPELINE (save), GST_STATE_PLAYING);
	return FALSE;
}

static void
load_finished (MarlinOperation *operation,
	       gpointer data)
{
	g_print ("Load finished - starting to save\n");
	g_timeout_add (3000, timeout_func, NULL);
}

static gboolean
idle_func (gpointer data)
{
	MarlinOperation *operation;
	MarlinLoadPipeline *load;
	
	operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (load_started), NULL);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (load_finished), NULL);

	load = g_object_new (MARLIN_LOAD_PIPELINE_TYPE,
			     "operation", operation,
			     "sample", sample,
			     "filename", in_filename,
			     NULL);

	marlin_pipeline_set_state (MARLIN_PIPELINE (load), GST_STATE_PLAYING);
	return FALSE;
}

int
main (int argc,
      char **argv)
{
	g_thread_init (NULL);
	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);
	marlin_mt_initialise ();

	if (argc != 3) {
		g_print ("Usage: %s <in filename> <out filename>\n", argv[0]);
		exit (1);
	}

	sample = marlin_sample_new ();
	in_filename = argv[1];
	out_filename = argv[2];

	g_idle_add (idle_func, NULL);
	gtk_main ();
}

	
