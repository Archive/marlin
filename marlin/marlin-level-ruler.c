/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <marlin/marlin-level-ruler.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-cursors.h>

enum {
	BASELINE_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_CHANNELS,
	PROP_LEVEL_DISPLAY
};

struct _MarlinLevelRulerPrivate {
	guint channels;
	float vmax, vmin; /* Upper and lower limits of the channel view */
	int base_offset, yofs, real_size;

	MarlinScale level_display;
	PangoLayout *layout;

	gboolean grabbed; /* Are we grabbing the level? */
	MarlinChannelPosition channel_grab; /*What channel was grabbed? */
	int ofs;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_LEVEL_RULER_TYPE, MarlinLevelRulerPrivate))
G_DEFINE_TYPE (MarlinLevelRuler, marlin_level_ruler, GTK_TYPE_WIDGET);

static guint32 signals[LAST_SIGNAL];

#define DEFAULT_CHANNELS 2
#define DEFAULT_VMAX (1.0)
#define DEFAULT_VMIN (-1.0)

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (marlin_level_ruler_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinLevelRuler *ruler = (MarlinLevelRuler *) object;
	MarlinLevelRulerPrivate *priv = ruler->priv;

	if (priv->layout) {
		g_object_unref (priv->layout);
		priv->layout = NULL;
	}

	G_OBJECT_CLASS (marlin_level_ruler_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
	      guint32 prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	GtkWidget *widget = GTK_WIDGET (object);
	MarlinLevelRuler *ruler = MARLIN_LEVEL_RULER (object);
	int chan_height, extra;

	switch (prop_id) {
	case PROP_CHANNELS:
		ruler->priv->channels = g_value_get_uint (value);

		extra = ruler->priv->channels - 1;
		chan_height = (widget->allocation.height - extra) / ruler->priv->channels;
		ruler->priv->real_size = chan_height;
		ruler->priv->yofs = (ruler->priv->real_size - chan_height) / 2;

		if (GTK_WIDGET_DRAWABLE (ruler)) {
			GdkRectangle area;
			GtkWidget *widget = GTK_WIDGET (ruler);

			area.x = 0;
			area.y = 0;
			area.width = widget->allocation.width;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area,
						    FALSE);
		}

		break;

	case PROP_LEVEL_DISPLAY:
		ruler->priv->level_display = g_value_get_enum (value);
		if (GTK_WIDGET_DRAWABLE (ruler)) {
			GdkRectangle area;
			GtkWidget *widget = GTK_WIDGET (ruler);

			area.x = 0;
			area.y = 0;
			area.width = widget->allocation.width;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area,
						    FALSE);
		}

		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint32 prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinLevelRuler *ruler = MARLIN_LEVEL_RULER (object);

	switch (prop_id) {
	case PROP_LEVEL_DISPLAY:
		g_value_set_enum (value, ruler->priv->level_display);
		break;

	default:
		break;
	}
}

static void
size_allocate (GtkWidget *widget,
	       GtkAllocation *allocation)
{
	MarlinLevelRuler *ruler;
	int extra, chan_height;
	double f, d;

	ruler = MARLIN_LEVEL_RULER (widget);

	widget->allocation = *allocation;

	extra = ruler->priv->channels - 1;
	chan_height = (widget->allocation.height - extra) / ruler->priv->channels;

	d = ruler->priv->vmax - ruler->priv->vmin;
	f = chan_height / d;
	ruler->priv->real_size = (int) (f * 2);
	ruler->priv->yofs = (ruler->priv->real_size - chan_height) / 2;

	if (GTK_WIDGET_REALIZED (widget)) {
		GdkRectangle area;

		gdk_window_move_resize (widget->window,
					allocation->x,
					allocation->y,
					allocation->width,
					allocation->height);

		area.x = 0;
		area.y = 0;
		area.width = allocation->width;
		area.height = allocation->height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}

	GTK_WIDGET_CLASS (marlin_level_ruler_parent_class)->size_allocate (widget, allocation);
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *req)
{
	MarlinLevelRuler *ruler = MARLIN_LEVEL_RULER (widget);
	int width;

	pango_layout_set_markup (ruler->priv->layout,
				 "<small><small>0.00</small></small>", -1);
	pango_layout_get_size (ruler->priv->layout, &width, NULL);
  	req->width = widget->style->xthickness * 2 + PANGO_PIXELS (width) + 7;
}

static void
realize (GtkWidget *widget)
{
	GdkWindowAttr attributes;
	int attributes_mask;

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);
	attributes.event_mask = gtk_widget_get_events (widget);
	attributes.event_mask |= (GDK_EXPOSURE_MASK |
				  GDK_BUTTON_PRESS_MASK |
				  GDK_BUTTON_RELEASE_MASK |
				  GDK_POINTER_MOTION_MASK);

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
					 &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
	gdk_window_set_back_pixmap (widget->window, NULL, FALSE);
}

#define YPOS(v) chan_area.y + chan_height - ((((v) - ruler->priv->vmin) * chan_height) / (ruler->priv->vmax - ruler->priv->vmin))

static void
draw_channel (GtkWidget *widget,
	      GdkRectangle *area,
	      GtkStateType state_type,
	      guint32 channel_num)
{
	MarlinLevelRuler *ruler = MARLIN_LEVEL_RULER (widget);
	MarlinLevelRulerPrivate *priv = ruler->priv;
	GdkRectangle chan_area;
	int chan_height, quart, half, extra, yv;
	int height, width;

	extra = ruler->priv->channels - 1;
	chan_height = (widget->allocation.height - extra) / ruler->priv->channels;
	quart = (int) (chan_height / 4);
	half = quart * 2;

	chan_area.x = 0;
	chan_area.y = (chan_height * channel_num) + channel_num;
	chan_area.width = widget->allocation.width;
	chan_area.height = chan_height;

	gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], &chan_area);
	gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], &chan_area);

	yv = YPOS (0.0) + priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->text_gc[state_type],
		       area->x + (area->width - 10), yv,
		       area->x + area->width, yv);

	if (priv->level_display == MARLIN_SCALE_LINEAR) {
		pango_layout_set_markup (priv->layout, "<small>0</small>", -1);
	} else {
		pango_layout_set_markup (priv->layout, "<small>-\xe2\x88\x9e</small>", -1);
	}
	pango_layout_get_size (priv->layout, &width, &height);

	gdk_draw_layout (widget->window,
			 widget->style->text_gc[state_type],
			 area->x + (area->width - 12 - PANGO_PIXELS (width)),
			 yv - (PANGO_PIXELS (height) / 2),
			 priv->layout);

	yv = YPOS (0.5) + priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->dark_gc[state_type],
		       area->x + (area->width - 5), yv,
		       area->x + area->width, yv);

	if (priv->level_display == MARLIN_SCALE_LINEAR) {
		pango_layout_set_markup (priv->layout, "<small><small>50</small></small>", -1);
	} else {
		char *str;

		str = g_strdup_printf ("<small><small>%f</small></small>",
/*  				       (double) marlin_percent_to_db (50)); */
				       0.0);
		pango_layout_set_markup (priv->layout, str, -1);
		g_free (str);
	}
	pango_layout_get_size (priv->layout, &width, &height);

	gdk_draw_layout (widget->window,
			 widget->style->dark_gc[state_type],
			 area->x + (area->width - 7 - PANGO_PIXELS (width)),
			 yv - (PANGO_PIXELS (height) / 2),
			 priv->layout);

	yv = YPOS (-0.5) + priv->base_offset;
	gdk_draw_line (widget->window,
		       widget->style->dark_gc[state_type],
		       area->x + (area->width - 5), yv,
		       area->x + area->width, yv);

	if (priv->level_display == MARLIN_SCALE_LINEAR) {
		pango_layout_set_markup (priv->layout, "<small><small>-50</small></small>", -1);
	} else {
		char *str;

		str = g_strdup_printf ("<small><small>%f</small></small>",
/*  				       (double) marlin_percent_to_db (-50)); */
				       0.0);

		pango_layout_set_markup (priv->layout, str, -1);
		g_free (str);
	}
	pango_layout_get_size (priv->layout, &width, &height);

	gdk_draw_layout (widget->window,
			 widget->style->dark_gc[state_type],
			 area->x + (area->width - 7 - PANGO_PIXELS (width)),
			 yv - (PANGO_PIXELS (height) / 2),
			 priv->layout);

	gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);
	gdk_gc_set_clip_rectangle (widget->style->dark_gc[state_type], NULL);
}

static void
ruler_paint (GtkWidget *widget,
	     GdkRectangle *area,
	     GtkStateType state_type)
{
	MarlinLevelRuler *ruler = MARLIN_LEVEL_RULER (widget);
	int chan_height, i;

	gtk_paint_box (widget->style, widget->window,
		       GTK_STATE_NORMAL, GTK_SHADOW_OUT,
		       NULL, widget, "marlin-db-ruler",
		       0, 0,
		       widget->allocation.width,
		       widget->allocation.height);

	chan_height = (widget->allocation.height - (ruler->priv->channels - 1)) / ruler->priv->channels;

	for (i = 0; i < ruler->priv->channels; i++) {
		GdkRectangle chan_area, inter;

		chan_area.x = 0;
		chan_area.y = (chan_height * i) + i;
		chan_area.width = widget->allocation.width;
		chan_area.height = chan_height;

		if (gdk_rectangle_intersect (area, &chan_area, &inter)) {
			draw_channel (widget, &inter, state_type, i);
		}

		if (i > 0) {
			gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], area);
			gdk_draw_line (widget->window,
				       widget->style->text_gc[state_type],
				       area->x, chan_height * i,
				       area->x + widget->allocation.width,
				       chan_height * i);
			gdk_gc_set_clip_rectangle (widget->style->text_gc[state_type], NULL);
		}
	}
}

static int
expose_event (GtkWidget *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle *rects;
		int i, n_rects;

		gdk_region_get_rectangles (event->region, &rects, &n_rects);
		for (i = 0; i < n_rects; i++) {
			ruler_paint (widget, &rects[i],
				     GTK_WIDGET_STATE (widget));
		}

		g_free (rects);
	}

	return FALSE;
}

static int
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	MarlinLevelRuler *ruler;
	MarlinLevelRulerPrivate *priv;
	GdkCursor *cursor;
	GdkRectangle area;
	int y, channel_height;

	/* Grab focus back */
	gtk_widget_grab_focus (widget);

	ruler = MARLIN_LEVEL_RULER (widget);
	priv = ruler->priv;

	channel_height = widget->allocation.height / priv->channels;

	y = event->y;
	if (y < channel_height) {
		priv->channel_grab = MARLIN_CHANNEL_LEFT;
	} else {
		priv->channel_grab = MARLIN_CHANNEL_RIGHT;
	}

	switch (event->button) {
	case 1:
		switch (event->type) {
		case GDK_BUTTON_PRESS:
			cursor = marlin_cursor_get (widget, HAND_CLOSED);
			gdk_pointer_grab (widget->window,
					  FALSE,
					  GDK_POINTER_MOTION_MASK |
					  GDK_BUTTON1_MOTION_MASK |
					  GDK_BUTTON_RELEASE_MASK,
					  NULL, cursor, event->time);
			gdk_cursor_unref (cursor);

			priv->grabbed = TRUE;

			priv->ofs = y;
			break;

		case GDK_2BUTTON_PRESS:
			/* Reset the base offset */
			priv->base_offset = 0;

			area.x = 0;
			area.y = 0;
			area.width = widget->allocation.width;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
			g_signal_emit (ruler, signals[BASELINE_CHANGED], 0, priv->base_offset);
			break;

		default:
			break;
		}

		break;

	default:
		break;
	}

	return FALSE;
}

static int
button_release_event (GtkWidget *widget,
		      GdkEventButton *event)
{
	MarlinLevelRuler *ruler;

	ruler = MARLIN_LEVEL_RULER (widget);

	switch (event->button) {
	case 1:
		if (ruler->priv->grabbed) {
			gdk_pointer_ungrab (GDK_CURRENT_TIME);
			ruler->priv->grabbed = FALSE;
		}

		ruler->priv->ofs = 0;
		break;

	default:
		break;
	}

	return FALSE;
}

static gboolean
motion_notify_event (GtkWidget *widget,
		     GdkEventMotion *event)
{
	MarlinLevelRuler *ruler;
	MarlinLevelRulerPrivate *priv;
	GdkRectangle chan_area, area;
	int dy, chan_height, extra;

	ruler = MARLIN_LEVEL_RULER (widget);
	priv = ruler->priv;

	extra = ruler->priv->channels - 1;
	chan_height = (widget->allocation.height - extra) / ruler->priv->channels;
	chan_area.x = 0;
	chan_area.y = 0;
	chan_area.width = widget->allocation.width;
	chan_area.height = chan_height;

	if (priv->grabbed) {
		dy = event->y - priv->ofs;
		priv->ofs = event->y;

		if (priv->yofs + dy <= 0) {
			priv->yofs = 0;
			return FALSE;
		} else if (chan_height + priv->yofs + dy >= priv->real_size) {
			priv->yofs = priv->real_size - chan_height;
			return FALSE;
		} else {
			priv->yofs += dy;
		}

		priv->base_offset += dy;

		area.x = 0;
		area.y = 0;
		area.width = widget->allocation.width;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

		g_signal_emit (ruler, signals[BASELINE_CHANGED], 0, priv->base_offset);
	}

	return FALSE;
}

static void
marlin_level_ruler_class_init (MarlinLevelRulerClass *klass)
{
	GObjectClass *o_class;
	GtkWidgetClass *w_class;

	o_class = G_OBJECT_CLASS (klass);
	w_class = GTK_WIDGET_CLASS (klass);

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	w_class->size_allocate = size_allocate;
 	w_class->size_request = size_request;
	w_class->realize = realize;
	w_class->expose_event = expose_event;
	w_class->button_press_event = button_press_event;
	w_class->button_release_event = button_release_event;
	w_class->motion_notify_event = motion_notify_event;

	g_type_class_add_private (o_class, sizeof (MarlinLevelRulerPrivate));

	g_object_class_install_property (o_class,
					 PROP_CHANNELS,
					 g_param_spec_uint ("channels",
							    "", "",
							    0, G_MAXUINT, 0,
							    G_PARAM_WRITABLE));
	g_object_class_install_property (o_class,
					 PROP_LEVEL_DISPLAY,
					 g_param_spec_enum ("scale",
							    "", "",
							    MARLIN_TYPE_SCALE,
							    MARLIN_SCALE_LINEAR,
							    G_PARAM_READWRITE));
	signals[BASELINE_CHANGED] = g_signal_new ("baseline-changed",
						  G_TYPE_FROM_CLASS (klass),
						  G_SIGNAL_RUN_FIRST |
						  G_SIGNAL_NO_RECURSE,
						  G_STRUCT_OFFSET (MarlinLevelRulerClass, baseline_changed),
						  NULL, NULL,
						  g_cclosure_marshal_VOID__INT,
						  G_TYPE_NONE,
						  1, G_TYPE_INT);
}

static void
marlin_level_ruler_init (MarlinLevelRuler *ruler)
{
	MarlinLevelRulerPrivate *priv;

	priv = ruler->priv = GET_PRIVATE (ruler);
	priv->channels = DEFAULT_CHANNELS;
	priv->vmin = DEFAULT_VMIN;
	priv->vmax = DEFAULT_VMAX;
	priv->level_display = MARLIN_SCALE_LINEAR;
	priv->layout = gtk_widget_create_pango_layout (GTK_WIDGET (ruler), "0");
}

void
marlin_level_ruler_set_levels (MarlinLevelRuler *ruler,
			       float vmin,
			       float vmax)
{
	GtkWidget *widget = GTK_WIDGET (ruler);
	double d, f;
	int extra, chan_height;

	ruler->priv->vmin = vmin;
	ruler->priv->vmax = vmax;

	d = vmax - vmin;

	extra = ruler->priv->channels - 1;
	chan_height = (widget->allocation.height - extra) / ruler->priv->channels;

	f = chan_height / d;
	ruler->priv->real_size = (int) (f * 2);

	ruler->priv->yofs = (ruler->priv->real_size - chan_height) / 2;

	if (GTK_WIDGET_DRAWABLE (ruler)) {
		GdkRectangle area;
		GtkWidget *widget = GTK_WIDGET (ruler);

		area.x = 0;
		area.y = 0;
		area.width = widget->allocation.width;
		area.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);
	}
}
