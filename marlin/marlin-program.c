/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <marlin/marlin-program.h>
#include <marlin/marlin-marshal.h>

#include <marlin/marlin-jack-play.h>
#include <marlin/marlin-jack-record.h>

enum {
	NEW_VIEW,
	CLIPBOARD_CHANGED,
	JACK_OWNER_CHANGED,
	LAST_SIGNAL
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_PROGRAM_TYPE, MarlinProgramPrivate))
G_DEFINE_TYPE (MarlinProgram, marlin_program, G_TYPE_OBJECT);

static MarlinProgram *main_program = NULL;
static guint signals[LAST_SIGNAL];

struct _MarlinProgramPrivate {
	MarlinSample *clipboard;
	char *clipboard_src;

	jack_client_t *client;
	guint current_owner; /* The current owner of jack */
	gboolean locked; /* Has the owner locked it */

	guint owner_count; /* The number of owners */

	MarlinJackPlay *player;
	MarlinJackRecord *recorder;
};

static void
finalize (GObject *object)
{
	MarlinProgram *program;
	MarlinProgramPrivate *priv;

	program = MARLIN_PROGRAM (object);
	priv = program->priv;

	if (priv->client) {
		jack_client_close (priv->client);
		priv->client = NULL;
	}

	g_free (priv->clipboard_src);

	G_OBJECT_CLASS (marlin_program_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinProgram *program;
	MarlinProgramPrivate *priv;

	program = MARLIN_PROGRAM (object);
	priv = program->priv;

	if (priv->clipboard != NULL) {
 		g_object_unref (G_OBJECT (priv->clipboard));
		priv->clipboard = NULL;
	}

	if (priv->player != NULL) {
		g_object_unref (G_OBJECT (priv->player));
		priv->player = NULL;
	}

	if (priv->recorder != NULL) {
		g_object_unref (G_OBJECT (priv->recorder));
		priv->recorder = NULL;
	}

	G_OBJECT_CLASS (marlin_program_parent_class)->dispose (object);
}

static void
marlin_program_class_init (MarlinProgramClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	object_class->finalize = finalize;
	object_class->dispose = dispose;

	g_type_class_add_private (object_class, sizeof (MarlinProgramPrivate));

	signals[NEW_VIEW] = g_signal_new ("new-view",
					  G_TYPE_FROM_CLASS (klass),
					  G_SIGNAL_RUN_FIRST |
					  G_SIGNAL_NO_RECURSE,
					  G_STRUCT_OFFSET (MarlinProgramClass, new_view),
					  NULL, NULL,
					  marlin_marshal_VOID__OBJECT,
					  G_TYPE_NONE, 1,
					  G_TYPE_OBJECT);
	signals[CLIPBOARD_CHANGED] = g_signal_new ("clipboard-changed",
						   G_TYPE_FROM_CLASS (klass),
						   G_SIGNAL_RUN_FIRST |
						   G_SIGNAL_NO_RECURSE,
						   G_STRUCT_OFFSET (MarlinProgramClass, clipboard_changed),
						   NULL, NULL,
						   g_cclosure_marshal_VOID__VOID,
						   G_TYPE_NONE, 0);
	signals[JACK_OWNER_CHANGED] = g_signal_new ("jack-owner-changed",
						    G_TYPE_FROM_CLASS (klass),
						    G_SIGNAL_RUN_FIRST |
						    G_SIGNAL_NO_RECURSE,
						    G_STRUCT_OFFSET (MarlinProgramClass, jack_owner_changed),
						    NULL, NULL,
						    g_cclosure_marshal_VOID__UINT,
						    G_TYPE_NONE, 1,
						    G_TYPE_UINT);
}

static void
marlin_program_init (MarlinProgram *program)
{
	MarlinProgramPrivate *priv;
	jack_options_t options = JackNullOption;
	jack_status_t status;

	priv = program->priv = GET_PRIVATE (program);

	priv->client = jack_client_open ("marlin", options,
					 &status, NULL);
	if (priv->client == NULL) {
		g_warning ("Could not start Jack server");
	} else {
		priv->player = marlin_jack_play_new (priv->client);
		priv->recorder = marlin_jack_record_new (priv->client);
	}

	priv->current_owner = MARLIN_PROGRAM_JACK_UNOWNED;
	priv->owner_count = 0;
}

/**
 * marlin_program_get_default:
 *
 * Obtains the default MarlinProgram for this instance of Marlin.
 *
 * Return value: A pointer to MarlinProgram.
 */
MarlinProgram *
marlin_program_get_default (void)
{
	if (main_program != NULL) {
		return main_program;
	}

	main_program = g_object_new (MARLIN_PROGRAM_TYPE, NULL);
	return main_program;
}

/**
 * marlin_program_destroy_default:
 *
 * Frees the default program. Should only be called by Marlin.
 */
void
marlin_program_destroy_default (void)
{
	if (main_program == NULL) {
		return;
	}

	/* Should this be held in a mutex? */
	g_object_unref (main_program);
	main_program = NULL;
}

/**
 * marlin_program_get_clipboard:
 * @program:
 *
 * Obtains the clipboard associated with @program.
 *
 * Returns: A #MarlinSample.
 */
MarlinSample *
marlin_program_get_clipboard (MarlinProgram *program)
{
	return (MarlinSample *) program->priv->clipboard;
}

/**
 * marlin_program_set_clipboard:
 * @program:
 * @clipboard_object:
 * @src:
 *
 * Sets the clipboard of @program to @clipboard_object and sets the
 * source description to @src.
 */
void
marlin_program_set_clipboard (MarlinProgram *program,
			      MarlinSample *clipboard_object,
			      const char *src)
{
	if (program->priv->clipboard != NULL) {
		g_object_unref (program->priv->clipboard);
	}

	program->priv->clipboard = (MarlinSample *) clipboard_object;
	if (program->priv->clipboard != NULL) {
		g_object_ref (program->priv->clipboard);
	}

	g_object_set (clipboard_object,
		      "name", _("Clipboard"),
		      NULL);

	if (program->priv->clipboard_src != NULL) {
		g_free (program->priv->clipboard_src);
	}

	program->priv->clipboard_src = g_strdup (src);

	g_signal_emit (G_OBJECT (program), signals[CLIPBOARD_CHANGED], 0);
}

/**
 * marlin_program_get_clip_source:
 * @program:
 *
 * Obtains the source of the clipboard.
 * FIXME: Should this be combined with get_clipboard?
 */
const char *
marlin_program_get_clip_source (MarlinProgram *program)
{
	return program->priv->clipboard_src;
}

/* FIXME: Should these be moved into the clipboard-info dialog? */
/**
 * marlin_program_play_clipboard:
 * @program:
 *
 * Starts the clipboard playing.
 */
void
marlin_program_play_clipboard (MarlinProgram *program)
{
#if 0
	marlin_pipeline_set_state (MARLIN_PIPELINE (program->priv->pipeline),
				   GST_STATE_PLAYING);
#endif
}

/**
 * marlin_program_stop_clipboard:
 * @program:
 *
 * Stop the clipboard playing.
 */
void
marlin_program_stop_clipboard (MarlinProgram *program)
{
#if 0
	marlin_pipeline_set_state (MARLIN_PIPELINE (program->priv->pipeline),
				   GST_STATE_READY);
#endif
}

/**
 * marlin_program_request_new_view:
 * @program: A #MarlinProgram.
 * @sample: A #MarlinSample.
 *
 * Requests that a new view be created for @sample.
 */
void
marlin_program_request_new_view (MarlinProgram *program,
				 MarlinSample *sample)
{
	g_signal_emit (G_OBJECT (program), signals[NEW_VIEW], 0, sample);
}

/**
 * marlin_program_request_jack_owner_id
 */
guint
marlin_program_request_jack_owner_id (MarlinProgram *program)
{
	MarlinProgramPrivate *priv = program->priv;

	return (++priv->owner_count);
}

/**
 * marlin_program_acquire_jack
 * @program: A #MarlinProgram object
 * @owner_id: The owner ID that wishes to acquire the Jack objects
 * @locked: Whether the acquirement should be locked
 *
 * Requests that the Jack client be acquired by @owner_id. If the Jack client
 * is not busy, and the previous owner did not lock the client then @owner_id
 * will become the new jack owner and the jack-owner-changed signal will be
 * emitted.
 * If @locked is TRUE then no other owner will be able to steal ownership from
 * @owner_id until @owner_id releases the Jack client with
 * #marlin_program_release_jack.
 */
void
marlin_program_acquire_jack (MarlinProgram *program,
			     guint          owner_id,
			     gboolean       locked)
{
	MarlinProgramPrivate *priv = program->priv;

	if (priv->locked) {
		return;
	}

	if (owner_id == priv->current_owner) {
		return;
	}

	if (marlin_jack_is_busy ((MarlinJack *) priv->player) ||
	    marlin_jack_is_busy ((MarlinJack *) priv->recorder)) {
		/* Jack is busy so cannot be acquired */
		return;
	}

	priv->current_owner = owner_id;
	priv->locked = locked;
	g_signal_emit (program, signals[JACK_OWNER_CHANGED], 0, owner_id);
}

/**
 * marlin_program_release_jack:
 * @program: A #MarlinProgram object
 * @owner_id: The owner ID that wishes to release the Jack object
 *
 * Releases ownership of the Jack client, emitting the jack-owner-changed signal
 */
void
marlin_program_release_jack (MarlinProgram *program,
			     guint          owner_id)
{
	MarlinProgramPrivate *priv = program->priv;

	if (owner_id != priv->current_owner) {
		return;
	}

	priv->current_owner = MARLIN_PROGRAM_JACK_UNOWNED;
	priv->locked = FALSE;
	g_signal_emit (program, signals[JACK_OWNER_CHANGED], 0,
		       MARLIN_PROGRAM_JACK_UNOWNED);
}

/**
 * marlin_program_get_player:
 * @program: A #MarlinProgram object
 *
 * Obtains the jack player.
 *
 * Return value: A #MarlinJackPlay object that should be unreffed when finished
 * with.
 */
MarlinJack *
marlin_program_get_player (MarlinProgram *program)
{
	MarlinProgramPrivate *priv = program->priv;

	if (priv->player) {
		return (MarlinJack *) g_object_ref (priv->player);
	} else {
		return NULL;
	}
}

/**
 * marlin_program_get_recorder:
 * @program: A #MarlinProgram object
 *
 * Obtains the jack recorder.
 *
 * Return value: A #MarlinJackRecord object that should be unreffed when
 * finished with.
 */
MarlinJack *
marlin_program_get_recorder (MarlinProgram *program)
{
	MarlinProgramPrivate *priv = program->priv;

	if (priv->recorder) {
		return (MarlinJack *) g_object_ref (priv->recorder);
	} else {
		return NULL;
	}
}

/**
 * marlin_program_get_jack_owner:
 * @program: A #MarlinProgram object
 *
 * Obtains the owner ID of the current Jack owner
 *
 * Return value: A guint of the current Jack owner.
 */
guint
marlin_program_get_jack_owner (MarlinProgram *program)
{
	MarlinProgramPrivate *priv = program->priv;

	return priv->current_owner;
}
