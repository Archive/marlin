/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#include <glib/gi18n.h>

#include <gio/gio.h>

#include <gtk/gtk.h>

#include <gst/gst.h>

#include <marlin/marlin-stock.h>
#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-file-chooser.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-a11y-helper.h>

struct _ChooserOpenData {
	GtkWidget *chooser;

	GtkWidget *options_frame;
	GtkWidget *info_frame;

	/* Info widgets */
	GtkWidget *length;
	GtkWidget *sample_rate;
	GtkWidget *name;
	GtkWidget *channels;
	GtkWidget *mimetype;

	/* Transport controls */
	GtkWidget *play;
	GtkWidget *stop;

	/* Filesel hooks */
	GtkTreeSelection *selection;
	guint32 change_id;

	/* Preview */
	GstElement *player;
	GstElement *md_pipeline;
	GstElement *sink;
	GstPad *decode_pad;
	gboolean md_eos;
	gboolean can_decode;
};

static void
free_open_data (GtkFileChooser          *fc,
		struct _ChooserOpenData *od)
{
	if (od->player != NULL) {
		gst_element_set_state (od->player, GST_STATE_NULL);
		g_object_unref (G_OBJECT (od->player));
	}
	if (od->md_pipeline != NULL) {
		gst_element_set_state (od->md_pipeline, GST_STATE_NULL);
		g_object_unref (G_OBJECT (od->md_pipeline));
	}

	g_free (od);
}

static void G_GNUC_UNUSED
clear_info (struct _ChooserOpenData *od)
{
	char *markup;

	markup = g_strdup_printf ("<span weight=\"bold\">%s</span>",
				  _("Unknown"));
	gtk_label_set_markup (GTK_LABEL (od->name), markup);
	g_free (markup);

	gtk_label_set_text (GTK_LABEL (od->sample_rate), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (od->channels), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (od->length), _("Unknown"));
	gtk_label_set_text (GTK_LABEL (od->mimetype), _("Unknown"));
}

/* Constructs a string out of a taglist: If there are multiple entries for a
   tag it concatenates them together as "a, b, c" */
static char *
get_tag (GstTagList *tags,
	 const char *tag)
{
	int i, count;
	GString *builder;
	char *string;

	builder = g_string_new ("");

	count = gst_tag_list_get_tag_size (tags, tag);
	if (count == 0) {
		g_string_free (builder, TRUE);
		return NULL;
	}

	for (i = 0; i < count; i++) {
		char *str;

		if (i != 0) {
			g_string_append (builder, ", ");
		}

		if (gst_tag_get_type (tag) == G_TYPE_STRING) {
			if (gst_tag_list_get_string_index (tags, tag,
							   i, &str)) {
				g_string_append (builder, str);
				g_free (str);
			}
		} else {
			str = g_strdup_value_contents
				(gst_tag_list_get_value_index (tags, tag, i));
			g_string_append (builder, str);
			g_free (str);
		}
	}

	string = builder->str;
	g_string_free (builder, FALSE);

	return string;
}

static void
set_info (struct _ChooserOpenData *od,
	  GstTagList              *tags)
{
	char *str;

	str = get_tag (tags, GST_TAG_TITLE);
	if (str) {
		char *title;

		title = g_strdup_printf ("<span weight=\"bold\">%s</span>",
					 str);
		g_free (str);
		gtk_label_set_markup (GTK_LABEL (od->name), title);
		g_free (title);
	} else {
		char *fullpath, *filename, *title;

		fullpath = gtk_file_chooser_get_preview_filename (GTK_FILE_CHOOSER (od->chooser));
		filename = g_path_get_basename (fullpath);
		g_free (fullpath);

		title = g_strdup_printf ("<span weight=\"bold\">%s</span>",
					 filename);
		g_free (filename);
		gtk_label_set_markup (GTK_LABEL (od->name), title);
		g_free (title);
	}
}

static void
unknown_type_cb (GstElement              *decodebin,
		 GstPad                  *pad,
		 GstCaps                 *caps,
		 struct _ChooserOpenData *od)
{
	gtk_file_chooser_set_preview_widget_active (GTK_FILE_CHOOSER (od->chooser), FALSE);
}

static void
new_pad (GstElement              *decodebin,
	 GstPad                  *pad,
	 gboolean                 final,
	 struct _ChooserOpenData *od)
{
	GstCaps *caps;
	GstStructure *structure;
	const char *mimetype;

	od->decode_pad = pad;

	caps = gst_pad_get_caps (pad);

	/* We get "ANY" caps for text/plain files etc */
	if (gst_caps_is_empty (caps) || gst_caps_is_any (caps)) {
		od->can_decode = FALSE;
	} else {
		/* is this pad audio? */
		structure = gst_caps_get_structure (caps, 0);
		mimetype = gst_structure_get_name (structure);
		if (g_str_has_prefix (mimetype, "audio/x-raw")) {
			GstPad *sink_pad;

			/* Link this to the fakesink */
			sink_pad = gst_element_get_pad (od->sink, "sink");
			gst_pad_link (pad, sink_pad);
			od->can_decode = TRUE;
		}
	}

	gst_caps_unref (caps);
}

static void
get_stream_info (struct _ChooserOpenData *od)
{
	GstCaps *caps;
	GstStructure *st;
	GstQuery *query;
	int value;

	if (od->decode_pad == NULL) {
		return;
	}

	caps = gst_pad_get_caps (od->decode_pad);
	st = gst_caps_get_structure (caps, 0);

	if (gst_structure_get_int (st, "rate", &value)) {
		char *s = g_strdup_printf ("%d hz", value);
		gtk_label_set_text (GTK_LABEL (od->sample_rate), s);
		g_free (s);
	}

	if (gst_structure_get_int (st, "channels", &value)) {
		char *s = marlin_channels_to_string (value);
		gtk_label_set_text (GTK_LABEL (od->channels), s);
		g_free (s);
	}

	gst_caps_unref (caps);

	query = gst_query_new_duration (GST_FORMAT_TIME);
	if (gst_pad_query (od->decode_pad, query)) {
		gint64 duration;
		char *time_str;

		gst_query_parse_duration (query, NULL, &duration);
		time_str = marlin_ms_to_pretty_time (duration / 1000000, FALSE);
		gtk_label_set_text (GTK_LABEL (od->length), time_str);
		g_free (time_str);
	}

	gst_query_unref (query);
}

static gboolean
metadata_bus_handler (GstBus                  *bus,
		      GstMessage              *message,
		      struct _ChooserOpenData *od)
{
	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_EOS:
		od->md_eos = TRUE;
		clear_info (od);
		return TRUE;

	case GST_MESSAGE_ERROR:
	{
		GError *error;
		char *debug, *src;

		gst_message_parse_error (message, &error, &debug);
		src = gst_element_get_name (GST_MESSAGE_SRC (message));
		g_warning ("Error from %s: %s\n%s", src, error->message, debug);
		g_error_free (error);
		g_free (src);
		g_free (debug);

		clear_info (od);
		od->md_eos = TRUE;
		return TRUE;
	}

	case GST_MESSAGE_TAG:
	{
		GstTagList *tags;

		gst_message_parse_tag (message, &tags);
		if (!tags) {
			g_warning ("Could not retrieve tag list");
			od->md_eos = TRUE;
			return TRUE;
		}

		set_info (od, tags);
		gst_tag_list_free (tags);
		break;
	}

	default:
		break;
	}

	return FALSE;
}

static void
metadata_event_loop (struct _ChooserOpenData *od,
		     GstBus                  *bus)
{
	gboolean done = FALSE;

	while (!done && !od->md_eos) {
		GstMessage *message;

		message = gst_bus_pop (bus);
		if (message == NULL) {
			return;
		}

		done = metadata_bus_handler (bus, message, od);
		gst_message_unref (message);
	}
}

static void
get_metadata (struct _ChooserOpenData *od,
	      const char              *uri)
{
	GstElement *decodebin, *source;
	GstStateChangeReturn sret;
	int change_timeout;
	GstBus *bus;

	od->md_pipeline = gst_pipeline_new (NULL);
	od->md_eos = FALSE;

	source = marlin_make_gst_source ("source");
	decodebin = gst_element_factory_make ("decodebin", "decodebin");
	g_signal_connect (G_OBJECT (decodebin), "unknown-type",
			  G_CALLBACK (unknown_type_cb), od);
	g_signal_connect (G_OBJECT (decodebin), "new-decoded-pad",
			  G_CALLBACK (new_pad), od);

	od->sink = gst_element_factory_make ("fakesink", "fakesink");
	gst_bin_add_many (GST_BIN (od->md_pipeline), source,
			  decodebin, od->sink, NULL);
	gst_element_link (source, decodebin);

	g_object_set (G_OBJECT (source),
		      "location", uri,
		      NULL);

	bus = gst_element_get_bus (GST_ELEMENT (od->md_pipeline));
	sret = gst_element_set_state (od->md_pipeline, GST_STATE_PAUSED);
	change_timeout = 0;
	while (sret == GST_STATE_CHANGE_ASYNC &&
	       !od->md_eos && change_timeout < 5) {
		GstMessage *msg;

		msg = gst_bus_timed_pop (bus, 1 * GST_SECOND);
		if (msg) {
			metadata_bus_handler (bus, msg, od);
			gst_message_unref (msg);
			change_timeout = 0;
		} else {
			change_timeout++;
		}

		sret = gst_element_get_state (od->md_pipeline, NULL, NULL, 1);
	}

	metadata_event_loop (od, bus);
	gst_object_unref (bus);

	get_stream_info (od);
	if (od->can_decode) {
		GFile *file;
		GFileInfo *info;

		file = g_file_new_for_uri (uri);
		info = g_file_query_info (file,
					  G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
					  G_FILE_QUERY_INFO_NONE, NULL, NULL);
		if (info) {
			char *desc;
			const char *content_type;

			content_type = g_file_info_get_content_type (info);
			desc = g_content_type_get_description (content_type);

			gtk_label_set_text (GTK_LABEL (od->mimetype), desc);
			g_free (desc);
			g_object_unref (info);
		}
		g_object_unref (file);
	}

	gst_element_set_state (od->md_pipeline, GST_STATE_NULL);
	gst_object_unref (GST_OBJECT (od->md_pipeline));
	od->md_pipeline = NULL;
}

static void
selection_changed (GtkFileChooser          *chooser,
		   struct _ChooserOpenData *od)
{
	char *filename = gtk_file_chooser_get_preview_filename (chooser);

	/* Destroy the old pipeline */
	if (od->md_pipeline != NULL) {
		gst_element_set_state (od->md_pipeline, GST_STATE_NULL);
		g_object_unref (G_OBJECT (od->md_pipeline));
		od->md_pipeline = NULL;
	}

	if (g_file_test (filename, G_FILE_TEST_IS_DIR)) {
		gtk_file_chooser_set_preview_widget_active (GTK_FILE_CHOOSER (od->chooser), FALSE);
		g_free (filename);
		return;
	}

	if (filename != NULL) {
		char *uri;

		gtk_file_chooser_set_preview_widget_active (GTK_FILE_CHOOSER (od->chooser), TRUE);
		uri = gtk_file_chooser_get_preview_uri (GTK_FILE_CHOOSER (od->chooser));
		gtk_widget_set_sensitive (od->play, TRUE);
		get_metadata (od, uri);
		g_free (filename);
		g_free (uri);
	} else {
		gtk_file_chooser_set_preview_widget_active (GTK_FILE_CHOOSER (od->chooser), FALSE);
		gtk_widget_set_sensitive (od->play, FALSE);
		clear_info (od);
	}
}

static gboolean
bus_message_cb (GstBus     *bus,
		GstMessage *message,
		gpointer    data)
{
	struct _ChooserOpenData *od = data;

	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_EOS:
		gtk_widget_show (od->play);
		gtk_widget_hide (od->stop);
		break;

	default:
		break;
	}

	return TRUE;
}

static void
start_playing (GtkWidget               *button,
	       struct _ChooserOpenData *od)
{
	char *uri;

	/* Tear it down */
	if (od->player != NULL) {
		gst_element_set_state (od->player, GST_STATE_NULL);
		g_object_unref (G_OBJECT (od->player));
		od->player = NULL;
	}

	/* Build it up */
 	uri = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (od->chooser));

	od->player = gst_element_factory_make ("playbin", "player");
	gst_bus_add_watch (gst_pipeline_get_bus (GST_PIPELINE (od->player)),
			   bus_message_cb, od);

	g_object_set (G_OBJECT (od->player),
		      "uri", uri,
		      NULL);

	gst_element_set_state (od->player, GST_STATE_PLAYING);

	gtk_widget_hide (od->play);
	gtk_widget_show (od->stop);
}

static void
stop_playing (GtkWidget               *button,
	      struct _ChooserOpenData *od)
{
	gst_element_set_state (od->player, GST_STATE_READY);

	gtk_widget_hide (od->stop);
	gtk_widget_show (od->play);
}

static void
build_info_contents (struct _ChooserOpenData *od)
{
	GtkWidget *table, *label, *vbox, *hbox;

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_file_chooser_set_preview_widget (GTK_FILE_CHOOSER (od->chooser), vbox);

	od->name = marlin_make_title_label (_("Untitled"));
	gtk_misc_set_alignment (GTK_MISC (od->name), 0.5, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (od->name), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), od->name, FALSE, FALSE, 0);

	table = marlin_make_table (4, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = marlin_make_title_label (_("Mime Type:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 0, GTK_FILL);

	od->mimetype = marlin_make_info_label (_("Unknown"));
	PACK (table, od->mimetype, 1, 0, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     od->mimetype, ATK_RELATION_LABELLED_BY);

	label = marlin_make_title_label (_("Length:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 1, GTK_FILL);

	od->length = marlin_make_info_label (_("Unknown"));
	PACK (table, od->length, 1, 1, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     od->length, ATK_RELATION_LABELLED_BY);

	label = marlin_make_title_label (_("Sample Rate:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 2, GTK_FILL);

	od->sample_rate = marlin_make_info_label (_("Unknown"));
	PACK (table, od->sample_rate, 1, 2, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     od->sample_rate, ATK_RELATION_LABELLED_BY);

	label = marlin_make_title_label (_("Channels:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	PACK (table, label, 0, 3, GTK_FILL);

	od->channels = marlin_make_info_label (_("Unknown"));
	PACK (table, od->channels, 1, 3, GTK_FILL);

	marlin_add_paired_relations (label, ATK_RELATION_LABEL_FOR,
				     od->channels, ATK_RELATION_LABELLED_BY);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	od->play = marlin_make_button (_("Preview File"), MARLIN_STOCK_PLAY);
	gtk_widget_set_sensitive (od->play, FALSE);
	g_signal_connect (G_OBJECT (od->play), "clicked",
			  G_CALLBACK (start_playing), od);
	gtk_box_pack_start (GTK_BOX (hbox), od->play, FALSE, FALSE, 0);

	od->stop = marlin_make_button (_("Stop Preview"), MARLIN_STOCK_STOP);
	g_signal_connect (G_OBJECT (od->stop), "clicked",
			  G_CALLBACK (stop_playing), od);
	gtk_box_pack_start (GTK_BOX (hbox), od->stop, FALSE, FALSE, 0);

	gtk_widget_show_all (vbox);
	gtk_widget_hide (od->stop);
}

GtkWidget *
marlin_file_open_dialog_new (GtkWindow *parent_window)
{
	struct _ChooserOpenData *od;
	GtkWidget *widget;
	GtkFileFilter *filter;

	od = g_new0 (struct _ChooserOpenData, 1);

	widget = g_object_new (GTK_TYPE_FILE_CHOOSER_DIALOG,
			       "action", GTK_FILE_CHOOSER_ACTION_OPEN,
			       "title", _("Open File"),
			       "local-only", FALSE,
			       "use-preview-label", FALSE,
			       NULL);
	od->chooser = widget;

	build_info_contents (od);

	g_signal_connect (G_OBJECT (od->chooser), "destroy",
			  G_CALLBACK (free_open_data), od);
	g_signal_connect (G_OBJECT (od->chooser), "update-preview",
			  G_CALLBACK (selection_changed), od);

	gtk_dialog_add_buttons (GTK_DIALOG (widget),
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OPEN, GTK_RESPONSE_OK,
				NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (widget), GTK_RESPONSE_OK);

	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("All Files"));
	gtk_file_filter_add_pattern (filter, "*");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (widget), filter);

	return widget;
}

GtkWidget *
marlin_file_save_dialog_new (GtkWindow *parent_window)
{
	GtkWidget *widget;

	widget = g_object_new (GTK_TYPE_FILE_CHOOSER_DIALOG,
			       "action", GTK_FILE_CHOOSER_ACTION_SAVE,
			       "title", _("Save Sample As..."),
			       NULL);
	gtk_dialog_add_buttons (GTK_DIALOG (widget),
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_SAVE, GTK_RESPONSE_OK,
				NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (widget), GTK_RESPONSE_OK);
	gtk_window_set_default_size (GTK_WINDOW (widget), 600, 400);

	return widget;
}
