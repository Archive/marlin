/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_CROSS_FADER_H__
#define __MARLIN_CROSS_FADER_H__

#include <marlin/marlin-sample.h>
#include <marlin/marlin-grid-point.h>
#include <marlin/marlin-grid.h>

G_BEGIN_DECLS

#define MARLIN_CROSS_FADER_TYPE (marlin_cross_fader_get_type ())
#define MARLIN_CROSS_FADER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_CROSS_FADER_TYPE, MarlinCrossFader))
#define MARLIN_CROSS_FADER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_CROSS_FADER_TYPE, MarlinCrossFaderClass))

typedef struct _MarlinCrossFader MarlinCrossFader;
typedef struct _MarlinCrossFaderClass MarlinCrossFaderClass;
typedef struct _MarlinCrossFaderPrivate MarlinCrossFaderPrivate;

struct _MarlinCrossFader {
	MarlinGrid parent;

	MarlinGridPoint *src_in, *src_out;
	MarlinGridPoint *dest_in, *dest_out;

	MarlinCrossFaderPrivate *priv;
};

struct _MarlinCrossFaderClass {
	MarlinGridClass parent_class;
};

GType marlin_cross_fader_get_type (void);
void marlin_cross_fader_set_length (MarlinCrossFader *fader,
				    guint             ms);

void marlin_cross_fader_set_levels (MarlinCrossFader *fader,
				    float             src_in,
				    float             src_out,
				    float             dest_in,
				    float             dest_out);
void marlin_cross_fader_get_levels (MarlinCrossFader *fader,
				    float            *src_in,
				    float            *src_out,
				    float            *dest_in,
				    float            *dest_out);
G_END_DECLS

#endif
