/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gst/gst.h>
#include <gst/gconf/gconf.h>

#include <marlin-record-pipeline.h>
#include <marlin-marshal.h>
#include <marlin-sample.h>
#include <marlin-sample-element.h>
#include <marlin-mt.h>

enum {
	PROP_0,
	PROP_SAMPLE,
	PROP_POSITION,
	PROP_SAMPLE_RATE,
	PROP_CHANNELS,
	PROP_BIT_RATE
};

enum {
	LEVEL,
	LAST_SIGNAL
};

struct _MarlinRecordPipelinePrivate {
	MarlinSample *sample;
	MarlinSampleElementSink *sink;

	GstElement *src_bin, *src, *oneton, *audioconvert, *level;
	GstPad *srcpad;
	
	guint64 position;
	guint32 tick_id;
	
	/* Sample settings */
	guint rate;
	guint channels;
	guint bitrate;
};

static MarlinPipelineClass *parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

static void
finalize (GObject *object)
{
	MarlinRecordPipeline *pipeline;

	pipeline = MARLIN_RECORD_PIPELINE (object);

	if (pipeline->priv == NULL) {
		return;
	}

	if (pipeline->priv->sample != NULL) {
		g_object_unref (G_OBJECT (pipeline->priv->sample));
	}

	g_free (pipeline->priv);
	pipeline->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
connect_pipeline (MarlinRecordPipeline *pipeline)
{
	GstPad *src, *sink;
	GstCaps *filtercaps;
	gboolean ret;

	marlin_pipeline_set_state (MARLIN_PIPELINE (pipeline), GST_STATE_READY);
	src = gst_element_get_pad (pipeline->priv->src, "src");
	sink = gst_element_get_pad (pipeline->priv->level, "sink");

	filtercaps = gst_caps_new_simple ("audio/x-raw-int",
					  "channels", G_TYPE_INT, pipeline->priv->channels,
					  "rate", G_TYPE_INT, pipeline->priv->rate,
					  "signed", G_TYPE_BOOLEAN, TRUE,
					  "width", G_TYPE_INT, 16,
					  "depth", G_TYPE_INT, 16,
					  NULL);
	ret = gst_pad_relink_filtered (src, sink, filtercaps);
	gst_caps_free (filtercaps);

	g_assert (ret);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinRecordPipeline *pipeline;
	MarlinRecordPipelinePrivate *priv;

	pipeline = MARLIN_RECORD_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_POSITION:
/* 		priv->position = g_value_get_ulong (value); */
		break;

	case PROP_SAMPLE:
		if (priv->sample != NULL) {
			g_object_unref (G_OBJECT (priv->sample));
		}

		priv->sample = g_value_get_object (value);
		if (priv->sample == NULL) {
			g_object_set (G_OBJECT (priv->sink),
				      "sample", NULL,
				      NULL);

			/* Finished now */
			return;
		}
		
		g_object_ref (G_OBJECT (priv->sample));

		g_object_get (G_OBJECT (priv->sample),
			      "sample_rate", &priv->rate,
			      "channels", &priv->channels,
			      NULL);

		g_object_set (G_OBJECT (priv->sink),
			      "sample", priv->sample,
			      NULL);

		/* Connect the pipeline */
 		connect_pipeline (pipeline);
		
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinRecordPipeline *pipeline;
	MarlinRecordPipelinePrivate *priv;

	pipeline = MARLIN_RECORD_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_POSITION:
		g_value_set_ulong (value, priv->position);
		break;

	case PROP_SAMPLE:
		g_value_set_object (value, priv->sample);
		break;

	default:
		break;
	}
}

static void
class_init (GObjectClass *klass)
{
	klass->finalize = finalize;
	klass->set_property = set_property;
	klass->get_property = get_property;

	parent_class = g_type_class_peek_parent (klass);

	/* Properties */
	g_object_class_install_property (klass,
					 PROP_POSITION,
					 g_param_spec_ulong ("position",
							     "Position",
							     "Position of record",
							     0, G_MAXULONG, 0,
							     G_PARAM_READABLE));

	g_object_class_install_property (klass,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "Sample",
							      "The sample to be recorded into",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));
	signals[LEVEL] = g_signal_new ("level", 
				       G_TYPE_FROM_CLASS (klass),
				       G_SIGNAL_RUN_LAST,
				       G_STRUCT_OFFSET (MarlinRecordPipelineClass, level),
				       NULL, NULL,
				       marlin_marshal_VOID__DOUBLE_INT_DOUBLE_DOUBLE_DOUBLE,
				       G_TYPE_NONE, 5,
				       G_TYPE_DOUBLE,
				       G_TYPE_INT,
				       G_TYPE_DOUBLE,
				       G_TYPE_DOUBLE,
				       G_TYPE_DOUBLE);
}

static void
oneton_new_pad (GstElement *oneton,
		GstPad *src,
		MarlinRecordPipeline *pipeline)
{
	GstPad *sink, *qsrc, *qsink;
	GstElement *q;
	const char *name;
	char *qname;

	/* Request a new pad */
	sink = gst_element_get_request_pad (GST_ELEMENT (pipeline->priv->sink), "sink%d");
	g_assert (sink != NULL);

	name = gst_pad_get_name (src);
	qname = g_strdup_printf ("queue.%s", name);
	q = gst_element_factory_make ("queue", qname);
	g_free (qname);

	marlin_pipeline_add (MARLIN_PIPELINE (pipeline), q);

	qsrc = gst_element_get_pad (q, "src");
	qsink = gst_element_get_pad (q, "sink");

	if (!gst_pad_link (src, qsink)) {
		g_warning ("Could not connect q to source");
	}

	if (!gst_pad_link (qsrc, sink)) {
		g_warning ("Could not connect q to sink");
	}

	/* The pipeline is already at PLAYING, so we need to
	   sync the state to get the queue up to speed */
	gst_element_sync_state_with_parent (q);
}

#define DEFAULT_BITRATE 16;

static void
record_eos_reached (GstElement *element,
		    MarlinRecordPipeline *pipeline)
{
	MarlinOperation *operation;

	g_object_get (G_OBJECT (pipeline),
		      "operation", &operation,
		      NULL);

#if 0
	marlin_sample_ensure_mmapped (pipeline->priv->sample, operation, NULL);
	marlin_sample_generate_peaks (pipeline->priv->sample, operation, NULL);
#endif
	marlin_pipeline_set_eos (MARLIN_PIPELINE (pipeline));
}

static GstElement *
get_real_src (GstElement *element)
{
	GList *elements = NULL;
	const GList *pads = NULL;

	if (!GST_IS_BIN (element)) {
		return element;
	}

	elements = (GList *) gst_bin_get_list (GST_BIN (element));
	while (elements) {
		element = GST_ELEMENT (elements->data);

		if (GST_IS_BIN (element)) {
			element = get_real_src (element);

			if (GST_IS_ELEMENT (element)) {
				return element;
			}
		} else {
			pads = gst_element_get_pad_list (element);

			while (pads) {
				if (GST_PAD_DIRECTION (GST_PAD (pads->data)) == GST_PAD_SRC) {
					return element;
				}

				pads = pads->next;
			}
		}

		elements = elements->next;
	}

	return NULL;
}

/* Level multithread stuff */
struct _level_msg {
	MarlinMTMsg msg;
	
	MarlinPipeline *pipeline;
	double time, rms, peak, decay;
	int channel;
};

static void
do_level (MarlinMTMsg *mm)
{
	struct _level_msg *lm = (struct _level_msg *) mm;

	g_signal_emit (lm->pipeline, signals[LEVEL], 0,
		       lm->time, lm->channel, lm->rms, lm->peak, lm->decay);
}

static MarlinMTMsgOp level_op = {
	NULL,
	do_level,
	NULL,
	NULL
};

static void
level_cb (GstElement *level,
	  double time,
	  int channel,
	  double rms,
	  double peak,
	  double decay,
	  MarlinRecordPipeline *pipeline)
{
	struct _level_msg *m;

	m = marlin_mt_msg_new (&level_op, NULL, sizeof (*m));
	m->pipeline = (MarlinPipeline *) pipeline;
	m->time = time;
	m->channel = channel;
	m->rms = rms;
	m->peak = peak;
	m->decay = decay;

	marlin_msgport_put (marlin_gui_port, (MarlinMsg *) m);
}

static void
init (MarlinRecordPipeline *pipeline)
{
	MarlinPipeline *p;
	GstElement *sink_bin;
	GstCaps *filtercaps;
	gboolean ret;
	
	p = MARLIN_PIPELINE (pipeline);
	pipeline->priv = g_new0 (MarlinRecordPipelinePrivate, 1);

	/* Make pipeline */
 	pipeline->priv->src_bin = gst_gconf_get_default_audio_src ();
	
	pipeline->priv->src = get_real_src (pipeline->priv->src_bin);
	pipeline->priv->srcpad = gst_element_get_pad (pipeline->priv->src, "src");	

	pipeline->priv->level = gst_element_factory_make ("level", 
							  "record-level");
	g_object_set (G_OBJECT (pipeline->priv->level),
		      "signal", TRUE,
		      "interval", 0.1,
		      NULL);
	g_signal_connect (pipeline->priv->level, "level",
			  G_CALLBACK (level_cb), pipeline);

	pipeline->priv->audioconvert = gst_element_factory_make ("audioconvert",
								 "record-convert");

	pipeline->priv->oneton = gst_element_factory_make ("deinterleave", 
							   "record-oneton");
	g_assert (pipeline->priv->oneton != NULL);

	g_signal_connect (G_OBJECT (pipeline->priv->oneton), "new-pad",
			  G_CALLBACK (oneton_new_pad), pipeline);

	sink_bin = gst_thread_new ("record-sink-thread");
	pipeline->priv->sink = marlin_sample_element_sink_new (NULL);
	g_signal_connect (G_OBJECT (pipeline->priv->sink), "eos",
			  G_CALLBACK (record_eos_reached), pipeline);
	gst_bin_add ((GstBin *) sink_bin, GST_ELEMENT (pipeline->priv->sink));

	marlin_pipeline_add (p, pipeline->priv->src_bin);
	marlin_pipeline_add (p, pipeline->priv->level);
	marlin_pipeline_add (p, GST_ELEMENT (pipeline->priv->audioconvert));
	marlin_pipeline_add (p, pipeline->priv->oneton);
	marlin_pipeline_add (p, sink_bin);
	
	/* Filter the connection between audioconvert and deinterleave
	   to be float */
	filtercaps = gst_caps_new_simple ("audio/x-raw-float", NULL);
	gst_element_link_filtered (pipeline->priv->audioconvert,
				   pipeline->priv->oneton, filtercaps);
	gst_caps_free (filtercaps);

	ret = gst_element_link (pipeline->priv->level, pipeline->priv->audioconvert);
	g_assert (ret);


	filtercaps = gst_caps_new_simple ("audio/x-raw-int",
					  "channels", G_TYPE_INT, 2,
					  "rate", G_TYPE_INT, 48000,
					  "signed", G_TYPE_BOOLEAN, TRUE,
					  "width", G_TYPE_INT, 16,
					  "depth", G_TYPE_INT, 16,
					  NULL);
	ret = gst_pad_link_filtered (pipeline->priv->srcpad, 
				     gst_element_get_pad (pipeline->priv->level, "sink"),
				     filtercaps);
	gst_caps_free (filtercaps);

	g_assert (ret);

	pipeline->priv->bitrate = DEFAULT_BITRATE;
}

GType
marlin_record_pipeline_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static GTypeInfo info = {
			sizeof (MarlinRecordPipelineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinRecordPipeline), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (MARLIN_PIPELINE_TYPE,
					       "MarlinRecordPipeline",
					       &info, 0);
	}

	return type;
}

MarlinRecordPipeline *
marlin_record_pipeline_new (void)
{
	return g_object_new (MARLIN_RECORD_PIPELINE_TYPE, NULL);
}

MarlinRecordPipeline *
marlin_record_pipeline_new_with_sample (MarlinSample *sample)
{
	return g_object_new (MARLIN_RECORD_PIPELINE_TYPE,
			     "sample", sample,
			     NULL);
}

void
marlin_record_pipeline_set_eos (MarlinRecordPipeline *pipeline)
{
	GstEvent *event;
	
	g_return_if_fail (IS_MARLIN_RECORD_PIPELINE (pipeline));

	event = gst_event_new (GST_EVENT_EOS);
	if (gst_element_send_event (pipeline->priv->src, event) == FALSE) {
		g_warning ("EOS send failed");
	}
}
