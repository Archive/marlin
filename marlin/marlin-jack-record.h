/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_JACK_RECORD_H__
#define __MARLIN_JACK_RECORD_H__

#include <glib-object.h>

#include <marlin/marlin-jack.h>

G_BEGIN_DECLS

#define MARLIN_JACK_RECORD_TYPE (marlin_jack_record_get_type ())
#define MARLIN_JACK_RECORD(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_JACK_RECORD_TYPE, MarlinJackRecord))
#define MARLIN_JACK_RECORD_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_JACK_RECORD_TYPE, MarlinJackRecordClass))

typedef struct _MarlinJackRecord MarlinJackRecord;
typedef struct _MarlinJackRecordPrivate MarlinJackRecordPrivate;
typedef struct _MarlinJackRecordClass MarlinJackRecordClass;

struct _MarlinJackRecord {
	MarlinJack parent;

	MarlinJackRecordPrivate *priv;
};

struct _MarlinJackRecordClass {
	MarlinJackClass parent_class;

	/* Signals */
	void (*record_finished) (MarlinJack *jack);
};

GType marlin_jack_record_get_type (void);
MarlinJackRecord *marlin_jack_record_new (jack_client_t *client);

G_END_DECLS

#endif
