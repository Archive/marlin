/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2005-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifndef __MARLIN_BASE_WINDOW_H__
#define __MARLIN_BASE_WINDOW_H__

#include <glib-object.h>

#include <marlin/marlin-sample.h>
#include <marlin/marlin-marker-view.h>
#include <marlin/marlin-sample-view.h>
#include <marlin/marlin-undo-manager.h>

G_BEGIN_DECLS

#define MARLIN_BASE_WINDOW_TYPE (marlin_base_window_get_type ())
#define MARLIN_BASE_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_BASE_WINDOW_TYPE, MarlinBaseWindow))
#define MARLIN_BASE_WINDOW_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST ((vtable), MARLIN_BASE_WINDOW_TYPE, MarlinBaseWindowClass))
#define MARLIN_IS_BASE_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_BASE_WINDOW_TYPE))
#define MARLIN_IS_BASE_WINDOW_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE ((vtable), MARLIN_BASE_WINDOW_TYPE))
#define MARLIN_BASE_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), MARLIN_BASE_WINDOW_TYPE, MarlinBaseWindowClass))

typedef struct _MarlinBaseWindow MarlinBaseWindow;
typedef struct _MarlinBaseWindowClass MarlinBaseWindowClass;

struct _MarlinBaseWindowClass {
	GTypeInterface base_iface;

	/* vtable */
	MarlinSample *(* get_sample) (MarlinBaseWindow *base);
	guint64 (* get_position) (MarlinBaseWindow *base);
	void (* set_position) (MarlinBaseWindow *base,
			       guint64 position);
	MarlinMarkerView *(* get_marker_view) (MarlinBaseWindow *base);
	MarlinSampleView *(* get_sample_view) (MarlinBaseWindow *base);
	MarlinUndoManager *(* get_undo_manager) (MarlinBaseWindow *base);
};

GType marlin_base_window_get_type (void) G_GNUC_CONST;
MarlinSample *marlin_base_window_get_sample (MarlinBaseWindow *base);
guint64 marlin_base_window_get_position (MarlinBaseWindow *base);
void marlin_base_window_set_position (MarlinBaseWindow *base,
				      guint64 position);
MarlinMarkerView *marlin_base_window_get_marker_view (MarlinBaseWindow *base);
MarlinSampleView *marlin_base_window_get_sample_view (MarlinBaseWindow *base);
MarlinUndoManager *marlin_base_window_get_undo_manager (MarlinBaseWindow *base);

G_END_DECLS

#endif
