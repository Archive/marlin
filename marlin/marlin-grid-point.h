/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_GRID_POINT_H__
#define __MARLIN_GRID_POINT_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MARLIN_GRID_POINT_TYPE (marlin_grid_point_get_type ())
#define MARLIN_GRID_POINT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_GRID_POINT_TYPE, MarlinGridPoint))
#define MARLIN_GRID_POINT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_GRID_POINT_TYPE, MarlinGridPointClass))

typedef struct _MarlinGridPointPrivate MarlinGridPointPrivate;

#define MARLIN_GRID_POINT_FIXED 0
#define MARLIN_GRID_POINT_FREE_X (1 << 0)
#define MARLIN_GRID_POINT_FREE_Y (1 << 1)

#define MARLIN_GRID_POINT_FREE (MARLIN_GRID_POINT_FREE_X | MARLIN_GRID_POINT_FREE_Y)

typedef struct _MarlinGridPoint {
	GObject parent;

	guint64 x;
	double level;
	
	MarlinGridPointPrivate *priv;
} MarlinGridPoint;

typedef struct _MarlinGridPointClass {
	GObjectClass parent_class;

	void (*moved) (MarlinGridPoint *point);
} MarlinGridPointClass;

GType marlin_grid_point_get_type (void);

MarlinGridPoint *marlin_grid_point_new (guint64 x,
					float   level);

void marlin_grid_point_set (MarlinGridPoint *point,
			    guint64          x,
			    float            level);
void marlin_grid_point_set_slave (MarlinGridPoint *master,
				  MarlinGridPoint *slave);


G_END_DECLS

#endif
