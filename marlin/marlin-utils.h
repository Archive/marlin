/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_UTILS_H__
#define __MARLIN_UTILS_H__

#include <glib.h>

#include <gconf/gconf-client.h>

#define MARLIN_DIALOG_BORDER_WIDTH 12
#define MARLIN_INFINITE_DB -96.0
#define MARLIN_INFINITY_TEXT "\xe2\x88\x9e"
#define MARLIN_INFINITY_DB_TEXT "\xe2\x88\x9e dB"

#define MARLIN_MAX_DB 0.0
#define MARLIN_MIN_DB -96.0

#define MARLIN_DEFAULT_BPM 120
#define MARLIN_DEFAULT_RATE 48000

guint64 marlin_ms_to_frames (guint64 ms,
			     guint rate);
guint64 marlin_frames_to_ms (guint64 frames,
			     guint rate);
char *marlin_ms_to_time_string (guint64 ms);
char *marlin_ms_to_time_frame_string (guint64 ms, guint rate);
char *marlin_ms_to_pretty_time (guint64 ms,
				gboolean include_milli);
guint64 marlin_time_string_to_ms (const char *str);
guint64 marlin_time_frame_string_to_ms (const char *str);

double marlin_db_to_ratio (double db);
double marlin_ratio_to_db (double db);
double marlin_db_to_percent (double db);
double marlin_percent_to_db (double percentage);

int marlin_gconf_get_int (const char *key);
void marlin_gconf_set_int (const char *key,
			   int value);
float marlin_gconf_get_float (const char *key);
float marlin_gconf_get_float_with_default (const char *key,
					   float       def);

void marlin_gconf_set_float (const char *key,
			     float value);
char *marlin_gconf_get_string (const char *key);
void marlin_gconf_set_string (const char *key,
			      const char *value);
gboolean marlin_gconf_get_bool (const char *key);

GConfClient *marlin_gconf_get_default (void);
void marlin_gconf_destroy_default (void);

char *marlin_channels_to_string (guint channels);

#endif
