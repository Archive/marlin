/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_BLOCK_H__
#define __MARLIN_BLOCK_H__

#include <sys/types.h>

#include <glib.h>

#include <marlin/marlin-types.h>
#include <marlin/marlin-read-write-lock.h>
#include <marlin/marlin-file.h>

#define MARLIN_FRAMES_PER_PEAK 128
#define MARLIN_PEAKS_PER_BLOCK 4096

/* 524288 */
#define MARLIN_BLOCK_SIZE (MARLIN_PEAKS_PER_BLOCK * MARLIN_FRAMES_PER_PEAK)
#define MARLIN_BLOCK_SIZE_BYTES (MARLIN_BLOCK_SIZE * sizeof (float))

#define MARLIN_FRAME_IN_BLOCK(block, frame) (((frame) >= (block)->start) && ((frame) <= (block)->end))

struct _MarlinBlock {
	MarlinReadWriteLock *lock; /* Lock, do not access any data
				      without first obtaining the lock */

	MarlinChannel *channel; /* Owner channel */

	struct _MarlinBlock *previous; /* A linked list of blocks */
	struct _MarlinBlock *next;

	guint64 start, end; /* The start and finish of this block inclusive */
	guint64 num_frames, num_peaks;

	/* Is the data mapped? */
	gboolean is_mapped;
	/* Can the data be unmapped? */
	gboolean unmappable;

	/* frame_region and peak_region will not be
	   valid unless is_mapped == TRUE */
	MarlinFileRegion *frame_region, *peak_region;
	MarlinFile *frame_file, *peak_file;

	off_t frame_offset, peak_offset; /* Offsets into the MarlinFiles */
};

MarlinBlock *marlin_block_new (MarlinChannel *channel,
			       MarlinFile *frame_file,
			       MarlinFile *peak_file);
void marlin_block_free (MarlinBlock *block);

void marlin_block_append (MarlinBlock *a,
			  MarlinBlock *b);

MarlinBlock *marlin_block_next (MarlinBlock *block);
MarlinBlock *marlin_block_previous (MarlinBlock *block);
MarlinBlock *marlin_block_last (MarlinBlock *block);

gboolean marlin_block_set_data (MarlinBlock *block,
				gpointer data,
				guint64 num_frames,
				GError **error);
gboolean marlin_block_map (MarlinBlock *block,
			   GError **error);
void marlin_block_unmap (MarlinBlock *block);

float *marlin_block_get_frame_data (MarlinBlock *block);
MarlinPeak *marlin_block_get_peak_data (MarlinBlock *block);

MarlinBlock *marlin_block_split (MarlinBlock *block,
				 guint64 split_frame);
MarlinBlock *marlin_block_copy (MarlinBlock *block);

MarlinBlock *marlin_block_copy_list (MarlinBlock *first,
				     guint64 *count);
void marlin_block_free_list (MarlinBlock *block);

void marlin_block_move_blocks_list (MarlinBlock *block,
			       MarlinChannel *new_channel);
guint64 marlin_block_recalculate_ranges (MarlinBlock *block);

guint marlin_block_get_buffer (MarlinBlock  *block,
			       float        *buffer,
			       guint64       start,
			       guint         frames_needed,
			       MarlinBlock **end_block);

#endif
