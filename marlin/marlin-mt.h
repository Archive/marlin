/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *  Based on mail-mt.h: Written by Michael Zucchi <notzed@ximian.com>
 *  Copyright (C) 2000 - 2002 Ximian, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_MT_H__
#define __MARLIN_MT_H__

#include <pthread.h>
#include <marlin-msgport.h>

typedef struct _MarlinMTMsgOp MarlinMTMsgOp;

typedef struct _MarlinMTMsg {
	MarlinMsg msg; /* parent type */
	MarlinMTMsgOp *ops; /* Operation functions */
	guint seq; /* Sequence number for synchronisation */
} MarlinMTMsg;

struct _MarlinMTMsgOp {
	char * (*describe_msg) (MarlinMTMsg *msg, int complete);
	void (*receive_msg) (MarlinMTMsg *msg);
	void (*reply_msg) (MarlinMTMsg *msg);
	void (*destroy_msg) (MarlinMTMsg *msg);
};

/* setup ports */
void marlin_msg_initialise (void);

/* allocate a new message */
void *marlin_mt_msg_new (MarlinMTMsgOp *ops,
			 MarlinMsgPort *reply_port,
			 size_t size);
void marlin_mt_msg_free (void *msg);
void marlin_mt_msg_check_error (void *msg);
void marlin_mt_msg_wait (guint msgid);

void marlin_mt_initialise (void);

extern MarlinMsgPort *marlin_gui_port;
extern MarlinMsgPort *marlin_gui_reply_port;

extern MarlinThread *marlin_mt_thread_queued;
extern MarlinThread *marlin_mt_thread_new;

extern pthread_t marlin_gui_thread;

#endif
