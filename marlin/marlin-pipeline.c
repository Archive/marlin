/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gst/gst.h>

#include <gconf/gconf-client.h>

#include <marlin/marlin-pipeline.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-mt.h>

enum {
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_OPERATION,
};

struct _MarlinPipelinePrivate {
	MarlinOperation *operation;

	guint32 position_tracker;
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_PIPELINE_TYPE, MarlinPipelinePrivate))
G_DEFINE_TYPE (MarlinPipeline, marlin_pipeline, GST_TYPE_PIPELINE);

static void
finalize (GObject *object)
{
	MarlinPipeline *pipeline;
	MarlinPipelinePrivate *priv;

	pipeline = MARLIN_PIPELINE (object);
	priv = pipeline->priv;

	G_OBJECT_CLASS (marlin_pipeline_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinPipeline *pipeline;
	MarlinPipelinePrivate *priv;

	pipeline = MARLIN_PIPELINE (object);
	priv = pipeline->priv;

	if (priv->position_tracker > 0) {
		g_source_remove (priv->position_tracker);
		priv->position_tracker = 0;
	}

	if (priv->operation) {
		g_object_unref (G_OBJECT (priv->operation));
		priv->operation = NULL;
	}

	G_OBJECT_CLASS (marlin_pipeline_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinPipeline *pipeline;
	MarlinPipelinePrivate *priv;

	pipeline = MARLIN_PIPELINE (object);
	priv = pipeline->priv;

	switch (prop_id) {
	case PROP_OPERATION:
		if (priv->operation) {
			g_object_unref (G_OBJECT (priv->operation));
		}

		priv->operation = MARLIN_OPERATION (g_value_dup_object (value));
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinPipeline *pipeline = MARLIN_PIPELINE (object);

	switch (prop_id) {
	case PROP_OPERATION:
		g_value_set_object (value, pipeline->priv->operation);
		break;

	default:
		break;
	}
}

static void
marlin_pipeline_class_init (MarlinPipelineClass *klass)
{
	GObjectClass *o_class;

	o_class = (GObjectClass *) klass;

	o_class->finalize = finalize;
	o_class->dispose = dispose;
	o_class->set_property = set_property;
	o_class->get_property = get_property;

	g_type_class_add_private (klass, sizeof (MarlinPipelinePrivate));

	g_object_class_install_property (o_class,
					 PROP_OPERATION,
					 g_param_spec_object ("operation",
							      "Operation",
							      "The operation this pipeline performs",
							      MARLIN_OPERATION_TYPE,
							      G_PARAM_READWRITE));
}

static gboolean
position_tracker (gpointer userdata)
{
	MarlinPipeline *pipeline;
	MarlinPipelineClass *p_class;

	pipeline = MARLIN_PIPELINE (userdata);
	p_class = MARLIN_PIPELINE_GET_CLASS (userdata);

	if (p_class->report_position) {
		p_class->report_position (pipeline);
	}

	return TRUE;
}

static void
bus_message_cb (GstBus     *bus,
		GstMessage *message,
		gpointer    data)
{
	MarlinPipeline *pipeline;
	MarlinPipelinePrivate *priv;

	pipeline = MARLIN_PIPELINE (data);
	priv = pipeline->priv;

	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_EOS:
		if (GST_MESSAGE_SRC (message) == (GstObject *) pipeline) {
			if (priv->position_tracker > 0) {
				g_source_remove (priv->position_tracker);
				priv->position_tracker = 0;
			}

			if (priv->operation) {
				marlin_operation_finish (priv->operation);
			}
		}
		break;

/*  	case GST_MESSAGE_WARNING: */
	case GST_MESSAGE_ERROR: {
		GError *error;
		char *debug;

		if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ERROR) {
			gst_message_parse_error (message, &error, &debug);
		} else {
			gst_message_parse_warning (message, &error, &debug);
		}

		if (priv->operation) {
			marlin_operation_set_error (priv->operation, error,
						    debug);
		}
		g_error_free (error);
		g_free (debug);
		break;
	}

	case GST_MESSAGE_STATE_CHANGED: {
		GstState old, new, pending;

		if (GST_MESSAGE_SRC (message) != (GstObject *) pipeline) {
			return;
		}

		gst_message_parse_state_changed (message, &old, &new, &pending);
		if (old == GST_STATE_NULL &&
		    new == GST_STATE_READY) {
			if (priv->operation) {
				marlin_operation_start (priv->operation);
			}
		} else if (old == GST_STATE_READY &&
			   new == GST_STATE_PAUSED) {
			/* Start the position tracker */
			priv->position_tracker = g_timeout_add
				(200, position_tracker, pipeline);
		} else if (old == GST_STATE_PAUSED &&
			   new == GST_STATE_READY) {
			/* Stop the position tracker */
			if (priv->position_tracker > 0) {
				g_source_remove (priv->position_tracker);
				priv->position_tracker = 0;
			}
		}
		break;
	}

	default:
		break;
	}

	return;
}

static void
marlin_pipeline_init (MarlinPipeline *pipeline)
{
	GstBus *bus;
	pipeline->priv = GET_PRIVATE (pipeline);

	pipeline->priv->operation = NULL;

	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	gst_bus_add_signal_watch (bus);
	g_signal_connect (bus, "message",
			  G_CALLBACK (bus_message_cb), pipeline);
}

/**
 * marlin_pipeline_get_operation:
 * @pipeline: A #MarlinPipeline.
 *
 * Retrieves the operation associated with @pipeline.
 *
 * Returns: A #MarlinOperation that is not to be unreffed after use.
 */
MarlinOperation *
marlin_pipeline_get_operation (MarlinPipeline *pipeline)
{
	return pipeline->priv->operation;
}
