/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public 
 *  License as published by the Free Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_FILE_SELECTION_H__
#define __MARLIN_FIlE_SELECTION_H__

#include <gtk/gtkfilechooserdialog.h>

#define MARLIN_FILE_SELECTION_TYPE (marlin_file_selection_get_type ())
#define MARLIN_FILE_SELECTION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARLIN_FILE_SELECTION_TYPE, MarlinFileSelection))
#define MARLIN_FILE_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_FILE_SELECTION_TYPE, MarlinFileSelectionClass))
#define IS_MARLIN_FILE_SELECTION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARLIN_FILE_SELECTION_TYPE))
#define IS_MARLIN_FILE_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MARLIN_FILE_SELECTION_TYPE))

typedef struct _MarlinFileSelection MarlinFileSelection;
typedef struct _MarlinFileSelectionClass MarlinFileSelectionClass;
typedef struct _MarlinFileSelectionPrivate MarlinFileSelectionPrivate;

struct _MarlinFileSelection {
	GtkFileChooserDialog parent;

	MarlinFileSelectionPrivate *priv;
};

struct _MarlinFileSelectionClass {
	GtkFileChooserDialogClass parent_class;
};

GType marlin_file_selection_get_type (void);
MarlinFileSelection *marlin_file_selection_new (gboolean show_options,
						gboolean show_info);

#endif
