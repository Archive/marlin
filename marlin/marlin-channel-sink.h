/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_CHANNEL_SINK_H__
#define __MARLIN_CHANNEL_SINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

#define MARLIN_CHANNEL_SINK_TYPE (marlin_channel_sink_get_type ())
#define MARLIN_CHANNEL_SINK(obj) (G_TYPE_CHECK_INSTANCE_CAST  ((obj), MARLIN_CHANNEL_SINK_TYPE, MarlinChannelSink))
#define MARLIN_CHANNEL_SINK_CLASS (klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MARLIN_CHANNEL_SINK_TYPE, MarlinChannelSinkClass))

typedef struct _MarlinChannelSink MarlinChannelSink;
typedef struct _MarlinChannelSinkPrivate MarlinChannelSinkPrivate;
typedef struct _MarlinChannelSinkClass MarlinChannelSinkClass;

struct _MarlinChannelSink {
	GstBaseSink element;

	MarlinChannelSinkPrivate *priv;

	guint64 frame_count;

	gboolean no_space; /* Are we out of space on the drive? */
	GError *no_space_error; /* GError associated with the no space error */
	guint64 no_space_frames; /* Frame count when error occurred */
};

struct _MarlinChannelSinkClass {
	GstBaseSinkClass parent_class;
};

GType marlin_channel_sink_get_type (void);

#endif
