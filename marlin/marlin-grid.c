/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003-2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include <glib.h>

#include <marlin/marlin-cursors.h>
#include <marlin/marlin-grid.h>
#include <marlin/marlin-utils.h>

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_GRID_TYPE, MarlinGridPrivate))

#define LEGEND_SPACING 19
#define HALF_POINT_WIDTH 6
#define POINT_WIDTH 12

#define BETWEEN(x, a, b) ((x) >= (a) && (x) <= (b))

#define FRAME_TO_PIXEL(f) (((f) - priv->start) / priv->frames_per_pixel)
#define PIXEL_TO_FRAME(p) (((guint64) (p) * (guint64) priv->frames_per_pixel) + priv->start)

#define LEVEL_TO_PIXEL(l) (priv->box_height - ((l) * priv->box_height))
#define PIXEL_TO_LEVEL(p) (((double) priv->box_height - (double) (p)) / (double) priv->box_height)

typedef struct _MarlinCurvePoint {
	MarlinListNode list;

	MarlinGridPoint *point;
	gboolean selected;
	guint32 movability;
} MarlinCurvePoint;

struct _MarlinGridPrivate {
	guint64 start;
	guint64 finish;

	int frames_per_pixel;

	int box_x;
	int box_y;
	int box_width;
	int box_height;

	cairo_font_options_t *options;

	GPtrArray *curves;

	MarlinGridCurve *selected_curve;
	MarlinCurvePoint *selected_point;
	gboolean in_grab;
};

G_DEFINE_TYPE (MarlinGrid, marlin_grid, GTK_TYPE_DRAWING_AREA);

static void
finalize (GObject *object)
{
	MarlinGrid *grid = (MarlinGrid *) object;
	MarlinGridPrivate *priv = grid->priv;

	if (priv->curves) {
		int i;

		for (i = 0; i < priv->curves->len; i++) {
			marlin_grid_curve_free (priv->curves->pdata[i]);
		}
		g_ptr_array_free (priv->curves, TRUE);
	}

	G_OBJECT_CLASS (marlin_grid_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	G_OBJECT_CLASS (marlin_grid_parent_class)->dispose (object);
}

static void
redraw_grid (MarlinGrid *grid)
{
	GtkWidget *widget = (GtkWidget *) grid;
	MarlinGridPrivate *priv = grid->priv;

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle rect;

		rect.x = priv->box_x;
		rect.y = priv->box_y;
		rect.width = priv->box_width;
		rect.height = priv->box_height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}
}

static void
size_allocate (GtkWidget     *widget,
	       GtkAllocation *allocation)
{
	MarlinGrid *grid = (MarlinGrid *) widget;
	MarlinGridPrivate *priv = grid->priv;
	guint64 length;

	if (GTK_WIDGET_REALIZED (widget)) {
		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);
	}

	length = priv->finish - priv->start;
	priv->frames_per_pixel = (int) ceil (((double)length) / (allocation->width - 38));

	GTK_WIDGET_CLASS (marlin_grid_parent_class)->size_allocate (widget, allocation);
}

static void
realize (GtkWidget *widget)
{
	/* Add events */
	gtk_widget_add_events (widget,
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK |
			       GDK_POINTER_MOTION_MASK);

	GTK_WIDGET_CLASS (marlin_grid_parent_class)->realize (widget);
}

static void
draw_point (MarlinGrid *grid,
	    cairo_t    *cr,
	    int         x,
	    int         y,
	    gboolean    selected)
{
	MarlinGridPrivate *priv = grid->priv;
	int x1, y1;

	x1 = x - HALF_POINT_WIDTH;
	y1 = y - HALF_POINT_WIDTH;

	cairo_save (cr);

	if (selected) {
		cairo_set_source_rgb (cr, 0, 0, 0);
	} else {
		cairo_set_source_rgb (cr, 1, 1, 1);
	}
	cairo_rectangle (cr, priv->box_x + x1, priv->box_y + y1,
			 POINT_WIDTH, POINT_WIDTH);
	cairo_fill (cr);

	cairo_set_antialias (cr, CAIRO_ANTIALIAS_NONE);
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_set_line_width (cr, 1);
	cairo_rectangle (cr, priv->box_x + x1 + 0.5f, priv->box_y + y1 + 0.5f,
			 POINT_WIDTH - 1, POINT_WIDTH - 1);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
draw_curve (MarlinGrid      *grid,
	    cairo_t         *cr,
	    MarlinGridCurve *curve)
{
	MarlinGridPrivate *priv = grid->priv;
	MarlinListNode *l;

	cairo_save (cr);

	for (l = curve->points->first; l; l = marlin_list_node_next (l)) {
		MarlinCurvePoint *p1, *p2;
		MarlinGridPoint *point1, *point2;
		guint x1, y1, x2, y2;

		p1 = (MarlinCurvePoint *) l;
		p2 = (MarlinCurvePoint *) (l->next);

		point1 = p1->point;

		x1 = FRAME_TO_PIXEL (point1->x);
		y1 = LEVEL_TO_PIXEL (point1->level);

		if (p2) {
			point2 = p2->point;

			x2 = FRAME_TO_PIXEL (point2->x);
			y2 = LEVEL_TO_PIXEL (point2->level);

			cairo_move_to (cr, priv->box_x + x1, priv->box_y + y1);
			cairo_line_to (cr, priv->box_x + x2, priv->box_y + y2);

			gdk_cairo_set_source_color (cr, &curve->colour);
			cairo_stroke (cr);
		}

		if (p1->movability != MARLIN_GRID_POINT_FIXED) {
			draw_point (grid, cr, x1, y1, p1->selected);
		}
	}

	cairo_restore (cr);
}

static void
draw_labels (MarlinGrid *grid,
	     cairo_t    *cr)
{
	MarlinGridClass *klass;
	MarlinGridPrivate *priv = grid->priv;
	float a, b;
	char *text;
	int value;
	float divwidth = (float) priv->box_width / 10.0f;
	int length_x;
	cairo_text_extents_t extents;
	float offsetx = 0;
	float offsety = 0;

	cairo_save (cr);

	cairo_set_font_options (cr, priv->options);

	length_x = priv->finish - priv->start;

	klass = MARLIN_GRID_GET_CLASS (grid);
	if (klass->draw_x_axis) {
		GdkRectangle rect;

		rect.x = priv->box_x;
		rect.y = priv->box_y;
		rect.width = priv->box_width;
		rect.height = priv->box_height;

		klass->draw_x_axis (grid, cr, &rect);
	} else {
		/* do x text */
		cairo_set_source_rgb (cr, 0, 0, 0);
		for (a = 0; a < 11; a += 5) {
			b = priv->box_x + (a * divwidth);
			value = ((length_x / 10) * a) + priv->start;
			text = g_strdup_printf ("%d", value);

			cairo_text_extents (cr, text, &extents);
			if (a == 0) {
				offsetx = 2;
			} else if (a == 10) {
				offsetx = extents.width;
			} else {
				offsetx = (extents.width / 2.0f);
			}

			cairo_move_to (cr, b - offsetx,
				       priv->box_y + priv->box_height + 15);
			cairo_show_text (cr, text);
			g_free (text);
		}
	}

	if (klass->draw_y_axis) {
		GdkRectangle rect;

		rect.x = priv->box_x;
		rect.y = priv->box_y;
		rect.width = priv->box_width;
		rect.height = priv->box_height;

		klass->draw_y_axis (grid, cr, &rect);
	} else {
		/* do y text */
		cairo_text_extents (cr, "0 dB", &extents);
		offsetx = extents.width + 5;
		offsety = extents.height;
		cairo_move_to (cr, priv->box_x - offsetx - 2,
			       priv->box_y + offsety);
		cairo_show_text (cr, "0 dB");

		cairo_text_extents (cr, "-" MARLIN_INFINITY_DB_TEXT, &extents);
		offsetx = extents.width + 5;
		cairo_move_to (cr, priv->box_x - offsetx - 2,
			       priv->box_y + priv->box_height);
		cairo_show_text (cr, "-" MARLIN_INFINITY_DB_TEXT);

		cairo_restore (cr);
	}
}

static void
draw_grid (MarlinGrid *grid,
	   cairo_t    *cr)
{
	MarlinGridPrivate *priv = grid->priv;
	float a, b;
	double dotted[] = {1., 2.};
	float divwidth = (float) priv->box_width / 10.0f;
	float divheight = (float) priv->box_height / 10.0f;

	cairo_save (cr);

	cairo_set_line_width (cr, 1);
	cairo_set_dash (cr, dotted, 2, 0.0);

	cairo_set_source_rgb (cr, 0.1, 0.1, 0.1);
	for (a = 1; a < 10; a++) {
		b = priv->box_x + (a * divwidth);
		cairo_move_to (cr, (int) b + 0.5f, priv->box_y);
		cairo_line_to (cr, (int) b + 0.5f, priv->box_y + priv->box_height);
		cairo_stroke (cr);
	}

	for (a = 1; a < 10; a++) {
		b = priv->box_y + (a * divheight);
		cairo_move_to (cr, priv->box_x, (int) b + 0.5f);
		cairo_line_to (cr, priv->box_x + priv->box_width, (int) b + 0.5f);
		cairo_stroke (cr);
	}

	cairo_restore (cr);
}

static void
_marlin_grid_paint (GtkWidget    *widget,
		    GdkRectangle *area)
{
	MarlinGrid *grid = (MarlinGrid *) widget;
	MarlinGridPrivate *priv;
	guint64 length;
	cairo_t *cr;
	int i;

	priv = grid->priv;

	cr = gdk_cairo_create (widget->window);

	length = priv->finish - priv->start;
	priv->box_x = 35;
	priv->box_y = 5;
	priv->box_width = length / (priv->frames_per_pixel + 1);
	priv->box_height = widget->allocation.height - (20 + priv->box_y);

	draw_labels (grid, cr);

	/* Draw background */
	cairo_rectangle (cr, priv->box_x, priv->box_y,
			 priv->box_width, priv->box_height);

	cairo_set_source_rgb (cr, 1, 1, 1);
	cairo_fill (cr);

	cairo_rectangle (cr, priv->box_x + 0.5f, priv->box_y + 0.5f,
			 priv->box_width - 1, priv->box_height - 1);
	cairo_set_source_rgb (cr, 0.1, 0.1, 0.1);
	cairo_set_line_width (cr, 1);
	cairo_stroke (cr);

	cairo_rectangle (cr, priv->box_x + 1, priv->box_y + 1,
			 priv->box_width - 2, priv->box_height - 2);
	cairo_clip (cr);
	draw_grid (grid, cr);

	for (i = 0; i < priv->curves->len; i++) {
		draw_curve (grid, cr, priv->curves->pdata[i]);
	}
}

static gboolean
expose_event (GtkWidget      *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		_marlin_grid_paint (widget, &event->area);
	}

	return FALSE;
}

static void
redraw_point (MarlinGrid      *grid,
	      MarlinGridPoint *point)
{
	MarlinGridPrivate *priv = grid->priv;
	GtkWidget *widget = (GtkWidget *) grid;
	GdkRectangle rect;

	rect.x = priv->box_x + FRAME_TO_PIXEL (point->x) - HALF_POINT_WIDTH;
	rect.y = priv->box_y + LEVEL_TO_PIXEL (point->level) - HALF_POINT_WIDTH;
	rect.width = POINT_WIDTH;
	rect.height = POINT_WIDTH;

	gdk_window_invalidate_rect (widget->window, &rect, FALSE);
}

static MarlinCurvePoint *
find_point (MarlinGrid       *grid,
	    int               x,
	    int               y,
	    MarlinGridCurve **curve)
{
	MarlinGridPrivate *priv = grid->priv;
	int i;
	
	for (i = 0; i < priv->curves->len; i++) {
		MarlinGridCurve *c = priv->curves->pdata[i];
		MarlinListNode *l;

		for (l = c->points->first; l; l = marlin_list_node_next (l)) {
			MarlinCurvePoint *p = (MarlinCurvePoint *) l;
			MarlinGridPoint *point = p->point;
			int px, py;

			if (p->movability == MARLIN_GRID_POINT_FIXED) {
				continue;
			}

			px = FRAME_TO_PIXEL (point->x);
			py = LEVEL_TO_PIXEL (point->level);
			
			if (BETWEEN (x, px - HALF_POINT_WIDTH, px + HALF_POINT_WIDTH) &&
			    BETWEEN (y, py - HALF_POINT_WIDTH, py + HALF_POINT_WIDTH)) {
				*curve = c;
				return p;
			}
		}
	}

	*curve = NULL;
	return NULL;
}

static gboolean
button_press_event (GtkWidget      *widget,
		    GdkEventButton *event)
{
	MarlinGrid *grid = (MarlinGrid *) widget;
	MarlinGridPrivate *priv = grid->priv;
	int real_x, real_y;
	MarlinGridCurve *curve;
	MarlinCurvePoint *point;

	if (event->button != 1) {
		return TRUE;
	}

	real_x = event->x - priv->box_x;
	real_y = event->y - priv->box_y;

	point = find_point (grid, real_x, real_y, &curve);

	if (point) {
		GdkCursor *hand = marlin_cursor_get (widget, HAND_CLOSED);

		priv->in_grab = TRUE;
		
		gdk_pointer_grab (widget->window, FALSE,
				  GDK_BUTTON_RELEASE_MASK |
				  GDK_BUTTON1_MOTION_MASK, NULL,
				  hand, event->time);
		gdk_cursor_unref (hand);
	}

	return FALSE;
}

static gboolean
button_release_event (GtkWidget      *widget,
		      GdkEventButton *event)
{
	MarlinGrid *grid = (MarlinGrid *) widget;
	MarlinGridPrivate *priv = grid->priv;

	if (priv->in_grab) {
		gdk_pointer_ungrab (event->time);
		priv->in_grab = FALSE;
	}

	return FALSE;
}

static void
drag_point (MarlinGrid     *grid,
	    GdkEventMotion *event)
{
	MarlinGridPrivate *priv = grid->priv;
	MarlinCurvePoint *point, *n, *p;
	MarlinGridPoint *gp;
	MarlinGridCurve *curve;
	int real_x, real_y;
	guint64 frame, upper, lower;
	double level;

	real_x = CLAMP (event->x - priv->box_x, 0, priv->box_width);
	real_y = CLAMP (event->y - priv->box_y, 0, priv->box_height);

	curve = priv->selected_curve;
	point = priv->selected_point;

	/* Restrict the frame to the range of the lower and upper bounds
	   or the range of the curve */
	n = (MarlinCurvePoint *) marlin_list_node_next ((MarlinListNode *) point);
	p = (MarlinCurvePoint *) marlin_list_node_previous ((MarlinListNode *) point);
	upper = n ? n->point->x : curve->range.finish;
	lower = p ? p->point->x : curve->range.start;

	gp = point->point;

	if (point->movability & MARLIN_GRID_POINT_FREE_X) {
		frame = CLAMP (PIXEL_TO_FRAME (real_x), lower, upper);
	} else {
		frame = gp->x;
	}

	if (point->movability & MARLIN_GRID_POINT_FREE_Y) {
		level = PIXEL_TO_LEVEL (real_y);
	} else {
		level = gp->level;
	}

	marlin_grid_point_set (gp, frame, level);

	redraw_grid (grid);
}

static gboolean
motion_notify_event (GtkWidget      *widget,
		     GdkEventMotion *event)
{
	MarlinGrid *grid = (MarlinGrid *) widget;
	MarlinGridPrivate *priv = grid->priv;
	int real_x, real_y;
	MarlinGridCurve *curve;
	MarlinCurvePoint *point;

	if (priv->in_grab && priv->selected_point) {
		drag_point (grid, event);
		return FALSE;
	}

	real_x = event->x - priv->box_x;
	real_y = event->y - priv->box_y;

	point = find_point (grid, real_x, real_y, &curve);
	
	if (point) {
		GdkCursor *hand = marlin_cursor_get (widget, HAND_OPEN);
		
		gdk_window_set_cursor (widget->window, hand);
		gdk_cursor_unref (hand);

		if (priv->selected_point) {
			priv->selected_point->selected = FALSE;
			redraw_point (grid, priv->selected_point->point);
		}
		priv->selected_point = point;
		priv->selected_curve = curve;

		point->selected = TRUE;	
		redraw_point (grid, point->point);
	} else {
		gdk_window_set_cursor (widget->window, NULL);
		if (priv->selected_point) {
			priv->selected_point->selected = FALSE;
			redraw_point (grid, priv->selected_point->point);
		}
		priv->selected_point = NULL;
		priv->selected_curve = NULL;
	}

	real_x = CLAMP(real_x, 0, priv->box_width);
	real_y = CLAMP(real_y, 0, priv->box_height);
	
	return FALSE;
}

static void
marlin_grid_class_init (MarlinGridClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	GtkWidgetClass *w_class = (GtkWidgetClass *) klass;

	g_type_class_add_private (klass, sizeof (MarlinGridPrivate));

	o_class->finalize = finalize;
	o_class->dispose = dispose;

	w_class->size_allocate = size_allocate;
/* 	w_class->size_request = size_request; */
	w_class->realize = realize;
	w_class->expose_event = expose_event;
	w_class->button_press_event = button_press_event;
	w_class->button_release_event = button_release_event;
	w_class->motion_notify_event = motion_notify_event;
}

static void
marlin_grid_init (MarlinGrid *grid)
{
	MarlinGridPrivate *priv;

	grid->priv = GET_PRIVATE (grid);
	priv = grid->priv;

	priv->start = 0;
	priv->finish = 0;

	priv->options = cairo_font_options_create ();
	priv->curves = g_ptr_array_new ();
}

void
marlin_grid_add_curve (MarlinGrid      *grid,
		       MarlinGridCurve *curve)
{
	MarlinGridPrivate *priv;
	int i;

	priv = grid->priv;

	g_ptr_array_add (priv->curves, curve);

	/* Find the new bounds */
	if (priv->curves->len > 1) {
		for (i = 0; i < priv->curves->len; i++) {
			MarlinGridCurve *c = priv->curves->pdata[i];

			priv->start = MIN (priv->start, c->range.start);
			priv->finish = MAX (priv->finish, c->range.finish);
		}
	} else {
		priv->start = curve->range.start;
		priv->finish = curve->range.finish;
	}
}

/**
 * marlin_grid_curve_new:
 * @range: A #MarlinRange
 *
 * Creates a new #MarlinGridCurve with the range defined in @range.
 *
 * Return value: A newly allocated #MarlinGridCurve.
 */
MarlinGridCurve *
marlin_grid_curve_new (MarlinRange *range)
{
	MarlinGridCurve *curve;

	curve = g_new0 (MarlinGridCurve, 1);
	curve->range.start = range->start;
	curve->range.finish = range->finish;
	curve->points = marlin_list_new (NULL);

	return curve;
}

/**
 * marlin_grid_curve_free:
 * @curve: A #MarlinGridCurve
 *
 * Frees all allocated resources used by @curve.
 */
void
marlin_grid_curve_free (MarlinGridCurve *curve)
{
	g_free (curve->name);
	if (curve->points) {
		marlin_list_free (curve->points);
	}

	g_free (curve);
}

/**
 * marlin_grid_curve_add_point:
 * @curve: The #MarlinGridCurve to add the point to.
 * @point: The #MarlinGridPoint.
 *
 * Adds @point to @curve.
 */
void
marlin_grid_curve_add_point (MarlinGridCurve *curve,
			     MarlinGridPoint *point,
			     guint32          movability)
{
	MarlinCurvePoint *cp;

	cp = g_new (MarlinCurvePoint, 1);
	cp->point = g_object_ref (point);
	cp->movability = movability;
	cp->selected = FALSE;

	marlin_list_append (curve->points, (MarlinListNode *) cp);
}
