/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2003 - 2008 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <string.h>

#include <glib/gi18n.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>

#include <marlin/marlin-cursors.h>
#include <marlin/marlin-sample.h>
#include <marlin/marlin-marker-model.h>
#include <marlin/marlin-marker-view.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-utils.h>
#include <marlin/marlin-x-utils.h>
#include <marlin/marlin-position-spinner.h>
#include <marlin/marlin-types.h>
#include <marlin/marlin-list.h>

enum {
	PROP_0,
	PROP_MODEL,
	PROP_FRAMES_PER_PIXEL,
	PROP_SAMPLE,
	PROP_SNAP_TO_TICKS,
	PROP_DISPLAY_TYPE,
	PROP_UNDO_MANAGER
};

enum {
	MOVE_CURSOR,
	ENTER_MARKER,
	LEAVE_MARKER,
	LAST_SIGNAL
};

struct _ViewMarker {
	MarlinMarker *marker; /* The marker in the model */

	guint64 real_position; /* The position it is currently at.
				  This may be different from marker->position
				  if we're in the process of moving the
				  marker */
	gboolean hidden; /* Is the marker visible or not */
};

struct _Region {
	MarlinListNode list; /* Parent */

	/* The markers at the start and end of the region */
	MarlinMarker *start; /* If NULL then the start is frame 0 */
	MarlinMarker *end; /* If NULL then the end if the last frame */

	guint64 midpoint;
};

struct _MarlinMarkerViewPrivate {
	MarlinMarkerModel *model;
	guint32 add_id, remove_id, move_id;

	/* Sample */
	MarlinSample *sample;
	guint32 notify_id;
	guint rate, bpm;
	MarlinUndoManager *undo;

	/* Pixmap to save redrawing */
	GdkPixmap *backing_store;
	GdkGC *non_gr_exp_gc;
	PangoLayout *layout;

	GList *markers; /* List of struct _ViewMarker */
	GHashTable *marker_to_view;
	GHashTable *position_to_markers; /* Get the struct _ViewMarker at
					    a position */

	MarlinList *regions; /* List of struct _Region */

	/* To update correctly. */
	guint fpp;

	int xofs; /* x offset in pixels */
	GtkAdjustment *hadj, *vadj; /* Adjustments for scrolling */

	/* To keep track of the mouse and marker */
	struct _ViewMarker *current_marker;
	struct _ViewMarker *focus_marker, *real_focus; /* Real focus is for
							  storing the
							  focus_marker while
							  we are out of
							  focus */

	/* Keeping an eye on the grab */
	gboolean in_drag_marker;
	struct _ViewMarker *drag_marker;
	guint64 floating_marker_pos;

	gboolean kb_grab;
	MarlinUndoContext *kb_ctxt;

	MarlinDisplay display;

	gboolean snap; /* Snap to ticks */
};

#define GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MARLIN_MARKER_VIEW_TYPE, MarlinMarkerViewPrivate))
G_DEFINE_TYPE (MarlinMarkerView, marlin_marker_view, GTK_TYPE_WIDGET);
static guint32 signals[LAST_SIGNAL] = { 0 };

#define VIEW_HEIGHT 32
#define EXPANDER_SIZE 14
#define EXPANDER_SIZE_HALF 7 /* This should be half of the EXPANDER_SIZE */
#define DEFAULT_FRAMES_PER_PIXEL 4096
#define HANDLE_WIDTH 12
#define HANDLE_HEIGHT 4

#define BETWEEN(x, a, b) ((x) >= (a) && (x) <= (b))

static void
clear_markers (MarlinMarkerView *view)
{
	GList *p;

	for (p = view->priv->markers; p; p = p->next) {
		struct _ViewMarker *vm = p->data;

		g_free (vm);
	}
	g_list_free (view->priv->markers);
	view->priv->markers = NULL;

	g_hash_table_destroy (view->priv->marker_to_view);
	view->priv->marker_to_view = NULL;
	g_hash_table_destroy (view->priv->position_to_markers);
	view->priv->position_to_markers = NULL;
}

static void
finalize (GObject *object)
{
	MarlinMarkerView *view;
	MarlinMarkerViewPrivate *priv;

	view = MARLIN_MARKER_VIEW (object);
	priv = view->priv;

	clear_markers (view);

	if (priv->regions) {
		marlin_list_free (priv->regions);
	}

	G_OBJECT_CLASS (marlin_marker_view_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
	MarlinMarkerView *view = (MarlinMarkerView *) object;
	MarlinMarkerViewPrivate *priv = view->priv;

	if (priv->model) {
		g_signal_handler_disconnect (priv->model, priv->add_id);
		g_signal_handler_disconnect (priv->model, priv->remove_id);
		g_signal_handler_disconnect (priv->model, priv->move_id);
		g_object_unref (G_OBJECT (priv->model));

		priv->model = NULL;
	}

	if (priv->undo) {
		if (priv->kb_ctxt) {
			marlin_undo_manager_context_cancel (priv->undo,
							    priv->kb_ctxt);
			priv->kb_ctxt = NULL;
		}

		g_object_unref (G_OBJECT (priv->undo));
		priv->undo = NULL;
	}

	if (priv->sample) {
		g_signal_handler_disconnect (priv->sample, priv->notify_id);
		g_object_unref (G_OBJECT (priv->sample));
		priv->sample = NULL;
	}

	if (priv->layout) {
		g_object_unref (G_OBJECT (priv->layout));
		priv->layout = NULL;
	}

	G_OBJECT_CLASS (marlin_marker_view_parent_class)->dispose (object);
}

static void
redraw_widget (GtkWidget *widget)
{
	GdkRectangle area;

	area.x = 0;
	area.y = 0;
	area.width = widget->allocation.width;
	area.height = widget->allocation.height;

	gdk_window_invalidate_rect (widget->window, &area, FALSE);
}

static void
change_focus_marker (MarlinMarkerView *view,
		     struct _ViewMarker *new_focus)
{
	GtkWidget *widget = (GtkWidget *) view;
	struct _ViewMarker *vm = view->priv->focus_marker;

	if (vm != NULL) {
		int x;

		x = ((vm->real_position / view->priv->fpp) - view->priv->xofs - EXPANDER_SIZE_HALF) - 2;

#if 0
		area.x = MAX (x, 0);
		area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
		area.width = EXPANDER_SIZE + 4;
		area.height = EXPANDER_SIZE + 4;
#endif
	}

	vm = view->priv->focus_marker = new_focus;
	if (vm != NULL) {
		int x;

		x = ((vm->real_position / view->priv->fpp) - view->priv->xofs - EXPANDER_SIZE_HALF) - 2;
#if 0
		area.x = MAX (x, 0);
		area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
		area.width = EXPANDER_SIZE + 4;
		area.height = EXPANDER_SIZE + 4;
#endif
	}

	redraw_widget (widget);
}

/* Region code */

static void G_GNUC_UNUSED
dump_region (struct _Region *region)
{
	g_print ("Region - %p\n", region);
	g_print ("region->start: %p\n", region->start);
	g_print ("region->end: %p\n", region->end);
}

static void
region_free (gpointer data)
{
	struct _Region *region = data;

	g_slice_free (struct _Region, region);
}

static void
reset_regions (MarlinMarkerView *view)
{
	MarlinMarkerViewPrivate *priv = view->priv;

	marlin_list_clear (priv->regions);

	/* Create a new region that goes from start -> finish */
	marlin_list_append (priv->regions,
			    (MarlinListNode *) g_slice_new0 (struct _Region));
}

static gboolean
find_region (MarlinMarkerView *view,
	     guint64           position,
	     struct _Region  **region)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	MarlinListNode *l;

	for (l = priv->regions->first; l; l = l->next) {
		struct _Region *r = (struct _Region *) l;
		guint64 region_start, region_end;

		if (r->start) {
			region_start = r->start->position;
		} else {
			region_start = 0;
		}

		if (r->end) {
			/* Regions go from start -> finish - 1 */
			region_end = r->end->position - 1;
		} else {
			g_object_get (priv->sample,
				      "total_frames", &region_end,
				      NULL);
			region_end--;
		}

		if (BETWEEN (position, region_start, region_end)) {
			*region = r;
			return TRUE;
		}
	}

	/* This cannot happen, the position should always be between
	   the start and the finish markers */
	return FALSE;
}

static void
find_regions_for_marker (MarlinMarkerView *view,
			 MarlinMarker     *marker,
			 struct _Region  **region1,
			 struct _Region  **region2)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	MarlinListNode *l;

	for (l = priv->regions->first; l; l = l->next) {
		struct _Region *region = (struct _Region *) l;

		if (region->start == marker) {
			*region2 = region;
		}

		if (region->end == marker) {
			*region1 = region;
		}
	}
}

static void
split_region (MarlinMarkerView *view,
	      struct _Region   *region,
	      MarlinMarker     *marker)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	struct _Region *new_region;

	g_return_if_fail (region != NULL);

	new_region = g_slice_new0 (struct _Region);
	new_region->start = marker;
	new_region->end = region->end;

	region->end = marker;

	/* Insert the region into the list */
	marlin_list_insert_after (priv->regions,
				  (MarlinListNode *) region,
				  (MarlinListNode *) new_region);
}

static void
join_regions (MarlinMarkerView *view,
	      struct _Region   *region_a,
	      struct _Region   *region_b)
{
	MarlinMarkerViewPrivate *priv = view->priv;

	g_return_if_fail (region_a != NULL);
	g_return_if_fail (region_b != NULL);

	region_a->end = region_b->end;

	marlin_list_remove (priv->regions, (MarlinListNode *) region_b);
}

/* Markers */
static int
compare_markers (gconstpointer a,
		 gconstpointer b)
{
	struct _ViewMarker *vm_a = (struct _ViewMarker *) a;
	struct _ViewMarker *vm_b = (struct _ViewMarker *) b;

	/* Sanity check just in case */
	if (a == b) {
		return 0;
	}

	/* This should never happen... */
	g_assert (a != NULL);
	g_assert (b != NULL);

	/* The threat of 64 bit arithmetic scares me
	   so I'm doing this the long way */
	if (vm_a->real_position < vm_b->real_position) {
		return -1;
	} else if (vm_a->real_position == vm_b->real_position) {
		return 0;
	} else {
		return 1;
	}
}

static void
marker_added (MarlinMarkerModel *model,
	      MarlinMarker *marker,
	      MarlinMarkerView *view)
{
	struct _ViewMarker *vm;
	struct _Region *region = NULL;

	vm = g_new (struct _ViewMarker, 1);
	vm->marker = marker;
	vm->real_position = marker->position;
	vm->hidden = FALSE;

	g_hash_table_insert (view->priv->marker_to_view, marker, vm);
	view->priv->markers = g_list_insert_sorted (view->priv->markers, vm,
						    (GCompareFunc) compare_markers);

	/* The newest added marker gets the focus */
	change_focus_marker (view, vm);

	/* Split the region this marker was in */
	find_region (view, marker->position, &region);
	if (region == NULL) {
		g_warning ("Region not found for position %llu",
			   marker->position);
		return;
	}

	split_region (view, region, marker);
}

static void
marker_removed (MarlinMarkerModel *model,
		MarlinMarker *marker,
		MarlinMarkerView *view)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	GtkWidget *widget = (GtkWidget *) view;
	struct _ViewMarker *vm;
	struct _Region *region1, *region2;
	gboolean was_focus;

	vm = g_hash_table_lookup (priv->marker_to_view, marker);
	g_assert (vm != NULL);

	g_hash_table_remove (priv->marker_to_view, marker);
	view->priv->markers = g_list_remove (priv->markers, vm);

	if (vm == priv->focus_marker) {
		was_focus = TRUE;
	} else {
		was_focus = FALSE;
	}

	find_regions_for_marker (view, marker, &region1, &region2);
	join_regions (view, region1, region2);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		GdkRectangle area;
		int x;

		change_focus_marker (view, NULL);

		x = ((vm->real_position / priv->fpp) - priv->xofs - EXPANDER_SIZE_HALF) - 2;
		area.x = MAX (x, 0);
		area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
		area.width = EXPANDER_SIZE + 4;
		area.height = EXPANDER_SIZE + 4;

		gdk_window_invalidate_rect (widget->window, &area, FALSE);

	}

	g_free (vm);
}

static void
marker_changed (MarlinMarkerModel *model,
		MarlinMarker *marker,
		MarlinMarkerView *view)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	GtkWidget *widget = (GtkWidget *) view;
	struct _ViewMarker *vm;
	struct _Region *region1, *region2, *new_region;

	vm = g_hash_table_lookup (priv->marker_to_view, marker);
	g_assert (vm != NULL);

	/* Join the old regions together */
	find_regions_for_marker (view, marker, &region1, &region2);
	join_regions (view, region1, region2);

	region_free (region2);

	/* Move the marker */
	vm->real_position = marker->position;

	/* Sort the new list */
	priv->markers = g_list_sort (priv->markers, compare_markers);

	/* Split the new markers region */
	find_region (view, marker->position, &new_region);
	split_region (view, new_region, marker);

	redraw_widget (widget);
}

static gboolean
find_marker (MarlinMarkerView *view,
	     guint64 position,
	     struct _ViewMarker **vm)
{
	GList *p;

	for (p = view->priv->markers; p; p = p->next) {
		struct _ViewMarker *vmarker = p->data;
		gint64 minus = ((gint64)vmarker->real_position) - (EXPANDER_SIZE_HALF *view->priv->fpp);

		if (( ((gint64)position) >= minus) &&
		    (position <= vmarker->real_position +
		     (EXPANDER_SIZE_HALF * view->priv->fpp))) {
			*vm = vmarker;
			return TRUE;
		}
	}

	*vm = NULL;
	return FALSE;
}

static void
add_markers (MarlinMarkerView *view,
	     GList *marks)
{
	GList *p;

	if (view->priv->marker_to_view == NULL) {
		view->priv->marker_to_view = g_hash_table_new (NULL, NULL);
	}

	if (view->priv->position_to_markers == NULL) {
		view->priv->position_to_markers = g_hash_table_new (NULL, NULL);
	}

	for (p = marks; p; p = p->next) {
		struct _ViewMarker *vm = g_new (struct _ViewMarker, 1);

		vm->marker = p->data;
		vm->real_position = vm->marker->position;
		vm->hidden = FALSE;

		view->priv->markers = g_list_prepend (view->priv->markers, vm);
		g_hash_table_insert (view->priv->marker_to_view, p->data, vm);
	}
}

static void
sample_notify (MarlinSample *sample,
	       GParamSpec   *pspec,
	       MarlinMarkerView *view)
{
	GtkWidget *widget;

	widget = GTK_WIDGET (view);
	if (strcmp (pspec->name, "sample-rate") == 0) {
		g_object_get (G_OBJECT (sample),
			      "sample_rate", &view->priv->rate,
			      NULL);
	} else {
		return;
	}

	if (GTK_WIDGET_DRAWABLE (view)) {
		GdkRectangle rect;

		rect.x = 0;
		rect.y = 0;
		rect.width = widget->allocation.width;
		rect.height = widget->allocation.height;

		gdk_window_invalidate_rect (widget->window,
					    &rect, FALSE);
	}
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	MarlinMarkerViewPrivate *priv;
	GtkWidget *widget;
	MarlinMarkerView *view;
	GList *marks;

	view = MARLIN_MARKER_VIEW (object);
	widget = GTK_WIDGET (object);
	priv = view->priv;

	switch (prop_id) {
	case PROP_MODEL:
		/* Disconnect the signal handlers and unref the model */
		if (priv->add_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->add_id);
		}
		if (priv->remove_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->remove_id);
		}
		if (priv->move_id > 0) {
			g_signal_handler_disconnect (G_OBJECT (priv->model),
						     priv->move_id);
		}

		if (priv->model != NULL) {
			g_object_unref (G_OBJECT (priv->model));
		}

		clear_markers (view);
		priv->model = (MarlinMarkerModel *) g_value_dup_object (value);
		g_object_get (G_OBJECT (priv->model),
			      "markers", &marks,
			      NULL);

		reset_regions (view);

		add_markers (view, marks);

		priv->add_id = g_signal_connect (G_OBJECT (priv->model),
						 "marker-added",
						 G_CALLBACK (marker_added),
						 view);
		priv->remove_id = g_signal_connect (G_OBJECT (priv->model),
						    "marker-removed",
						    G_CALLBACK (marker_removed),
						    view);
		priv->move_id = g_signal_connect (G_OBJECT (priv->model),
						  "marker-changed",
						  G_CALLBACK (marker_changed),
						  view);

		break;

	case PROP_FRAMES_PER_PIXEL:
		priv->fpp = g_value_get_uint (value);

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle rect;

			rect.x = 0;
			rect.y = 0;
			rect.width = widget->allocation.width;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window,
						    &rect, FALSE);
		}
		break;

	case PROP_SAMPLE:
		if (priv->sample != NULL) {
			g_signal_handler_disconnect (priv->sample,
						     priv->notify_id);
			g_object_unref (G_OBJECT (priv->sample));
		}

		priv->sample = (MarlinSample *) g_value_dup_object (value);
		priv->notify_id = g_signal_connect (priv->sample,
						    "notify",
						    G_CALLBACK (sample_notify), 
						    view);

		g_object_get (G_OBJECT (priv->sample),
			      "sample-rate", &priv->rate,
			      "bpm", &priv->bpm,
			      NULL);
		break;

	case PROP_SNAP_TO_TICKS:
		priv->snap = g_value_get_boolean (value);
		break;

	case PROP_DISPLAY_TYPE:
		priv->display = g_value_get_enum (value);

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle rect;

			rect.x = 0;
			rect.y = 0;
			rect.width = widget->allocation.width;
			rect.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window,
						    &rect, FALSE);
		}

		break;

	case PROP_UNDO_MANAGER:
		priv->undo = (MarlinUndoManager *) g_value_dup_object (value);
		break;

	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinMarkerView *view;

	view = MARLIN_MARKER_VIEW (object);

	switch (prop_id) {
	case PROP_MODEL:
		g_value_set_object (value, view->priv->model);
		break;

	case PROP_SAMPLE:
		g_value_set_object (value, view->priv->sample);
		break;

	case PROP_SNAP_TO_TICKS:
		g_value_set_boolean (value, view->priv->snap);
		break;

	default:
		break;
	}
}

static void
make_pixmap (MarlinMarkerView *view,
	     int aw,
	     int ah)
{
	GtkWidget *widget;
	int width, height;

	widget = GTK_WIDGET (view);

	if (view->priv->backing_store) {
		gdk_drawable_get_size (view->priv->backing_store, &width, &height);
		if ((width == aw) &&
		    (height == ah)) {
			return;
		}

		g_object_unref (view->priv->backing_store);
	}

	view->priv->backing_store = gdk_pixmap_new (widget->window,
						    aw, ah, -1);

	if (view->priv->non_gr_exp_gc == NULL) {
		view->priv->non_gr_exp_gc = gdk_gc_new (widget->window);

		gdk_gc_set_exposures (view->priv->non_gr_exp_gc, FALSE);
	}
}

static void
size_allocate (GtkWidget *widget,
	       GtkAllocation *allocation)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);

	if (GTK_WIDGET_REALIZED (widget)) {
		GdkRectangle rect;

		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);
		make_pixmap (view, allocation->width, allocation->height);

		rect.x = 0;
		rect.y = 0;
		rect.width = allocation->width;
		rect.height = allocation->height;

		gdk_window_invalidate_rect (widget->window, &rect, FALSE);
	}

	/* Chain up */
	GTK_WIDGET_CLASS (marlin_marker_view_parent_class)->size_allocate (widget, allocation);
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *req)
{
	int font_size;
	PangoFontDescription *fd;

	fd = widget->style->font_desc;

	font_size = pango_font_description_get_size (fd);

	req->height = PANGO_PIXELS (font_size) + widget->style->ythickness * 2 + 22;
}

static void
realize (GtkWidget *widget)
{
	MarlinMarkerView *view;
	MarlinMarkerViewPrivate *priv;
	GdkWindowAttr attr;
	int attr_mask;

	view = MARLIN_MARKER_VIEW (widget);
	priv = view->priv;

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attr.window_type = GDK_WINDOW_CHILD;
	attr.x = widget->allocation.x;
	attr.y = widget->allocation.y;
	attr.width = widget->allocation.width;
	attr.height = widget->allocation.height;
	attr.wclass = GDK_INPUT_OUTPUT;
	attr.visual = gtk_widget_get_visual (widget);
	attr.colormap = gtk_widget_get_colormap (widget);
	attr.event_mask = (gtk_widget_get_events (widget)
			   | GDK_EXPOSURE_MASK
			   | GDK_BUTTON_PRESS_MASK
			   | GDK_BUTTON_RELEASE_MASK
			   | GDK_POINTER_MOTION_MASK
			   | GDK_LEAVE_NOTIFY_MASK);

	attr_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_COLORMAP | GDK_WA_VISUAL;

	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
					 &attr, attr_mask);
	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
	gdk_window_set_back_pixmap (widget->window, NULL, FALSE);

	make_pixmap (view, widget->allocation.width, widget->allocation.height);
}

static void
unrealize (GtkWidget *widget)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);

	if (view->priv->backing_store) {
		g_object_unref (view->priv->backing_store);
	}

	if (view->priv->non_gr_exp_gc) {
		g_object_unref (view->priv->non_gr_exp_gc);
	}

	view->priv->backing_store = NULL;
	view->priv->non_gr_exp_gc = NULL;

	GTK_WIDGET_CLASS (marlin_marker_view_parent_class)->unrealize (widget);
}

static int
get_frames_increment (MarlinMarkerView *view)
{
	int increment, i, f, factor;

	i = 1;
	f = 100;
	increment = i * f;
	/* Go up as 100, 200, 500, 1000, 2000, 5000 and so on... */
	for (factor = 1; ; factor *= 2) {
		if (view->priv->fpp <= factor) {
			break;
		}

		i++;
		if (i == 3) {
			i = 5;
		} else if (i == 6) {
			i = 1;
			f *= 10;
		}

		increment = i * f;
	}

	return increment;
}

static int
get_time_increment (MarlinMarkerView *view)
{
	int factor, i;
	/* These are the increments SF uses, up to 1:131,072 zoom level
	   which we can't do, just cos... FIXME
	   There must be a better way than this to work it out? */
	double increments[] = { 0.005, 0.005, 0.01, 0.01, 0.05, 0.1,
				0.1, 0.5, 1, 1, 2, 5, 10, 20, 50,
				50, 100, 500 };

	for (factor = 1, i =0; ; factor *= 2, i++) {
		if (view->priv->fpp <= factor) {
			break;
		}
	}

	return (int) ((float)(increments[i] * view->priv->rate));
}

static int
get_beat_increment (MarlinMarkerView *view)
{
	int factor, i;
	/* These are the increments SF uses, up to 1:131,072 zoom level
	   which we can't do, just cos... FIXME
	   There must be a better way than this to work it out? */
	double increments[] = { 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 2, 5, 10, 20, 50,
				50, 100, 500 };

	for (factor = 1, i =0; ; factor *= 2, i++) {
		if (view->priv->fpp <= factor) {
			break;
		}
	}

	return (int) ((float)(increments[i] * view->priv->rate));
}

static int
get_view_increment (MarlinMarkerView *view)
{
	switch (view->priv->display) {
	case MARLIN_DISPLAY_FRAMES:
		return get_frames_increment (view);

	case MARLIN_DISPLAY_TIME_LONG:
	case MARLIN_DISPLAY_SECONDS:
		return get_time_increment (view);

		/* FIXME */
	case MARLIN_DISPLAY_TIME_FRAMES:
		return get_time_increment (view);

	case MARLIN_DISPLAY_BEATS:
		return get_beat_increment (view);

	default:
		g_assert_not_reached ();
	}

	/* Cannot get here */
	return 0;
}

#if 0
static const char *
get_view_format (double inc)
{
	if (((int)(inc * 100.0) % 100) > 0) {
		return "%.3f";
	} else {
		return "%.0f";
	}
}
#endif

static char *
get_view_text (MarlinMarkerView *view,
	       guint64 position)
{
	guint64 ms;
	char *str, *ret_str;

	switch (view->priv->display) {
	case MARLIN_DISPLAY_FRAMES:
		return g_strdup_printf ("%llu", position);

	case MARLIN_DISPLAY_TIME_LONG:
		ms = marlin_frames_to_ms (position, view->priv->rate);
		str = marlin_ms_to_time_string (ms);
		ret_str = g_strdup_printf ("<small><small>%s</small></small>", str);
		g_free (str);
		return ret_str;

	case MARLIN_DISPLAY_SECONDS:
		ms = marlin_frames_to_ms (position, view->priv->rate);
		if (((int)(ms / 10.0) % 100) > 0) {
			return g_strdup_printf ("%.3f", (double) ms / 1000.0);
		} else {
			return g_strdup_printf ("%.0f", (double) ms / 1000.0);
		}

	case MARLIN_DISPLAY_TIME_FRAMES:
		ms = marlin_frames_to_ms (position, view->priv->rate);
		str = marlin_ms_to_time_frame_string (ms, view->priv->rate);
		ret_str = g_strdup_printf ("<small><small>%s</small></small>", str);
		g_free (str);
		return ret_str;

	case MARLIN_DISPLAY_BEATS: {
		guint64 fpm = view->priv->rate * 60;
		guint64 fpb = fpm / view->priv->bpm;

		return g_strdup_printf ("%llu", position / fpb);
	}

	default:
		g_assert_not_reached ();
	}

	return "";
}

static void
draw_ticks (MarlinMarkerView *view,
	    GdkRectangle *area,
	    GtkStateType state_type)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	GtkWidget *widget = (GtkWidget *) view;
	GdkGC *gc;
	int width, height;
	int xthickness, ythickness;
	guint64 ruler_upper, ruler_lower, start, first_mark;
	guint64 point, width_in_frames;
	int increment, gap, s;
	int th, tw;
	char *unit_str;

	xthickness = widget->style->xthickness;
	ythickness = widget->style->ythickness;

	width = widget->allocation.width;
	height = widget->allocation.height - ythickness * 2;

	gc = widget->style->fg_gc[GTK_STATE_NORMAL];

	gtk_paint_box (widget->style, view->priv->backing_store,
		       GTK_STATE_NORMAL, GTK_SHADOW_OUT,
		       NULL, widget, "hruler",
		       0, 0,
		       widget->allocation.width, widget->allocation.height);

	gdk_draw_line (view->priv->backing_store, gc,
		       xthickness,
		       (height - 14) + ythickness,
		       widget->allocation.width - xthickness,
		       (height - 14) + ythickness);

	ruler_lower = priv->xofs * priv->fpp;
	ruler_upper = (priv->xofs + width) * priv->fpp;

/* 	desired = CLAMP (priv->fpp, 1, 16384); */

	increment = get_view_increment (view);
	gap = increment / 10;

	start = (priv->xofs * priv->fpp);
	s = start % increment;
	if (s == 0) {
		first_mark = 0;
	} else {
		first_mark = start - s;
	}

	width_in_frames = (width + priv->xofs) * priv->fpp;
	for (point = first_mark; point <= width_in_frames; point += increment) {
		int mark;
		int j;
		guint64 count;

		mark = point / priv->fpp;
		gdk_draw_line (view->priv->backing_store, gc,
			       mark - priv->xofs, (height - 14) + ythickness,
			       mark - priv->xofs, (height - 14) - 4 + ythickness);

		for (j = 1; j < 10; j++) {
			int pos = (point + (j * gap)) / priv->fpp;

			gdk_draw_line (view->priv->backing_store, gc,
				       pos - priv->xofs,
				       (height - 14) + ythickness,
				       pos - priv->xofs,
				       (height - 14) - 2 + ythickness);
		}

		/* Draw the text */
		count = point;
		if (count % increment != 0) {
			count = ((count / increment) + 1) * increment;
		}

		unit_str = get_view_text (view, count);
		pango_layout_set_markup (view->priv->layout, unit_str, -1);
		g_free (unit_str);
		pango_layout_get_size (view->priv->layout, &tw, &th);

		gtk_paint_layout (widget->style, view->priv->backing_store,
				  GTK_WIDGET_STATE (widget), FALSE,
				  NULL, widget, "hruler",
				  mark - priv->xofs - (PANGO_PIXELS (tw) / 2),
				  (widget->allocation.height - 24 - (widget->style->ythickness * 2)) - (PANGO_PIXELS (th)) / 2 + widget->style->ythickness,
				  view->priv->layout);
	}
}

static void
draw_arrow (GtkWidget *widget,
	    cairo_t   *cr,
	    int        x,
	    int        y,
	    int        size,
	    gboolean   selected)
{
	int half_size = size / 2;

	cairo_save (cr);

	if (selected) {
		gdk_cairo_set_source_color (cr, &widget->style->fg[GTK_STATE_NORMAL]);
	} else {
		gdk_cairo_set_source_color (cr, &widget->style->base[GTK_STATE_NORMAL]);
	}
	cairo_set_line_width (cr, 1);

	cairo_move_to (cr, x, y);
	cairo_line_to (cr, x - half_size, y - size);
	cairo_line_to (cr, x + half_size, y - size);
	cairo_line_to (cr, x, y);

	cairo_fill_preserve (cr);

	gdk_cairo_set_source_color (cr, &widget->style->fg[GTK_STATE_NORMAL]);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
draw_markers (MarlinMarkerView *view,
	      GdkRectangle     *area,
	      GtkStateType      state_type)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	GtkWidget *widget = (GtkWidget *) view;
	cairo_t *cr;
	GList *m;
	guint64 first, last;

	first = priv->xofs * priv->fpp;
	last = first + (widget->allocation.width * priv->fpp);

	cr = gdk_cairo_create (widget->window);
	for (m = priv->markers; m; m = m->next) {
		struct _ViewMarker *marker = m->data;
		int x;

		if (marker->hidden) {
			continue;
		}

		if (marker->real_position < first ||
		    marker->real_position > last) {
			continue;
		}

		x = ((int)(marker->real_position / priv->fpp)) - priv->xofs;
		draw_arrow (widget, cr, MAX (x, 0),
			    widget->allocation.height,
			    EXPANDER_SIZE, (marker == priv->current_marker));

		if (GTK_WIDGET_HAS_FOCUS (widget) &&
		    marker == view->priv->focus_marker) {
			gtk_paint_focus (widget->style, widget->window,
					 state_type, area, widget,
					 "marlin-marker-view",
					 (marker->real_position / view->priv->fpp) - view->priv->xofs - EXPANDER_SIZE_HALF - 2,
					 widget->allocation.height - (widget->style->ythickness + EXPANDER_SIZE) - 2,
					 EXPANDER_SIZE + 4,
					 EXPANDER_SIZE + 4);
		}
	}

	if (view->priv->in_drag_marker) {
		/* FIXME: Should we only draw this if the floating marker
		   is inside area? */
		draw_arrow (widget, cr,
			    (priv->floating_marker_pos / priv->fpp) - priv->xofs,
			    widget->allocation.height, EXPANDER_SIZE, TRUE);
	}

	cairo_destroy (cr);
}

static void
draw_region (GtkWidget *widget,
	     cairo_t   *cr,
	     int        region_start,
	     int        region_end,
	     int        y)
{
	int midpoint = (region_end - region_start) / 2;

	cairo_save (cr);

	cairo_set_line_width (cr, 1);

	cairo_save (cr);
	/* We want to make an ellipse */
 	cairo_translate (cr, region_start + midpoint, y);
 	cairo_scale (cr, (double)(region_end - region_start) / 2.0,
		     (double) EXPANDER_SIZE - 4);

	cairo_arc_negative (cr, 0, 0, 1, 0, -M_PI);

	gdk_cairo_set_source_color (cr, &widget->style->mid[GTK_STATE_NORMAL]);
	cairo_fill_preserve (cr);

	cairo_restore (cr);
	gdk_cairo_set_source_color (cr, &widget->style->dark[GTK_STATE_NORMAL]);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
draw_grab_handle (GtkWidget *widget,
		  cairo_t   *cr,
		  int        x,
		  int        y,
		  int        handle_width)
{
	cairo_save (cr);

	cairo_set_antialias (cr, CAIRO_ANTIALIAS_NONE);
	cairo_set_line_width (cr, 1);

	cairo_rectangle (cr, x - (handle_width / 2),
			 y - (HANDLE_HEIGHT / 2), handle_width, HANDLE_HEIGHT);

	gdk_cairo_set_source_color (cr, &widget->style->base[GTK_STATE_NORMAL]);
	cairo_fill_preserve (cr);

	gdk_cairo_set_source_color (cr, &widget->style->fg[GTK_STATE_NORMAL]);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
draw_regions (MarlinMarkerView *view,
	      GdkRectangle     *area,
	      GtkStateType      state)
{
	GtkWidget *widget = (GtkWidget *) view;
	MarlinMarkerViewPrivate *priv = view->priv;
	MarlinListNode *r;
	cairo_t *cr;
	guint64 first_frame, last_frame;
	int y;

	/* We don't want to draw regions if there are no markers */
	if (priv->markers == NULL) {
		return;
	}

	first_frame = priv->xofs * priv->fpp;
	g_object_get (priv->sample,
		      "total-frames", &last_frame,
		      NULL);

	y = widget->allocation.height - EXPANDER_SIZE_HALF;

	cr = gdk_cairo_create (widget->window);

	for (r = priv->regions->first; r; r = r->next) {
		struct _Region *region = (struct _Region *) r;
		guint64 region_start, region_end, midpoint;
		int handle_width;

		if (region->start) {
			region_start = region->start->position;
		} else {
			region_start = 0;
		}

		if (region->end) {
			region_end = region->end->position - 1;
		} else {
			g_object_get (priv->sample,
				      "total_frames", &region_end,
				      NULL);
			region_end--;
		}

		draw_region (widget, cr,
			     (region_start / priv->fpp) - priv->xofs,
			     (region_end / priv->fpp) - priv->xofs,
			     widget->allocation.height);

		/* Draw grab handle */
		midpoint = region_start + ((region_end - region_start) / 2);
		handle_width = MIN (region_end - region_start - 4,
				    HANDLE_WIDTH);

		draw_grab_handle (widget, cr,
				  (midpoint / priv->fpp) - priv->xofs,
				  y, handle_width);
	}

	cairo_destroy (cr);
}

static gboolean
expose_event (GtkWidget      *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);

		draw_ticks (view, &event->area, GTK_STATE_NORMAL);
		gdk_draw_drawable (widget->window,
				   view->priv->non_gr_exp_gc,
				   view->priv->backing_store,
				   0, 0, 0, 0,
				   widget->allocation.width,
				   widget->allocation.height);
		draw_regions (view, &event->area, GTK_STATE_NORMAL);
		draw_markers (view, &event->area, GTK_STATE_NORMAL);
	}

	return FALSE;
}

static void
marker_set_hidden (MarlinMarkerView *view,
		   struct _ViewMarker *marker,
		   gboolean hidden)
{
	GdkRectangle area;
	GtkWidget *widget = GTK_WIDGET (view);
	int x;

	marker->hidden = hidden;

	x = ((marker->real_position / view->priv->fpp) - EXPANDER_SIZE_HALF) - 2;

	area.x = MAX (x, 0);
	area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
	area.width = EXPANDER_SIZE + 4;
	area.height = EXPANDER_SIZE + 4;

	gdk_window_invalidate_rect (widget->window, &area, FALSE);
}

/* The floating marker is a "virtual" marker drawn on the
   marker view when a real marker is being dragged.
   This virtual marker only exists as a position in the view
   and doesn't have any real representation as a struct _ViewMarker
   or in the MarlinMarkerView */
static void
redraw_floating_marker (MarlinMarkerView *view,
			guint64 new_position)
{
	GtkWidget *widget = GTK_WIDGET (view);
	GdkRectangle area;
	guint64 total_frames;
	int x;

	x = ((view->priv->floating_marker_pos / view->priv->fpp) - view->priv->xofs - EXPANDER_SIZE_HALF) - 2;

	area.x = MAX (x, 0);
	area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
	area.width = EXPANDER_SIZE + 4;
	area.height = EXPANDER_SIZE + 4;

	gdk_window_invalidate_rect (widget->window, &area, FALSE);

	g_object_get (G_OBJECT (view->priv->sample),
		      "total_frames", &total_frames,
		      NULL);

	view->priv->floating_marker_pos = new_position < total_frames ?
		new_position : total_frames - view->priv->fpp;

	x = ((view->priv->floating_marker_pos / view->priv->fpp) - view->priv->xofs - EXPANDER_SIZE_HALF) - 2;

	area.x = MAX (x, 0);
	area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE - 2;
	area.width = EXPANDER_SIZE + 4;
	area.height = EXPANDER_SIZE + 4;

	gdk_window_invalidate_rect (widget->window, &area, FALSE);
}

static gboolean
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	MarlinMarkerView *view;
	struct _ViewMarker *vm;
	guint64 real;

	view = MARLIN_MARKER_VIEW (widget);

	real = (event->x + view->priv->xofs) * view->priv->fpp;

	gtk_widget_grab_focus (widget);

	switch (event->button) {
	case 1:
		if (view->priv->kb_grab) {
			/* Drop marker */
			view->priv->kb_grab = FALSE;
			marlin_undo_manager_context_end (view->priv->undo,
							 view->priv->kb_ctxt);
			view->priv->kb_ctxt = NULL;
		}

		if (find_marker (view, real, &vm)) {
			GdkCursor *grab_hand;

			change_focus_marker (view, vm);

			/* In drag */
			view->priv->in_drag_marker = TRUE;
			view->priv->drag_marker = vm;

			marker_set_hidden (view, vm, TRUE);
			redraw_floating_marker (view, real);

			grab_hand = marlin_cursor_get (widget, HAND_CLOSED);

			gdk_pointer_grab (widget->window, FALSE,
					  GDK_BUTTON_RELEASE_MASK |
					  GDK_BUTTON1_MOTION_MASK, NULL,
					  grab_hand, event->time);
			gdk_cursor_unref (grab_hand);
		}
		break;

	case 2:
		break;

	case 3:
#if 0
		gtk_menu_popup (GTK_MENU (view->priv->popup), NULL, NULL,
				NULL, NULL, 3, event->time);
#endif
		break;
	}

	return FALSE;
}

static guint64
snap_to_tick (MarlinMarkerView *view,
	      guint64 position)
{
	int increment, gap;

	increment = get_view_increment (view);
	gap = increment / 10;

	position = ((position + (gap / 2)) / gap) * gap;

	return position;
}

#define MARKER_REMOVE_THRESHOLD 10
static gboolean
button_release_event (GtkWidget *widget,
		      GdkEventButton *event)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);
	MarlinMarkerViewPrivate *priv = view->priv;
	int x;
	guint64 position, total;
	struct _ViewMarker *vm;

	x = MAX (0, event->x);
	position = (x + view->priv->xofs) * view->priv->fpp;

	g_object_get (priv->sample,
		      "total-frames", &total,
		      NULL);
	position = position < total ? position : total - view->priv->fpp;

	if (view->priv->snap) {
		position = snap_to_tick (view, position);
	}

	switch (event->button) {
	case 1:
		g_object_get (G_OBJECT (view->priv->sample),
			      "total_frames", &total,
			      NULL);

		if (view->priv->in_drag_marker) {
			MarlinUndoContext *ctxt;

			gdk_pointer_ungrab (event->time);
			if (event->x < -MARKER_REMOVE_THRESHOLD ||
			    event->x > (widget->allocation.width + MARKER_REMOVE_THRESHOLD) ||
			    event->y < -MARKER_REMOVE_THRESHOLD ||
			    event->y > (widget->allocation.height + MARKER_REMOVE_THRESHOLD)) {
				ctxt = marlin_undo_manager_context_begin (priv->undo,
									  _("Remove Marker"));
				marlin_marker_model_remove_marker (priv->model,
								   priv->drag_marker->marker,
								   ctxt);
			} else {
				ctxt = marlin_undo_manager_context_begin (priv->undo,
									  _("Move Marker"));
				marlin_marker_model_move_marker (priv->model,
								 priv->drag_marker->marker,
								 position,
								 ctxt);
			}

			marlin_undo_manager_context_end (priv->undo, ctxt);

			marker_set_hidden (view, view->priv->drag_marker, FALSE);
			view->priv->in_drag_marker = FALSE;
			view->priv->drag_marker = NULL;

			redraw_floating_marker (view, position);
			return FALSE;
		}

		if (position > total) {
			return FALSE;
		}

		if (find_marker (view, position, &vm) == FALSE) {
			MarlinUndoContext *ctxt;

			ctxt = marlin_undo_manager_context_begin (priv->undo,
								  _("Add Marker"));

			marlin_marker_model_add_marker (view->priv->model, position, NULL, ctxt);
			marlin_undo_manager_context_end (priv->undo, ctxt);
		}

		return FALSE;

	case 2:
		break;

	case 3:
		break;
	}

	return FALSE;
}

static void
dragging_marker (MarlinMarkerView *view,
		 GdkEventMotion *event)
{
	MarlinMarkerViewPrivate *priv = view->priv;
	guint64 position;
	int x;

	x = MAX (event->x, 0);
	position = (x + priv->xofs) * priv->fpp;

	if (view->priv->snap) {
		position = snap_to_tick (view, position);
	}
	redraw_floating_marker (view, position);
}

static gboolean
motion_notify_event (GtkWidget *widget,
		     GdkEventMotion *event)
{
	MarlinMarkerView *view;
	MarlinMarkerViewPrivate *priv;
	struct _ViewMarker *vm;
	struct _Region *region;
	guint64 px;

	view = MARLIN_MARKER_VIEW (widget);
	priv = view->priv;

	if (priv->in_drag_marker) {
		dragging_marker (view, event);
		return FALSE;
	}

	if (event->y < (widget->allocation.height - (14 + widget->style->ythickness))) {
		if (priv->current_marker != NULL) {
			if (GTK_WIDGET_DRAWABLE (widget)) {
				GdkRectangle area;

				area.x = (priv->current_marker->real_position / view->priv->fpp) - 5;
				area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE;
				area.width = EXPANDER_SIZE;
				area.height = EXPANDER_SIZE;

				gdk_window_invalidate_rect (widget->window, &area, FALSE);
			}
		}

		priv->current_marker = NULL;
		return TRUE;
	}

	px = (priv->xofs + event->x) * priv->fpp;

	if (find_marker (view, px, &vm)) {
		GdkCursor *hand = marlin_cursor_get (widget, HAND_OPEN);

		gdk_window_set_cursor (widget->window, hand);
		gdk_cursor_unref (hand);

		priv->current_marker = vm;

		/* Draw it in a different colour */
		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;
			int x;

			x = (vm->real_position / view->priv->fpp) - EXPANDER_SIZE_HALF;

			area.x = MAX (x, 0);
			area.y = widget->allocation.height - EXPANDER_SIZE;
			area.width = EXPANDER_SIZE;
			area.height = EXPANDER_SIZE;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		g_signal_emit (G_OBJECT (view), signals[ENTER_MARKER], 0, (gpointer) vm->marker);
	} else if (priv->current_marker != NULL) {
		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;
			int x;

			x = (priv->current_marker->real_position / view->priv->fpp) - 5;

			area.x = MAX (x, 0);
			area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE;
			area.width = EXPANDER_SIZE;
			area.height = EXPANDER_SIZE;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}
		gdk_window_set_cursor (widget->window, NULL);
		priv->current_marker = NULL;

		/* Out of a marker so turn the menus off again */
		g_signal_emit (G_OBJECT (view), signals[LEAVE_MARKER], 0);
	} if (find_region (view, px, &region)) {

	}else {
		gdk_window_set_cursor (widget->window, NULL);
		priv->current_marker = NULL;
	}

	return FALSE;
}

static gboolean
leave_notify_event (GtkWidget *widget,
		    GdkEventCrossing *event)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);
	MarlinMarkerViewPrivate *priv = view->priv;

	if (priv->current_marker != NULL) {
		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;
			int x;

			/* Redraw the area around the current marker so that
			   it will be unfocused */
			x = ((priv->current_marker->real_position / view->priv->fpp) - priv->xofs - EXPANDER_SIZE_HALF) - 2;

			area.x = MAX (x, 0);
			area.y = widget->allocation.height - widget->style->ythickness - EXPANDER_SIZE;
			area.width = EXPANDER_SIZE + 4;
			area.height = EXPANDER_SIZE + 4;

			if (priv->current_marker == priv->focus_marker) {
				area.x = MAX (0, ((int) area.x) - 2);
				area.y -= 2;
				area.width += 4;
				area.height += 4;
			}

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		/* Set the current marker to NULL so that it loses focus */
		priv->current_marker = NULL;
	}

	return FALSE;
}

static gboolean
focus_in_event (GtkWidget *widget,
		GdkEventFocus *event)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);

	change_focus_marker (view, view->priv->real_focus);

	return FALSE;
}

static gboolean
focus_out_event (GtkWidget *widget,
		 GdkEventFocus *event)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);

	view->priv->real_focus = view->priv->focus_marker;
	change_focus_marker (view, NULL);

	if (view->priv->kb_grab) {
		/* Drop marker */
		view->priv->kb_grab = FALSE;
		marlin_undo_manager_context_end (view->priv->undo,
						 view->priv->kb_ctxt);
		view->priv->kb_ctxt = NULL;
	}

	return FALSE;
}

static gboolean
key_press_event (GtkWidget *widget,
		 GdkEventKey *event)
{
	MarlinMarkerView *view = MARLIN_MARKER_VIEW (widget);
	MarlinMarkerViewPrivate *priv = view->priv;
	MarlinMarker *fm;
	MarlinUndoContext *ctxt;
	GList *focus_marker, *new_focus;
	guint64 pos;

	switch (event->keyval) {
	case GDK_Left:
		if (view->priv->kb_grab) {
			pos = view->priv->focus_marker->real_position -
				view->priv->fpp;

			if (event->state & GDK_SHIFT_MASK) {
				guint64 new_pos;

				new_pos = snap_to_tick (view, pos);
				/* If the position was snapped to the right
				   then we need to move to the previous tick */
				if (new_pos >= pos) {
					int increment, gap;

					increment = get_view_increment (view);
					gap = increment / 10;

					pos = new_pos - gap;
				}
			}

			marlin_marker_model_move_marker (view->priv->model,
							 view->priv->focus_marker->marker,
							 pos,
							 view->priv->kb_ctxt);
			return TRUE;
		}

		/* Select previous marker */
		focus_marker = g_list_find (view->priv->markers,
					    view->priv->focus_marker);
		if (focus_marker == NULL) {
			/* Erk? */
			return FALSE;
		}

		new_focus = focus_marker->prev;
		if (new_focus == NULL) {
			/* We've passed the first marker */
			return FALSE;
		}

		change_focus_marker (view, (struct _ViewMarker *) new_focus->data);
		break;

	case GDK_Right:
		if (view->priv->kb_grab) {
			pos = view->priv->focus_marker->real_position +
				view->priv->fpp;

			if (event->state & GDK_SHIFT_MASK) {
				guint64 new_pos;

				new_pos = snap_to_tick (view, pos);
				/* If the position was snapped to the left
				   then we need to move to the next tick */
				if (new_pos <= pos) {
					int increment, gap;

					increment = get_view_increment (view);
					gap = increment / 10;

					pos = new_pos + gap;
				}
			}

			marlin_marker_model_move_marker (view->priv->model,
							 view->priv->focus_marker->marker,
							 pos,
							 view->priv->kb_ctxt);
			return TRUE;
		}

		/* Select next marker */
		focus_marker = g_list_find (view->priv->markers,
					    view->priv->focus_marker);
		if (focus_marker == NULL) {
			/* Again, I say "Erk?" */
			return FALSE;
		}

		new_focus = focus_marker->next;
		if (new_focus == NULL) {
			/* Passed the last marker out into the world */
			return FALSE;
		}

		change_focus_marker (view, (struct _ViewMarker *) new_focus->data);
		break;

	case GDK_space:
		/* Grab or drop marker */
		if (view->priv->kb_grab) {
			/* Drop */
			view->priv->kb_grab = FALSE;
			marlin_undo_manager_context_end (view->priv->undo,
							 view->priv->kb_ctxt);
			view->priv->kb_ctxt = NULL;
		} else {
			view->priv->kb_ctxt = marlin_undo_manager_context_begin (view->priv->undo, _("Move Marker"));
			view->priv->kb_grab = TRUE;
		}

		break;

	case GDK_BackSpace:
		/* Select next marker */
		focus_marker = g_list_find (view->priv->markers,
					    view->priv->focus_marker);
		if (focus_marker == NULL) {
			/* Again, I say "Erk?" */
			return FALSE;
		}

		fm = ((struct _ViewMarker *) focus_marker->data)->marker;
		new_focus = focus_marker->next;
		if (new_focus) {
			struct _ViewMarker *vm = new_focus->data;
			change_focus_marker (view, vm);
		}

		ctxt = marlin_undo_manager_context_begin (priv->undo,
							  _("Remove Marker"));
		marlin_marker_model_remove_marker (priv->model, fm, ctxt);
		marlin_undo_manager_context_end (priv->undo, ctxt);

		break;

	default:
		return FALSE;
	}

	return TRUE;
}

static void
scroll_to (MarlinMarkerView *view,
	   int x)
{
	MarlinMarkerViewPrivate *priv;
	GdkRectangle area;
	GtkWidget *widget;
	int xofs;
	int width, height;
	int src_x, dest_x;

	widget = GTK_WIDGET (view);
	priv = view->priv;

	xofs = x - priv->xofs;

	if (xofs == 0) {
		return;
	}

	priv->xofs = x;

	if (GTK_WIDGET_DRAWABLE (widget) == FALSE) {
		return;
	}

	width = widget->allocation.width;
	height = widget->allocation.height;

	if (abs (xofs) >= width) {
		goto redraw;
	}

	/* Copy the window area */
	src_x = xofs < 0 ? 0 : xofs;
	dest_x = xofs < 0 ? -xofs : 0;

 redraw:
	area.x = 0;
	area.y = 0;
	area.width = width;
	area.height = height;

	gdk_window_invalidate_rect (widget->window, &area, FALSE);
}

static void
adjustment_changed_cb (GtkAdjustment *adj,
		       MarlinMarkerView *view)
{
	scroll_to (view, view->priv->hadj->value);
}

static void
set_scroll_adjustments (MarlinMarkerView *view,
			GtkAdjustment *hadj,
			GtkAdjustment *vadj)
{
	MarlinMarkerViewPrivate *priv;
	gboolean need_adjust;

	priv = view->priv;

	if (hadj == NULL ||
	    vadj == NULL) {
		return;
	}

	if (priv->hadj && priv->hadj != hadj) {
		g_signal_handlers_disconnect_matched (G_OBJECT (priv->hadj),
						      G_SIGNAL_MATCH_DATA,
						      0, 0, NULL,
						      NULL, view);
		g_object_unref (G_OBJECT (priv->hadj));
	}

	if (priv->vadj && priv->vadj != vadj) {
		g_object_unref (G_OBJECT (priv->vadj));
	}

	need_adjust = FALSE;

	if (priv->hadj != hadj) {
		priv->hadj = hadj;
		g_object_ref (G_OBJECT (hadj));

		g_signal_connect (G_OBJECT (priv->hadj), "value-changed",
				  G_CALLBACK (adjustment_changed_cb), view);
		need_adjust = TRUE;
	}

	if (priv->vadj != vadj) {
		priv->vadj = vadj;
		g_object_ref (G_OBJECT (priv->vadj));
	}

	if (need_adjust == TRUE) {
		adjustment_changed_cb (NULL, view);
	}
}

static void
marlin_marker_view_class_init (MarlinMarkerViewClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	object_class->finalize = finalize;
	object_class->dispose = dispose;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->size_allocate = size_allocate;
	widget_class->size_request = size_request;
	widget_class->realize = realize;
	widget_class->unrealize = unrealize;
	widget_class->expose_event = expose_event;
	widget_class->button_press_event = button_press_event;
	widget_class->button_release_event = button_release_event;
	widget_class->motion_notify_event = motion_notify_event;
	widget_class->leave_notify_event = leave_notify_event;
	widget_class->focus_in_event = focus_in_event;
	widget_class->focus_out_event = focus_out_event;
	widget_class->key_press_event = key_press_event;

	klass->set_scroll_adjustments = set_scroll_adjustments;

	g_type_class_add_private (object_class, sizeof (MarlinMarkerViewPrivate));

	g_object_class_install_property (object_class,
					 PROP_MODEL,
					 g_param_spec_object ("model",
							      "", "",
							      MARLIN_MARKER_MODEL_TYPE,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_FRAMES_PER_PIXEL,
					 g_param_spec_uint ("frames_per_pixel",
							    "", "",
							    0, G_MAXUINT, 0,
							    G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
					 PROP_SAMPLE,
					 g_param_spec_object ("sample",
							      "", "",
							      MARLIN_SAMPLE_TYPE,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_SNAP_TO_TICKS,
					 g_param_spec_boolean ("snap_to_ticks",
							       "", "",
							       FALSE,
							       G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_DISPLAY_TYPE,
					 g_param_spec_enum ("display_type",
							    "", "",
							    MARLIN_TYPE_DISPLAY,
							    MARLIN_DISPLAY_FRAMES,
							    G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_UNDO_MANAGER,
					 g_param_spec_object ("undo_manager",
							      "", "",
							      MARLIN_UNDO_MANAGER_TYPE,
							      G_PARAM_WRITABLE));

	gtk_widget_class_install_style_property (widget_class,
						 g_param_spec_int ("expander_size",
								   "", "",
								   0, G_MAXINT, 10,
								   G_PARAM_READABLE));
	widget_class->set_scroll_adjustments_signal =
		g_signal_new ("set_scroll_adjustments",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MarlinMarkerViewClass, set_scroll_adjustments),
			      NULL, NULL,
			      marlin_marshal_VOID__OBJECT_OBJECT,
			      G_TYPE_NONE, 2,
			      GTK_TYPE_ADJUSTMENT,
			      GTK_TYPE_ADJUSTMENT);

	signals[MOVE_CURSOR] = g_signal_new ("move-cursor",
					     G_TYPE_FROM_CLASS (klass),
					     G_SIGNAL_RUN_FIRST |
					     G_SIGNAL_NO_RECURSE,
					     G_STRUCT_OFFSET (MarlinMarkerViewClass, move_cursor),
					     NULL, NULL,
					     marlin_marshal_VOID__UINT64,
					     G_TYPE_NONE,
					     1, G_TYPE_UINT64);
	signals[ENTER_MARKER] = g_signal_new ("enter-marker",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinMarkerViewClass, enter_marker),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__POINTER,
					      G_TYPE_NONE,
					      1, G_TYPE_POINTER);
	signals[LEAVE_MARKER] = g_signal_new ("leave-marker",
					      G_TYPE_FROM_CLASS (klass),
					      G_SIGNAL_RUN_FIRST |
					      G_SIGNAL_NO_RECURSE,
					      G_STRUCT_OFFSET (MarlinMarkerViewClass, leave_marker),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__VOID,
					      G_TYPE_NONE,
					      0);
}

static void
marlin_marker_view_init (MarlinMarkerView *view)
{
	MarlinMarkerViewPrivate *priv;

	GTK_WIDGET_SET_FLAGS (view, GTK_CAN_FOCUS);

	priv = GET_PRIVATE (view);
	view->priv = priv;

	priv->marker_to_view = g_hash_table_new (NULL, NULL);
	priv->position_to_markers = g_hash_table_new (NULL, NULL);
	priv->fpp = DEFAULT_FRAMES_PER_PIXEL;
	priv->display = MARLIN_DISPLAY_FRAMES;
	priv->layout = gtk_widget_create_pango_layout (GTK_WIDGET (view), "0");
	priv->bpm = MARLIN_DEFAULT_BPM;

	priv->regions = marlin_list_new (region_free);
}

/**
 * marlin_marker_view_new:
 * @model: The #MarlinMarkerModel that we use.
 * @sample: The #MarlinSample that we use.
 *
 * Creates a new #MarlinMarkerView based on @model and @sample.
 *
 * Return value: Newly created #MarlinMarkerView.
 */
MarlinMarkerView *
marlin_marker_view_new (MarlinMarkerModel *model,
			MarlinSample *sample)
{
	return g_object_new (MARLIN_MARKER_VIEW_TYPE,
			     "model", model,
			     "sample", sample,
			     NULL);
}

/**
 * marlin_marker_view_get_selected_marker:
 * @view: The #MarlinMarkerView.
 *
 * Gets the selected #MarlinMarker.
 *
 * Return value: A #MarlinMarker
 */
MarlinMarker *
marlin_marker_view_get_selected_marker (MarlinMarkerView *view)
{
	if (view->priv->current_marker) {
		return view->priv->current_marker->marker;
	} else {
		return NULL;
	}
}
