#include <marlin/marlin-grid.h>

#include <gtk/gtk.h>

int 
main (int argc,
      char **argv)
{
	GtkWidget *window, *grid;
	MarlinGridCurve *curve;
	MarlinGridPoint *point, *slave;
	MarlinRange range;

	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (window), 12);
	gtk_window_set_default_size (GTK_WINDOW (window), 300, 250);
	
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (gtk_main_quit), NULL);

	grid = g_object_new (MARLIN_GRID_TYPE, NULL);
	gtk_container_add (GTK_CONTAINER (window), grid);

	range.start = 65342;
	range.finish = 1048237;

	curve = marlin_grid_curve_new (&range);
	curve->colour.red = 65535;
	curve->colour.green = 0;
	curve->colour.blue = 0;

	slave = marlin_grid_point_new (65342, 0.5);
	marlin_grid_curve_add_point (curve, slave, MARLIN_GRID_POINT_FREE);

	point = marlin_grid_point_new (100000, 0.5);
	marlin_grid_point_set_slave (point, slave);

	marlin_grid_curve_add_point (curve, point, MARLIN_GRID_POINT_FREE);

	marlin_grid_add_curve (MARLIN_GRID (grid), curve);

	range.start = 95000;
	range.finish = 500000;

	curve = marlin_grid_curve_new (&range);
	curve->colour.red = 0;
	curve->colour.green = 0;
	curve->colour.blue = 65535;

	slave = marlin_grid_point_new (95000, 0.5);
	marlin_grid_curve_add_point (curve, slave, MARLIN_GRID_POINT_FREE);

	point = marlin_grid_point_new (250000, 0.5);
	marlin_grid_point_set_slave (point, slave);
	marlin_grid_curve_add_point (curve, point, MARLIN_GRID_POINT_FREE);

	point = marlin_grid_point_new (450000, 0.5);
	marlin_grid_curve_add_point (curve, point, MARLIN_GRID_POINT_FREE);

	slave = marlin_grid_point_new (500000, 0.5);
	marlin_grid_point_set_slave (point, slave);
	marlin_grid_curve_add_point (curve, slave, MARLIN_GRID_POINT_FREE);

	marlin_grid_add_curve (MARLIN_GRID (grid), curve);

	gtk_widget_show_all (window);

	gtk_main ();

	return 0;
}
