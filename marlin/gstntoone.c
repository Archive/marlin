/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *  Based one stereosplit: Richard Boulton <richard@tartarus.org>
 *  Based on stereo2mono: Zaheer Merali <zaheer@bellworldwide.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gst/gst.h>

#define GST_TYPE_NTOONE (gst_ntoone_get_type ())
#define GST_NTOONE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_NTOONE, GstNToOne))
#define GST_NTOONE_class(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_NTOONE, GstNToOneClass))
#define GST_IS_NTOONE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_NTOONE))
#define GST_IS_NTOONE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_NTOONE))

typedef struct _GstNToOne GstNToOne;
typedef struct _GstNToOneClass GstNToOneClass;

struct _GstNToOne {
	GstElement element;

	GstPad *srcpad;

	int channels;
	int width;
	guint64 offset;
	
	GList *sinkpads;
};

struct _GstNToOneClass {
	GstElementClass parent_class;
};

struct _buffer_data {
	GstBuffer *buf;
	gint16 *data;
	guint size;
};

static GstElementDetails ntoone_details = {
	"N Channel combiner",
	"Filter/Audio/Conversion",
	"Combines N mono PCM streams into one N Channel PCM stream",
	VERSION,
	"Iain <iain@prettypeople.org>",
	"Copyright (C) 2002",
};

enum {
	LAST_SIGNAL
};

enum {
	ARG_0,
};

static GstPadTemplate *
ntoone_sink_factory (void)
{
	return gst_pad_template_new (
		"sink_%d",
		GST_PAD_SINK,
		GST_PAD_REQUEST,
		gst_caps_new (
			"int_n_channel_sink",
			"audio/raw",
			gst_props_new (
				"format", GST_PROPS_STRING ("int"),
				"law", GST_PROPS_INT (0),
				"endianness", GST_PROPS_INT (G_BYTE_ORDER),
				"signed", GST_PROPS_BOOLEAN (TRUE),
				"width", GST_PROPS_INT (16),
				"depth", GST_PROPS_INT (16),
				"rate", GST_PROPS_INT_RANGE (4000, 96000),
				"channels", GST_PROPS_INT (1),
				NULL)),
		NULL);
}

static GstPadTemplate *
ntoone_src_factory (void)
{
	return gst_pad_template_new (
		"src",
		GST_PAD_SRC,
		GST_PAD_ALWAYS,
		gst_caps_new (
			"int_n_channel_src",
			"audio/raw",
			gst_props_new (
				"format", GST_PROPS_STRING ("int"),
				"law", GST_PROPS_INT (0),
				"endianness", GST_PROPS_INT (G_BYTE_ORDER),
				"signed", GST_PROPS_BOOLEAN (TRUE),
				"width", GST_PROPS_INT (16),
				"depth", GST_PROPS_INT (16),
				"rate", GST_PROPS_INT_RANGE (4000, 96000),
				"channels", GST_PROPS_INT_RANGE (1, 4),
				NULL)),
		NULL);
}

GType gst_ntoone_get_type (void);
static void gst_ntoone_class_init (GstNToOneClass *klass);
static void gst_ntoone_init (GstNToOne *ntoone);
static void gst_ntoone_loop (GstElement *element);
static void inline gst_ntoone_fast_16bit_chain (gint16 *data,
						int channels,
						struct _buffer_data **mono_data,
						guint numbytes);
static void inline gst_ntoone_fast_8bit_chain (gint8 *data,
					       int channels,
					       struct _buffer_data **mono_data,
					       guint numbytes);

static GstPadTemplate *sinktemplate, *srctemplate;
static GstElementClass *parent_class = NULL;

static GstPadConnectReturn
gst_ntoone_connect (GstPad *pad,
		    GstCaps *caps)
{
	GstNToOne *ntoone;

	ntoone = GST_NTOONE (gst_pad_get_parent (pad));

	g_return_val_if_fail (GST_IS_NTOONE (ntoone), GST_PAD_CONNECT_REFUSED);

	if (!GST_CAPS_IS_FIXED (caps)) {
		return GST_PAD_CONNECT_DELAYED;
	}

	if (pad == ntoone->srcpad) {
		GstCaps *srccaps;
		GList *p;
		
		srccaps = gst_caps_copy (caps);
		gst_caps_set (srccaps, "channels", GST_PROPS_INT (1));

		for (p = ntoone->sinkpads; p; p = p->next) {
			GstPad *pad = p->data;

			if (!gst_pad_try_set_caps (pad, srccaps)) {
				return GST_PAD_CONNECT_REFUSED;
			}
		}
	} else {
#if 0
		/* Set the src and the other sinks to match */
		GstCaps *srccaps, *sinkcaps;
		GList *p;

		srccaps = gst_caps_copy (caps);
		sinkcaps = gst_caps_copy (caps);

		gst_caps_set (srccaps, "channels", GST_PROPS_INT (ntoone->channels));
		gst_caps_set (sinkcaps, "channels", GST_PROPS_INT (1));

		if (!gst_pad_try_set_caps (ntoone->srcpad, srccaps)) {
			return GST_PAD_CONNECT_REFUSED;
		}

		for (p = ntoone->sinkpads; p; p = p->next) {
			GstPad *pad = p->data;

			if (!gst_pad_try_set_caps (pad, sinkcaps)) {
				return GST_PAD_CONNECT_REFUSED;
			}
		}
#endif
	}

	gst_caps_get_int (caps, "width", &ntoone->width);

	return GST_PAD_CONNECT_OK;
}

static GstPad *
request_new_pad (GstElement *element,
		 GstPadTemplate *templ,
		 const char *unused)
{
	char *name;
	GstNToOne *ntoone;
	GstPad *pad;

	ntoone = GST_NTOONE (element);

	if (templ->direction != GST_PAD_SINK) {
		g_warning ("GstNToOne: Request new pad that is not a SINK");
		return NULL;
	}

	name = g_strdup_printf ("sink%d", ntoone->channels);
	pad = gst_pad_new_from_template (templ, name);
	g_free (name);

	gst_element_add_pad (element, pad);
	gst_pad_set_connect_function (pad, gst_ntoone_connect);
	ntoone->sinkpads = g_list_append (ntoone->sinkpads, pad);

	ntoone->channels++;
	return pad;
}

GType
gst_ntoone_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (GstNToOneClass), NULL, NULL,
			(GClassInitFunc) gst_ntoone_class_init, NULL, NULL,
			sizeof (GstNToOne), 0, (GInstanceInitFunc) gst_ntoone_init,
		};

		type = g_type_register_static (GST_TYPE_ELEMENT,
					       "GstNToOne", &info, 0);
	}

	return type;
}

static void
gst_ntoone_class_init (GstNToOneClass *klass)
{
	GstElementClass *element_class;

	element_class = GST_ELEMENT_CLASS (klass);
	element_class->request_new_pad = request_new_pad;
	
	parent_class = g_type_class_ref (GST_TYPE_ELEMENT);
}

static void
gst_ntoone_init (GstNToOne *ntoone)
{
	/* Create the src pad, the sink pads are all request pads */
	ntoone->srcpad = gst_pad_new_from_template (srctemplate, "src");
	gst_pad_set_connect_function (ntoone->srcpad, gst_ntoone_connect);
	gst_element_add_pad (GST_ELEMENT (ntoone), ntoone->srcpad);

	gst_element_set_loop_function (GST_ELEMENT (ntoone), gst_ntoone_loop);
	
	ntoone->width = 0;
	ntoone->channels = 0; /* Channels is the number of sink pads */
}

static struct _buffer_data **
buffer_pull (GstElement *element)
{
	GstNToOne *ntoone;
	struct _buffer_data **bufs;
	GList *p;
	int i;

	ntoone = GST_NTOONE (element);
	bufs = g_new (struct _buffer_data *, ntoone->channels);

	for (i = 0, p = ntoone->sinkpads; p; i++, p = p->next) {
		GstPad *pad = p->data;
		GstBuffer *buf;

		do {
			buf = gst_pad_pull (pad);
			
			if (GST_IS_EVENT (buf)) {
				switch (GST_EVENT_TYPE (buf)) {
				case GST_EVENT_EOS:
					buf = NULL;
					goto end;

				default:
					gst_pad_event_default (pad,
							       GST_EVENT (buf));
					break;
				}
				
				buf = NULL;
			}
		} while (buf == NULL);

	end:
		/* We now have a valid buf */
		if (buf == NULL) {
			bufs[i] = NULL;
		} else {
			bufs[i] = g_new (struct _buffer_data, 1);
			bufs[i]->buf = buf;
			bufs[i]->data = (gint16 *) GST_BUFFER_DATA (buf);
			bufs[i]->size = GST_BUFFER_SIZE (buf);
		}
	}

	/* How to handle a NULL buffer if others aren't NULL... */
	for (i = 0; i < ntoone->channels; i++) {
		if (bufs[i] != NULL) {
			return bufs;
		}
	}

	g_free (bufs);
	return NULL;
}

static guint
find_largest_buffer (int channels,
		     struct _buffer_data **in_bufs)
{
	int i;
	guint largest = 0;
	
	for (i = 0; i < channels; i++) {
		if (in_bufs[i] != NULL) {
			largest = MAX (in_bufs[i]->size, largest);
		}
	}

	return largest;
}

static void
gst_ntoone_loop (GstElement *element)
{
	GstNToOne *ntoone;
	GstBuffer *out_buf;
	struct _buffer_data **in_bufs;
	gint16 *data;
	int i;
	
	ntoone = GST_NTOONE (element);

	in_bufs = buffer_pull (element);

	g_print ("Loop\n");
	while (in_bufs != NULL) {
		guint largest, size;

		g_print ("loop...\n");
		largest = get_largest_buffer (ntoone->channels, in_bufs);
		size = largest * ntoone->channels;
		
		out_buf = gst_buffer_new ();
		GST_BUFFER_DATA (out_buf) = (char *) g_new (gint16, size);
		GST_BUFFER_SIZE (out_buf) = size;
		GST_BUFFER_OFFSET (out_buf) = ntoone->offset;
		GST_BUFFER_TIMESTAMP (out_buf) = ntoone->offset * GST_SECOND;

		data = (gint16 *) GST_BUFFER_DATA (out_buf);
		switch (ntoone->width) {
		case 16:
			gst_ntoone_fast_16bit_chain (data, ntoone->channels,
						     in_bufs, size);
			break;

		case 8:
			gst_ntoone_fast_8bit_chain (GST_BUFFER_DATA (out_buf),
						    ntoone->channels,
						    in_bufs, size);
			break;

		default:
			g_warning ("Caps nego failed\n");
			break;

		}

		gst_pad_push (ntoone->srcpad, out_buf);

		for (i = 0; i < ntoone->channels; i++) {
			if (in_bufs[i] != NULL) {
				gst_buffer_unref (in_bufs[i]->buf);
				g_free (in_bufs[i]);
			}
		}

		g_free (in_bufs);

		in_bufs = buffer_pull (element);
		gst_element_yield (element);
	}

	gst_element_set_eos (element);
}

static void inline
gst_ntoone_fast_16bit_chain (gint16 *data,
			     int channels,
			     struct _buffer_data **mono_data,
			     guint numbytes)
{
	int i, j, k;

	for (i = 0, k = 0; k < numbytes; i+= channels, k++) {
		int pos = i * channels;
		
		for (j = 0; j < channels; j++) {
			if (k >= mono_data[j]->size) {
				data[pos + j] = 0;
			} else {
				data[pos + j] = mono_data[j]->data[k];
			}
		}
	}
}

static void inline
gst_ntoone_fast_8bit_chain (gint8 *data,
			    int channels,
			    struct _buffer_data **mono_data,
			    guint numbytes)
{
	int i, j, k;
	
	for (i = 0, k = 0; k < numbytes; i+= channels, k++) {
		int pos = i * channels;
		
		for (j = 0; j < channels; j++) {
			if (k >= mono_data[j]->size) {
				data[pos + j] = 0;
			} else {
				data[pos + j] = GST_BUFFER_DATA (mono_data[j]->buf)[k];
			}
		}
	}
}

static gboolean
plugin_init (GModule *module,
	     GstPlugin *plugin)
{
	GstElementFactory *factory;

	factory = gst_element_factory_new ("ntoone", GST_TYPE_NTOONE,
					   &ntoone_details);
	g_return_val_if_fail (factory != NULL, FALSE);

	sinktemplate = ntoone_sink_factory ();
	gst_element_factory_add_pad_template (factory, sinktemplate);

	srctemplate = ntoone_src_factory ();
	gst_element_factory_add_pad_template (factory, srctemplate);

	gst_plugin_add_feature (plugin, GST_PLUGIN_FEATURE (factory));

	return TRUE;
}

GstPluginDesc plugin_desc = {
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	"ntoone",
	plugin_init
};
