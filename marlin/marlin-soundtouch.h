/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2007 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_SOUNDTOUCH_H__
#define __MARLIN_SOUNDTOUCH_H__

#include <glib.h>

#ifdef __cplusplus

#include <stdexcept>
#include <stdio.h>
#include <soundtouch/SoundTouch.h>
using namespace soundtouch;
using namespace std;

#ifdef INTEGER_SAMPLES
#error Marlin requires SoundTouch to be compiled with floating point sample support
#endif

#endif
G_BEGIN_DECLS

typedef enum _MarlinSoundtouchSetting {
	MARLIN_SOUNDTOUCH_SETTING_USE_AA_FILTER,
	MARLIN_SOUNDTOUCH_SETTING_AA_FILTER_LENGTH,
	MARLIN_SOUNDTOUCH_SETTING_USE_QUICKSEEK,
	MARLIN_SOUNDTOUCH_SETTING_SEQUENCE_MS,
	MARLIN_SOUNDTOUCH_SETTING_SEEKWINDOW_MS,
	MARLIN_SOUNDTOUCH_SETTING_OVERLAP_MS,
} MarlinSoundtouchSetting;

typedef struct MarlinSoundtouch MarlinSoundtouch;

MarlinSoundtouch *marlin_soundtouch_new (void);
void marlin_soundtouch_free (MarlinSoundtouch *soundtouch);

void marlin_soundtouch_set_channels (MarlinSoundtouch *soundtouch,
				     int               channels);
void marlin_soundtouch_set_sample_rate (MarlinSoundtouch *soundtouch,
					int               sample_rate);

void marlin_soundtouch_set_rate (MarlinSoundtouch *soundtouch, 
				 float             rate);
void marlin_soundtouch_set_rate_change (MarlinSoundtouch *soundtouch,
					float             rate);
void marlin_soundtouch_set_tempo (MarlinSoundtouch *soundtouch,
				  float             tempo);
void marlin_soundtouch_set_tempo_change (MarlinSoundtouch *soundtouch,
					 float             tempo);

void marlin_soundtouch_set_pitch (MarlinSoundtouch *soundtouch,
				  float             pitch);
void marlin_soundtouch_set_pitch_octaves (MarlinSoundtouch *soundtouch,
					  float             pitch);
void marlin_soundtouch_set_pitch_semitones (MarlinSoundtouch *soundtouch,
					    float             pitch);

int marlin_soundtouch_set (MarlinSoundtouch       *soundtouch,
			   MarlinSoundtouchSetting setting,
			   guint                   value);
guint marlin_soundtouch_get (MarlinSoundtouch       *soundtouch,
			     MarlinSoundtouchSetting setting);

void marlin_soundtouch_put_samples (MarlinSoundtouch *soundtouch,
				    float            *samples,
				    int               length);
guint marlin_soundtouch_take_samples (MarlinSoundtouch *soundtouch,
				      float            *samples,
				      guint             max_samples);

void marlin_soundtouch_flush (MarlinSoundtouch *soundtouch);
void marlin_soundtouch_clear (MarlinSoundtouch *soundtouch);

guint marlin_soundtouch_length (MarlinSoundtouch *soundtouch);
guint marlin_soundtouch_unprocessed_length (MarlinSoundtouch *soundtouch);
gboolean marlin_soundtouch_empty (MarlinSoundtouch *soundtouch);

G_END_DECLS

#endif
