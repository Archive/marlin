/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright (C) 2002 Iain Holmes 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-object.h>
#include <marlin/marlin-time-line.h>
#include <marlin/marlin-marshal.h>
#include <marlin/marlin-sample-selection.h>

enum {
	CURSOR_CHANGED,
	PAGE_START_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_TOTAL,
	PROP_PER_PAGE,
	PROP_POSITION,
	PROP_PAGE_START,
	PROP_PER_PIXEL,
	PROP_SELECTION,
};

struct _MarlinTimeLinePrivate {
	guint64 number_of_frames; /* The number of frame that are in
				    the sample */
	guint64 frames_per_page; /* The number of frames that are shown
				   in one page of the timeline */
	guint frames_per_pixel; /* The number of frames that one pixel
				    represents */
	
	guint64 position; /* The position of the cursor */
	guint64 start, finish; /* The position of the page */

	MarlinSampleSelection *selection; /* Selection reference */
	guint changed_id;
};
	
static GtkWidgetClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];

#define DEBUG 1

#ifdef DEBUG
# define d(x) x
#else
# define d(x)
#endif

#define DEFAULT_FRAMES_PER_PIXEL 64

static void
finalize (GObject *object)
{
	MarlinTimeLine *timeline;

	timeline = MARLIN_TIME_LINE (object);

	if (timeline->priv == NULL) {
		return;
	}

	if (timeline->priv->selection != NULL) {
		g_signal_handler_disconnect (G_OBJECT (timeline->priv->selection),
					     timeline->priv->changed_id);
		g_object_unref (G_OBJECT (timeline->priv->selection));
	}
	
	g_free (timeline->priv);
	timeline->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
selection_changed (MarlinSampleSelection *selection,
		   MarlinTimeLine *timeline)
{
	GdkRectangle window;

	window.x = 0;
	window.y = 0;
	window.width = GTK_WIDGET (timeline)->allocation.width;
	window.height = GTK_WIDGET (timeline)->allocation.height;

	gdk_window_invalidate_rect (GTK_WIDGET (timeline)->window,
				    &window, FALSE);
}

static void
set_property (GObject *object,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *pspec)
{
	GtkWidget *widget;
	MarlinTimeLine *timeline;
	MarlinTimeLinePrivate *priv;
	int old_finish, min_finish, max_finish;
	int old_start, min_start, max_start;
	int old_position;
	
	widget = GTK_WIDGET (object);
	timeline = MARLIN_TIME_LINE (object);
	priv = timeline->priv;
	
	switch (prop_id) {
	case PROP_TOTAL:
		priv->number_of_frames = g_value_get_uint64 (value);
		priv->frames_per_pixel = (guint) (priv->number_of_frames / widget->allocation.width);
		if (priv->frames_per_pixel == 0) {
			priv->frames_per_pixel = 1;
		}
		
		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;

			area.x = 0;
			area.y = 0;
			area.width = widget->allocation.width;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}
		
		break;

	case PROP_PER_PAGE:
		old_finish = priv->finish;
		
		priv->frames_per_page = g_value_get_uint64 (value);
		priv->finish = priv->start + priv->frames_per_page;

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;
			int min_x, max_x;

			min_finish = MIN (old_finish, priv->finish);
			max_finish = MAX (old_finish, priv->finish);

			min_x = (int) (min_finish / priv->frames_per_pixel) - 1;
			max_x = (int) (max_finish / priv->frames_per_pixel) + 1;

			area.x = MAX (min_x, 0);
			area.y = 0;
			area.width = MIN (max_x - min_x, widget->allocation.width);
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}
					    
		break;

	case PROP_POSITION:
		old_position = priv->position;
		priv->position = g_value_get_uint64 (value);

		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;

			area.x = (int) (old_position / priv->frames_per_pixel);
			area.y = 0;
			area.width = 1;
			area.height = widget->allocation.height;

 			gdk_window_invalidate_rect (widget->window, &area, FALSE);

			/* Only need to change the x,
			   because the rest is the same */
			area.x = widget->allocation.x + (int) (priv->position / priv->frames_per_pixel);
 			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		g_signal_emit (object, signals[CURSOR_CHANGED], 0, priv->position);
		break;

	case PROP_PAGE_START:
		old_start = priv->start;
		priv->start = g_value_get_uint64 (value);
		priv->finish = priv->start + priv->frames_per_page;
		
		if (GTK_WIDGET_DRAWABLE (widget)) {
			GdkRectangle area;

			min_start = MIN (old_start, priv->start);
			max_start = MAX (old_start, priv->start);

			area.x = (int) (min_start / priv->frames_per_pixel);
			area.y = 0;
			area.width = (int) (((max_start - min_start) + priv->frames_per_page) / priv->frames_per_pixel) + 1;
			area.height = widget->allocation.height;

			gdk_window_invalidate_rect (widget->window, &area, FALSE);
		}

		g_signal_emit (object, signals[PAGE_START_CHANGED], 0, (guint64) priv->start);
		break;

	case PROP_SELECTION:
		if (priv->selection != NULL) {
			g_signal_handler_disconnect (G_OBJECT (priv->selection),
						     priv->changed_id);
			g_object_unref (priv->selection);
		}

		priv->selection = g_value_get_object (value);
		g_object_ref (priv->selection);

		
		priv->changed_id = g_signal_connect (G_OBJECT (priv->selection),
						     "changed",
						     G_CALLBACK (selection_changed), timeline);
		
		break;
	default:
		break;
	}
}

static void
get_property (GObject *object,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *pspec)
{
	MarlinTimeLine *timeline;
	MarlinTimeLinePrivate *priv;

	timeline = MARLIN_TIME_LINE (object);
	priv = timeline->priv;

	switch (prop_id) {
	case PROP_TOTAL:
		g_value_set_uint64 (value, priv->number_of_frames);
		break;

	case PROP_POSITION:
		g_value_set_uint64 (value, priv->position);
		break;

	case PROP_PAGE_START:
		g_value_set_uint64 (value, priv->start);
		break;

	case PROP_PER_PAGE:
		g_value_set_uint64 (value, priv->frames_per_page);
		break;

	case PROP_PER_PIXEL:
		g_value_set_uint (value, priv->frames_per_pixel);
		break;

	case PROP_SELECTION:
		g_value_set_object (value, priv->selection);
		break;
		
	default:
		break;
	}
}

static void
size_allocate (GtkWidget *widget,
	       GtkAllocation *allocation)
{
	MarlinTimeLine *timeline;
	MarlinTimeLinePrivate *priv;
	
	timeline = MARLIN_TIME_LINE (widget);
	priv = timeline->priv;

	priv->frames_per_pixel = (int)(priv->number_of_frames / allocation->width);
	if (priv->frames_per_pixel == 0) {
		priv->frames_per_pixel = 1;
	}
	
	/* Chain up */
	GTK_WIDGET_CLASS (parent_class)->size_allocate (widget, allocation);
}

static void
size_request (GtkWidget *widget,
	      GtkRequisition *requisition)
{
	MarlinTimeLine *timeline;
	MarlinTimeLinePrivate *priv;
	int font_size;

	timeline = MARLIN_TIME_LINE (widget);
	priv = timeline->priv;
	
	requisition->width = (int) (priv->number_of_frames / priv->frames_per_pixel);

	/* Some fonts are huge, and it looks dumb */
	requisition->height = 10;
}

static void
realize (GtkWidget *widget)
{
	/* Add an event to receive the clicks */
	gtk_widget_add_events (widget,
			       GDK_BUTTON_PRESS_MASK |
			       GDK_KEY_PRESS_MASK);

	GTK_WIDGET_CLASS (parent_class)->realize (widget);
}

/*
 * There are 3 parts to the timeline.
 * A: The part that represents the bit before the visible page
 * B: The part that represents the visible page
 * C: The part that represents the bit after the visible page.
 *
 * |==========[         ]==============================|
 * ^^^^^A^^^^^ ^^^^B^^^^ ^^^^^^^^^^^^^^^^^^C^^^^^^^^^^^
 */
static void
_marlin_time_line_paint (MarlinTimeLine *timeline,
			 GdkRectangle *area,
			 GtkStateType state_type)
{
	GtkWidget *widget;
	MarlinTimeLinePrivate *priv;
	GdkRectangle rect, inter;
	GList *p;
	int width, height;
	int x, y;

	priv = timeline->priv;
	widget = GTK_WIDGET (timeline);

	x = 0;
	y = 0;
	width = widget->allocation.width;
	height = widget->allocation.height;

	/* Special case when there's no frames */
	if (priv->number_of_frames == 0) {
		rect.x = x;
		rect.y = y;
		rect.width = width;
		rect.height = height;

		if (gdk_rectangle_intersect (area, &rect, &inter)) {
			gdk_draw_rectangle (widget->window,
					    widget->style->dark_gc[state_type],
					    TRUE, inter.x, inter.y,
					    inter.width, inter.height);
		}

		return;
	}
	
	/* Generate the rectangle for A */
	rect.x = x;
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->start / priv->frames_per_pixel);

	/* Check if the area intersects with A */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->dark_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Generate the rectangle for B */
	rect.x = x + (int) (priv->start / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->frames_per_page / priv->frames_per_pixel);

	/* Check if area intersects with B and draw only the background */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->base_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Generate the rectangle for C */
	rect.x = x + (int) (priv->finish / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = width - rect.x;

	/* Check if the area intersects with C */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->dark_gc[state_type],
				    TRUE, inter.x, inter.y,
				    inter.width, inter.height);
	}

	/* Draw the selections */
	if (priv->selection != NULL) {
		MarlinCoverage sel_coverage;
		guint64 sel_start, sel_finish;

		marlin_sample_selection_get (priv->selection, &sel_coverage,
					     &sel_start, &sel_finish);

		rect.x = (int) (sel_start) / priv->frames_per_pixel;
		switch (sel_coverage) {
		case MARLIN_COVERAGE_BOTH:
			rect.y = 0;
			rect.height = height;
			break;
			
		case MARLIN_COVERAGE_LEFT:
			rect.y = 0;
			rect.height = (int) (height / 2);
			break;
			
		case MARLIN_COVERAGE_RIGHT:
			rect.y = (int) (height / 2);
			rect.height = (int) (height / 2);
			break;
			
		default:
			break;
		}
		
		/* Do the divisions before the subtraction due to
		   rounding errors that occur the other way round */
		rect.width = (int) (sel_finish / priv->frames_per_pixel) - (sel_start / priv->frames_per_pixel);
		
		if (gdk_rectangle_intersect (area, &rect, &inter)) {
			gdk_draw_rectangle (widget->window,
					    widget->style->base_gc[GTK_STATE_SELECTED],
					    TRUE, inter.x, inter.y,
					    inter.width, inter.height);
		}
	}
	
	/* Draw cursor */
	gdk_draw_line (widget->window,
		       widget->style->text_gc[state_type],
		       x + (int) (priv->position / priv->frames_per_pixel),
		       y,
		       x + (int) (priv->position / priv->frames_per_pixel),
		       height);
	
	/* Generate the rectangle for B to draw the decals */
	rect.x = x + (int) (priv->start / priv->frames_per_pixel);
	rect.y = y;
	rect.height = height;
	rect.width = (int) (priv->frames_per_page / priv->frames_per_pixel);

	/* Check if area intersects with B */
	if (gdk_rectangle_intersect (area, &rect, &inter)) {
		GdkPoint points[4];

		gdk_draw_line (widget->window,
			       widget->style->text_gc[state_type],
			       rect.x, rect.y,
			       (rect.x + rect.width) - 1, rect.y);

		gdk_draw_line (widget->window,
			       widget->style->text_gc[state_type],
			       rect.x, (rect.y + height) - 1,
			       (rect.x + rect.width) - 1,
			       (rect.y + height) - 1);

		/* Draw page end */
		points[0].x = rect.x;
		points[0].y = rect.y + 1;
		points[1].x = rect.x;
		points[1].y = (rect.y + height) - 2;
 		points[2].x = (rect.x + rect.width) - 1;
		points[2].y = rect.y + 1;
		
		points[3].x = (rect.x + rect.width) - 1;
		points[3].y = (rect.y + height) - 2;

		gdk_draw_points (widget->window,
				 widget->style->text_gc[state_type],
				 points, 4);
	}
}

static int
expose_event (GtkWidget *widget,
	      GdkEventExpose *event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		MarlinTimeLine *timeline = MARLIN_TIME_LINE (widget);

		_marlin_time_line_paint (timeline, &event->area,
					 GTK_WIDGET_STATE (widget));
	}
	
	return FALSE;
}

static int
button_press_event (GtkWidget *widget,
		    GdkEventButton *event)
{
	MarlinTimeLine *timeline;
	int x;
	guint64 position;

	timeline = MARLIN_TIME_LINE (widget);
	
	x = event->x;
	position = (guint) (x * timeline->priv->frames_per_pixel);

	switch (event->button) {
	case 1:
		g_object_set (G_OBJECT (widget),
			      "cursor_position", position,
			      NULL);
		
		/* Move the page as well */
		if (event->type == GDK_2BUTTON_PRESS) {
			guint64 start, half, end;
			
			half = (guint) (timeline->priv->frames_per_page / 2);
			
			/* Check it is always >= 0 */
			start = MAX ((int) (position - half), 0);
			
			/* Check it's never > end */
			end = timeline->priv->number_of_frames -
				timeline->priv->frames_per_page;
			start = MIN (start, end);
			
			g_object_set (G_OBJECT (widget),
				      "page_start", start,
				      NULL);
		}

		break;

	case 3:
		/* Start/Stop playback at position */
		g_print ("TODO: Start / Stop playback at %u\n", position);
		break;

	default:
		break;
	}

	return FALSE;
}

static void
class_init (MarlinTimeLineClass *klass)
{
	GObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = G_OBJECT_CLASS (klass);
	widget_class = GTK_WIDGET_CLASS (klass);

	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	widget_class->size_allocate = size_allocate;
	widget_class->size_request = size_request;
	widget_class->realize = realize;
	widget_class->expose_event = expose_event;
	widget_class->button_press_event = button_press_event;
	
	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (object_class,
					 PROP_TOTAL,
					 g_param_spec_uint64 ("total_frames",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PER_PAGE,
					 g_param_spec_uint64 ("frames_per_page",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_POSITION,
					 g_param_spec_uint64 ("cursor_position",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_PAGE_START,
					 g_param_spec_uint64 ("page_start",
							      "",
							      "",
							      0,
							      G_MAXUINT64,
							      0,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_PER_PIXEL,
					 g_param_spec_uint ("frames_per_pixel",
							    "",
							    "",
							    0,
							    G_MAXUINT,
							    0,
							    G_PARAM_READABLE));
	g_object_class_install_property (object_class,
					 PROP_SELECTION,
					 g_param_spec_object ("selection",
							      "", "",
							      MARLIN_SAMPLE_SELECTION_TYPE,
							      G_PARAM_READWRITE));
	
	signals[CURSOR_CHANGED] = g_signal_new ("cursor-changed",
						G_TYPE_FROM_CLASS (klass),
						G_SIGNAL_RUN_FIRST |
						G_SIGNAL_NO_RECURSE,
						G_STRUCT_OFFSET (MarlinTimeLineClass, cursor_changed),
						NULL, NULL,
						marlin_marshal_VOID__UINT64,
						G_TYPE_NONE,
						1, G_TYPE_UINT64);
	signals[PAGE_START_CHANGED] = g_signal_new ("page-start-changed",
						    G_TYPE_FROM_CLASS (klass),
						    G_SIGNAL_RUN_FIRST |
						    G_SIGNAL_NO_RECURSE,
						    G_STRUCT_OFFSET (MarlinTimeLineClass, page_start_changed),
						    NULL, NULL,
						    marlin_marshal_VOID__UINT64,
						    G_TYPE_NONE,
						    1, G_TYPE_UINT64);
}

static void
init (MarlinTimeLine *timeline)
{
	timeline->priv = g_new0 (MarlinTimeLinePrivate, 1);
	timeline->priv->frames_per_pixel = DEFAULT_FRAMES_PER_PIXEL;
}

GType
marlin_time_line_get_type (void)
{
	static GType type = 0;

	if (type == 0) {
		GTypeInfo info = {
			sizeof (MarlinTimeLineClass), NULL, NULL,
			(GClassInitFunc) class_init, NULL, NULL,
			sizeof (MarlinTimeLine), 0, (GInstanceInitFunc) init
		};

		type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					       "MarlinTimeLine",
					       &info, 0);
	}

	return type;
}

GtkWidget *
marlin_time_line_new (guint64 total_frames,
		      guint64 frames_per_page)
{
	MarlinTimeLine *timeline;

	timeline = g_object_new (MARLIN_TIME_LINE_TYPE,
				 "total_frames", total_frames,
				 "frames_per_page", frames_per_page,
				 NULL);
	return GTK_WIDGET (timeline);
}
