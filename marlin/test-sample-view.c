/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@prettypeople.org>
 *
 *  Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gtk/gtk.h>

#include <gst/gst.h>

#include <marlin-sample.h>
#include <marlin-sample-view.h>
#include <marlin-channel.h>
#include <marlin-block.h>
#include <marlin/marlin-operation.h>
#include <marlin/marlin-load-pipeline.h>

GtkWidget *progress_window = NULL, *progress_label, *progress_bar;
MarlinSample *sample;
MarlinSampleView *view;

static void
destroy_window (GObject *window,
		MarlinSampleView *view)
{
	g_object_unref (G_OBJECT (sample));
	
	gtk_main_quit ();
}

static void
load_started (MarlinOperation *operation,
	      gpointer data)
{
	char *filename, *title;
	GtkWidget *vbox;

	g_print ("Hello? - %d\n", pthread_self ());
	g_object_get (G_OBJECT (sample),
		      "filename", &filename,
		      NULL);
	
	progress_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	title = g_strdup_printf ("Loading %s", filename);
	gtk_window_set_title (GTK_WINDOW (progress_window), title);
	g_free (title);

	vbox = gtk_vbox_new (FALSE, 0);

	gtk_container_add (GTK_CONTAINER (progress_window), vbox);

	title = g_strdup_printf ("Loading %s", filename);
	g_free (filename);
	progress_label = gtk_label_new (title);
	g_free (title);
	gtk_box_pack_start (GTK_BOX (vbox), progress_label, FALSE, FALSE, 0);

	progress_bar = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), progress_bar, FALSE, FALSE, 0);

	gtk_widget_show_all (progress_window);
}

static void
sample_notify (MarlinSample *sample,
	       GParamSpec *pspec,
	       gpointer data)
{
#if 0
	char *text, *filename;
	guint64 frames;
	
	if (strcmp (g_param_spec_get_name (pspec), "total-frames") != 0) {
		return;
	}

	g_print ("Notify self: %d\n", pthread_self ());
	g_object_get (G_OBJECT (sample),
		      "total_frames", &frames,
		      NULL);
	
	/* Check the progress window exists */
	if (progress_window == NULL) {
		return;
	}

	g_object_get (G_OBJECT (sample),
		      "filename", &filename,
		      NULL);

	text = g_strdup_printf ("Loading %s (%llu)", filename, frames);
	g_free (filename);
	gtk_label_set_text (GTK_LABEL (progress_label), text);
	g_free (text);
#endif
}

static void
progress_changed (MarlinOperation *operation,
		  int progress,
		  gpointer data)
{
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), (float) progress / 100.0);
}

struct _idle_data {
	MarlinSample *sample;
	char *filename;
};

static void
load_finished (MarlinOperation *operation,
	       gpointer data)
{
	int number, rate;
	int channels;
	char *filename;
	
	gtk_object_destroy (GTK_OBJECT (progress_window));
	
	g_object_get (sample,
		      "total_frames", &number,
		      "sample_rate", &rate,
		      "channels", &channels,
		      "filename", &filename,
		      NULL);
	
	g_print ("Information for %s\n", filename);
	g_print ("--------------------\n");
	g_print ("Number of samples: %d\n", number);
	g_print ("Sample rate: %d\n", rate);
	g_print ("Channels: %d\n", channels);

	g_free (filename);

	/* Set the sample on the view now */
	g_object_set (G_OBJECT (view),
		      "sample", sample,
		      NULL);
}

static gboolean
idle_func (struct _idle_data *data)
{
	MarlinOperation *operation;
	MarlinLoadPipeline *load;
	g_print ("Loading %s\n", data->filename);

	operation = marlin_operation_new ();
	g_signal_connect (G_OBJECT (operation), "started",
			  G_CALLBACK (load_started), NULL);
	g_signal_connect (G_OBJECT (operation), "finished",
			  G_CALLBACK (load_finished), NULL);
	g_signal_connect (G_OBJECT (operation), "progress",
			  G_CALLBACK (progress_changed), NULL);
	
	load = marlin_load_pipeline_new (operation);
	g_object_set (G_OBJECT (load),
		      "filename", data->filename,
		      "sample", data->sample,
		      NULL);

	marlin_pipeline_set_state (MARLIN_PIPELINE (load), GST_STATE_PLAYING);
	return FALSE;
}

static void
cursor_move (MarlinSampleView *view,
	     GtkMovementStep step,
	     int count,
	     gboolean extend_selection,
	     gpointer userdata)
{
#ifdef I_WANT_IRRITATING_CRAP_PRINTED
	g_print ("Cursor moved %d frames to the %s\n", abs (count),
		 (count < 0 ? "left" : "right"));
#endif
}

int
main (int argc,
      char **argv)
{
	GtkWidget *window, *sw;
	float length;
	int frames;
	struct _idle_data data;
	
	gtk_init (&argc, &argv);

	g_thread_init (NULL);

	gst_init (&argc, &argv);
	marlin_mt_initialise ();

	g_print ("Self - %d\n", pthread_self ());
	if (argc < 3) {
		g_print ("Usage: %s <file> <frames_per_pixel>\n", argv[0]);
		exit (1);
	}

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	sample = marlin_sample_new ();
	data.sample = sample;
	data.filename = argv[1];

	g_signal_connect (G_OBJECT (sample), "notify",
			  G_CALLBACK (sample_notify), NULL);
	
	view = marlin_sample_view_new (atoi (argv[2]));

	g_signal_connect (G_OBJECT (view), "move_cursor",
			  G_CALLBACK (cursor_move), NULL);
	
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (destroy_window), view);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_ALWAYS,
					GTK_POLICY_NEVER);
	gtk_container_add (GTK_CONTAINER (sw), view);
	
	gtk_container_add (GTK_CONTAINER (window), sw);

	gtk_widget_show_all (window);

	g_idle_add (idle_func, &data);
	gtk_main ();
}
