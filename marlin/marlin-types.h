/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifndef __MARLIN_TYPES_H__
#define __MARLIN_TYPES_H__

#include <glib-object.h>

typedef struct _MarlinSample MarlinSample;
typedef struct _MarlinChannel MarlinChannel;
typedef struct _MarlinBlock MarlinBlock;

/* Define this by hand here so that we don't have to include ladspa.h
   This will probably break if someone else includes ladspa.h and this */
typedef struct _LADSPA_Descriptor LADSPA_Descriptor;

typedef struct _MarlinPeak {
	short low;
	short high;
	short avg_positive;
	short avg_negative;
} MarlinPeak;

typedef enum _MarlinChannelPosition {
	MARLIN_CHANNEL_LEFT,
	MARLIN_CHANNEL_RIGHT,
} MarlinChannelPosition;

typedef enum _MarlinDisplay {
	MARLIN_DISPLAY_FRAMES,
	MARLIN_DISPLAY_TIME_LONG,
	MARLIN_DISPLAY_SECONDS,
	MARLIN_DISPLAY_TIME_FRAMES,
	MARLIN_DISPLAY_BEATS,
	MARLIN_DISPLAY_NULL
} MarlinDisplay;

typedef enum _MarlinScale {
	MARLIN_SCALE_LINEAR,
	MARLIN_SCALE_LOG,
} MarlinScale;

typedef float (* MarlinFadeFunc) (guint64 position,
				  gpointer closure);

typedef enum {
	MARLIN_COVERAGE_BOTH,
	MARLIN_COVERAGE_LEFT,
	MARLIN_COVERAGE_RIGHT,
	MARLIN_COVERAGE_NONE,
} MarlinCoverage;

/* A lot of the functions take ranges
   Using this as a type to hold a range might be useful
   FIXME? */
typedef struct _MarlinRange {
	guint64 start;
	guint64 finish;
	MarlinCoverage coverage;
} MarlinRange;

typedef enum {
	MARLIN_TIMESIG_TWO_FOUR,
	MARLIN_TIMESIG_THREE_FOUR,
	MARLIN_TIMESIG_FOUR_FOUR,
	MARLIN_TIMESIG_FIVE_FOUR,
	MARLIN_TIMESIG_SIX_EIGHT,
	MARLIN_TIMESIG_SEVEN_EIGHT,
	MARLIN_TIMESIG_NINE_EIGHT
} MarlinTimesig;

#define MARLIN_TYPE_DISPLAY (marlin_display_get_type ())
#define MARLIN_TYPE_SCALE (marlin_scale_get_type ())
#define MARLIN_TYPE_COVERAGE (marlin_coverage_get_type ())
#define MARLIN_TYPE_TIMESIG (marlin_timesig_get_type ())

GType marlin_scale_get_type (void);
GType marlin_display_get_type (void);
GType marlin_coverage_get_type (void);
GType marlin_timesig_get_type (void);


#endif
