/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002-2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include <gst/gst.h>

#include <marlin/marlin-gst-extras.h>
#include <marlin/marlin-channel-joiner.h>
#include <marlin/marlin-channel-sink.h>
#include <marlin/marlin-channel-src.h>
#include <marlin/marlin-channel-splitter.h>

struct _maps {
	char *mimetype;
	char *factoryname;
};

GList *
get_mime_types (void)
{
#if 0
	GList *factories, *templs;
	GstElementFactory *factory;
	GstCaps *caps;
	gboolean src;
	static GList *out_mime_types = NULL;

	if (out_mime_types) {
		return out_mime_types;
	}

	factories = gst_registry_pool_feature_list (GST_TYPE_ELEMENT_FACTORY);
	g_return_val_if_fail (factories != NULL, NULL);

	while (factories) {
		const char *mime_type = NULL;

		src = FALSE;
		
		factory = (GstElementFactory *) factories->data;
		templs = factory->padtemplates;

		while (templs) {
			int i;
			if (GST_PAD_TEMPLATE_DIRECTION (templs->data) == GST_PAD_SRC) {
				GstStructure *structure;

				caps = GST_PAD_TEMPLATE_CAPS (templs->data);
				if (gst_caps_get_size (caps) < 1) {
					templs = templs->next;
					continue;
				}

				for (i = 0; i < gst_caps_get_size (caps); i++) {
					const char *name;

					structure = gst_caps_get_structure (caps, i);
					name = gst_structure_get_name (structure);
					if (strcmp ("audio/x-raw-int", name) == 0 ||
					    strcmp ("audio/x-raw-float", name) == 0) {
						/* We've got the src... */
						src = TRUE;
					}
				}
			} else if (GST_PAD_TEMPLATE_DIRECTION (templs->data) == GST_PAD_SINK) {
				GstStructure *structure;

				caps = GST_PAD_TEMPLATE_CAPS (templs->data);
				if (gst_caps_get_size (caps) < 1) {
					templs = templs->next;
					continue;
				}

				structure = gst_caps_get_structure (caps, 0);
				mime_type = gst_structure_get_name (structure);
			}

			templs = templs->next;

		}

		if (src && mime_type && strcmp ("text/plain", mime_type) 
		    && g_list_find_custom (out_mime_types, mime_type, 
					   (GCompareFunc) strcmp) == NULL) {
			out_mime_types = g_list_prepend (out_mime_types, 
							 g_strdup (mime_type));

			/* Ugly hack because shared-mime-info gets mp3's 
			   mimetype wrong. it should be audio/mpeg, but they
			   have audio/x-mp3 */
			if (strcmp (mime_type, "audio/mpeg") == 0) {
				out_mime_types = g_list_prepend (out_mime_types,
								 g_strdup ("audio/x-mp3"));
			}
		}
	
		factories = factories->next;
	}
	
	return out_mime_types;
#endif
	return NULL;
}


static GstElementFactory *
get_factory_for_mime (const char *sink_mime_id,
		      const char *src_mime_id)
{
#if 0
	GList *factories, *templs;
	GstElementFactory *factory;
	GstCaps *caps;
	gboolean src, sink;
	
	factories = gst_registry_pool_feature_list (GST_TYPE_ELEMENT_FACTORY);
	g_return_val_if_fail (factories != NULL, NULL);
	
	while (factories) {
		src = sink = FALSE;
		
		factory = (GstElementFactory *) factories->data;
		templs = factory->padtemplates;

		while (templs) {
			if (GST_PAD_TEMPLATE_DIRECTION (templs->data) == GST_PAD_SINK) {
				GstStructure *structure;

				caps = GST_PAD_TEMPLATE_CAPS (templs->data);
				if (gst_caps_get_size (caps) < 1) {
					templs = templs->next;
					continue;
				}
				
				structure = gst_caps_get_structure (caps, 0);
				if (strcmp (gst_structure_get_name (structure), sink_mime_id) == 0) {
					/* We've got the sink... */
					sink = TRUE;
				} else {
					sink = FALSE;
				}
			} else if (GST_PAD_TEMPLATE_DIRECTION (templs->data) == GST_PAD_SRC) {
				GstStructure *structure;

				caps = GST_PAD_TEMPLATE_CAPS (templs->data);
				if (gst_caps_get_size (caps) < 1) {
					templs = templs->next;
					continue;
				}

				structure = gst_caps_get_structure (caps, 0);
				if (strcmp (gst_structure_get_name (structure), src_mime_id) == 0) {
					/* We've got the src */
					src = TRUE;
				} else {
					src = FALSE;
				}
			}

			/* If we're here and src and sink are true
			   we've got our encoder. */
			if (src && sink) {
				return factory;
			}

			templs = templs->next;
		}

		factories = factories->next;
	}
#endif
	return NULL;
}

GstElement *
get_decoder_for_mime (const char *mimetype,
		      const char *name)
{
	struct _maps mime_to_pipeline[] = {
		{ "audio/x-mp3", "mad" },
		{ "audio/mpeg", "mad" },
		{ "application/ogg", "vorbisfile" },
		{ "application/x-ogg", "vorbisfile" },
		{ "audio/x-wav", "wavparse" },
		{ "audio/x-m4a", "qtdemux ! faad" },
		{ "audio/MP4A-LATM", "qtdemux ! faad" },
		{ "audio/mp4", "qtdemux ! faad" },
		{ NULL, NULL },
	};
	int i;
	GstElementFactory *factory;
	
	for (i = 0; mime_to_pipeline[i].mimetype; i++) {
		if (strcmp (mimetype, mime_to_pipeline[i].mimetype) == 0) {
			return gst_element_factory_make (mime_to_pipeline[i].factoryname, name);
		}
	}

	factory = get_factory_for_mime (mimetype, "audio/x-raw-int");

	if (factory != NULL) {
		return gst_element_factory_create (factory, name);
	} else {
		return NULL;
	}
}

#if 0
GstElement *
get_decoder_for_mime (const char *mimetype,
		      const char *name)
{
	GstElementFactory *factory;

	factory = get_decoder_factory_for_mime (mimetype);
	if (factory == NULL) {
		return NULL;
	}

	return gst_element_factory_create (factory, name);
}
#endif

#if 0
static GstElementFactory *
get_default_encoder_factory_for_mime (const char *mimetype)
{
	return get_factory_for_mime ("audio/x-raw-int", mimetype);
}
#endif

struct _maps mime_to_fact[] = {
	{ "audio/x-mp3", "lame name=encoder-element ! audio/mpeg ! id3mux" },
	{ "audio/mpeg", "lame name=encoder-element ! audio/mpeg ! id3mux" },
	{ "application/ogg", "vorbisenc name=encoder-element ! oggmux" },
	{ "application/x-ogg", "vorbisenc name=encoder-element ! oggmux" },
	{ "audio/x-wav", "wavenc name=encoder-element" },
	{ "audio/x-flac", "flacenc name=encoder-element ! oggmux" },
	{ "audio/x-speex", "speexenc name=encoder-element ! oggmux" },
	{ NULL, NULL },
};

GstElement *
create_bin_from_pipeline (const char *pipeline)
{
	GstElement *encoder;
	char *full;
	GError *err = NULL;

	full = g_strdup_printf ("(name=profile-encoder identity name=encoder_start ! %s ! identity name=encoder_end )", pipeline);
	encoder = gst_parse_launch (full, &err);
	if (err != NULL) {
		g_warning ("Error parsing pipeline");
		g_free (full);
		g_error_free (err);
		encoder = NULL;
	} else {
		GstPad *src, *sink;
		GstElement *id;

		/* Hook up ghost pads to the start and end */
		id = gst_bin_get_by_name (GST_BIN (encoder), "encoder_start");
		g_assert (id != NULL);
		
		sink = gst_element_get_pad (id, "sink");
		gst_element_add_pad (encoder, 
				     gst_ghost_pad_new ("sink-ghost", sink));
		
		id = gst_bin_get_by_name (GST_BIN (encoder), "encoder_end");
		g_assert (id != NULL);
		
		src = gst_element_get_pad (id, "src");
		gst_element_add_pad (encoder, 
				     gst_ghost_pad_new ("src-ghost", src));
		
 		g_print ("Using %s as pipeline\n", full);
		g_free (full);
	}

	return encoder;
}

GstElement *
get_encoder_for_mime (const char *mimetype)
{
	int i;
/* 	GstElementFactory *factory; */

	for (i = 0; mime_to_fact[i].mimetype; i++) {
		if (strcmp (mimetype, mime_to_fact[i].mimetype) == 0) {
			return  create_bin_from_pipeline (mime_to_fact[i].factoryname);
		}
	}

#if 0
	factory = get_default_encoder_factory_for_mime (mimetype);
	if (factory) {
		return gst_element_factory_create (factory, "encoder");
	}
#endif

	g_warning ("Unknown mimetype: %s", mimetype);
	return NULL;
}

char *
get_string_from_caps (GstCaps *caps,
		      const char *key)
{
	const GValue *value;
	GstStructure *structure;

	if (caps == NULL) {
		return g_strdup (_("Unknown"));
	}
	
	if (gst_caps_get_size (caps) < 1) {
		return g_strdup (_("Unknown"));
	}

	structure = gst_caps_get_structure (caps, 0);
	value = gst_structure_get_value (structure, key);
	if (value == NULL) {
		return NULL;
	}

	if (G_VALUE_TYPE(value) == G_TYPE_INT) {
		return g_strdup_printf ("%d", g_value_get_int (value));
	}else if (G_VALUE_TYPE(value) == G_TYPE_STRING) {
		return g_strdup_printf ("%s", g_value_get_string (value));
	}else {
		return g_strdup (_("Unknown"));
	}

	return NULL;
}

int
get_int_from_caps (GstCaps *caps,
		   const char *key)
{
	const GValue *value;
	GstStructure *structure;

	if (caps == NULL) {
		return -1;
	}
	
	if (gst_caps_get_size (caps) < 1) {
		return -1;
	}

	structure = gst_caps_get_structure (caps, 0);
	value = gst_structure_get_value (structure, key);
	if (value == NULL) {
		return -1;
	}

	if (G_VALUE_TYPE(value) == G_TYPE_INT) {
		return g_value_get_int (value);
	}else {
		return -1;
	}
	return -1;
}

char *
get_glist_from_caps (GstCaps *caps,
		     const char *key)
{
	const GValue *value;
	GstStructure *structure;

	if (caps == NULL) {
		return NULL;
	}
	
	if (gst_caps_get_size (caps) < 1) {
		return g_strdup (_("Unknown"));
	}

	structure = gst_caps_get_structure (caps, 0);
	value = gst_structure_get_value (structure, key);
	if (value == NULL) {
		return NULL;
	}

	/* FIXME */

	return NULL;
}

struct _vfs_map {
	char *vfs;
	char *gst;
};

struct _vfs_map vfs_to_gst[] = {
	{ "audio/x-mp3", "audio/mpeg" },
	{ "application/x-ogg", "application/ogg" },
	{ NULL, NULL }
};

const char *
marlin_vfs_mime_to_gst (const char *vfs)
{
	int i;

	for (i = 0; vfs_to_gst[i].vfs; i++) {
		if (strcmp (vfs, vfs_to_gst[i].vfs) == 0) {
			return vfs_to_gst[i].gst;
		}
	}

	return vfs;
}

const char *
marlin_gst_mime_to_vfs (const char *gst)
{
	int i;

	for (i = 0; vfs_to_gst[i].gst; i++) {
		/* Returns the first mapping from gst to vfs */
		if (strcmp (gst, vfs_to_gst[i].gst) == 0) {
			return vfs_to_gst[i].vfs;
		}
	}

	return gst;
}

gboolean
marlin_gst_can_encode (const char *mimetype)
{
	int i;

	for (i = 0; mime_to_fact[i].mimetype; i++) {
		if (strcmp (mimetype, mime_to_fact[i].mimetype) == 0) {
			return TRUE;
		}
	}

	return FALSE;
}

GstElement *
marlin_make_gst_source (const char *name)
{
	char *srcs[] = { "giosrc", "gnomevfssrc", "filesrc", NULL };
	int i;

	for (i = 0; srcs[i]; i++) {
		GstElement *src;

		src = gst_element_factory_make (srcs[i], name);
		if (src) {
			return src;
		}
	}

	return NULL;
}

static void
register_element (GstPlugin  *plugin,
		  const char *name,
		  GType       type)
{
	gboolean ret;

	ret = gst_element_register (plugin, name, GST_RANK_NONE, type);
	
	if (ret == FALSE) {
		g_warning ("Failed to register %s", name);
	} 
}
	
static gboolean
register_elements (GstPlugin *plugin)
{
	register_element (plugin, "marlin-channel-sink",
			  MARLIN_CHANNEL_SINK_TYPE);
	register_element (plugin, "marlin-channel-src",
			  MARLIN_CHANNEL_SRC_TYPE);
	register_element (plugin, "marlin-channel-splitter", 
			  MARLIN_CHANNEL_SPLITTER_TYPE);
	register_element (plugin, "marlin-channel-joiner",
			  MARLIN_CHANNEL_JOINER_TYPE);
	return TRUE;
}

static GstPluginDesc plugin_desc = {
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	"marlin-plugins",
	"GStreamer plugins for Marlin",
	register_elements,
	VERSION,
	"LGPL",
	PACKAGE,
	"marlin",
	"http://marlin.sf.net",
	GST_PADDING_INIT
};

void
marlin_gst_register (void)
{
	gst_tag_register (MARLIN_TAG_BPM, GST_TAG_FLAG_META,
			  G_TYPE_STRING, "", "",
			  gst_tag_merge_strings_with_comma);

	_gst_plugin_register_static (&plugin_desc);
}
